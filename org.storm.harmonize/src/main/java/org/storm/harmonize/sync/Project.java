/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.harmonize.sync;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.harmonize.Harmonizer;

import aQute.bnd.maven.support.Maven;
import aQute.bnd.maven.support.ProjectPom;
import aQute.bnd.osgi.Builder;
import aQute.bnd.osgi.Jar;

/**
 * A model of a project in eclipse.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Project
{
	private static final Logger s_log = LoggerFactory.getLogger(Project.class);

	/** The project path. */
	private final String projectPath;

	/** The source path. */
	private final String sourcePath;

	/** The bin path. */
	private final String binPath;

	/** The output file. */
	private final String outputFile;

	/** The watcher for this project. */
	private WatchKey watchKey;

	/** The watch service. */
	private WatchService watchService;

	/** The project bomb. */
	private ProjectPom projectPom;

	/** The pom file. */
	private String pom;

	/** Maven helper */
	private Maven maven;

	/**
	 * Instantiates a new project.
	 * 
	 * @param projectPath
	 *            the project path
	 * @param sourcePath
	 *            the source path
	 * @param binPath
	 *            the bin path
	 * @param outputFile
	 *            the output file
	 */
	public Project(String pomFile, String projectPath, String sourcePath, String binPath, String outputFile)
	{
		this.pom = pomFile;
		this.projectPath = projectPath;
		this.sourcePath = sourcePath;
		this.binPath = binPath;
		this.outputFile = outputFile;
		init();
	}

	/**
	 * Inits the.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void init()
	{
		try
		{
			watchService = Harmonizer.INSTANCE.getWatchService();
			File watchDirFile = new File(binPath);
			Path watchDirPath = watchDirFile.toPath();

			watchKey = watchDirPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
			Maven mvn = new Maven(Executors.newCachedThreadPool());
			projectPom = mvn.createProjectModel(new File(pom));
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}

	}

	/**
	 * Gets the project path.
	 * 
	 * @return the project path
	 */
	public String getProjectPath()
	{
		return projectPath;
	}

	/**
	 * Gets the source path.
	 * 
	 * @return the source path
	 */
	public String getSourcePath()
	{
		return sourcePath;
	}

	/**
	 * Gets the bin path.
	 * 
	 * @return the bin path
	 */
	public String getBinPath()
	{
		return binPath;
	}

	/**
	 * Gets the output file.
	 * 
	 * @return the output file
	 */
	public String getOutputFile()
	{
		return outputFile;
	}

	/**
	 * Gets the watch key.
	 * 
	 * @return the watch key
	 */
	public WatchKey getWatchKey()
	{
		return watchKey;
	}

	/**
	 * Gets the watch service.
	 * 
	 * @return the watch service
	 */
	public WatchService getWatchService()
	{
		return watchService;
	}

	/**
	 * Prints the string representation of this class.
	 * 
	 * @return the string representation of this class
	 */
	public String toString()
	{
		return "Project [projectPath=" + projectPath + ", sourcePath=" + sourcePath + ", binPath=" + binPath + ", outputFile=" + outputFile + "]";
	}
	
	@SuppressWarnings("resource")
	public File update()
	{
		File theFile = null;
		try
		{

			Builder b = new Builder();
			b.addClasspath(new File(binPath));
			Jar jar = b.build();
			jar.write(theFile);
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}
		return theFile;
	}

	public void remove()
	{

	}

}
