/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.harmonize;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.harmonize.locator.ArtifactDiscovery;
import org.storm.harmonize.locator.Locator;
import org.storm.harmonize.sync.Project;
import org.storm.harmonize.workspace.Workspace;

/**
 * The Enum Harmonizer.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unused")
public enum Harmonizer
{
	
	/** The instance. */
	INSTANCE;

	/** The Constant s_log. */
	private static final Logger s_log = LoggerFactory.getLogger(Harmonizer.class);
	
	/** The filesystem. */
	private FileSystem filesystem = FileSystems.getDefault();
	
	/** The locator. */
	private Locator locator;
	
	/** The workspace. */
	private Workspace workspace;
	
	/** The discovery. */
	private ArtifactDiscovery discovery;
	
	/** The executor. */
	private ExecutorService executor = Executors.newCachedThreadPool();
	
	/** The projects. */
	private List<Project> projects;
	/**
	 * Retrieve the native watch service provided by the nio package.
	 * 
	 * @return the native watch service.
	 */
	public WatchService getWatchService()
	{
		try
		{
			return filesystem.newWatchService();
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Start.
	 * 
	 * @param context
	 *            the context
	 */
	public void start(BundleContext context)
	{
		String stormHome = System.getProperty("storm.home");
		discovery = new ArtifactDiscovery();
		try
		{
			Files.walkFileTree(Paths.get(stormHome, new String[0]), discovery);
			projects = discovery.getProjects();
			
			executor.execute(new ChangeListener());
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
	}
	
	/**
	 * The listener interface for receiving change events. The class that is interested in processing a change event implements this interface, and the object created with that
	 * class is registered with a component using the component's <code>addChangeListener<code> method. When
	 * the change event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see ChangeEvent
	 */
	private class ChangeListener implements Runnable{

		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			while (projects.size() > 0)
			{
				for (Project p : projects)
				{
					WatchKey key = p.getWatchService().poll();
					if (key == null) continue;
					List<WatchEvent<?>> watches = key.pollEvents();
					if (watches != null && watches.size() > 0)
					{
						for (WatchEvent<?> watch : watches)
						{
							if(watch.kind() == (ENTRY_CREATE)) {
								p.update();
							}
							else if(watch.kind() == ENTRY_DELETE) {
								p.remove();
							}
							else if(watch.kind() == ENTRY_MODIFY) {
								p.update();
							}
						}
					}
				}
			}
			
		}
		
	}
	
	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Harmonizer.INSTANCE.start(null);
	}
}
