/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.harmonize.locator;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import org.storm.harmonize.Constants;
import org.storm.harmonize.sync.Project;

/**
 * Discovers system jars on the file system.
 * 
 * @author Trung Khuu
 * @since 1.0
 *
 */
public class ArtifactDiscovery extends SimpleFileVisitor<Path> implements Constants
{
	private List<Project> configuredProjects = new ArrayList<>();
	
	/* (non-Javadoc)
	 * @see java.nio.file.SimpleFileVisitor#visitFile(java.lang.Object, java.nio.file.attribute.BasicFileAttributes)
	 */
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
	{
		File currFile =	file.toFile();
		File parentFile = currFile.getParentFile();
		String parentPath = parentFile.getPath();
		String fileName = currFile.getName();
		if(POM.equals(fileName)) {
			;
			Project project = new Project(currFile.getAbsolutePath(), parentFile.getName(), parentPath + DEFAULT_SRC, parentPath + DEFAULT_TARGET, parentPath + "/target/" + parentFile.getName() + "-1.0.0.jar");
			configuredProjects.add(project);
			
		}
		
		return FileVisitResult.CONTINUE;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Project> getProjects() {
		return configuredProjects;
	}
}
