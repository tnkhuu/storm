/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.harmonize.locator;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Locates Bundles
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Locator
{
	private final Logger s_log = LoggerFactory.getLogger(Locator.class);
	private final FileSystem fs = FileSystems.getDefault();
	
	/**
	 * Instantiates a new locator.
	 */
	public Locator() {
		
	}

	/**
	 * Locate bundles.
	 * 
	 * @param stormHome
	 *            the storm home
	 */
	public void locateBundles(String stormHome)
	{
		Path home = fs.getPath(stormHome);
		try (DirectoryStream<Path> flow = Files.newDirectoryStream(home, "*.jar"))
		{
			for (Path item : flow)
			{
				BasicFileAttributes attrs = Files.readAttributes(item, BasicFileAttributes.class);
				Date creationDate = new Date(attrs.creationTime().toMillis());
				Date accessDate = new Date(attrs.lastAccessTime().toMillis());
				Date updateDate = new Date(attrs.lastModifiedTime().toMillis());
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

				System.out.format("%s\t%s\t%s\t%s%n", item, df.format(creationDate), df.format(accessDate), df.format(updateDate));
			}
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}

	}
	
}
