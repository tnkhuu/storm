/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.management;

/**
 * The Enum VMState.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public enum VMState
{

	/** The running. */
	RUNNING,

	/** The idle. */
	IDLE,

	/** The paused. */
	PAUSED,

	/** The dying. */
	DYING,

	/** The shutting down. */
	SHUTTING_DOWN,

	/** The shut off. */
	SHUT_OFF,

	/** The crashed. */
	CRASHED,

	/** The suspending. */
	SUSPENDING,

	/** The destroy. */
	DESTROY,

	/** The restart. */
	RESTART

}
