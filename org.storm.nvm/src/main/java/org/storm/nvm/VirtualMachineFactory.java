/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm;

import org.storm.nvm.management.FilesystemType;
import org.storm.nvm.management.VMState;
import org.storm.nvm.management.VirtualisationType;
import org.storm.nvm.model.Clock;
import org.storm.nvm.model.Console;
import org.storm.nvm.model.Devices;
import org.storm.nvm.model.Filesystem;
import org.storm.nvm.model.FilesystemSource;
import org.storm.nvm.model.FilesystemTarget;
import org.storm.nvm.model.Interface;
import org.storm.nvm.model.OperatingSystem;
import org.storm.nvm.model.VirtualMachine;

/**
 * A factory for creating VirtualMachine objects.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class VirtualMachineFactory
{

	/**
	 * Creates a new VirtualMachine object.
	 * 
	 * @param name
	 *            the name
	 * @param cpus
	 *            the cpus
	 * @param memory
	 *            the memory
	 * @return the virtual machine
	 */
	public VirtualMachine createLxc(String name, int cpus, int memory)
	{

		VirtualMachine virtualMachine = new VirtualMachine();
		virtualMachine
				.setType(VirtualisationType.LXC)
				.setVcpu(cpus)
				.setMemory(memory)
				.setName(name)
				.setPowerOff(VMState.DESTROY)
				.setReset(VMState.RESTART)
				.setClockOffset(new Clock().setOffset("utc"))
				.setCrash(VMState.DESTROY)
				.setOs(new OperatingSystem().setInit("/init").setType("exe"))
				.setDevices(
						new Devices().setEmulator(Environment.getLXCEmutator()).setConsoles(new Console().setType("pty")).setNetworkInterfaces(Interface.DEFAULT_NETWORK_INTERFACE));

		return virtualMachine;
	}
	
	public VirtualMachine createStormLxc(String name, String filesystemPath, int cpus, int memory)
	{

		VirtualMachine virtualMachine = new VirtualMachine();
		virtualMachine
				.setType(VirtualisationType.LXC)
				.setVcpu(cpus)
				.setMemory(memory)
				.setName(name)
				.setPowerOff(VMState.DESTROY)
				.setReset(VMState.RESTART)
				.setClockOffset(new Clock().setOffset("utc"))
				.setCrash(VMState.DESTROY)
				.setOs(new OperatingSystem().setInit("/sbin/init").setType("exe"))
				.setDevices(
						new Devices().setEmulator(Environment.getLXCEmutator())
						.setConsoles(new Console().setType("pty"))
						.setFileSystems(new Filesystem().setSource(new FilesystemSource().setDir(filesystemPath)).setTarget(new FilesystemTarget().setDir("/"))
								.setType(FilesystemType.mount)).
								setNetworkInterfaces(Interface.DEFAULT_NETWORK_INTERFACE));

		return virtualMachine;
	}

	/**
	 * Creates the.
	 * 
	 * @param type
	 *            the type
	 * @param name
	 *            the name
	 * @param cpus
	 *            the cpus
	 * @param memory
	 *            the memory
	 * @param os
	 *            the os
	 * @return the virtual machine
	 */
	public VirtualMachine create(VirtualisationType type, String name, int cpus, int memory, OperatingSystem os)
	{

		VirtualMachine virtualMachine = new VirtualMachine();
		virtualMachine
				.setType(type)
				.setVcpu(cpus)
				.setMemory(memory)
				.setName(name)
				.setPowerOff(VMState.DESTROY)
				.setReset(VMState.RESTART)
				.setClockOffset(new Clock().setOffset("utc"))
				.setCrash(VMState.DESTROY)
				.setOs(os)
				.setDevices(
						new Devices().setEmulator(Environment.getLXCEmutator()).setConsoles(new Console().setType("pty")).setNetworkInterfaces(Interface.DEFAULT_NETWORK_INTERFACE));

		return virtualMachine;
	}
}
