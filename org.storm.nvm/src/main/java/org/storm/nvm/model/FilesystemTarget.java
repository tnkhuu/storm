package org.storm.nvm.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("target")
public class FilesystemTarget
{

	/** The type. */
	@XStreamAlias("dir")
	@XStreamAsAttribute
	String dir;

	public String getDir()
	{
		return dir;
	}

	public FilesystemTarget setDir(String dir)
	{
		this.dir = dir;
		return this;
	}
	
	
}
