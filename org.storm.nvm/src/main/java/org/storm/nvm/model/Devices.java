/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class Devices.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@XStreamAlias("devices")
public class Devices
{

	/** The emulator. */
	@XStreamAlias("emulator")
	String emulator;

	/** The network interfaces. */
	@XStreamAlias("interface")
	Interface networkInterfaces;

	/** The file systems. */
	@XStreamAlias("filesystem")
	Filesystem fileSystems;

	/** The consoles. */
	@XStreamAlias("console")
	Console consoles;

	/**
	 * Gets the emulator.
	 * 
	 * @return the emulator
	 */
	public String getEmulator()
	{
		return emulator;
	}

	/**
	 * Sets the emulator.
	 * 
	 * @param emulator
	 *            the emulator
	 * @return the devices
	 */
	public Devices setEmulator(String emulator)
	{
		this.emulator = emulator;
		return this;
	}

	/**
	 * Gets the network interfaces.
	 * 
	 * @return the network interfaces
	 */
	public Interface getNetworkInterfaces()
	{
		return networkInterfaces;
	}

	/**
	 * Sets the network interfaces.
	 * 
	 * @param networkInterfaces
	 *            the network interfaces
	 * @return the devices
	 */
	public Devices setNetworkInterfaces(Interface networkInterfaces)
	{
		this.networkInterfaces = networkInterfaces;
		return this;
	}

	/**
	 * Gets the file systems.
	 * 
	 * @return the file systems
	 */
	public Filesystem getFileSystems()
	{
		return fileSystems;
	}

	/**
	 * Sets the file systems.
	 * 
	 * @param fileSystems
	 *            the file systems
	 * @return the devices
	 */
	public Devices setFileSystems(Filesystem fileSystems)
	{
		this.fileSystems = fileSystems;
		return this;
	}

	/**
	 * Gets the consoles.
	 * 
	 * @return the consoles
	 */
	public Console getConsoles()
	{
		return consoles;
	}

	/**
	 * Sets the consoles.
	 * 
	 * @param consoles
	 *            the consoles
	 * @return the devices
	 */
	public Devices setConsoles(Console consoles)
	{
		this.consoles = consoles;
		return this;
	}

}
