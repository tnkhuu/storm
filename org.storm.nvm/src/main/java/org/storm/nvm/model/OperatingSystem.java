/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class OperatingSystem.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@XStreamAlias("os")
public class OperatingSystem
{

	/** The shell. */
	public static OperatingSystem SHELL = new OperatingSystem().setInit("/bin/sh").setType("exe");

	/** The bash. */
	public static OperatingSystem BASH = new OperatingSystem().setInit("/bin/bash").setType("exe");

	/** The tomcat. */
	public static OperatingSystem TOMCAT = new OperatingSystem().setInit("/usr/share/tomcat7/bin/catalina.sh").setType("exe");

	/** The type. */
	@XStreamAlias("type")
	String type;

	/** The init. */
	@XStreamAlias("init")
	String init;

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the type
	 * @return the operating system
	 */
	public OperatingSystem setType(String type)
	{
		this.type = type;
		return this;
	}

	/**
	 * Gets the inits the.
	 * 
	 * @return the inits the
	 */
	public String getInit()
	{
		return init;
	}

	/**
	 * Sets the init.
	 * 
	 * @param init
	 *            the init
	 * @return the operating system
	 */
	public OperatingSystem setInit(String init)
	{
		this.init = init;
		return this;
	}
}
