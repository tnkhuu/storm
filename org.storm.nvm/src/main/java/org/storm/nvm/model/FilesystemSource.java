package org.storm.nvm.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("source")
public class FilesystemSource
{
	/** The type. */
	@XStreamAlias("dir")
	@XStreamAsAttribute
	String dir;
	

	public String getDir()
	{
		return dir;
	}

	public FilesystemSource setDir(String dir)
	{
		this.dir = dir;
		return this;
	}
	
	
}
