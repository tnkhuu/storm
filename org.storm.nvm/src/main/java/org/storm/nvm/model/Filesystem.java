/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import org.storm.nvm.management.FilesystemType;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * The Class Filesystem.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@XStreamAlias("filesystem")
public class Filesystem
{

	/** The type. */
	@XStreamAlias("type")
	@XStreamAsAttribute
	FilesystemType type;

	/** The source dir. */
	FilesystemSource source;
	FilesystemTarget target;
	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public FilesystemType getType()
	{
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the type
	 * @return the filesystem
	 */
	public Filesystem setType(FilesystemType type)
	{
		this.type = type;
		return this;
	}

	/**
	 * Gets the source dir.
	 * 
	 * @return the source dir
	 */
	public FilesystemSource getSource()
	{
		return source;
	}

	/**
	 * Sets the source dir.
	 * 
	 * @param sourceDir
	 *            the source dir
	 * @return the filesystem
	 */
	public Filesystem setSource(FilesystemSource sourceDir)
	{
		this.source = sourceDir;
		return this;
	}

	/**
	 * Gets the target dir.
	 * 
	 * @return the target dir
	 */
	public FilesystemTarget getTarget()
	{
		return target;
	}

	/**
	 * Sets the target dir.
	 * 
	 * @param targetDir
	 *            the target dir
	 * @return the filesystem
	 */
	public Filesystem setTarget(FilesystemTarget targetDir)
	{
		this.target = targetDir;
		return this;
	}


}
