/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model.serialization;

import org.storm.nvm.management.VMState;
import org.storm.nvm.management.VirtualisationType;

import com.thoughtworks.xstream.XStream;

/**
 * The Class SerializationServiceImpl.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SerializationServiceImpl
{
	public static final String UNDERSCORE = "_";
	public static final String DOUBLE_UNDERSCORE = "__";
	/** The xstream. */
	final XStream xstream;

	/**
	 * Instantiates a new serialization service impl.
	 */
	public SerializationServiceImpl()
	{
		xstream = new XStream();
		xstream.registerConverter(new EnumConverter(), XStream.PRIORITY_VERY_HIGH);
		xstream.registerConverter(new EnumSingleValueConverter(VirtualisationType.class), XStream.PRIORITY_VERY_HIGH);
		xstream.registerConverter(new EnumSingleValueConverter(VMState.class), XStream.PRIORITY_VERY_HIGH);
		xstream.autodetectAnnotations(true);
	}

	/**
	 * Gets the x stream.
	 * 
	 * @return the x stream
	 */
	public XStream getXStream()
	{
		return xstream;
	}

}
