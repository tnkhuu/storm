/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

/**
 * The Interface Hardware.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Hardware
{

	/** The CP ux1. */
	int CPUx1 = 1;

	/** The CP ux2. */
	int CPUx2 = 2;

	/** The CP ux4. */
	int CPUx4 = 4;

	/** The CP ux6. */
	int CPUx6 = 6;

	/** The CP ux8. */
	int CPUx8 = 8;

	/** The CP ux16. */
	int CPUx16 = 16;

	/** The CP ux32. */
	int CPUx32 = 32;

	/** The MEMOR y_16 mb. */
	int MEMORY_16MB = 0x4000;

	/** The MEMOR y_32 mb. */
	int MEMORY_32MB = 0x8000;

	/** The MEMOR y_64 mb. */
	int MEMORY_64MB = 0x10000;

	/** The MEMOR y_128 mb. */
	int MEMORY_128MB = 0x20000;

	/** The MEMOR y_256 mb. */
	int MEMORY_256MB = 0x40000;

	/** The MEMOR y_512 mb. */
	int MEMORY_512MB = 0x80000;

	/** The MEMOR y_1 gb. */
	int MEMORY_1GB = 0xA00000;
}
