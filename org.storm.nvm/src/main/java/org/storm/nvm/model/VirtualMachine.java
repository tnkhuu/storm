/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import org.storm.nvm.management.VMState;
import org.storm.nvm.management.VirtualisationType;
import org.storm.nvm.model.serialization.SerializationServiceImpl;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * The Class VirtualMachine.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@XStreamAlias("domain")
public class VirtualMachine
{

	/** The type. */
	@XStreamAlias("type")
	@XStreamAsAttribute
	VirtualisationType type;

	/** The name. */
	@XStreamAlias("name")
	String name;

	@XStreamAlias("on_poweroff")
	VMState on_poweroff;

	@XStreamAlias("on_reboot")
	VMState on_reboot;

	@XStreamAlias("on_crash")
	VMState on_crash;

	/** The memory. */
	@XStreamAlias("memory")
	int memory;

	/** The vcpu. */
	@XStreamAlias("vcpu")
	int vcpu;

	/** The devices. */
	Devices devices;

	/** The os. */
	OperatingSystem os;

	/** The clock offset. */
	@XStreamAlias("clock")
	Clock clockOffset;

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public VirtualisationType getType()
	{
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the type
	 * @return the virtual machine
	 */
	public VirtualMachine setType(VirtualisationType type)
	{
		this.type = type;
		return this;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name
	 * @return the virtual machine
	 */
	public VirtualMachine setName(String name)
	{
		this.name = name;
		return this;
	}

	/**
	 * Gets the clock offset.
	 * 
	 * @return the clock offset
	 */
	public Clock getClockOffset()
	{
		return clockOffset;
	}

	/**
	 * Sets the clock offset.
	 * 
	 * @param clockOffset
	 *            the clock offset
	 * @return the virtual machine
	 */
	public VirtualMachine setClockOffset(Clock clockOffset)
	{
		this.clockOffset = clockOffset;
		return this;
	}

	/**
	 * Gets the power off.
	 * 
	 * @return the power off
	 */
	public VMState getPowerOff()
	{
		return on_poweroff;
	}

	/**
	 * Sets the power off.
	 * 
	 * @param powerOff
	 *            the power off
	 * @return the virtual machine
	 */
	public VirtualMachine setPowerOff(VMState powerOff)
	{
		this.on_poweroff = powerOff;
		return this;
	}

	/**
	 * Gets the reset.
	 * 
	 * @return the reset
	 */
	public VMState getReset()
	{
		return on_reboot;
	}

	/**
	 * Sets the reset.
	 * 
	 * @param reset
	 *            the reset
	 * @return the virtual machine
	 */
	public VirtualMachine setReset(VMState reset)
	{
		this.on_reboot = reset;
		return this;
	}

	/**
	 * Gets the crash.
	 * 
	 * @return the crash
	 */
	public VMState getCrash()
	{
		return on_crash;
	}

	/**
	 * Sets the crash.
	 * 
	 * @param crash
	 *            the crash
	 * @return the virtual machine
	 */
	public VirtualMachine setCrash(VMState crash)
	{
		this.on_crash = crash;
		return this;
	}

	/**
	 * Gets the memory.
	 * 
	 * @return the memory
	 */
	public int getMemory()
	{
		return memory;
	}

	/**
	 * Sets the memory.
	 * 
	 * @param memory
	 *            the memory
	 * @return the virtual machine
	 */
	public VirtualMachine setMemory(int memory)
	{
		this.memory = memory;
		return this;
	}

	/**
	 * Gets the vcpu.
	 * 
	 * @return the vcpu
	 */
	public int getVcpu()
	{
		return vcpu;
	}

	/**
	 * Sets the vcpu.
	 * 
	 * @param vcpu
	 *            the vcpu
	 * @return the virtual machine
	 */
	public VirtualMachine setVcpu(int vcpu)
	{
		this.vcpu = vcpu;
		return this;
	}

	/**
	 * Gets the devices.
	 * 
	 * @return the devices
	 */
	public Devices getDevices()
	{
		return devices;
	}

	/**
	 * Sets the devices.
	 * 
	 * @param devices
	 *            the devices
	 * @return the virtual machine
	 */
	public VirtualMachine setDevices(Devices devices)
	{
		this.devices = devices;
		return this;
	}

	/**
	 * Gets the os.
	 * 
	 * @return the os
	 */
	public OperatingSystem getOs()
	{
		return os;
	}

	/**
	 * Sets the os.
	 * 
	 * @param os
	 *            the os
	 * @return the virtual machine
	 */
	public VirtualMachine setOs(OperatingSystem os)
	{
		this.os = os;
		return this;
	}

	/**
	 * To xml.
	 * 
	 * @return the string
	 */
	public String toXML()
	{

		SerializationServiceImpl serv = new SerializationServiceImpl();
		// nasty hack due to xstream bug.
		return serv.getXStream().toXML(this).replaceAll(SerializationServiceImpl.DOUBLE_UNDERSCORE, SerializationServiceImpl.UNDERSCORE);
	}
}
