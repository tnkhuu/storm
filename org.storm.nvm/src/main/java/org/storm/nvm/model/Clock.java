/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * The Class Clock.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@XStreamAlias("clock")
public class Clock
{

	/** The offset. */
	@XStreamAlias("offset")
	@XStreamAsAttribute
	String offset;

	/**
	 * Gets the offset.
	 * 
	 * @return the offset
	 */
	public String getOffset()
	{
		return offset;
	}

	/**
	 * Sets the offset.
	 * 
	 * @param offset
	 *            the offset
	 * @return the clock
	 */
	public Clock setOffset(String offset)
	{
		this.offset = offset;
		return this;
	}

}
