/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model.serialization;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

/**
 * A single value converter for arbitrary enums. Converter is internally
 * automatically instantiated for enum types.
 * 
 */
public class EnumSingleValueConverter extends AbstractSingleValueConverter
{

	/** The enum type. */
	@SuppressWarnings("rawtypes")
	private final Class<? extends Enum> enumType;

	/**
	 * Instantiates a new enum single value converter.
	 * 
	 * @param type
	 *            the type
	 */
	@SuppressWarnings("rawtypes")
	public EnumSingleValueConverter(Class<? extends Enum> type)
	{
		if (!Enum.class.isAssignableFrom(type) && (type != Enum.class))
		{
			throw new IllegalArgumentException("Converter can only handle defined enums");
		}
		enumType = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter
	 * #canConvert(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class type)
	{
		return enumType.isAssignableFrom(type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter
	 * #toString(java.lang.Object)
	 */
	@Override
	public String toString(Object obj)
	{
		return Enum.class.cast(obj).name().toLowerCase();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter
	 * #fromString(java.lang.String)
	 */
	@Override
	public Object fromString(String str)
	{
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Enum result = Enum.valueOf(enumType, str);
		return result;
	}
}
