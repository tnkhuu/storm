package org.storm.nvm.command;

import java.util.List;

import org.apache.felix.service.command.Descriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.nvm.api.VirtualMachineService;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

@Component(properties = { "osgi.command.scope=virt", "osgi.command.function=list" }, provide = Object.class)
public class ListVirtualInstances
{
	private static Logger s_log = LoggerFactory.getLogger(ListVirtualInstances.class);
	private VirtualMachineService vmservice;

	@Reference
	public void setVirtualMachineService(VirtualMachineService vmservice)
	{
		this.vmservice = vmservice;
	}

	@Descriptor("list lxc instances")
	public void list()
	{

		List<String> instances = vmservice.listInstances();
		for (String d : instances)
		{
			s_log.info(d);
		}

	}
}
