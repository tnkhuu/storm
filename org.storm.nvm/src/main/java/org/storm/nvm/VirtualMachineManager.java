/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm;

import java.util.ArrayList;
import java.util.List;

import org.storm.libvirt.Connect;
import org.storm.libvirt.Domain;
import org.storm.libvirt.LibvirtException;
import org.storm.libvirt.Network;
import org.storm.nvm.model.VirtualMachine;
import org.storm.nvm.model.VirtualNetwork;

/**
 * Handles querying the libvirt engine and managing the connection to the native
 * protocol.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class VirtualMachineManager
{

	/**
	 * Define a new vm type.
	 * 
	 * @param domain
	 *            the domain
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain define(VirtualMachine domain) throws LibvirtException
	{

		Connect connect = new Connect(domain.getType().uri());
		return connect.domainDefineXML(domain.toXML());
	}

	/**
	 * Start a vm on this node.
	 * 
	 * @param domain
	 *            the domain
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain start(VirtualMachine domain) throws LibvirtException
	{
		Connect connect = new Connect(domain.getType().uri());
		Domain vm = connect.domainCreateXML(domain.toXML(), 0);
		return vm;
	}

	/**
	 * Suspend the specified vm on this node.
	 * 
	 * @param uri
	 *            the uri
	 * @param domainName
	 *            the domain name
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain suspend(String uri, String domainName) throws LibvirtException
	{
		Connect connect = new Connect(uri);
		Domain vm = connect.domainLookupByName(domainName);
		vm.suspend();
		return vm;
	}

	/**
	 * Create a new virtual network on this node.
	 * 
	 * @param uri
	 *            the uri
	 * @param virtualNetwork
	 *            the virtual network
	 * @return the network
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Network createNetwork(String uri, VirtualNetwork virtualNetwork) throws LibvirtException
	{
		Connect connect = new Connect(uri);
		Network network = connect.networkCreateXML(virtualNetwork.toXML());
		return network;
	}

	/**
	 * Destroy a vm instance.
	 * 
	 * @param uri
	 *            the uri
	 * @param domainName
	 *            the domain name
	 * @return true, if successful
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public boolean destroy(String uri, String domainName) throws LibvirtException
	{
		Connect connect = new Connect(uri);
		Domain dom = connect.domainLookupByName(domainName);
		dom.destroy();
		return true;
	}

	/**
	 * Lists all active vms.
	 * 
	 * @param uri
	 *            the uri
	 * @return the list
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public List<Domain> list(String uri) throws LibvirtException
	{
		List<Domain> domains = new ArrayList<Domain>();
		Connect connect = new Connect(uri);
		int[] domainIDs = connect.listDomains();
		for (int id : domainIDs)
		{
			domains.add(connect.domainLookupByID(id));
		}
		return domains;
	}

	/**
	 * Gets the hypervisor type.
	 * 
	 * @param uri
	 *            the uri
	 * @return the hypervisor type
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getHypervisorType(String uri) throws LibvirtException
	{
		Connect connect = new Connect(uri);
		return connect.getType();
	}

	/**
	 * Gets the hypervisor version.
	 * 
	 * @param uri
	 *            the uri
	 * @return the hypervisor version
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getHypervisorVersion(String uri) throws LibvirtException
	{
		Connect connect = new Connect(uri);
		return connect.getVersion();
	}

	/**
	 * Gets the libvirt version.
	 * 
	 * @param uri
	 *            the uri
	 * @return the libvirt version
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getLibvirtVersion(String uri) throws LibvirtException
	{
		Connect connect = new Connect(uri);
		return connect.getLibVirVersion();
	}

	/**
	 * Lists all defined vms.
	 * 
	 * @param uri
	 *            the uri
	 * @return the list
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public List<Domain> listDefinedDomains(String uri) throws LibvirtException
	{
		List<Domain> domains = new ArrayList<Domain>();
		Connect connect = new Connect(uri);
		String[] domainIDs = connect.listDefinedDomains();
		for (String id : domainIDs)
		{
			domains.add(connect.domainLookupByName(id));
		}
		return domains;
	}
}
