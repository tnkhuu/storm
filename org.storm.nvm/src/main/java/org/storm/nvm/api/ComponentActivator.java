/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.api;

import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.factories.FilterFactory;
import org.storm.api.services.DistributedService;
import org.storm.api.services.LogicalVolumeService;
import org.storm.nvm.VirtualMachineServiceImpl;

/**
 * The Nvm.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator implements BundleActivator
{

	/** The logger. */
	private static Logger					logger	= LoggerFactory.getLogger(ComponentActivator.class);
	ServiceReference<LogicalVolumeService>	ls;
	LogicalVolumeService					lvservice;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(final BundleContext context) throws Exception
	{
		context.registerService(new String[] { VirtualMachineService.class.getName(), DistributedService.class.getName() }, new VirtualMachineServiceImpl(),
				new Hashtable<String, String>());
		ls = context.getServiceReference(LogicalVolumeService.class);
		if (ls != null)
		{
			lvservice = context.getService(ls);
		}
		else
		{
			context.addServiceListener(new ServiceListener()
			{
				@Override
				public void serviceChanged(ServiceEvent event)
				{

					switch (event.getType())
					{
					case ServiceEvent.REGISTERED:
					{
						@SuppressWarnings("unchecked")
						ServiceReference<LogicalVolumeService> lsr = (ServiceReference<LogicalVolumeService>) event.getServiceReference();
						lvservice = context.getService(lsr);
					}
					}

				}
			}, FilterFactory.createServiceFilterString(LogicalVolumeService.class));
		}
		logger.info("Started The NVM Virtualization Provider.");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception
	{
		logger.info("Stopping NVM Virtualization Provider ...");

	}

}
