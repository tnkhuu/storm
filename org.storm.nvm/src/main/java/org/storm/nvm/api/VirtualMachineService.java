/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.api;

import java.util.List;

import org.storm.api.services.DistributedService;

/**
 * The Interface VirtualMachineService.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface VirtualMachineService extends DistributedService
{

	/**
	 * List all running instances on this node.
	 * 
	 * @return a list of all running instances.
	 */
	public List<String> listInstances();

	/**
	 * Determines if the current platform has been correctly configured and
	 * wired into the OS's hyper visor system.
	 * 
	 * @return true if the system is properly configured.
	 */
	public boolean isHyperVisorEnabled();
}
