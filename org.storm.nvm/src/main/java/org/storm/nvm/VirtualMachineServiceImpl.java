/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.libvirt.Domain;
import org.storm.libvirt.LibvirtException;
import org.storm.nvm.api.VirtualMachineService;
import org.storm.nvm.management.VirtualisationType;

import aQute.bnd.annotation.component.Component;

@Component
public class VirtualMachineServiceImpl implements VirtualMachineService
{
	private static final String LXC_HYPERVISOR_TYPE = "LXC";
	private static final long LIBVIRT_VERSION = 9008;
	private static final long HYPERVISOR_VERSION = 3002000;
	private static Logger s_log = LoggerFactory.getLogger(VirtualMachineServiceImpl.class);
	private VirtualMachineManager vmm = new VirtualMachineManager();

	/**
	 * List all running instances on this node.
	 * 
	 * @return a list of all running instances.
	 */
	@Override
	public List<String> listInstances()
	{
		List<String> results = new ArrayList<>();
		try
		{
			List<Domain> domains = vmm.list(VirtualisationType.LXC.uri());
			for (Domain d : domains)
			{
				results.add(d.getName());
			}
		}
		catch (LibvirtException e)
		{
			s_log.error(e.getMessage(), e);
		}
		return results;
	}

	/**
	 * Determines if the current platform has been correctly configured and
	 * wired into the OS's hyper visor system.
	 * 
	 * @return true if the system is properly configured.
	 */
	@Override
	public boolean isHyperVisorEnabled()
	{
		boolean enabled = true;
		try
		{
			String hyperVisorType = vmm.getHypervisorType(VirtualisationType.LXC.uri());
			long hyperVisorVersion = vmm.getHypervisorVersion(VirtualisationType.LXC.uri());
			long libvirtVersion = vmm.getLibvirtVersion(VirtualisationType.LXC.uri());
			enabled = LXC_HYPERVISOR_TYPE.equals(hyperVisorType) && (HYPERVISOR_VERSION == hyperVisorVersion) && (LIBVIRT_VERSION == libvirtVersion);

		}
		catch (Exception e)
		{
			enabled = false;
		}
		return enabled;
	}
}
