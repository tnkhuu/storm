/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.storm.libvirt.Domain;
import org.storm.libvirt.LibvirtException;
import org.storm.nvm.VirtualMachineFactory;
import org.storm.nvm.VirtualMachineManager;
import org.storm.nvm.management.VirtualisationType;

/**
 * Utility and setup methods for integrated tests involving the hyper visor.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class AbstractVMTest
{
	protected VirtualMachineManager vmm;
	protected VirtualMachineFactory vmFactory;

	@Before
	public void cleanup() throws LibvirtException
	{
		destroyAllInstances();
		vmFactory = new VirtualMachineFactory();
		vmm = new VirtualMachineManager();
	}

	@After
	public void after() throws LibvirtException
	{
		destroyAllInstances();
	}

	protected void destroyAllInstances() throws LibvirtException
	{
		vmm = new VirtualMachineManager();
		List<Domain> doms = vmm.listDefinedDomains(VirtualisationType.LXC.uri());
		for (Domain d : doms)
		{
			d.undefine();
		}
		doms = vmm.list(VirtualisationType.LXC.uri());

		for (Domain d : doms)
		{
			d.destroy();
		}
	}
}
