/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import static org.storm.nvm.model.Hardware.CPUx1;
import static org.storm.nvm.model.Hardware.CPUx2;
import static org.storm.nvm.model.Hardware.MEMORY_16MB;
import static org.storm.nvm.model.Hardware.MEMORY_64MB;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.storm.libvirt.Domain;
import org.storm.libvirt.LibvirtException;
import org.storm.nvm.VirtualMachineFactory;
import org.storm.nvm.VirtualMachineManager;
import org.storm.nvm.management.VirtualisationType;

/**
 * Tests for the {@link VirtualMachineManager}
 * 
 * @author Trung Khuu
 * @since 1.0
 * @note: This test will only run on linux environments properly setup with
 *        libvirt.
 * 
 */
public class VirtualMachineManagerTest
{
	private VirtualMachineManager vmm;
	private VirtualMachineFactory vmFactory;

	@Before
	public void before()
	{
		vmFactory = new VirtualMachineFactory();
		vmm = new VirtualMachineManager();
	}

	@Test
	public void testHypervisorChecks() throws LibvirtException
	{

		Assert.assertNotNull(vmm.getHypervisorType(VirtualisationType.LXC.uri()));
	}

	/**
	 * Test define simple lxc.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	@Test
	public void testDefineSimpleLxc() throws LibvirtException
	{
		Domain vm = vmm.define(vmFactory.createLxc("vm01", 1, 16000));
		System.out.println(vm.getName());
		System.out.println(vm.getMaxMemory());
		System.out.println(vm.getUUIDString());
		System.out.println(vm.getOSType());
		System.out.println(vm.getAutostart());
		System.out.println(vm.getInfo());
	}

	@Test
	public void testDefineLxc() throws LibvirtException
	{
		VirtualMachine vm = vmFactory.create(VirtualisationType.LXC, "vm02", CPUx1, MEMORY_16MB, OperatingSystem.SHELL);
		Domain defined = vmm.define(vm);
		Assert.assertNotNull(defined);
		System.out.println(defined.getName());
		System.out.println(defined.getMaxMemory());
		System.out.println(defined.getUUIDString());
		System.out.println(defined.getOSType());
		System.out.println(defined.getAutostart());
		System.out.println(defined.getInfo());
	}

	@Test
	public void testStartLxcShell() throws LibvirtException
	{
		VirtualMachine vm = vmFactory.create(VirtualisationType.LXC, "vm03", CPUx2, MEMORY_64MB, OperatingSystem.SHELL);
		Domain defined = vmm.start(vm);
		Assert.assertNotNull(defined);
		System.out.println(defined.getName());
		System.out.println(defined.getMaxMemory());
		System.out.println(defined.getUUIDString());
		System.out.println(defined.getOSType());
		System.out.println(defined.getAutostart());
		System.out.println(defined.getInfo());

	}

	@Test
	public void testStartLxcTomcat() throws LibvirtException
	{
		VirtualMachine vm = vmFactory.create(VirtualisationType.LXC, "vm04", CPUx1, MEMORY_16MB, OperatingSystem.TOMCAT);
		Domain defined = vmm.start(vm);
		Assert.assertNotNull(defined);
		System.out.println(defined.getName());
		System.out.println(defined.getMaxMemory());
		System.out.println(defined.getUUIDString());
		System.out.println(defined.getOSType());
		System.out.println(defined.getAutostart());
		System.out.println(defined.getInfo());

		// sudo route add -net 192.168.122.0 netmask 255.255.255.0 gw
		// 192.168.122.1

	}

	@Before
	public void cleanup() throws LibvirtException
	{
		List<Domain> doms = vmm.listDefinedDomains(VirtualisationType.LXC.uri());
		for (Domain d : doms)
		{
			d.undefine();
		}

		doms = vmm.list(VirtualisationType.LXC.uri());
		for (Domain d : doms)
		{
			d.destroy();
		}
	}

}
