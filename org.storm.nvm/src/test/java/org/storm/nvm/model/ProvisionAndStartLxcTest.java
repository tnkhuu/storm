/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import static org.storm.nvm.model.Hardware.CPUx1;
import static org.storm.nvm.model.Hardware.MEMORY_16MB;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.storm.libvirt.Domain;
import org.storm.libvirt.LibvirtException;
import org.storm.nvm.management.VirtualisationType;

/**
 * An integrated CRUD Tests to create and destroy virtualised LXC instances
 * 
 * @author Trung Khuu
 * @since 1.0
 * @note: This test will only run on linux environments properly setup with
 *        libvirt.
 */
public class ProvisionAndStartLxcTest extends AbstractVMTest
{
	private int INSTANCES = 10;

	@Test
	public void testProvisionAndStartLxcs() throws LibvirtException
	{
		List<Domain> startedVms = new ArrayList<Domain>();
		for (int i = 0; i < INSTANCES; i++)
		{
			String vmName = "vm-" + i;
			VirtualMachine vm = vmFactory.create(VirtualisationType.LXC, vmName, CPUx1, MEMORY_16MB, OperatingSystem.SHELL);
			startedVms.add(vmm.start(vm));
		}
		Assert.assertEquals(INSTANCES, startedVms.size());
		for (Domain vm : startedVms)
		{
			System.out.println("NAME: " + vm.getName() + " | UUID:" + vm.getUUIDString() + " | VCPU:" + vm.getInfo().nrVirtCpu + " | MAX MEMORY:" + vm.getMaxMemory() + "(KB)"
					+ " | USED MEMORY: " + vm.getInfo().memory + "(KB)" + " | CPU TIME:" + vm.getInfo().cpuTime);

		}
	}

}
