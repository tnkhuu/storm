/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import static org.storm.nvm.model.Hardware.CPUx1;
import static org.storm.nvm.model.Hardware.MEMORY_16MB;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.storm.nvm.VirtualMachineFactory;

/**
 * Tests the {@link VirtualMachineFactory} for its ability to generate valid vm
 * specifications.
 * 
 * @author Trung Khuu
 * @since 1.0
 * @note: This test will only run on linux environments properly setup with
 *        libvirt.
 */
public class VirtualMachineSpecificationBuilderTest
{
	private VirtualMachineFactory vmFactory;
	/** The vm name. */
	String VMNAME = "test01";

	@Before
	public void before()
	{
		vmFactory = new VirtualMachineFactory();
	}

	/**
	 * Test a simple lxc vm specification.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testSimpleLxcSpecification() throws IOException
	{
		Assert.assertTrue(isEqual(vmFactory.createLxc(VMNAME, CPUx1, MEMORY_16MB), "lxc_spec.xml"));
	}

	public boolean isEqual(VirtualMachine vm, String resource) throws IOException
	{
		InputStream in = getClass().getClassLoader().getResourceAsStream(resource);

		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(reader);
		StringBuilder builder = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null)
		{
			builder.append(line).append("\n");
		}
		return normalize(vm.toXML()).equals(normalize(builder.toString()));
	}

	private String normalize(String target)
	{
		return target.replaceAll("\n", "").replaceAll("\r", "").trim();
	}
}
