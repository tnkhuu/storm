/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import junit.framework.Assert;

import org.junit.Test;
import org.storm.tools.system.SystemUtils;

/**
 * Tests for the {@link SystemUtils}
 * 
 * @author Trung Khuu
 * @since 1.0
 * @note: This test will only run on linux environments properly setup with
 *        libvirt.
 */
public class SystemUtilsTest
{

	@Test
	public void testLxcEmulatorVerification()
	{
		Assert.assertTrue(SystemUtils.getLxcEmutalorLocation() != null);
		Assert.assertTrue(SystemUtils.getLxcEmutalorLocation().contains("libvirt_lxc"));
	}
}
