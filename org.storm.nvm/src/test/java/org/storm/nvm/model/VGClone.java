package org.storm.nvm.model;

import org.junit.Test;
import org.storm.libvirt.Domain;
import org.storm.libvirt.LibvirtException;
import org.storm.nvm.VirtualMachineFactory;
import org.storm.nvm.VirtualMachineManager;

import static org.storm.nvm.model.Hardware.CPUx2;
import static org.storm.nvm.model.Hardware.MEMORY_512MB;
public class VGClone
{

	
	@Test
	public void testLogicalVMIntegration() throws LibvirtException {
//		LogicalVolumeService serv = new LogicalVolumnServiceImpl();
		VirtualMachineFactory factory = new VirtualMachineFactory();
		VirtualMachineManager vmm = new VirtualMachineManager();

	//String fs =	serv.createSnapshotVolumn("storm", 1000, "lvm", "lxc");
	VirtualMachine vm = factory.createStormLxc("storm", "/mnt/storm/lxc/stormlvm", CPUx2, MEMORY_512MB);
	System.out.println(vm.toXML());
	
	
	Domain d = vmm.start(vm);
	System.out.println("NAME: " + d.getName() + " | UUID:" + d.getUUIDString() + " | VCPU:" + d.getInfo().nrVirtCpu + " | MAX MEMORY:" + d.getMaxMemory() + "(KB)"
			+ " | USED MEMORY: " + d.getInfo().memory + "(KB)" + " | CPU TIME:" + d.getInfo().cpuTime);
	}
	
	//lxc-create -n stormlvm -t ubuntu-storm -B lvm --lvname stormlvm --vgname lxc --fstype ext4 --fssize 1024M -- -T /home/tkhuu/.storm/dist/storm-cloud-1.0.0-lxc-rootfs.tar.gz --auth-key /home/tkhuu/.ssh/id_rsa.pub --role SERVER
}
