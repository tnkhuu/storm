/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nvm.model;

import static org.storm.nvm.model.Hardware.CPUx1;
import static org.storm.nvm.model.Hardware.MEMORY_16MB;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.storm.libvirt.Domain;
import org.storm.libvirt.LibvirtException;
import org.storm.nvm.VirtualMachineFactory;
import org.storm.nvm.VirtualMachineManager;
import org.storm.nvm.VirtualMachineServiceImpl;
import org.storm.nvm.api.VirtualMachineService;
import org.storm.nvm.management.VirtualisationType;

/**
 * Tests for the {@link VirtualMachineServiceImpl}
 * 
 * @author Trung Khuu
 * @since 1.0
 * @note: This test will only run on linux environments properly setup with
 *        libvirt.
 */
public class VirtualMachineServiceImplTest
{
	private VirtualMachineService service = null;
	private VirtualMachineManager vmm;
	private VirtualMachineFactory vmFactory;

	@Before
	public void before()
	{
		service = new VirtualMachineServiceImpl();
		vmm = new VirtualMachineManager();
		vmFactory = new VirtualMachineFactory();
	}

	@Test
	public void testIsHyperVisorEnabled()
	{
		Assert.assertTrue(service.isHyperVisorEnabled());
	}

	@Test
	public void testListInstances() throws LibvirtException
	{
		VirtualMachine vm = vmFactory.create(VirtualisationType.LXC, "vm01", CPUx1, MEMORY_16MB, OperatingSystem.SHELL);
		Domain startedVm = vmm.start(vm);
		Assert.assertNotNull(startedVm);
		List<Domain> instances = vmm.list(VirtualisationType.LXC.uri());
		Assert.assertEquals(1, instances.size());
	}
}
