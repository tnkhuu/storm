/*
 * Copyright (c) 2006-2009 Hyperic, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.heuristics.sigar;

import java.io.File;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.hyperic.sigar.OperatingSystem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.SigarLoader;
import org.hyperic.sigar.win32.LocaleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Display Sigar, java and system version information.
 */
public class Version extends SigarCommandBase {

    public Version(Shell shell) {
        super(shell);
    }
    
    
    private static Logger s_log = LoggerFactory.getLogger(Version.class);
    public Version() {
        super();
    }

    public String getUsageShort() {
        return "Display sigar and system version info";
    }

    private static String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            return "unknown";
        }
    }

    private static void printNativeInfo(PrintStream os) {
    	
    	
        String version =
            "java=" + Sigar.VERSION_STRING +
            ", native=" + Sigar.NATIVE_VERSION_STRING;
        String build =
            "java=" + Sigar.BUILD_DATE +
            ", native=" + Sigar.NATIVE_BUILD_DATE;
        String scm =
            "java=" + Sigar.SCM_REVISION +
            ", native=" + Sigar.NATIVE_SCM_REVISION;
        String archlib =
            SigarLoader.getNativeLibraryName();

        s_log.info("Sigar version......." + version);
        s_log.info("Build date.........." + build);
        s_log.info("SCM rev............." + scm);
        String host = getHostName();
        String fqdn;
        Sigar sigar = new Sigar(); 
        try {
            File lib = sigar.getNativeLibrary();
            if (lib != null) {
                archlib = lib.getName();
            }
            fqdn = sigar.getFQDN();
        } catch (SigarException e) {
            fqdn = "unknown";
        } finally {
            sigar.close();
        }

        s_log.info("Archlib............." + archlib);

        s_log.info("Current fqdn........" + fqdn);
        if (!fqdn.equals(host)) {
        	s_log.info("Hostname............" + host);
        }        

        if (SigarLoader.IS_WIN32) {
            LocaleInfo info = new LocaleInfo();
            s_log.info("Language............" + info);
            s_log.info("Perflib lang id....." +
                       info.getPerflibLangId());
        }
    }
    
    public static void printInfo(PrintStream os) {
        try {
            printNativeInfo(os);
        } catch (UnsatisfiedLinkError e) {
            s_log.info("*******ERROR******* " + e);
        }

        s_log.info("Current user........" +
                   System.getProperty("user.name"));
        s_log.info("");
        
        OperatingSystem sys = OperatingSystem.getInstance();
        s_log.info("OS description......" + sys.getDescription());
        s_log.info("OS name............." + sys.getName());
        s_log.info("OS arch............." + sys.getArch());
        s_log.info("OS machine.........." + sys.getMachine());
        s_log.info("OS version.........." + sys.getVersion());
        s_log.info("OS patch level......" + sys.getPatchLevel());
        s_log.info("OS vendor..........." + sys.getVendor());
        s_log.info("OS vendor version..." + sys.getVendorVersion());
        if (sys.getVendorCodeName() != null) {
            s_log.info("OS code name........" + sys.getVendorCodeName());
        }
        s_log.info("OS data model......." + sys.getDataModel());
        s_log.info("OS cpu endian......." + sys.getCpuEndian());

        s_log.info("Java vm version....." + 
                   System.getProperty("java.vm.version"));
        s_log.info("Java vm vendor......" + 
                System.getProperty("java.vm.vendor"));
        s_log.info("Java home..........." +
                System.getProperty("java.home"));
    }

    public void output(String[] args) {
        printInfo(this.out);
    }

    public static void main(String[] args) throws Exception {
        new Version().processCommand(args);
    }

}
