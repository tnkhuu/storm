/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.heuristics;

import java.net.UnknownHostException;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.dns.DNSAsyncClient;
import org.storm.api.dns.MessageCallback;
import org.storm.api.factories.FilterFactory;
import org.storm.api.kernel.Actor;
import org.storm.api.services.ActivityService;
import org.storm.dns.client.DNSPacket;
import org.xbill.java.dns.unsafe.protocol.Section;
import org.xbill.java.dns.unsafe.record.Record;

/**
 * Activity Monitor
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({"rawtypes",  "unchecked" })
public class ActivityServiceImpl extends Actor implements ActivityService
{

	private static final Logger	s_log	= LoggerFactory.getLogger(ActivityServiceImpl.class);
	private String				name;
	private volatile long		lastActive;
	private DNSAsyncClient		client;

	/**
	 * Instantiates a new Activity Service.
	 * 
	 * @param name
	 *            the name
	 */
	public ActivityServiceImpl(final BundleContext context)
	{
		this.lastActive = System.currentTimeMillis();
		ServiceReference<DNSAsyncClient> dsr = context.getServiceReference(DNSAsyncClient.class);
		if (dsr == null)
		{
			try
			{
				context.addServiceListener(new ServiceListener()
				{

					@Override
					public void serviceChanged(ServiceEvent event)
					{
						ServiceReference<DNSAsyncClient> dsr = (ServiceReference<DNSAsyncClient>) event.getServiceReference();
						client = context.getService(dsr);

					}
				}, FilterFactory.createServiceFilterString(DNSAsyncClient.class));
			}
			catch (InvalidSyntaxException e)
			{
				s_log.error(e.getMessage(), e);
			}
		}
		else
		{
			client = context.getService(dsr);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ActivityService#getInactivetyPeriod()
	 */
	@Override
	public long getInactivetyPeriod()
	{
		return System.currentTimeMillis() - lastActive;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ActivityService#setLastActive(long)
	 */
	public void setLastActive(long lastActive)
	{
		this.lastActive = lastActive;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ActivityService#setLastActive()
	 */
	public void setLastActive()
	{
		this.lastActive = System.currentTimeMillis();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ActivityService#getApplicationName()
	 */
	@Override
	public String getApplicationName()
	{
		if (name == null)
		{
			if (client != null)
			{
				try
				{
					client.reverseLookup(getOutboundAddress(), new MessageCallback<DNSPacket>()
					{

						@Override
						public void onComplete(DNSPacket packet)
						{

							Record[] result = packet.getDecodedMessage().getSectionArray(Section.ANSWER);
							if (result != null && result.length > 0)
							{
								ActivityServiceImpl.this.name = result[0].name.toString();
							}
						}

						@Override
						public void onFail(Throwable t)
						{
							s_log.error(t.getMessage(), t);

						}
					});
				}
				catch (UnknownHostException e)
				{
					s_log.error(e.getMessage(), e);
				}
			}
		}
		return name;
	}

}
