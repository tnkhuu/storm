/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.heuristics;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.fusesource.hawtdispatch.DispatchQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.CloudHealthService;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.Container;
import org.storm.api.services.vm.Memory;
import org.storm.api.services.vm.Metric;
import org.storm.api.services.vm.Rate;

/**
 * A query-able monitoring service that observes and reports on the health of a
 * specific node.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CloudHealthServiceImpl implements CloudHealthService
{
	@SuppressWarnings("unused")
	private volatile DispatchQueue	dispatcher;
	private static final Logger	s_log	= LoggerFactory.getLogger(CloudHealthServiceImpl.class);
	private volatile boolean	running	= true;

	/**
	 * Default constructor.
	 * 
	 * @param dispatcher
	 */
	public CloudHealthServiceImpl()
	{

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.CloudHealthService#getActiveContainers()
	 */
	@Override
	public List<Container> getActiveContainers()
	{
		List<Container> res = Collections.emptyList();
		VirtualizationProvider provider = ComponentActivator.getVirtualizationProvider();
		if (provider != null)
		{
			try
			{
				return provider.listContainers();
			}
			catch (IOException e)
			{
				s_log.error(e.getMessage(), e);
			}
		}
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.CloudHealthService#getAvailableDiskspace()
	 */
	@Override
	public Metric getAvailableDiskspace()
	{
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.CloudHealthService#getSystemLoad()
	 */
	@Override
	public int getSystemLoad()
	{
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.CloudHealthService#getUsedMemory()
	 */
	@Override
	public Memory getUsedMemory()
	{
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.CloudHealthService#getDiskIOPS()
	 */
	@Override
	public Rate getDiskIOPS()
	{
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.CloudHealthService#getNetworkTraffic()
	 */
	@Override
	public Rate getNetworkTraffic()
	{
		return null;
	}

	@Override
	public void run()
	{
		while (running)
		{
			
		}

	}

}
