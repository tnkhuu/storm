/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.heuristics.topology;

import java.util.HashMap;
import java.util.Map;

import org.storm.api.services.vm.Metric;
import org.storm.nexus.api.Role;

import com.google.common.net.HostAndPort;

/**
 * A node in the cloud
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Node
{
	
	/** The role. */
	private final Role role;
	
	/** The ip. */
	private final HostAndPort ip;
	
	/** The load. */
	private final int load;
	
	/** The traffic. */
	private final Metric traffic;
	
	/** Adjacent nodes*/
	private final Map<Relationship, Node> connections;
	
	/**
	 * Instantiates a new node.
	 * 
	 * @param role
	 *            the role
	 * @param ip
	 *            the ip
	 * @param load
	 *            the load
	 * @param traffic
	 *            the traffic
	 */
	public Node(Role role, HostAndPort ip, int load, Metric traffic, HashMap<Relationship, Node> connections)
	{
		this.role = role;
		this.ip = ip;
		this.load = load;
		this.traffic = traffic;
		this.connections = connections;
	}
	
	/**
	 * Gets the role.
	 * 
	 * @return the role
	 */
	public Role getRole()
	{
		return role;
	}
	
	/**
	 * Gets the ip.
	 * 
	 * @return the ip
	 */
	public HostAndPort getIp()
	{
		return ip;
	}
	
	/**
	 * Gets the load.
	 * 
	 * @return the load
	 */
	public int getLoad()
	{
		return load;
	}
	
	/**
	 * Gets the traffic.
	 * 
	 * @return the traffic
	 */
	public Metric getTraffic()
	{
		return traffic;
	}
	
	/**
	 * 
	 * @return the adjacent connections.
	 */
	public Map<Relationship, Node> getConnections()
	{
		return connections;
	}
}
