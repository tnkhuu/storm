/**
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.heuristics;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.fusesource.hawtdispatch.Dispatcher;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.ActivityService;
import org.storm.api.services.CloudHealthService;
import org.storm.api.services.DistributedService;
import org.storm.api.services.VirtualizationProvider;

/**
 * Provides a heuristics service for the current node.
 *
 * @author Trung Khuu
 * @since 1.0.
 */
public class ComponentActivator extends DependencyActivatorBase implements BundleActivator
{
    private volatile VirtualizationProvider ref;
    private static volatile ComponentActivator INSTANCE;
    private static Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);
    private final Object mutex = new Object();
    private volatile boolean initialized;

    /**
     * Gets the virtualization provider.
     *
     * @return the virtualization provider
     */
    public static VirtualizationProvider getVirtualizationProvider()
    {
        return INSTANCE == null ? null : INSTANCE.ref;
    }

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception
    {
        if (!initialized)
        {
            synchronized (mutex)
            {
                if (!initialized)
                {
                    initialized = true;
                    manager.add(createComponent()
                            .setInterface(new String[]{CloudHealthService.class.getName(), DistributedService.class.getName()}, null)
                            .setImplementation(CloudHealthServiceImpl.class)
                            .add(createServiceDependency().setService(Dispatcher.class)
                                    .setRequired(true)));
                    manager.add(createComponent()
                            .setInterface(new String[]{ActivityService.class.getName(), DistributedService.class.getName()}, null)
                            .setImplementation(new ActivityServiceImpl(context)));

                    s_log.info("Cloud Heuristics Services Activated.");
                }
            }
        }

    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception
    {


    }

}
