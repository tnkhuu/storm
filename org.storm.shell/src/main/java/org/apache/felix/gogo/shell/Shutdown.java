/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.felix.gogo.shell;

import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Shutdown.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Shutdown
{
	public static String[] function = new String[]{"shutdown"};
	/** The s_log. */
	private Logger s_log = LoggerFactory.getLogger(Shutdown.class);
	
	/** The context. */
	BundleContext context;

	/**
	 * Instantiates a new shutdown.
	 * 
	 * @param ctx
	 *            the ctx
	 */
	public Shutdown(BundleContext ctx)
	{
		this.context = ctx;
	}

	/**
	 * Shutdown.
	 */
	public void shutdown()
	{
		try
		{
			context.getBundle(0).stop();
		}
		catch (BundleException e)
		{
			s_log.error(e.getMessage(), e);
			System.exit(0);
		}
	}
}
