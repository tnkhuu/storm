/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.felix.gogo.shell;

import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.apache.felix.bundlerepository.RepositoryService;
import org.apache.felix.gogo.command.Basic;
import org.apache.felix.gogo.command.Files;
import org.apache.felix.gogo.command.Inspect42;
import org.apache.felix.gogo.command.OBR;
import org.apache.felix.service.command.CommandProcessor;
import org.apache.felix.service.command.CommandSession;
import org.apache.felix.service.command.Converter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@SuppressWarnings({ "unchecked", "rawtypes" })
public class Activator implements BundleActivator, Runnable
{
	private static final Logger s_log = LoggerFactory.getLogger(Activator.class);
    private BundleContext context;
    private ServiceTracker commandProcessorTracker;
    private Set<ServiceRegistration> regs = new HashSet<ServiceRegistration>();
    private CommandSession session;
    private Shell shell;
    private Thread thread;
    private volatile ServiceTracker m_tracker = null;

	public void start(final BundleContext bc) throws Exception
    {
        context = bc;
        commandProcessorTracker = processorTracker();
        Hashtable<String, Object> props = new Hashtable<>();
        props.put("osgi.command.scope", "felix");
        props.put("osgi.command.function", new String[] {
            "bundlelevel", "frameworklevel", "headers",
            "help", "install", "lb", "log", "refresh",
            "resolve", "start", "stop", "uninstall", "update",
            "which" });
        bc.registerService(
            Basic.class.getName(), new Basic(bc), props);

        // Register "inspect" command for R4.3 or R4.2 depending
        // on the underlying framework.
        props.put("osgi.command.scope", "felix");
        props.put("osgi.command.function", new String[] { "inspect" });
        try
        {
            bc.registerService(
                    Inspect42.class.getName(), new Inspect42(bc), props);
        }
        catch (Throwable th)
        {
        	s_log.info("Failed to register bundle inspection services. Reason : " + th.getMessage());
        }

        props.put("osgi.command.scope", "felix");
        props.put("osgi.command.function", new String[] { "cd", "ls" });
        bc.registerService(
            Files.class.getName(), new Files(bc), props);

        m_tracker = new ServiceTracker(
                bc, RepositoryService.class, null);
            m_tracker.open();
            props.put("osgi.command.scope", "obr");
            props.put("osgi.command.function", new String[] {
                "deploy", "info", "javadoc", "list", "repos", "source" });
            bc.registerService(
                OBR.class.getName(), new OBR(bc, m_tracker), props);

    }

    public void stop(BundleContext context) throws Exception
    {
        if (thread != null)
        {
            thread.interrupt();
        }

        commandProcessorTracker.close();
        
        Iterator<ServiceRegistration> iterator = regs.iterator();
        while (iterator.hasNext())
        {
            ServiceRegistration reg = iterator.next();
            reg.unregister();
            iterator.remove();
        }
    }

    public void run()
    {
        try
        {
            // wait for gosh command to be registered
            for (int i = 0; (i < 100) && session.get("gogo:gosh") == null; ++i) {
	            Thread.sleep(10);
            }
            Thread.sleep(2000);
            String args = context.getProperty("gosh.args");
            args = (args == null) ? "" : args;
            session.execute("gosh --login " + args);
        }
        catch (Exception e)
        {
            Object loc = session.get(".location");
            if (null == loc || !loc.toString().contains(":"))
            {
                loc = "gogo";
            }

            System.err.println(loc + ": " + e.getClass().getSimpleName() + ": " + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }
    }

    private void startShell(BundleContext context, CommandProcessor processor)
    {
        Dictionary<String, Object> dict = new Hashtable<String, Object>();
        dict.put(CommandProcessor.COMMAND_SCOPE, "felix");

        // register converters
        regs.add(context.registerService(Converter.class.getName(), new Converters(context), null));
        
        // register commands
        
        dict.put(CommandProcessor.COMMAND_FUNCTION, Builtin.functions);
        regs.add(context.registerService(Builtin.class.getName(), new Builtin(), dict));

        dict.put(CommandProcessor.COMMAND_FUNCTION, Procedural.functions);
        regs.add(context.registerService(Procedural.class.getName(), new Procedural(), dict));

        dict.put(CommandProcessor.COMMAND_FUNCTION, Posix.functions);
        regs.add(context.registerService(Posix.class.getName(), new Posix(), dict));

        dict.put(CommandProcessor.COMMAND_FUNCTION, Telnet.functions);
        regs.add(context.registerService(Telnet.class.getName(), new Telnet(processor), dict));
        
        shell = new Shell(context, processor);
        dict.put(CommandProcessor.COMMAND_FUNCTION, Shell.functions);
        regs.add(context.registerService(Shell.class.getName(), shell, dict));
        
        dict.put(CommandProcessor.COMMAND_FUNCTION, Shutdown.function);
        regs.add(context.registerService(Shutdown.class.getName(), new Shutdown(context), dict));
        
        // start shell
        session = processor.createSession(System.in, System.out, System.err);
        thread = new Thread(this, "Shell");
        thread.start();
    }

    private ServiceTracker processorTracker()
    {
        ServiceTracker t = new ServiceTracker(context, CommandProcessor.class.getName(),
            null)
        {
            @Override
            public Object addingService(ServiceReference reference)
            {
                CommandProcessor processor = (CommandProcessor) super.addingService(reference);
                startShell(context, processor);
                return processor;
            }

            @Override
            public void removedService(ServiceReference reference, Object service)
            {
                if (thread != null)
                {
                    thread.interrupt();
                }
                super.removedService(reference, service);
            }
        };

        t.open();
        return t;
    }
}