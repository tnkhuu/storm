/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.CountDownLatch;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.storm.api.dns.MessageCallback;
import org.storm.dns.client.DNSClient;
import org.storm.dns.client.DNSPacket;
import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Rcode;
import org.xbill.java.dns.unsafe.protocol.Section;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.Record;

@Ignore("Dont have access to dns server.")
public class DNSClientTest
{

	private DNSClient client = new DNSClient(new InetSocketAddress("10.0.3.46", 8053));

	/**
	 * Test client.
	 * 
	 * @throws TextParseException
	 *             the text parse exception
	 * @throws UnknownHostException
	 *             the unknown host exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@Test
	public void testUpdateClient() throws TextParseException, UnknownHostException, InterruptedException
	{

		final CountDownLatch latch = new CountDownLatch(1);
		Message msg = Message.newUpdate(Name.fromString("stormclowd.com."));
		Record r = new ARecord(Name.fromString("g.stormclowd.com."), DClass.IN, 1800L, InetAddress.getByName("10.0.3.162"));
		msg.addRecord(r, Section.UPDATE);
		client.send(msg, new MessageCallback<DNSPacket>()
		{

			@Override
			public void onFail(Throwable t)
			{
				Assert.fail("Failed message");

			}

			@Override
			public void onComplete(DNSPacket packet)
			{
				int rcode = packet.getDecodedMessage().getRcode();
				Assert.assertEquals(Rcode.NOERROR, rcode);
				latch.countDown();

			}
		});

		latch.await();
	}

	/**
	 * Test client.
	 * 
	 * @throws TextParseException
	 *             the text parse exception
	 * @throws UnknownHostException
	 *             the unknown host exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@Test @Ignore
	public void testUpdateAndQueryClient() throws TextParseException, UnknownHostException, InterruptedException
	{

		final CountDownLatch latch = new CountDownLatch(1);
		Message msg = Message.newUpdate(Name.fromString("stormclowd.com."));
		Record r = new ARecord(Name.fromString("ensemble.stormclowd.com."), DClass.IN, 1800L, InetAddress.getByName("192.168.1.10"));
		msg.addRecord(r, Section.UPDATE);
		client.send(msg, new MessageCallback<DNSPacket>()
		{

			@Override
			public void onFail(Throwable t)
			{
				Assert.fail("Failed message");

			}

			@Override
			public void onComplete(DNSPacket packet)
			{
				int rcode = packet.getDecodedMessage().getRcode();
				Assert.assertEquals(Rcode.NOERROR, rcode);
				latch.countDown();

			}
		});

		latch.await();
		final CountDownLatch another = new CountDownLatch(1);
		Message m = Message.newQuery(r);
		m.addRecord(r, Section.QUESTION);
		final int id = m.getHeader().getID();
		client.send(m, new MessageCallback<DNSPacket>()
		{

			@Override
			public void onFail(Throwable t)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onComplete(DNSPacket packet)
			{
				int packetId = packet.getDecodedMessage().getHeader().getID();
				if (packetId == id)
				{
					Record[] set = packet.getDecodedMessage().getSectionArray(Section.ANSWER);
					Assert.assertNotNull(set);
					Assert.assertEquals(1, set.length);
					try
					{
						Assert.assertEquals(Name.fromString("ensemble.stormclowd.com."), set[0].name);
					}
					catch (TextParseException e)
					{
						e.printStackTrace();
					}
					another.countDown();
				}
			}
		});
		another.await();
	}
}
