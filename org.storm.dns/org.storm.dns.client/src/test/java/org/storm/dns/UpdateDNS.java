package org.storm.dns;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import org.storm.api.dns.MessageCallback;
import org.storm.dns.client.DNSClient;
import org.storm.dns.client.DNSPacket;

public class UpdateDNS
{

	public static void main(String[] args) throws UnknownHostException
	{
		 DNSClient client = new DNSClient(new InetSocketAddress("dns.stormclowd.com", 53));
		 client.update("stormclowd.com.", "www.stormclowd.com.", "10.0.3.55", new MessageCallback<DNSPacket>()
		{
			
			@Override
			public void onFail(Throwable t)
			{
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onComplete(DNSPacket packet)
			{
				System.out.println("Done");
				
			}
		});

	}

}
