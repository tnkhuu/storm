/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.socket.DatagramPacket;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.xbill.java.dns.unsafe.WireParseException;
import org.xbill.java.dns.unsafe.protocol.Flags;
import org.xbill.java.dns.unsafe.protocol.Message;

/**
 * A Netty based {@link DatagramPacket} extended to understand DNS semantics.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DNSPacket extends DatagramPacket
{
	private Message decodedMessage;

	public static enum WriteMode
	{
		ENCODE, REFERENCE, DUPLICATE
	}

	public DNSPacket(ByteBuf buffer, InetSocketAddress remoteAddress, boolean decode)
	{
		this(-1, buffer, remoteAddress, decode);
	}

	public DNSPacket(int id, ByteBuf buffer, InetSocketAddress remoteAddress, boolean decode)
	{
		super(id == -1 ? buffer : overwriteHeaderId(buffer, id), remoteAddress);
		try
		{

			if (decode)
			{
				decodedMessage = new Message(buffer.array());
			}
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Instantiates a new DNS packet.
	 * 
	 * @param buffer
	 *            the buffer
	 * @param remoteAddress
	 *            the remote address
	 */
	public DNSPacket(ByteBuf buffer, InetSocketAddress remoteAddress)
	{
		this(buffer, remoteAddress, true);
	}

	public Message getDecodedMessage()
	{
		return decodedMessage;
	}

	public DNSPacket setResponse()
	{
		try
		{
			byte[] buff = data().array();
			// read the 16bit unsigned flag value on the msg header
			int flags = readFlag(buff);
			System.out.println(getFlag(0, flags));
			int newFlag = setFlag(0, flags);
			System.out.println("After: " + getFlag(0, newFlag));
			// set the response bit
			// write it back out to the buffer.
			writeFlag(buff, newFlag);
		}
		catch (WireParseException e)
		{
			throw new RuntimeException(e);
		}
		return this;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (!(obj instanceof DNSPacket))
		{
			return false;
		}
		DNSPacket other = (DNSPacket) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode()
	{
		return getId();
	}

	public void setId(int id)
	{
		overwriteHeaderId(data(), id);
	}

	public int getId()
	{
		int ch1 = data().array()[0] & 0xFF;
		int ch2 = data().array()[1] & 0xFF;

		return (ch1 << 8) + ch2;
	}

	private static ByteBuf overwriteHeaderId(ByteBuf buffer, int id)
	{
		buffer.setByte(0, (byte) (id >>> 8 & 0xFF));
		buffer.setByte(1, (byte) (id & 0xFF));
		return buffer;
	}

	public int readFlag(byte[] buffer) throws WireParseException
	{

		int b1 = buffer[2] & 0xFF;
		int b2 = buffer[3] & 0xFF;
		return (b2 << 8) + b1;
	}

	public void writeFlag(byte[] buffer, int val) throws WireParseException
	{

		buffer[2] = (byte) (val >>> 8 & 0xFF);
		buffer[3] = (byte) (val & 0xFF);
	}

	/**
	 * Sets a flag to the supplied value
	 * 
	 * @see Flags
	 */
	public int setFlag(int bit, int flag)
	{
		return flag |= 1 << 15 - bit;
	}

	/**
	 * unsets a flag
	 * 
	 * @see Flags
	 */
	public void unsetFlag(int bit, int flag)
	{
		// bits are indexed from left to right
		flag = flag &= ~(1 << 15 - bit);
	}

	public boolean getFlag(int bit, int flags)
	{
		// bits are indexed from left to right
		return (flags & 1 << 15 - bit) != 0;
	}

}
