/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.io.unsafe;

/**
 * JVM native data structure sizes and offsets.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("restriction")
public interface JVMConstants
{

	/** The size of a boolean in bytes */
	int SIZE_OF_BOOLEAN = 1;

	/** The size of int in bytes */
	int SIZE_OF_INT = 4;

	/** The size of char in bytes */
	int SIZE_OF_CHAR = 4;

	/** The size of long in bytes */
	int SIZE_OF_LONG = 8;

	/** The size of double in bytes */
	int SIZE_OF_DOUBLE = 8;

	/** The byte array offset. */
	long BYTE_ARRAY_OFFSET = UnsafeHook.getUnsafe().arrayBaseOffset(byte[].class);

	/** The long array offset. */
	long LONG_ARRAY_OFFSET = UnsafeHook.getUnsafe().arrayBaseOffset(long[].class);

	/** The double array offset. */
	long DOUBLE_ARRAY_OFFSET = UnsafeHook.getUnsafe().arrayBaseOffset(double[].class);

	/** The float array offset. */
	long FLOAT_ARRAY_OFFSET = UnsafeHook.getUnsafe().arrayBaseOffset(float[].class);
}
