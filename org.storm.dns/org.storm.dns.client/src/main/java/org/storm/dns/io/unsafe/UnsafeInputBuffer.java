/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.io.unsafe;

import org.xbill.java.dns.unsafe.WireParseException;

import sun.misc.Unsafe;

/**
 * Input buffer with performance comparable to c.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("restriction")
public class UnsafeInputBuffer implements JVMConstants
{

	protected int pos;
	protected int end;
	protected byte[] array;
	protected final Unsafe unsafe;
	protected Marker mark;

	/**
	 * Instantiates a new unsafe input buffer.
	 * 
	 * @param buffer
	 *            the buffer
	 */
	public UnsafeInputBuffer(final byte[] buffer)
	{
		assert buffer != null;
		this.pos = 0;
		this.end = buffer.length;
		this.array = buffer;
		this.unsafe = UnsafeHook.getUnsafe();
		this.mark = new Marker();
	}

	/**
	 * Reset.
	 */
	public void reset()
	{
		this.pos = 0;
	}

	/**
	 * Require.
	 * 
	 * @param n
	 *            the n
	 */
	protected void require(int n)
	{
		if (n > remaining())
		{
			throw new IllegalStateException("Exceeded Buffer");
		}
	}

	/**
	 * Remaining.
	 * 
	 * @return the int
	 */
	public int remaining()
	{
		return end - pos;
	}

	/**
	 * Read u8.
	 * 
	 * @return the int
	 */
	public int readU8()
	{
		require(1);
		int b1 = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos++) & 0xFF;
		return b1;

	}

	/**
	 * Read u16.
	 * 
	 * @return the int
	 * @throws WireParseException
	 *             the wire parse exception
	 */
	public int readU16() throws WireParseException
	{
		require(2);
		int b1 = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos++) & 0xFF;
		int b2 = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos++) & 0xFF;
		return (b1 << 8) + b2;
	}

	/**
	 * Read u32.
	 * 
	 * @return the long
	 * @throws WireParseException
	 *             the wire parse exception
	 */
	public long readU32() throws WireParseException
	{
		require(4);
		int b1 = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos++) & 0xFF;
		int b2 = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos++) & 0xFF;
		int b3 = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos++) & 0xFF;
		int b4 = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos++) & 0xFF;
		return ((long) b1 << 24) + (b2 << 16) + (b3 << 8) + b4;
	}

	/**
	 * Reader pos.
	 * 
	 * @return the int
	 */
	public int readerPos()
	{
		return pos;
	}

	/**
	 * Read byte array.
	 * 
	 * @param len
	 *            the len
	 * @return the byte[]
	 * @throws WireParseException
	 *             the wire parse exception
	 */
	public byte[] readByteArray(int len) throws WireParseException
	{
		require(len);
		byte[] values = new byte[len];

		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET + pos, values, BYTE_ARRAY_OFFSET, len);
		pos += len;
		return values;
	}

	/**
	 * Read byte array.
	 * 
	 * @param b
	 *            the b
	 * @param off
	 *            the off
	 * @param len
	 *            the len
	 * @throws WireParseException
	 *             the wire parse exception
	 */
	public void readByteArray(byte[] b, int off, int len) throws WireParseException
	{
		require(len);
		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET + pos, b, BYTE_ARRAY_OFFSET + off, len);
		pos += len;
	}

	/**
	 * Read byte array.
	 * 
	 * @return the byte[]
	 */
	public byte[] readByteArray()
	{
		int len = remaining();
		byte[] values = new byte[len];
		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET + pos, values, BYTE_ARRAY_OFFSET, len);
		pos += len;
		return values;
	}

	/**
	 * Read counted string.
	 * 
	 * @return the byte[]
	 * @throws WireParseException
	 *             the wire parse exception
	 */
	public byte[] readCountedString() throws WireParseException
	{
		require(1);
		int theByte = unsafe.getByte(array, BYTE_ARRAY_OFFSET + pos);
		pos++;
		int len = theByte;
		return readByteArray(len);
	}

	/**
	 * Read boolean.
	 * 
	 * @return true, if successful
	 */
	public boolean readBoolean()
	{
		boolean value = unsafe.getBoolean(array, BYTE_ARRAY_OFFSET + pos);
		pos += SIZE_OF_BOOLEAN;

		return value;
	}

	/**
	 * Read int.
	 * 
	 * @return the int
	 */
	public int readInt()
	{
		int value = unsafe.getInt(array, BYTE_ARRAY_OFFSET + pos);
		pos += SIZE_OF_INT;

		return value;
	}

	/**
	 * Read long.
	 * 
	 * @return the long
	 */
	public long readLong()
	{
		long value = unsafe.getLong(array, BYTE_ARRAY_OFFSET + pos);
		pos += SIZE_OF_LONG;

		return value;
	}

	/**
	 * Read double array.
	 * 
	 * @return the double[]
	 */
	public double[] readDoubleArray()
	{
		int arraySize = readInt();
		double[] values = new double[arraySize];

		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET + pos, values, DOUBLE_ARRAY_OFFSET, bytesToCopy);
		pos += bytesToCopy;
		return values;
	}

	/**
	 * Read long array.
	 * 
	 * @return the long[]
	 */
	public long[] readLongArray()
	{
		int arraySize = readInt();
		long[] values = new long[arraySize];

		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET + pos, values, LONG_ARRAY_OFFSET, bytesToCopy);
		pos += bytesToCopy;

		return values;
	}

	/**
	 * Seek.
	 * 
	 * @param marker
	 *            the marker
	 */
	public void seek(Marker marker)
	{
		pos = marker.start;
		end = array.length;
	}

	/**
	 * The Class Marker.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class Marker
	{

		/** The start. */
		private int start = -1;

		/** The end. */
		private int end = -1;

		/**
		 * Gets the start.
		 * 
		 * @return the start
		 */
		public int getStart()
		{
			return start;
		}

		/**
		 * Gets the end.
		 * 
		 * @return the end
		 */
		public int getEnd()
		{
			return end;
		}

		/**
		 * Sets the end.
		 * 
		 * @param end
		 *            the new end
		 */
		public void setEnd(int end)
		{
			this.end = end;
		}

		/**
		 * Sets the start.
		 * 
		 * @param start
		 *            the new start
		 */
		public void setStart(int start)
		{
			this.start = start;
		}

		/**
		 * Mark.
		 * 
		 * @param start
		 *            the start
		 * @param end
		 *            the end
		 */
		public void mark(int start, int end)
		{
			this.start = start;
			this.end = end;
		}

		/**
		 * Reset.
		 */
		public void reset()
		{
			start = -1;
			end = -1;
		}

	}
}