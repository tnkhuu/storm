/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.io.unsafe;

import sun.misc.Unsafe;

/**
 * Output buffer with performance comparable to c.
 * 
 * Performs 3x faster then Direct NIO and 10x faster then normal streams.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("restriction")
public class UnsafeOutputBuffer implements JVMConstants
{

	protected byte[] array;
	protected int pos;
	protected int marker;
	protected final Unsafe unsafe;

	/**
	 * Instantiates a new unsafe output buffer.
	 * 
	 * @param size
	 *            the size
	 */
	public UnsafeOutputBuffer(int size)
	{
		array = new byte[size];
		pos = 0;
		marker = -1;
		this.unsafe = UnsafeHook.getUnsafe();
	}

	/**
	 * Create a new UnsafeOutputBuffer.
	 */
	public UnsafeOutputBuffer()
	{
		this(32);
	}

	/**
	 * Current.
	 * 
	 * @return the int
	 */
	public int current()
	{
		return pos;
	}

	/**
	 * Check.
	 * 
	 * @param val
	 *            the val
	 * @param bits
	 *            the bits
	 */
	private void check(long val, int bits)
	{
		long max = 1;
		max <<= bits;
		if (val < 0 || val > max)
		{
			throw new IllegalArgumentException(val + " out of range for " + bits + " bit value");
		}
	}

	/**
	 * Need.
	 * 
	 * @param n
	 *            the n
	 */
	private void need(int n)
	{
		if (array.length - pos >= n)
		{
			return;
		}
		int newsize = array.length * 2;
		if (newsize < pos + n)
		{
			newsize = pos + n;
		}
		byte[] newarray = new byte[newsize];

		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET, newarray, BYTE_ARRAY_OFFSET, pos);

		array = newarray;
	}

	/**
	 * Seek.
	 * 
	 * @param index
	 *            the index
	 */
	public void seek(int index)
	{
		if (index > pos)
		{
			throw new IllegalArgumentException("cannot jump past " + "end of data");
		}
		pos = index;
	}

	/**
	 * Mark.
	 */
	public void mark()
	{
		marker = pos;
	}

	/**
	 * Restore.
	 */
	public void restore()
	{
		if (marker < 0)
		{
			throw new IllegalStateException("no previous state");
		}
		pos = marker;
		marker = -1;
	}

	/**
	 * Write u8.
	 * 
	 * @param val
	 *            the val
	 */
	public void writeU8(int val)
	{
		check(val, 8);
		need(1);
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + pos++, (byte) (val & 0xFF));
	}

	/**
	 * Write u16.
	 * 
	 * @param val
	 *            the val
	 */
	public void writeU16(int val)
	{
		check(val, 16);
		need(2);
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + pos++, (byte) (val >>> 8 & 0xFF));
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + pos++, (byte) (val & 0xFF));
	}

	/**
	 * Write unsigned 16 byte integer.
	 * 
	 * @param val
	 *            the val
	 * @param where
	 *            the where
	 */
	public void writeU16At(int val, int where)
	{
		check(val, 16);
		if (where > pos - 2)
		{
			throw new IllegalArgumentException("cannot write past " + "end of data");
		}
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + where++, (byte) (val >>> 8 & 0xFF));
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + where++, (byte) (val & 0xFF));
	}

	/**
	 * Write an unsigned 32 byte long
	 * 
	 * @param val
	 *            the val
	 */
	public void writeU32(long val)
	{
		check(val, 32);
		need(4);
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + pos++, (byte) (val >>> 24 & 0xFF));
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + pos++, (byte) (val >>> 16 & 0xFF));
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + pos++, (byte) (val >>> 8 & 0xFF));
		unsafe.putByte(array, BYTE_ARRAY_OFFSET + pos++, (byte) (val & 0xFF));
	}

	/**
	 * Write byte array.
	 * 
	 * @param b
	 *            the b
	 * @param off
	 *            the off
	 * @param len
	 *            the len
	 */
	public void writeByteArray(byte[] b, int off, int len)
	{
		need(len);

		unsafe.copyMemory(b, BYTE_ARRAY_OFFSET + off, array, BYTE_ARRAY_OFFSET + pos, len);
		pos += len;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeByteArray(byte[])
	 */
	/**
	 * Write byte array.
	 * 
	 * @param b
	 *            the b
	 */
	public void writeByteArray(byte[] b)
	{
		writeByteArray(b, 0, b.length);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeCountedString(byte[])
	 */
	/**
	 * Write counted string.
	 * 
	 * @param s
	 *            the s
	 */
	public void writeCountedString(byte[] s)
	{
		if (s.length > 0xFF)
		{
			throw new IllegalArgumentException("Invalid counted string");
		}
		need(1 + s.length);
		array[pos++] = (byte) (s.length & 0xFF);
		writeByteArray(s, 0, s.length);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#toByteArray()
	 */
	/**
	 * To byte array.
	 * 
	 * @return the byte[]
	 */
	public byte[] toByteArray()
	{
		byte[] values = new byte[pos];
		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET, values, BYTE_ARRAY_OFFSET, pos);
		return values;
	}

	/**
	 * To wrapped byte array.
	 * 
	 * @return the byte[]
	 */
	public byte[] toWrappedByteArray()
	{
		byte[] values = new byte[pos];
		unsafe.copyMemory(array, BYTE_ARRAY_OFFSET, values, BYTE_ARRAY_OFFSET, pos);
		return values;
	}

	/**
	 * Write int.
	 * 
	 * @param value
	 *            the value
	 */
	public void writeInt(final int value)
	{
		unsafe.putInt(array, BYTE_ARRAY_OFFSET + pos, value);
		pos += SIZE_OF_INT;
	}

	/**
	 * Write long array.
	 * 
	 * @param values
	 *            the values
	 */
	public void writeLongArray(final long[] values)
	{
		writeInt(values.length);

		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(values, LONG_ARRAY_OFFSET, array, BYTE_ARRAY_OFFSET + pos, bytesToCopy);
		pos += bytesToCopy;
	}

	/**
	 * Write boolean.
	 * 
	 * @param value
	 *            the value
	 */
	public void writeBoolean(final boolean value)
	{
		unsafe.putBoolean(array, BYTE_ARRAY_OFFSET + pos, value);
		pos += SIZE_OF_BOOLEAN;
	}

	/**
	 * Write long.
	 * 
	 * @param value
	 *            the value
	 */
	public void writeLong(final long value)
	{
		unsafe.putLong(array, BYTE_ARRAY_OFFSET + pos, value);
		pos += SIZE_OF_LONG;
	}

	/**
	 * Write double array.
	 * 
	 * @param values
	 *            the values
	 */
	public void writeDoubleArray(final double[] values)
	{
		writeInt(values.length);

		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(values, DOUBLE_ARRAY_OFFSET, array, BYTE_ARRAY_OFFSET + pos, bytesToCopy);
		pos += bytesToCopy;
	}

	/**
	 * The Class Marker.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class Marker
	{

		/** The start. */
		private int start = -1;

		/** The end. */
		private int end = -1;

		/**
		 * Gets the start.
		 * 
		 * @return the start
		 */
		public int getStart()
		{
			return start;
		}

		/**
		 * Gets the end.
		 * 
		 * @return the end
		 */
		public int getEnd()
		{
			return end;
		}

		/**
		 * Sets the end.
		 * 
		 * @param end
		 *            the new end
		 */
		public void setEnd(int end)
		{
			this.end = end;
		}

		/**
		 * Sets the start.
		 * 
		 * @param start
		 *            the new start
		 */
		public void setStart(int start)
		{
			this.start = start;
		}

		/**
		 * Mark.
		 * 
		 * @param start
		 *            the start
		 * @param end
		 *            the end
		 */
		public void mark(int start, int end)
		{
			this.start = start;
			this.end = end;
		}

		/**
		 * Reset.
		 */
		public void reset()
		{
			start = -1;
			end = -1;
		}

	}
}
