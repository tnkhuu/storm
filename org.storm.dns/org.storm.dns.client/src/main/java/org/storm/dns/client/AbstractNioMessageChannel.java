/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.client;

import io.netty.buffer.MessageBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.nio.AbstractNioChannel;

import java.io.IOException;
import java.nio.channels.SelectableChannel;

/**
 * The Modified Netty {@link AbstractNioMessageChannel} to more optimally handle
 * {@link DNSPacket}'s.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
abstract class AbstractNioMessageChannel extends AbstractNioChannel
{

	/**
	 * Instantiates a new abstract nio message channel.
	 * 
	 * @param parent
	 *            the parent
	 * @param id
	 *            the id
	 * @param ch
	 *            the ch
	 * @param readInterestOp
	 *            the read interest op
	 */
	protected AbstractNioMessageChannel(Channel parent, Integer id, SelectableChannel ch, int readInterestOp)
	{
		super(parent, id, ch, readInterestOp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.netty.channel.AbstractChannel#newUnsafe()
	 */
	@Override
	protected NioMessageUnsafe newUnsafe()
	{
		return new NioMessageUnsafe();
	}

	/**
	 * The Class NioMessageUnsafe.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private final class NioMessageUnsafe extends AbstractNioUnsafe
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see io.netty.channel.socket.nio.AbstractNioChannel.NioUnsafe#read()
		 */
		@Override
		public void read()
		{
			assert eventLoop().inEventLoop();

			final ChannelPipeline pipeline = pipeline();
			final MessageBuf<Object> msgBuf = pipeline.inboundMessageBuffer();
			boolean closed = false;
			boolean read = false;
			try
			{
				for (;;)
				{
					int localReadAmount = doReadMessages(msgBuf);
					if (localReadAmount > 0)
					{
						read = true;
					}
					else if (localReadAmount == 0)
					{
						break;
					}
					else if (localReadAmount < 0)
					{
						closed = true;
						break;
					}
				}
			}
			catch (Throwable t)
			{
				if (read)
				{
					read = false;
					pipeline.fireInboundBufferUpdated();
				}
				pipeline().fireExceptionCaught(t);
				if (t instanceof IOException)
				{
					close(voidFuture());
				}
			}
			finally
			{
				if (read)
				{
					pipeline.fireInboundBufferUpdated();
				}
				if (closed && isOpen())
				{
					close(voidFuture());
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.AbstractChannel#doFlushMessageBuffer(io.netty.buffer
	 * .MessageBuf)
	 */
	@Override
	protected void doFlushMessageBuffer(MessageBuf<Object> buf) throws Exception
	{
		final int writeSpinCount = config().getWriteSpinCount() - 1;
		while (!buf.isEmpty())
		{
			boolean wrote = false;
			for (int i = writeSpinCount; i >= 0; i--)
			{
				int localFlushedAmount = doWriteMessages(buf, i == 0);
				if (localFlushedAmount > 0)
				{
					wrote = true;
					break;
				}
			}

			if (!wrote)
			{
				break;
			}
		}
	}

	/**
	 * Do read messages.
	 * 
	 * @param buf
	 *            the buf
	 * @return the int
	 * @throws Exception
	 *             the exception
	 */
	protected abstract int doReadMessages(MessageBuf<Object> buf) throws Exception;

	/**
	 * Do write messages.
	 * 
	 * @param buf
	 *            the buf
	 * @param lastSpin
	 *            the last spin
	 * @return the int
	 * @throws Exception
	 *             the exception
	 */
	protected abstract int doWriteMessages(MessageBuf<Object> buf, boolean lastSpin) throws Exception;
}
