/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.dns.client;

import io.netty.channel.ChannelException;
import io.netty.channel.socket.DefaultDatagramChannelConfig;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.NetworkChannel;
import java.util.Enumeration;


/**
 * Netty Specific configuration
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class NioDatagramChannelConfig extends DefaultDatagramChannelConfig
{
	private static final Object IP_MULTICAST_TTL;
	private static final Object IP_MULTICAST_IF;
	private static final Object IP_MULTICAST_LOOP;
	private static final Method GET_OPTION;
	private static final Method SET_OPTION;

	static
	{
		ClassLoader classLoader = DatagramChannel.class.getClassLoader();
		Class<?> socketOptionType = null;
		try
		{
			socketOptionType = Class.forName("java.net.SocketOption", true, classLoader);
		}
		catch (Exception e)
		{
			// Not Java 7+
		}
		Class<?> stdSocketOptionType = null;
		try
		{
			stdSocketOptionType = Class.forName("java.net.StandardSocketOptions", true, classLoader);
		}
		catch (Exception e)
		{
			// Not Java 7+
		}

		Object ipMulticastTtl = null;
		Object ipMulticastIf = null;
		Object ipMulticastLoop = null;
		Method getOption = null;
		Method setOption = null;
		if (socketOptionType != null)
		{
			try
			{
				ipMulticastTtl = stdSocketOptionType.getDeclaredField("IP_MULTICAST_TTL").get(null);
			}
			catch (Exception e)
			{
				throw new Error("cannot locate the IP_MULTICAST_TTL field", e);
			}

			try
			{
				ipMulticastIf = stdSocketOptionType.getDeclaredField("IP_MULTICAST_IF").get(null);
			}
			catch (Exception e)
			{
				throw new Error("cannot locate the IP_MULTICAST_IF field", e);
			}

			try
			{
				ipMulticastLoop = stdSocketOptionType.getDeclaredField("IP_MULTICAST_LOOP").get(null);
			}
			catch (Exception e)
			{
				throw new Error("cannot locate the IP_MULTICAST_LOOP field", e);
			}

			try
			{
				getOption = NetworkChannel.class.getDeclaredMethod("getOption", socketOptionType);
			}
			catch (Exception e)
			{
				throw new Error("cannot locate the getOption() method", e);
			}

			try
			{
				setOption = NetworkChannel.class.getDeclaredMethod("setOption", socketOptionType, Object.class);
			}
			catch (Exception e)
			{
				throw new Error("cannot locate the setOption() method", e);
			}
		}
		IP_MULTICAST_TTL = ipMulticastTtl;
		IP_MULTICAST_IF = ipMulticastIf;
		IP_MULTICAST_LOOP = ipMulticastLoop;
		GET_OPTION = getOption;
		SET_OPTION = setOption;
	}

	/** The channel. */
	private final DatagramChannel channel;

	/**
	 * Instantiates a new nio datagram channel config.
	 * 
	 * @param channel
	 *            the channel
	 */
	NioDatagramChannelConfig(DatagramChannel channel)
	{
		super(channel.socket());
		this.channel = channel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.netty.channel.socket.DefaultDatagramChannelConfig#getTimeToLive()
	 */
	@Override
	public int getTimeToLive()
	{
		return (Integer) getOption0(IP_MULTICAST_TTL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.socket.DefaultDatagramChannelConfig#setTimeToLive(int)
	 */
	@Override
	public void setTimeToLive(int ttl)
	{
		setOption0(IP_MULTICAST_TTL, ttl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.netty.channel.socket.DefaultDatagramChannelConfig#getInterface()
	 */
	@Override
	public InetAddress getInterface()
	{
		NetworkInterface inf = getNetworkInterface();
		if (inf == null)
		{
			return null;
		}
		else
		{
			Enumeration<InetAddress> addresses = inf.getInetAddresses();
			if (addresses.hasMoreElements())
			{
				return addresses.nextElement();
			}
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.socket.DefaultDatagramChannelConfig#setInterface(java
	 * .net.InetAddress)
	 */
	@Override
	public void setInterface(InetAddress interfaceAddress)
	{
		try
		{
			setNetworkInterface(NetworkInterface.getByInetAddress(interfaceAddress));
		}
		catch (SocketException e)
		{
			throw new ChannelException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.socket.DefaultDatagramChannelConfig#getNetworkInterface
	 * ()
	 */
	@Override
	public NetworkInterface getNetworkInterface()
	{
		return (NetworkInterface) getOption0(IP_MULTICAST_IF);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.socket.DefaultDatagramChannelConfig#setNetworkInterface
	 * (java.net.NetworkInterface)
	 */
	@Override
	public void setNetworkInterface(NetworkInterface networkInterface)
	{
		setOption0(IP_MULTICAST_IF, networkInterface);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.socket.DefaultDatagramChannelConfig#isLoopbackModeDisabled
	 * ()
	 */
	@Override
	public boolean isLoopbackModeDisabled()
	{
		return (Boolean) getOption0(IP_MULTICAST_LOOP);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.socket.DefaultDatagramChannelConfig#setLoopbackModeDisabled
	 * (boolean)
	 */
	@Override
	public void setLoopbackModeDisabled(boolean loopbackModeDisabled)
	{
		setOption0(IP_MULTICAST_LOOP, loopbackModeDisabled);
	}

	/**
	 * Gets the option0.
	 * 
	 * @param option
	 *            the option
	 * @return the option0
	 */
	private Object getOption0(Object option)
	{
		if (DNSConfig.javaVersion() < 7)
		{
			throw new UnsupportedOperationException();
		}
		else
		{
			try
			{
				return GET_OPTION.invoke(channel, option);
			}
			catch (Exception e)
			{
				throw new ChannelException(e);
			}
		}
	}

	/**
	 * Sets the option0.
	 * 
	 * @param option
	 *            the option
	 * @param value
	 *            the value
	 */
	private void setOption0(Object option, Object value)
	{
		if (DNSConfig.javaVersion() < 7)
		{
			throw new UnsupportedOperationException();
		}
		else
		{
			try
			{
				SET_OPTION.invoke(channel, option, value);
			}
			catch (Exception e)
			{
				throw new ChannelException(e);
			}
		}
	}

}
