/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.io.unsafe;

import org.xbill.java.dns.unsafe.MessageInputBuffer;

/**
 * Input buffer with performance comparable to c.
 * 
 * Performs 3x faster then Direct NIO and 10x faster then normal streams.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class UnsafeInput extends UnsafeInputBuffer implements MessageInputBuffer
{

	/**
	 * Creates a new DNSInput.
	 * 
	 * @param input
	 *            The byte array to read from
	 */
	public UnsafeInput(byte[] input)
	{
		super(input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.unsafe.UnsafeInputBuffer#readerPos()
	 */
	@Override
	public int readerPos()
	{
		return pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#setActive(int)
	 */
	@Override
	public void setActive(int len)
	{
		if (len > array.length - pos)
		{
			throw new IllegalArgumentException("cannot set active " + "region past end of input");
		}
		end = pos + len;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#clearActive()
	 */
	@Override
	public void clearActive()
	{
		end = array.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#saveActive()
	 */
	@Override
	public int saveActive()
	{
		return end;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#restoreActive(int)
	 */
	@Override
	public void restoreActive(int pos)
	{
		if (pos > array.length)
		{
			throw new IllegalArgumentException("cannot set active " + "region past end of input");
		}
		end = pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#seek(int)
	 */
	@Override
	public void seek(int index)
	{
		if (index >= array.length)
		{
			throw new IllegalArgumentException("cannot jump past " + "end of input");
		}
		pos = index;
		end = array.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#save()
	 */
	@Override
	public void save()
	{
		mark.mark(pos, end);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#restore()
	 */
	@Override
	public void restore()
	{
		if (mark.getStart() < 0)
		{
			throw new IllegalStateException("no previous state");
		}
		pos = mark.getStart();
		end = mark.getEnd();
		mark.reset();
	}

}
