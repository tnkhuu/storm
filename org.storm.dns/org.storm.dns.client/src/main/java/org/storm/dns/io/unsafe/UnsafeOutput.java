/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.io.unsafe;

import org.xbill.java.dns.unsafe.MessageOutputBuffer;

/**
 * Output buffer with performance comparable to c and a adapter to the
 * {@link MessageOutputBuffer} interface.
 * 
 * Performs 3x faster then Direct NIO and 10x faster then normal streams.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class UnsafeOutput extends UnsafeOutputBuffer implements MessageOutputBuffer
{

	/**
	 * Instantiates a new unsafe output.
	 * 
	 * @param size
	 *            the size
	 */
	public UnsafeOutput(int size)
	{
		super(size);
	}

	/**
	 * Instantiates a new unsafe output.
	 */
	public UnsafeOutput()
	{
		this(32);
	}

}
