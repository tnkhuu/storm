/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioEventLoopGroup;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.dns.DNSAsyncClient;
import org.storm.api.dns.MessageCallback;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Section;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.Record;

/**
 * A Asynchronous DNSClient using Netty.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DNSClient implements DNSAsyncClient<Message, DNSPacket>
{

	private static final Logger s_log = LoggerFactory.getLogger(DNSClient.class);
	private final InetSocketAddress	recursiveServer;
	private volatile Channel					dnsChannel;
	private ConcurrentHashMap<Integer, MessageCallback<DNSPacket>> callbacks = new ConcurrentHashMap<>();

	/**
	 * Instantiates a new dNS client.
	 * 
	 * @param recursive
	 *            the recursive
	 */
	public DNSClient(InetSocketAddress recursive)
	{
		this.recursiveServer = recursive;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.dns.DNSAsyncClient#send(java.lang.Object, org.storm.api.dns.MessageCallback)
	 */
	@Override
	public void send(final Message msg, final MessageCallback<DNSPacket> callback)
	{
		if (dnsChannel == null)
		{
			synchronized (this)
			{
				if (dnsChannel == null)
				{

					dnsChannel = resolve(new DNSResolvingHandler()
					{

						@Override
						public void messageReceived(ChannelHandlerContext resolveContext, DNSPacket resolved) throws Exception
						{
							MessageCallback<DNSPacket> cb =	callbacks.remove(resolved.getId());
							if(cb != null)
							{
									cb.onComplete(resolved);
							}
						}

						@Override
						public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
						{
							cause.printStackTrace();
						}
					});
				}
			}
		}
		callbacks.put(msg.getHeader().getID(), callback);
		dnsChannel.write(new DNSPacket(Unpooled.wrappedBuffer(msg.toUnwrappedByteArray(msg.getHeader().getID())), recursiveServer));

	}

	/**
	 * Resolve.
	 * 
	 * @param handler
	 *            the handler
	 * @return the channel
	 */
	public Channel resolve(DNSResolvingHandler handler)
	{
		try
		{
			return new Bootstrap().group(new NioEventLoopGroup()).channel(DNSDatagramChannel.class).localAddress(new InetSocketAddress(0)).option(ChannelOption.SO_BROADCAST, true)
					.option(ChannelOption.SO_BROADCAST, true).option(ChannelOption.SO_REUSEADDR, true).option(ChannelOption.SO_SNDBUF, 1048576)
					.option(ChannelOption.SO_RCVBUF, 1048576).handler(handler).bind().sync().channel();
		}
		catch (InterruptedException e)
		{
			throw new IllegalStateException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.api.dns.DNSAsyncClient#update(java.lang.String, java.lang.String, java.lang.String, org.storm.api.dns.MessageCallback)
	 */
	@Override
	public void update(String zone, String domain, String ipAddress, MessageCallback<DNSPacket> cb) throws UnknownHostException
	{
		int attempts = 10;
		boolean success = false;
		while (!success && attempts-- > 0)
		{
			try
			{
				Record r = new ARecord(Name.fromString(domain), DClass.IN, 1800L, InetAddress.getByName(ipAddress));
				Message msg = Message.newUpdate(Name.fromString(zone));
				msg.addRecord(r, Section.UPDATE);
				send(msg, cb);
				success = true;
			}
			catch (TextParseException e)
			{
				throw new RuntimeException(e);
			}
			catch (UnknownHostException e)
			{
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e1)
				{

				}
			}
		}
		if (!success)
		{
			throw new UnknownHostException("Unknown host: " + domain);
		}

	}

	/* (non-Javadoc)
	 * @see org.storm.api.dns.DNSAsyncClient#question(java.lang.String, org.storm.api.dns.MessageCallback)
	 */
	@Override
	public void question(String domain, MessageCallback<DNSPacket> cb) throws UnknownHostException
	{
		Record r;
		try
		{
			r = new ARecord(Name.fromString(domain), DClass.IN, 1800L, InetAddress.getLocalHost());
			Message msg = Message.newQuery(r);
			send(msg, cb);
		}
		catch (TextParseException e)
		{
			throw new RuntimeException(e);
		}

	}

	/**
	 * Question.
	 * 
	 * @param domain
	 *            the domain
	 * @param cb
	 *            the cb
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public void question(Name domain, MessageCallback<DNSPacket> cb) throws UnknownHostException
	{
		Record r = new ARecord(domain, DClass.IN, 1800L, InetAddress.getLocalHost());
		Message msg = Message.newQuery(r);
		send(msg, cb);
	}

	/* (non-Javadoc)
	 * @see org.storm.api.dns.DNSAsyncClient#reverseLookup(java.lang.String, org.storm.api.dns.MessageCallback)
	 */
	@Override
	public void reverseLookup(String ip, MessageCallback<DNSPacket> cb) throws UnknownHostException
	{
		try
		{
			Record r = new ARecord(Name.fromString("."), DClass.IN, 1800L, InetAddress.getByName(ip));
			Message msg = Message.newReverseQuery(r);
			send(msg, cb);
		}
		catch (TextParseException e)
		{
			s_log.error(e.getMessage(), e);
		}
		
	}

}
