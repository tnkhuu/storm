/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.dns.client;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * The DNS Configurations. A enumerated singleton for easy access.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public enum DNSConfig
{
	CONFIG;

	private static Map<String, Object> systemDefault;
	static
	{
		systemDefault = new HashMap<String, Object>();
		systemDefault.put("dns_port", 53);
		systemDefault.put("dns_packet_recieve_buffer", 268435456);
		systemDefault.put("dns_packet_send_buffer", 268435456);
		systemDefault.put("dns_recursive_resolution_target", "8.8.8.8");
		systemDefault.put("dns_recursive_resolution_port", 53);
		systemDefault.put("dns_redis_server", "localhost");
		systemDefault.put("dns_redis_port", 53);

	}

	public Object get(String key)
	{

		Object value = getEnvConfig(key);

		if (value == null)
		{
			value = systemDefault.get(key);
		}
		return value;
	}

	public String getString(String key)
	{

		Object value = getEnvConfig(key);

		if (value == null)
		{
			value = systemDefault.get(key);
		}
		return value != null ? value.toString() : null;
	}

	public int getValue(String key)
	{
		return Integer.valueOf(get(key).toString());
	}

	public static int javaVersion()
	{

		try
		{
			Class.forName("java.util.concurrent.LinkedTransferQueue", false, BlockingQueue.class.getClassLoader());
			return 7;
		}
		catch (Exception e)
		{
			// Ignore
		}

		return 6;
	}

	public static String getEnvConfig(String key)
	{
		return System.getenv(key);
	}
}
