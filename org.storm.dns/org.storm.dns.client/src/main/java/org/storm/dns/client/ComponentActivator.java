/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.client;

import java.net.InetSocketAddress;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.storm.api.dns.DNSAsyncClient;
import org.storm.nexus.api.Endpoints;

/**
 * DNS Client Activator.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator extends DependencyActivatorBase implements BundleActivator
{
	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception
	{
		DNSClient client = new DNSClient(new InetSocketAddress(Endpoints.DNS_ABS.getDomain(), 53));
		manager.add(createComponent().setInterface(DNSAsyncClient.class.getName(), null)
									 .setImplementation(client));
		
	}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception
	{
		
		
	}

}
