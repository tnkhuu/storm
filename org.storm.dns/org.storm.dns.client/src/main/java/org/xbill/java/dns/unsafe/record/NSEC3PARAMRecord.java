/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Base16;
import org.xbill.java.dns.unsafe.util.Compression;

public class NSEC3PARAMRecord extends Record
{

	private static final long serialVersionUID = -8689038598776316533L;

	private int hashAlg;
	private int flags;
	private int iterations;
	private byte salt[];

	public NSEC3PARAMRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new NSEC3PARAMRecord();
	}

	/**
	 * Creates an NSEC3PARAM record from the given data.
	 * 
	 * @param name
	 *            The ownername of the NSEC3PARAM record (generally the zone
	 *            name).
	 * @param dclass
	 *            The class.
	 * @param ttl
	 *            The TTL.
	 * @param hashAlg
	 *            The hash algorithm.
	 * @param flags
	 *            The value of the flags field.
	 * @param iterations
	 *            The number of hash iterations.
	 * @param salt
	 *            The salt to use (may be null).
	 */
	public NSEC3PARAMRecord(Name name, int dclass, long ttl, int hashAlg, int flags, int iterations, byte[] salt)
	{
		super(name, Type.NSEC3PARAM, dclass, ttl);
		this.hashAlg = checkU8("hashAlg", hashAlg);
		this.flags = checkU8("flags", flags);
		this.iterations = checkU16("iterations", iterations);

		if (salt != null)
		{
			if (salt.length > 255)
			{
				throw new IllegalArgumentException("Invalid salt " + "length");
			}
			if (salt.length > 0)
			{
				this.salt = new byte[salt.length];
				System.arraycopy(salt, 0, this.salt, 0, salt.length);
			}
		}
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		hashAlg = in.readU8();
		flags = in.readU8();
		iterations = in.readU16();

		int salt_length = in.readU8();
		if (salt_length > 0)
		{
			salt = in.readByteArray(salt_length);
		}
		else
		{
			salt = null;
		}
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU8(hashAlg);
		out.writeU8(flags);
		out.writeU16(iterations);

		if (salt != null)
		{
			out.writeU8(salt.length);
			out.writeByteArray(salt);
		}
		else
		{
			out.writeU8(0);
		}
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		hashAlg = st.getUInt8();
		flags = st.getUInt8();
		iterations = st.getUInt16();

		String s = st.getString();
		if (s.equals("-"))
		{
			salt = null;
		}
		else
		{
			st.unget();
			salt = st.getHexString();
			if (salt.length > 255)
			{
				throw st.exception("salt value too long");
			}
		}
	}

	/** Converts rdata to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(hashAlg);
		sb.append(' ');
		sb.append(flags);
		sb.append(' ');
		sb.append(iterations);
		sb.append(' ');
		if (salt == null)
		{
			sb.append('-');
		}
		else
		{
			sb.append(Base16.toString(salt));
		}

		return sb.toString();
	}

	/** Returns the hash algorithm */
	public int getHashAlgorithm()
	{
		return hashAlg;
	}

	/** Returns the flags */
	public int getFlags()
	{
		return flags;
	}

	/** Returns the number of iterations */
	public int getIterations()
	{
		return iterations;
	}

	/** Returns the salt */
	public byte[] getSalt()
	{
		return salt;
	}

	/**
	 * Hashes a name with the parameters of this NSEC3PARAM record.
	 * 
	 * @param name
	 *            The name to hash
	 * @return The hashed version of the name
	 * @throws NoSuchAlgorithmException
	 *             The hash algorithm is unknown.
	 */
	public byte[] hashName(Name name) throws NoSuchAlgorithmException
	{
		return NSEC3Record.hashName(name, hashAlg, iterations, salt);
	}

}
