/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe;

import org.storm.dns.io.unsafe.UnsafeOutput;

/**
 * The DNSOutput.
 * 
 * @author Trung Khuu
 * @since 1.0
 * @deprecated - Use {@link UnsafeOutput} instead
 */
@Deprecated
public class DNSOutput implements MessageOutputBuffer
{
	private byte[] array;
	private int pos;
	private int marker;

	/**
	 * Create a new DNSOutput with a specified size.
	 * 
	 * @param size
	 *            The initial size
	 */
	public DNSOutput(int size)
	{
		array = new byte[size];
		pos = 0;
		marker = -1;
	}

	/**
	 * Create a new DNSOutput.
	 */
	public DNSOutput()
	{
		this(32);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#current()
	 */
	@Override
	public int current()
	{
		return pos;
	}

	/**
	 * Check.
	 * 
	 * @param val
	 *            the val
	 * @param bits
	 *            the bits
	 */
	private void check(long val, int bits)
	{
		long max = 1;
		max <<= bits;
		if (val < 0 || val > max)
		{
			throw new IllegalArgumentException(val + " out of range for " + bits + " bit value");
		}
	}

	/**
	 * Need.
	 * 
	 * @param n
	 *            the n
	 */
	private void need(int n)
	{
		if (array.length - pos >= n)
		{
			return;
		}
		int newsize = array.length * 2;
		if (newsize < pos + n)
		{
			newsize = pos + n;
		}
		byte[] newarray = new byte[newsize];
		System.arraycopy(array, 0, newarray, 0, pos);
		array = newarray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#jump(int)
	 */
	@Override
	public void seek(int index)
	{
		if (index > pos)
		{
			throw new IllegalArgumentException("cannot jump past " + "end of data");
		}
		pos = index;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#save()
	 */
	@Override
	public void mark()
	{
		marker = pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#restore()
	 */
	@Override
	public void restore()
	{
		if (marker < 0)
		{
			throw new IllegalStateException("no previous state");
		}
		pos = marker;
		marker = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeU8(int)
	 */
	@Override
	public void writeU8(int val)
	{
		check(val, 8);
		need(1);
		array[pos++] = (byte) (val & 0xFF);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeU16(int)
	 */
	@Override
	public void writeU16(int val)
	{
		check(val, 16);
		need(2);
		array[pos++] = (byte) (val >>> 8 & 0xFF);
		array[pos++] = (byte) (val & 0xFF);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeU16At(int, int)
	 */
	@Override
	public void writeU16At(int val, int where)
	{
		check(val, 16);
		if (where > pos - 2)
		{
			throw new IllegalArgumentException("cannot write past " + "end of data");
		}
		array[where++] = (byte) (val >>> 8 & 0xFF);
		array[where++] = (byte) (val & 0xFF);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeU32(long)
	 */
	@Override
	public void writeU32(long val)
	{
		check(val, 32);
		need(4);
		array[pos++] = (byte) (val >>> 24 & 0xFF);
		array[pos++] = (byte) (val >>> 16 & 0xFF);
		array[pos++] = (byte) (val >>> 8 & 0xFF);
		array[pos++] = (byte) (val & 0xFF);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeByteArray(byte[], int,
	 * int)
	 */
	@Override
	public void writeByteArray(byte[] b, int off, int len)
	{
		need(len);
		System.arraycopy(b, off, array, pos, len);
		pos += len;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeByteArray(byte[])
	 */
	@Override
	public void writeByteArray(byte[] b)
	{
		writeByteArray(b, 0, b.length);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#writeCountedString(byte[])
	 */
	@Override
	public void writeCountedString(byte[] s)
	{
		if (s.length > 0xFF)
		{
			throw new IllegalArgumentException("Invalid counted string");
		}
		need(1 + s.length);
		array[pos++] = (byte) (s.length & 0xFF);
		writeByteArray(s, 0, s.length);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageOutputBuffer#toByteArray()
	 */
	@Override
	public byte[] toByteArray()
	{
		byte[] out = new byte[pos];
		System.arraycopy(array, 0, out, 0, pos);
		return out;
	}

}
