/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Base16;
import org.xbill.java.dns.unsafe.util.Compression;

public class NSAPRecord extends Record
{

	private static final long serialVersionUID = -1037209403185658593L;

	private byte[] address;

	public NSAPRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new NSAPRecord();
	}

	private static final byte[] checkAndConvertAddress(String address)
	{
		if (!address.substring(0, 2).equalsIgnoreCase("0x"))
		{
			return null;
		}
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		boolean partial = false;
		int current = 0;
		for (int i = 2; i < address.length(); i++)
		{
			char c = address.charAt(i);
			if (c == '.')
			{
				continue;
			}
			int value = Character.digit(c, 16);
			if (value == -1)
			{
				return null;
			}
			if (partial)
			{
				current += value;
				bytes.write(current);
				partial = false;
			}
			else
			{
				current = value << 4;
				partial = true;
			}

		}
		if (partial)
		{
			return null;
		}
		return bytes.toByteArray();
	}

	/**
	 * Creates an NSAP Record from the given data
	 * 
	 * @param address
	 *            The NSAP address.
	 * @throws IllegalArgumentException
	 *             The address is not a valid NSAP address.
	 */
	public NSAPRecord(Name name, int dclass, long ttl, String address)
	{
		super(name, Type.NSAP, dclass, ttl);
		this.address = checkAndConvertAddress(address);
		if (this.address == null)
		{
			throw new IllegalArgumentException("invalid NSAP address " + address);
		}
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		address = in.readByteArray();
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		String addr = st.getString();
		this.address = checkAndConvertAddress(addr);
		if (this.address == null)
		{
			throw st.exception("invalid NSAP address " + addr);
		}
	}

	/**
	 * Returns the NSAP address.
	 */
	public String getAddress()
	{
		return byteArrayToString(address, false);
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeByteArray(address);
	}

	@Override
	public String rrToString()
	{
		return "0x" + Base16.toString(address);
	}

}
