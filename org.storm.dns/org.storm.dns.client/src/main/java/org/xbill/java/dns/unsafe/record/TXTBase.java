/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.util.Compression;

/**
 * Implements common functionality for the many record types whose format is a
 * list of strings.
 * 
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class TXTBase extends Record
{

	private static final long serialVersionUID = -4319510507246305931L;

	protected List strings;

	protected TXTBase()
	{
	}

	protected TXTBase(Name name, int type, int dclass, long ttl)
	{
		super(name, type, dclass, ttl);
	}

	protected TXTBase(Name name, int type, int dclass, long ttl, List strings)
	{
		super(name, type, dclass, ttl);
		if (strings == null)
		{
			throw new IllegalArgumentException("strings must not be null");
		}
		this.strings = new ArrayList(strings.size());
		Iterator it = strings.iterator();
		try
		{
			while (it.hasNext())
			{
				String s = (String) it.next();
				this.strings.add(byteArrayFromString(s));
			}
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	protected TXTBase(Name name, int type, int dclass, long ttl, String string)
	{
		this(name, type, dclass, ttl, Collections.singletonList(string));
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		strings = new ArrayList(2);
		while (in.remaining() > 0)
		{
			byte[] b = in.readCountedString();
			strings.add(b);
		}
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		strings = new ArrayList(2);
		while (true)
		{
			Tokenizer.Token t = st.get();
			if (!t.isString())
			{
				break;
			}
			try
			{
				strings.add(byteArrayFromString(t.value));
			}
			catch (TextParseException e)
			{
				throw st.exception(e.getMessage());
			}

		}
		st.unget();
	}

	/** converts to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		Iterator it = strings.iterator();
		while (it.hasNext())
		{
			byte[] array = (byte[]) it.next();
			sb.append(byteArrayToString(array, true));
			if (it.hasNext())
			{
				sb.append(" ");
			}
		}
		return sb.toString();
	}

	/**
	 * Returns the text strings
	 * 
	 * @return A list of Strings corresponding to the text strings.
	 */
	public List getStrings()
	{
		List list = new ArrayList(strings.size());
		for (int i = 0; i < strings.size(); i++)
		{
			list.add(byteArrayToString((byte[]) strings.get(i), false));
		}
		return list;
	}

	/**
	 * Returns the text strings
	 * 
	 * @return A list of byte arrays corresponding to the text strings.
	 */
	public List getStringsAsByteArrays()
	{
		return strings;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		Iterator it = strings.iterator();
		while (it.hasNext())
		{
			byte[] b = (byte[]) it.next();
			out.writeCountedString(b);
		}
	}

}
