/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

import org.xbill.java.dns.unsafe.util.Hexdump;

public class Client
{

	protected long endTime;
	protected SelectionKey key;

	public Client(SelectableChannel channel, long endTime) throws IOException
	{
		boolean done = false;
		Selector selector = null;
		this.endTime = endTime;
		try
		{
			selector = Selector.open();
			channel.configureBlocking(false);
			key = channel.register(selector, SelectionKey.OP_READ);
			done = true;
		}
		finally
		{
			if (!done && selector != null)
			{
				selector.close();
			}
			if (!done)
			{
				channel.close();
			}
		}
	}

	static public void blockUntil(SelectionKey key, long endTime) throws IOException
	{
		long timeout = endTime - System.currentTimeMillis();
		int nkeys = 0;
		if (timeout > 0)
		{
			nkeys = key.selector().select(timeout);
		}
		else if (timeout == 0)
		{
			nkeys = key.selector().selectNow();
		}
		if (nkeys == 0)
		{
			throw new SocketTimeoutException();
		}
	}

	static public void verboseLog(String prefix, byte[] data)
	{
		if (Options.check("verbosemsg"))
		{
			System.err.println(Hexdump.dump(prefix, data));
		}
	}

	public void cleanup() throws IOException
	{
		key.selector().close();
		key.channel().close();
	}

}
