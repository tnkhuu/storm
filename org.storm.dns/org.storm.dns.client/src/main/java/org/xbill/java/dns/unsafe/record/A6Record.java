/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Address;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class A6Record extends Record
{

	private static final long serialVersionUID = -8815026887337346789L;

	private int prefixBits;
	private InetAddress suffix;
	private Name prefix;

	public A6Record()
	{
	}

	@Override
	public Record getObject()
	{
		return new A6Record();
	}

	/**
	 * Creates an A6 Record from the given data
	 * 
	 * @param prefixBits
	 *            The number of bits in the address prefix
	 * @param suffix
	 *            The address suffix
	 * @param prefix
	 *            The name of the prefix
	 */
	public A6Record(Name name, int dclass, long ttl, int prefixBits, InetAddress suffix, Name prefix)
	{
		super(name, Type.A6, dclass, ttl);
		this.prefixBits = checkU8("prefixBits", prefixBits);
		if (suffix != null && Address.familyOf(suffix) != Address.IPv6)
		{
			throw new IllegalArgumentException("invalid IPv6 address");
		}
		this.suffix = suffix;
		if (prefix != null)
		{
			this.prefix = checkName("prefix", prefix);
		}
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		prefixBits = in.readU8();
		int suffixbits = 128 - prefixBits;
		int suffixbytes = (suffixbits + 7) / 8;
		if (prefixBits < 128)
		{
			byte[] bytes = new byte[16];
			in.readByteArray(bytes, 16 - suffixbytes, suffixbytes);
			suffix = InetAddress.getByAddress(bytes);
		}
		if (prefixBits > 0)
		{
			prefix = new Name(in);
		}
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		prefixBits = st.getUInt8();
		if (prefixBits > 128)
		{
			throw st.exception("prefix bits must be [0..128]");
		}
		else if (prefixBits < 128)
		{
			String s = st.getString();
			try
			{
				suffix = Address.getByAddress(s, Address.IPv6);
			}
			catch (UnknownHostException e)
			{
				throw st.exception("invalid IPv6 address: " + s);
			}
		}
		if (prefixBits > 0)
		{
			prefix = st.getName(origin);
		}
	}

	/** Converts rdata to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(prefixBits);
		if (suffix != null)
		{
			sb.append(" ");
			sb.append(suffix.getHostAddress());
		}
		if (prefix != null)
		{
			sb.append(" ");
			sb.append(prefix);
		}
		return sb.toString();
	}

	/** Returns the number of bits in the prefix */
	public int getPrefixBits()
	{
		return prefixBits;
	}

	/** Returns the address suffix */
	public InetAddress getSuffix()
	{
		return suffix;
	}

	/** Returns the address prefix */
	public Name getPrefix()
	{
		return prefix;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU8(prefixBits);
		if (suffix != null)
		{
			int suffixbits = 128 - prefixBits;
			int suffixbytes = (suffixbits + 7) / 8;
			byte[] data = suffix.getAddress();
			out.writeByteArray(data, 16 - suffixbytes, suffixbytes);
		}
		if (prefix != null)
		{
			prefix.toWire(out, null, canonical);
		}
	}

}
