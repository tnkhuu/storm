/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;

public class DNAMERecord extends SingleNameBase
{

	private static final long serialVersionUID = 2670767677200844154L;

	public DNAMERecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new DNAMERecord();
	}

	/**
	 * Creates a new DNAMERecord with the given data
	 * 
	 * @param alias
	 *            The name to which the DNAME alias points
	 */
	public DNAMERecord(Name name, int dclass, long ttl, Name alias)
	{
		super(name, Type.DNAME, dclass, ttl, alias, "alias");
	}

	/**
	 * Gets the target of the DNAME Record
	 */
	public Name getTarget()
	{
		return getSingleName();
	}

	/** Gets the alias specified by the DNAME Record */
	public Name getAlias()
	{
		return getSingleName();
	}

}
