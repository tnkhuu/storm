/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class SRVRecord extends Record
{

	private static final long serialVersionUID = -3886460132387522052L;

	private int priority, weight, port;
	private Name target;

	public SRVRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new SRVRecord();
	}

	/**
	 * Creates an SRV Record from the given data
	 * 
	 * @param priority
	 *            The priority of this SRV. Records with lower priority are
	 *            preferred.
	 * @param weight
	 *            The weight, used to select between records at the same
	 *            priority.
	 * @param port
	 *            The TCP/UDP port that the service uses
	 * @param target
	 *            The host running the service
	 */
	public SRVRecord(Name name, int dclass, long ttl, int priority, int weight, int port, Name target)
	{
		super(name, Type.SRV, dclass, ttl);
		this.priority = checkU16("priority", priority);
		this.weight = checkU16("weight", weight);
		this.port = checkU16("port", port);
		this.target = checkName("target", target);
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		priority = in.readU16();
		weight = in.readU16();
		port = in.readU16();
		target = new Name(in);
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		priority = st.getUInt16();
		weight = st.getUInt16();
		port = st.getUInt16();
		target = st.getName(origin);
	}

	/** Converts rdata to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(priority + " ");
		sb.append(weight + " ");
		sb.append(port + " ");
		sb.append(target);
		return sb.toString();
	}

	/** Returns the priority */
	public int getPriority()
	{
		return priority;
	}

	/** Returns the weight */
	public int getWeight()
	{
		return weight;
	}

	/** Returns the port that the service runs on */
	public int getPort()
	{
		return port;
	}

	/** Returns the host running that the service */
	public Name getTarget()
	{
		return target;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU16(priority);
		out.writeU16(weight);
		out.writeU16(port);
		target.toWire(out, null, canonical);
	}

	@Override
	public Name getAdditionalName()
	{
		return target;
	}

}
