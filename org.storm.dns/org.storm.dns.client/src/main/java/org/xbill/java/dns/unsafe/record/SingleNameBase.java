/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.util.Compression;

/**
 * Implements common functionality for the many record types whose format is a
 * single name.
 * 
 */

public abstract class SingleNameBase extends Record
{

	private static final long serialVersionUID = -18595042501413L;

	protected Name singleName;

	public SingleNameBase()
	{
	}

	public SingleNameBase(Name name, int type, int dclass, long ttl)
	{
		super(name, type, dclass, ttl);
	}

	public SingleNameBase(Name name, int type, int dclass, long ttl, Name singleName, String description)
	{
		super(name, type, dclass, ttl);
		this.singleName = checkName(description, singleName);
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		singleName = new Name(in);
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		singleName = st.getName(origin);
	}

	@Override
	public String rrToString()
	{
		return singleName.toString();
	}

	public Name getSingleName()
	{
		return singleName;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		singleName.toWire(out, null, canonical);
	}

}
