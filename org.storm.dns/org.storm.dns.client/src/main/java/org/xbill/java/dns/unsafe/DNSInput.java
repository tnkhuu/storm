/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe;

import org.storm.dns.io.unsafe.UnsafeInput;

/**
 * The DNSInput.
 * 
 * @author Trung Khuu
 * @since 1.0
 * @deprecated - use {@link UnsafeInput} instead
 */
@Deprecated
public class DNSInput implements MessageInputBuffer
{
	private byte[] array;
	private int pos;
	private int end;
	private int markerStart;
	private int markerEnd;

	/**
	 * Creates a new DNSInput.
	 * 
	 * @param input
	 *            The byte array to read from
	 */
	public DNSInput(byte[] input)
	{
		array = input;
		pos = 0;
		end = array.length;
		markerStart = -1;
		markerEnd = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#current()
	 */
	@Override
	public int readerPos()
	{
		return pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#remaining()
	 */
	@Override
	public int remaining()
	{
		return end - pos;
	}

	/**
	 * Require.
	 * 
	 * @param n
	 *            the n
	 * @throws WireParseException
	 *             the wire parse exception
	 */
	private void require(int n) throws WireParseException
	{
		if (n > remaining())
		{
			throw new WireParseException("end of input");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#setActive(int)
	 */
	@Override
	public void setActive(int len)
	{
		if (len > array.length - pos)
		{
			throw new IllegalArgumentException("cannot set active " + "region past end of input");
		}
		end = pos + len;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#clearActive()
	 */
	@Override
	public void clearActive()
	{
		end = array.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#saveActive()
	 */
	@Override
	public int saveActive()
	{
		return end;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#restoreActive(int)
	 */
	@Override
	public void restoreActive(int pos)
	{
		if (pos > array.length)
		{
			throw new IllegalArgumentException("cannot set active " + "region past end of input");
		}
		end = pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#jump(int)
	 */
	@Override
	public void seek(int index)
	{
		if (index >= array.length)
		{
			throw new IllegalArgumentException("cannot jump past " + "end of input");
		}
		pos = index;
		end = array.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#save()
	 */
	@Override
	public void save()
	{
		markerStart = pos;
		markerEnd = end;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#restore()
	 */
	@Override
	public void restore()
	{
		if (markerStart < 0)
		{
			throw new IllegalStateException("no previous state");
		}
		pos = markerStart;
		end = markerEnd;
		markerStart = -1;
		markerEnd = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#readU8()
	 */
	@Override
	public int readU8() throws WireParseException
	{
		require(1);
		return array[pos++] & 0xFF;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#readU16()
	 */
	@Override
	public int readU16() throws WireParseException
	{
		require(2);
		int b1 = array[pos++] & 0xFF;
		int b2 = array[pos++] & 0xFF;
		return (b1 << 8) + b2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#readU32()
	 */
	@Override
	public long readU32() throws WireParseException
	{
		require(4);
		int b1 = array[pos++] & 0xFF;
		int b2 = array[pos++] & 0xFF;
		int b3 = array[pos++] & 0xFF;
		int b4 = array[pos++] & 0xFF;
		return ((long) b1 << 24) + (b2 << 16) + (b3 << 8) + b4;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#readByteArray(byte[], int, int)
	 */
	@Override
	public void readByteArray(byte[] b, int off, int len) throws WireParseException
	{
		require(len);
		System.arraycopy(array, pos, b, off, len);
		pos += len;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#readByteArray(int)
	 */
	@Override
	public byte[] readByteArray(int len) throws WireParseException
	{
		require(len);
		byte[] out = new byte[len];
		System.arraycopy(array, pos, out, 0, len);
		pos += len;
		return out;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#readByteArray()
	 */
	@Override
	public byte[] readByteArray()
	{
		int len = remaining();
		byte[] out = new byte[len];
		System.arraycopy(array, pos, out, 0, len);
		pos += len;
		return out;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.io.MessageInputBuffer#readCountedString()
	 */
	@Override
	public byte[] readCountedString() throws WireParseException
	{
		require(1);
		int len = array[pos++] & 0xFF;
		return readByteArray(len);
	}

}
