/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;

public class MFRecord extends SingleNameBase
{

	private static final long serialVersionUID = -6670449036843028169L;

	public MFRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new MFRecord();
	}

	/**
	 * Creates a new MF Record with the given data
	 * 
	 * @param mailAgent
	 *            The mail agent that forwards mail for the domain.
	 */
	public MFRecord(Name name, int dclass, long ttl, Name mailAgent)
	{
		super(name, Type.MF, dclass, ttl, mailAgent, "mail agent");
	}

	/** Gets the mail agent for the domain */
	public Name getMailAgent()
	{
		return getSingleName();
	}

	@Override
	public Name getAdditionalName()
	{
		return getSingleName();
	}

}
