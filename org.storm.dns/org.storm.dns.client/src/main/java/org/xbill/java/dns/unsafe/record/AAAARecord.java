/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;
import java.net.InetAddress;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Address;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

/**
 * IPv6 Address Record - maps a domain name to an IPv6 address
 * 
 */

public class AAAARecord extends Record
{

	private static final long serialVersionUID = -4588601512069748050L;

	private InetAddress address;

	public AAAARecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new AAAARecord();
	}

	/**
	 * Creates an AAAA Record from the given data
	 * 
	 * @param address
	 *            The address suffix
	 */
	public AAAARecord(Name name, int dclass, long ttl, InetAddress address)
	{
		super(name, Type.AAAA, dclass, ttl);
		if (Address.familyOf(address) != Address.IPv6)
		{
			throw new IllegalArgumentException("invalid IPv6 address");
		}
		this.address = address;
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		address = InetAddress.getByAddress(in.readByteArray(16));
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		address = st.getAddress(Address.IPv6);
	}

	/** Converts rdata to a String */
	@Override
	public String rrToString()
	{
		return address.getHostAddress();
	}

	/** Returns the address */
	public InetAddress getAddress()
	{
		return address;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeByteArray(address.getAddress());
	}

}
