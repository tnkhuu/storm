/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

import org.xbill.java.dns.unsafe.util.Mnemonic;

public final class Flags
{

	private static Mnemonic flags = new Mnemonic("DNS Header Flag", Mnemonic.CASE_LOWER);

	/** query/response */
	public static final byte QR = 0;

	/** authoritative answer */
	public static final byte AA = 5;

	/** truncated */
	public static final byte TC = 6;

	/** recursion desired */
	public static final byte RD = 7;

	/** recursion available */
	public static final byte RA = 8;

	/** authenticated data */
	public static final byte AD = 10;

	/** (security) checking disabled */
	public static final byte CD = 11;

	/** dnssec ok (extended) */
	public static final int DO = ExtendedFlags.DO;

	static
	{
		flags.setMaximum(0xF);
		flags.setPrefix("FLAG");
		flags.setNumericAllowed(true);

		flags.add(QR, "qr");
		flags.add(AA, "aa");
		flags.add(TC, "tc");
		flags.add(RD, "rd");
		flags.add(RA, "ra");
		flags.add(AD, "ad");
		flags.add(CD, "cd");
	}

	private Flags()
	{
	}

	/** Converts a numeric Flag into a String */
	public static String string(int i)
	{
		return flags.getText(i);
	}

	/** Converts a String representation of an Flag into its numeric value */
	public static int value(String s)
	{
		return flags.getValue(s);
	}

	/**
	 * Indicates if a bit in the flags field is a flag or not. If it's part of
	 * the rcode or opcode, it's not.
	 */
	public static boolean isFlag(int index)
	{
		flags.check(index);
		if (index >= 1 && index <= 4 || index >= 12)
		{
			return false;
		}
		return true;
	}

}
