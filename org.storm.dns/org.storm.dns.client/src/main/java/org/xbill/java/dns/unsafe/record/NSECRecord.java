/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.protocol.TypeBitmap;
import org.xbill.java.dns.unsafe.util.Compression;

public class NSECRecord extends Record
{

	private static final long serialVersionUID = -5165065768816265385L;

	private Name next;
	private TypeBitmap types;

	public NSECRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new NSECRecord();
	}

	/**
	 * Creates an NSEC Record from the given data.
	 * 
	 * @param next
	 *            The following name in an ordered list of the zone
	 * @param types
	 *            An array containing the types present.
	 */
	public NSECRecord(Name name, int dclass, long ttl, Name next, int[] types)
	{
		super(name, Type.NSEC, dclass, ttl);
		this.next = checkName("next", next);
		for (int i = 0; i < types.length; i++)
		{
			Type.check(types[i]);
		}
		this.types = new TypeBitmap(types);
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		next = new Name(in);
		types = new TypeBitmap(in);
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		// Note: The next name is not lowercased.
		next.toWire(out, null, false);
		types.toWire(out);
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		next = st.getName(origin);
		types = new TypeBitmap(st);
	}

	/** Converts rdata to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(next);
		if (!types.empty())
		{
			sb.append(' ');
			sb.append(types.toString());
		}
		return sb.toString();
	}

	/** Returns the next name */
	public Name getNext()
	{
		return next;
	}

	/** Returns the set of types defined for this name */
	public int[] getTypes()
	{
		return types.toArray();
	}

	/** Returns whether a specific type is in the set of types. */
	public boolean hasType(int type)
	{
		return types.contains(type);
	}

}
