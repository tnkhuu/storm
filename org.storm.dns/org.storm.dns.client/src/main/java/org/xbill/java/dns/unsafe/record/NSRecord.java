/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;

/**
 * Name Server Record - contains the name server serving the named zone
 * 
 */

public class NSRecord extends SingleCompressedNameBase
{

	private static final long serialVersionUID = 487170758138268838L;

	public NSRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new NSRecord();
	}

	/**
	 * Creates a new NS Record with the given data
	 * 
	 * @param target
	 *            The name server for the given domain
	 */
	public NSRecord(Name name, int dclass, long ttl, Name target)
	{
		super(name, Type.NS, dclass, ttl, target, "target");
	}

	/** Gets the target of the NS Record */
	public Name getTarget()
	{
		return getSingleName();
	}

	@Override
	public Name getAdditionalName()
	{
		return getSingleName();
	}

}
