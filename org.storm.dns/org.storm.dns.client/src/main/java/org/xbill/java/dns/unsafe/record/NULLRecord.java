/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class NULLRecord extends Record
{

	private static final long serialVersionUID = -5796493183235216538L;

	private byte[] data;

	public NULLRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new NULLRecord();
	}

	/**
	 * Creates a NULL record from the given data.
	 * 
	 * @param data
	 *            The contents of the record.
	 */
	public NULLRecord(Name name, int dclass, long ttl, byte[] data)
	{
		super(name, Type.NULL, dclass, ttl);

		if (data.length > 0xFFFF)
		{
			throw new IllegalArgumentException("data must be <65536 bytes");
		}
		this.data = data;
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		data = in.readByteArray();
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		throw st.exception("no defined text format for NULL records");
	}

	@Override
	public String rrToString()
	{
		return unknownToString(data);
	}

	/** Returns the contents of this record. */
	public byte[] getData()
	{
		return data;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeByteArray(data);
	}

}
