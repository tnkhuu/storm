/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class PXRecord extends Record
{

	private static final long serialVersionUID = 1811540008806660667L;

	private int preference;
	private Name map822;
	private Name mapX400;

	public PXRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new PXRecord();
	}

	/**
	 * Creates an PX Record from the given data
	 * 
	 * @param preference
	 *            The preference of this mail address.
	 * @param map822
	 *            The RFC 822 component of the mail address.
	 * @param mapX400
	 *            The X.400 component of the mail address.
	 */
	public PXRecord(Name name, int dclass, long ttl, int preference, Name map822, Name mapX400)
	{
		super(name, Type.PX, dclass, ttl);

		this.preference = checkU16("preference", preference);
		this.map822 = checkName("map822", map822);
		this.mapX400 = checkName("mapX400", mapX400);
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		preference = in.readU16();
		map822 = new Name(in);
		mapX400 = new Name(in);
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		preference = st.getUInt16();
		map822 = st.getName(origin);
		mapX400 = st.getName(origin);
	}

	/** Converts the PX Record to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(preference);
		sb.append(" ");
		sb.append(map822);
		sb.append(" ");
		sb.append(mapX400);
		return sb.toString();
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU16(preference);
		map822.toWire(out, null, canonical);
		mapX400.toWire(out, null, canonical);
	}

	/** Gets the preference of the route. */
	public int getPreference()
	{
		return preference;
	}

	/** Gets the RFC 822 component of the mail address. */
	public Name getMap822()
	{
		return map822;
	}

	/** Gets the X.400 component of the mail address. */
	public Name getMapX400()
	{
		return mapX400;
	}

}
