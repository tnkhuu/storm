/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;
import java.util.BitSet;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class NXTRecord extends Record
{

	private static final long serialVersionUID = -8851454400765507520L;

	private Name next;
	private BitSet bitmap;

	public NXTRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new NXTRecord();
	}

	/**
	 * Creates an NXT Record from the given data
	 * 
	 * @param next
	 *            The following name in an ordered list of the zone
	 * @param bitmap
	 *            The set of type for which records exist at this name
	 */
	public NXTRecord(Name name, int dclass, long ttl, Name next, BitSet bitmap)
	{
		super(name, Type.NXT, dclass, ttl);
		this.next = checkName("next", next);
		this.bitmap = bitmap;
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		next = new Name(in);
		bitmap = new BitSet();
		int bitmapLength = in.remaining();
		for (int i = 0; i < bitmapLength; i++)
		{
			int t = in.readU8();
			for (int j = 0; j < 8; j++)
			{
				if ((t & 1 << 7 - j) != 0)
				{
					bitmap.set(i * 8 + j);
				}
			}
		}
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		next = st.getName(origin);
		bitmap = new BitSet();
		while (true)
		{
			Tokenizer.Token t = st.get();
			if (!t.isString())
			{
				break;
			}
			int typecode = Type.value(t.value, true);
			if (typecode <= 0 || typecode > 128)
			{
				throw st.exception("Invalid type: " + t.value);
			}
			bitmap.set(typecode);
		}
		st.unget();
	}

	/** Converts rdata to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(next);
		int length = bitmap.length();
		for (short i = 0; i < length; i++)
		{
			if (bitmap.get(i))
			{
				sb.append(" ");
				sb.append(Type.string(i));
			}
		}
		return sb.toString();
	}

	/** Returns the next name */
	public Name getNext()
	{
		return next;
	}

	/** Returns the set of types defined for this name */
	public BitSet getBitmap()
	{
		return bitmap;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		next.toWire(out, null, canonical);
		int length = bitmap.length();
		for (int i = 0, t = 0; i < length; i++)
		{
			t |= bitmap.get(i) ? 1 << 7 - i % 8 : 0;
			if (i % 8 == 7 || i == length - 1)
			{
				out.writeU8(t);
				t = 0;
			}
		}
	}

}
