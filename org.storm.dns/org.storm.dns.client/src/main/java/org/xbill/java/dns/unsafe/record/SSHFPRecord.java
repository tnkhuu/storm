/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Base64;
import org.xbill.java.dns.unsafe.util.Compression;

public class SSHFPRecord extends Record
{

	private static final long serialVersionUID = -8104701402654687025L;

	public static class Algorithm
	{
		private Algorithm()
		{
		}

		public static final int RSA = 1;
		public static final int DSS = 2;
	}

	public static class Digest
	{
		private Digest()
		{
		}

		public static final int SHA1 = 1;
	}

	private int alg;
	private int digestType;
	private byte[] fingerprint;

	public SSHFPRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new SSHFPRecord();
	}

	/**
	 * Creates an SSHFP Record from the given data.
	 * 
	 * @param alg
	 *            The public key's algorithm.
	 * @param digestType
	 *            The public key's digest type.
	 * @param fingerprint
	 *            The public key's fingerprint.
	 */
	public SSHFPRecord(Name name, int dclass, long ttl, int alg, int digestType, byte[] fingerprint)
	{
		super(name, Type.SSHFP, dclass, ttl);
		this.alg = checkU8("alg", alg);
		this.digestType = checkU8("digestType", digestType);
		this.fingerprint = fingerprint;
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		alg = in.readU8();
		digestType = in.readU8();
		fingerprint = in.readByteArray();
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		alg = st.getUInt8();
		digestType = st.getUInt8();
		fingerprint = st.getHex(true);
	}

	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(alg);
		sb.append(" ");
		sb.append(digestType);
		sb.append(" ");
		sb.append(Base64.toString(fingerprint));
		return sb.toString();
	}

	/** Returns the public key's algorithm. */
	public int getAlgorithm()
	{
		return alg;
	}

	/** Returns the public key's digest type. */
	public int getDigestType()
	{
		return digestType;
	}

	/** Returns the fingerprint */
	public byte[] getFingerPrint()
	{
		return fingerprint;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU8(alg);
		out.writeU8(digestType);
		out.writeByteArray(fingerprint);
	}

}
