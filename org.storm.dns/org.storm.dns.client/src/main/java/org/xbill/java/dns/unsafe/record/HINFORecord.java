/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class HINFORecord extends Record
{

	private static final long serialVersionUID = -4732870630947452112L;

	private byte[] cpu, os;

	public HINFORecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new HINFORecord();
	}

	/**
	 * Creates an HINFO Record from the given data
	 * 
	 * @param cpu
	 *            A string describing the host's CPU
	 * @param os
	 *            A string describing the host's OS
	 * @throws IllegalArgumentException
	 *             One of the strings has invalid escapes
	 */
	public HINFORecord(Name name, int dclass, long ttl, String cpu, String os)
	{
		super(name, Type.HINFO, dclass, ttl);
		try
		{
			this.cpu = byteArrayFromString(cpu);
			this.os = byteArrayFromString(os);
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		cpu = in.readCountedString();
		os = in.readCountedString();
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		try
		{
			cpu = byteArrayFromString(st.getString());
			os = byteArrayFromString(st.getString());
		}
		catch (TextParseException e)
		{
			throw st.exception(e.getMessage());
		}
	}

	/**
	 * Returns the host's CPU
	 */
	public String getCPU()
	{
		return byteArrayToString(cpu, false);
	}

	/**
	 * Returns the host's OS
	 */
	public String getOS()
	{
		return byteArrayToString(os, false);
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeCountedString(cpu);
		out.writeCountedString(os);
	}

	/**
	 * Converts to a string
	 */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(byteArrayToString(cpu, true));
		sb.append(" ");
		sb.append(byteArrayToString(os, true));
		return sb.toString();
	}

}
