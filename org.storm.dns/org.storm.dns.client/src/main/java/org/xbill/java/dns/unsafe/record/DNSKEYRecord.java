/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;
import java.security.PublicKey;

import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.DNSSec;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;

public class DNSKEYRecord extends KEYBase
{

	public static class Protocol
	{
		private Protocol()
		{
		}

		/** Key will be used for DNSSec */
		public static final int DNSSEC = 3;
	}

	public static class Flags
	{
		private Flags()
		{
		}

		/** Key is a zone key */
		public static final int ZONE_KEY = 0x100;

		/** Key is a secure entry point key */
		public static final int SEP_KEY = 0x1;

		/** Key has been revoked */
		public static final int REVOKE = 0x80;
	}

	private static final long serialVersionUID = -8679800040426675002L;

	public DNSKEYRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new DNSKEYRecord();
	}

	/**
	 * Creates a DNSKEY Record from the given data
	 * 
	 * @param flags
	 *            Flags describing the key's properties
	 * @param proto
	 *            The protocol that the key was created for
	 * @param alg
	 *            The key's algorithm
	 * @param key
	 *            Binary representation of the key
	 */
	public DNSKEYRecord(Name name, int dclass, long ttl, int flags, int proto, int alg, byte[] key)
	{
		super(name, Type.DNSKEY, dclass, ttl, flags, proto, alg, key);
	}

	/**
	 * Creates a DNSKEY Record from the given data
	 * 
	 * @param flags
	 *            Flags describing the key's properties
	 * @param proto
	 *            The protocol that the key was created for
	 * @param alg
	 *            The key's algorithm
	 * @param key
	 *            The key as a PublicKey
	 * @throws DNSSec.DNSSECException
	 *             The PublicKey could not be converted into DNS format.
	 */
	public DNSKEYRecord(Name name, int dclass, long ttl, int flags, int proto, int alg, PublicKey key) throws DNSSec.DNSSECException
	{
		super(name, Type.DNSKEY, dclass, ttl, flags, proto, alg, DNSSec.fromPublicKey(key, alg));
		publicKey = key;
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		flags = st.getUInt16();
		proto = st.getUInt8();
		String algString = st.getString();
		alg = DNSSec.Algorithm.value(algString);
		if (alg < 0)
		{
			throw st.exception("Invalid algorithm: " + algString);
		}
		key = st.getBase64();
	}

}
