/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class RPRecord extends Record
{

	private static final long serialVersionUID = 8124584364211337460L;

	private Name mailbox;
	private Name textDomain;

	public RPRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new RPRecord();
	}

	/**
	 * Creates an RP Record from the given data
	 * 
	 * @param mailbox
	 *            The responsible person
	 * @param textDomain
	 *            The address where TXT records can be found
	 */
	public RPRecord(Name name, int dclass, long ttl, Name mailbox, Name textDomain)
	{
		super(name, Type.RP, dclass, ttl);

		this.mailbox = checkName("mailbox", mailbox);
		this.textDomain = checkName("textDomain", textDomain);
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		mailbox = new Name(in);
		textDomain = new Name(in);
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		mailbox = st.getName(origin);
		textDomain = st.getName(origin);
	}

	/** Converts the RP Record to a String */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(mailbox);
		sb.append(" ");
		sb.append(textDomain);
		return sb.toString();
	}

	/** Gets the mailbox address of the RP Record */
	public Name getMailbox()
	{
		return mailbox;
	}

	/** Gets the text domain info of the RP Record */
	public Name getTextDomain()
	{
		return textDomain;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		mailbox.toWire(out, null, canonical);
		textDomain.toWire(out, null, canonical);
	}

}
