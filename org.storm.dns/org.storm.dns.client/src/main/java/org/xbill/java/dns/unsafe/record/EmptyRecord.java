/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.util.Compression;

/**
 * A class implementing Records with no data; that is, records used in the
 * question section of messages and meta-records in dynamic update.
 * 
 */

class EmptyRecord extends Record
{

	private static final long serialVersionUID = 3601852050646429582L;

	EmptyRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new EmptyRecord();
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
	}

	@Override
	public String rrToString()
	{
		return "";
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
	}

}
