/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.record.Record;
import org.xbill.java.dns.unsafe.util.Compression;

/**
 * Implements common functionality for the many record types whose format is an
 * unsigned 16 bit integer followed by a name.
 * 
 */

public abstract class U16NameBase extends Record
{

	private static final long serialVersionUID = -8315884183112502995L;

	protected int u16Field;
	protected Name nameField;

	public U16NameBase()
	{
	}

	public U16NameBase(Name name, int type, int dclass, long ttl)
	{
		super(name, type, dclass, ttl);
	}

	public U16NameBase(Name name, int type, int dclass, long ttl, int u16Field, String u16Description, Name nameField, String nameDescription)
	{
		super(name, type, dclass, ttl);
		this.u16Field = checkU16(u16Description, u16Field);
		this.nameField = checkName(nameDescription, nameField);
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		u16Field = in.readU16();
		nameField = new Name(in);
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		u16Field = st.getUInt16();
		nameField = st.getName(origin);
	}

	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(u16Field);
		sb.append(" ");
		sb.append(nameField);
		return sb.toString();
	}

	public int getU16Field()
	{
		return u16Field;
	}

	public Name getNameField()
	{
		return nameField;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU16(u16Field);
		nameField.toWire(out, null, canonical);
	}

}
