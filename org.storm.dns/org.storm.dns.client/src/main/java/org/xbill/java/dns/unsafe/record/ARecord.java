/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Address;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

/**
 * Address Record - maps a domain name to an Internet address
 * 
 */
public class ARecord extends Record
{

	private static final long serialVersionUID = -2172609200849142323L;

	private int addr;

	public ARecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new ARecord();
	}

	private static final int fromArray(byte[] array)
	{
		return (array[0] & 0xFF) << 24 | (array[1] & 0xFF) << 16 | (array[2] & 0xFF) << 8 | array[3] & 0xFF;
	}

	private static final byte[] toArray(int addr)
	{
		byte[] bytes = new byte[4];
		bytes[0] = (byte) (addr >>> 24 & 0xFF);
		bytes[1] = (byte) (addr >>> 16 & 0xFF);
		bytes[2] = (byte) (addr >>> 8 & 0xFF);
		bytes[3] = (byte) (addr & 0xFF);
		return bytes;
	}

	/**
	 * Creates an A Record from the given data
	 * 
	 * @param address
	 *            The address that the name refers to
	 */
	public ARecord(Name name, int dclass, long ttl, InetAddress address)
	{
		super(name, Type.A, dclass, ttl);
		if (Address.familyOf(address) != Address.IPv4)
		{
			throw new IllegalArgumentException("invalid IPv4 address");
		}
		addr = fromArray(address.getAddress());
	}

	public ARecord(Name name, int dclass, long ttl, int address)
	{
		super(name, Type.A, dclass, ttl);
		addr = address;
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		addr = fromArray(in.readByteArray(4));
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		InetAddress address = st.getAddress(Address.IPv4);
		addr = fromArray(address.getAddress());
	}

	/** Converts rdata to a String */
	@Override
	public String rrToString()
	{
		return Address.toDottedQuad(toArray(addr));
	}

	/** Returns the Internet address */
	public InetAddress getAddress()
	{
		try
		{
			return InetAddress.getByAddress(toArray(addr));
		}
		catch (UnknownHostException e)
		{
			return null;
		}
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU32(addr & 0xFFFFFFFFL);
	}

	public byte[] toByteArray()
	{
		return toArray(addr);
	}

	public byte[] toRedis()
	{
		byte[] bytes = new byte[12];
		bytes[0] = (byte) (addr >>> 24 & 0xFF);
		bytes[1] = (byte) (addr >>> 16 & 0xFF);
		bytes[2] = (byte) (addr >>> 8 & 0xFF);
		bytes[3] = (byte) (addr & 0xFF);
		bytes[4] = (byte) (ttl >>> 56 & 0xFF);
		bytes[5] = (byte) (ttl >>> 48 & 0xFF);
		bytes[6] = (byte) (ttl >>> 40 & 0xFF);
		bytes[7] = (byte) (ttl >>> 32 & 0xFF);
		bytes[8] = (byte) (ttl >>> 24 & 0xFF);
		bytes[9] = (byte) (ttl >>> 16 & 0xFF);
		bytes[10] = (byte) (ttl >>> 8 & 0xFF);
		bytes[11] = (byte) (ttl >>> 0 & 0xFF);
		return bytes;
	}

	public static int getAddr(byte[] redisStore)
	{
		return fromArray(redisStore);
	}

	public static long getTTL(byte[] redisStore)
	{
		return ((long) redisStore[4] << 56) + ((long) (redisStore[5] & 0xFF) << 48) + ((long) (redisStore[6] & 0xFF) << 40) + ((long) (redisStore[7] & 0xFF) << 32)
				+ ((long) (redisStore[8] & 0xFF) << 24) + ((redisStore[9] & 0xFF) << 16) + ((redisStore[10] & 0xFF) << 8) + ((redisStore[11] & 0xFF) << 0);
	}

}
