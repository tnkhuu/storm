/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.util.List;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;

public class TXTRecord extends TXTBase
{

	private static final long serialVersionUID = -5780785764284221342L;

	public TXTRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new TXTRecord();
	}

	/**
	 * Creates a TXT Record from the given data
	 * 
	 * @param strings
	 *            The text strings
	 * @throws IllegalArgumentException
	 *             One of the strings has invalid escapes
	 */
	@SuppressWarnings("rawtypes")
	public TXTRecord(Name name, int dclass, long ttl, List strings)
	{
		super(name, Type.TXT, dclass, ttl, strings);
	}

	/**
	 * Creates a TXT Record from the given data
	 * 
	 * @param string
	 *            One text string
	 * @throws IllegalArgumentException
	 *             The string has invalid escapes
	 */
	public TXTRecord(Name name, int dclass, long ttl, String string)
	{
		super(name, Type.TXT, dclass, ttl, string);
	}

}
