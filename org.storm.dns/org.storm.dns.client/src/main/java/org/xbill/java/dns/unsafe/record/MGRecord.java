/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;

public class MGRecord extends SingleNameBase
{

	private static final long serialVersionUID = -3980055550863644582L;

	public MGRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new MGRecord();
	}

	/**
	 * Creates a new MG Record with the given data
	 * 
	 * @param mailbox
	 *            The mailbox that is a member of the group specified by the
	 *            domain.
	 */
	public MGRecord(Name name, int dclass, long ttl, Name mailbox)
	{
		super(name, Type.MG, dclass, ttl, mailbox, "mailbox");
	}

	/** Gets the mailbox in the mail group specified by the domain */
	public Name getMailbox()
	{
		return getSingleName();
	}

}
