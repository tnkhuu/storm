/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe;

public interface MessageOutputBuffer
{

	/**
	 * Returns the current position.
	 */
	public abstract int current();

	/**
	 * Resets the current position of the output stream to the specified index.
	 * 
	 * @param index
	 *            The new current position.
	 * @throws IllegalArgumentException
	 *             The index is not within the output.
	 */
	public abstract void seek(int index);

	/**
	 * Saves the current state of the output stream.
	 * 
	 * @throws IllegalArgumentException
	 *             The index is not within the output.
	 */
	public abstract void mark();

	/**
	 * Restores the input stream to its state before the call to {@link #save}.
	 */
	public abstract void restore();

	/**
	 * Writes an unsigned 8 bit value to the stream.
	 * 
	 * @param val
	 *            The value to be written
	 */
	public abstract void writeU8(int val);

	/**
	 * Writes an unsigned 16 bit value to the stream.
	 * 
	 * @param val
	 *            The value to be written
	 */
	public abstract void writeU16(int val);

	/**
	 * Writes an unsigned 16 bit value to the specified position in the stream.
	 * 
	 * @param val
	 *            The value to be written
	 * @param where
	 *            The position to write the value.
	 */
	public abstract void writeU16At(int val, int where);

	/**
	 * Writes an unsigned 32 bit value to the stream.
	 * 
	 * @param val
	 *            The value to be written
	 */
	public abstract void writeU32(long val);

	/**
	 * Writes a byte array to the stream.
	 * 
	 * @param b
	 *            The array to write.
	 * @param off
	 *            The offset of the array to start copying data from.
	 * @param len
	 *            The number of bytes to write.
	 */
	public abstract void writeByteArray(byte[] b, int off, int len);

	/**
	 * Writes a byte array to the stream.
	 * 
	 * @param b
	 *            The array to write.
	 */
	public abstract void writeByteArray(byte[] b);

	/**
	 * Writes a counted string from the stream. A counted string is a one byte
	 * value indicating string length, followed by bytes of data.
	 * 
	 * @param s
	 *            The string to write.
	 */
	public abstract void writeCountedString(byte[] s);

	/**
	 * Returns a byte array containing the current contents of the stream.
	 */
	public abstract byte[] toByteArray();

}