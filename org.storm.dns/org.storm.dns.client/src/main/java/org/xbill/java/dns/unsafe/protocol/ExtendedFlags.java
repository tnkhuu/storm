/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

import org.xbill.java.dns.unsafe.util.Mnemonic;

public final class ExtendedFlags
{

	private static Mnemonic extflags = new Mnemonic("EDNS Flag", Mnemonic.CASE_LOWER);

	/** dnssec ok */
	public static final int DO = 0x8000;

	static
	{
		extflags.setMaximum(0xFFFF);
		extflags.setPrefix("FLAG");
		extflags.setNumericAllowed(true);

		extflags.add(DO, "do");
	}

	private ExtendedFlags()
	{
	}

	/** Converts a numeric extended flag into a String */
	public static String string(int i)
	{
		return extflags.getText(i);
	}

	/**
	 * Converts a textual representation of an extended flag into its numeric
	 * value
	 */
	public static int value(String s)
	{
		return extflags.getValue(s);
	}

}
