/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

import java.io.IOException;
import java.util.Arrays;

import org.storm.dns.io.unsafe.UnsafeInput;
import org.xbill.java.dns.unsafe.DNSOutput;
import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.WireParseException;
import org.xbill.java.dns.unsafe.record.Record;
import org.xbill.java.dns.unsafe.util.Mnemonic;

@SuppressWarnings("deprecation")
public abstract class EDNSOption
{

	public static class Code
	{
		private Code()
		{
		}

		/** Name Server Identifier, RFC 5001 */
		public final static int NSID = 3;

		/** Client Subnet, defined in draft-vandergaast-edns-client-subnet-00 */
		public final static int CLIENT_SUBNET = 20730;

		private static Mnemonic codes = new Mnemonic("EDNS Option Codes", Mnemonic.CASE_UPPER);

		static
		{
			codes.setMaximum(0xFFFF);
			codes.setPrefix("CODE");
			codes.setNumericAllowed(true);

			codes.add(NSID, "NSID");
			codes.add(CLIENT_SUBNET, "CLIENT_SUBNET");
		}

		/**
		 * Converts an EDNS Option Code into its textual representation
		 */
		public static String string(int code)
		{
			return codes.getText(code);
		}

		/**
		 * Converts a textual representation of an EDNS Option Code into its
		 * numeric value.
		 * 
		 * @param s
		 *            The textual representation of the option code
		 * @return The option code, or -1 on error.
		 */
		public static int value(String s)
		{
			return codes.getValue(s);
		}
	}

	private final int code;

	/**
	 * Creates an option with the given option code and data.
	 */
	public EDNSOption(int code)
	{
		this.code = Record.checkU16("code", code);
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();

		sb.append("{");
		sb.append(EDNSOption.Code.string(code));
		sb.append(": ");
		sb.append(optionToString());
		sb.append("}");

		return sb.toString();
	}

	/**
	 * Returns the EDNS Option's code.
	 * 
	 * @return the option code
	 */
	public int getCode()
	{
		return code;
	}

	/**
	 * Returns the EDNS Option's data, as a byte array.
	 * 
	 * @return the option data
	 */
	public byte[] getData()
	{
		MessageOutputBuffer out = new DNSOutput();
		optionToWire(out);
		return out.toByteArray();
	}

	/**
	 * Converts the wire format of an EDNS Option (the option data only) into
	 * the type-specific format.
	 * 
	 * @param in
	 *            The input Stream.
	 */
	public abstract void optionFromWire(MessageInputBuffer in) throws IOException;

	/**
	 * Converts the wire format of an EDNS Option (including code and length)
	 * into the type-specific format.
	 * 
	 * @param out
	 *            The input stream.
	 */
	public static EDNSOption fromWire(MessageInputBuffer in) throws IOException
	{
		int code, length;

		code = in.readU16();
		length = in.readU16();
		if (in.remaining() < length)
		{
			throw new WireParseException("truncated option");
		}
		int save = in.saveActive();
		in.setActive(length);
		EDNSOption option;
		switch (code)
		{
		case Code.NSID:
			option = new NSIDOption();
			break;
		case Code.CLIENT_SUBNET:
			option = new ClientSubnetOption();
			break;
		default:
			option = new GenericEDNSOption(code);
			break;
		}
		option.optionFromWire(in);
		in.restoreActive(save);

		return option;
	}

	/**
	 * Converts the wire format of an EDNS Option (including code and length)
	 * into the type-specific format.
	 * 
	 * @return The option, in wire format.
	 */
	public static EDNSOption fromWire(byte[] b) throws IOException
	{
		return fromWire(new UnsafeInput(b));
	}

	/**
	 * Converts an EDNS Option (the type-specific option data only) into wire
	 * format.
	 * 
	 * @param out
	 *            The output stream.
	 */
	public abstract void optionToWire(MessageOutputBuffer out);

	/**
	 * Converts an EDNS Option (including code and length) into wire format.
	 * 
	 * @param out
	 *            The output stream.
	 */
	public void toWire(MessageOutputBuffer out)
	{
		out.writeU16(code);
		int lengthPosition = out.current();
		out.writeU16(0); /* until we know better */
		optionToWire(out);
		int length = out.current() - lengthPosition - 2;
		out.writeU16At(length, lengthPosition);
	}

	/**
	 * Converts an EDNS Option (including code and length) into wire format.
	 * 
	 * @return The option, in wire format.
	 */
	public byte[] toWire() throws IOException
	{
		MessageOutputBuffer out = new DNSOutput();
		toWire(out);
		return out.toByteArray();
	}

	/**
	 * Determines if two EDNS Options are identical.
	 * 
	 * @param arg
	 *            The option to compare to
	 * @return true if the options are equal, false otherwise.
	 */
	@Override
	public boolean equals(Object arg)
	{
		if (arg == null || !(arg instanceof EDNSOption))
		{
			return false;
		}
		EDNSOption opt = (EDNSOption) arg;
		if (code != opt.code)
		{
			return false;
		}
		return Arrays.equals(getData(), opt.getData());
	}

	/**
	 * Generates a hash code based on the EDNS Option's data.
	 */
	@Override
	public int hashCode()
	{
		byte[] array = getData();
		int hashval = 0;
		for (int i = 0; i < array.length; i++)
		{
			hashval += (hashval << 3) + (array[i] & 0xFF);
		}
		return hashval;
	}

	public abstract String optionToString();

}
