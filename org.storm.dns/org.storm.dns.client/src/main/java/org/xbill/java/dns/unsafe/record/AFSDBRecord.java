/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.protocol.U16NameBase;

/**
 * AFS Data Base Record - maps a domain name to the name of an AFS cell database
 * server.
 */

public class AFSDBRecord extends U16NameBase
{

	private static final long serialVersionUID = 3034379930729102437L;

	public AFSDBRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new AFSDBRecord();
	}

	/**
	 * Creates an AFSDB Record from the given data.
	 * 
	 * @param subtype
	 *            Indicates the type of service provided by the host.
	 * @param host
	 *            The host providing the service.
	 */
	public AFSDBRecord(Name name, int dclass, long ttl, int subtype, Name host)
	{
		super(name, Type.AFSDB, dclass, ttl, subtype, "subtype", host, "host");
	}

	/** Gets the subtype indicating the service provided by the host. */
	public int getSubtype()
	{
		return getU16Field();
	}

	/** Gets the host providing service for the domain. */
	public Name getHost()
	{
		return getNameField();
	}

}
