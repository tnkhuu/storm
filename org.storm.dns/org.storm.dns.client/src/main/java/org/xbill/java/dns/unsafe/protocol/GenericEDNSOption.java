/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.record.Record;
import org.xbill.java.dns.unsafe.util.Base16;

public class GenericEDNSOption extends EDNSOption
{

	private byte[] data;

	public GenericEDNSOption(int code)
	{
		super(code);
	}

	/**
	 * Construct a generic EDNS option.
	 * 
	 * @param data
	 *            The contents of the option.
	 */
	public GenericEDNSOption(int code, byte[] data)
	{
		super(code);
		this.data = Record.checkByteArrayLength("option data", data, 0xFFFF);
	}

	@Override
	public void optionFromWire(MessageInputBuffer in) throws IOException
	{
		data = in.readByteArray();
	}

	@Override
	public void optionToWire(MessageOutputBuffer out)
	{
		out.writeByteArray(data);
	}

	@Override
	public String optionToString()
	{
		return "<" + Base16.toString(data) + ">";
	}

}
