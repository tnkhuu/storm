/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.util.Date;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;

public class SIGRecord extends SIGBase
{

	private static final long serialVersionUID = 4963556060953589058L;

	public SIGRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new SIGRecord();
	}

	/**
	 * Creates an SIG Record from the given data
	 * 
	 * @param covered
	 *            The RRset type covered by this signature
	 * @param alg
	 *            The cryptographic algorithm of the key that generated the
	 *            signature
	 * @param origttl
	 *            The original TTL of the RRset
	 * @param expire
	 *            The time at which the signature expires
	 * @param timeSigned
	 *            The time at which this signature was generated
	 * @param footprint
	 *            The footprint/key id of the signing key.
	 * @param signer
	 *            The owner of the signing key
	 * @param signature
	 *            Binary data representing the signature
	 */
	public SIGRecord(Name name, int dclass, long ttl, int covered, int alg, long origttl, Date expire, Date timeSigned, int footprint, Name signer, byte[] signature)
	{
		super(name, Type.SIG, dclass, ttl, covered, alg, origttl, expire, timeSigned, footprint, signer, signature);
	}

}
