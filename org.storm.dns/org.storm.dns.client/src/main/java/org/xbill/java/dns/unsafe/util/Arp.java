/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.util;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.protocol.Name;

public class Arp
{
	private static final Logger	s_log	= LoggerFactory.getLogger(Arp.class);
	private final static String	DOT		= ".";
	private final static String	IN_ARPA	= "in-addr.arpa.";

	/**
	 * Creates the arp address.
	 * 
	 * @param address
	 *            the address
	 * @return the name
	 */
	public static Name createArpAddress(InetAddress address)
	{
		Name arpDomainName = null;

		byte[] array = address.getAddress();
		StringBuilder builder = new StringBuilder();

		builder.append((array[3] & 0xFF)).append(DOT);
		builder.append((array[2] & 0xFF)).append(DOT);
		builder.append((array[1] & 0xFF)).append(DOT);
		builder.append((array[0] & 0xFF)).append(DOT);
		builder.append(IN_ARPA);
		try
		{
			arpDomainName = Name.fromString(builder.toString());
		}
		catch (TextParseException e)
		{
			s_log.error(e.getMessage(), e);
		}
		return arpDomainName;
	}
}
