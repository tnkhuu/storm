/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Base16;
import org.xbill.java.dns.unsafe.util.Compression;

public class DLVRecord extends Record
{

	public static final int SHA1_DIGEST_ID = DSRecord.Digest.SHA1;
	public static final int SHA256_DIGEST_ID = DSRecord.Digest.SHA1;

	private static final long serialVersionUID = 1960742375677534148L;

	private int footprint;
	private int alg;
	private int digestid;
	private byte[] digest;

	public DLVRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new DLVRecord();
	}

	/**
	 * Creates a DLV Record from the given data
	 * 
	 * @param footprint
	 *            The original KEY record's footprint (keyid).
	 * @param alg
	 *            The original key algorithm.
	 * @param digestid
	 *            The digest id code.
	 * @param digest
	 *            A hash of the original key.
	 */
	public DLVRecord(Name name, int dclass, long ttl, int footprint, int alg, int digestid, byte[] digest)
	{
		super(name, Type.DLV, dclass, ttl);
		this.footprint = checkU16("footprint", footprint);
		this.alg = checkU8("alg", alg);
		this.digestid = checkU8("digestid", digestid);
		this.digest = digest;
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		footprint = in.readU16();
		alg = in.readU8();
		digestid = in.readU8();
		digest = in.readByteArray();
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		footprint = st.getUInt16();
		alg = st.getUInt8();
		digestid = st.getUInt8();
		digest = st.getHex();
	}

	/**
	 * Converts rdata to a String
	 */
	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(footprint);
		sb.append(" ");
		sb.append(alg);
		sb.append(" ");
		sb.append(digestid);
		if (digest != null)
		{
			sb.append(" ");
			sb.append(Base16.toString(digest));
		}

		return sb.toString();
	}

	/**
	 * Returns the key's algorithm.
	 */
	public int getAlgorithm()
	{
		return alg;
	}

	/**
	 * Returns the key's Digest ID.
	 */
	public int getDigestID()
	{
		return digestid;
	}

	/**
	 * Returns the binary hash of the key.
	 */
	public byte[] getDigest()
	{
		return digest;
	}

	/**
	 * Returns the key's footprint.
	 */
	public int getFootprint()
	{
		return footprint;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeU16(footprint);
		out.writeU8(alg);
		out.writeU8(digestid);
		if (digest != null)
		{
			out.writeByteArray(digest);
		}
	}

}
