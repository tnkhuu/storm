/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class ISDNRecord extends Record
{

	private static final long serialVersionUID = -8730801385178968798L;

	private byte[] address;
	private byte[] subAddress;

	public ISDNRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new ISDNRecord();
	}

	/**
	 * Creates an ISDN Record from the given data
	 * 
	 * @param address
	 *            The ISDN number associated with the domain.
	 * @param subAddress
	 *            The subaddress, if any.
	 * @throws IllegalArgumentException
	 *             One of the strings is invalid.
	 */
	public ISDNRecord(Name name, int dclass, long ttl, String address, String subAddress)
	{
		super(name, Type.ISDN, dclass, ttl);
		try
		{
			this.address = byteArrayFromString(address);
			if (subAddress != null)
			{
				this.subAddress = byteArrayFromString(subAddress);
			}
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		address = in.readCountedString();
		if (in.remaining() > 0)
		{
			subAddress = in.readCountedString();
		}
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		try
		{
			address = byteArrayFromString(st.getString());
			Tokenizer.Token t = st.get();
			if (t.isString())
			{
				subAddress = byteArrayFromString(t.value);
			}
			else
			{
				st.unget();
			}
		}
		catch (TextParseException e)
		{
			throw st.exception(e.getMessage());
		}
	}

	/**
	 * Returns the ISDN number associated with the domain.
	 */
	public String getAddress()
	{
		return byteArrayToString(address, false);
	}

	/**
	 * Returns the ISDN subaddress, or null if there is none.
	 */
	public String getSubAddress()
	{
		if (subAddress == null)
		{
			return null;
		}
		return byteArrayToString(subAddress, false);
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeCountedString(address);
		if (subAddress != null)
		{
			out.writeCountedString(subAddress);
		}
	}

	@Override
	public String rrToString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(byteArrayToString(address, true));
		if (subAddress != null)
		{
			sb.append(" ");
			sb.append(byteArrayToString(subAddress, true));
		}
		return sb.toString();
	}

}
