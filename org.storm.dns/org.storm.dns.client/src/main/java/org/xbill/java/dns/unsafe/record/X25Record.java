/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.util.Compression;

public class X25Record extends Record
{

	private static final long serialVersionUID = 4267576252335579764L;

	private byte[] address;

	public X25Record()
	{
	}

	@Override
	public Record getObject()
	{
		return new X25Record();
	}

	private static final byte[] checkAndConvertAddress(String address)
	{
		int length = address.length();
		byte[] out = new byte[length];
		for (int i = 0; i < length; i++)
		{
			char c = address.charAt(i);
			if (!Character.isDigit(c))
			{
				return null;
			}
			out[i] = (byte) c;
		}
		return out;
	}

	/**
	 * Creates an X25 Record from the given data
	 * 
	 * @param address
	 *            The X.25 PSDN address.
	 * @throws IllegalArgumentException
	 *             The address is not a valid PSDN address.
	 */
	public X25Record(Name name, int dclass, long ttl, String address)
	{
		super(name, Type.X25, dclass, ttl);
		this.address = checkAndConvertAddress(address);
		if (this.address == null)
		{
			throw new IllegalArgumentException("invalid PSDN address " + address);
		}
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		address = in.readCountedString();
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		String addr = st.getString();
		this.address = checkAndConvertAddress(addr);
		if (this.address == null)
		{
			throw st.exception("invalid PSDN address " + addr);
		}
	}

	/**
	 * Returns the X.25 PSDN address.
	 */
	public String getAddress()
	{
		return byteArrayToString(address, false);
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeCountedString(address);
	}

	@Override
	public String rrToString()
	{
		return byteArrayToString(address, true);
	}

}
