/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

import org.xbill.java.dns.unsafe.record.OPTRecord;

/**
 * The Name Server Identifier Option, define in RFC 5001.
 * 
 * @see OPTRecord
 */
public class NSIDOption extends GenericEDNSOption
{

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 74739759292589056L;

	NSIDOption()
	{
		super(EDNSOption.Code.NSID);
	}

	/**
	 * Construct an NSID option.
	 * 
	 * @param data
	 *            The contents of the option.
	 */
	public NSIDOption(byte[] data)
	{
		super(EDNSOption.Code.NSID, data);
	}

}
