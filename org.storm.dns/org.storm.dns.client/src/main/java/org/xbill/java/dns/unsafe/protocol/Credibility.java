/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.protocol;

/**
 * Constants relating to the credibility of cached data, which is based on the
 * data's source. The constants NORMAL and ANY should be used by most callers.
 * 
 */

public final class Credibility
{

	private Credibility()
	{
	}

	/** A hint or cache file on disk. */
	public static final int HINT = 0;

	/** The additional section of a response. */
	public static final int ADDITIONAL = 1;

	/** The additional section of a response. */
	public static final int GLUE = 2;

	/** The authority section of a nonauthoritative response. */
	public static final int NONAUTH_AUTHORITY = 3;

	/** The answer section of a nonauthoritative response. */
	public static final int NONAUTH_ANSWER = 3;

	/** The authority section of an authoritative response. */
	public static final int AUTH_AUTHORITY = 4;

	/** The answer section of a authoritative response. */
	public static final int AUTH_ANSWER = 4;

	/** A zone. */
	public static final int ZONE = 5;

	/** Credible data. */
	public static final int NORMAL = 3;

	/** Data not required to be credible. */
	public static final int ANY = 1;

}
