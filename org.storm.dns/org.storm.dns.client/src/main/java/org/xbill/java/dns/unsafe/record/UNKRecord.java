/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.xbill.java.dns.unsafe.record;

import java.io.IOException;

import org.xbill.java.dns.unsafe.MessageInputBuffer;
import org.xbill.java.dns.unsafe.MessageOutputBuffer;
import org.xbill.java.dns.unsafe.Tokenizer;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.util.Compression;

public class UNKRecord extends Record
{

	private static final long serialVersionUID = -4193583311594626915L;

	private byte[] data;

	public UNKRecord()
	{
	}

	@Override
	public Record getObject()
	{
		return new UNKRecord();
	}

	@Override
	public void rrFromWire(MessageInputBuffer in) throws IOException
	{
		data = in.readByteArray();
	}

	@Override
	public void rdataFromString(Tokenizer st, Name origin) throws IOException
	{
		throw st.exception("invalid unknown RR encoding");
	}

	/** Converts this Record to the String "unknown format" */
	@Override
	public String rrToString()
	{
		return unknownToString(data);
	}

	/** Returns the contents of this record. */
	public byte[] getData()
	{
		return data;
	}

	@Override
	public void rrToWire(MessageOutputBuffer out, Compression c, boolean canonical)
	{
		out.writeByteArray(data);
	}

}
