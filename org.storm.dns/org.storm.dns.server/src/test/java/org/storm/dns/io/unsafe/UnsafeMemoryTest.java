package org.storm.dns.io.unsafe;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import junit.framework.Assert;

import org.junit.Test;
import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.Record;

public class UnsafeMemoryTest
{
	@Test
	public void testUnsafeMemoryRead() throws IOException
	{

		Message m = getQuery(1);
		int id = m.getHeader().getID();

		byte[] data = m.toWire();
		UnsafeInput uin = new UnsafeInput(data);
		Message newmb = new Message(uin);
		Record r = newmb.getQuestion();
		Record comparitor = m.getQuestion();
		Assert.assertTrue(r.name.equals(comparitor.name));
		Assert.assertTrue(id == newmb.getHeader().getID());

	}

	@Test
	public void testUnsafeMemoryWrite() throws IOException
	{

		Message m = getQuery(1);
		int id = m.getHeader().getID();

		byte[] data = m.toWire();
		UnsafeInput uin = new UnsafeInput(data);
		Message newmb = new Message(uin);

		byte[] unsafeOut = newmb.toWireUnsafe(new UnsafeOutput());
		uin = new UnsafeInput(unsafeOut);
		newmb = new Message(uin);

		Record r = newmb.getQuestion();
		Record comparitor = m.getQuestion();
		Assert.assertTrue(r.name.equals(comparitor.name));
		Assert.assertTrue(id == newmb.getHeader().getID());

	}

	public Message getQuery(int i) throws TextParseException, UnknownHostException
	{
		return Message.newQuery(new ARecord(new Name(i + ".google.com."), DClass.IN, 0l, InetAddress.getByName("127.0.0.1")));
	}

}
