package org.storm.dns.protocol;

import java.net.InetAddress;
import java.net.UnknownHostException;

import junit.framework.Assert;

import org.junit.Test;
import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.RelativeNameException;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.ARecord;

public class ARecordTest
{
	@Test
	public void testCanConstructName() throws Exception
	{
		Name name = Name.fromString("google.com.");
		Assert.assertEquals("google.com.", name.toString());
	}

	@Test
	public void testUnqualifiedNameThrowsException() throws TextParseException, UnknownHostException
	{
		try
		{
			Name name = Name.fromString("www.google.com");
			System.out.println(name);
			ARecord record = new ARecord(name, DClass.IN, 1800, InetAddress.getLocalHost());
			Assert.fail("Record: " + record + " is unqualified.");
		}
		catch (RelativeNameException e)
		{
			// expected.
		}
	}

	@Test
	public void testCanConstructRecords() throws Exception
	{
		Name name = Name.fromString("google.com.");
		ARecord record = new ARecord(name, DClass.IN, 1800, InetAddress.getLocalHost());

		Assert.assertNotNull(record.getAddress());
		Assert.assertEquals(name, record.getName());
		Assert.assertEquals(DClass.IN, record.dclass);
		Assert.assertEquals(1800, record.ttl);
	}

}
