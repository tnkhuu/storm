package org.storm.dns.io.unsafe;

import java.io.IOException;
import java.net.InetAddress;

import org.storm.dns.persistence.RedisStore;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.Record;

public class RedisStoreTest
{

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException
	{
		RedisStore store = new RedisStore();
		Name name = Name.fromString("www.ebay.com.");
		ARecord record = new ARecord(name, DClass.IN, 18000, InetAddress.getByName("168.182.1.34"));

		store.addEntry(name, record);

		Record lookupedUp = store.getRecord(name);
		System.out.println(lookupedUp);
		// returns www.ebay.com. 18000 IN A 168.182.1.34

	}

}
