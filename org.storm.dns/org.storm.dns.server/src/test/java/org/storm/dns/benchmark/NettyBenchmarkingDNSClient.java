package org.storm.dns.benchmark;

import static org.storm.dns.client.DNSConfig.CONFIG;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioEventLoopGroup;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

import org.storm.dns.client.DNSDatagramChannel;
import org.storm.dns.client.DNSPacket;
import org.storm.dns.client.TextParseException;
import org.storm.dns.server.AsyncDNSClientHandler;
import org.storm.dns.server.ComponentActivator;
import org.storm.dns.server.Settings;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.ARecord;

public class NettyBenchmarkingDNSClient
{
	Bootstrap bootstrap;
	int port;
	private static byte[] querybytes;
	Channel channel;
	ComponentActivator dns;
	List<Integer> msgIds = new ArrayList<>();
	ConcurrentLinkedDeque<DNSPacket> sentQueries = new ConcurrentLinkedDeque<>();

	public static void main(String[] args)
	{
		new NettyBenchmarkingDNSClient().startServer().runBenchMark();
	}

	private NettyBenchmarkingDNSClient startServer()
	{
		dns = new ComponentActivator();
		try
		{
			dns.start(null);
			Thread.sleep(500);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("DNS is up.");
		return this;

	}

	public void runBenchMark()
	{
		try
		{
			final int req = 200000;
			DecimalFormat myFormatter = new DecimalFormat("###,###,###");
			System.out.println("Running tests. Will attempt to execute " + myFormatter.format(req) + " queries");
			bootstrap = new Bootstrap();

			port = 1339;
			int sendBuffer = CONFIG.getValue(Settings.SEND_BUF);
			int rcvBuffer = CONFIG.getValue(Settings.RCV_BUF);
			channel = bootstrap.group(new NioEventLoopGroup()).channel(DNSDatagramChannel.class).localAddress(new InetSocketAddress(port)).option(ChannelOption.SO_BROADCAST, true)
					.option(ChannelOption.SO_SNDBUF, sendBuffer).option(ChannelOption.SO_RCVBUF, rcvBuffer).option(ChannelOption.SO_REUSEADDR, true)
					.handler(new AsyncDNSClientHandler()
					{

						@Override
						public void messageReceived(ChannelHandlerContext ctx, DNSPacket msg) throws Exception
						{
							sentQueries.remove(msg);
						}
					}).bind().awaitUninterruptibly().channel();

			long time = System.currentTimeMillis();

			for (int i = 0; i < req; i++)
			{
				sendQuery(i);
			}
			while (!sentQueries.isEmpty())
			{
				Thread.yield();
			}
			double end = System.currentTimeMillis() - time;
			double seconds = end / 1000;

			String output = myFormatter.format(req / seconds);

			System.out.println("All dns queries resolved @ " + output + " queries/sec");
			System.exit(0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void sendQuery(int id)
	{

		if (querybytes == null)
		{
			Name name;
			try
			{
				name = Name.fromString("google.com.");
				ARecord record = new ARecord(name, DClass.IN, 1800, InetAddress.getLocalHost());

				Message msg = Message.newQuery(record);
				querybytes = msg.toWire();

			}
			catch (TextParseException e)
			{
				e.printStackTrace();
			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
			}

		}
		DNSPacket packet = new DNSPacket(Unpooled.copiedBuffer(querybytes), new InetSocketAddress("localhost", 8053), false);
		packet.setId(id);
		sentQueries.add(packet);
		channel.write(packet);
	}
}
