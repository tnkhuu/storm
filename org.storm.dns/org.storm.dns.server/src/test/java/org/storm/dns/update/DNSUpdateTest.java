package org.storm.dns.update;

import static org.storm.dns.client.DNSConfig.CONFIG;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioEventLoopGroup;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import org.junit.Before;
import org.junit.Test;
import org.storm.dns.client.DNSDatagramChannel;
import org.storm.dns.client.DNSPacket;
import org.storm.dns.server.AsyncDNSClientHandler;
import org.storm.dns.server.DNSServer;
import org.storm.dns.server.Settings;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Section;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.Record;

public class DNSUpdateTest
{
	Channel channel;
	Bootstrap bootstrap;
	@Before
	public void before() throws Exception {
		new DNSServer(null).run(8053);
		Thread.sleep(2000);
		
		setupAsyncClient();
	}
	@Test
	public void testDynamicUpdate() throws IOException {
		
		Message msg =	Message.newUpdate(Name.fromString("stormclowd.com."));
		Record r = new ARecord(Name.fromString("ensemble.stormclowd.com."), DClass.IN, 1800L, InetAddress.getByName("192.168.1.10"));
		msg.addRecord(r, Section.UPDATE);
		
		channel.write(new DNSPacket(Unpooled.wrappedBuffer(msg.toWire()), new InetSocketAddress("localhost", 8053)));
		
		r = new ARecord(Name.fromString("ensemble.stormclowd.com."), DClass.IN, 1800L, InetAddress.getByName("10.0.3.1"));
		Message query =	Message.newQuery(r);
		
		channel.write(new DNSPacket(Unpooled.wrappedBuffer(query.toWire()), new InetSocketAddress("localhost", 8053)));
	/*	byte[] result = null;//	UDPClient.sendrecv(new InetSocketAddress("localhost", 8053), query.toWire(), 512, 30000L);
		Message res = new Message(result);
		Record[] answer =	res.getSectionArray(Section.ANSWER);
		Assert.assertEquals(1, answer.length);
		Assert.assertTrue(answer[0] instanceof ARecord);
		ARecord arec = (ARecord) answer[0];
		Assert.assertEquals("192.168.1.10", arec.getAddress().getHostAddress());*/
	}
	
	
	
	private void setupAsyncClient() {
		Bootstrap bootstrap = new Bootstrap();
		int sendBuffer = CONFIG.getValue(Settings.SEND_BUF);
		int rcvBuffer = CONFIG.getValue(Settings.RCV_BUF);
		channel = bootstrap.group(new NioEventLoopGroup()).channel(DNSDatagramChannel.class).localAddress(new InetSocketAddress(0)).option(ChannelOption.SO_BROADCAST, true)
				.option(ChannelOption.SO_SNDBUF, sendBuffer).option(ChannelOption.SO_RCVBUF, rcvBuffer).option(ChannelOption.SO_REUSEADDR, true)
				.handler(new AsyncDNSClientHandler()
				{

					@Override
					public void messageReceived(ChannelHandlerContext ctx, DNSPacket msg) throws Exception
					{
						System.out.println(msg.toString());
					}
				}).bind().awaitUninterruptibly().channel();
	}
}
