package org.storm.dns.protocol;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.storm.api.dns.NamingService;
import org.storm.dns.server.DNSServer;
import org.storm.dns.server.NamingServiceImpl;
import org.storm.nexus.api.Endpoints;

@Ignore
public class NamingServiceImplTest
{

	private static DNSServer server;
	private static NamingService service;

	@BeforeClass
	public static void beforeClass() throws Exception
	{
		server = new DNSServer(null);
		server.run(8053);
		service = new NamingServiceImpl();
		Thread.sleep(2000);
	}

	@AfterClass
	public static void afterClass()
	{
		server.shutdown();
	}

	@SuppressWarnings("unused")
	@Test
	public void testResolveAsString()
	{
		String address = service.resolveAsString(Endpoints.SERVICES_ABS.getDomain());
		// Assert.assertEquals(CachedRecords.getCachedRecord(Services.GATEWAY),
		// address);
	}

	@SuppressWarnings("unused")
	@Test
	public void testResolveAsInt()
	{
		int address = service.resolveAsInt(Endpoints.SERVICES_ABS.getDomain());
		// Assert.assertEquals(2130706433, address);
	}

	@SuppressWarnings("unused")
	@Test
	public void testResolveAsBytes()
	{
		byte[] address = service.resolve(Endpoints.SERVICES_ABS.getDomain());
		// Assert.assertTrue(Arrays.equals(new byte[]{127,0,0,1}, address));
	}

}
