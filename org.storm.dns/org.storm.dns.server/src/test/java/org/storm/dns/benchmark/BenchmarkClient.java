package org.storm.dns.benchmark;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.storm.dns.server.ComponentActivator;

/**
 * Only exists for reference.
 * 
 * @author tkhuu
 * @see NettyBenchmarkingDNSClient
 */
@Deprecated
public class BenchmarkClient
{
	static ExecutorService exec = Executors.newCachedThreadPool();

	public static void main(String args[]) throws Exception
	{
		final String stormDns = "127.0.0.1:8053";
		final String hostname = "google.com";
		final CountDownLatch dnsStarted = new CountDownLatch(1);
		exec.submit(new Runnable()
		{

			@Override
			public void run()
			{
				ComponentActivator dns = new ComponentActivator();
				try
				{
					dns.start(null);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					dnsStarted.countDown();
				}

			}
		});
		Thread.sleep(1000);
		dnsStarted.await();
		System.out.println("DNS is up");
		final int times = 50000;
		final int threads = 10;
		final int total = times * threads;
		final CountDownLatch cd = new CountDownLatch(total);
		final long startTime = System.nanoTime();
		System.out.println("Running tests with " + threads + " threads each executing " + times + " requests ...");
		for (int i = 0; i < threads; i++)
		{
			exec.submit(new Runnable()
			{

				@Override
				public void run()
				{
					for (int i = 0; i < times; i++)
					{
						DNSRecordLookup.lookup(stormDns, hostname, DNSRecordLookup.RECORD_A);
						// System.out.println(res);
						cd.countDown();
					}

				}
			});
		}

		cd.await();
		long endTime = System.nanoTime();
		long elapsedNs = endTime - startTime;
		double millis = elapsedNs * 0.000001;
		double secs = millis / 1000;
		System.out.println("Performed " + total + " lookups in " + millis + "(millis). Rate:  " + total / secs + "/sec");
		System.exit(0);

	}
}