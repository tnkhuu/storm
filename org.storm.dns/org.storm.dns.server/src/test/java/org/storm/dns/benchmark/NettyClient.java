package org.storm.dns.benchmark;

import static org.storm.dns.client.DNSConfig.CONFIG;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioEventLoopGroup;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.CountDownLatch;

import org.storm.dns.client.DNSDatagramChannel;
import org.storm.dns.client.DNSPacket;
import org.storm.dns.client.TextParseException;
import org.storm.dns.server.Settings;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.ARecord;

public class NettyClient extends ChannelInboundMessageHandlerAdapter<DNSPacket> implements Runnable
{
	Bootstrap bootstrap;
	byte[] querybytes;
	Channel channel;
	int amount;
	CountDownLatch cd;

	public NettyClient(int amount)
	{
		this.amount = amount;
		cd = new CountDownLatch(amount);
		bootstrap = new Bootstrap();
		int sendBuffer = CONFIG.getValue(Settings.SEND_BUF);
		int rcvBuffer = CONFIG.getValue(Settings.RCV_BUF);
		channel = bootstrap.group(new NioEventLoopGroup()).channel(DNSDatagramChannel.class).localAddress(new InetSocketAddress(0)).option(ChannelOption.SO_BROADCAST, true)
				.option(ChannelOption.SO_SNDBUF, sendBuffer).option(ChannelOption.SO_RCVBUF, rcvBuffer).option(ChannelOption.SO_REUSEADDR, true).handler(this).bind()
				.awaitUninterruptibly().channel();

	}

	@Override
	public void run()
	{
		for (int i = 0; i < amount; i++)
		{
			sendQuery();
		}

	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, DNSPacket msg) throws Exception
	{

	}

	public void sendQuery()
	{

		if (querybytes == null)
		{
			Name name;
			try
			{
				name = Name.fromString("google.com.");
				ARecord record = new ARecord(name, DClass.IN, 1800, InetAddress.getLocalHost());

				Message msg = Message.newQuery(record);
				querybytes = msg.toWire();

			}
			catch (TextParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (UnknownHostException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		channel.write(new DNSPacket(Unpooled.copiedBuffer(querybytes), new InetSocketAddress("localhost", 8053), false));

	}
}
