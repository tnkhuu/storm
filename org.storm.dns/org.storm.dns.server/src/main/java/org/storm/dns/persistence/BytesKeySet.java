/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.util.HashSet;

/**
 * Map that uses byte[]s for keys. Wraps them for you. Passing a non-byte[] or
 * non-BytesKey will result in a CCE.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BytesKeySet extends HashSet<BytesKey>
{
	private static final long serialVersionUID = 1L;

	/**
	 * Adds the.
	 * 
	 * @param member
	 *            the member
	 * @return true, if successful
	 */
	public boolean add(byte[] member)
	{
		return super.add(new BytesKey(member));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashSet#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o)
	{
		return o instanceof byte[] ? contains((byte[]) o) : super.contains(o);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashSet#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o)
	{
		return o instanceof byte[] ? remove((byte[]) o) : super.remove(o);
	}

	/**
	 * Contains.
	 * 
	 * @param member
	 *            the member
	 * @return true, if successful
	 */
	public boolean contains(byte[] member)
	{
		return super.contains(new BytesKey(member));
	}

	/**
	 * Removes the.
	 * 
	 * @param member
	 *            the member
	 * @return true, if successful
	 */
	public boolean remove(byte[] member)
	{
		return super.remove(new BytesKey(member));
	}
}
