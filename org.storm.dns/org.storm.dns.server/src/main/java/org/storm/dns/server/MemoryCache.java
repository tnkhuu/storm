/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.server;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.dns.client.TextParseException;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.api.Services;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.PTRRecord;
import org.xbill.java.dns.unsafe.record.RRset;
import org.xbill.java.dns.unsafe.record.Record;
import org.xbill.java.dns.unsafe.util.Arp;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * The Class CachedRecords.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MemoryCache implements DNSCache
{

	private static final Logger s_log = LoggerFactory.getLogger(MemoryCache.class);
	private static Cache<Name, RRset> cachedRecords = CacheBuilder.newBuilder().expireAfterWrite(1800, TimeUnit.SECONDS).build();
	private static ConcurrentHashMap<Name, RRset> nonexpiringCache = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<Name, PTRRecord> arpCache = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, Name> reverseIpCache = new ConcurrentHashMap<>();
	static Name STORM_DOMAIN;
	static Name PROXIES;
	static
	{
		try
		{
			STORM_DOMAIN = new Name(Endpoints.ORIGIN);
			PROXIES = new Name(Endpoints.PROXY_ABS.getDomain());
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}
	}

	/**
	 * Instantiates a new cached records.
	 */
	public MemoryCache()
	{
		seedRecords();
	}

	/**
	 * Seed records.
	 */
	private void seedRecords()
	{
		addNonExpiringEntry(Endpoints.REGISTRATION_ABS.getDomain(), getOutboundAddress());
		addNonExpiringEntry(Endpoints.DNS_ABS.getDomain(), getOutboundAddress());
		
		try
		{
			addNonExpiringEntry(InetAddress.getLocalHost().getHostName() + ".", getOutboundAddress());
			Name arpName = Arp.createArpAddress(InetAddress.getByName(getOutboundAddress()));
			addPTRRecord(arpName.getName(), Endpoints.DNS_ABS.getDomain());
		}
		catch (UnknownHostException e)
		{
			s_log.error(e.getMessage(), e);
		}
	}

	@Override
	public void addEntry(Name domain, Record record)
	{
		RRset existing = cachedRecords.getIfPresent(domain);
		if (existing == null)
		{
			existing = new RRset(record);
		}
		else
		{
			existing.addRR(record);
		}

		cachedRecords.put(domain, existing);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.DNSCache#addEntry(java.lang.String, java.lang.String)
	 */
	@Override
	public void addEntry(String domain, String ip)
	{
		try
		{
			if (domain.charAt(domain.length() - 1) != '.')
			{
				domain = domain + ".";
			}
			Name name = Name.fromString(domain);
			ARecord record = new ARecord(name, DClass.IN, 1800, InetAddress.getByName(ip));
			RRset existing = cachedRecords.getIfPresent(name);
			if (existing == null)
			{
				existing = new RRset(record);
			}
			else
			{
				existing.addRR(record);
			}

			cachedRecords.put(name, existing);
		}
		catch (TextParseException e)
		{
			throw new IllegalStateException(e);
		}
		catch (UnknownHostException e)
		{
			throw new IllegalStateException(e);
		}
	}

	public void addNonExpiringEntry(String domain, String ip)
	{
		try
		{
			if (domain.charAt(domain.length() - 1) != '.')
			{
				domain = domain + ".";
			}
			Name name = Name.fromString(domain);
			ARecord record = new ARecord(name, DClass.IN, 1800, InetAddress.getByName(ip));
			RRset existing = nonexpiringCache.get(name);
			if (existing == null)
			{
				existing = new RRset(record);
			}
			else
			{
				existing.addRR(record);
			}

			nonexpiringCache.putIfAbsent(name, existing);
			reverseIpCache.putIfAbsent(ip, name);
		}
		catch (TextParseException e)
		{
			throw new IllegalStateException(e);
		}
		catch (UnknownHostException e)
		{
			throw new IllegalStateException(e);
		}
	}

	public void addPTRRecord(String arpName, String hostname)
	{
		try
		{
			if (arpName.charAt(arpName.length() - 1) != '.')
			{
				arpName = arpName + ".";
			}
			Name name = Name.fromString(arpName);
			PTRRecord record = new PTRRecord(name, DClass.IN, 1800, Name.fromString(hostname));
			arpCache.putIfAbsent(name, record);
		}
		catch (TextParseException e)
		{
			throw new IllegalStateException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.DNSCache#addEntry(org.xbill.java.dns.unsafe.protocol.Name, org.xbill.java.dns.unsafe.record.RRset)
	 */
	@Override
	public void addEntry(Name domain, RRset records)
	{
		cachedRecords.put(domain, records);
	}

	@Override
	public void addManagedEntry(Name domain, Record record)
	{

		RRset existing = nonexpiringCache.get(domain);
		if (existing == null)
		{
			existing = new RRset(record);
		}
		else
		{
			existing.addRR(record);
		}

		nonexpiringCache.putIfAbsent(domain, existing);
		if (record instanceof ARecord)
		{
			ARecord rec = (ARecord) record;
			reverseIpCache.putIfAbsent(rec.getAddress().getHostAddress(), rec.getName());
		}

	}

	public void addPTRRecord(Name name, PTRRecord record)
	{
		arpCache.putIfAbsent(name, record);
	}

	public PTRRecord getPTRRecord(Name name)
	{
		return arpCache.get(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.DNSCache#containsEntry(java.lang.String)
	 */
	@Override
	public boolean containsEntry(String domain)
	{
		try
		{
			Name name = Name.fromString(domain);
			return cachedRecords.getIfPresent(name) != null;
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.DNSCache#removeEntry(java.lang.String)
	 */
	@Override
	public void removeEntry(String host)
	{
		try
		{
			Name name = Name.fromString(host);
			cachedRecords.invalidate(name);
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	public void removeManagedEntry(String host)
	{
		try
		{
			Name name = Name.fromString(host);
			nonexpiringCache.remove(name);
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.DNSCache#getEntry(java.lang.String)
	 */
	@Override
	public RRset getEntry(String host)
	{
		try
		{
			Name name = Name.fromString(host);
			return getEntry(name);
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.DNSCache#getEntry(org.xbill.java.dns.unsafe.protocol.Name)
	 */
	@Override
	public RRset getEntry(Name query)
	{

		RRset rr = nonexpiringCache.get(query);
		if (rr == null)
		{
			rr = cachedRecords.getIfPresent(query);
		}
		if (rr == null && query.subdomain(PROXIES))
		{
			// route all storm queries to the backing proxy cluster.
			return nonexpiringCache.get(PROXIES);

		}
		return rr;
	}

	public Name reverseLookup(String ip)
	{

		return reverseIpCache.get(ip);
	}

	/**
	 * Gets the outbound address.
	 * 
	 * @return the outbound address
	 */
	private static String getOutboundAddress()
	{
		String address = null;
		try
		{
			NetworkInterface ni = NetworkInterface.getByName(Services.OUTBOUND_ETHERNET_NAME);
			Enumeration<InetAddress> addresses = ni.getInetAddresses();

			while (addresses.hasMoreElements())
			{
				InetAddress addr = addresses.nextElement();
				if (addr instanceof Inet4Address)
				{
					address = addr.getHostAddress();
					break;
				}
			}
		}
		catch (Exception e)
		{

		}
		if (address == null)
		{
			try
			{
				address = InetAddress.getLocalHost().getHostAddress();
			}
			catch (UnknownHostException e)
			{
				// should never happen
			}
		}
		return address;
	}

}
