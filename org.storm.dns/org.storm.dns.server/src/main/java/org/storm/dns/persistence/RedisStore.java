/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.storm.dns.persistence;

import java.io.IOException;
import java.net.InetAddress;

import org.storm.dns.client.DNSConfig;
import org.storm.dns.client.TextParseException;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.RRset;
import org.xbill.java.dns.unsafe.record.Record;

/**
 * The Class RedisStore.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RedisStore
{
	private int port = DNSConfig.CONFIG.getValue("dns.redis.port");
	private String host = DNSConfig.CONFIG.getString("dns.redis.server");
	private RedisClient client;

	/**
	 * Instantiates a new redis store.
	 */
	public RedisStore()
	{
		try
		{
			client = new RedisClient(host, port);
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Adds the entry.
	 * 
	 * @param name
	 *            the name
	 * @param record
	 *            the record
	 */
	public void addEntry(Name name, ARecord record)
	{
		client.set(name.toString(), record.toRedis());
		client.pexpire(name.toString(), record.getTTL());
	}

	/**
	 * Adds the entry.
	 * 
	 * @param name
	 *            the name
	 * @param record
	 *            the record
	 */
	public void addEntry(Name name, Record record)
	{
		byte[] data;
		if (record instanceof ARecord)
		{
			data = ((ARecord) record).toRedis();
		}
		else
		{
			data = record.toWireCanonical();
		}
		client.set(name.toString(), data);
		// TTL's are stored in seconds, but redis's expire metric is in millis.
		// Need to compensate.
		client.pexpire(name.toString(), record.getTTL() * 1000);
	}

	/**
	 * Adds the entry.
	 * 
	 * @param name
	 *            the name
	 * @param ip
	 *            the ip
	 * @param ttl
	 *            the ttl
	 */
	public void addEntry(String name, InetAddress ip, long ttl)
	{

		client.set(name, ip.getAddress());
		client.pexpire(name, ttl);

	}

	/**
	 * Gets the record.
	 * 
	 * @param name
	 *            the name
	 * @return the record
	 */
	public Record getRecord(String name)
	{
		try
		{
			Name host = Name.fromString(name);
			BulkReply reply = client.get(host);
			if (reply.data() == null)
			{
				return null;
			}
			else
			{
				int address = ARecord.getAddr(reply.data());
				long ttl = ARecord.getTTL(reply.data());
				ARecord record = new ARecord(host, DClass.IN, ttl, address);
				return record;
			}
		}
		catch (TextParseException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Gets the record.
	 * 
	 * @param name
	 *            the name
	 * @return the record
	 */
	public Record getRecord(Name name)
	{
		BulkReply reply = client.get(name);
		if (reply.data() == null)
		{
			return null;
		}
		else
		{
			int address = ARecord.getAddr(reply.data());
			long ttl = ARecord.getTTL(reply.data());
			ARecord record = new ARecord(name, DClass.IN, ttl, address);
			return record;
		}
	}

	/**
	 * Gets the records.
	 * 
	 * @param name
	 *            the name
	 * @return the records
	 */
	public RRset getRecords(Name name)
	{

		BulkReply reply = client.get(name);
		if (reply.data() == null)
		{
			return null;
		}
		else
		{

			int address = ARecord.getAddr(reply.data());
			long ttl = ARecord.getTTL(reply.data());
			ARecord record = new ARecord(name, DClass.IN, ttl, address);
			return new RRset(record);
		}
	}
}
