/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import com.google.common.base.Charsets;

/**
 * BulkReply.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BulkReply implements Reply<byte[]>
{

	public static final char MARKER = '$';
	private final byte[] bytes;

	/**
	 * Instantiates a new bulk reply.
	 * 
	 * @param bytes
	 *            the bytes
	 */
	public BulkReply(byte[] bytes)
	{
		this.bytes = bytes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.Reply#data()
	 */
	@Override
	public byte[] data()
	{
		return bytes;
	}

	/**
	 * As ascii string.
	 * 
	 * @return the string
	 */
	public String asAsciiString()
	{
		if (bytes == null)
		{
			return null;
		}
		return new String(bytes, Charsets.US_ASCII);
	}

	/**
	 * As ut f8 string.
	 * 
	 * @return the string
	 */
	public String asUTF8String()
	{
		if (bytes == null)
		{
			return null;
		}
		return new String(bytes, Charsets.UTF_8);
	}

	/**
	 * As string.
	 * 
	 * @param charset
	 *            the charset
	 * @return the string
	 */
	public String asString(Charset charset)
	{
		if (bytes == null)
		{
			return null;
		}
		return new String(bytes, charset);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.Reply#write(java.io.OutputStream)
	 */
	@Override
	public void write(OutputStream os) throws IOException
	{
		os.write(MARKER);
		os.write(RedisProtocol.toBytes(bytes.length));
		os.write(CRLF);
		os.write(bytes);
		os.write(CRLF);
	}
}
