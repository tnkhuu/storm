/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.server;

import static org.storm.dns.client.DNSConfig.CONFIG;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;

import java.net.InetSocketAddress;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.dns.MessageCallback;
import org.storm.dns.client.DNSClient;
import org.storm.dns.client.DNSPacket;
import org.xbill.java.dns.unsafe.MessageFactory;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Flags;
import org.xbill.java.dns.unsafe.protocol.Header;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Opcode;
import org.xbill.java.dns.unsafe.protocol.Section;
import org.xbill.java.dns.unsafe.protocol.Type;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.CNAMERecord;
import org.xbill.java.dns.unsafe.record.PTRRecord;
import org.xbill.java.dns.unsafe.record.RRset;
import org.xbill.java.dns.unsafe.record.Record;
import org.xbill.java.dns.unsafe.util.Arp;

/**
 * The core dns routing logic to handle lookups or do async recursive queries or
 *
 * @author Trung Khuu
 * @since 1.0
 */
public class AsyncCachedDNSHandler extends ChannelInboundMessageHandlerAdapter<DNSPacket>
{

	private static final Logger s_log = LoggerFactory.getLogger(AsyncCachedDNSHandler.class);
	private static final InetSocketAddress recursiveServer = new InetSocketAddress(CONFIG.getString(Settings.RECURSIVE_RES), CONFIG.getValue(Settings.RECURSIVE_RES_PORT));
	private final DNSCache store;
	private final DNSClient client = new DNSClient(recursiveServer);

	/**
	 * Instantiates a new async cached dns handler.
	 * 
	 * @param cache
	 *            the cache
	 */
	public AsyncCachedDNSHandler(DNSCache cache)
	{
		this.store = cache;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.netty.channel.ChannelInboundMessageHandlerAdapter#messageReceived( io.netty.channel.ChannelHandlerContext, java.lang.Object)
	 */
	@Override
	public void messageReceived(final ChannelHandlerContext ctx, final DNSPacket msg) throws Exception
	{
		final Message query = msg.getDecodedMessage();
		final Header header = query.getHeader();
		switch (header.getOpcode())
		{
		case Opcode.UPDATE:
		{
			updateRecord(ctx, msg, query);
			break;
		}
		case Opcode.IQUERY:
		{
			reverseLookup(ctx, msg, query, header);
			break;
		}
		case Opcode.QUERY:
		{
			lookup(ctx, msg, query, header);
			break;
		}
		default:
		{
			s_log.warn("Unsupported DNS Query found: {}", msg);
			break;
		}
		}

	}

	/**
	 * Reverse lookup.
	 * 
	 * @param ctx
	 *            the ctx
	 * @param msg
	 *            the msg
	 * @param query
	 *            the query
	 * @param header
	 *            the header
	 */
	private void reverseLookup(final ChannelHandlerContext ctx, final DNSPacket msg, final Message query, final Header header)
	{
		final Record[] answer = query.getSectionArray(Section.ANSWER);
		if (answer != null && answer.length > 0 && answer[0] instanceof ARecord)
		{
			ARecord rec = (ARecord) answer[0];
			String address = rec.getAddress().getHostAddress();
			Name name = store.reverseLookup(address);
			if (name != null)
			{
				query.removeRecord(rec, Section.ANSWER);
				ARecord question = new ARecord(name, DClass.IN, 1800, rec.getAddress());
				query.addRecord(rec.withName(name), Section.ANSWER);
				query.addRecord(question, Section.QUESTION);
				ctx.write(query);
			}
		}

	}

	/**
	 * Lookup.
	 * 
	 * @param ctx
	 *            the ctx
	 * @param msg
	 *            the msg
	 * @param query
	 *            the query
	 * @param header
	 *            the header
	 */
	private void lookup(final ChannelHandlerContext ctx, final DNSPacket msg, final Message query, final Header header)
	{
		final int msgID = header.getID();
		final Record question = query.getQuestion();
		final Name hostName = question.getName();
		final int type = question.getType();
		final boolean recursion = query.getHeader().getFlag(Flags.RD);
		if (type == Type.PTR)
		{
			PTRRecord record = store.getPTRRecord(hostName);
			if (record != null)
			{
				query.getHeader().setFlag(Flags.QR);
				query.getHeader().setFlag(Flags.RA);
				query.addRecord(record, Section.ANSWER);
				DNSPacket packet = new DNSPacket(msgID, Unpooled.wrappedBuffer(query.toWire()), msg.remoteAddress(), false);
				ctx.write(packet);
			}
		}
		else
		{
			RRset set = store.getEntry(hostName);
			final String remoteAddr = msg.remoteAddress().getHostString();
			s_log.debug("{} is looking up {}", remoteAddr, hostName);
			if (set == null)
			{
				client.send(query, new MessageCallback<DNSPacket>()
				{
					@Override
					public void onComplete(final DNSPacket resolved)
					{
						RRset[] rrset = resolved.getDecodedMessage().getSectionRRsets(Section.ANSWER);
						if (rrset != null && rrset.length > 0)
						{
							Iterator<Record> riter = rrset[0].rrs();
							boolean cache = true;
							while (riter.hasNext())
							{
								Record r = riter.next();
								if (Type.CNAME == r.getType() && recursion)
								{
									cache = false;
									CNAMERecord cname = (CNAMERecord) r;
									Record question = Record.newRecord(cname.getAlias(), Type.A, DClass.IN);
									query.addRecord(cname, Section.ANSWER);
									Message m = MessageFactory.newQuery(question, resolved.getId());
									client.send(m, new MessageCallback<DNSPacket>()
									{

										@Override
										public void onFail(Throwable t)
										{
											s_log.error(t.getMessage(), t);

										}

										@Override
										public void onComplete(DNSPacket packet)
										{
											Message m = packet.getDecodedMessage();
											Record[] recs = m.getSectionArray(Section.ANSWER);
											if (recs != null && recs.length > 0)
											{
												for (Record r : recs)
												{
													query.addRecord(r, Section.ANSWER);
												}

											}
											query.getHeader().setFlag(Flags.QR);
											query.getHeader().setFlag(Flags.RA);
											ctx.write(new DNSPacket(msgID, Unpooled.wrappedBuffer(query.toWire()), msg.remoteAddress(), false));

										}
									});
									break;
								}
							}
							if (cache)
							{
								store.addEntry(hostName, rrset[0]);
								ctx.write(new DNSPacket(msgID, resolved.data(), msg.remoteAddress(), false));
							}
						}

					}

					@Override
					public void onFail(Throwable t)
					{
						s_log.error(t.getMessage(), t);

					}
				});

			}
			else
			{
				Iterator<Record> s = set.rrs();
				Record r = null;
				while (s.hasNext())
				{
					r = s.next();
					query.addRecord(r, Section.ANSWER);
				}
				query.getHeader().setFlag(Flags.QR);
				query.getHeader().setFlag(Flags.RA);
				s_log.debug("Resolved name {} to {} for {}", hostName, r != null ? r.toString() : null, remoteAddr);
				DNSPacket packet = new DNSPacket(msgID, Unpooled.wrappedBuffer(query.toWire()), msg.remoteAddress(), false);
				ctx.write(packet);

			}
		}
	}

	/**
	 * Update record.
	 * 
	 * @param ctx
	 *            the ctx
	 * @param msg
	 *            the msg
	 * @param query
	 *            the query
	 */
	private void updateRecord(final ChannelHandlerContext ctx, final DNSPacket msg, final Message query)
	{
		Record[] zone = query.getSectionArray(Section.ZONE);
		if (zone != null && zone.length == 1)
		{
			// TODO: Verify zone authority
			// Record rzone = zone[0];
			Record[] update = query.getSectionArray(Section.UPDATE);
			s_log.debug("Update for  {} by {}", update[0].getName(), msg.remoteAddress());
			for (Record r : update)
			{
				long ttl = r.getTTL();
				int type = r.getType();
				if (ttl == 0 && Type.ANY == type)
				{
					Name name = r.getName();
					store.removeEntry(name.toString());
					ctx.write(query);
				}
				else
				{
					ARecord rec = (ARecord) r;
					ARecord record = new ARecord(rec.name, DClass.IN, 1800, rec.getAddress());
					Name arpName = Arp.createArpAddress(rec.getAddress());
					PTRRecord ptr = new PTRRecord(arpName, DClass.IN, 1800, rec.name);
					s_log.debug("Registered domain {} with record {}", rec.name, rec.getAddress().getHostAddress());

					store.addPTRRecord(arpName, ptr);
					store.addManagedEntry(r.getName(), record);
					ctx.write(msg);
				}

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.netty.channel.ChannelStateHandlerAdapter#exceptionCaught(io.netty. channel.ChannelHandlerContext, java.lang.Throwable)
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable e) throws Exception
	{
		ctx.close();
		s_log.error(e.getMessage(), e);
	}

}
