/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Class MultiBulkReply.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({ "rawtypes" })
public class MultiBulkReply implements Reply<Reply[]>
{

	public static final char MARKER = '*';
	private final Reply[] replies;

	/**
	 * Instantiates a new multi bulk reply.
	 * 
	 * @param is
	 *            the is
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public MultiBulkReply(InputStream is) throws IOException
	{
		long size = RedisProtocol.readLong(is);
		if (size == -1)
		{
			replies = null;
		}
		else
		{
			if (size > Integer.MAX_VALUE || size < 0)
			{
				throw new IllegalArgumentException("Invalid size: " + size);
			}
			replies = new Reply[(int) size];
			for (int i = 0; i < size; i++)
			{
				replies[i] = RedisProtocol.receive(is);
			}
		}
	}

	/**
	 * Instantiates a new multi bulk reply.
	 * 
	 * @param replies
	 *            the replies
	 */
	public MultiBulkReply(Reply[] replies)
	{
		this.replies = replies;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.Reply#data()
	 */
	@Override
	public Reply[] data()
	{
		return replies;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.Reply#write(java.io.OutputStream)
	 */
	@Override
	public void write(OutputStream os) throws IOException
	{
		os.write(MARKER);
		if (replies == null)
		{
			os.write(Encoding.NEG_ONE_WITH_CRLF);
		}
		else
		{
			os.write(RedisProtocol.toBytes(replies.length));
			os.write(CRLF);
			for (Reply reply : replies)
			{
				reply.write(os);
			}
		}
	}

	/**
	 * As string list.
	 * 
	 * @param charset
	 *            the charset
	 * @return the list
	 */
	public List<String> asStringList(Charset charset)
	{
		if (replies == null)
		{
			return null;
		}
		List<String> strings = new ArrayList<String>(replies.length);
		for (Reply reply : replies)
		{
			if (reply instanceof BulkReply)
			{
				strings.add(((BulkReply) reply).asString(charset));
			}
			else
			{
				throw new IllegalArgumentException("Could not convert " + reply + " to a string");
			}
		}
		return strings;
	}

	/**
	 * As string set.
	 * 
	 * @param charset
	 *            the charset
	 * @return the sets the
	 */
	public Set<String> asStringSet(Charset charset)
	{
		if (replies == null)
		{
			return null;
		}
		Set<String> strings = new HashSet<String>(replies.length);
		for (Reply reply : replies)
		{
			if (reply instanceof BulkReply)
			{
				strings.add(((BulkReply) reply).asString(charset));
			}
			else
			{
				throw new IllegalArgumentException("Could not convert " + reply + " to a string");
			}
		}
		return strings;
	}

	/**
	 * As string map.
	 * 
	 * @param charset
	 *            the charset
	 * @return the map
	 */
	public Map<String, String> asStringMap(Charset charset)
	{
		if (replies == null)
		{
			return null;
		}
		int length = replies.length;
		Map<String, String> map = new HashMap<String, String>(length);
		if (length % 2 != 0)
		{
			throw new IllegalArgumentException("Odd number of replies");
		}
		for (int i = 0; i < length; i += 2)
		{
			Reply key = replies[i];
			Reply value = replies[i + 1];
			if (key instanceof BulkReply)
			{
				if (value instanceof BulkReply)
				{
					map.put(((BulkReply) key).asString(charset), ((BulkReply) value).asString(charset));
				}
				else
				{
					throw new IllegalArgumentException("Could not convert value: " + value + " to a string");
				}
			}
			else
			{
				throw new IllegalArgumentException("Could not convert key: " + key + " to a string");
			}
		}
		return map;
	}
}
