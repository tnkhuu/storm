/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.util.Comparator;

import com.google.common.primitives.SignedBytes;

/**
 * The Class BytesKey.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BytesKey extends BytesValue implements Comparable<BytesKey>
{

	private static final Comparator<byte[]> COMPARATOR = SignedBytes.lexicographicalComparator();
	private final int hashCode;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.BytesValue#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		BytesKey other = (BytesKey) o;
		return o instanceof BytesKey && hashCode == other.hashCode && equals(bytes, other.bytes);
	}

	/**
	 * Instantiates a new bytes key.
	 * 
	 * @param bytes
	 *            the bytes
	 */
	public BytesKey(byte[] bytes)
	{
		super(bytes);
		int hashCode = 0;
		for (byte aByte : this.bytes)
		{
			hashCode += 43 * aByte;
		}
		this.hashCode = hashCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.BytesValue#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return hashCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(BytesKey o)
	{
		return COMPARATOR.compare(this.bytes, o.bytes);
	}
}
