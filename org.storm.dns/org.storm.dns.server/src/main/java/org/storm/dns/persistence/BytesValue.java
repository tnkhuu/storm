/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

/**
 * The Class BytesValue.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BytesValue
{

	/** The bytes. */
	protected final byte[] bytes;

	/**
	 * Instantiates a new bytes value.
	 * 
	 * @param bytes
	 *            the bytes
	 */
	public BytesValue(byte[] bytes)
	{
		this.bytes = bytes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int hashCode = 0;
		for (byte aByte : this.bytes)
		{
			hashCode += 43 * aByte;
		}
		return hashCode;
	}

	/**
	 * Equals.
	 * 
	 * @param thisBytes
	 *            the this bytes
	 * @param otherBytes
	 *            the other bytes
	 * @return true, if successful
	 */
	public static boolean equals(byte[] thisBytes, byte[] otherBytes)
	{
		int length = thisBytes.length;
		if (length != otherBytes.length)
		{
			return false;
		}
		int half = length / 2;
		for (int i = 0; i < half; i++)
		{
			int end = length - i - 1;
			if (thisBytes[end] != otherBytes[end])
			{
				return false;
			}
			if (thisBytes[i] != otherBytes[i])
			{
				return false;
			}
		}
		if (half != length - half)
		{
			if (thisBytes[half] != otherBytes[half])
			{
				return false;
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		return o instanceof BytesKey && equals(bytes, ((BytesKey) o).bytes);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return new String(bytes);
	}

	/**
	 * Gets the bytes.
	 * 
	 * @return the bytes
	 */
	public byte[] getBytes()
	{
		return bytes;
	}
}
