/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

/**
 * The ZSetEntry.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ZSetEntry implements Comparable<ZSetEntry>
{
	private final BytesKey key;
	private double score;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return key.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof ZSetEntry && key.equals(((ZSetEntry) obj).key);
	}

	/**
	 * Instantiates a new z set entry.
	 * 
	 * @param key
	 *            the key
	 * @param score
	 *            the score
	 */
	public ZSetEntry(BytesKey key, double score)
	{
		this.key = key;
		this.score = score;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ZSetEntry o)
	{
		double diff = score - o.score;
		return diff < 0 ? -1 : diff == 0 ? 0 : 1;
	}

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public BytesKey getKey()
	{
		return key;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	public double getScore()
	{
		return score;
	}

	/**
	 * Increment.
	 * 
	 * @param value
	 *            the value
	 * @return the double
	 */
	public double increment(double value)
	{
		return score += value;
	}
}
