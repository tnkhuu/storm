/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.dns.server;

import static org.storm.dns.client.DNSConfig.CONFIG;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioEventLoopGroup;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.dns.NamingService;
import org.storm.api.services.DistributedService;
import org.storm.api.services.RegistrationService;
import org.storm.dns.client.DNSDatagramChannel;

/**
 * A blazingly fast Authoritive or Caching DNS server capable of resolving
 * requests as fast as widely used C based DNSServers such as BIND.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DNSServer
{
	private Logger logger = LoggerFactory.getLogger(DNSServer.class);
	private Bootstrap bootstrap;
	private ExecutorService exec = Executors.newSingleThreadExecutor();
	private ChannelFuture future;
	private int port;
	private DNSCache records = new MemoryCache();
	private final BundleContext context;
	private RegistrationService registrationService;

	public DNSServer(BundleContext context)
	{
		this.context = context;
	}

	/**
	 * Start the DNS Server
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void run(final int port) throws Exception
	{
		this.port = port;
		exec.execute(new Runnable()
		{

			@Override
			public void run()
			{
				try
				{
					
					bootstrap = new Bootstrap();
					int sendBuffer = CONFIG.getValue(Settings.SEND_BUF);
					int rcvBuffer = CONFIG.getValue(Settings.RCV_BUF);
					registrationService = new RegistrationServiceImpl(records);
					future = bootstrap.group(new NioEventLoopGroup()).channel(DNSDatagramChannel.class).localAddress(new InetSocketAddress(DNSServer.this.port))
							.option(ChannelOption.SO_BROADCAST, true).option(ChannelOption.SO_SNDBUF, sendBuffer).option(ChannelOption.SO_RCVBUF, rcvBuffer)
							.option(ChannelOption.SO_REUSEADDR, true).handler(new AsyncCachedDNSHandler(records)).bind().sync().await();
					if (context != null)
					{
						context.registerService(new String[]{ RegistrationService.class.getName(),DistributedService.class.getName() }, registrationService, null);
						context.registerService(new String[] { NamingService.class.getName(), DistributedService.class.getName() }, new NamingServiceImpl(), null);
					}

				}
				catch (Exception e)
				{
					logger.error(e.getMessage(), e);
				}

			}
		});
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				shutdown();
			}
		}));
	}

	public int getPort()
	{
		return CONFIG.getValue(Settings.DNS_PORT);
	}

	public void shutdown()
	{
		try
		{
			future.channel().close();
		}
		catch (Exception e)
		{

		}
		bootstrap.shutdown();
		exec.shutdown();
	}

	public static void main(String[] args) throws Exception
	{
		new DNSServer(null).run(8053);
	}
}
