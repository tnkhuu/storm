/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.server;

import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.record.PTRRecord;
import org.xbill.java.dns.unsafe.record.RRset;
import org.xbill.java.dns.unsafe.record.Record;

/**
 * The Interface DNSCache.
 */
public interface DNSCache
{

	/**
	 * Adds the entry.
	 * 
	 * @param domain
	 *            the domain
	 * @param ip
	 *            the ip
	 */
	void addEntry(String domain, String ip);

	/**
	 * Adds the entry.
	 * 
	 * @param domain
	 *            the domain
	 * @param records
	 *            the records
	 */
	void addEntry(Name domain, RRset records);
	
	/**
	 * Adds the entry.
	 * 
	 * @param domain
	 *            the domain
	 * @param record
	 *            the record
	 */
	void addEntry(Name domain, Record record);
	
	/**
	 * Adds the managed entry.
	 * 
	 * @param domain
	 *            the domain
	 * @param record
	 *            the record
	 */
	void addManagedEntry(Name domain, Record record);
	
	/**
	 * Adds the ptr record.
	 * 
	 * @param name
	 *            the name
	 * @param record
	 *            the record
	 */
	void addPTRRecord(Name name, PTRRecord record);
	
	/**
	 * Gets the pTR record.
	 * 
	 * @param name
	 *            the name
	 * @param record
	 *            the record
	 * @return the pTR record
	 */
	PTRRecord getPTRRecord(Name name);
	
	/**
	 * Removes the managed entry.
	 * 
	 * @param host
	 *            the host
	 */
	void removeManagedEntry(String host);

	/**
	 * Contains entry.
	 * 
	 * @param domain
	 *            the domain
	 * @return true, if successful
	 */
	boolean containsEntry(String domain);

	/**
	 * Removes the entry.
	 * 
	 * @param host
	 *            the host
	 */
	void removeEntry(String host);

	/**
	 * Gets the entry.
	 * 
	 * @param host
	 *            the host
	 * @return the entry
	 */
	RRset getEntry(String host);
	
	/**
	 * Perform a reverse dns lookup
	 * 
	 * @param ip the ip address
	 * @return the domain name
	 */
	Name reverseLookup(String ip);

	/**
	 * Gets the entry.
	 * 
	 * @param host
	 *            the host
	 * @return the entry
	 */
	RRset getEntry(Name host);

}