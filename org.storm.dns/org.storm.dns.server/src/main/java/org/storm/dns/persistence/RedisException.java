/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

/**
 * The Class RedisException.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RedisException extends RuntimeException
{

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new redis exception.
	 */
	public RedisException()
	{
		super();
	}

	/**
	 * Instantiates a new redis exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public RedisException(Throwable cause)
	{
		super(cause);
	}

	/**
	 * Instantiates a new redis exception.
	 * 
	 * @param message
	 *            the message
	 */
	public RedisException(String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new redis exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public RedisException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
