/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;

import com.google.common.base.Charsets;
import com.google.common.util.concurrent.ListenableFuture;

/**
 * The Class RedisClient.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class RedisClient extends RedisClientBase
{

	protected Pipeline pipeline = new Pipeline();

	/**
	 * Instantiates a new redis client.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public RedisClient(String host, int port) throws IOException
	{
		this(new Socket(host, port));
	}

	/**
	 * Instantiates a new redis client.
	 * 
	 * @param socket
	 *            the socket
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public RedisClient(Socket socket) throws IOException
	{
		super(socket, Executors.newSingleThreadExecutor());
	}

	/**
	 * Pipeline.
	 * 
	 * @return the pipeline
	 */
	public Pipeline pipeline()
	{
		return pipeline;
	}

	/** The Constant APPEND. */
	private static final String APPEND = "APPEND";

	/** The Constant APPEND_BYTES. */
	private static final byte[] APPEND_BYTES = APPEND.getBytes(Charsets.US_ASCII);

	/** The Constant APPEND_VERSION. */
	private static final int APPEND_VERSION = parseVersion("2.0.0");

	/**
	 * Append a value to a key String.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply append(Object key0, Object value1) throws RedisException
	{
		if (version < APPEND_VERSION)
		{
			throw new RedisException("Server does not support APPEND");
		}
		return (IntegerReply) execute(APPEND, new Command(APPEND_BYTES, key0, value1));
	}

	/** The Constant BITCOUNT. */
	private static final String BITCOUNT = "BITCOUNT";

	/** The Constant BITCOUNT_BYTES. */
	private static final byte[] BITCOUNT_BYTES = BITCOUNT.getBytes(Charsets.US_ASCII);

	/** The Constant BITCOUNT_VERSION. */
	private static final int BITCOUNT_VERSION = parseVersion("2.6.0");

	/**
	 * Count set bits in a string String.
	 * 
	 * @param key0
	 *            the key0
	 * @param start1
	 *            the start1
	 * @param end2
	 *            the end2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply bitcount(Object key0, Object start1, Object end2) throws RedisException
	{
		if (version < BITCOUNT_VERSION)
		{
			throw new RedisException("Server does not support BITCOUNT");
		}
		return (IntegerReply) execute(BITCOUNT, new Command(BITCOUNT_BYTES, key0, start1, end2));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Bitcount_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply bitcount_(Object... arguments) throws RedisException
	{
		if (version < BITCOUNT_VERSION)
		{
			throw new RedisException("Server does not support BITCOUNT");
		}
		return (IntegerReply) execute(BITCOUNT, new Command(BITCOUNT_BYTES, arguments));
	}

	/** The Constant BITOP. */
	private static final String BITOP = "BITOP";

	/** The Constant BITOP_BYTES. */
	private static final byte[] BITOP_BYTES = BITOP.getBytes(Charsets.US_ASCII);

	/** The Constant BITOP_VERSION. */
	private static final int BITOP_VERSION = parseVersion("2.6.0");

	/**
	 * Perform bitwise operations between strings String.
	 * 
	 * @param operation0
	 *            the operation0
	 * @param destkey1
	 *            the destkey1
	 * @param key2
	 *            the key2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply bitop(Object operation0, Object destkey1, Object[] key2) throws RedisException
	{
		if (version < BITOP_VERSION)
		{
			throw new RedisException("Server does not support BITOP");
		}
		List list = new ArrayList();
		list.add(operation0);
		list.add(destkey1);
		Collections.addAll(list, key2);
		return (IntegerReply) execute(BITOP, new Command(BITOP_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Bitop_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply bitop_(Object... arguments) throws RedisException
	{
		if (version < BITOP_VERSION)
		{
			throw new RedisException("Server does not support BITOP");
		}
		return (IntegerReply) execute(BITOP, new Command(BITOP_BYTES, arguments));
	}

	/** The Constant DECR. */
	private static final String DECR = "DECR";

	/** The Constant DECR_BYTES. */
	private static final byte[] DECR_BYTES = DECR.getBytes(Charsets.US_ASCII);

	/** The Constant DECR_VERSION. */
	private static final int DECR_VERSION = parseVersion("1.0.0");

	/**
	 * Decrement the integer value of a key by one String.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply decr(Object key0) throws RedisException
	{
		if (version < DECR_VERSION)
		{
			throw new RedisException("Server does not support DECR");
		}
		return (IntegerReply) execute(DECR, new Command(DECR_BYTES, key0));
	}

	/** The Constant DECRBY. */
	private static final String DECRBY = "DECRBY";

	/** The Constant DECRBY_BYTES. */
	private static final byte[] DECRBY_BYTES = DECRBY.getBytes(Charsets.US_ASCII);

	/** The Constant DECRBY_VERSION. */
	private static final int DECRBY_VERSION = parseVersion("1.0.0");

	/**
	 * Decrement the integer value of a key by the given number String.
	 * 
	 * @param key0
	 *            the key0
	 * @param decrement1
	 *            the decrement1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply decrby(Object key0, Object decrement1) throws RedisException
	{
		if (version < DECRBY_VERSION)
		{
			throw new RedisException("Server does not support DECRBY");
		}
		return (IntegerReply) execute(DECRBY, new Command(DECRBY_BYTES, key0, decrement1));
	}

	/** The Constant GET. */
	private static final String GET = "GET";

	/** The Constant GET_BYTES. */
	private static final byte[] GET_BYTES = GET.getBytes(Charsets.US_ASCII);

	/** The Constant GET_VERSION. */
	private static final int GET_VERSION = parseVersion("1.0.0");

	/**
	 * Get the value of a key String.
	 * 
	 * @param key0
	 *            the key0
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply get(Object key0) throws RedisException
	{
		if (version < GET_VERSION)
		{
			throw new RedisException("Server does not support GET");
		}
		return (BulkReply) execute(GET, new Command(GET_BYTES, key0));
	}

	/** The Constant GETBIT. */
	private static final String GETBIT = "GETBIT";

	/** The Constant GETBIT_BYTES. */
	private static final byte[] GETBIT_BYTES = GETBIT.getBytes(Charsets.US_ASCII);

	/** The Constant GETBIT_VERSION. */
	private static final int GETBIT_VERSION = parseVersion("2.2.0");

	/**
	 * Returns the bit value at offset in the string value stored at key String.
	 * 
	 * @param key0
	 *            the key0
	 * @param offset1
	 *            the offset1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply getbit(Object key0, Object offset1) throws RedisException
	{
		if (version < GETBIT_VERSION)
		{
			throw new RedisException("Server does not support GETBIT");
		}
		return (IntegerReply) execute(GETBIT, new Command(GETBIT_BYTES, key0, offset1));
	}

	/** The Constant GETRANGE. */
	private static final String GETRANGE = "GETRANGE";

	/** The Constant GETRANGE_BYTES. */
	private static final byte[] GETRANGE_BYTES = GETRANGE.getBytes(Charsets.US_ASCII);

	/** The Constant GETRANGE_VERSION. */
	private static final int GETRANGE_VERSION = parseVersion("2.4.0");

	/**
	 * Get a substring of the string stored at a key String.
	 * 
	 * @param key0
	 *            the key0
	 * @param start1
	 *            the start1
	 * @param end2
	 *            the end2
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply getrange(Object key0, Object start1, Object end2) throws RedisException
	{
		if (version < GETRANGE_VERSION)
		{
			throw new RedisException("Server does not support GETRANGE");
		}
		return (BulkReply) execute(GETRANGE, new Command(GETRANGE_BYTES, key0, start1, end2));
	}

	/** The Constant GETSET. */
	private static final String GETSET = "GETSET";

	/** The Constant GETSET_BYTES. */
	private static final byte[] GETSET_BYTES = GETSET.getBytes(Charsets.US_ASCII);

	/** The Constant GETSET_VERSION. */
	private static final int GETSET_VERSION = parseVersion("1.0.0");

	/**
	 * Set the string value of a key and return its old value String.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply getset(Object key0, Object value1) throws RedisException
	{
		if (version < GETSET_VERSION)
		{
			throw new RedisException("Server does not support GETSET");
		}
		return (BulkReply) execute(GETSET, new Command(GETSET_BYTES, key0, value1));
	}

	/** The Constant INCR. */
	private static final String INCR = "INCR";

	/** The Constant INCR_BYTES. */
	private static final byte[] INCR_BYTES = INCR.getBytes(Charsets.US_ASCII);

	/** The Constant INCR_VERSION. */
	private static final int INCR_VERSION = parseVersion("1.0.0");

	/**
	 * Increment the integer value of a key by one String.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply incr(Object key0) throws RedisException
	{
		if (version < INCR_VERSION)
		{
			throw new RedisException("Server does not support INCR");
		}
		return (IntegerReply) execute(INCR, new Command(INCR_BYTES, key0));
	}

	/** The Constant INCRBY. */
	private static final String INCRBY = "INCRBY";

	/** The Constant INCRBY_BYTES. */
	private static final byte[] INCRBY_BYTES = INCRBY.getBytes(Charsets.US_ASCII);

	/** The Constant INCRBY_VERSION. */
	private static final int INCRBY_VERSION = parseVersion("1.0.0");

	/**
	 * Increment the integer value of a key by the given amount String.
	 * 
	 * @param key0
	 *            the key0
	 * @param increment1
	 *            the increment1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply incrby(Object key0, Object increment1) throws RedisException
	{
		if (version < INCRBY_VERSION)
		{
			throw new RedisException("Server does not support INCRBY");
		}
		return (IntegerReply) execute(INCRBY, new Command(INCRBY_BYTES, key0, increment1));
	}

	/** The Constant INCRBYFLOAT. */
	private static final String INCRBYFLOAT = "INCRBYFLOAT";

	/** The Constant INCRBYFLOAT_BYTES. */
	private static final byte[] INCRBYFLOAT_BYTES = INCRBYFLOAT.getBytes(Charsets.US_ASCII);

	/** The Constant INCRBYFLOAT_VERSION. */
	private static final int INCRBYFLOAT_VERSION = parseVersion("2.6.0");

	/**
	 * Increment the float value of a key by the given amount String.
	 * 
	 * @param key0
	 *            the key0
	 * @param increment1
	 *            the increment1
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply incrbyfloat(Object key0, Object increment1) throws RedisException
	{
		if (version < INCRBYFLOAT_VERSION)
		{
			throw new RedisException("Server does not support INCRBYFLOAT");
		}
		return (BulkReply) execute(INCRBYFLOAT, new Command(INCRBYFLOAT_BYTES, key0, increment1));
	}

	/** The Constant MGET. */
	private static final String MGET = "MGET";

	/** The Constant MGET_BYTES. */
	private static final byte[] MGET_BYTES = MGET.getBytes(Charsets.US_ASCII);

	/** The Constant MGET_VERSION. */
	private static final int MGET_VERSION = parseVersion("1.0.0");

	/**
	 * Get the values of all the given keys String.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply mget(Object[] key0) throws RedisException
	{
		if (version < MGET_VERSION)
		{
			throw new RedisException("Server does not support MGET");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (MultiBulkReply) execute(MGET, new Command(MGET_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Mget_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply mget_(Object... arguments) throws RedisException
	{
		if (version < MGET_VERSION)
		{
			throw new RedisException("Server does not support MGET");
		}
		return (MultiBulkReply) execute(MGET, new Command(MGET_BYTES, arguments));
	}

	/** The Constant MSET. */
	private static final String MSET = "MSET";

	/** The Constant MSET_BYTES. */
	private static final byte[] MSET_BYTES = MSET.getBytes(Charsets.US_ASCII);

	/** The Constant MSET_VERSION. */
	private static final int MSET_VERSION = parseVersion("1.0.1");

	/**
	 * Set multiple keys to multiple values String.
	 * 
	 * @param key_or_value0
	 *            the key_or_value0
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply mset(Object[] key_or_value0) throws RedisException
	{
		if (version < MSET_VERSION)
		{
			throw new RedisException("Server does not support MSET");
		}
		List list = new ArrayList();
		Collections.addAll(list, key_or_value0);
		return (StatusReply) execute(MSET, new Command(MSET_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Mset_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the status reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply mset_(Object... arguments) throws RedisException
	{
		if (version < MSET_VERSION)
		{
			throw new RedisException("Server does not support MSET");
		}
		return (StatusReply) execute(MSET, new Command(MSET_BYTES, arguments));
	}

	/** The Constant MSETNX. */
	private static final String MSETNX = "MSETNX";

	/** The Constant MSETNX_BYTES. */
	private static final byte[] MSETNX_BYTES = MSETNX.getBytes(Charsets.US_ASCII);

	/** The Constant MSETNX_VERSION. */
	private static final int MSETNX_VERSION = parseVersion("1.0.1");

	/**
	 * Set multiple keys to multiple values, only if none of the keys exist
	 * String.
	 * 
	 * @param key_or_value0
	 *            the key_or_value0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply msetnx(Object[] key_or_value0) throws RedisException
	{
		if (version < MSETNX_VERSION)
		{
			throw new RedisException("Server does not support MSETNX");
		}
		List list = new ArrayList();
		Collections.addAll(list, key_or_value0);
		return (IntegerReply) execute(MSETNX, new Command(MSETNX_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Msetnx_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply msetnx_(Object... arguments) throws RedisException
	{
		if (version < MSETNX_VERSION)
		{
			throw new RedisException("Server does not support MSETNX");
		}
		return (IntegerReply) execute(MSETNX, new Command(MSETNX_BYTES, arguments));
	}

	/** The Constant PSETEX. */
	private static final String PSETEX = "PSETEX";

	/** The Constant PSETEX_BYTES. */
	private static final byte[] PSETEX_BYTES = PSETEX.getBytes(Charsets.US_ASCII);

	/** The Constant PSETEX_VERSION. */
	private static final int PSETEX_VERSION = parseVersion("2.6.0");

	/**
	 * Set the value and expiration in milliseconds of a key String.
	 * 
	 * @param key0
	 *            the key0
	 * @param milliseconds1
	 *            the milliseconds1
	 * @param value2
	 *            the value2
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply psetex(Object key0, Object milliseconds1, Object value2) throws RedisException
	{
		if (version < PSETEX_VERSION)
		{
			throw new RedisException("Server does not support PSETEX");
		}
		return execute(PSETEX, new Command(PSETEX_BYTES, key0, milliseconds1, value2));
	}

	/** The Constant SET. */
	private static final String SET = "SET";

	/** The Constant SET_BYTES. */
	private static final byte[] SET_BYTES = SET.getBytes(Charsets.US_ASCII);

	/** The Constant SET_VERSION. */
	private static final int SET_VERSION = parseVersion("1.0.0");

	/**
	 * Set the string value of a key String.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply set(Object key0, Object value1) throws RedisException
	{
		if (version < SET_VERSION)
		{
			throw new RedisException("Server does not support SET");
		}
		return (StatusReply) execute(SET, new Command(SET_BYTES, key0, value1));
	}

	/** The Constant SETBIT. */
	private static final String SETBIT = "SETBIT";

	/** The Constant SETBIT_BYTES. */
	private static final byte[] SETBIT_BYTES = SETBIT.getBytes(Charsets.US_ASCII);

	/** The Constant SETBIT_VERSION. */
	private static final int SETBIT_VERSION = parseVersion("2.2.0");

	/**
	 * Sets or clears the bit at offset in the string value stored at key
	 * String.
	 * 
	 * @param key0
	 *            the key0
	 * @param offset1
	 *            the offset1
	 * @param value2
	 *            the value2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply setbit(Object key0, Object offset1, Object value2) throws RedisException
	{
		if (version < SETBIT_VERSION)
		{
			throw new RedisException("Server does not support SETBIT");
		}
		return (IntegerReply) execute(SETBIT, new Command(SETBIT_BYTES, key0, offset1, value2));
	}

	/** The Constant SETEX. */
	private static final String SETEX = "SETEX";

	/** The Constant SETEX_BYTES. */
	private static final byte[] SETEX_BYTES = SETEX.getBytes(Charsets.US_ASCII);

	/** The Constant SETEX_VERSION. */
	private static final int SETEX_VERSION = parseVersion("2.0.0");

	/**
	 * Set the value and expiration of a key String.
	 * 
	 * @param key0
	 *            the key0
	 * @param seconds1
	 *            the seconds1
	 * @param value2
	 *            the value2
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply setex(Object key0, Object seconds1, Object value2) throws RedisException
	{
		if (version < SETEX_VERSION)
		{
			throw new RedisException("Server does not support SETEX");
		}
		return (StatusReply) execute(SETEX, new Command(SETEX_BYTES, key0, seconds1, value2));
	}

	/** The Constant SETNX. */
	private static final String SETNX = "SETNX";

	/** The Constant SETNX_BYTES. */
	private static final byte[] SETNX_BYTES = SETNX.getBytes(Charsets.US_ASCII);

	/** The Constant SETNX_VERSION. */
	private static final int SETNX_VERSION = parseVersion("1.0.0");

	/**
	 * Set the value of a key, only if the key does not exist String.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply setnx(Object key0, Object value1) throws RedisException
	{
		if (version < SETNX_VERSION)
		{
			throw new RedisException("Server does not support SETNX");
		}
		return (IntegerReply) execute(SETNX, new Command(SETNX_BYTES, key0, value1));
	}

	/** The Constant SETRANGE. */
	private static final String SETRANGE = "SETRANGE";

	/** The Constant SETRANGE_BYTES. */
	private static final byte[] SETRANGE_BYTES = SETRANGE.getBytes(Charsets.US_ASCII);

	/** The Constant SETRANGE_VERSION. */
	private static final int SETRANGE_VERSION = parseVersion("2.2.0");

	/**
	 * Overwrite part of a string at key starting at the specified offset
	 * String.
	 * 
	 * @param key0
	 *            the key0
	 * @param offset1
	 *            the offset1
	 * @param value2
	 *            the value2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply setrange(Object key0, Object offset1, Object value2) throws RedisException
	{
		if (version < SETRANGE_VERSION)
		{
			throw new RedisException("Server does not support SETRANGE");
		}
		return (IntegerReply) execute(SETRANGE, new Command(SETRANGE_BYTES, key0, offset1, value2));
	}

	/** The Constant STRLEN. */
	private static final String STRLEN = "STRLEN";

	/** The Constant STRLEN_BYTES. */
	private static final byte[] STRLEN_BYTES = STRLEN.getBytes(Charsets.US_ASCII);

	/** The Constant STRLEN_VERSION. */
	private static final int STRLEN_VERSION = parseVersion("2.2.0");

	/**
	 * Get the length of the value stored in a key String.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply strlen(Object key0) throws RedisException
	{
		if (version < STRLEN_VERSION)
		{
			throw new RedisException("Server does not support STRLEN");
		}
		return (IntegerReply) execute(STRLEN, new Command(STRLEN_BYTES, key0));
	}

	/** The Constant ECHO. */
	private static final String ECHO = "ECHO";

	/** The Constant ECHO_BYTES. */
	private static final byte[] ECHO_BYTES = ECHO.getBytes(Charsets.US_ASCII);

	/** The Constant ECHO_VERSION. */
	private static final int ECHO_VERSION = parseVersion("1.0.0");

	/**
	 * Echo the given string Connection.
	 * 
	 * @param message0
	 *            the message0
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply echo(Object message0) throws RedisException
	{
		if (version < ECHO_VERSION)
		{
			throw new RedisException("Server does not support ECHO");
		}
		return (BulkReply) execute(ECHO, new Command(ECHO_BYTES, message0));
	}

	/** The Constant PING. */
	private static final String PING = "PING";

	/** The Constant PING_BYTES. */
	private static final byte[] PING_BYTES = PING.getBytes(Charsets.US_ASCII);

	/** The Constant PING_VERSION. */
	private static final int PING_VERSION = parseVersion("1.0.0");

	/**
	 * Ping the server Connection.
	 * 
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply ping() throws RedisException
	{
		if (version < PING_VERSION)
		{
			throw new RedisException("Server does not support PING");
		}
		return (StatusReply) execute(PING, new Command(PING_BYTES));
	}

	/** The Constant QUIT. */
	private static final String QUIT = "QUIT";

	/** The Constant QUIT_BYTES. */
	private static final byte[] QUIT_BYTES = QUIT.getBytes(Charsets.US_ASCII);

	/** The Constant QUIT_VERSION. */
	private static final int QUIT_VERSION = parseVersion("1.0.0");

	/**
	 * Close the connection Connection.
	 * 
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply quit() throws RedisException
	{
		if (version < QUIT_VERSION)
		{
			throw new RedisException("Server does not support QUIT");
		}
		return (StatusReply) execute(QUIT, new Command(QUIT_BYTES));
	}

	/** The Constant SELECT. */
	private static final String SELECT = "SELECT";

	/** The Constant SELECT_BYTES. */
	private static final byte[] SELECT_BYTES = SELECT.getBytes(Charsets.US_ASCII);

	/** The Constant SELECT_VERSION. */
	private static final int SELECT_VERSION = parseVersion("1.0.0");

	/**
	 * Change the selected database for the current connection Connection.
	 * 
	 * @param index0
	 *            the index0
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply select(Object index0) throws RedisException
	{
		if (version < SELECT_VERSION)
		{
			throw new RedisException("Server does not support SELECT");
		}
		return (StatusReply) execute(SELECT, new Command(SELECT_BYTES, index0));
	}

	/** The Constant BGREWRITEAOF. */
	private static final String BGREWRITEAOF = "BGREWRITEAOF";

	/** The Constant BGREWRITEAOF_BYTES. */
	private static final byte[] BGREWRITEAOF_BYTES = BGREWRITEAOF.getBytes(Charsets.US_ASCII);

	/** The Constant BGREWRITEAOF_VERSION. */
	private static final int BGREWRITEAOF_VERSION = parseVersion("1.0.0");

	/**
	 * Asynchronously rewrite the append-only file Server.
	 * 
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply bgrewriteaof() throws RedisException
	{
		if (version < BGREWRITEAOF_VERSION)
		{
			throw new RedisException("Server does not support BGREWRITEAOF");
		}
		return (StatusReply) execute(BGREWRITEAOF, new Command(BGREWRITEAOF_BYTES));
	}

	/** The Constant BGSAVE. */
	private static final String BGSAVE = "BGSAVE";

	/** The Constant BGSAVE_BYTES. */
	private static final byte[] BGSAVE_BYTES = BGSAVE.getBytes(Charsets.US_ASCII);

	/** The Constant BGSAVE_VERSION. */
	private static final int BGSAVE_VERSION = parseVersion("1.0.0");

	/**
	 * Asynchronously save the dataset to disk Server.
	 * 
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply bgsave() throws RedisException
	{
		if (version < BGSAVE_VERSION)
		{
			throw new RedisException("Server does not support BGSAVE");
		}
		return (StatusReply) execute(BGSAVE, new Command(BGSAVE_BYTES));
	}

	/** The Constant CLIENT_KILL. */
	private static final String CLIENT_KILL = "CLIENT";

	/** The Constant CLIENT_KILL2. */
	private static final String CLIENT_KILL2 = "KILL";

	/** The Constant CLIENT_KILL2_BYTES. */
	private static final byte[] CLIENT_KILL2_BYTES = CLIENT_KILL2.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_KILL_BYTES. */
	private static final byte[] CLIENT_KILL_BYTES = CLIENT_KILL.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_KILL_VERSION. */
	private static final int CLIENT_KILL_VERSION = parseVersion("2.4.0");

	/**
	 * Kill the connection of a client Server.
	 * 
	 * @param ip_port0
	 *            the ip_port0
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply client_kill(Object ip_port0) throws RedisException
	{
		if (version < CLIENT_KILL_VERSION)
		{
			throw new RedisException("Server does not support CLIENT_KILL");
		}
		return execute(CLIENT_KILL, new Command(CLIENT_KILL_BYTES, CLIENT_KILL2_BYTES, ip_port0));
	}

	/** The Constant CLIENT_LIST. */
	private static final String CLIENT_LIST = "CLIENT";

	/** The Constant CLIENT_LIST2. */
	private static final String CLIENT_LIST2 = "LIST";

	/** The Constant CLIENT_LIST2_BYTES. */
	private static final byte[] CLIENT_LIST2_BYTES = CLIENT_LIST2.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_LIST_BYTES. */
	private static final byte[] CLIENT_LIST_BYTES = CLIENT_LIST.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_LIST_VERSION. */
	private static final int CLIENT_LIST_VERSION = parseVersion("2.4.0");

	/**
	 * Get the list of client connections Server.
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply client_list() throws RedisException
	{
		if (version < CLIENT_LIST_VERSION)
		{
			throw new RedisException("Server does not support CLIENT_LIST");
		}
		return execute(CLIENT_LIST, new Command(CLIENT_LIST_BYTES, CLIENT_LIST2_BYTES));
	}

	/** The Constant CLIENT_GETNAME. */
	private static final String CLIENT_GETNAME = "CLIENT";

	/** The Constant CLIENT_GETNAME2. */
	private static final String CLIENT_GETNAME2 = "GETNAME";

	/** The Constant CLIENT_GETNAME2_BYTES. */
	private static final byte[] CLIENT_GETNAME2_BYTES = CLIENT_GETNAME2.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_GETNAME_BYTES. */
	private static final byte[] CLIENT_GETNAME_BYTES = CLIENT_GETNAME.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_GETNAME_VERSION. */
	private static final int CLIENT_GETNAME_VERSION = parseVersion("2.6.9");

	/**
	 * Get the current connection name Server.
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply client_getname() throws RedisException
	{
		if (version < CLIENT_GETNAME_VERSION)
		{
			throw new RedisException("Server does not support CLIENT_GETNAME");
		}
		return execute(CLIENT_GETNAME, new Command(CLIENT_GETNAME_BYTES, CLIENT_GETNAME2_BYTES));
	}

	/** The Constant CLIENT_SETNAME. */
	private static final String CLIENT_SETNAME = "CLIENT";

	/** The Constant CLIENT_SETNAME2. */
	private static final String CLIENT_SETNAME2 = "SETNAME";

	/** The Constant CLIENT_SETNAME2_BYTES. */
	private static final byte[] CLIENT_SETNAME2_BYTES = CLIENT_SETNAME2.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_SETNAME_BYTES. */
	private static final byte[] CLIENT_SETNAME_BYTES = CLIENT_SETNAME.getBytes(Charsets.US_ASCII);

	/** The Constant CLIENT_SETNAME_VERSION. */
	private static final int CLIENT_SETNAME_VERSION = parseVersion("2.6.9");

	/**
	 * Set the current connection name Server.
	 * 
	 * @param connection_name0
	 *            the connection_name0
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply client_setname(Object connection_name0) throws RedisException
	{
		if (version < CLIENT_SETNAME_VERSION)
		{
			throw new RedisException("Server does not support CLIENT_SETNAME");
		}
		return execute(CLIENT_SETNAME, new Command(CLIENT_SETNAME_BYTES, CLIENT_SETNAME2_BYTES, connection_name0));
	}

	/** The Constant CONFIG_GET. */
	private static final String CONFIG_GET = "CONFIG";

	/** The Constant CONFIG_GET2. */
	private static final String CONFIG_GET2 = "GET";

	/** The Constant CONFIG_GET2_BYTES. */
	private static final byte[] CONFIG_GET2_BYTES = CONFIG_GET2.getBytes(Charsets.US_ASCII);

	/** The Constant CONFIG_GET_BYTES. */
	private static final byte[] CONFIG_GET_BYTES = CONFIG_GET.getBytes(Charsets.US_ASCII);

	/** The Constant CONFIG_GET_VERSION. */
	private static final int CONFIG_GET_VERSION = parseVersion("2.0.0");

	/**
	 * Get the value of a configuration parameter Server.
	 * 
	 * @param parameter0
	 *            the parameter0
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply config_get(Object parameter0) throws RedisException
	{
		if (version < CONFIG_GET_VERSION)
		{
			throw new RedisException("Server does not support CONFIG_GET");
		}
		return execute(CONFIG_GET, new Command(CONFIG_GET_BYTES, CONFIG_GET2_BYTES, parameter0));
	}

	/** The Constant CONFIG_SET. */
	private static final String CONFIG_SET = "CONFIG";

	/** The Constant CONFIG_SET2. */
	private static final String CONFIG_SET2 = "SET";

	/** The Constant CONFIG_SET2_BYTES. */
	private static final byte[] CONFIG_SET2_BYTES = CONFIG_SET2.getBytes(Charsets.US_ASCII);

	/** The Constant CONFIG_SET_BYTES. */
	private static final byte[] CONFIG_SET_BYTES = CONFIG_SET.getBytes(Charsets.US_ASCII);

	/** The Constant CONFIG_SET_VERSION. */
	private static final int CONFIG_SET_VERSION = parseVersion("2.0.0");

	/**
	 * Set a configuration parameter to the given value Server.
	 * 
	 * @param parameter0
	 *            the parameter0
	 * @param value1
	 *            the value1
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */

	public Reply config_set(Object parameter0, Object value1) throws RedisException
	{
		if (version < CONFIG_SET_VERSION)
		{
			throw new RedisException("Server does not support CONFIG_SET");
		}
		return execute(CONFIG_SET, new Command(CONFIG_SET_BYTES, CONFIG_SET2_BYTES, parameter0, value1));
	}

	/** The Constant CONFIG_RESETSTAT. */
	private static final String CONFIG_RESETSTAT = "CONFIG";

	/** The Constant CONFIG_RESETSTAT2. */
	private static final String CONFIG_RESETSTAT2 = "RESETSTAT";

	/** The Constant CONFIG_RESETSTAT2_BYTES. */
	private static final byte[] CONFIG_RESETSTAT2_BYTES = CONFIG_RESETSTAT2.getBytes(Charsets.US_ASCII);

	/** The Constant CONFIG_RESETSTAT_BYTES. */
	private static final byte[] CONFIG_RESETSTAT_BYTES = CONFIG_RESETSTAT.getBytes(Charsets.US_ASCII);

	/** The Constant CONFIG_RESETSTAT_VERSION. */
	private static final int CONFIG_RESETSTAT_VERSION = parseVersion("2.0.0");

	/**
	 * Reset the stats returned by INFO Server.
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply config_resetstat() throws RedisException
	{
		if (version < CONFIG_RESETSTAT_VERSION)
		{
			throw new RedisException("Server does not support CONFIG_RESETSTAT");
		}
		return execute(CONFIG_RESETSTAT, new Command(CONFIG_RESETSTAT_BYTES, CONFIG_RESETSTAT2_BYTES));
	}

	/** The Constant DBSIZE. */
	private static final String DBSIZE = "DBSIZE";

	/** The Constant DBSIZE_BYTES. */
	private static final byte[] DBSIZE_BYTES = DBSIZE.getBytes(Charsets.US_ASCII);

	/** The Constant DBSIZE_VERSION. */
	private static final int DBSIZE_VERSION = parseVersion("1.0.0");

	/**
	 * Return the number of keys in the selected database Server.
	 * 
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply dbsize() throws RedisException
	{
		if (version < DBSIZE_VERSION)
		{
			throw new RedisException("Server does not support DBSIZE");
		}
		return (IntegerReply) execute(DBSIZE, new Command(DBSIZE_BYTES));
	}

	/** The Constant DEBUG_OBJECT. */
	private static final String DEBUG_OBJECT = "DEBUG";

	/** The Constant DEBUG_OBJECT2. */
	private static final String DEBUG_OBJECT2 = "OBJECT";

	/** The Constant DEBUG_OBJECT2_BYTES. */
	private static final byte[] DEBUG_OBJECT2_BYTES = DEBUG_OBJECT2.getBytes(Charsets.US_ASCII);

	/** The Constant DEBUG_OBJECT_BYTES. */
	private static final byte[] DEBUG_OBJECT_BYTES = DEBUG_OBJECT.getBytes(Charsets.US_ASCII);

	/** The Constant DEBUG_OBJECT_VERSION. */
	private static final int DEBUG_OBJECT_VERSION = parseVersion("1.0.0");

	/**
	 * Get debugging information about a key Server.
	 * 
	 * @param key0
	 *            the key0
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply debug_object(Object key0) throws RedisException
	{
		if (version < DEBUG_OBJECT_VERSION)
		{
			throw new RedisException("Server does not support DEBUG_OBJECT");
		}
		return execute(DEBUG_OBJECT, new Command(DEBUG_OBJECT_BYTES, DEBUG_OBJECT2_BYTES, key0));
	}

	/** The Constant DEBUG_SEGFAULT. */
	private static final String DEBUG_SEGFAULT = "DEBUG";

	/** The Constant DEBUG_SEGFAULT2. */
	private static final String DEBUG_SEGFAULT2 = "SEGFAULT";

	/** The Constant DEBUG_SEGFAULT2_BYTES. */
	private static final byte[] DEBUG_SEGFAULT2_BYTES = DEBUG_SEGFAULT2.getBytes(Charsets.US_ASCII);

	/** The Constant DEBUG_SEGFAULT_BYTES. */
	private static final byte[] DEBUG_SEGFAULT_BYTES = DEBUG_SEGFAULT.getBytes(Charsets.US_ASCII);

	/** The Constant DEBUG_SEGFAULT_VERSION. */
	private static final int DEBUG_SEGFAULT_VERSION = parseVersion("1.0.0");

	/**
	 * Make the server crash Server.
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply debug_segfault() throws RedisException
	{
		if (version < DEBUG_SEGFAULT_VERSION)
		{
			throw new RedisException("Server does not support DEBUG_SEGFAULT");
		}
		return execute(DEBUG_SEGFAULT, new Command(DEBUG_SEGFAULT_BYTES, DEBUG_SEGFAULT2_BYTES));
	}

	/** The Constant FLUSHALL. */
	private static final String FLUSHALL = "FLUSHALL";

	/** The Constant FLUSHALL_BYTES. */
	private static final byte[] FLUSHALL_BYTES = FLUSHALL.getBytes(Charsets.US_ASCII);

	/** The Constant FLUSHALL_VERSION. */
	private static final int FLUSHALL_VERSION = parseVersion("1.0.0");

	/**
	 * Remove all keys from all databases Server.
	 * 
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply flushall() throws RedisException
	{
		if (version < FLUSHALL_VERSION)
		{
			throw new RedisException("Server does not support FLUSHALL");
		}
		return (StatusReply) execute(FLUSHALL, new Command(FLUSHALL_BYTES));
	}

	/** The Constant FLUSHDB. */
	private static final String FLUSHDB = "FLUSHDB";

	/** The Constant FLUSHDB_BYTES. */
	private static final byte[] FLUSHDB_BYTES = FLUSHDB.getBytes(Charsets.US_ASCII);

	/** The Constant FLUSHDB_VERSION. */
	private static final int FLUSHDB_VERSION = parseVersion("1.0.0");

	/**
	 * Remove all keys from the current database Server.
	 * 
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply flushdb() throws RedisException
	{
		if (version < FLUSHDB_VERSION)
		{
			throw new RedisException("Server does not support FLUSHDB");
		}
		return (StatusReply) execute(FLUSHDB, new Command(FLUSHDB_BYTES));
	}

	/** The Constant INFO. */
	private static final String INFO = "INFO";

	/** The Constant INFO_BYTES. */
	private static final byte[] INFO_BYTES = INFO.getBytes(Charsets.US_ASCII);

	/** The Constant INFO_VERSION. */
	private static final int INFO_VERSION = parseVersion("1.0.0");

	/**
	 * Get information and statistics about the server Server.
	 * 
	 * @param section0
	 *            the section0
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply info(Object section0) throws RedisException
	{
		if (version < INFO_VERSION)
		{
			throw new RedisException("Server does not support INFO");
		}
		return (BulkReply) execute(INFO, new Command(INFO_BYTES, section0));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Info_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply info_(Object... arguments) throws RedisException
	{
		if (version < INFO_VERSION)
		{
			throw new RedisException("Server does not support INFO");
		}
		return (BulkReply) execute(INFO, new Command(INFO_BYTES, arguments));
	}

	/** The Constant LASTSAVE. */
	private static final String LASTSAVE = "LASTSAVE";

	/** The Constant LASTSAVE_BYTES. */
	private static final byte[] LASTSAVE_BYTES = LASTSAVE.getBytes(Charsets.US_ASCII);

	/** The Constant LASTSAVE_VERSION. */
	private static final int LASTSAVE_VERSION = parseVersion("1.0.0");

	/**
	 * Get the UNIX time stamp of the last successful save to disk Server.
	 * 
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply lastsave() throws RedisException
	{
		if (version < LASTSAVE_VERSION)
		{
			throw new RedisException("Server does not support LASTSAVE");
		}
		return (IntegerReply) execute(LASTSAVE, new Command(LASTSAVE_BYTES));
	}

	/** The Constant MONITOR. */
	private static final String MONITOR = "MONITOR";

	/** The Constant MONITOR_BYTES. */
	private static final byte[] MONITOR_BYTES = MONITOR.getBytes(Charsets.US_ASCII);

	/** The Constant MONITOR_VERSION. */
	private static final int MONITOR_VERSION = parseVersion("1.0.0");

	/**
	 * Listen for all requests received by the server in real time Server.
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply monitor() throws RedisException
	{
		if (version < MONITOR_VERSION)
		{
			throw new RedisException("Server does not support MONITOR");
		}
		return execute(MONITOR, new Command(MONITOR_BYTES));
	}

	/** The Constant SAVE. */
	private static final String SAVE = "SAVE";

	/** The Constant SAVE_BYTES. */
	private static final byte[] SAVE_BYTES = SAVE.getBytes(Charsets.US_ASCII);

	/** The Constant SAVE_VERSION. */
	private static final int SAVE_VERSION = parseVersion("1.0.0");

	/**
	 * Synchronously save the dataset to disk Server.
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply save() throws RedisException
	{
		if (version < SAVE_VERSION)
		{
			throw new RedisException("Server does not support SAVE");
		}
		return execute(SAVE, new Command(SAVE_BYTES));
	}

	/** The Constant SHUTDOWN. */
	private static final String SHUTDOWN = "SHUTDOWN";

	/** The Constant SHUTDOWN_BYTES. */
	private static final byte[] SHUTDOWN_BYTES = SHUTDOWN.getBytes(Charsets.US_ASCII);

	/** The Constant SHUTDOWN_VERSION. */
	private static final int SHUTDOWN_VERSION = parseVersion("1.0.0");

	/**
	 * Synchronously save the dataset to disk and then shut down the server
	 * Server.
	 * 
	 * @param NOSAVE0
	 *            the nOSAV e0
	 * @param SAVE1
	 *            the sAV e1
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply shutdown(Object NOSAVE0, Object SAVE1) throws RedisException
	{
		if (version < SHUTDOWN_VERSION)
		{
			throw new RedisException("Server does not support SHUTDOWN");
		}
		return (StatusReply) execute(SHUTDOWN, new Command(SHUTDOWN_BYTES, NOSAVE0, SAVE1));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Shutdown_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the status reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply shutdown_(Object... arguments) throws RedisException
	{
		if (version < SHUTDOWN_VERSION)
		{
			throw new RedisException("Server does not support SHUTDOWN");
		}
		return (StatusReply) execute(SHUTDOWN, new Command(SHUTDOWN_BYTES, arguments));
	}

	/** The Constant SLAVEOF. */
	private static final String SLAVEOF = "SLAVEOF";

	/** The Constant SLAVEOF_BYTES. */
	private static final byte[] SLAVEOF_BYTES = SLAVEOF.getBytes(Charsets.US_ASCII);

	/** The Constant SLAVEOF_VERSION. */
	private static final int SLAVEOF_VERSION = parseVersion("1.0.0");

	/**
	 * Make the server a slave of another instance, or promote it as master
	 * Server.
	 * 
	 * @param host0
	 *            the host0
	 * @param port1
	 *            the port1
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply slaveof(Object host0, Object port1) throws RedisException
	{
		if (version < SLAVEOF_VERSION)
		{
			throw new RedisException("Server does not support SLAVEOF");
		}
		return (StatusReply) execute(SLAVEOF, new Command(SLAVEOF_BYTES, host0, port1));
	}

	/** The Constant SLOWLOG. */
	private static final String SLOWLOG = "SLOWLOG";

	/** The Constant SLOWLOG_BYTES. */
	private static final byte[] SLOWLOG_BYTES = SLOWLOG.getBytes(Charsets.US_ASCII);

	/** The Constant SLOWLOG_VERSION. */
	private static final int SLOWLOG_VERSION = parseVersion("2.2.12");

	/**
	 * Manages the Redis slow queries log Server.
	 * 
	 * @param subcommand0
	 *            the subcommand0
	 * @param argument1
	 *            the argument1
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply slowlog(Object subcommand0, Object argument1) throws RedisException
	{
		if (version < SLOWLOG_VERSION)
		{
			throw new RedisException("Server does not support SLOWLOG");
		}
		return execute(SLOWLOG, new Command(SLOWLOG_BYTES, subcommand0, argument1));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Slowlog_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply slowlog_(Object... arguments) throws RedisException
	{
		if (version < SLOWLOG_VERSION)
		{
			throw new RedisException("Server does not support SLOWLOG");
		}
		return execute(SLOWLOG, new Command(SLOWLOG_BYTES, arguments));
	}

	/** The Constant SYNC. */
	private static final String SYNC = "SYNC";

	/** The Constant SYNC_BYTES. */
	private static final byte[] SYNC_BYTES = SYNC.getBytes(Charsets.US_ASCII);

	/** The Constant SYNC_VERSION. */
	private static final int SYNC_VERSION = parseVersion("1.0.0");

	/**
	 * Internal command used for replication Server.
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply sync() throws RedisException
	{
		if (version < SYNC_VERSION)
		{
			throw new RedisException("Server does not support SYNC");
		}
		return execute(SYNC, new Command(SYNC_BYTES));
	}

	/** The Constant TIME. */
	private static final String TIME = "TIME";

	/** The Constant TIME_BYTES. */
	private static final byte[] TIME_BYTES = TIME.getBytes(Charsets.US_ASCII);

	/** The Constant TIME_VERSION. */
	private static final int TIME_VERSION = parseVersion("2.6.0");

	/**
	 * Return the current server time Server.
	 * 
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply time() throws RedisException
	{
		if (version < TIME_VERSION)
		{
			throw new RedisException("Server does not support TIME");
		}
		return (MultiBulkReply) execute(TIME, new Command(TIME_BYTES));
	}

	/** The Constant BLPOP. */
	private static final String BLPOP = "BLPOP";

	/** The Constant BLPOP_BYTES. */
	private static final byte[] BLPOP_BYTES = BLPOP.getBytes(Charsets.US_ASCII);

	/** The Constant BLPOP_VERSION. */
	private static final int BLPOP_VERSION = parseVersion("2.0.0");

	/**
	 * Remove and get the first element in a list, or block until one is
	 * available List.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply blpop(Object[] key0) throws RedisException
	{
		if (version < BLPOP_VERSION)
		{
			throw new RedisException("Server does not support BLPOP");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (MultiBulkReply) execute(BLPOP, new Command(BLPOP_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Blpop_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply blpop_(Object... arguments) throws RedisException
	{
		if (version < BLPOP_VERSION)
		{
			throw new RedisException("Server does not support BLPOP");
		}
		return (MultiBulkReply) execute(BLPOP, new Command(BLPOP_BYTES, arguments));
	}

	/** The Constant BRPOP. */
	private static final String BRPOP = "BRPOP";

	/** The Constant BRPOP_BYTES. */
	private static final byte[] BRPOP_BYTES = BRPOP.getBytes(Charsets.US_ASCII);

	/** The Constant BRPOP_VERSION. */
	private static final int BRPOP_VERSION = parseVersion("2.0.0");

	/**
	 * Remove and get the last element in a list, or block until one is
	 * available List.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply brpop(Object[] key0) throws RedisException
	{
		if (version < BRPOP_VERSION)
		{
			throw new RedisException("Server does not support BRPOP");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (MultiBulkReply) execute(BRPOP, new Command(BRPOP_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Brpop_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply brpop_(Object... arguments) throws RedisException
	{
		if (version < BRPOP_VERSION)
		{
			throw new RedisException("Server does not support BRPOP");
		}
		return (MultiBulkReply) execute(BRPOP, new Command(BRPOP_BYTES, arguments));
	}

	/** The Constant BRPOPLPUSH. */
	private static final String BRPOPLPUSH = "BRPOPLPUSH";

	/** The Constant BRPOPLPUSH_BYTES. */
	private static final byte[] BRPOPLPUSH_BYTES = BRPOPLPUSH.getBytes(Charsets.US_ASCII);

	/** The Constant BRPOPLPUSH_VERSION. */
	private static final int BRPOPLPUSH_VERSION = parseVersion("2.2.0");

	/**
	 * Pop a value from a list, push it to another list and return it; or block
	 * until one is available List.
	 * 
	 * @param source0
	 *            the source0
	 * @param destination1
	 *            the destination1
	 * @param timeout2
	 *            the timeout2
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply brpoplpush(Object source0, Object destination1, Object timeout2) throws RedisException
	{
		if (version < BRPOPLPUSH_VERSION)
		{
			throw new RedisException("Server does not support BRPOPLPUSH");
		}
		return (BulkReply) execute(BRPOPLPUSH, new Command(BRPOPLPUSH_BYTES, source0, destination1, timeout2));
	}

	/** The Constant LINDEX. */
	private static final String LINDEX = "LINDEX";

	/** The Constant LINDEX_BYTES. */
	private static final byte[] LINDEX_BYTES = LINDEX.getBytes(Charsets.US_ASCII);

	/** The Constant LINDEX_VERSION. */
	private static final int LINDEX_VERSION = parseVersion("1.0.0");

	/**
	 * Get an element from a list by its index List.
	 * 
	 * @param key0
	 *            the key0
	 * @param index1
	 *            the index1
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply lindex(Object key0, Object index1) throws RedisException
	{
		if (version < LINDEX_VERSION)
		{
			throw new RedisException("Server does not support LINDEX");
		}
		return (BulkReply) execute(LINDEX, new Command(LINDEX_BYTES, key0, index1));
	}

	/** The Constant LINSERT. */
	private static final String LINSERT = "LINSERT";

	/** The Constant LINSERT_BYTES. */
	private static final byte[] LINSERT_BYTES = LINSERT.getBytes(Charsets.US_ASCII);

	/** The Constant LINSERT_VERSION. */
	private static final int LINSERT_VERSION = parseVersion("2.2.0");

	/**
	 * Insert an element before or after another element in a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @param where1
	 *            the where1
	 * @param pivot2
	 *            the pivot2
	 * @param value3
	 *            the value3
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply linsert(Object key0, Object where1, Object pivot2, Object value3) throws RedisException
	{
		if (version < LINSERT_VERSION)
		{
			throw new RedisException("Server does not support LINSERT");
		}
		List list = new ArrayList();
		list.add(key0);
		list.add(where1);
		list.add(pivot2);
		list.add(value3);
		return (IntegerReply) execute(LINSERT, new Command(LINSERT_BYTES, list.toArray(new Object[list.size()])));
	}

	/** The Constant LLEN. */
	private static final String LLEN = "LLEN";

	/** The Constant LLEN_BYTES. */
	private static final byte[] LLEN_BYTES = LLEN.getBytes(Charsets.US_ASCII);

	/** The Constant LLEN_VERSION. */
	private static final int LLEN_VERSION = parseVersion("1.0.0");

	/**
	 * Get the length of a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply llen(Object key0) throws RedisException
	{
		if (version < LLEN_VERSION)
		{
			throw new RedisException("Server does not support LLEN");
		}
		return (IntegerReply) execute(LLEN, new Command(LLEN_BYTES, key0));
	}

	/** The Constant LPOP. */
	private static final String LPOP = "LPOP";

	/** The Constant LPOP_BYTES. */
	private static final byte[] LPOP_BYTES = LPOP.getBytes(Charsets.US_ASCII);

	/** The Constant LPOP_VERSION. */
	private static final int LPOP_VERSION = parseVersion("1.0.0");

	/**
	 * Remove and get the first element in a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply lpop(Object key0) throws RedisException
	{
		if (version < LPOP_VERSION)
		{
			throw new RedisException("Server does not support LPOP");
		}
		return (BulkReply) execute(LPOP, new Command(LPOP_BYTES, key0));
	}

	/** The Constant LPUSH. */
	private static final String LPUSH = "LPUSH";

	/** The Constant LPUSH_BYTES. */
	private static final byte[] LPUSH_BYTES = LPUSH.getBytes(Charsets.US_ASCII);

	/** The Constant LPUSH_VERSION. */
	private static final int LPUSH_VERSION = parseVersion("1.0.0");

	/**
	 * Prepend one or multiple values to a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply lpush(Object key0, Object[] value1) throws RedisException
	{
		if (version < LPUSH_VERSION)
		{
			throw new RedisException("Server does not support LPUSH");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, value1);
		return (IntegerReply) execute(LPUSH, new Command(LPUSH_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Lpush_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply lpush_(Object... arguments) throws RedisException
	{
		if (version < LPUSH_VERSION)
		{
			throw new RedisException("Server does not support LPUSH");
		}
		return (IntegerReply) execute(LPUSH, new Command(LPUSH_BYTES, arguments));
	}

	/** The Constant LPUSHX. */
	private static final String LPUSHX = "LPUSHX";

	/** The Constant LPUSHX_BYTES. */
	private static final byte[] LPUSHX_BYTES = LPUSHX.getBytes(Charsets.US_ASCII);

	/** The Constant LPUSHX_VERSION. */
	private static final int LPUSHX_VERSION = parseVersion("2.2.0");

	/**
	 * Prepend a value to a list, only if the list exists List.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply lpushx(Object key0, Object value1) throws RedisException
	{
		if (version < LPUSHX_VERSION)
		{
			throw new RedisException("Server does not support LPUSHX");
		}
		return (IntegerReply) execute(LPUSHX, new Command(LPUSHX_BYTES, key0, value1));
	}

	/** The Constant LRANGE. */
	private static final String LRANGE = "LRANGE";

	/** The Constant LRANGE_BYTES. */
	private static final byte[] LRANGE_BYTES = LRANGE.getBytes(Charsets.US_ASCII);

	/** The Constant LRANGE_VERSION. */
	private static final int LRANGE_VERSION = parseVersion("1.0.0");

	/**
	 * Get a range of elements from a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @param start1
	 *            the start1
	 * @param stop2
	 *            the stop2
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply lrange(Object key0, Object start1, Object stop2) throws RedisException
	{
		if (version < LRANGE_VERSION)
		{
			throw new RedisException("Server does not support LRANGE");
		}
		return (MultiBulkReply) execute(LRANGE, new Command(LRANGE_BYTES, key0, start1, stop2));
	}

	/** The Constant LREM. */
	private static final String LREM = "LREM";

	/** The Constant LREM_BYTES. */
	private static final byte[] LREM_BYTES = LREM.getBytes(Charsets.US_ASCII);

	/** The Constant LREM_VERSION. */
	private static final int LREM_VERSION = parseVersion("1.0.0");

	/**
	 * Remove elements from a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @param count1
	 *            the count1
	 * @param value2
	 *            the value2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply lrem(Object key0, Object count1, Object value2) throws RedisException
	{
		if (version < LREM_VERSION)
		{
			throw new RedisException("Server does not support LREM");
		}
		return (IntegerReply) execute(LREM, new Command(LREM_BYTES, key0, count1, value2));
	}

	/** The Constant LSET. */
	private static final String LSET = "LSET";

	/** The Constant LSET_BYTES. */
	private static final byte[] LSET_BYTES = LSET.getBytes(Charsets.US_ASCII);

	/** The Constant LSET_VERSION. */
	private static final int LSET_VERSION = parseVersion("1.0.0");

	/**
	 * Set the value of an element in a list by its index List.
	 * 
	 * @param key0
	 *            the key0
	 * @param index1
	 *            the index1
	 * @param value2
	 *            the value2
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply lset(Object key0, Object index1, Object value2) throws RedisException
	{
		if (version < LSET_VERSION)
		{
			throw new RedisException("Server does not support LSET");
		}
		return (StatusReply) execute(LSET, new Command(LSET_BYTES, key0, index1, value2));
	}

	/** The Constant LTRIM. */
	private static final String LTRIM = "LTRIM";

	/** The Constant LTRIM_BYTES. */
	private static final byte[] LTRIM_BYTES = LTRIM.getBytes(Charsets.US_ASCII);

	/** The Constant LTRIM_VERSION. */
	private static final int LTRIM_VERSION = parseVersion("1.0.0");

	/**
	 * Trim a list to the specified range List.
	 * 
	 * @param key0
	 *            the key0
	 * @param start1
	 *            the start1
	 * @param stop2
	 *            the stop2
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply ltrim(Object key0, Object start1, Object stop2) throws RedisException
	{
		if (version < LTRIM_VERSION)
		{
			throw new RedisException("Server does not support LTRIM");
		}
		return (StatusReply) execute(LTRIM, new Command(LTRIM_BYTES, key0, start1, stop2));
	}

	/** The Constant RPOP. */
	private static final String RPOP = "RPOP";

	/** The Constant RPOP_BYTES. */
	private static final byte[] RPOP_BYTES = RPOP.getBytes(Charsets.US_ASCII);

	/** The Constant RPOP_VERSION. */
	private static final int RPOP_VERSION = parseVersion("1.0.0");

	/**
	 * Remove and get the last element in a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply rpop(Object key0) throws RedisException
	{
		if (version < RPOP_VERSION)
		{
			throw new RedisException("Server does not support RPOP");
		}
		return (BulkReply) execute(RPOP, new Command(RPOP_BYTES, key0));
	}

	/** The Constant RPOPLPUSH. */
	private static final String RPOPLPUSH = "RPOPLPUSH";

	/** The Constant RPOPLPUSH_BYTES. */
	private static final byte[] RPOPLPUSH_BYTES = RPOPLPUSH.getBytes(Charsets.US_ASCII);

	/** The Constant RPOPLPUSH_VERSION. */
	private static final int RPOPLPUSH_VERSION = parseVersion("1.2.0");

	/**
	 * Remove the last element in a list, append it to another list and return
	 * it List.
	 * 
	 * @param source0
	 *            the source0
	 * @param destination1
	 *            the destination1
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply rpoplpush(Object source0, Object destination1) throws RedisException
	{
		if (version < RPOPLPUSH_VERSION)
		{
			throw new RedisException("Server does not support RPOPLPUSH");
		}
		return (BulkReply) execute(RPOPLPUSH, new Command(RPOPLPUSH_BYTES, source0, destination1));
	}

	/** The Constant RPUSH. */
	private static final String RPUSH = "RPUSH";

	/** The Constant RPUSH_BYTES. */
	private static final byte[] RPUSH_BYTES = RPUSH.getBytes(Charsets.US_ASCII);

	/** The Constant RPUSH_VERSION. */
	private static final int RPUSH_VERSION = parseVersion("1.0.0");

	/**
	 * Append one or multiple values to a list List.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply rpush(Object key0, Object[] value1) throws RedisException
	{
		if (version < RPUSH_VERSION)
		{
			throw new RedisException("Server does not support RPUSH");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, value1);
		return (IntegerReply) execute(RPUSH, new Command(RPUSH_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Rpush_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply rpush_(Object... arguments) throws RedisException
	{
		if (version < RPUSH_VERSION)
		{
			throw new RedisException("Server does not support RPUSH");
		}
		return (IntegerReply) execute(RPUSH, new Command(RPUSH_BYTES, arguments));
	}

	/** The Constant RPUSHX. */
	private static final String RPUSHX = "RPUSHX";

	/** The Constant RPUSHX_BYTES. */
	private static final byte[] RPUSHX_BYTES = RPUSHX.getBytes(Charsets.US_ASCII);

	/** The Constant RPUSHX_VERSION. */
	private static final int RPUSHX_VERSION = parseVersion("2.2.0");

	/**
	 * Append a value to a list, only if the list exists List.
	 * 
	 * @param key0
	 *            the key0
	 * @param value1
	 *            the value1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply rpushx(Object key0, Object value1) throws RedisException
	{
		if (version < RPUSHX_VERSION)
		{
			throw new RedisException("Server does not support RPUSHX");
		}
		return (IntegerReply) execute(RPUSHX, new Command(RPUSHX_BYTES, key0, value1));
	}

	/** The Constant DEL. */
	private static final String DEL = "DEL";

	/** The Constant DEL_BYTES. */
	private static final byte[] DEL_BYTES = DEL.getBytes(Charsets.US_ASCII);

	/** The Constant DEL_VERSION. */
	private static final int DEL_VERSION = parseVersion("1.0.0");

	/**
	 * Delete a key Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply del(Object[] key0) throws RedisException
	{
		if (version < DEL_VERSION)
		{
			throw new RedisException("Server does not support DEL");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (IntegerReply) execute(DEL, new Command(DEL_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Del_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply del_(Object... arguments) throws RedisException
	{
		if (version < DEL_VERSION)
		{
			throw new RedisException("Server does not support DEL");
		}
		return (IntegerReply) execute(DEL, new Command(DEL_BYTES, arguments));
	}

	/** The Constant DUMP. */
	private static final String DUMP = "DUMP";

	/** The Constant DUMP_BYTES. */
	private static final byte[] DUMP_BYTES = DUMP.getBytes(Charsets.US_ASCII);

	/** The Constant DUMP_VERSION. */
	private static final int DUMP_VERSION = parseVersion("2.6.0");

	/**
	 * Return a serialized version of the value stored at the specified key.
	 * Generic
	 * 
	 * @param key0
	 *            the key0
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply dump(Object key0) throws RedisException
	{
		if (version < DUMP_VERSION)
		{
			throw new RedisException("Server does not support DUMP");
		}
		return (BulkReply) execute(DUMP, new Command(DUMP_BYTES, key0));
	}

	/** The Constant EXISTS. */
	private static final String EXISTS = "EXISTS";

	/** The Constant EXISTS_BYTES. */
	private static final byte[] EXISTS_BYTES = EXISTS.getBytes(Charsets.US_ASCII);

	/** The Constant EXISTS_VERSION. */
	private static final int EXISTS_VERSION = parseVersion("1.0.0");

	/**
	 * Determine if a key exists Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply exists(Object key0) throws RedisException
	{
		if (version < EXISTS_VERSION)
		{
			throw new RedisException("Server does not support EXISTS");
		}
		return (IntegerReply) execute(EXISTS, new Command(EXISTS_BYTES, key0));
	}

	/** The Constant EXPIRE. */
	private static final String EXPIRE = "EXPIRE";

	/** The Constant EXPIRE_BYTES. */
	private static final byte[] EXPIRE_BYTES = EXPIRE.getBytes(Charsets.US_ASCII);

	/** The Constant EXPIRE_VERSION. */
	private static final int EXPIRE_VERSION = parseVersion("1.0.0");

	/**
	 * Set a key's time to live in seconds Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param seconds1
	 *            the seconds1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply expire(Object key0, Object seconds1) throws RedisException
	{
		if (version < EXPIRE_VERSION)
		{
			throw new RedisException("Server does not support EXPIRE");
		}
		return (IntegerReply) execute(EXPIRE, new Command(EXPIRE_BYTES, key0, seconds1));
	}

	/** The Constant EXPIREAT. */
	private static final String EXPIREAT = "EXPIREAT";

	/** The Constant EXPIREAT_BYTES. */
	private static final byte[] EXPIREAT_BYTES = EXPIREAT.getBytes(Charsets.US_ASCII);

	/** The Constant EXPIREAT_VERSION. */
	private static final int EXPIREAT_VERSION = parseVersion("1.2.0");

	/**
	 * Set the expiration for a key as a UNIX timestamp Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param timestamp1
	 *            the timestamp1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply expireat(Object key0, Object timestamp1) throws RedisException
	{
		if (version < EXPIREAT_VERSION)
		{
			throw new RedisException("Server does not support EXPIREAT");
		}
		return (IntegerReply) execute(EXPIREAT, new Command(EXPIREAT_BYTES, key0, timestamp1));
	}

	/** The Constant KEYS. */
	private static final String KEYS = "KEYS";

	/** The Constant KEYS_BYTES. */
	private static final byte[] KEYS_BYTES = KEYS.getBytes(Charsets.US_ASCII);

	/** The Constant KEYS_VERSION. */
	private static final int KEYS_VERSION = parseVersion("1.0.0");

	/**
	 * Find all keys matching the given pattern Generic.
	 * 
	 * @param pattern0
	 *            the pattern0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply keys(Object pattern0) throws RedisException
	{
		if (version < KEYS_VERSION)
		{
			throw new RedisException("Server does not support KEYS");
		}
		return (MultiBulkReply) execute(KEYS, new Command(KEYS_BYTES, pattern0));
	}

	/** The Constant MIGRATE. */
	private static final String MIGRATE = "MIGRATE";

	/** The Constant MIGRATE_BYTES. */
	private static final byte[] MIGRATE_BYTES = MIGRATE.getBytes(Charsets.US_ASCII);

	/** The Constant MIGRATE_VERSION. */
	private static final int MIGRATE_VERSION = parseVersion("2.6.0");

	/**
	 * Atomically transfer a key from a Redis instance to another one. Generic
	 * 
	 * @param host0
	 *            the host0
	 * @param port1
	 *            the port1
	 * @param key2
	 *            the key2
	 * @param destination_db3
	 *            the destination_db3
	 * @param timeout4
	 *            the timeout4
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply migrate(Object host0, Object port1, Object key2, Object destination_db3, Object timeout4) throws RedisException
	{
		if (version < MIGRATE_VERSION)
		{
			throw new RedisException("Server does not support MIGRATE");
		}
		List list = new ArrayList();
		list.add(host0);
		list.add(port1);
		list.add(key2);
		list.add(destination_db3);
		list.add(timeout4);
		return (StatusReply) execute(MIGRATE, new Command(MIGRATE_BYTES, list.toArray(new Object[list.size()])));
	}

	/** The Constant MOVE. */
	private static final String MOVE = "MOVE";

	/** The Constant MOVE_BYTES. */
	private static final byte[] MOVE_BYTES = MOVE.getBytes(Charsets.US_ASCII);

	/** The Constant MOVE_VERSION. */
	private static final int MOVE_VERSION = parseVersion("1.0.0");

	/**
	 * Move a key to another database Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param db1
	 *            the db1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply move(Object key0, Object db1) throws RedisException
	{
		if (version < MOVE_VERSION)
		{
			throw new RedisException("Server does not support MOVE");
		}
		return (IntegerReply) execute(MOVE, new Command(MOVE_BYTES, key0, db1));
	}

	/** The Constant OBJECT. */
	private static final String OBJECT = "OBJECT";

	/** The Constant OBJECT_BYTES. */
	private static final byte[] OBJECT_BYTES = OBJECT.getBytes(Charsets.US_ASCII);

	/** The Constant OBJECT_VERSION. */
	private static final int OBJECT_VERSION = parseVersion("2.2.3");

	/**
	 * Inspect the internals of Redis objects Generic.
	 * 
	 * @param subcommand0
	 *            the subcommand0
	 * @param arguments1
	 *            the arguments1
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply object(Object subcommand0, Object[] arguments1) throws RedisException
	{
		if (version < OBJECT_VERSION)
		{
			throw new RedisException("Server does not support OBJECT");
		}
		List list = new ArrayList();
		list.add(subcommand0);
		Collections.addAll(list, arguments1);
		return execute(OBJECT, new Command(OBJECT_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Object_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply object_(Object... arguments) throws RedisException
	{
		if (version < OBJECT_VERSION)
		{
			throw new RedisException("Server does not support OBJECT");
		}
		return execute(OBJECT, new Command(OBJECT_BYTES, arguments));
	}

	/** The Constant PERSIST. */
	private static final String PERSIST = "PERSIST";

	/** The Constant PERSIST_BYTES. */
	private static final byte[] PERSIST_BYTES = PERSIST.getBytes(Charsets.US_ASCII);

	/** The Constant PERSIST_VERSION. */
	private static final int PERSIST_VERSION = parseVersion("2.2.0");

	/**
	 * Remove the expiration from a key Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply persist(Object key0) throws RedisException
	{
		if (version < PERSIST_VERSION)
		{
			throw new RedisException("Server does not support PERSIST");
		}
		return (IntegerReply) execute(PERSIST, new Command(PERSIST_BYTES, key0));
	}

	/** The Constant PEXPIRE. */
	private static final String PEXPIRE = "PEXPIRE";

	/** The Constant PEXPIRE_BYTES. */
	private static final byte[] PEXPIRE_BYTES = PEXPIRE.getBytes(Charsets.US_ASCII);

	/** The Constant PEXPIRE_VERSION. */
	private static final int PEXPIRE_VERSION = parseVersion("2.6.0");

	/**
	 * Set a key's time to live in milliseconds Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param milliseconds1
	 *            the milliseconds1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply pexpire(Object key0, Object milliseconds1) throws RedisException
	{
		if (version < PEXPIRE_VERSION)
		{
			throw new RedisException("Server does not support PEXPIRE");
		}
		return (IntegerReply) execute(PEXPIRE, new Command(PEXPIRE_BYTES, key0, milliseconds1));
	}

	/** The Constant PEXPIREAT. */
	private static final String PEXPIREAT = "PEXPIREAT";

	/** The Constant PEXPIREAT_BYTES. */
	private static final byte[] PEXPIREAT_BYTES = PEXPIREAT.getBytes(Charsets.US_ASCII);

	/** The Constant PEXPIREAT_VERSION. */
	private static final int PEXPIREAT_VERSION = parseVersion("2.6.0");

	/**
	 * Set the expiration for a key as a UNIX timestamp specified in
	 * milliseconds Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param milliseconds_timestamp1
	 *            the milliseconds_timestamp1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply pexpireat(Object key0, Object milliseconds_timestamp1) throws RedisException
	{
		if (version < PEXPIREAT_VERSION)
		{
			throw new RedisException("Server does not support PEXPIREAT");
		}
		return (IntegerReply) execute(PEXPIREAT, new Command(PEXPIREAT_BYTES, key0, milliseconds_timestamp1));
	}

	/** The Constant PTTL. */
	private static final String PTTL = "PTTL";

	/** The Constant PTTL_BYTES. */
	private static final byte[] PTTL_BYTES = PTTL.getBytes(Charsets.US_ASCII);

	/** The Constant PTTL_VERSION. */
	private static final int PTTL_VERSION = parseVersion("2.6.0");

	/**
	 * Get the time to live for a key in milliseconds Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply pttl(Object key0) throws RedisException
	{
		if (version < PTTL_VERSION)
		{
			throw new RedisException("Server does not support PTTL");
		}
		return (IntegerReply) execute(PTTL, new Command(PTTL_BYTES, key0));
	}

	/** The Constant RANDOMKEY. */
	private static final String RANDOMKEY = "RANDOMKEY";

	/** The Constant RANDOMKEY_BYTES. */
	private static final byte[] RANDOMKEY_BYTES = RANDOMKEY.getBytes(Charsets.US_ASCII);

	/** The Constant RANDOMKEY_VERSION. */
	private static final int RANDOMKEY_VERSION = parseVersion("1.0.0");

	/**
	 * Return a random key from the keyspace Generic.
	 * 
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply randomkey() throws RedisException
	{
		if (version < RANDOMKEY_VERSION)
		{
			throw new RedisException("Server does not support RANDOMKEY");
		}
		return (BulkReply) execute(RANDOMKEY, new Command(RANDOMKEY_BYTES));
	}

	/** The Constant RENAME. */
	private static final String RENAME = "RENAME";

	/** The Constant RENAME_BYTES. */
	private static final byte[] RENAME_BYTES = RENAME.getBytes(Charsets.US_ASCII);

	/** The Constant RENAME_VERSION. */
	private static final int RENAME_VERSION = parseVersion("1.0.0");

	/**
	 * Rename a key Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param newkey1
	 *            the newkey1
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply rename(Object key0, Object newkey1) throws RedisException
	{
		if (version < RENAME_VERSION)
		{
			throw new RedisException("Server does not support RENAME");
		}
		return (StatusReply) execute(RENAME, new Command(RENAME_BYTES, key0, newkey1));
	}

	/** The Constant RENAMENX. */
	private static final String RENAMENX = "RENAMENX";

	/** The Constant RENAMENX_BYTES. */
	private static final byte[] RENAMENX_BYTES = RENAMENX.getBytes(Charsets.US_ASCII);

	/** The Constant RENAMENX_VERSION. */
	private static final int RENAMENX_VERSION = parseVersion("1.0.0");

	/**
	 * Rename a key, only if the new key does not exist Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param newkey1
	 *            the newkey1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply renamenx(Object key0, Object newkey1) throws RedisException
	{
		if (version < RENAMENX_VERSION)
		{
			throw new RedisException("Server does not support RENAMENX");
		}
		return (IntegerReply) execute(RENAMENX, new Command(RENAMENX_BYTES, key0, newkey1));
	}

	/** The Constant RESTORE. */
	private static final String RESTORE = "RESTORE";

	/** The Constant RESTORE_BYTES. */
	private static final byte[] RESTORE_BYTES = RESTORE.getBytes(Charsets.US_ASCII);

	/** The Constant RESTORE_VERSION. */
	private static final int RESTORE_VERSION = parseVersion("2.6.0");

	/**
	 * Create a key using the provided serialized value, previously obtained
	 * using DUMP. Generic
	 * 
	 * @param key0
	 *            the key0
	 * @param ttl1
	 *            the ttl1
	 * @param serialized_value2
	 *            the serialized_value2
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply restore(Object key0, Object ttl1, Object serialized_value2) throws RedisException
	{
		if (version < RESTORE_VERSION)
		{
			throw new RedisException("Server does not support RESTORE");
		}
		return (StatusReply) execute(RESTORE, new Command(RESTORE_BYTES, key0, ttl1, serialized_value2));
	}

	/** The Constant SORT. */
	private static final String SORT = "SORT";

	/** The Constant SORT_BYTES. */
	private static final byte[] SORT_BYTES = SORT.getBytes(Charsets.US_ASCII);

	/** The Constant SORT_VERSION. */
	private static final int SORT_VERSION = parseVersion("1.0.0");

	/**
	 * Sort the elements in a list, set or sorted set Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @param pattern1
	 *            the pattern1
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply sort(Object key0, Object[] pattern1) throws RedisException
	{
		if (version < SORT_VERSION)
		{
			throw new RedisException("Server does not support SORT");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, pattern1);
		return execute(SORT, new Command(SORT_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sort_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply sort_(Object... arguments) throws RedisException
	{
		if (version < SORT_VERSION)
		{
			throw new RedisException("Server does not support SORT");
		}
		return execute(SORT, new Command(SORT_BYTES, arguments));
	}

	/** The Constant TTL. */
	private static final String TTL = "TTL";

	/** The Constant TTL_BYTES. */
	private static final byte[] TTL_BYTES = TTL.getBytes(Charsets.US_ASCII);

	/** The Constant TTL_VERSION. */
	private static final int TTL_VERSION = parseVersion("1.0.0");

	/**
	 * Get the time to live for a key Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply ttl(Object key0) throws RedisException
	{
		if (version < TTL_VERSION)
		{
			throw new RedisException("Server does not support TTL");
		}
		return (IntegerReply) execute(TTL, new Command(TTL_BYTES, key0));
	}

	/** The Constant TYPE. */
	private static final String TYPE = "TYPE";

	/** The Constant TYPE_BYTES. */
	private static final byte[] TYPE_BYTES = TYPE.getBytes(Charsets.US_ASCII);

	/** The Constant TYPE_VERSION. */
	private static final int TYPE_VERSION = parseVersion("1.0.0");

	/**
	 * Determine the type stored at key Generic.
	 * 
	 * @param key0
	 *            the key0
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply type(Object key0) throws RedisException
	{
		if (version < TYPE_VERSION)
		{
			throw new RedisException("Server does not support TYPE");
		}
		return (StatusReply) execute(TYPE, new Command(TYPE_BYTES, key0));
	}

	/** The Constant UNWATCH. */
	private static final String UNWATCH = "UNWATCH";

	/** The Constant UNWATCH_BYTES. */
	private static final byte[] UNWATCH_BYTES = UNWATCH.getBytes(Charsets.US_ASCII);

	/** The Constant UNWATCH_VERSION. */
	private static final int UNWATCH_VERSION = parseVersion("2.2.0");

	/**
	 * Forget about all watched keys Transactions.
	 * 
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply unwatch() throws RedisException
	{
		if (version < UNWATCH_VERSION)
		{
			throw new RedisException("Server does not support UNWATCH");
		}
		return (StatusReply) execute(UNWATCH, new Command(UNWATCH_BYTES));
	}

	/** The Constant WATCH. */
	private static final String WATCH = "WATCH";

	/** The Constant WATCH_BYTES. */
	private static final byte[] WATCH_BYTES = WATCH.getBytes(Charsets.US_ASCII);

	/** The Constant WATCH_VERSION. */
	private static final int WATCH_VERSION = parseVersion("2.2.0");

	/**
	 * Watch the given keys to determine execution of the MULTI/EXEC block
	 * Transactions.
	 * 
	 * @param key0
	 *            the key0
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply watch(Object[] key0) throws RedisException
	{
		if (version < WATCH_VERSION)
		{
			throw new RedisException("Server does not support WATCH");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (StatusReply) execute(WATCH, new Command(WATCH_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Watch_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the status reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply watch_(Object... arguments) throws RedisException
	{
		if (version < WATCH_VERSION)
		{
			throw new RedisException("Server does not support WATCH");
		}
		return (StatusReply) execute(WATCH, new Command(WATCH_BYTES, arguments));
	}

	/** The Constant EVAL. */
	private static final String EVAL = "EVAL";

	/** The Constant EVAL_BYTES. */
	private static final byte[] EVAL_BYTES = EVAL.getBytes(Charsets.US_ASCII);

	/** The Constant EVAL_VERSION. */
	private static final int EVAL_VERSION = parseVersion("2.6.0");

	/**
	 * Execute a Lua script server side Scripting.
	 * 
	 * @param script0
	 *            the script0
	 * @param numkeys1
	 *            the numkeys1
	 * @param key2
	 *            the key2
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply eval(Object script0, Object numkeys1, Object[] key2) throws RedisException
	{
		if (version < EVAL_VERSION)
		{
			throw new RedisException("Server does not support EVAL");
		}
		List list = new ArrayList();
		list.add(script0);
		list.add(numkeys1);
		Collections.addAll(list, key2);
		return execute(EVAL, new Command(EVAL_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Eval_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply eval_(Object... arguments) throws RedisException
	{
		if (version < EVAL_VERSION)
		{
			throw new RedisException("Server does not support EVAL");
		}
		return execute(EVAL, new Command(EVAL_BYTES, arguments));
	}

	/** The Constant EVALSHA. */
	private static final String EVALSHA = "EVALSHA";

	/** The Constant EVALSHA_BYTES. */
	private static final byte[] EVALSHA_BYTES = EVALSHA.getBytes(Charsets.US_ASCII);

	/** The Constant EVALSHA_VERSION. */
	private static final int EVALSHA_VERSION = parseVersion("2.6.0");

	/**
	 * Execute a Lua script server side Scripting.
	 * 
	 * @param sha10
	 *            the sha10
	 * @param numkeys1
	 *            the numkeys1
	 * @param key2
	 *            the key2
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply evalsha(Object sha10, Object numkeys1, Object[] key2) throws RedisException
	{
		if (version < EVALSHA_VERSION)
		{
			throw new RedisException("Server does not support EVALSHA");
		}
		List list = new ArrayList();
		list.add(sha10);
		list.add(numkeys1);
		Collections.addAll(list, key2);
		return execute(EVALSHA, new Command(EVALSHA_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Evalsha_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply evalsha_(Object... arguments) throws RedisException
	{
		if (version < EVALSHA_VERSION)
		{
			throw new RedisException("Server does not support EVALSHA");
		}
		return execute(EVALSHA, new Command(EVALSHA_BYTES, arguments));
	}

	/** The Constant SCRIPT_EXISTS. */
	private static final String SCRIPT_EXISTS = "SCRIPT";

	/** The Constant SCRIPT_EXISTS2. */
	private static final String SCRIPT_EXISTS2 = "EXISTS";

	/** The Constant SCRIPT_EXISTS2_BYTES. */
	private static final byte[] SCRIPT_EXISTS2_BYTES = SCRIPT_EXISTS2.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_EXISTS_BYTES. */
	private static final byte[] SCRIPT_EXISTS_BYTES = SCRIPT_EXISTS.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_EXISTS_VERSION. */
	private static final int SCRIPT_EXISTS_VERSION = parseVersion("2.6.0");

	/**
	 * Check existence of scripts in the script cache. Scripting
	 * 
	 * @param script0
	 *            the script0
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply script_exists(Object[] script0) throws RedisException
	{
		if (version < SCRIPT_EXISTS_VERSION)
		{
			throw new RedisException("Server does not support SCRIPT_EXISTS");
		}
		List list = new ArrayList();
		Collections.addAll(list, script0);
		return execute(SCRIPT_EXISTS, new Command(SCRIPT_EXISTS_BYTES, SCRIPT_EXISTS2_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Script_exists_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply script_exists_(Object... arguments) throws RedisException
	{
		if (version < SCRIPT_EXISTS_VERSION)
		{
			throw new RedisException("Server does not support SCRIPT_EXISTS");
		}
		return execute(SCRIPT_EXISTS, new Command(SCRIPT_EXISTS_BYTES, SCRIPT_EXISTS2_BYTES, arguments));
	}

	/** The Constant SCRIPT_FLUSH. */
	private static final String SCRIPT_FLUSH = "SCRIPT";

	/** The Constant SCRIPT_FLUSH2. */
	private static final String SCRIPT_FLUSH2 = "FLUSH";

	/** The Constant SCRIPT_FLUSH2_BYTES. */
	private static final byte[] SCRIPT_FLUSH2_BYTES = SCRIPT_FLUSH2.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_FLUSH_BYTES. */
	private static final byte[] SCRIPT_FLUSH_BYTES = SCRIPT_FLUSH.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_FLUSH_VERSION. */
	private static final int SCRIPT_FLUSH_VERSION = parseVersion("2.6.0");

	/**
	 * Remove all the scripts from the script cache. Scripting
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply script_flush() throws RedisException
	{
		if (version < SCRIPT_FLUSH_VERSION)
		{
			throw new RedisException("Server does not support SCRIPT_FLUSH");
		}
		return execute(SCRIPT_FLUSH, new Command(SCRIPT_FLUSH_BYTES, SCRIPT_FLUSH2_BYTES));
	}

	/** The Constant SCRIPT_KILL. */
	private static final String SCRIPT_KILL = "SCRIPT";

	/** The Constant SCRIPT_KILL2. */
	private static final String SCRIPT_KILL2 = "KILL";

	/** The Constant SCRIPT_KILL2_BYTES. */
	private static final byte[] SCRIPT_KILL2_BYTES = SCRIPT_KILL2.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_KILL_BYTES. */
	private static final byte[] SCRIPT_KILL_BYTES = SCRIPT_KILL.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_KILL_VERSION. */
	private static final int SCRIPT_KILL_VERSION = parseVersion("2.6.0");

	/**
	 * Kill the script currently in execution. Scripting
	 * 
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply script_kill() throws RedisException
	{
		if (version < SCRIPT_KILL_VERSION)
		{
			throw new RedisException("Server does not support SCRIPT_KILL");
		}
		return execute(SCRIPT_KILL, new Command(SCRIPT_KILL_BYTES, SCRIPT_KILL2_BYTES));
	}

	/** The Constant SCRIPT_LOAD. */
	private static final String SCRIPT_LOAD = "SCRIPT";

	/** The Constant SCRIPT_LOAD2. */
	private static final String SCRIPT_LOAD2 = "LOAD";

	/** The Constant SCRIPT_LOAD2_BYTES. */
	private static final byte[] SCRIPT_LOAD2_BYTES = SCRIPT_LOAD2.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_LOAD_BYTES. */
	private static final byte[] SCRIPT_LOAD_BYTES = SCRIPT_LOAD.getBytes(Charsets.US_ASCII);

	/** The Constant SCRIPT_LOAD_VERSION. */
	private static final int SCRIPT_LOAD_VERSION = parseVersion("2.6.0");

	/**
	 * Load the specified Lua script into the script cache. Scripting
	 * 
	 * @param script0
	 *            the script0
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply script_load(Object script0) throws RedisException
	{
		if (version < SCRIPT_LOAD_VERSION)
		{
			throw new RedisException("Server does not support SCRIPT_LOAD");
		}
		return execute(SCRIPT_LOAD, new Command(SCRIPT_LOAD_BYTES, SCRIPT_LOAD2_BYTES, script0));
	}

	/** The Constant HDEL. */
	private static final String HDEL = "HDEL";

	/** The Constant HDEL_BYTES. */
	private static final byte[] HDEL_BYTES = HDEL.getBytes(Charsets.US_ASCII);

	/** The Constant HDEL_VERSION. */
	private static final int HDEL_VERSION = parseVersion("2.0.0");

	/**
	 * Delete one or more hash fields Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply hdel(Object key0, Object[] field1) throws RedisException
	{
		if (version < HDEL_VERSION)
		{
			throw new RedisException("Server does not support HDEL");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, field1);
		return (IntegerReply) execute(HDEL, new Command(HDEL_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Hdel_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply hdel_(Object... arguments) throws RedisException
	{
		if (version < HDEL_VERSION)
		{
			throw new RedisException("Server does not support HDEL");
		}
		return (IntegerReply) execute(HDEL, new Command(HDEL_BYTES, arguments));
	}

	/** The Constant HEXISTS. */
	private static final String HEXISTS = "HEXISTS";

	/** The Constant HEXISTS_BYTES. */
	private static final byte[] HEXISTS_BYTES = HEXISTS.getBytes(Charsets.US_ASCII);

	/** The Constant HEXISTS_VERSION. */
	private static final int HEXISTS_VERSION = parseVersion("2.0.0");

	/**
	 * Determine if a hash field exists Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply hexists(Object key0, Object field1) throws RedisException
	{
		if (version < HEXISTS_VERSION)
		{
			throw new RedisException("Server does not support HEXISTS");
		}
		return (IntegerReply) execute(HEXISTS, new Command(HEXISTS_BYTES, key0, field1));
	}

	/** The Constant HGET. */
	private static final String HGET = "HGET";

	/** The Constant HGET_BYTES. */
	private static final byte[] HGET_BYTES = HGET.getBytes(Charsets.US_ASCII);

	/** The Constant HGET_VERSION. */
	private static final int HGET_VERSION = parseVersion("2.0.0");

	/**
	 * Get the value of a hash field Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply hget(Object key0, Object field1) throws RedisException
	{
		if (version < HGET_VERSION)
		{
			throw new RedisException("Server does not support HGET");
		}
		return (BulkReply) execute(HGET, new Command(HGET_BYTES, key0, field1));
	}

	/** The Constant HGETALL. */
	private static final String HGETALL = "HGETALL";

	/** The Constant HGETALL_BYTES. */
	private static final byte[] HGETALL_BYTES = HGETALL.getBytes(Charsets.US_ASCII);

	/** The Constant HGETALL_VERSION. */
	private static final int HGETALL_VERSION = parseVersion("2.0.0");

	/**
	 * Get all the fields and values in a hash Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply hgetall(Object key0) throws RedisException
	{
		if (version < HGETALL_VERSION)
		{
			throw new RedisException("Server does not support HGETALL");
		}
		return (MultiBulkReply) execute(HGETALL, new Command(HGETALL_BYTES, key0));
	}

	/** The Constant HINCRBY. */
	private static final String HINCRBY = "HINCRBY";

	/** The Constant HINCRBY_BYTES. */
	private static final byte[] HINCRBY_BYTES = HINCRBY.getBytes(Charsets.US_ASCII);

	/** The Constant HINCRBY_VERSION. */
	private static final int HINCRBY_VERSION = parseVersion("2.0.0");

	/**
	 * Increment the integer value of a hash field by the given number Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @param increment2
	 *            the increment2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply hincrby(Object key0, Object field1, Object increment2) throws RedisException
	{
		if (version < HINCRBY_VERSION)
		{
			throw new RedisException("Server does not support HINCRBY");
		}
		return (IntegerReply) execute(HINCRBY, new Command(HINCRBY_BYTES, key0, field1, increment2));
	}

	/** The Constant HINCRBYFLOAT. */
	private static final String HINCRBYFLOAT = "HINCRBYFLOAT";

	/** The Constant HINCRBYFLOAT_BYTES. */
	private static final byte[] HINCRBYFLOAT_BYTES = HINCRBYFLOAT.getBytes(Charsets.US_ASCII);

	/** The Constant HINCRBYFLOAT_VERSION. */
	private static final int HINCRBYFLOAT_VERSION = parseVersion("2.6.0");

	/**
	 * Increment the float value of a hash field by the given amount Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @param increment2
	 *            the increment2
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply hincrbyfloat(Object key0, Object field1, Object increment2) throws RedisException
	{
		if (version < HINCRBYFLOAT_VERSION)
		{
			throw new RedisException("Server does not support HINCRBYFLOAT");
		}
		return (BulkReply) execute(HINCRBYFLOAT, new Command(HINCRBYFLOAT_BYTES, key0, field1, increment2));
	}

	/** The Constant HKEYS. */
	private static final String HKEYS = "HKEYS";

	/** The Constant HKEYS_BYTES. */
	private static final byte[] HKEYS_BYTES = HKEYS.getBytes(Charsets.US_ASCII);

	/** The Constant HKEYS_VERSION. */
	private static final int HKEYS_VERSION = parseVersion("2.0.0");

	/**
	 * Get all the fields in a hash Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply hkeys(Object key0) throws RedisException
	{
		if (version < HKEYS_VERSION)
		{
			throw new RedisException("Server does not support HKEYS");
		}
		return (MultiBulkReply) execute(HKEYS, new Command(HKEYS_BYTES, key0));
	}

	/** The Constant HLEN. */
	private static final String HLEN = "HLEN";

	/** The Constant HLEN_BYTES. */
	private static final byte[] HLEN_BYTES = HLEN.getBytes(Charsets.US_ASCII);

	/** The Constant HLEN_VERSION. */
	private static final int HLEN_VERSION = parseVersion("2.0.0");

	/**
	 * Get the number of fields in a hash Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply hlen(Object key0) throws RedisException
	{
		if (version < HLEN_VERSION)
		{
			throw new RedisException("Server does not support HLEN");
		}
		return (IntegerReply) execute(HLEN, new Command(HLEN_BYTES, key0));
	}

	/** The Constant HMGET. */
	private static final String HMGET = "HMGET";

	/** The Constant HMGET_BYTES. */
	private static final byte[] HMGET_BYTES = HMGET.getBytes(Charsets.US_ASCII);

	/** The Constant HMGET_VERSION. */
	private static final int HMGET_VERSION = parseVersion("2.0.0");

	/**
	 * Get the values of all the given hash fields Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply hmget(Object key0, Object[] field1) throws RedisException
	{
		if (version < HMGET_VERSION)
		{
			throw new RedisException("Server does not support HMGET");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, field1);
		return (MultiBulkReply) execute(HMGET, new Command(HMGET_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Hmget_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply hmget_(Object... arguments) throws RedisException
	{
		if (version < HMGET_VERSION)
		{
			throw new RedisException("Server does not support HMGET");
		}
		return (MultiBulkReply) execute(HMGET, new Command(HMGET_BYTES, arguments));
	}

	/** The Constant HMSET. */
	private static final String HMSET = "HMSET";

	/** The Constant HMSET_BYTES. */
	private static final byte[] HMSET_BYTES = HMSET.getBytes(Charsets.US_ASCII);

	/** The Constant HMSET_VERSION. */
	private static final int HMSET_VERSION = parseVersion("2.0.0");

	/**
	 * Set multiple hash fields to multiple values Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field_or_value1
	 *            the field_or_value1
	 * @return StatusReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply hmset(Object key0, Object[] field_or_value1) throws RedisException
	{
		if (version < HMSET_VERSION)
		{
			throw new RedisException("Server does not support HMSET");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, field_or_value1);
		return (StatusReply) execute(HMSET, new Command(HMSET_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Hmset_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the status reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public StatusReply hmset_(Object... arguments) throws RedisException
	{
		if (version < HMSET_VERSION)
		{
			throw new RedisException("Server does not support HMSET");
		}
		return (StatusReply) execute(HMSET, new Command(HMSET_BYTES, arguments));
	}

	/** The Constant HSET. */
	private static final String HSET = "HSET";

	/** The Constant HSET_BYTES. */
	private static final byte[] HSET_BYTES = HSET.getBytes(Charsets.US_ASCII);

	/** The Constant HSET_VERSION. */
	private static final int HSET_VERSION = parseVersion("2.0.0");

	/**
	 * Set the string value of a hash field Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @param value2
	 *            the value2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply hset(Object key0, Object field1, Object value2) throws RedisException
	{
		if (version < HSET_VERSION)
		{
			throw new RedisException("Server does not support HSET");
		}
		return (IntegerReply) execute(HSET, new Command(HSET_BYTES, key0, field1, value2));
	}

	/** The Constant HSETNX. */
	private static final String HSETNX = "HSETNX";

	/** The Constant HSETNX_BYTES. */
	private static final byte[] HSETNX_BYTES = HSETNX.getBytes(Charsets.US_ASCII);

	/** The Constant HSETNX_VERSION. */
	private static final int HSETNX_VERSION = parseVersion("2.0.0");

	/**
	 * Set the value of a hash field, only if the field does not exist Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @param field1
	 *            the field1
	 * @param value2
	 *            the value2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply hsetnx(Object key0, Object field1, Object value2) throws RedisException
	{
		if (version < HSETNX_VERSION)
		{
			throw new RedisException("Server does not support HSETNX");
		}
		return (IntegerReply) execute(HSETNX, new Command(HSETNX_BYTES, key0, field1, value2));
	}

	/** The Constant HVALS. */
	private static final String HVALS = "HVALS";

	/** The Constant HVALS_BYTES. */
	private static final byte[] HVALS_BYTES = HVALS.getBytes(Charsets.US_ASCII);

	/** The Constant HVALS_VERSION. */
	private static final int HVALS_VERSION = parseVersion("2.0.0");

	/**
	 * Get all the values in a hash Hash.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply hvals(Object key0) throws RedisException
	{
		if (version < HVALS_VERSION)
		{
			throw new RedisException("Server does not support HVALS");
		}
		return (MultiBulkReply) execute(HVALS, new Command(HVALS_BYTES, key0));
	}

	/** The Constant PUBLISH. */
	private static final String PUBLISH = "PUBLISH";

	/** The Constant PUBLISH_BYTES. */
	private static final byte[] PUBLISH_BYTES = PUBLISH.getBytes(Charsets.US_ASCII);

	/** The Constant PUBLISH_VERSION. */
	private static final int PUBLISH_VERSION = parseVersion("2.0.0");

	/**
	 * Post a message to a channel Pubsub.
	 * 
	 * @param channel0
	 *            the channel0
	 * @param message1
	 *            the message1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply publish(Object channel0, Object message1) throws RedisException
	{
		if (version < PUBLISH_VERSION)
		{
			throw new RedisException("Server does not support PUBLISH");
		}
		return (IntegerReply) execute(PUBLISH, new Command(PUBLISH_BYTES, channel0, message1));
	}

	/** The Constant SADD. */
	private static final String SADD = "SADD";

	/** The Constant SADD_BYTES. */
	private static final byte[] SADD_BYTES = SADD.getBytes(Charsets.US_ASCII);

	/** The Constant SADD_VERSION. */
	private static final int SADD_VERSION = parseVersion("1.0.0");

	/**
	 * Add one or more members to a set Set.
	 * 
	 * @param key0
	 *            the key0
	 * @param member1
	 *            the member1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sadd(Object key0, Object[] member1) throws RedisException
	{
		if (version < SADD_VERSION)
		{
			throw new RedisException("Server does not support SADD");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, member1);
		return (IntegerReply) execute(SADD, new Command(SADD_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sadd_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sadd_(Object... arguments) throws RedisException
	{
		if (version < SADD_VERSION)
		{
			throw new RedisException("Server does not support SADD");
		}
		return (IntegerReply) execute(SADD, new Command(SADD_BYTES, arguments));
	}

	/** The Constant SCARD. */
	private static final String SCARD = "SCARD";

	/** The Constant SCARD_BYTES. */
	private static final byte[] SCARD_BYTES = SCARD.getBytes(Charsets.US_ASCII);

	/** The Constant SCARD_VERSION. */
	private static final int SCARD_VERSION = parseVersion("1.0.0");

	/**
	 * Get the number of members in a set Set.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply scard(Object key0) throws RedisException
	{
		if (version < SCARD_VERSION)
		{
			throw new RedisException("Server does not support SCARD");
		}
		return (IntegerReply) execute(SCARD, new Command(SCARD_BYTES, key0));
	}

	/** The Constant SDIFF. */
	private static final String SDIFF = "SDIFF";

	/** The Constant SDIFF_BYTES. */
	private static final byte[] SDIFF_BYTES = SDIFF.getBytes(Charsets.US_ASCII);

	/** The Constant SDIFF_VERSION. */
	private static final int SDIFF_VERSION = parseVersion("1.0.0");

	/**
	 * Subtract multiple sets Set.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply sdiff(Object[] key0) throws RedisException
	{
		if (version < SDIFF_VERSION)
		{
			throw new RedisException("Server does not support SDIFF");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (MultiBulkReply) execute(SDIFF, new Command(SDIFF_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sdiff_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply sdiff_(Object... arguments) throws RedisException
	{
		if (version < SDIFF_VERSION)
		{
			throw new RedisException("Server does not support SDIFF");
		}
		return (MultiBulkReply) execute(SDIFF, new Command(SDIFF_BYTES, arguments));
	}

	/** The Constant SDIFFSTORE. */
	private static final String SDIFFSTORE = "SDIFFSTORE";

	/** The Constant SDIFFSTORE_BYTES. */
	private static final byte[] SDIFFSTORE_BYTES = SDIFFSTORE.getBytes(Charsets.US_ASCII);

	/** The Constant SDIFFSTORE_VERSION. */
	private static final int SDIFFSTORE_VERSION = parseVersion("1.0.0");

	/**
	 * Subtract multiple sets and store the resulting set in a key Set.
	 * 
	 * @param destination0
	 *            the destination0
	 * @param key1
	 *            the key1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sdiffstore(Object destination0, Object[] key1) throws RedisException
	{
		if (version < SDIFFSTORE_VERSION)
		{
			throw new RedisException("Server does not support SDIFFSTORE");
		}
		List list = new ArrayList();
		list.add(destination0);
		Collections.addAll(list, key1);
		return (IntegerReply) execute(SDIFFSTORE, new Command(SDIFFSTORE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sdiffstore_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sdiffstore_(Object... arguments) throws RedisException
	{
		if (version < SDIFFSTORE_VERSION)
		{
			throw new RedisException("Server does not support SDIFFSTORE");
		}
		return (IntegerReply) execute(SDIFFSTORE, new Command(SDIFFSTORE_BYTES, arguments));
	}

	/** The Constant SINTER. */
	private static final String SINTER = "SINTER";

	/** The Constant SINTER_BYTES. */
	private static final byte[] SINTER_BYTES = SINTER.getBytes(Charsets.US_ASCII);

	/** The Constant SINTER_VERSION. */
	private static final int SINTER_VERSION = parseVersion("1.0.0");

	/**
	 * Intersect multiple sets Set.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply sinter(Object[] key0) throws RedisException
	{
		if (version < SINTER_VERSION)
		{
			throw new RedisException("Server does not support SINTER");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (MultiBulkReply) execute(SINTER, new Command(SINTER_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sinter_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply sinter_(Object... arguments) throws RedisException
	{
		if (version < SINTER_VERSION)
		{
			throw new RedisException("Server does not support SINTER");
		}
		return (MultiBulkReply) execute(SINTER, new Command(SINTER_BYTES, arguments));
	}

	/** The Constant SINTERSTORE. */
	private static final String SINTERSTORE = "SINTERSTORE";

	/** The Constant SINTERSTORE_BYTES. */
	private static final byte[] SINTERSTORE_BYTES = SINTERSTORE.getBytes(Charsets.US_ASCII);

	/** The Constant SINTERSTORE_VERSION. */
	private static final int SINTERSTORE_VERSION = parseVersion("1.0.0");

	/**
	 * Intersect multiple sets and store the resulting set in a key Set.
	 * 
	 * @param destination0
	 *            the destination0
	 * @param key1
	 *            the key1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sinterstore(Object destination0, Object[] key1) throws RedisException
	{
		if (version < SINTERSTORE_VERSION)
		{
			throw new RedisException("Server does not support SINTERSTORE");
		}
		List list = new ArrayList();
		list.add(destination0);
		Collections.addAll(list, key1);
		return (IntegerReply) execute(SINTERSTORE, new Command(SINTERSTORE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sinterstore_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sinterstore_(Object... arguments) throws RedisException
	{
		if (version < SINTERSTORE_VERSION)
		{
			throw new RedisException("Server does not support SINTERSTORE");
		}
		return (IntegerReply) execute(SINTERSTORE, new Command(SINTERSTORE_BYTES, arguments));
	}

	/** The Constant SISMEMBER. */
	private static final String SISMEMBER = "SISMEMBER";

	/** The Constant SISMEMBER_BYTES. */
	private static final byte[] SISMEMBER_BYTES = SISMEMBER.getBytes(Charsets.US_ASCII);

	/** The Constant SISMEMBER_VERSION. */
	private static final int SISMEMBER_VERSION = parseVersion("1.0.0");

	/**
	 * Determine if a given value is a member of a set Set.
	 * 
	 * @param key0
	 *            the key0
	 * @param member1
	 *            the member1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sismember(Object key0, Object member1) throws RedisException
	{
		if (version < SISMEMBER_VERSION)
		{
			throw new RedisException("Server does not support SISMEMBER");
		}
		return (IntegerReply) execute(SISMEMBER, new Command(SISMEMBER_BYTES, key0, member1));
	}

	/** The Constant SMEMBERS. */
	private static final String SMEMBERS = "SMEMBERS";

	/** The Constant SMEMBERS_BYTES. */
	private static final byte[] SMEMBERS_BYTES = SMEMBERS.getBytes(Charsets.US_ASCII);

	/** The Constant SMEMBERS_VERSION. */
	private static final int SMEMBERS_VERSION = parseVersion("1.0.0");

	/**
	 * Get all the members in a set Set.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply smembers(Object key0) throws RedisException
	{
		if (version < SMEMBERS_VERSION)
		{
			throw new RedisException("Server does not support SMEMBERS");
		}
		return (MultiBulkReply) execute(SMEMBERS, new Command(SMEMBERS_BYTES, key0));
	}

	/** The Constant SMOVE. */
	private static final String SMOVE = "SMOVE";

	/** The Constant SMOVE_BYTES. */
	private static final byte[] SMOVE_BYTES = SMOVE.getBytes(Charsets.US_ASCII);

	/** The Constant SMOVE_VERSION. */
	private static final int SMOVE_VERSION = parseVersion("1.0.0");

	/**
	 * Move a member from one set to another Set.
	 * 
	 * @param source0
	 *            the source0
	 * @param destination1
	 *            the destination1
	 * @param member2
	 *            the member2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply smove(Object source0, Object destination1, Object member2) throws RedisException
	{
		if (version < SMOVE_VERSION)
		{
			throw new RedisException("Server does not support SMOVE");
		}
		return (IntegerReply) execute(SMOVE, new Command(SMOVE_BYTES, source0, destination1, member2));
	}

	/** The Constant SPOP. */
	private static final String SPOP = "SPOP";

	/** The Constant SPOP_BYTES. */
	private static final byte[] SPOP_BYTES = SPOP.getBytes(Charsets.US_ASCII);

	/** The Constant SPOP_VERSION. */
	private static final int SPOP_VERSION = parseVersion("1.0.0");

	/**
	 * Remove and return a random member from a set Set.
	 * 
	 * @param key0
	 *            the key0
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply spop(Object key0) throws RedisException
	{
		if (version < SPOP_VERSION)
		{
			throw new RedisException("Server does not support SPOP");
		}
		return (BulkReply) execute(SPOP, new Command(SPOP_BYTES, key0));
	}

	/** The Constant SRANDMEMBER. */
	private static final String SRANDMEMBER = "SRANDMEMBER";

	/** The Constant SRANDMEMBER_BYTES. */
	private static final byte[] SRANDMEMBER_BYTES = SRANDMEMBER.getBytes(Charsets.US_ASCII);

	/** The Constant SRANDMEMBER_VERSION. */
	private static final int SRANDMEMBER_VERSION = parseVersion("1.0.0");

	/**
	 * Get one or multiple random members from a set Set.
	 * 
	 * @param key0
	 *            the key0
	 * @param count1
	 *            the count1
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply srandmember(Object key0, Object count1) throws RedisException
	{
		if (version < SRANDMEMBER_VERSION)
		{
			throw new RedisException("Server does not support SRANDMEMBER");
		}
		return execute(SRANDMEMBER, new Command(SRANDMEMBER_BYTES, key0, count1));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Srandmember_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply srandmember_(Object... arguments) throws RedisException
	{
		if (version < SRANDMEMBER_VERSION)
		{
			throw new RedisException("Server does not support SRANDMEMBER");
		}
		return execute(SRANDMEMBER, new Command(SRANDMEMBER_BYTES, arguments));
	}

	/** The Constant SREM. */
	private static final String SREM = "SREM";

	/** The Constant SREM_BYTES. */
	private static final byte[] SREM_BYTES = SREM.getBytes(Charsets.US_ASCII);

	/** The Constant SREM_VERSION. */
	private static final int SREM_VERSION = parseVersion("1.0.0");

	/**
	 * Remove one or more members from a set Set.
	 * 
	 * @param key0
	 *            the key0
	 * @param member1
	 *            the member1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply srem(Object key0, Object[] member1) throws RedisException
	{
		if (version < SREM_VERSION)
		{
			throw new RedisException("Server does not support SREM");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, member1);
		return (IntegerReply) execute(SREM, new Command(SREM_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Srem_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply srem_(Object... arguments) throws RedisException
	{
		if (version < SREM_VERSION)
		{
			throw new RedisException("Server does not support SREM");
		}
		return (IntegerReply) execute(SREM, new Command(SREM_BYTES, arguments));
	}

	/** The Constant SUNION. */
	private static final String SUNION = "SUNION";

	/** The Constant SUNION_BYTES. */
	private static final byte[] SUNION_BYTES = SUNION.getBytes(Charsets.US_ASCII);

	/** The Constant SUNION_VERSION. */
	private static final int SUNION_VERSION = parseVersion("1.0.0");

	/**
	 * Add multiple sets Set.
	 * 
	 * @param key0
	 *            the key0
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply sunion(Object[] key0) throws RedisException
	{
		if (version < SUNION_VERSION)
		{
			throw new RedisException("Server does not support SUNION");
		}
		List list = new ArrayList();
		Collections.addAll(list, key0);
		return (MultiBulkReply) execute(SUNION, new Command(SUNION_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sunion_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply sunion_(Object... arguments) throws RedisException
	{
		if (version < SUNION_VERSION)
		{
			throw new RedisException("Server does not support SUNION");
		}
		return (MultiBulkReply) execute(SUNION, new Command(SUNION_BYTES, arguments));
	}

	/** The Constant SUNIONSTORE. */
	private static final String SUNIONSTORE = "SUNIONSTORE";

	/** The Constant SUNIONSTORE_BYTES. */
	private static final byte[] SUNIONSTORE_BYTES = SUNIONSTORE.getBytes(Charsets.US_ASCII);

	/** The Constant SUNIONSTORE_VERSION. */
	private static final int SUNIONSTORE_VERSION = parseVersion("1.0.0");

	/**
	 * Add multiple sets and store the resulting set in a key Set.
	 * 
	 * @param destination0
	 *            the destination0
	 * @param key1
	 *            the key1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sunionstore(Object destination0, Object[] key1) throws RedisException
	{
		if (version < SUNIONSTORE_VERSION)
		{
			throw new RedisException("Server does not support SUNIONSTORE");
		}
		List list = new ArrayList();
		list.add(destination0);
		Collections.addAll(list, key1);
		return (IntegerReply) execute(SUNIONSTORE, new Command(SUNIONSTORE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Sunionstore_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply sunionstore_(Object... arguments) throws RedisException
	{
		if (version < SUNIONSTORE_VERSION)
		{
			throw new RedisException("Server does not support SUNIONSTORE");
		}
		return (IntegerReply) execute(SUNIONSTORE, new Command(SUNIONSTORE_BYTES, arguments));
	}

	/** The Constant ZADD. */
	private static final String ZADD = "ZADD";

	/** The Constant ZADD_BYTES. */
	private static final byte[] ZADD_BYTES = ZADD.getBytes(Charsets.US_ASCII);

	/** The Constant ZADD_VERSION. */
	private static final int ZADD_VERSION = parseVersion("1.2.0");

	/**
	 * Add one or more members to a sorted set, or update its score if it
	 * already exists Sorted_set.
	 * 
	 * @param args
	 *            the args
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zadd(Object[] args) throws RedisException
	{
		if (version < ZADD_VERSION)
		{
			throw new RedisException("Server does not support ZADD");
		}
		return (IntegerReply) execute(ZADD, new Command(ZADD_BYTES, args));
	}

	/** The Constant ZCARD. */
	private static final String ZCARD = "ZCARD";

	/** The Constant ZCARD_BYTES. */
	private static final byte[] ZCARD_BYTES = ZCARD.getBytes(Charsets.US_ASCII);

	/** The Constant ZCARD_VERSION. */
	private static final int ZCARD_VERSION = parseVersion("1.2.0");

	/**
	 * Get the number of members in a sorted set Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zcard(Object key0) throws RedisException
	{
		if (version < ZCARD_VERSION)
		{
			throw new RedisException("Server does not support ZCARD");
		}
		return (IntegerReply) execute(ZCARD, new Command(ZCARD_BYTES, key0));
	}

	/** The Constant ZCOUNT. */
	private static final String ZCOUNT = "ZCOUNT";

	/** The Constant ZCOUNT_BYTES. */
	private static final byte[] ZCOUNT_BYTES = ZCOUNT.getBytes(Charsets.US_ASCII);

	/** The Constant ZCOUNT_VERSION. */
	private static final int ZCOUNT_VERSION = parseVersion("2.0.0");

	/**
	 * Count the members in a sorted set with scores within the given values
	 * Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param min1
	 *            the min1
	 * @param max2
	 *            the max2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zcount(Object key0, Object min1, Object max2) throws RedisException
	{
		if (version < ZCOUNT_VERSION)
		{
			throw new RedisException("Server does not support ZCOUNT");
		}
		return (IntegerReply) execute(ZCOUNT, new Command(ZCOUNT_BYTES, key0, min1, max2));
	}

	/** The Constant ZINCRBY. */
	private static final String ZINCRBY = "ZINCRBY";

	/** The Constant ZINCRBY_BYTES. */
	private static final byte[] ZINCRBY_BYTES = ZINCRBY.getBytes(Charsets.US_ASCII);

	/** The Constant ZINCRBY_VERSION. */
	private static final int ZINCRBY_VERSION = parseVersion("1.2.0");

	/**
	 * Increment the score of a member in a sorted set Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param increment1
	 *            the increment1
	 * @param member2
	 *            the member2
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply zincrby(Object key0, Object increment1, Object member2) throws RedisException
	{
		if (version < ZINCRBY_VERSION)
		{
			throw new RedisException("Server does not support ZINCRBY");
		}
		return (BulkReply) execute(ZINCRBY, new Command(ZINCRBY_BYTES, key0, increment1, member2));
	}

	/** The Constant ZINTERSTORE. */
	private static final String ZINTERSTORE = "ZINTERSTORE";

	/** The Constant ZINTERSTORE_BYTES. */
	private static final byte[] ZINTERSTORE_BYTES = ZINTERSTORE.getBytes(Charsets.US_ASCII);

	/** The Constant ZINTERSTORE_VERSION. */
	private static final int ZINTERSTORE_VERSION = parseVersion("2.0.0");

	/**
	 * Intersect multiple sorted sets and store the resulting sorted set in a
	 * new key Sorted_set.
	 * 
	 * @param destination0
	 *            the destination0
	 * @param numkeys1
	 *            the numkeys1
	 * @param key2
	 *            the key2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zinterstore(Object destination0, Object numkeys1, Object[] key2) throws RedisException
	{
		if (version < ZINTERSTORE_VERSION)
		{
			throw new RedisException("Server does not support ZINTERSTORE");
		}
		List list = new ArrayList();
		list.add(destination0);
		list.add(numkeys1);
		Collections.addAll(list, key2);
		return (IntegerReply) execute(ZINTERSTORE, new Command(ZINTERSTORE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Zinterstore_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zinterstore_(Object... arguments) throws RedisException
	{
		if (version < ZINTERSTORE_VERSION)
		{
			throw new RedisException("Server does not support ZINTERSTORE");
		}
		return (IntegerReply) execute(ZINTERSTORE, new Command(ZINTERSTORE_BYTES, arguments));
	}

	/** The Constant ZRANGE. */
	private static final String ZRANGE = "ZRANGE";

	/** The Constant ZRANGE_BYTES. */
	private static final byte[] ZRANGE_BYTES = ZRANGE.getBytes(Charsets.US_ASCII);

	/** The Constant ZRANGE_VERSION. */
	private static final int ZRANGE_VERSION = parseVersion("1.2.0");

	/**
	 * Return a range of members in a sorted set, by index Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param start1
	 *            the start1
	 * @param stop2
	 *            the stop2
	 * @param withscores3
	 *            the withscores3
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrange(Object key0, Object start1, Object stop2, Object withscores3) throws RedisException
	{
		if (version < ZRANGE_VERSION)
		{
			throw new RedisException("Server does not support ZRANGE");
		}
		List list = new ArrayList();
		list.add(key0);
		list.add(start1);
		list.add(stop2);
		if (withscores3 != null)
		{
			list.add(withscores3);
		}
		return (MultiBulkReply) execute(ZRANGE, new Command(ZRANGE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Zrange_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrange_(Object... arguments) throws RedisException
	{
		if (version < ZRANGE_VERSION)
		{
			throw new RedisException("Server does not support ZRANGE");
		}
		return (MultiBulkReply) execute(ZRANGE, new Command(ZRANGE_BYTES, arguments));
	}

	/** The Constant ZRANGEBYSCORE. */
	private static final String ZRANGEBYSCORE = "ZRANGEBYSCORE";

	/** The Constant ZRANGEBYSCORE_BYTES. */
	private static final byte[] ZRANGEBYSCORE_BYTES = ZRANGEBYSCORE.getBytes(Charsets.US_ASCII);

	/** The Constant ZRANGEBYSCORE_VERSION. */
	private static final int ZRANGEBYSCORE_VERSION = parseVersion("1.0.5");

	/**
	 * Return a range of members in a sorted set, by score Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param min1
	 *            the min1
	 * @param max2
	 *            the max2
	 * @param withscores3
	 *            the withscores3
	 * @param offset_or_count4
	 *            the offset_or_count4
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrangebyscore(Object key0, Object min1, Object max2, Object withscores3, Object[] offset_or_count4) throws RedisException
	{
		if (version < ZRANGEBYSCORE_VERSION)
		{
			throw new RedisException("Server does not support ZRANGEBYSCORE");
		}
		List list = new ArrayList();
		list.add(key0);
		list.add(min1);
		list.add(max2);
		if (withscores3 != null)
		{
			list.add(withscores3);
		}
		Collections.addAll(list, offset_or_count4);
		return (MultiBulkReply) execute(ZRANGEBYSCORE, new Command(ZRANGEBYSCORE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Zrangebyscore_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrangebyscore_(Object... arguments) throws RedisException
	{
		if (version < ZRANGEBYSCORE_VERSION)
		{
			throw new RedisException("Server does not support ZRANGEBYSCORE");
		}
		return (MultiBulkReply) execute(ZRANGEBYSCORE, new Command(ZRANGEBYSCORE_BYTES, arguments));
	}

	/** The Constant ZRANK. */
	private static final String ZRANK = "ZRANK";

	/** The Constant ZRANK_BYTES. */
	private static final byte[] ZRANK_BYTES = ZRANK.getBytes(Charsets.US_ASCII);

	/** The Constant ZRANK_VERSION. */
	private static final int ZRANK_VERSION = parseVersion("2.0.0");

	/**
	 * Determine the index of a member in a sorted set Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param member1
	 *            the member1
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply zrank(Object key0, Object member1) throws RedisException
	{
		if (version < ZRANK_VERSION)
		{
			throw new RedisException("Server does not support ZRANK");
		}
		return execute(ZRANK, new Command(ZRANK_BYTES, key0, member1));
	}

	/** The Constant ZREM. */
	private static final String ZREM = "ZREM";

	/** The Constant ZREM_BYTES. */
	private static final byte[] ZREM_BYTES = ZREM.getBytes(Charsets.US_ASCII);

	/** The Constant ZREM_VERSION. */
	private static final int ZREM_VERSION = parseVersion("1.2.0");

	/**
	 * Remove one or more members from a sorted set Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param member1
	 *            the member1
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zrem(Object key0, Object[] member1) throws RedisException
	{
		if (version < ZREM_VERSION)
		{
			throw new RedisException("Server does not support ZREM");
		}
		List list = new ArrayList();
		list.add(key0);
		Collections.addAll(list, member1);
		return (IntegerReply) execute(ZREM, new Command(ZREM_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Zrem_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zrem_(Object... arguments) throws RedisException
	{
		if (version < ZREM_VERSION)
		{
			throw new RedisException("Server does not support ZREM");
		}
		return (IntegerReply) execute(ZREM, new Command(ZREM_BYTES, arguments));
	}

	/** The Constant ZREMRANGEBYRANK. */
	private static final String ZREMRANGEBYRANK = "ZREMRANGEBYRANK";

	/** The Constant ZREMRANGEBYRANK_BYTES. */
	private static final byte[] ZREMRANGEBYRANK_BYTES = ZREMRANGEBYRANK.getBytes(Charsets.US_ASCII);

	/** The Constant ZREMRANGEBYRANK_VERSION. */
	private static final int ZREMRANGEBYRANK_VERSION = parseVersion("2.0.0");

	/**
	 * Remove all members in a sorted set within the given indexes Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param start1
	 *            the start1
	 * @param stop2
	 *            the stop2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zremrangebyrank(Object key0, Object start1, Object stop2) throws RedisException
	{
		if (version < ZREMRANGEBYRANK_VERSION)
		{
			throw new RedisException("Server does not support ZREMRANGEBYRANK");
		}
		return (IntegerReply) execute(ZREMRANGEBYRANK, new Command(ZREMRANGEBYRANK_BYTES, key0, start1, stop2));
	}

	/** The Constant ZREMRANGEBYSCORE. */
	private static final String ZREMRANGEBYSCORE = "ZREMRANGEBYSCORE";

	/** The Constant ZREMRANGEBYSCORE_BYTES. */
	private static final byte[] ZREMRANGEBYSCORE_BYTES = ZREMRANGEBYSCORE.getBytes(Charsets.US_ASCII);

	/** The Constant ZREMRANGEBYSCORE_VERSION. */
	private static final int ZREMRANGEBYSCORE_VERSION = parseVersion("1.2.0");

	/**
	 * Remove all members in a sorted set within the given scores Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param min1
	 *            the min1
	 * @param max2
	 *            the max2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zremrangebyscore(Object key0, Object min1, Object max2) throws RedisException
	{
		if (version < ZREMRANGEBYSCORE_VERSION)
		{
			throw new RedisException("Server does not support ZREMRANGEBYSCORE");
		}
		return (IntegerReply) execute(ZREMRANGEBYSCORE, new Command(ZREMRANGEBYSCORE_BYTES, key0, min1, max2));
	}

	/** The Constant ZREVRANGE. */
	private static final String ZREVRANGE = "ZREVRANGE";

	/** The Constant ZREVRANGE_BYTES. */
	private static final byte[] ZREVRANGE_BYTES = ZREVRANGE.getBytes(Charsets.US_ASCII);

	/** The Constant ZREVRANGE_VERSION. */
	private static final int ZREVRANGE_VERSION = parseVersion("1.2.0");

	/**
	 * Return a range of members in a sorted set, by index, with scores ordered
	 * from high to low Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param start1
	 *            the start1
	 * @param stop2
	 *            the stop2
	 * @param withscores3
	 *            the withscores3
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrevrange(Object key0, Object start1, Object stop2, Object withscores3) throws RedisException
	{
		if (version < ZREVRANGE_VERSION)
		{
			throw new RedisException("Server does not support ZREVRANGE");
		}
		List list = new ArrayList();
		list.add(key0);
		list.add(start1);
		list.add(stop2);
		if (withscores3 != null)
		{
			list.add(withscores3);
		}
		return (MultiBulkReply) execute(ZREVRANGE, new Command(ZREVRANGE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Zrevrange_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrevrange_(Object... arguments) throws RedisException
	{
		if (version < ZREVRANGE_VERSION)
		{
			throw new RedisException("Server does not support ZREVRANGE");
		}
		return (MultiBulkReply) execute(ZREVRANGE, new Command(ZREVRANGE_BYTES, arguments));
	}

	/** The Constant ZREVRANGEBYSCORE. */
	private static final String ZREVRANGEBYSCORE = "ZREVRANGEBYSCORE";

	/** The Constant ZREVRANGEBYSCORE_BYTES. */
	private static final byte[] ZREVRANGEBYSCORE_BYTES = ZREVRANGEBYSCORE.getBytes(Charsets.US_ASCII);

	/** The Constant ZREVRANGEBYSCORE_VERSION. */
	private static final int ZREVRANGEBYSCORE_VERSION = parseVersion("2.2.0");

	/**
	 * Return a range of members in a sorted set, by score, with scores ordered
	 * from high to low Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param max1
	 *            the max1
	 * @param min2
	 *            the min2
	 * @param withscores3
	 *            the withscores3
	 * @param offset_or_count4
	 *            the offset_or_count4
	 * @return MultiBulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrevrangebyscore(Object key0, Object max1, Object min2, Object withscores3, Object[] offset_or_count4) throws RedisException
	{
		if (version < ZREVRANGEBYSCORE_VERSION)
		{
			throw new RedisException("Server does not support ZREVRANGEBYSCORE");
		}
		List list = new ArrayList();
		list.add(key0);
		list.add(max1);
		list.add(min2);
		if (withscores3 != null)
		{
			list.add(withscores3);
		}
		Collections.addAll(list, offset_or_count4);
		return (MultiBulkReply) execute(ZREVRANGEBYSCORE, new Command(ZREVRANGEBYSCORE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Zrevrangebyscore_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the multi bulk reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public MultiBulkReply zrevrangebyscore_(Object... arguments) throws RedisException
	{
		if (version < ZREVRANGEBYSCORE_VERSION)
		{
			throw new RedisException("Server does not support ZREVRANGEBYSCORE");
		}
		return (MultiBulkReply) execute(ZREVRANGEBYSCORE, new Command(ZREVRANGEBYSCORE_BYTES, arguments));
	}

	/** The Constant ZREVRANK. */
	private static final String ZREVRANK = "ZREVRANK";

	/** The Constant ZREVRANK_BYTES. */
	private static final byte[] ZREVRANK_BYTES = ZREVRANK.getBytes(Charsets.US_ASCII);

	/** The Constant ZREVRANK_VERSION. */
	private static final int ZREVRANK_VERSION = parseVersion("2.0.0");

	/**
	 * Determine the index of a member in a sorted set, with scores ordered from
	 * high to low Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param member1
	 *            the member1
	 * @return Reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public Reply zrevrank(Object key0, Object member1) throws RedisException
	{
		if (version < ZREVRANK_VERSION)
		{
			throw new RedisException("Server does not support ZREVRANK");
		}
		return execute(ZREVRANK, new Command(ZREVRANK_BYTES, key0, member1));
	}

	/** The Constant ZSCORE. */
	private static final String ZSCORE = "ZSCORE";

	/** The Constant ZSCORE_BYTES. */
	private static final byte[] ZSCORE_BYTES = ZSCORE.getBytes(Charsets.US_ASCII);

	/** The Constant ZSCORE_VERSION. */
	private static final int ZSCORE_VERSION = parseVersion("1.2.0");

	/**
	 * Get the score associated with the given member in a sorted set
	 * Sorted_set.
	 * 
	 * @param key0
	 *            the key0
	 * @param member1
	 *            the member1
	 * @return BulkReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public BulkReply zscore(Object key0, Object member1) throws RedisException
	{
		if (version < ZSCORE_VERSION)
		{
			throw new RedisException("Server does not support ZSCORE");
		}
		return (BulkReply) execute(ZSCORE, new Command(ZSCORE_BYTES, key0, member1));
	}

	/** The Constant ZUNIONSTORE. */
	private static final String ZUNIONSTORE = "ZUNIONSTORE";

	/** The Constant ZUNIONSTORE_BYTES. */
	private static final byte[] ZUNIONSTORE_BYTES = ZUNIONSTORE.getBytes(Charsets.US_ASCII);

	/** The Constant ZUNIONSTORE_VERSION. */
	private static final int ZUNIONSTORE_VERSION = parseVersion("2.0.0");

	/**
	 * Add multiple sorted sets and store the resulting sorted set in a new key
	 * Sorted_set.
	 * 
	 * @param destination0
	 *            the destination0
	 * @param numkeys1
	 *            the numkeys1
	 * @param key2
	 *            the key2
	 * @return IntegerReply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zunionstore(Object destination0, Object numkeys1, Object[] key2) throws RedisException
	{
		if (version < ZUNIONSTORE_VERSION)
		{
			throw new RedisException("Server does not support ZUNIONSTORE");
		}
		List list = new ArrayList();
		list.add(destination0);
		list.add(numkeys1);
		Collections.addAll(list, key2);
		return (IntegerReply) execute(ZUNIONSTORE, new Command(ZUNIONSTORE_BYTES, list.toArray(new Object[list.size()])));
	}

	// Varargs version to simplify commands with optional or multiple arguments
	/**
	 * Zunionstore_.
	 * 
	 * @param arguments
	 *            the arguments
	 * @return the integer reply
	 * @throws RedisException
	 *             the redis exception
	 */
	public IntegerReply zunionstore_(Object... arguments) throws RedisException
	{
		if (version < ZUNIONSTORE_VERSION)
		{
			throw new RedisException("Server does not support ZUNIONSTORE");
		}
		return (IntegerReply) execute(ZUNIONSTORE, new Command(ZUNIONSTORE_BYTES, arguments));
	}

	/**
	 * The Class Pipeline.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public class Pipeline
	{

		/**
		 * Append a value to a key String.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> append(Object key0, Object value1) throws RedisException
		{
			if (version < APPEND_VERSION)
			{
				throw new RedisException("Server does not support APPEND");
			}
			return (ListenableFuture<IntegerReply>) pipeline(APPEND, new Command(APPEND_BYTES, key0, value1));
		}

		/**
		 * Count set bits in a string String.
		 * 
		 * @param key0
		 *            the key0
		 * @param start1
		 *            the start1
		 * @param end2
		 *            the end2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> bitcount(Object key0, Object start1, Object end2) throws RedisException
		{
			if (version < BITCOUNT_VERSION)
			{
				throw new RedisException("Server does not support BITCOUNT");
			}
			return (ListenableFuture<IntegerReply>) pipeline(BITCOUNT, new Command(BITCOUNT_BYTES, key0, start1, end2));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Bitcount_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> bitcount_(Object... arguments) throws RedisException
		{
			if (version < BITCOUNT_VERSION)
			{
				throw new RedisException("Server does not support BITCOUNT");
			}
			return (ListenableFuture<IntegerReply>) pipeline(BITCOUNT, new Command(BITCOUNT_BYTES, arguments));
		}

		/**
		 * Perform bitwise operations between strings String.
		 * 
		 * @param operation0
		 *            the operation0
		 * @param destkey1
		 *            the destkey1
		 * @param key2
		 *            the key2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> bitop(Object operation0, Object destkey1, Object[] key2) throws RedisException
		{
			if (version < BITOP_VERSION)
			{
				throw new RedisException("Server does not support BITOP");
			}
			List list = new ArrayList();
			list.add(operation0);
			list.add(destkey1);
			Collections.addAll(list, key2);
			return (ListenableFuture<IntegerReply>) pipeline(BITOP, new Command(BITOP_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Bitop_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> bitop_(Object... arguments) throws RedisException
		{
			if (version < BITOP_VERSION)
			{
				throw new RedisException("Server does not support BITOP");
			}
			return (ListenableFuture<IntegerReply>) pipeline(BITOP, new Command(BITOP_BYTES, arguments));
		}

		/**
		 * Decrement the integer value of a key by one String.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> decr(Object key0) throws RedisException
		{
			if (version < DECR_VERSION)
			{
				throw new RedisException("Server does not support DECR");
			}
			return (ListenableFuture<IntegerReply>) pipeline(DECR, new Command(DECR_BYTES, key0));
		}

		/**
		 * Decrement the integer value of a key by the given number String.
		 * 
		 * @param key0
		 *            the key0
		 * @param decrement1
		 *            the decrement1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> decrby(Object key0, Object decrement1) throws RedisException
		{
			if (version < DECRBY_VERSION)
			{
				throw new RedisException("Server does not support DECRBY");
			}
			return (ListenableFuture<IntegerReply>) pipeline(DECRBY, new Command(DECRBY_BYTES, key0, decrement1));
		}

		/**
		 * Get the value of a key String.
		 * 
		 * @param key0
		 *            the key0
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> get(Object key0) throws RedisException
		{
			if (version < GET_VERSION)
			{
				throw new RedisException("Server does not support GET");
			}
			return (ListenableFuture<BulkReply>) pipeline(GET, new Command(GET_BYTES, key0));
		}

		/**
		 * Returns the bit value at offset in the string value stored at key
		 * String.
		 * 
		 * @param key0
		 *            the key0
		 * @param offset1
		 *            the offset1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> getbit(Object key0, Object offset1) throws RedisException
		{
			if (version < GETBIT_VERSION)
			{
				throw new RedisException("Server does not support GETBIT");
			}
			return (ListenableFuture<IntegerReply>) pipeline(GETBIT, new Command(GETBIT_BYTES, key0, offset1));
		}

		/**
		 * Get a substring of the string stored at a key String.
		 * 
		 * @param key0
		 *            the key0
		 * @param start1
		 *            the start1
		 * @param end2
		 *            the end2
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> getrange(Object key0, Object start1, Object end2) throws RedisException
		{
			if (version < GETRANGE_VERSION)
			{
				throw new RedisException("Server does not support GETRANGE");
			}
			return (ListenableFuture<BulkReply>) pipeline(GETRANGE, new Command(GETRANGE_BYTES, key0, start1, end2));
		}

		/**
		 * Set the string value of a key and return its old value String.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> getset(Object key0, Object value1) throws RedisException
		{
			if (version < GETSET_VERSION)
			{
				throw new RedisException("Server does not support GETSET");
			}
			return (ListenableFuture<BulkReply>) pipeline(GETSET, new Command(GETSET_BYTES, key0, value1));
		}

		/**
		 * Increment the integer value of a key by one String.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> incr(Object key0) throws RedisException
		{
			if (version < INCR_VERSION)
			{
				throw new RedisException("Server does not support INCR");
			}
			return (ListenableFuture<IntegerReply>) pipeline(INCR, new Command(INCR_BYTES, key0));
		}

		/**
		 * Increment the integer value of a key by the given amount String.
		 * 
		 * @param key0
		 *            the key0
		 * @param increment1
		 *            the increment1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> incrby(Object key0, Object increment1) throws RedisException
		{
			if (version < INCRBY_VERSION)
			{
				throw new RedisException("Server does not support INCRBY");
			}
			return (ListenableFuture<IntegerReply>) pipeline(INCRBY, new Command(INCRBY_BYTES, key0, increment1));
		}

		/**
		 * Increment the float value of a key by the given amount String.
		 * 
		 * @param key0
		 *            the key0
		 * @param increment1
		 *            the increment1
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> incrbyfloat(Object key0, Object increment1) throws RedisException
		{
			if (version < INCRBYFLOAT_VERSION)
			{
				throw new RedisException("Server does not support INCRBYFLOAT");
			}
			return (ListenableFuture<BulkReply>) pipeline(INCRBYFLOAT, new Command(INCRBYFLOAT_BYTES, key0, increment1));
		}

		/**
		 * Get the values of all the given keys String.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> mget(Object[] key0) throws RedisException
		{
			if (version < MGET_VERSION)
			{
				throw new RedisException("Server does not support MGET");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<MultiBulkReply>) pipeline(MGET, new Command(MGET_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Mget_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> mget_(Object... arguments) throws RedisException
		{
			if (version < MGET_VERSION)
			{
				throw new RedisException("Server does not support MGET");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(MGET, new Command(MGET_BYTES, arguments));
		}

		/**
		 * Set multiple keys to multiple values String.
		 * 
		 * @param key_or_value0
		 *            the key_or_value0
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> mset(Object[] key_or_value0) throws RedisException
		{
			if (version < MSET_VERSION)
			{
				throw new RedisException("Server does not support MSET");
			}
			List list = new ArrayList();
			Collections.addAll(list, key_or_value0);
			return (ListenableFuture<StatusReply>) pipeline(MSET, new Command(MSET_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Mset_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> mset_(Object... arguments) throws RedisException
		{
			if (version < MSET_VERSION)
			{
				throw new RedisException("Server does not support MSET");
			}
			return (ListenableFuture<StatusReply>) pipeline(MSET, new Command(MSET_BYTES, arguments));
		}

		/**
		 * Set multiple keys to multiple values, only if none of the keys exist
		 * String.
		 * 
		 * @param key_or_value0
		 *            the key_or_value0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> msetnx(Object[] key_or_value0) throws RedisException
		{
			if (version < MSETNX_VERSION)
			{
				throw new RedisException("Server does not support MSETNX");
			}
			List list = new ArrayList();
			Collections.addAll(list, key_or_value0);
			return (ListenableFuture<IntegerReply>) pipeline(MSETNX, new Command(MSETNX_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Msetnx_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> msetnx_(Object... arguments) throws RedisException
		{
			if (version < MSETNX_VERSION)
			{
				throw new RedisException("Server does not support MSETNX");
			}
			return (ListenableFuture<IntegerReply>) pipeline(MSETNX, new Command(MSETNX_BYTES, arguments));
		}

		/**
		 * Set the value and expiration in milliseconds of a key String.
		 * 
		 * @param key0
		 *            the key0
		 * @param milliseconds1
		 *            the milliseconds1
		 * @param value2
		 *            the value2
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> psetex(Object key0, Object milliseconds1, Object value2) throws RedisException
		{
			if (version < PSETEX_VERSION)
			{
				throw new RedisException("Server does not support PSETEX");
			}
			return (ListenableFuture<Reply>) pipeline(PSETEX, new Command(PSETEX_BYTES, key0, milliseconds1, value2));
		}

		/**
		 * Set the string value of a key String.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> set(Object key0, Object value1) throws RedisException
		{
			if (version < SET_VERSION)
			{
				throw new RedisException("Server does not support SET");
			}
			return (ListenableFuture<StatusReply>) pipeline(SET, new Command(SET_BYTES, key0, value1));
		}

		/**
		 * Sets or clears the bit at offset in the string value stored at key
		 * String.
		 * 
		 * @param key0
		 *            the key0
		 * @param offset1
		 *            the offset1
		 * @param value2
		 *            the value2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> setbit(Object key0, Object offset1, Object value2) throws RedisException
		{
			if (version < SETBIT_VERSION)
			{
				throw new RedisException("Server does not support SETBIT");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SETBIT, new Command(SETBIT_BYTES, key0, offset1, value2));
		}

		/**
		 * Set the value and expiration of a key String.
		 * 
		 * @param key0
		 *            the key0
		 * @param seconds1
		 *            the seconds1
		 * @param value2
		 *            the value2
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> setex(Object key0, Object seconds1, Object value2) throws RedisException
		{
			if (version < SETEX_VERSION)
			{
				throw new RedisException("Server does not support SETEX");
			}
			return (ListenableFuture<StatusReply>) pipeline(SETEX, new Command(SETEX_BYTES, key0, seconds1, value2));
		}

		/**
		 * Set the value of a key, only if the key does not exist String.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> setnx(Object key0, Object value1) throws RedisException
		{
			if (version < SETNX_VERSION)
			{
				throw new RedisException("Server does not support SETNX");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SETNX, new Command(SETNX_BYTES, key0, value1));
		}

		/**
		 * Overwrite part of a string at key starting at the specified offset
		 * String.
		 * 
		 * @param key0
		 *            the key0
		 * @param offset1
		 *            the offset1
		 * @param value2
		 *            the value2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> setrange(Object key0, Object offset1, Object value2) throws RedisException
		{
			if (version < SETRANGE_VERSION)
			{
				throw new RedisException("Server does not support SETRANGE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SETRANGE, new Command(SETRANGE_BYTES, key0, offset1, value2));
		}

		/**
		 * Get the length of the value stored in a key String.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> strlen(Object key0) throws RedisException
		{
			if (version < STRLEN_VERSION)
			{
				throw new RedisException("Server does not support STRLEN");
			}
			return (ListenableFuture<IntegerReply>) pipeline(STRLEN, new Command(STRLEN_BYTES, key0));
		}

		/**
		 * Echo the given string Connection.
		 * 
		 * @param message0
		 *            the message0
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> echo(Object message0) throws RedisException
		{
			if (version < ECHO_VERSION)
			{
				throw new RedisException("Server does not support ECHO");
			}
			return (ListenableFuture<BulkReply>) pipeline(ECHO, new Command(ECHO_BYTES, message0));
		}

		/**
		 * Ping the server Connection.
		 * 
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> ping() throws RedisException
		{
			if (version < PING_VERSION)
			{
				throw new RedisException("Server does not support PING");
			}
			return (ListenableFuture<StatusReply>) pipeline(PING, new Command(PING_BYTES));
		}

		/**
		 * Close the connection Connection.
		 * 
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> quit() throws RedisException
		{
			if (version < QUIT_VERSION)
			{
				throw new RedisException("Server does not support QUIT");
			}
			return (ListenableFuture<StatusReply>) pipeline(QUIT, new Command(QUIT_BYTES));
		}

		/**
		 * Change the selected database for the current connection Connection.
		 * 
		 * @param index0
		 *            the index0
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> select(Object index0) throws RedisException
		{
			if (version < SELECT_VERSION)
			{
				throw new RedisException("Server does not support SELECT");
			}
			return (ListenableFuture<StatusReply>) pipeline(SELECT, new Command(SELECT_BYTES, index0));
		}

		/**
		 * Asynchronously rewrite the append-only file Server.
		 * 
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> bgrewriteaof() throws RedisException
		{
			if (version < BGREWRITEAOF_VERSION)
			{
				throw new RedisException("Server does not support BGREWRITEAOF");
			}
			return (ListenableFuture<StatusReply>) pipeline(BGREWRITEAOF, new Command(BGREWRITEAOF_BYTES));
		}

		/**
		 * Asynchronously save the dataset to disk Server.
		 * 
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> bgsave() throws RedisException
		{
			if (version < BGSAVE_VERSION)
			{
				throw new RedisException("Server does not support BGSAVE");
			}
			return (ListenableFuture<StatusReply>) pipeline(BGSAVE, new Command(BGSAVE_BYTES));
		}

		/**
		 * Kill the connection of a client Server.
		 * 
		 * @param ip_port0
		 *            the ip_port0
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> client_kill(Object ip_port0) throws RedisException
		{
			if (version < CLIENT_KILL_VERSION)
			{
				throw new RedisException("Server does not support CLIENT_KILL");
			}
			return (ListenableFuture<Reply>) pipeline(CLIENT_KILL, new Command(CLIENT_KILL_BYTES, CLIENT_KILL2_BYTES, ip_port0));
		}

		/**
		 * Get the list of client connections Server.
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> client_list() throws RedisException
		{
			if (version < CLIENT_LIST_VERSION)
			{
				throw new RedisException("Server does not support CLIENT_LIST");
			}
			return (ListenableFuture<Reply>) pipeline(CLIENT_LIST, new Command(CLIENT_LIST_BYTES, CLIENT_LIST2_BYTES));
		}

		/**
		 * Get the current connection name Server.
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> client_getname() throws RedisException
		{
			if (version < CLIENT_GETNAME_VERSION)
			{
				throw new RedisException("Server does not support CLIENT_GETNAME");
			}
			return (ListenableFuture<Reply>) pipeline(CLIENT_GETNAME, new Command(CLIENT_GETNAME_BYTES, CLIENT_GETNAME2_BYTES));
		}

		/**
		 * Set the current connection name Server.
		 * 
		 * @param connection_name0
		 *            the connection_name0
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> client_setname(Object connection_name0) throws RedisException
		{
			if (version < CLIENT_SETNAME_VERSION)
			{
				throw new RedisException("Server does not support CLIENT_SETNAME");
			}
			return (ListenableFuture<Reply>) pipeline(CLIENT_SETNAME, new Command(CLIENT_SETNAME_BYTES, CLIENT_SETNAME2_BYTES, connection_name0));
		}

		/**
		 * Get the value of a configuration parameter Server.
		 * 
		 * @param parameter0
		 *            the parameter0
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> config_get(Object parameter0) throws RedisException
		{
			if (version < CONFIG_GET_VERSION)
			{
				throw new RedisException("Server does not support CONFIG_GET");
			}
			return (ListenableFuture<Reply>) pipeline(CONFIG_GET, new Command(CONFIG_GET_BYTES, CONFIG_GET2_BYTES, parameter0));
		}

		/**
		 * Set a configuration parameter to the given value Server.
		 * 
		 * @param parameter0
		 *            the parameter0
		 * @param value1
		 *            the value1
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> config_set(Object parameter0, Object value1) throws RedisException
		{
			if (version < CONFIG_SET_VERSION)
			{
				throw new RedisException("Server does not support CONFIG_SET");
			}
			return (ListenableFuture<Reply>) pipeline(CONFIG_SET, new Command(CONFIG_SET_BYTES, CONFIG_SET2_BYTES, parameter0, value1));
		}

		/**
		 * Reset the stats returned by INFO Server.
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> config_resetstat() throws RedisException
		{
			if (version < CONFIG_RESETSTAT_VERSION)
			{
				throw new RedisException("Server does not support CONFIG_RESETSTAT");
			}
			return (ListenableFuture<Reply>) pipeline(CONFIG_RESETSTAT, new Command(CONFIG_RESETSTAT_BYTES, CONFIG_RESETSTAT2_BYTES));
		}

		/**
		 * Return the number of keys in the selected database Server.
		 * 
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> dbsize() throws RedisException
		{
			if (version < DBSIZE_VERSION)
			{
				throw new RedisException("Server does not support DBSIZE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(DBSIZE, new Command(DBSIZE_BYTES));
		}

		/**
		 * Get debugging information about a key Server.
		 * 
		 * @param key0
		 *            the key0
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> debug_object(Object key0) throws RedisException
		{
			if (version < DEBUG_OBJECT_VERSION)
			{
				throw new RedisException("Server does not support DEBUG_OBJECT");
			}
			return (ListenableFuture<Reply>) pipeline(DEBUG_OBJECT, new Command(DEBUG_OBJECT_BYTES, DEBUG_OBJECT2_BYTES, key0));
		}

		/**
		 * Make the server crash Server.
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> debug_segfault() throws RedisException
		{
			if (version < DEBUG_SEGFAULT_VERSION)
			{
				throw new RedisException("Server does not support DEBUG_SEGFAULT");
			}
			return (ListenableFuture<Reply>) pipeline(DEBUG_SEGFAULT, new Command(DEBUG_SEGFAULT_BYTES, DEBUG_SEGFAULT2_BYTES));
		}

		/**
		 * Remove all keys from all databases Server.
		 * 
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> flushall() throws RedisException
		{
			if (version < FLUSHALL_VERSION)
			{
				throw new RedisException("Server does not support FLUSHALL");
			}
			return (ListenableFuture<StatusReply>) pipeline(FLUSHALL, new Command(FLUSHALL_BYTES));
		}

		/**
		 * Remove all keys from the current database Server.
		 * 
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> flushdb() throws RedisException
		{
			if (version < FLUSHDB_VERSION)
			{
				throw new RedisException("Server does not support FLUSHDB");
			}
			return (ListenableFuture<StatusReply>) pipeline(FLUSHDB, new Command(FLUSHDB_BYTES));
		}

		/**
		 * Get information and statistics about the server Server.
		 * 
		 * @param section0
		 *            the section0
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> info(Object section0) throws RedisException
		{
			if (version < INFO_VERSION)
			{
				throw new RedisException("Server does not support INFO");
			}
			return (ListenableFuture<BulkReply>) pipeline(INFO, new Command(INFO_BYTES, section0));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Info_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> info_(Object... arguments) throws RedisException
		{
			if (version < INFO_VERSION)
			{
				throw new RedisException("Server does not support INFO");
			}
			return (ListenableFuture<BulkReply>) pipeline(INFO, new Command(INFO_BYTES, arguments));
		}

		/**
		 * Get the UNIX time stamp of the last successful save to disk Server.
		 * 
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> lastsave() throws RedisException
		{
			if (version < LASTSAVE_VERSION)
			{
				throw new RedisException("Server does not support LASTSAVE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(LASTSAVE, new Command(LASTSAVE_BYTES));
		}

		/**
		 * Listen for all requests received by the server in real time Server.
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> monitor() throws RedisException
		{
			if (version < MONITOR_VERSION)
			{
				throw new RedisException("Server does not support MONITOR");
			}
			return (ListenableFuture<Reply>) pipeline(MONITOR, new Command(MONITOR_BYTES));
		}

		/**
		 * Synchronously save the dataset to disk Server.
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> save() throws RedisException
		{
			if (version < SAVE_VERSION)
			{
				throw new RedisException("Server does not support SAVE");
			}
			return (ListenableFuture<Reply>) pipeline(SAVE, new Command(SAVE_BYTES));
		}

		/**
		 * Synchronously save the dataset to disk and then shut down the server
		 * Server.
		 * 
		 * @param NOSAVE0
		 *            the nOSAV e0
		 * @param SAVE1
		 *            the sAV e1
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> shutdown(Object NOSAVE0, Object SAVE1) throws RedisException
		{
			if (version < SHUTDOWN_VERSION)
			{
				throw new RedisException("Server does not support SHUTDOWN");
			}
			return (ListenableFuture<StatusReply>) pipeline(SHUTDOWN, new Command(SHUTDOWN_BYTES, NOSAVE0, SAVE1));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Shutdown_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> shutdown_(Object... arguments) throws RedisException
		{
			if (version < SHUTDOWN_VERSION)
			{
				throw new RedisException("Server does not support SHUTDOWN");
			}
			return (ListenableFuture<StatusReply>) pipeline(SHUTDOWN, new Command(SHUTDOWN_BYTES, arguments));
		}

		/**
		 * Make the server a slave of another instance, or promote it as master
		 * Server.
		 * 
		 * @param host0
		 *            the host0
		 * @param port1
		 *            the port1
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> slaveof(Object host0, Object port1) throws RedisException
		{
			if (version < SLAVEOF_VERSION)
			{
				throw new RedisException("Server does not support SLAVEOF");
			}
			return (ListenableFuture<StatusReply>) pipeline(SLAVEOF, new Command(SLAVEOF_BYTES, host0, port1));
		}

		/**
		 * Manages the Redis slow queries log Server.
		 * 
		 * @param subcommand0
		 *            the subcommand0
		 * @param argument1
		 *            the argument1
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> slowlog(Object subcommand0, Object argument1) throws RedisException
		{
			if (version < SLOWLOG_VERSION)
			{
				throw new RedisException("Server does not support SLOWLOG");
			}
			return (ListenableFuture<Reply>) pipeline(SLOWLOG, new Command(SLOWLOG_BYTES, subcommand0, argument1));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Slowlog_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> slowlog_(Object... arguments) throws RedisException
		{
			if (version < SLOWLOG_VERSION)
			{
				throw new RedisException("Server does not support SLOWLOG");
			}
			return (ListenableFuture<Reply>) pipeline(SLOWLOG, new Command(SLOWLOG_BYTES, arguments));
		}

		/**
		 * Internal command used for replication Server.
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> sync() throws RedisException
		{
			if (version < SYNC_VERSION)
			{
				throw new RedisException("Server does not support SYNC");
			}
			return (ListenableFuture<Reply>) pipeline(SYNC, new Command(SYNC_BYTES));
		}

		/**
		 * Return the current server time Server.
		 * 
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> time() throws RedisException
		{
			if (version < TIME_VERSION)
			{
				throw new RedisException("Server does not support TIME");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(TIME, new Command(TIME_BYTES));
		}

		/**
		 * Remove and get the first element in a list, or block until one is
		 * available List.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> blpop(Object[] key0) throws RedisException
		{
			if (version < BLPOP_VERSION)
			{
				throw new RedisException("Server does not support BLPOP");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<MultiBulkReply>) pipeline(BLPOP, new Command(BLPOP_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Blpop_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> blpop_(Object... arguments) throws RedisException
		{
			if (version < BLPOP_VERSION)
			{
				throw new RedisException("Server does not support BLPOP");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(BLPOP, new Command(BLPOP_BYTES, arguments));
		}

		/**
		 * Remove and get the last element in a list, or block until one is
		 * available List.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> brpop(Object[] key0) throws RedisException
		{
			if (version < BRPOP_VERSION)
			{
				throw new RedisException("Server does not support BRPOP");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<MultiBulkReply>) pipeline(BRPOP, new Command(BRPOP_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Brpop_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> brpop_(Object... arguments) throws RedisException
		{
			if (version < BRPOP_VERSION)
			{
				throw new RedisException("Server does not support BRPOP");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(BRPOP, new Command(BRPOP_BYTES, arguments));
		}

		/**
		 * Pop a value from a list, push it to another list and return it; or
		 * block until one is available List.
		 * 
		 * @param source0
		 *            the source0
		 * @param destination1
		 *            the destination1
		 * @param timeout2
		 *            the timeout2
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> brpoplpush(Object source0, Object destination1, Object timeout2) throws RedisException
		{
			if (version < BRPOPLPUSH_VERSION)
			{
				throw new RedisException("Server does not support BRPOPLPUSH");
			}
			return (ListenableFuture<BulkReply>) pipeline(BRPOPLPUSH, new Command(BRPOPLPUSH_BYTES, source0, destination1, timeout2));
		}

		/**
		 * Get an element from a list by its index List.
		 * 
		 * @param key0
		 *            the key0
		 * @param index1
		 *            the index1
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> lindex(Object key0, Object index1) throws RedisException
		{
			if (version < LINDEX_VERSION)
			{
				throw new RedisException("Server does not support LINDEX");
			}
			return (ListenableFuture<BulkReply>) pipeline(LINDEX, new Command(LINDEX_BYTES, key0, index1));
		}

		/**
		 * Insert an element before or after another element in a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @param where1
		 *            the where1
		 * @param pivot2
		 *            the pivot2
		 * @param value3
		 *            the value3
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> linsert(Object key0, Object where1, Object pivot2, Object value3) throws RedisException
		{
			if (version < LINSERT_VERSION)
			{
				throw new RedisException("Server does not support LINSERT");
			}
			List list = new ArrayList();
			list.add(key0);
			list.add(where1);
			list.add(pivot2);
			list.add(value3);
			return (ListenableFuture<IntegerReply>) pipeline(LINSERT, new Command(LINSERT_BYTES, list.toArray(new Object[list.size()])));
		}

		/**
		 * Get the length of a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> llen(Object key0) throws RedisException
		{
			if (version < LLEN_VERSION)
			{
				throw new RedisException("Server does not support LLEN");
			}
			return (ListenableFuture<IntegerReply>) pipeline(LLEN, new Command(LLEN_BYTES, key0));
		}

		/**
		 * Remove and get the first element in a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> lpop(Object key0) throws RedisException
		{
			if (version < LPOP_VERSION)
			{
				throw new RedisException("Server does not support LPOP");
			}
			return (ListenableFuture<BulkReply>) pipeline(LPOP, new Command(LPOP_BYTES, key0));
		}

		/**
		 * Prepend one or multiple values to a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> lpush(Object key0, Object[] value1) throws RedisException
		{
			if (version < LPUSH_VERSION)
			{
				throw new RedisException("Server does not support LPUSH");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, value1);
			return (ListenableFuture<IntegerReply>) pipeline(LPUSH, new Command(LPUSH_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Lpush_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> lpush_(Object... arguments) throws RedisException
		{
			if (version < LPUSH_VERSION)
			{
				throw new RedisException("Server does not support LPUSH");
			}
			return (ListenableFuture<IntegerReply>) pipeline(LPUSH, new Command(LPUSH_BYTES, arguments));
		}

		/**
		 * Prepend a value to a list, only if the list exists List.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> lpushx(Object key0, Object value1) throws RedisException
		{
			if (version < LPUSHX_VERSION)
			{
				throw new RedisException("Server does not support LPUSHX");
			}
			return (ListenableFuture<IntegerReply>) pipeline(LPUSHX, new Command(LPUSHX_BYTES, key0, value1));
		}

		/**
		 * Get a range of elements from a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @param start1
		 *            the start1
		 * @param stop2
		 *            the stop2
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> lrange(Object key0, Object start1, Object stop2) throws RedisException
		{
			if (version < LRANGE_VERSION)
			{
				throw new RedisException("Server does not support LRANGE");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(LRANGE, new Command(LRANGE_BYTES, key0, start1, stop2));
		}

		/**
		 * Remove elements from a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @param count1
		 *            the count1
		 * @param value2
		 *            the value2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> lrem(Object key0, Object count1, Object value2) throws RedisException
		{
			if (version < LREM_VERSION)
			{
				throw new RedisException("Server does not support LREM");
			}
			return (ListenableFuture<IntegerReply>) pipeline(LREM, new Command(LREM_BYTES, key0, count1, value2));
		}

		/**
		 * Set the value of an element in a list by its index List.
		 * 
		 * @param key0
		 *            the key0
		 * @param index1
		 *            the index1
		 * @param value2
		 *            the value2
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> lset(Object key0, Object index1, Object value2) throws RedisException
		{
			if (version < LSET_VERSION)
			{
				throw new RedisException("Server does not support LSET");
			}
			return (ListenableFuture<StatusReply>) pipeline(LSET, new Command(LSET_BYTES, key0, index1, value2));
		}

		/**
		 * Trim a list to the specified range List.
		 * 
		 * @param key0
		 *            the key0
		 * @param start1
		 *            the start1
		 * @param stop2
		 *            the stop2
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> ltrim(Object key0, Object start1, Object stop2) throws RedisException
		{
			if (version < LTRIM_VERSION)
			{
				throw new RedisException("Server does not support LTRIM");
			}
			return (ListenableFuture<StatusReply>) pipeline(LTRIM, new Command(LTRIM_BYTES, key0, start1, stop2));
		}

		/**
		 * Remove and get the last element in a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> rpop(Object key0) throws RedisException
		{
			if (version < RPOP_VERSION)
			{
				throw new RedisException("Server does not support RPOP");
			}
			return (ListenableFuture<BulkReply>) pipeline(RPOP, new Command(RPOP_BYTES, key0));
		}

		/**
		 * Remove the last element in a list, append it to another list and
		 * return it List.
		 * 
		 * @param source0
		 *            the source0
		 * @param destination1
		 *            the destination1
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> rpoplpush(Object source0, Object destination1) throws RedisException
		{
			if (version < RPOPLPUSH_VERSION)
			{
				throw new RedisException("Server does not support RPOPLPUSH");
			}
			return (ListenableFuture<BulkReply>) pipeline(RPOPLPUSH, new Command(RPOPLPUSH_BYTES, source0, destination1));
		}

		/**
		 * Append one or multiple values to a list List.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> rpush(Object key0, Object[] value1) throws RedisException
		{
			if (version < RPUSH_VERSION)
			{
				throw new RedisException("Server does not support RPUSH");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, value1);
			return (ListenableFuture<IntegerReply>) pipeline(RPUSH, new Command(RPUSH_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Rpush_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> rpush_(Object... arguments) throws RedisException
		{
			if (version < RPUSH_VERSION)
			{
				throw new RedisException("Server does not support RPUSH");
			}
			return (ListenableFuture<IntegerReply>) pipeline(RPUSH, new Command(RPUSH_BYTES, arguments));
		}

		/**
		 * Append a value to a list, only if the list exists List.
		 * 
		 * @param key0
		 *            the key0
		 * @param value1
		 *            the value1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> rpushx(Object key0, Object value1) throws RedisException
		{
			if (version < RPUSHX_VERSION)
			{
				throw new RedisException("Server does not support RPUSHX");
			}
			return (ListenableFuture<IntegerReply>) pipeline(RPUSHX, new Command(RPUSHX_BYTES, key0, value1));
		}

		/**
		 * Delete a key Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> del(Object[] key0) throws RedisException
		{
			if (version < DEL_VERSION)
			{
				throw new RedisException("Server does not support DEL");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<IntegerReply>) pipeline(DEL, new Command(DEL_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Del_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> del_(Object... arguments) throws RedisException
		{
			if (version < DEL_VERSION)
			{
				throw new RedisException("Server does not support DEL");
			}
			return (ListenableFuture<IntegerReply>) pipeline(DEL, new Command(DEL_BYTES, arguments));
		}

		/**
		 * Return a serialized version of the value stored at the specified key.
		 * Generic
		 * 
		 * @param key0
		 *            the key0
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> dump(Object key0) throws RedisException
		{
			if (version < DUMP_VERSION)
			{
				throw new RedisException("Server does not support DUMP");
			}
			return (ListenableFuture<BulkReply>) pipeline(DUMP, new Command(DUMP_BYTES, key0));
		}

		/**
		 * Determine if a key exists Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> exists(Object key0) throws RedisException
		{
			if (version < EXISTS_VERSION)
			{
				throw new RedisException("Server does not support EXISTS");
			}
			return (ListenableFuture<IntegerReply>) pipeline(EXISTS, new Command(EXISTS_BYTES, key0));
		}

		/**
		 * Set a key's time to live in seconds Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param seconds1
		 *            the seconds1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> expire(Object key0, Object seconds1) throws RedisException
		{
			if (version < EXPIRE_VERSION)
			{
				throw new RedisException("Server does not support EXPIRE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(EXPIRE, new Command(EXPIRE_BYTES, key0, seconds1));
		}

		/**
		 * Set the expiration for a key as a UNIX timestamp Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param timestamp1
		 *            the timestamp1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> expireat(Object key0, Object timestamp1) throws RedisException
		{
			if (version < EXPIREAT_VERSION)
			{
				throw new RedisException("Server does not support EXPIREAT");
			}
			return (ListenableFuture<IntegerReply>) pipeline(EXPIREAT, new Command(EXPIREAT_BYTES, key0, timestamp1));
		}

		/**
		 * Find all keys matching the given pattern Generic.
		 * 
		 * @param pattern0
		 *            the pattern0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> keys(Object pattern0) throws RedisException
		{
			if (version < KEYS_VERSION)
			{
				throw new RedisException("Server does not support KEYS");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(KEYS, new Command(KEYS_BYTES, pattern0));
		}

		/**
		 * Atomically transfer a key from a Redis instance to another one.
		 * Generic
		 * 
		 * @param host0
		 *            the host0
		 * @param port1
		 *            the port1
		 * @param key2
		 *            the key2
		 * @param destination_db3
		 *            the destination_db3
		 * @param timeout4
		 *            the timeout4
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> migrate(Object host0, Object port1, Object key2, Object destination_db3, Object timeout4) throws RedisException
		{
			if (version < MIGRATE_VERSION)
			{
				throw new RedisException("Server does not support MIGRATE");
			}
			List list = new ArrayList();
			list.add(host0);
			list.add(port1);
			list.add(key2);
			list.add(destination_db3);
			list.add(timeout4);
			return (ListenableFuture<StatusReply>) pipeline(MIGRATE, new Command(MIGRATE_BYTES, list.toArray(new Object[list.size()])));
		}

		/**
		 * Move a key to another database Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param db1
		 *            the db1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> move(Object key0, Object db1) throws RedisException
		{
			if (version < MOVE_VERSION)
			{
				throw new RedisException("Server does not support MOVE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(MOVE, new Command(MOVE_BYTES, key0, db1));
		}

		/**
		 * Inspect the internals of Redis objects Generic.
		 * 
		 * @param subcommand0
		 *            the subcommand0
		 * @param arguments1
		 *            the arguments1
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> object(Object subcommand0, Object[] arguments1) throws RedisException
		{
			if (version < OBJECT_VERSION)
			{
				throw new RedisException("Server does not support OBJECT");
			}
			List list = new ArrayList();
			list.add(subcommand0);
			Collections.addAll(list, arguments1);
			return (ListenableFuture<Reply>) pipeline(OBJECT, new Command(OBJECT_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Object_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> object_(Object... arguments) throws RedisException
		{
			if (version < OBJECT_VERSION)
			{
				throw new RedisException("Server does not support OBJECT");
			}
			return (ListenableFuture<Reply>) pipeline(OBJECT, new Command(OBJECT_BYTES, arguments));
		}

		/**
		 * Remove the expiration from a key Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> persist(Object key0) throws RedisException
		{
			if (version < PERSIST_VERSION)
			{
				throw new RedisException("Server does not support PERSIST");
			}
			return (ListenableFuture<IntegerReply>) pipeline(PERSIST, new Command(PERSIST_BYTES, key0));
		}

		/**
		 * Set a key's time to live in milliseconds Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param milliseconds1
		 *            the milliseconds1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> pexpire(Object key0, Object milliseconds1) throws RedisException
		{
			if (version < PEXPIRE_VERSION)
			{
				throw new RedisException("Server does not support PEXPIRE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(PEXPIRE, new Command(PEXPIRE_BYTES, key0, milliseconds1));
		}

		/**
		 * Set the expiration for a key as a UNIX timestamp specified in
		 * milliseconds Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param milliseconds_timestamp1
		 *            the milliseconds_timestamp1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> pexpireat(Object key0, Object milliseconds_timestamp1) throws RedisException
		{
			if (version < PEXPIREAT_VERSION)
			{
				throw new RedisException("Server does not support PEXPIREAT");
			}
			return (ListenableFuture<IntegerReply>) pipeline(PEXPIREAT, new Command(PEXPIREAT_BYTES, key0, milliseconds_timestamp1));
		}

		/**
		 * Get the time to live for a key in milliseconds Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> pttl(Object key0) throws RedisException
		{
			if (version < PTTL_VERSION)
			{
				throw new RedisException("Server does not support PTTL");
			}
			return (ListenableFuture<IntegerReply>) pipeline(PTTL, new Command(PTTL_BYTES, key0));
		}

		/**
		 * Return a random key from the keyspace Generic.
		 * 
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> randomkey() throws RedisException
		{
			if (version < RANDOMKEY_VERSION)
			{
				throw new RedisException("Server does not support RANDOMKEY");
			}
			return (ListenableFuture<BulkReply>) pipeline(RANDOMKEY, new Command(RANDOMKEY_BYTES));
		}

		/**
		 * Rename a key Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param newkey1
		 *            the newkey1
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> rename(Object key0, Object newkey1) throws RedisException
		{
			if (version < RENAME_VERSION)
			{
				throw new RedisException("Server does not support RENAME");
			}
			return (ListenableFuture<StatusReply>) pipeline(RENAME, new Command(RENAME_BYTES, key0, newkey1));
		}

		/**
		 * Rename a key, only if the new key does not exist Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param newkey1
		 *            the newkey1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> renamenx(Object key0, Object newkey1) throws RedisException
		{
			if (version < RENAMENX_VERSION)
			{
				throw new RedisException("Server does not support RENAMENX");
			}
			return (ListenableFuture<IntegerReply>) pipeline(RENAMENX, new Command(RENAMENX_BYTES, key0, newkey1));
		}

		/**
		 * Create a key using the provided serialized value, previously obtained
		 * using DUMP. Generic
		 * 
		 * @param key0
		 *            the key0
		 * @param ttl1
		 *            the ttl1
		 * @param serialized_value2
		 *            the serialized_value2
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> restore(Object key0, Object ttl1, Object serialized_value2) throws RedisException
		{
			if (version < RESTORE_VERSION)
			{
				throw new RedisException("Server does not support RESTORE");
			}
			return (ListenableFuture<StatusReply>) pipeline(RESTORE, new Command(RESTORE_BYTES, key0, ttl1, serialized_value2));
		}

		/**
		 * Sort the elements in a list, set or sorted set Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @param pattern1
		 *            the pattern1
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> sort(Object key0, Object[] pattern1) throws RedisException
		{
			if (version < SORT_VERSION)
			{
				throw new RedisException("Server does not support SORT");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, pattern1);
			return (ListenableFuture<Reply>) pipeline(SORT, new Command(SORT_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sort_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> sort_(Object... arguments) throws RedisException
		{
			if (version < SORT_VERSION)
			{
				throw new RedisException("Server does not support SORT");
			}
			return (ListenableFuture<Reply>) pipeline(SORT, new Command(SORT_BYTES, arguments));
		}

		/**
		 * Get the time to live for a key Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> ttl(Object key0) throws RedisException
		{
			if (version < TTL_VERSION)
			{
				throw new RedisException("Server does not support TTL");
			}
			return (ListenableFuture<IntegerReply>) pipeline(TTL, new Command(TTL_BYTES, key0));
		}

		/**
		 * Determine the type stored at key Generic.
		 * 
		 * @param key0
		 *            the key0
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> type(Object key0) throws RedisException
		{
			if (version < TYPE_VERSION)
			{
				throw new RedisException("Server does not support TYPE");
			}
			return (ListenableFuture<StatusReply>) pipeline(TYPE, new Command(TYPE_BYTES, key0));
		}

		/**
		 * Forget about all watched keys Transactions.
		 * 
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> unwatch() throws RedisException
		{
			if (version < UNWATCH_VERSION)
			{
				throw new RedisException("Server does not support UNWATCH");
			}
			return (ListenableFuture<StatusReply>) pipeline(UNWATCH, new Command(UNWATCH_BYTES));
		}

		/**
		 * Watch the given keys to determine execution of the MULTI/EXEC block
		 * Transactions.
		 * 
		 * @param key0
		 *            the key0
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> watch(Object[] key0) throws RedisException
		{
			if (version < WATCH_VERSION)
			{
				throw new RedisException("Server does not support WATCH");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<StatusReply>) pipeline(WATCH, new Command(WATCH_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Watch_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> watch_(Object... arguments) throws RedisException
		{
			if (version < WATCH_VERSION)
			{
				throw new RedisException("Server does not support WATCH");
			}
			return (ListenableFuture<StatusReply>) pipeline(WATCH, new Command(WATCH_BYTES, arguments));
		}

		/**
		 * Execute a Lua script server side Scripting.
		 * 
		 * @param script0
		 *            the script0
		 * @param numkeys1
		 *            the numkeys1
		 * @param key2
		 *            the key2
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> eval(Object script0, Object numkeys1, Object[] key2) throws RedisException
		{
			if (version < EVAL_VERSION)
			{
				throw new RedisException("Server does not support EVAL");
			}
			List list = new ArrayList();
			list.add(script0);
			list.add(numkeys1);
			Collections.addAll(list, key2);
			return (ListenableFuture<Reply>) pipeline(EVAL, new Command(EVAL_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Eval_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> eval_(Object... arguments) throws RedisException
		{
			if (version < EVAL_VERSION)
			{
				throw new RedisException("Server does not support EVAL");
			}
			return (ListenableFuture<Reply>) pipeline(EVAL, new Command(EVAL_BYTES, arguments));
		}

		/**
		 * Execute a Lua script server side Scripting.
		 * 
		 * @param sha10
		 *            the sha10
		 * @param numkeys1
		 *            the numkeys1
		 * @param key2
		 *            the key2
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> evalsha(Object sha10, Object numkeys1, Object[] key2) throws RedisException
		{
			if (version < EVALSHA_VERSION)
			{
				throw new RedisException("Server does not support EVALSHA");
			}
			List list = new ArrayList();
			list.add(sha10);
			list.add(numkeys1);
			Collections.addAll(list, key2);
			return (ListenableFuture<Reply>) pipeline(EVALSHA, new Command(EVALSHA_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Evalsha_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> evalsha_(Object... arguments) throws RedisException
		{
			if (version < EVALSHA_VERSION)
			{
				throw new RedisException("Server does not support EVALSHA");
			}
			return (ListenableFuture<Reply>) pipeline(EVALSHA, new Command(EVALSHA_BYTES, arguments));
		}

		/**
		 * Check existence of scripts in the script cache. Scripting
		 * 
		 * @param script0
		 *            the script0
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> script_exists(Object[] script0) throws RedisException
		{
			if (version < SCRIPT_EXISTS_VERSION)
			{
				throw new RedisException("Server does not support SCRIPT_EXISTS");
			}
			List list = new ArrayList();
			Collections.addAll(list, script0);
			return (ListenableFuture<Reply>) pipeline(SCRIPT_EXISTS, new Command(SCRIPT_EXISTS_BYTES, SCRIPT_EXISTS2_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Script_exists_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> script_exists_(Object... arguments) throws RedisException
		{
			if (version < SCRIPT_EXISTS_VERSION)
			{
				throw new RedisException("Server does not support SCRIPT_EXISTS");
			}
			return (ListenableFuture<Reply>) pipeline(SCRIPT_EXISTS, new Command(SCRIPT_EXISTS_BYTES, SCRIPT_EXISTS2_BYTES, arguments));
		}

		/**
		 * Remove all the scripts from the script cache. Scripting
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> script_flush() throws RedisException
		{
			if (version < SCRIPT_FLUSH_VERSION)
			{
				throw new RedisException("Server does not support SCRIPT_FLUSH");
			}
			return (ListenableFuture<Reply>) pipeline(SCRIPT_FLUSH, new Command(SCRIPT_FLUSH_BYTES, SCRIPT_FLUSH2_BYTES));
		}

		/**
		 * Kill the script currently in execution. Scripting
		 * 
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> script_kill() throws RedisException
		{
			if (version < SCRIPT_KILL_VERSION)
			{
				throw new RedisException("Server does not support SCRIPT_KILL");
			}
			return (ListenableFuture<Reply>) pipeline(SCRIPT_KILL, new Command(SCRIPT_KILL_BYTES, SCRIPT_KILL2_BYTES));
		}

		/**
		 * Load the specified Lua script into the script cache. Scripting
		 * 
		 * @param script0
		 *            the script0
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> script_load(Object script0) throws RedisException
		{
			if (version < SCRIPT_LOAD_VERSION)
			{
				throw new RedisException("Server does not support SCRIPT_LOAD");
			}
			return (ListenableFuture<Reply>) pipeline(SCRIPT_LOAD, new Command(SCRIPT_LOAD_BYTES, SCRIPT_LOAD2_BYTES, script0));
		}

		/**
		 * Delete one or more hash fields Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> hdel(Object key0, Object[] field1) throws RedisException
		{
			if (version < HDEL_VERSION)
			{
				throw new RedisException("Server does not support HDEL");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, field1);
			return (ListenableFuture<IntegerReply>) pipeline(HDEL, new Command(HDEL_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Hdel_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> hdel_(Object... arguments) throws RedisException
		{
			if (version < HDEL_VERSION)
			{
				throw new RedisException("Server does not support HDEL");
			}
			return (ListenableFuture<IntegerReply>) pipeline(HDEL, new Command(HDEL_BYTES, arguments));
		}

		/**
		 * Determine if a hash field exists Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> hexists(Object key0, Object field1) throws RedisException
		{
			if (version < HEXISTS_VERSION)
			{
				throw new RedisException("Server does not support HEXISTS");
			}
			return (ListenableFuture<IntegerReply>) pipeline(HEXISTS, new Command(HEXISTS_BYTES, key0, field1));
		}

		/**
		 * Get the value of a hash field Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> hget(Object key0, Object field1) throws RedisException
		{
			if (version < HGET_VERSION)
			{
				throw new RedisException("Server does not support HGET");
			}
			return (ListenableFuture<BulkReply>) pipeline(HGET, new Command(HGET_BYTES, key0, field1));
		}

		/**
		 * Get all the fields and values in a hash Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> hgetall(Object key0) throws RedisException
		{
			if (version < HGETALL_VERSION)
			{
				throw new RedisException("Server does not support HGETALL");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(HGETALL, new Command(HGETALL_BYTES, key0));
		}

		/**
		 * Increment the integer value of a hash field by the given number Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @param increment2
		 *            the increment2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> hincrby(Object key0, Object field1, Object increment2) throws RedisException
		{
			if (version < HINCRBY_VERSION)
			{
				throw new RedisException("Server does not support HINCRBY");
			}
			return (ListenableFuture<IntegerReply>) pipeline(HINCRBY, new Command(HINCRBY_BYTES, key0, field1, increment2));
		}

		/**
		 * Increment the float value of a hash field by the given amount Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @param increment2
		 *            the increment2
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> hincrbyfloat(Object key0, Object field1, Object increment2) throws RedisException
		{
			if (version < HINCRBYFLOAT_VERSION)
			{
				throw new RedisException("Server does not support HINCRBYFLOAT");
			}
			return (ListenableFuture<BulkReply>) pipeline(HINCRBYFLOAT, new Command(HINCRBYFLOAT_BYTES, key0, field1, increment2));
		}

		/**
		 * Get all the fields in a hash Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> hkeys(Object key0) throws RedisException
		{
			if (version < HKEYS_VERSION)
			{
				throw new RedisException("Server does not support HKEYS");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(HKEYS, new Command(HKEYS_BYTES, key0));
		}

		/**
		 * Get the number of fields in a hash Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> hlen(Object key0) throws RedisException
		{
			if (version < HLEN_VERSION)
			{
				throw new RedisException("Server does not support HLEN");
			}
			return (ListenableFuture<IntegerReply>) pipeline(HLEN, new Command(HLEN_BYTES, key0));
		}

		/**
		 * Get the values of all the given hash fields Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> hmget(Object key0, Object[] field1) throws RedisException
		{
			if (version < HMGET_VERSION)
			{
				throw new RedisException("Server does not support HMGET");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, field1);
			return (ListenableFuture<MultiBulkReply>) pipeline(HMGET, new Command(HMGET_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Hmget_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> hmget_(Object... arguments) throws RedisException
		{
			if (version < HMGET_VERSION)
			{
				throw new RedisException("Server does not support HMGET");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(HMGET, new Command(HMGET_BYTES, arguments));
		}

		/**
		 * Set multiple hash fields to multiple values Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field_or_value1
		 *            the field_or_value1
		 * @return StatusReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> hmset(Object key0, Object[] field_or_value1) throws RedisException
		{
			if (version < HMSET_VERSION)
			{
				throw new RedisException("Server does not support HMSET");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, field_or_value1);
			return (ListenableFuture<StatusReply>) pipeline(HMSET, new Command(HMSET_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Hmset_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<StatusReply> hmset_(Object... arguments) throws RedisException
		{
			if (version < HMSET_VERSION)
			{
				throw new RedisException("Server does not support HMSET");
			}
			return (ListenableFuture<StatusReply>) pipeline(HMSET, new Command(HMSET_BYTES, arguments));
		}

		/**
		 * Set the string value of a hash field Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @param value2
		 *            the value2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> hset(Object key0, Object field1, Object value2) throws RedisException
		{
			if (version < HSET_VERSION)
			{
				throw new RedisException("Server does not support HSET");
			}
			return (ListenableFuture<IntegerReply>) pipeline(HSET, new Command(HSET_BYTES, key0, field1, value2));
		}

		/**
		 * Set the value of a hash field, only if the field does not exist Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @param field1
		 *            the field1
		 * @param value2
		 *            the value2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> hsetnx(Object key0, Object field1, Object value2) throws RedisException
		{
			if (version < HSETNX_VERSION)
			{
				throw new RedisException("Server does not support HSETNX");
			}
			return (ListenableFuture<IntegerReply>) pipeline(HSETNX, new Command(HSETNX_BYTES, key0, field1, value2));
		}

		/**
		 * Get all the values in a hash Hash.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> hvals(Object key0) throws RedisException
		{
			if (version < HVALS_VERSION)
			{
				throw new RedisException("Server does not support HVALS");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(HVALS, new Command(HVALS_BYTES, key0));
		}

		/**
		 * Post a message to a channel Pubsub.
		 * 
		 * @param channel0
		 *            the channel0
		 * @param message1
		 *            the message1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> publish(Object channel0, Object message1) throws RedisException
		{
			if (version < PUBLISH_VERSION)
			{
				throw new RedisException("Server does not support PUBLISH");
			}
			return (ListenableFuture<IntegerReply>) pipeline(PUBLISH, new Command(PUBLISH_BYTES, channel0, message1));
		}

		/**
		 * Add one or more members to a set Set.
		 * 
		 * @param key0
		 *            the key0
		 * @param member1
		 *            the member1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sadd(Object key0, Object[] member1) throws RedisException
		{
			if (version < SADD_VERSION)
			{
				throw new RedisException("Server does not support SADD");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, member1);
			return (ListenableFuture<IntegerReply>) pipeline(SADD, new Command(SADD_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sadd_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sadd_(Object... arguments) throws RedisException
		{
			if (version < SADD_VERSION)
			{
				throw new RedisException("Server does not support SADD");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SADD, new Command(SADD_BYTES, arguments));
		}

		/**
		 * Get the number of members in a set Set.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> scard(Object key0) throws RedisException
		{
			if (version < SCARD_VERSION)
			{
				throw new RedisException("Server does not support SCARD");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SCARD, new Command(SCARD_BYTES, key0));
		}

		/**
		 * Subtract multiple sets Set.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> sdiff(Object[] key0) throws RedisException
		{
			if (version < SDIFF_VERSION)
			{
				throw new RedisException("Server does not support SDIFF");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<MultiBulkReply>) pipeline(SDIFF, new Command(SDIFF_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sdiff_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> sdiff_(Object... arguments) throws RedisException
		{
			if (version < SDIFF_VERSION)
			{
				throw new RedisException("Server does not support SDIFF");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(SDIFF, new Command(SDIFF_BYTES, arguments));
		}

		/**
		 * Subtract multiple sets and store the resulting set in a key Set.
		 * 
		 * @param destination0
		 *            the destination0
		 * @param key1
		 *            the key1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sdiffstore(Object destination0, Object[] key1) throws RedisException
		{
			if (version < SDIFFSTORE_VERSION)
			{
				throw new RedisException("Server does not support SDIFFSTORE");
			}
			List list = new ArrayList();
			list.add(destination0);
			Collections.addAll(list, key1);
			return (ListenableFuture<IntegerReply>) pipeline(SDIFFSTORE, new Command(SDIFFSTORE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sdiffstore_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sdiffstore_(Object... arguments) throws RedisException
		{
			if (version < SDIFFSTORE_VERSION)
			{
				throw new RedisException("Server does not support SDIFFSTORE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SDIFFSTORE, new Command(SDIFFSTORE_BYTES, arguments));
		}

		/**
		 * Intersect multiple sets Set.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> sinter(Object[] key0) throws RedisException
		{
			if (version < SINTER_VERSION)
			{
				throw new RedisException("Server does not support SINTER");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<MultiBulkReply>) pipeline(SINTER, new Command(SINTER_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sinter_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> sinter_(Object... arguments) throws RedisException
		{
			if (version < SINTER_VERSION)
			{
				throw new RedisException("Server does not support SINTER");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(SINTER, new Command(SINTER_BYTES, arguments));
		}

		/**
		 * Intersect multiple sets and store the resulting set in a key Set.
		 * 
		 * @param destination0
		 *            the destination0
		 * @param key1
		 *            the key1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sinterstore(Object destination0, Object[] key1) throws RedisException
		{
			if (version < SINTERSTORE_VERSION)
			{
				throw new RedisException("Server does not support SINTERSTORE");
			}
			List list = new ArrayList();
			list.add(destination0);
			Collections.addAll(list, key1);
			return (ListenableFuture<IntegerReply>) pipeline(SINTERSTORE, new Command(SINTERSTORE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sinterstore_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sinterstore_(Object... arguments) throws RedisException
		{
			if (version < SINTERSTORE_VERSION)
			{
				throw new RedisException("Server does not support SINTERSTORE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SINTERSTORE, new Command(SINTERSTORE_BYTES, arguments));
		}

		/**
		 * Determine if a given value is a member of a set Set.
		 * 
		 * @param key0
		 *            the key0
		 * @param member1
		 *            the member1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sismember(Object key0, Object member1) throws RedisException
		{
			if (version < SISMEMBER_VERSION)
			{
				throw new RedisException("Server does not support SISMEMBER");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SISMEMBER, new Command(SISMEMBER_BYTES, key0, member1));
		}

		/**
		 * Get all the members in a set Set.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> smembers(Object key0) throws RedisException
		{
			if (version < SMEMBERS_VERSION)
			{
				throw new RedisException("Server does not support SMEMBERS");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(SMEMBERS, new Command(SMEMBERS_BYTES, key0));
		}

		/**
		 * Move a member from one set to another Set.
		 * 
		 * @param source0
		 *            the source0
		 * @param destination1
		 *            the destination1
		 * @param member2
		 *            the member2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> smove(Object source0, Object destination1, Object member2) throws RedisException
		{
			if (version < SMOVE_VERSION)
			{
				throw new RedisException("Server does not support SMOVE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SMOVE, new Command(SMOVE_BYTES, source0, destination1, member2));
		}

		/**
		 * Remove and return a random member from a set Set.
		 * 
		 * @param key0
		 *            the key0
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> spop(Object key0) throws RedisException
		{
			if (version < SPOP_VERSION)
			{
				throw new RedisException("Server does not support SPOP");
			}
			return (ListenableFuture<BulkReply>) pipeline(SPOP, new Command(SPOP_BYTES, key0));
		}

		/**
		 * Get one or multiple random members from a set Set.
		 * 
		 * @param key0
		 *            the key0
		 * @param count1
		 *            the count1
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> srandmember(Object key0, Object count1) throws RedisException
		{
			if (version < SRANDMEMBER_VERSION)
			{
				throw new RedisException("Server does not support SRANDMEMBER");
			}
			return (ListenableFuture<Reply>) pipeline(SRANDMEMBER, new Command(SRANDMEMBER_BYTES, key0, count1));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Srandmember_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> srandmember_(Object... arguments) throws RedisException
		{
			if (version < SRANDMEMBER_VERSION)
			{
				throw new RedisException("Server does not support SRANDMEMBER");
			}
			return (ListenableFuture<Reply>) pipeline(SRANDMEMBER, new Command(SRANDMEMBER_BYTES, arguments));
		}

		/**
		 * Remove one or more members from a set Set.
		 * 
		 * @param key0
		 *            the key0
		 * @param member1
		 *            the member1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> srem(Object key0, Object[] member1) throws RedisException
		{
			if (version < SREM_VERSION)
			{
				throw new RedisException("Server does not support SREM");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, member1);
			return (ListenableFuture<IntegerReply>) pipeline(SREM, new Command(SREM_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Srem_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> srem_(Object... arguments) throws RedisException
		{
			if (version < SREM_VERSION)
			{
				throw new RedisException("Server does not support SREM");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SREM, new Command(SREM_BYTES, arguments));
		}

		/**
		 * Add multiple sets Set.
		 * 
		 * @param key0
		 *            the key0
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> sunion(Object[] key0) throws RedisException
		{
			if (version < SUNION_VERSION)
			{
				throw new RedisException("Server does not support SUNION");
			}
			List list = new ArrayList();
			Collections.addAll(list, key0);
			return (ListenableFuture<MultiBulkReply>) pipeline(SUNION, new Command(SUNION_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sunion_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> sunion_(Object... arguments) throws RedisException
		{
			if (version < SUNION_VERSION)
			{
				throw new RedisException("Server does not support SUNION");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(SUNION, new Command(SUNION_BYTES, arguments));
		}

		/**
		 * Add multiple sets and store the resulting set in a key Set.
		 * 
		 * @param destination0
		 *            the destination0
		 * @param key1
		 *            the key1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sunionstore(Object destination0, Object[] key1) throws RedisException
		{
			if (version < SUNIONSTORE_VERSION)
			{
				throw new RedisException("Server does not support SUNIONSTORE");
			}
			List list = new ArrayList();
			list.add(destination0);
			Collections.addAll(list, key1);
			return (ListenableFuture<IntegerReply>) pipeline(SUNIONSTORE, new Command(SUNIONSTORE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Sunionstore_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> sunionstore_(Object... arguments) throws RedisException
		{
			if (version < SUNIONSTORE_VERSION)
			{
				throw new RedisException("Server does not support SUNIONSTORE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(SUNIONSTORE, new Command(SUNIONSTORE_BYTES, arguments));
		}

		/**
		 * Add one or more members to a sorted set, or update its score if it
		 * already exists Sorted_set.
		 * 
		 * @param args
		 *            the args
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zadd(Object[] args) throws RedisException
		{
			if (version < ZADD_VERSION)
			{
				throw new RedisException("Server does not support ZADD");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZADD, new Command(ZADD_BYTES, args));
		}

		/**
		 * Get the number of members in a sorted set Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zcard(Object key0) throws RedisException
		{
			if (version < ZCARD_VERSION)
			{
				throw new RedisException("Server does not support ZCARD");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZCARD, new Command(ZCARD_BYTES, key0));
		}

		/**
		 * Count the members in a sorted set with scores within the given values
		 * Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param min1
		 *            the min1
		 * @param max2
		 *            the max2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zcount(Object key0, Object min1, Object max2) throws RedisException
		{
			if (version < ZCOUNT_VERSION)
			{
				throw new RedisException("Server does not support ZCOUNT");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZCOUNT, new Command(ZCOUNT_BYTES, key0, min1, max2));
		}

		/**
		 * Increment the score of a member in a sorted set Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param increment1
		 *            the increment1
		 * @param member2
		 *            the member2
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> zincrby(Object key0, Object increment1, Object member2) throws RedisException
		{
			if (version < ZINCRBY_VERSION)
			{
				throw new RedisException("Server does not support ZINCRBY");
			}
			return (ListenableFuture<BulkReply>) pipeline(ZINCRBY, new Command(ZINCRBY_BYTES, key0, increment1, member2));
		}

		/**
		 * Intersect multiple sorted sets and store the resulting sorted set in
		 * a new key Sorted_set.
		 * 
		 * @param destination0
		 *            the destination0
		 * @param numkeys1
		 *            the numkeys1
		 * @param key2
		 *            the key2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zinterstore(Object destination0, Object numkeys1, Object[] key2) throws RedisException
		{
			if (version < ZINTERSTORE_VERSION)
			{
				throw new RedisException("Server does not support ZINTERSTORE");
			}
			List list = new ArrayList();
			list.add(destination0);
			list.add(numkeys1);
			Collections.addAll(list, key2);
			return (ListenableFuture<IntegerReply>) pipeline(ZINTERSTORE, new Command(ZINTERSTORE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Zinterstore_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zinterstore_(Object... arguments) throws RedisException
		{
			if (version < ZINTERSTORE_VERSION)
			{
				throw new RedisException("Server does not support ZINTERSTORE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZINTERSTORE, new Command(ZINTERSTORE_BYTES, arguments));
		}

		/**
		 * Return a range of members in a sorted set, by index Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param start1
		 *            the start1
		 * @param stop2
		 *            the stop2
		 * @param withscores3
		 *            the withscores3
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrange(Object key0, Object start1, Object stop2, Object withscores3) throws RedisException
		{
			if (version < ZRANGE_VERSION)
			{
				throw new RedisException("Server does not support ZRANGE");
			}
			List list = new ArrayList();
			list.add(key0);
			list.add(start1);
			list.add(stop2);
			if (withscores3 != null)
			{
				list.add(withscores3);
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(ZRANGE, new Command(ZRANGE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Zrange_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrange_(Object... arguments) throws RedisException
		{
			if (version < ZRANGE_VERSION)
			{
				throw new RedisException("Server does not support ZRANGE");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(ZRANGE, new Command(ZRANGE_BYTES, arguments));
		}

		/**
		 * Return a range of members in a sorted set, by score Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param min1
		 *            the min1
		 * @param max2
		 *            the max2
		 * @param withscores3
		 *            the withscores3
		 * @param offset_or_count4
		 *            the offset_or_count4
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrangebyscore(Object key0, Object min1, Object max2, Object withscores3, Object[] offset_or_count4) throws RedisException
		{
			if (version < ZRANGEBYSCORE_VERSION)
			{
				throw new RedisException("Server does not support ZRANGEBYSCORE");
			}
			List list = new ArrayList();
			list.add(key0);
			list.add(min1);
			list.add(max2);
			if (withscores3 != null)
			{
				list.add(withscores3);
			}
			Collections.addAll(list, offset_or_count4);
			return (ListenableFuture<MultiBulkReply>) pipeline(ZRANGEBYSCORE, new Command(ZRANGEBYSCORE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Zrangebyscore_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrangebyscore_(Object... arguments) throws RedisException
		{
			if (version < ZRANGEBYSCORE_VERSION)
			{
				throw new RedisException("Server does not support ZRANGEBYSCORE");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(ZRANGEBYSCORE, new Command(ZRANGEBYSCORE_BYTES, arguments));
		}

		/**
		 * Determine the index of a member in a sorted set Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param member1
		 *            the member1
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> zrank(Object key0, Object member1) throws RedisException
		{
			if (version < ZRANK_VERSION)
			{
				throw new RedisException("Server does not support ZRANK");
			}
			return (ListenableFuture<Reply>) pipeline(ZRANK, new Command(ZRANK_BYTES, key0, member1));
		}

		/**
		 * Remove one or more members from a sorted set Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param member1
		 *            the member1
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zrem(Object key0, Object[] member1) throws RedisException
		{
			if (version < ZREM_VERSION)
			{
				throw new RedisException("Server does not support ZREM");
			}
			List list = new ArrayList();
			list.add(key0);
			Collections.addAll(list, member1);
			return (ListenableFuture<IntegerReply>) pipeline(ZREM, new Command(ZREM_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Zrem_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zrem_(Object... arguments) throws RedisException
		{
			if (version < ZREM_VERSION)
			{
				throw new RedisException("Server does not support ZREM");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZREM, new Command(ZREM_BYTES, arguments));
		}

		/**
		 * Remove all members in a sorted set within the given indexes
		 * Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param start1
		 *            the start1
		 * @param stop2
		 *            the stop2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zremrangebyrank(Object key0, Object start1, Object stop2) throws RedisException
		{
			if (version < ZREMRANGEBYRANK_VERSION)
			{
				throw new RedisException("Server does not support ZREMRANGEBYRANK");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZREMRANGEBYRANK, new Command(ZREMRANGEBYRANK_BYTES, key0, start1, stop2));
		}

		/**
		 * Remove all members in a sorted set within the given scores
		 * Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param min1
		 *            the min1
		 * @param max2
		 *            the max2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zremrangebyscore(Object key0, Object min1, Object max2) throws RedisException
		{
			if (version < ZREMRANGEBYSCORE_VERSION)
			{
				throw new RedisException("Server does not support ZREMRANGEBYSCORE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZREMRANGEBYSCORE, new Command(ZREMRANGEBYSCORE_BYTES, key0, min1, max2));
		}

		/**
		 * Return a range of members in a sorted set, by index, with scores
		 * ordered from high to low Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param start1
		 *            the start1
		 * @param stop2
		 *            the stop2
		 * @param withscores3
		 *            the withscores3
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrevrange(Object key0, Object start1, Object stop2, Object withscores3) throws RedisException
		{
			if (version < ZREVRANGE_VERSION)
			{
				throw new RedisException("Server does not support ZREVRANGE");
			}
			List list = new ArrayList();
			list.add(key0);
			list.add(start1);
			list.add(stop2);
			if (withscores3 != null)
			{
				list.add(withscores3);
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(ZREVRANGE, new Command(ZREVRANGE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Zrevrange_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrevrange_(Object... arguments) throws RedisException
		{
			if (version < ZREVRANGE_VERSION)
			{
				throw new RedisException("Server does not support ZREVRANGE");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(ZREVRANGE, new Command(ZREVRANGE_BYTES, arguments));
		}

		/**
		 * Return a range of members in a sorted set, by score, with scores
		 * ordered from high to low Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param max1
		 *            the max1
		 * @param min2
		 *            the min2
		 * @param withscores3
		 *            the withscores3
		 * @param offset_or_count4
		 *            the offset_or_count4
		 * @return MultiBulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrevrangebyscore(Object key0, Object max1, Object min2, Object withscores3, Object[] offset_or_count4) throws RedisException
		{
			if (version < ZREVRANGEBYSCORE_VERSION)
			{
				throw new RedisException("Server does not support ZREVRANGEBYSCORE");
			}
			List list = new ArrayList();
			list.add(key0);
			list.add(max1);
			list.add(min2);
			if (withscores3 != null)
			{
				list.add(withscores3);
			}
			Collections.addAll(list, offset_or_count4);
			return (ListenableFuture<MultiBulkReply>) pipeline(ZREVRANGEBYSCORE, new Command(ZREVRANGEBYSCORE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Zrevrangebyscore_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<MultiBulkReply> zrevrangebyscore_(Object... arguments) throws RedisException
		{
			if (version < ZREVRANGEBYSCORE_VERSION)
			{
				throw new RedisException("Server does not support ZREVRANGEBYSCORE");
			}
			return (ListenableFuture<MultiBulkReply>) pipeline(ZREVRANGEBYSCORE, new Command(ZREVRANGEBYSCORE_BYTES, arguments));
		}

		/**
		 * Determine the index of a member in a sorted set, with scores ordered
		 * from high to low Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param member1
		 *            the member1
		 * @return Reply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<Reply> zrevrank(Object key0, Object member1) throws RedisException
		{
			if (version < ZREVRANK_VERSION)
			{
				throw new RedisException("Server does not support ZREVRANK");
			}
			return (ListenableFuture<Reply>) pipeline(ZREVRANK, new Command(ZREVRANK_BYTES, key0, member1));
		}

		/**
		 * Get the score associated with the given member in a sorted set
		 * Sorted_set.
		 * 
		 * @param key0
		 *            the key0
		 * @param member1
		 *            the member1
		 * @return BulkReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<BulkReply> zscore(Object key0, Object member1) throws RedisException
		{
			if (version < ZSCORE_VERSION)
			{
				throw new RedisException("Server does not support ZSCORE");
			}
			return (ListenableFuture<BulkReply>) pipeline(ZSCORE, new Command(ZSCORE_BYTES, key0, member1));
		}

		/**
		 * Add multiple sorted sets and store the resulting sorted set in a new
		 * key Sorted_set.
		 * 
		 * @param destination0
		 *            the destination0
		 * @param numkeys1
		 *            the numkeys1
		 * @param key2
		 *            the key2
		 * @return IntegerReply
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zunionstore(Object destination0, Object numkeys1, Object[] key2) throws RedisException
		{
			if (version < ZUNIONSTORE_VERSION)
			{
				throw new RedisException("Server does not support ZUNIONSTORE");
			}
			List list = new ArrayList();
			list.add(destination0);
			list.add(numkeys1);
			Collections.addAll(list, key2);
			return (ListenableFuture<IntegerReply>) pipeline(ZUNIONSTORE, new Command(ZUNIONSTORE_BYTES, list.toArray(new Object[list.size()])));
		}

		// Varargs version to simplify commands with optional or multiple
		// arguments
		/**
		 * Zunionstore_.
		 * 
		 * @param arguments
		 *            the arguments
		 * @return the listenable future
		 * @throws RedisException
		 *             the redis exception
		 */
		public ListenableFuture<IntegerReply> zunionstore_(Object... arguments) throws RedisException
		{
			if (version < ZUNIONSTORE_VERSION)
			{
				throw new RedisException("Server does not support ZUNIONSTORE");
			}
			return (ListenableFuture<IntegerReply>) pipeline(ZUNIONSTORE, new Command(ZUNIONSTORE_BYTES, arguments));
		}
	}
}
