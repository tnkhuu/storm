/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.io.IOException;
import java.io.OutputStream;

/**
 * The Class IntegerReply.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class IntegerReply implements Reply<Long>
{

	public static final char MARKER = ':';
	private final long integer;

	/**
	 * Instantiates a new integer reply.
	 * 
	 * @param integer
	 *            the integer
	 */
	public IntegerReply(long integer)
	{
		this.integer = integer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.Reply#data()
	 */
	@Override
	public Long data()
	{
		return integer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.dns.persistence.Reply#write(java.io.OutputStream)
	 */
	@Override
	public void write(OutputStream os) throws IOException
	{
		os.write(MARKER);
		os.write(RedisProtocol.toBytes(integer));
		os.write(CRLF);
	}
}
