/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Implements the Redis Universal Protocol. Send a command, receive a command,
 * send a reply and receive a reply.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({ "rawtypes" })
public class RedisProtocol
{
	public static final char CR = '\r';
	public static final char LF = '\n';
	private static final char ZERO = '0';
	private final BufferedInputStream is;
	private final OutputStream os;

	/**
	 * Create a new RedisProtocol from a socket connection.
	 * 
	 * @param socket
	 *            the socket
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public RedisProtocol(Socket socket) throws IOException
	{
		is = new BufferedInputStream(socket.getInputStream());
		os = new BufferedOutputStream(socket.getOutputStream());
	}

	/**
	 * Read fixed size field from the stream.
	 * 
	 * @param is
	 *            the is
	 * @return the byte[]
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static byte[] readBytes(InputStream is) throws IOException
	{
		long size = readLong(is);
		if (size > Integer.MAX_VALUE)
		{
			throw new IllegalArgumentException("Java only supports arrays up to " + Integer.MAX_VALUE + " in size");
		}
		int read;
		if (size == -1)
		{
			return null;
		}
		if (size < 0)
		{
			throw new IllegalArgumentException("Invalid size: " + size);
		}
		byte[] bytes = new byte[(int) size];
		int total = 0;
		while (total < bytes.length && (read = is.read(bytes, total, bytes.length - total)) != -1)
		{
			total += read;
		}
		if (total < bytes.length)
		{
			throw new IOException("Failed to read enough bytes: " + total);
		}
		int cr = is.read();
		int lf = is.read();
		if (cr != CR || lf != LF)
		{
			throw new IOException("Improper line ending: " + cr + ", " + lf);
		}
		return bytes;
	}

	/**
	 * Read a signed ascii integer from the input stream.
	 * 
	 * @param is
	 *            the is
	 * @return the long
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static long readLong(InputStream is) throws IOException
	{
		int sign;
		int read = is.read();
		if (read == '-')
		{
			read = is.read();
			sign = -1;
		}
		else
		{
			sign = 1;
		}
		long number = 0;
		do
		{
			if (read == -1)
			{
				throw new EOFException("Unexpected end of stream");
			}
			else if (read == CR)
			{
				if (is.read() == LF)
				{
					return number * sign;
				}
			}
			int value = read - ZERO;
			if (value >= 0 && value < 10)
			{
				number *= 10;
				number += value;
			}
			else
			{
				throw new IOException("Invalid character in integer");
			}
			read = is.read();
		}
		while (true);
	}

	/**
	 * Read a Reply from an input stream.
	 * 
	 * @param is
	 *            the is
	 * @return the reply
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("deprecation")
	public static Reply receive(InputStream is) throws IOException
	{
		int code = is.read();
		if (code == -1)
		{
			return null;
		}
		switch (code)
		{
		case StatusReply.MARKER:
		{
			return new StatusReply(new DataInputStream(is).readLine());
		}
		case ErrorReply.MARKER:
		{
			return new ErrorReply(new DataInputStream(is).readLine());
		}
		case IntegerReply.MARKER:
		{
			return new IntegerReply(readLong(is));
		}
		case BulkReply.MARKER:
		{
			return new BulkReply(readBytes(is));
		}
		case MultiBulkReply.MARKER:
		{
			return new MultiBulkReply(is);
		}
		default:
		{
			throw new IOException("Unexpected character in stream: " + code);
		}
		}
	}

	/**
	 * To bytes.
	 * 
	 * @param length
	 *            the length
	 * @return the byte[]
	 */
	public static byte[] toBytes(Number length)
	{
		return length.toString().getBytes();
	}

	/**
	 * Wait for a reply on the input stream.
	 * 
	 * @return the reply
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Reply receiveAsync() throws IOException
	{
		synchronized (is)
		{
			return receive(is);
		}
	}

	/**
	 * Send a command over the wire, do not wait for a reponse.
	 * 
	 * @param command
	 *            the command
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void sendAsync(Command command) throws IOException
	{
		synchronized (os)
		{
			command.write(os);
		}
		os.flush();
	}

	/**
	 * Close the input and output streams. Will also disconnect the socket.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void close() throws IOException
	{
		is.close();
		os.close();
	}
}
