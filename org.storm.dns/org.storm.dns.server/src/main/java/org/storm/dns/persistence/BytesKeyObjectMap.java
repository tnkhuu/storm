/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.util.HashMap;

/**
 * Map that uses byte[]s for keys. Wraps them for you. Passing a non-byte[] or
 * non-BytesKey will result in a CCE.
 * 
 * @param <V>
 *            the value type
 * @author Trung Khuu
 * @since 1.0
 */
public class BytesKeyObjectMap<V> extends HashMap<Object, V>
{

	private static final long serialVersionUID = 1L;

	/**
	 * Make key.
	 * 
	 * @param key
	 *            the key
	 * @return the bytes key
	 */
	private BytesKey makeKey(Object key)
	{
		return key instanceof byte[] ? new BytesKey((byte[]) key) : (BytesKey) key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#get(java.lang.Object)
	 */
	@Override
	public V get(Object o)
	{
		return get(makeKey(o));
	}

	/**
	 * Gets the.
	 * 
	 * @param bytes
	 *            the bytes
	 * @return the v
	 */
	public V get(byte[] bytes)
	{
		return get(new BytesKey(bytes));
	}

	/**
	 * Gets the.
	 * 
	 * @param key
	 *            the key
	 * @return the v
	 */
	public V get(BytesKey key)
	{
		return super.get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object o)
	{
		return containsKey(makeKey(o));
	}

	/**
	 * Contains key.
	 * 
	 * @param bytes
	 *            the bytes
	 * @return true, if successful
	 */
	public boolean containsKey(byte[] bytes)
	{
		return containsKey(new BytesKey(bytes));
	}

	/**
	 * Contains key.
	 * 
	 * @param key
	 *            the key
	 * @return true, if successful
	 */
	public boolean containsKey(BytesKey key)
	{
		return super.containsKey(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public V put(Object o, V value)
	{
		return put(makeKey(o), value);
	}

	/**
	 * Put.
	 * 
	 * @param bytes
	 *            the bytes
	 * @param value
	 *            the value
	 * @return the v
	 */
	public V put(byte[] bytes, V value)
	{
		return put(new BytesKey(bytes), value);
	}

	/**
	 * Put.
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 * @return the v
	 */
	public V put(BytesKey key, V value)
	{
		return super.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#remove(java.lang.Object)
	 */
	@Override
	public V remove(Object o)
	{
		return remove(makeKey(o));
	}

	/**
	 * Removes the.
	 * 
	 * @param bytes
	 *            the bytes
	 * @return the v
	 */
	public V remove(byte[] bytes)
	{
		return remove(new BytesKey(bytes));
	}

	/**
	 * Removes the.
	 * 
	 * @param key
	 *            the key
	 * @return the v
	 */
	public V remove(BytesKey key)
	{
		return super.remove(key);
	}
}
