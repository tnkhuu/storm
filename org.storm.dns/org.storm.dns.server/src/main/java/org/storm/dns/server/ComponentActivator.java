/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Kernel;
import org.storm.api.kernel.Actor;
import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;
import org.storm.api.network.TableType;
import org.storm.api.services.RoutingTableService;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.api.Role;
import org.storm.nexus.api.Services;

/**
 * The Class DNS.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator extends Actor implements BundleActivator
{
	private Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);
	private static final String UTF8 = "UTF-8";
	private static final String RESOLV_CONF = "/etc/resolv.conf";
	private static final String NAME_SERVER = "nameserver ";
	private DNSServer server;
	private boolean started = false;
	ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(final BundleContext context) throws Exception
	{
		int port = DNSConfig.CONFIG.getValue(Settings.DNS_PORT);
		if (getRole() == Role.DNS)
		{
			port = 53;
		}
		if (getRole() != Role.INITIAL)
		{
			if (!started)
			{
				try
				{
					server = new DNSServer(context);
					server.run(port);
					
					exec.schedule(new Runnable()
					{
						
						@Override
						public void run()
						{
							addDNSRecord(context, Endpoints.DNS_ABS.getDomain());
							
						}
					}, 5, TimeUnit.SECONDS);
					started = true;
				}
				finally
				{
					
				}
				s_log.info("Started The Domain Name Server @ dns://0.0.0.0:" + server.getPort());
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception
	{
		if (started && server != null)
		{
			try
			{
				started = false;
				s_log.info("Shutting down DNS");
				server.shutdown();
				server = null;
			}
			finally
			{
				
			}
		}

	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String[] args) throws Exception
	{
		new ComponentActivator().start(null);
	}

	/**
	 * Register kernel routes.
	 * 
	 * @param context
	 *            the context
	 */
	@SuppressWarnings("unused")
	private void registerKernelRoutes(BundleContext context)
	{
		try
		{
			ServiceReference<Kernel> kernelsr = context.getServiceReference(Kernel.class);
			Kernel kernel = context.getService(kernelsr);
			RoutingTableService routeService = kernel.getRoutingTableService();
			routeService.routePorts(Protocol.UDP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 53, 8053);

			File f = new File(RESOLV_CONF);
			writeStringToFile(f, NAME_SERVER + Endpoints.UPSTREAM_DNS + "\n", Charset.defaultCharset());
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Unregister kernel routes.
	 * 
	 * @param context
	 *            the context
	 */
	@SuppressWarnings("unused")
	private void unregisterKernelRoutes(BundleContext context)
	{
		try
		{
			ServiceReference<Kernel> kernelsr = context.getServiceReference(Kernel.class);
			Kernel kernel = context.getService(kernelsr);
			RoutingTableService routeService = kernel.getRoutingTableService();
			routeService.deleteRoute(Protocol.UDP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 53, 8053);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Write.
	 * 
	 * @param data
	 *            the data
	 * @param output
	 *            the output
	 * @param encoding
	 *            the encoding
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void write(String data, OutputStream output, Charset encoding) throws IOException
	{
		if (data != null)
		{
			output.write(data.getBytes(UTF8));
		}
	}

	/**
	 * Write string to file.
	 * 
	 * @param file
	 *            the file
	 * @param data
	 *            the data
	 * @param encoding
	 *            the encoding
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void writeStringToFile(File file, String data, Charset encoding) throws IOException
	{
		OutputStream out = null;
		try
		{
			out = openOutputStream(file, false);
			write(data, out, encoding);
			out.close();
		}
		finally
		{
			out.close();
		}
	}

	/**
	 * Open output stream.
	 * 
	 * @param file
	 *            the file
	 * @param append
	 *            the append
	 * @return the file output stream
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static FileOutputStream openOutputStream(File file, boolean append) throws IOException
	{
		if (file.exists())
		{
			if (file.isDirectory())
			{
				throw new IOException("File '" + file + "' exists but is a directory");
			}
			if (file.canWrite() == false)
			{
				throw new IOException("File '" + file + "' cannot be written to");
			}
		}
		else
		{
			File parent = file.getParentFile();
			if (parent != null)
			{
				if (!parent.mkdirs() && !parent.isDirectory())
				{
					throw new IOException("Directory '" + parent + "' could not be created");
				}
			}
		}
		return new FileOutputStream(file, append);
	}

}
