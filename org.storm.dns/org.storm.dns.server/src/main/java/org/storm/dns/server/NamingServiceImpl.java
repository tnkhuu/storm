/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PortUnreachableException;
import java.net.SocketTimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.dns.NamingService;
import org.xbill.java.dns.unsafe.UDPClient;
import org.xbill.java.dns.unsafe.protocol.DClass;
import org.xbill.java.dns.unsafe.protocol.Message;
import org.xbill.java.dns.unsafe.protocol.Name;
import org.xbill.java.dns.unsafe.protocol.Section;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.Record;

import aQute.bnd.annotation.component.Component;

/**
 * A implementation of the {@link NamingService}.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component
public class NamingServiceImpl implements NamingService
{

	private static final Logger s_log = LoggerFactory.getLogger(NamingServiceImpl.class);
	// TODO: reconfigure to port 53 when were near complete.
	private InetSocketAddress DNS_SERVER = new InetSocketAddress("0.0.0.0", 8053);
	private static final int MAX_ATTEMPTS = 10;

	/**
	 * Resolves host names to IP addresses.
	 * 
	 * @param hostname
	 *            the host name to resolve
	 * @return the 4 byte IP address as an byte array.
	 */
	@Override
	public byte[] resolve(String hostname)
	{
		ARecord address = doResolveWithRetry(hostname);
		return address != null ? address.getAddress().getAddress() : null;
	}

	/**
	 * Resolve the host name as return the result in its compacted integer form
	 * 
	 * @param hostname
	 *            the host name
	 * @return the IP address as an integer.
	 */
	@Override
	public int resolveAsInt(String hostname)
	{
		byte[] address = resolve(hostname);
		return address != null ? fromArray(address) : null;
	}

	/**
	 * Resolve the host name as return the result in its dot delimited string
	 * form.
	 * 
	 * @param hostname
	 *            the host name
	 * @return the IP address as a string.
	 */
	@Override
	public String resolveAsString(String hostname)
	{
		ARecord address = doResolveWithRetry(hostname);
		return address != null ? address.getAddress().getHostAddress() : null;
	}

	/**
	 * Makes the actual resolving call.
	 * 
	 * @param hostname
	 *            the host name
	 * @return the a record
	 */
	private ARecord doResolve(String hostname)
	{
		ARecord address = null;
		try
		{
			if (hostname.charAt(hostname.length() - 1) != '.')
			{
				hostname = hostname + ".";
			}
			Message qry = Message.newQuery(new ARecord(new Name(hostname), DClass.IN, 180, InetAddress.getLocalHost()));
			byte[] results = UDPClient.sendrecv(new InetSocketAddress(0), DNS_SERVER, qry.toWire(), 512, System.currentTimeMillis() + 10000);
			Message answer = new Message(results);
			Record[] rec = answer.getSectionArray(Section.ANSWER);
			if (rec.length > 0)
			{
				address = (ARecord) rec[0];
			}
		}
		catch (PortUnreachableException | SocketTimeoutException e)
		{
			// Could mean DNS is still starting.
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
		return address;
	}

	/**
	 * Makes the actual resolving call.
	 * 
	 * @param hostname
	 *            the host name
	 * @return the a record
	 */
	private ARecord doResolveWithRetry(String hostname)
	{

		ARecord result = null;
		int attempts = 0;
		while (attempts < MAX_ATTEMPTS)
		{
			try
			{
				result = doResolve(hostname);
				if (result != null)
				{
					break;
				}
			}
			catch (Exception e)
			{
				try
				{
					Thread.sleep(1000 * attempts);
				}
				catch (InterruptedException e1)
				{
					// ignore
				}
			}
			attempts++;
		}
		return result;

	}

	/**
	 * Converts an 4 byte IP address to an integer.
	 * 
	 * @param array
	 *            the array
	 * @return the integer representation of the IP.
	 */
	private final int fromArray(byte[] array)
	{
		return (array[0] & 0xFF) << 24 | (array[1] & 0xFF) << 16 | (array[2] & 0xFF) << 8 | array[3] & 0xFF;
	}

}
