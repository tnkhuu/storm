/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.server;

/**
 * Global DNS Configuration keys.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Settings
{
	String DNS_PORT = "dns_port";
	String RCV_BUF = "dns_packet_recieve_buffer";
	String SEND_BUF = "dns_packet_send_buffer";
	String RECURSIVE_RES = "dns_recursive_resolution_target";
	String RECURSIVE_RES_PORT = "dns_recursive_resolution_port";
	String REDIS_SERVER = "dns_redis_server";
	String REDIS_PORT = "dns_redis_port";
	String DNS_PROP = "dns_properties";

}
