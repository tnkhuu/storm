/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.util.Collection;
import java.util.TreeSet;

/**
 * The Class BytesKeyZSet.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BytesKeyZSet extends TreeSet<ZSetEntry>
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The map. */
	private BytesKeyObjectMap<ZSetEntry> map;

	/**
	 * Instantiates a new bytes key z set.
	 */
	public BytesKeyZSet()
	{
	}

	/**
	 * Instantiates a new bytes key z set.
	 * 
	 * @param destination
	 *            the destination
	 */
	public BytesKeyZSet(BytesKeyZSet destination)
	{
		super(destination);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.TreeSet#add(java.lang.Object)
	 */
	@Override
	public boolean add(ZSetEntry zSetEntry)
	{
		getMap().put(zSetEntry.getKey(), zSetEntry);
		return super.add(zSetEntry);
	}

	/**
	 * Gets the map.
	 * 
	 * @return the map
	 */
	private BytesKeyObjectMap<ZSetEntry> getMap()
	{
		if (map == null)
		{
			map = new BytesKeyObjectMap<ZSetEntry>();
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.TreeSet#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o)
	{
		if (o instanceof ZSetEntry)
		{
			ZSetEntry removed = getMap().remove(((ZSetEntry) o).getKey());
			return removed != null && super.remove(removed);
		}
		return super.remove(o);
	}

	/**
	 * Gets the.
	 * 
	 * @param key
	 *            the key
	 * @return the z set entry
	 */
	public ZSetEntry get(BytesKey key)
	{
		return getMap().get(key);
	}

	/**
	 * Gets the.
	 * 
	 * @param key
	 *            the key
	 * @return the z set entry
	 */
	public ZSetEntry get(byte[] key)
	{
		return getMap().get(new BytesKey(key));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.TreeSet#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends ZSetEntry> c)
	{
		boolean changed = super.addAll(c);
		for (ZSetEntry zSetEntry : c)
		{
			getMap().put(zSetEntry.getKey(), zSetEntry);
		}
		return changed;
	}
}
