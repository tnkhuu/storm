/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.server;

import java.util.Iterator;

import org.storm.api.services.RegistrationService;
import org.storm.nexus.api.Role;
import org.xbill.java.dns.unsafe.record.ARecord;
import org.xbill.java.dns.unsafe.record.RRset;
import org.xbill.java.dns.unsafe.record.Record;

/**
 * A registration service for the DNS. 
 * Maps commonly known aliases to the available set
 * of running nodes, services or containers. 
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RegistrationServiceImpl implements RegistrationService
{
	
	private final DNSCache cache;

	/**
	 * Instantiates a new registration service impl.
	 * 
	 * @param cache
	 *            the cache
	 */
	public RegistrationServiceImpl(DNSCache cache)
	{
		this.cache = cache;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.RegistrationService#register(java.lang.String, java.lang.String)
	 */
	@Override
	public void register(String domain, String ip)
	{
		cache.addEntry(domain, ip);

	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.RegistrationService#unregister(java.lang.String, java.lang.String)
	 */
	@Override
	public void unregister(String domain, String ip)
	{
		RRset set = cache.getEntry(domain);
		if (set != null)
		{
			if (set.size() == 1)
			{
				cache.removeEntry(ip);
			}
			else
			{
				Record toRemove = null;
				Iterator<Record> recordIter = set.rrs();
				while (recordIter.hasNext())
				{
					Record r = recordIter.next();
					if (r instanceof ARecord)
					{
						ARecord ar = (ARecord) r;
						if (ip.equals(ar.getAddress().getHostAddress()))
						{
							toRemove = ar;
							break;
						}
					}
				}
				if(toRemove != null) {
					set.deleteRR(toRemove);
				}
			}
		}

	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.RegistrationService#isregistered(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean isregistered(String domain, String ip)
	{
		RRset set = cache.getEntry(domain);
		if (set != null)
		{
			if (set.size() == 1)
			{
				return true;
			}
			else
			{
				Iterator<Record> recordIter = set.rrs();
				while (recordIter.hasNext())
				{
					Record r = recordIter.next();
					if (r instanceof ARecord)
					{
						ARecord ar = (ARecord) r;
						if (ip.equals(ar.getAddress().getHostAddress()))
						{
							return true;
						}
					}
				}
				return false;
			}
		}
		else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.RegistrationService#register(org.storm.api.kernel.Role, java.lang.String)
	 */
	@Override
	public void register(Role role, String ip)
	{
		register(role.getDomain(true), ip);
		
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.RegistrationService#unregister(org.storm.api.kernel.Role, java.lang.String)
	 */
	@Override
	public void unregister(Role domain, String ip)
	{
		unregister(domain.getDomain(true), ip);
		
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.RegistrationService#isregistered(org.storm.api.kernel.Role, java.lang.String)
	 */
	@Override
	public boolean isregistered(Role domain, String ip)
	{
		return isregistered(domain.getDomain(true), ip);
	}

}
