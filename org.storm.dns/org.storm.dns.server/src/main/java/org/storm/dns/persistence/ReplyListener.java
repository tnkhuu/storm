/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

/**
 * Callback interface for subscribe commands.
 * 
 * @see ReplyEvent
 */
public interface ReplyListener
{

	/**
	 * Subscribed.
	 * 
	 * @param name
	 *            the name
	 * @param channels
	 *            the channels
	 */
	void subscribed(byte[] name, int channels);

	/**
	 * Psubscribed.
	 * 
	 * @param name
	 *            the name
	 * @param channels
	 *            the channels
	 */
	void psubscribed(byte[] name, int channels);

	/**
	 * Unsubscribed.
	 * 
	 * @param name
	 *            the name
	 * @param channels
	 *            the channels
	 */
	void unsubscribed(byte[] name, int channels);

	/**
	 * Punsubscribed.
	 * 
	 * @param name
	 *            the name
	 * @param channels
	 *            the channels
	 */
	void punsubscribed(byte[] name, int channels);

	/**
	 * Message.
	 * 
	 * @param channel
	 *            the channel
	 * @param message
	 *            the message
	 */
	void message(byte[] channel, byte[] message);

	/**
	 * Pmessage.
	 * 
	 * @param pattern
	 *            the pattern
	 * @param channel
	 *            the channel
	 * @param message
	 *            the message
	 */
	void pmessage(byte[] pattern, byte[] channel, byte[] message);
}
