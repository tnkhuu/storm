/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.dns.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * The ZSet.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ZSet implements Iterable<ZSetEntry>
{
	private static final BytesKey EMPTY = new BytesKey(new byte[0]);
	private BytesKeyObjectMap<ZSetEntry> map = new BytesKeyObjectMap<ZSetEntry>();
	private List<ZSetEntry> list = new ArrayList<ZSetEntry>();

	/**
	 * Instantiates a new z set.
	 * 
	 * @param destination
	 *            the destination
	 */
	public ZSet(ZSet destination)
	{
		map.putAll(destination.map);
		list.addAll(destination.list);
	}

	/**
	 * Instantiates a new z set.
	 */
	public ZSet()
	{
	}

	/**
	 * Size.
	 * 
	 * @return the int
	 */
	public int size()
	{
		return list.size();
	}

	/**
	 * Gets the.
	 * 
	 * @param member2
	 *            the member2
	 * @return the z set entry
	 */
	public ZSetEntry get(byte[] member2)
	{
		return map.get(member2);
	}

	/**
	 * Removes the.
	 * 
	 * @param member2
	 *            the member2
	 * @return true, if successful
	 */
	public boolean remove(byte[] member2)
	{
		return remove(new BytesKey(member2));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ZSetEntry> iterator()
	{
		return list.iterator();
	}

	/**
	 * Gets the.
	 * 
	 * @param key
	 *            the key
	 * @return the z set entry
	 */
	public ZSetEntry get(BytesKey key)
	{
		return map.get(key);
	}

	/**
	 * List.
	 * 
	 * @return the list
	 */
	public List<ZSetEntry> list()
	{
		return list;
	}

	/**
	 * The Class ScoreComparator.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	@SuppressWarnings("unused")
	private static class ScoreComparator implements Comparator<ZSetEntry>
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(ZSetEntry o1, ZSetEntry o2)
		{
			double value = o1.getScore() - o2.getScore();
			return value < 0 ? -1 : value == o1.getKey().compareTo(o2.getKey()) ? 0 : 1;
		}
	}

	/**
	 * Adds the all.
	 * 
	 * @param other
	 *            the other
	 */
	public void addAll(ZSet other)
	{
		for (ZSetEntry zSetEntry : other.list)
		{
			remove(zSetEntry.getKey());
			add(zSetEntry.getKey(), zSetEntry.getScore());
		}
	}

	/**
	 * Removes the.
	 * 
	 * @param key
	 *            the key
	 * @return true, if successful
	 */
	public boolean remove(BytesKey key)
	{
		ZSetEntry current = map.get(key);
		if (current != null)
		{
			map.remove(key);
			int index = Collections.binarySearch(list, current);
			list.remove(index);
		}
		return current == null;
	}

	/**
	 * Adds the.
	 * 
	 * @param key
	 *            the key
	 * @param score
	 *            the score
	 * @return true, if successful
	 */
	public boolean add(BytesKey key, double score)
	{
		ZSetEntry current = map.get(key);
		if (current != null)
		{
			map.remove(key);
			int index = Collections.binarySearch(list, current);
			list.remove(index);
		}
		ZSetEntry entry = new ZSetEntry(key, score);
		map.put(key, entry);
		int index = find(Collections.binarySearch(list, entry));
		list.add(index, entry);
		return current == null;
	}

	/**
	 * Sub set.
	 * 
	 * @param minIndex
	 *            the min index
	 * @param maxIndex
	 *            the max index
	 * @return the iterable
	 */
	public Iterable<ZSetEntry> subSet(final int minIndex, int maxIndex)
	{
		final int finalMaxIndex = Math.min(maxIndex, list.size() - 1);
		return new Iterable<ZSetEntry>()
		{
			@Override
			public Iterator<ZSetEntry> iterator()
			{
				return new Iterator<ZSetEntry>()
				{
					int min = minIndex;

					@Override
					public boolean hasNext()
					{
						return min <= finalMaxIndex;
					}

					@Override
					public ZSetEntry next()
					{
						return list.get(min++);
					}

					@Override
					public void remove()
					{
					}
				};
			}
		};
	}

	/**
	 * Checks if is empty.
	 * 
	 * @return true, if is empty
	 */
	public boolean isEmpty()
	{
		return list.size() == 0;
	}

	/**
	 * Sub set.
	 * 
	 * @param min
	 *            the min
	 * @param max
	 *            the max
	 * @return the list
	 */
	public List<ZSetEntry> subSet(double min, double max)
	{
		int minIndex = find(Collections.binarySearch(list, new ZSetEntry(EMPTY, min)));
		int maxIndex = find(Collections.binarySearch(list, new ZSetEntry(EMPTY, max)));
		if (list.get(maxIndex).getScore() > max)
		{
			maxIndex = maxIndex - 1;
		}
		return list.subList(minIndex, maxIndex + 1);
	}

	/**
	 * Find.
	 * 
	 * @param minIndex
	 *            the min index
	 * @return the int
	 */
	private int find(int minIndex)
	{
		return minIndex < 0 ? -(minIndex + 1) : minIndex;
	}
}
