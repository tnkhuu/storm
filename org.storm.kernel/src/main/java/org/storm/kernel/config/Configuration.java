/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.config;

/**
 * The Class Configuration.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Configuration
{

	public static final String STORM_HOME = "storm.home";
	public static final String STORM_CONF = "storm.conf";
	public static final String KERNEL_PROPERTIES = "META-INF/kernel.properties";
	public static final String OBR_PARSER_CLASS = "obr.xml.class";
	public static final String BUNDLE_DROPIN_FOLDER = "/drop";
	public static final String OBR_PROTOCOL = "obr";
	public static final String BUNDLE_WATCH_POLL_DELAY = "3000";
	public static final String DEFAULT_START_LEVEL = "1";
	public static final String NO_DELAY = "true";

}
