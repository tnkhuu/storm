/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.config;

import java.util.Properties;

/**
 * The Velocity Template Config
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class VelocityConfig
{

	/**
	 * Creates the default.
	 * 
	 * @return the properties
	 */
	public static Properties createDefault(String templatesDir) {
		Properties props = new Properties();
		props.put("resource.loader", "file");
		props.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
		props.put("file.resource.loader.path", templatesDir);
		props.put("file.resource.loader.cache", "true");
		return props;
	}
}
