/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.storm.kernel.core.replicate;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.storm.api.Storm;
import org.storm.api.services.ReplicationService;
import org.storm.api.services.vm.build.Plan;
import org.storm.api.services.vm.build.Step;
import org.storm.kernel.core.replicate.steps.DuplicateDistributionStep;

/**
 * The Class LocalReplicator.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class LocalReplicator implements ReplicationService
{
	private static final String CLONE_JVM = "Clone Storm JVM";

	/**
	 * Replicate another instance of this storm platform to the given URL
	 * target.
	 * 
	 * @param target
	 *            the target
	 * @return a plan on how to do replicate.
	 */
	@Override
	public Plan replicate(final URL target)
	{
		return new Plan()
		{
			@Override
			public List<? extends Step> getSteps()
			{
				return Arrays.asList(new DuplicateDistributionStep(target, System.getProperty(Storm.VERSION)));
			}

			@Override
			public String getName()
			{
				return CLONE_JVM;
			}
		};

	}

}
