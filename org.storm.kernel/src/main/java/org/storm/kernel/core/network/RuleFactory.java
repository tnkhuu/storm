/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core.network;

import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;


/**
 * A factory for creating Rule objects.
 */
public class RuleFactory
{
	
	private static final String DEFAULT_ETH = "eth0";

	/**
	 * Forward port rule.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param dport
	 *            the dport
	 * @param toPort
	 *            the to port
	 * @return the rule
	 */
	public static Rule forwardPortRule(Protocol protocol,  int dport, int toPort) {
		return forwardPortRule(protocol, ChainType.PREROUTING, DEFAULT_ETH, dport, toPort);
	}
	
	public static Rule forwardPortRule(Protocol protocol,ChainType type, String ethernetDevice,  int dport, String ip, int toPort) {
		return Rule.buildFromCommand(String.format("-A %s -i %s -p %s -m %s --dport %s -j DNAT --to-destination %s",type.name(), ethernetDevice, protocol.name().toLowerCase(), protocol.name().toLowerCase(), dport, ip + ":" + toPort));
	}
	
	/**
	 * Forward port rule.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param dport
	 *            the dport
	 * @param toPort
	 *            the to port
	 * @return the rule
	 */
	public static Rule forwardPortRule(Protocol protocol, ChainType type, int dport, int toPort) {
		return forwardPortRule(protocol, type, DEFAULT_ETH, dport, toPort);
	}
	
	/**
	 * Forward port rule.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param ethernetDevice
	 *            the ethernet device
	 * @param dport
	 *            the dport
	 * @param toPort
	 *            the to port
	 * @return the rule
	 */
	public static Rule forwardPortRule(Protocol protocol, ChainType type, String ethernetDevice, int dport, int toPort) {
		
		return Rule.buildFromCommand(String.format("-A %s -i %s -p %s -m %s --dport %s -j REDIRECT --to-ports %s",type.name(), ethernetDevice, protocol.name().toLowerCase(), protocol.name().toLowerCase(), dport, toPort));
	}
	
	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		System.out.println(RuleFactory.forwardPortRule(Protocol.TCP, 80, 8080));
	}
}
