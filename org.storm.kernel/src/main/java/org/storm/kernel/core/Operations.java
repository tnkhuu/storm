/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Kernel;
import org.storm.api.Storm;
import org.storm.api.event.StormEvent;
import org.storm.api.factories.FilterFactory;
import org.storm.api.kernel.Actor;
import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;
import org.storm.api.network.TableType;
import org.storm.api.services.LogicalVolumeService;
import org.storm.api.services.RoutingTableService;
import org.storm.api.services.TopologyService;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.kernel.core.listener.CallbackListener;
import org.storm.nexus.api.EndPoint;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.api.Role;
import org.storm.nexus.api.Services;

/**
 * Launches the required containers at boot and kick starts the cloud cluster.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Operations extends Actor implements Runnable
{

	private static final Logger s_log = LoggerFactory.getLogger(Operations.class);
	private static final String RESOLV_CONF = "/etc/resolv.conf";
	private static final String LOCAL_IP = "0.0.0.0";
    private CallbackListener<TopologyService> runnable;
	private TopologyService topo;
    private volatile Kernel kernel;
	private volatile boolean started = false;
	private volatile LogicalVolumeService logicalVolumeService;
	private volatile VirtualizationProvider virtualizationProvider;


	/**
	 * Instantiates a new operations.
	 * 
	 * @param kernel - the kernel
     * @param runnable - the runnable to execute.
	 */
	public Operations(Kernel kernel, CallbackListener<TopologyService> runnable)
	{
		this.kernel = kernel;
		this.runnable = runnable;
	}

    /**
     * Run this runnable and boot the cloud environment.
     */
	public void run()
	{
		try
		{
			Role r = ((KernelImpl) kernel).getRole();
			if (Role.INITIAL == r)
			{

				ServiceReference<VirtualizationProvider> vsp = kernel.getBundleContext().getServiceReference(VirtualizationProvider.class);
				if (vsp == null)
				{
					try
					{
						kernel.getBundleContext().addServiceListener(new ServiceListener()
						{

							@Override
							public void serviceChanged(ServiceEvent event)
							{
								switch (event.getType())
								{
								case ServiceEvent.REGISTERED:
								{
									virtualizationProvider = (VirtualizationProvider) kernel.getBundleContext().getService(event.getServiceReference());
									bootCloudCluster(kernel.getBundleContext());
									break;
								}
								default:
									break;
								}

							}
						}, FilterFactory.createServiceFilterString(VirtualizationProvider.class));
					}
					catch (InvalidSyntaxException e)
					{
						// Wont happent.
					}
				}
				else
				{
					virtualizationProvider = kernel.getBundleContext().getService(vsp);

				}

				ServiceReference<LogicalVolumeService> lsr = kernel.getBundleContext().getServiceReference(LogicalVolumeService.class);
				if (lsr == null)
				{
					try
					{
						kernel.getBundleContext().addServiceListener(new ServiceListener()
						{

							@Override
							public void serviceChanged(ServiceEvent event)
							{
								switch (event.getType())
								{
								case ServiceEvent.REGISTERED:
								{
									logicalVolumeService = (LogicalVolumeService) kernel.getBundleContext().getService(event.getServiceReference());
									bootCloudCluster(kernel.getBundleContext());
									break;
								}
								default:
									break;
								}

							}
						}, FilterFactory.createServiceFilterString(LogicalVolumeService.class));
					}
					catch (InvalidSyntaxException e)
					{
						s_log.error(e.getMessage(), e);
					}
				}
				else
				{
					logicalVolumeService = kernel.getBundleContext().getService(lsr);
				}
				if (logicalVolumeService != null && virtualizationProvider != null)
				{
					bootCloudCluster(kernel.getBundleContext());
				}

			}
			else
			{
				s_log.info("Kernel is in slave state. Downloading required traits.");
				String[] traits = r.getTraits();
				List<Bundle> bundles = new ArrayList<>();
				// install all required traits and dependencies.
				for (String trait : traits)
				{
					Bundle bundle = install("http://artifacts:1331/artifacts/" + getTraitName(trait));
					if (bundle != null)
					{
						bundles.add(bundle);
					}

				}
				// start them once all downloaded.
				for (Bundle b : bundles)
				{
					try
					{
						b.start();
					}
					catch (BundleException e)
					{
						s_log.error(e.getMessage(), e);
					}
				}
			}
		}
		finally
		{
			if (topo != null && runnable != null)
			{
				runnable.onComplete(topo);
			}
		}

	}

	/**
	 * Boot cloud cluster.
	 * 
	 * @param context
	 *            the context
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	private void bootCloudCluster(final BundleContext context)
	{

		if (!started && virtualizationProvider != null && logicalVolumeService != null)
		{
			synchronized (this)
			{

				if (!started)
				{
					topo = new TopologyServiceImpl();
					started = true;

						s_log.info("Kernel is in master state. Will now launch required container services");
                        s_log.info("Launching the Storm Cluster");
						if (!logicalVolumeService.baseLxcLogicalVolumnExists())
						{
							long start = System.currentTimeMillis();
							logicalVolumeService.createLxcBaseLogicalVolumn(LogicalVolumeService.BASE_LVM_IMAGE_NAME, Role.DEFAULT.name(), LOCAL_IP, 1500);
							long end = System.currentTimeMillis() - start;
							s_log.info("Base LXC Started in {} millis", end);
						}
						long start = System.currentTimeMillis();
						final String dnsip = createContainer(Role.DNS.name().toLowerCase(), Role.DNS, LOCAL_IP, topo);
						long end = System.currentTimeMillis() - start;
						s_log.info("DNS Started in {} millis", end);
						if (dnsip != null)
						{
							applyDNSSettings(dnsip);
							Dictionary properties = new Hashtable();
							properties.put(StormEvent.SERVICE_ADDRESS, dnsip);
							Event event = new Event(StormEvent.SERVICE_DNS_READY, properties);
							kernel.getEventAdminService().postEvent(event);
							start = System.currentTimeMillis();
							String ensembleIp = createContainer(Role.ENSEMBLE.name().toLowerCase(), Role.ENSEMBLE, dnsip, topo);
							end = System.currentTimeMillis() - start;
							s_log.info("Ensemble Started in {} millis", end);
							
							start = System.currentTimeMillis();
							final String proxyAddr = createContainer(Role.PROXY.name().toLowerCase(), Role.PROXY, dnsip, topo);
							end = System.currentTimeMillis() - start;
							s_log.info("Proxy Started in {} millis", end);
							registerKernelRoutes(kernel.getBundleContext(), proxyAddr);

							properties = new Hashtable();
							properties.put(StormEvent.SERVICE_ADDRESS, ensembleIp);
							event = new Event(StormEvent.SERVICE_ENSEMBLE_READY, properties);
							kernel.getEventAdminService().postEvent(event);
							start = System.currentTimeMillis();
							createContainer(Role.SERVER.name().toLowerCase(), Role.SERVER, dnsip, topo);
							end = System.currentTimeMillis() - start;
							s_log.info("Server Started in {} millis", end);
							s_log.info("All Storm Cluster Nodes Launched. ");
						}
						else
						{
							throw new IllegalStateException("Terminating the boot sequence. Could not initialize Storm Cloud's DNS. ");
						}

				}
			}
		}
	}

	/**
	 * Creates the container.
	 * 
	 * @param name
	 *            the name
	 * @param role
	 *            the role
	 * @param dnsip
	 *            the dnsip
	 * @param topo
	 *            the topo
	 * @return the string
	 */
	private String createContainer(String name, Role role, final String dnsip, TopologyService topo)
	{
		String snapshot;
		s_log.info("Removing existing {} servers... ", role.name().toLowerCase());
		//virtualizationProvider.destroyContainer(HypervisorDriver.LXC, role.name().toLowerCase());
		snapshot = logicalVolumeService.createSnapshotVolumn(role.name().toLowerCase(), role.name(), 1500, LogicalVolumeService.BASE_LXC_LVM_DIR,
				LogicalVolumeService.LXC_VOLUMN_NAME);
		virtualizationProvider.createContainer(HypervisorDriver.LXC, role.name(), role.name().toLowerCase(), dnsip, snapshot);

		s_log.info("Launching an {} container", name);
		String ip = startContainer(virtualizationProvider, HypervisorDriver.LXC, name);
		topo.addEndpoint(role, new EndPoint(role.name(), ip));
		s_log.info("{} is now online with address {}", name, ip);
		return ip;
	}

	/**
	 * Start container.
	 * 
	 * @param vp
	 *            the vp
	 * @param type
	 *            the type
	 * @param containerName
	 *            the container name
	 * @return the string
	 */
	public String startContainer(VirtualizationProvider vp, HypervisorDriver type, String containerName)
	{
		return vp.startContainer(containerName);
	}

	/**
	 * Install.
	 * 
	 * @param url
	 *            the url
	 * @return the installed bundle
	 */
	private Bundle install(String url)
	{

		String location = url.trim();
		Bundle bundle = null;
		try
		{
			bundle = kernel.getBundleContext().installBundle(location, null);
		}
		catch (IllegalStateException ex)
		{
			s_log.error(ex.getMessage(), ex);
		}
		catch (BundleException ex)
		{
			if (ex.getNestedException() != null)
			{
				if (ex.getNestedException() instanceof ConnectException)
				{
					try
					{
						Thread.sleep(1000);
					}
					catch (InterruptedException e)
					{
                          //don't care.
					}
					install(url);
				}
				s_log.error(ex.getMessage(), ex.getNestedException());
			}
			else
			{
				s_log.error(ex.getMessage(), ex);
			}
		}
		catch (Exception ex)
		{
			s_log.error(ex.getMessage(), ex);
		}
		return bundle;

	}

	/**
	 * Gets the trait name.
	 * 
	 * @param trait
	 *            the trait
	 * @return the trait name
	 */
	private String getTraitName(String trait)
	{
		String version = System.getProperty(Storm.VERSION);
		return trait + "-" + version + ".jar";
	}

	/**
	 * Apply dns settings.
	 * 
	 * @param ip
	 *            the ip
	 * @return true, if successful
	 */
	private boolean applyDNSSettings(final String ip)
	{
		boolean applied = false;
		try
		{
			final RoutingTableService routeService = kernel.getRoutingTableService();
			routeService.setPermissions("777", RESOLV_CONF);
			File f = new File(RESOLV_CONF);
			final String existing = FileUtils.readFileToString(f);
			FileUtils.writeStringToFile(f, "nameserver " + ip + "\n", Charset.defaultCharset());

			routeService.routePorts(Protocol.UDP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 53, ip, 53);
			applied = true;
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					s_log.info("Reconfiguring the systems DNS Settings back to original values.");
					File f = new File(RESOLV_CONF);
					try
					{
						routeService.setPermissions("777", RESOLV_CONF);
						FileUtils.writeStringToFile(f, existing);
						RoutingTableService routeService = kernel.getRoutingTableService();
						routeService.deleteRoute(Protocol.UDP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 53, ip, 53);
					}
					catch (IOException e)
					{
						s_log.error(e.getMessage(), e);
					}
				}
			}));

			s_log.info("Switched over to using {} as our primary DNS. Resolution for unknown domains will be forwarded to {}(Google's DNS)", ip, Endpoints.UPSTREAM_DNS);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}

		return applied;
	}

	/**
	 * Execute.
	 * 
	 * @param commands
	 *            the commands
	 * @param workingDir
	 *            the working dir
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String execute(String[] commands, File workingDir) throws IOException
	{
		System.out.println(Arrays.toString(commands));
		Process process = Runtime.getRuntime().exec(commands, null, workingDir);
		String result = getOutput(process.getInputStream());
		System.out.println(result);
		return result;
	}

	/**
	 * Gets the output.
	 * 
	 * @param in
	 *            the in
	 * @return the output
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private String getOutput(InputStream in) throws IOException
	{
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader bi = new BufferedReader(reader);
		StringBuilder builder = new StringBuilder();
		for (String s = bi.readLine(); s != null; s = bi.readLine())
		{
			builder.append(s).append("\n");
		}
		return builder.toString();

	}

	/**
	 * Register kernel routes.
	 * 
	 * @param context
	 *            the context
	 * @param ip
	 *            the ip
	 */
	private void registerKernelRoutes(BundleContext context, String ip)
	{
		try
		{
			ServiceReference<Kernel> kernelsr = context.getServiceReference(Kernel.class);
			Kernel kernel = context.getService(kernelsr);
			RoutingTableService routeService = kernel.getRoutingTableService();
			routeService.routePorts(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 80, ip, 80);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Unregister kernel routes.
	 * 
	 * @param context
	 *            the context
	 * @param ip
	 *            the ip
	 */
	@SuppressWarnings("unused")
	private void unregisterKernelRoutes(BundleContext context, String ip)
	{
		try
		{
			ServiceReference<Kernel> kernelsr = context.getServiceReference(Kernel.class);
			Kernel kernel = context.getService(kernelsr);
			RoutingTableService routeService = kernel.getRoutingTableService();
			routeService.deleteRoute(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 80, ip, 80);

		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

}
