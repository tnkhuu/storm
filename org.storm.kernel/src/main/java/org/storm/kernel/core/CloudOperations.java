/**
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.ConnectException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Kernel;
import org.storm.api.Storm;
import org.storm.api.event.StormEvent;
import org.storm.api.factories.FilterFactory;
import org.storm.api.kernel.Actor;
import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;
import org.storm.api.network.TableType;
import org.storm.api.services.BlobStoreService;
import org.storm.api.services.ComputeService;
import org.storm.api.services.LogicalVolumeService;
import org.storm.api.services.RoutingTableService;
import org.storm.api.services.TopologyService;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.kernel.core.listener.CallbackListener;
import org.storm.lxc.VelocityConfig;
import org.storm.nexus.api.EndPoint;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.api.Role;
import org.storm.nexus.api.Services;

/**
 * Launches the required containers at boot and kick starts the
 * cloud cluster.
 *
 * @author Trung Khuu
 * @since 1.0
 */
public class CloudOperations extends Actor implements Runnable
{
    private static final Logger s_log = LoggerFactory.getLogger(CloudOperations.class);
    private static final String AMAZON_SETUP_SCRIPT = "setup-amazon.sh";
    private static final String RESOLV_CONF = "/etc/resolv.conf";
    private Kernel kernel;
    private TopologyService topologyService;
    private VelocityEngine velocity;
    private String outboundAddress;
    private volatile boolean started = false;
    private CallbackListener<TopologyService> runnable;
    private LogicalVolumeService logicalVolumeService;
    private VirtualizationProvider virtualizationProvider;
    private ComputeService computeService;


    /**
     * Instantiates a new operations.
     *
     * @param kernel the kernel
     */
    public CloudOperations(Kernel kernel, CallbackListener<TopologyService> runnable, String outboundAddress)
    {
        this.kernel = kernel;
        this.runnable = runnable;
        this.outboundAddress = outboundAddress;
        this.velocity = new VelocityEngine(VelocityConfig.createDefault(System.getProperty(Storm.STORM_HOME) + "/scripts/setup"));
    }

    /**
     * Execute this runnable and bootstrap the cloud environment.
     */
    public void run()
    {
        try
        {
            Role r = ((KernelImpl) kernel).getRole();
            if (Role.INITIAL == r)
            {

                ServiceReference<VirtualizationProvider> vsp = kernel.getBundleContext().getServiceReference(VirtualizationProvider.class);
                if (vsp == null)
                {
                    try
                    {
                        kernel.getBundleContext().addServiceListener(new ServiceListener()
                        {

                            @Override
                            public void serviceChanged(ServiceEvent event)
                            {
                                switch (event.getType())
                                {
                                    case ServiceEvent.REGISTERED:
                                    {
                                        virtualizationProvider = (VirtualizationProvider) kernel.getBundleContext().getService(event.getServiceReference());
                                        bootCloudCluster(kernel.getBundleContext());
                                        break;
                                    }
                                    default:
                                        break;
                                }

                            }
                        }, FilterFactory.createServiceFilterString(VirtualizationProvider.class));
                    }
                    catch (InvalidSyntaxException e)
                    {
                        // Wont happen.
                    }
                }
                else
                {
                    virtualizationProvider = kernel.getBundleContext().getService(vsp);

                }

                ServiceReference<LogicalVolumeService> lsr = kernel.getBundleContext().getServiceReference(LogicalVolumeService.class);
                if (lsr == null)
                {
                    try
                    {
                        kernel.getBundleContext().addServiceListener(new ServiceListener()
                        {

                            @Override
                            public void serviceChanged(ServiceEvent event)
                            {
                                switch (event.getType())
                                {
                                    case ServiceEvent.REGISTERED:
                                    {
                                        logicalVolumeService = (LogicalVolumeService) kernel.getBundleContext().getService(event.getServiceReference());
                                        bootCloudCluster(kernel.getBundleContext());
                                        break;
                                    }
                                    default:
                                        break;
                                }

                            }
                        }, FilterFactory.createServiceFilterString(LogicalVolumeService.class));
                    }
                    catch (InvalidSyntaxException e)
                    {
                        s_log.error(e.getMessage(), e);
                    }
                }
                else
                {
                    logicalVolumeService = kernel.getBundleContext().getService(lsr);
                }
                if (logicalVolumeService != null && virtualizationProvider != null)
                {
                    bootCloudCluster(kernel.getBundleContext());
                }

            }
            else
            {
                s_log.info("Kernel is in slave state. Downloading required traits.");
                String[] traits = r.getTraits();
                List<Bundle> bundles = new ArrayList<>();
                // install all required traits and dependencies.
                for (String trait : traits)
                {
                    Bundle bundle = install("http://artifacts:1331/artifacts/" + getTraitName(trait));
                    if (bundle != null)
                    {
                        bundles.add(bundle);
                    }

                }
                // start them once all downloaded.
                for (Bundle b : bundles)
                {
                    try
                    {
                        b.start();
                    }
                    catch (BundleException e)
                    {
                        s_log.error(e.getMessage(), e);
                    }
                }
            }
        }
        finally
        {
            if (topologyService != null && runnable != null)
            {
                runnable.onComplete(topologyService);
            }
        }

    }


    /**
     * Boot cloud cluster.
     *
     * @param context the context
     */
    @SuppressWarnings({"unchecked", "rawtypes", "unused"})
    private void bootCloudCluster(final BundleContext context)
    {

        if (!started && virtualizationProvider != null && logicalVolumeService != null)
        {

            synchronized (this)
            {

                if (!started)
                {
                    topologyService = new TopologyServiceImpl();
                    started = true;
                    ServiceReference<ComputeService> csr = context.getServiceReference(ComputeService.class);
                    if (csr != null)
                    {
                        computeService = context.getService(csr);
                    }

                    try
                    {
                        s_log.info("Kernel is in master state on amazon.");
                        s_log.info("Launching the Storm Cluster");
                        BlobStoreService<?> store = kernel.getBlobStore();
                        String home = System.getProperty(Storm.STORM_HOME);
                        Template config = velocity.getTemplate(AMAZON_SETUP_SCRIPT);
                        byte[] userData = getUserDataScript(config, Role.DNS);
                        VirtualMachine dns = computeService.createInstance(Role.DNS, userData, HypervisorDriver.LXC, HypervisorType.PARAVIRTUALIZED);
                        Set<String> ips = dns.getIpAddress();
                        String dnsIP = ips.iterator().next();
                        applyDNSSettings(dnsIP);

                        Dictionary properties = new Hashtable();
                        properties.put(StormEvent.SERVICE_ADDRESS, dnsIP);
                        Event event = new Event(StormEvent.SERVICE_DNS_READY, properties);
                        kernel.getEventAdminService().postEvent(event);
                        userData = getUserDataScript(config, Role.ENSEMBLE);
                        VirtualMachine ensemble = computeService.createInstance(Role.ENSEMBLE, userData, HypervisorDriver.LXC, HypervisorType.PARAVIRTUALIZED);//computeService.createInitialInstance(Role.ENSEMBLE, HypervisorDriver.LXC, userData, HypervisorType.PARAVIRTUALIZED, true);

                        ips = dns.getIpAddress();
                        String ensembleIp = ips.iterator().next();
                        userData = getUserDataScript(config, Role.PROXY);
                        VirtualMachine proxy = computeService.createInstance(Role.PROXY, userData, HypervisorDriver.LXC, HypervisorType.PARAVIRTUALIZED);//computeService.createInitialInstance(Role.ENSEMBLE, HypervisorDriver.LXC, userData, HypervisorType.PARAVIRTUALIZED, true);

                        properties = new Hashtable();
                        properties.put(StormEvent.SERVICE_ADDRESS, ensembleIp);
                        event = new Event(StormEvent.SERVICE_ENSEMBLE_READY, properties);
                        kernel.getEventAdminService().postEvent(event);
                        s_log.info("All Storm Cluster Nodes Launched. ");
                    }
                    catch (IOException e)
                    {
                        s_log.error(e.getMessage(), e);
                        throw new RuntimeException(e);
                    }

                }
            }
        }
    }

    private byte[] getUserDataScript(Template config, Role role) throws IOException, FileNotFoundException
    {
        VelocityContext vcontext = new VelocityContext();

        String ip = kernel.getMetaDataService().getIp4Address();
        if (ip == null)
        {
            ip = outboundAddress;
        }
        vcontext.put("tarUrl", "http://" + ip + ":1331/target/storm-cloud-1.0.0.tar.gz");
        vcontext.put("role", role.name());
        vcontext.put("rootip", ip);
        StringWriter writer = new StringWriter();
        config.merge(vcontext, writer);
        File configOut = new File("/tmp/setup-amazon-" + role.name().toLowerCase() + ".sh");
        FileUtils.write(configOut, writer.toString());
        return IOUtils.toByteArray(new FileInputStream(new File("/tmp/setup-amazon-" + role.name().toLowerCase() + ".sh")));
    }

    /**
     * Creates the container.
     *
     * @param name  the name
     * @param role  the role
     * @param dnsIP the dnsIP
     * @param topologyService  the topologyService
     * @return the ip address
     */
    @SuppressWarnings("unused")
    private String createContainer(String name, Role role, final String dnsIP, TopologyService topologyService)
    {
        String snapshot;
        s_log.info("Removing existing {} servers... ", role.name().toLowerCase());
        virtualizationProvider.destroyContainer(HypervisorDriver.LXC, role.name().toLowerCase());
        snapshot = logicalVolumeService.createSnapshotVolumn(role.name().toLowerCase(), role.name(), 1500, LogicalVolumeService.BASE_LXC_LVM_DIR, LogicalVolumeService.LXC_VOLUMN_NAME);
        virtualizationProvider.createContainer(HypervisorDriver.LXC, role.name(), role.name().toLowerCase(), dnsIP, snapshot);

        s_log.info("Launching an {} container", name);
        String ip = startContainer(virtualizationProvider, HypervisorDriver.LXC, name);
        topologyService.addEndpoint(role, new EndPoint(role.name(), ip));
        s_log.info("{} is now online with address {}", name, ip);
        return ip;
    }

    /**
     * Start container.
     *
     * @param vp            the vp
     * @param type          the type
     * @param containerName the container name
     * @return the string
     */
    public String startContainer(VirtualizationProvider vp, HypervisorDriver type, String containerName)
    {
        return vp.startContainer(containerName);
    }

    /**
     * Install.
     *
     * @param url the url
     * @return the bundle
     */
    private Bundle install(String url)
    {

        String location = url.trim();
        Bundle bundle = null;
        try
        {
            bundle = kernel.getBundleContext().installBundle(location, null);
        }
        catch (IllegalStateException ex)
        {
            s_log.error(ex.getMessage(), ex);
        }
        catch (BundleException ex)
        {
            if (ex.getNestedException() != null)
            {
                if (ex.getNestedException() instanceof ConnectException)
                {
                    try
                    {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e)
                    {
                          //don't care.
                    }
                    install(url);
                }
                s_log.error(ex.getMessage(), ex.getNestedException());
            }
            else
            {
                s_log.error(ex.getMessage(), ex);
            }
        }
        catch (Exception ex)
        {
            s_log.error(ex.getMessage(), ex);
        }
        return bundle;

    }

    /**
     * Gets the trait name.
     *
     * @param trait the trait
     * @return the trait name
     */
    private String getTraitName(String trait)
    {
        String version = System.getProperty(Storm.VERSION);
        return trait + "-" + version + ".jar";
    }

    /**
     * Apply dns settings.
     *
     * @param ip the ip
     * @return true, if successful
     */
    private boolean applyDNSSettings(final String ip)
    {
        boolean applied = false;
        try
        {
            final RoutingTableService routeService = kernel.getRoutingTableService();
            routeService.setPermissions("777", RESOLV_CONF);
            File f = new File(RESOLV_CONF);
            final String existing = FileUtils.readFileToString(f);
            FileUtils.writeStringToFile(f, "nameserver " + ip + "\n", Charset.defaultCharset());

            routeService.routePorts(Protocol.UDP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 53, ip, 53);
            applied = true;
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
            {

                @Override
                public void run()
                {
                    s_log.info("Reconfiguring the systems DNS Settings back to original values.");
                    File f = new File(RESOLV_CONF);
                    try
                    {
                        routeService.setPermissions("777", RESOLV_CONF);
                        FileUtils.writeStringToFile(f, existing);
                        RoutingTableService routeService = kernel.getRoutingTableService();
                        routeService.deleteRoute(Protocol.UDP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 53, ip, 53);
                    }
                    catch (IOException e)
                    {
                        s_log.error(e.getMessage(), e);
                    }
                }
            }));

            s_log.info("Switched over to using {} as our primary DNS. Resolution for unknown domains will be forwarded to {}(Google's DNS)", ip,
                    Endpoints.UPSTREAM_DNS);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        return applied;
    }

    /**
     * Execute.
     *
     * @param commands   the commands
     * @param workingDir the working dir
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String execute(String[] commands, File workingDir) throws IOException
    {
        System.out.println(Arrays.toString(commands));
        Process process = Runtime.getRuntime().exec(commands, null, workingDir);
        String result = getOutput(process.getInputStream());
        System.out.println(result);
        return result;
    }

    /**
     * Gets the output.
     *
     * @param in the in
     * @return the output
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private String getOutput(InputStream in) throws IOException
    {
        InputStreamReader reader = new InputStreamReader(in);
        BufferedReader bi = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        for (String s = bi.readLine(); s != null; s = bi.readLine())
        {
            builder.append(s).append("\n");
        }
        return builder.toString();

    }

    /**
     * Register kernel routes.
     *
     * @param context the context
     * @param ip      the ip
     */
    @SuppressWarnings("unused")
    private void registerKernelRoutes(BundleContext context, String ip)
    {
        try
        {
            ServiceReference<Kernel> kernelsr = context.getServiceReference(Kernel.class);
            Kernel kernel = context.getService(kernelsr);
            RoutingTableService routeService = kernel.getRoutingTableService();
            routeService.routePorts(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 80, ip, 80);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Unregister kernel routes.
     *
     * @param context the context
     * @param ip      the ip
     */
    @SuppressWarnings("unused")
    private void unregisterKernelRoutes(BundleContext context, String ip)
    {
        try
        {
            ServiceReference<Kernel> kernelsr = context.getServiceReference(Kernel.class);
            Kernel kernel = context.getService(kernelsr);
            RoutingTableService routeService = kernel.getRoutingTableService();
            routeService.deleteRoute(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, Services.OUTBOUND_ETHERNET_NAME, 80, ip, 80);

        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

}
