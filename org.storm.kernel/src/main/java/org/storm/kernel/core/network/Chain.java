/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.kernel.core.network;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link IPTableService} config chain.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Chain extends Command implements Cloneable, List<Rule>
{

	protected static final Pattern chainPattern = Pattern.compile(":[\\s]*([^ ]*)[\\s]*([^ ]*)]*[\\s]*(\\[(\\d*):(\\d*)\\])*");
	private static final Policy PREDEFINED_POLICY = Policy.ACCEPT;
	private final String chainName;
	private long packetsNum;
	private long bytesNum;
	private Policy defaultPolicy = PREDEFINED_POLICY;
	private final List<Rule> rules;

	public enum Policy
	{
		ACCEPT,
		DROP,
		REJECT
	}
	/**
	 * Create a chain with the specified name.
	 * 
	 * @param name
	 *            the name
	 */
	public Chain(String name)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("Invalid chain name");
		}
		this.chainName = name;
		rules = new ArrayList<Rule>();
	}

	/**
	 * Try to parse an iptables chain definition.
	 * 
	 * @param chain
	 *            the chain
	 * @return the chain
	 * @throws ParsingException
	 *             If some parsing error occurs
	 */
	protected static Chain parse(String chain) throws ParsingException
	{
		if (chain == null)
		{
			throw new NullPointerException();
		}

		Matcher chainMatcher = chainPattern.matcher(chain);

		if (!chainMatcher.find())
		{
			throw new ParsingException("Invalid chain format");
		}

		return buildParsedChain(chainMatcher);
	}

	/**
	 * Builds the parsed chain.
	 * 
	 * @param chainMatcher
	 *            the chain matcher
	 * @return the chain
	 */
	private static Chain buildParsedChain(Matcher chainMatcher)
	{
		Chain newChain = new Chain(chainMatcher.group(1));

		newChain.setDefaultPolicy(parseDefaultPolicy(chainMatcher));
		newChain.setPacketsNum(parsePacketsNum(chainMatcher));
		newChain.setBytesNum(parseBytesNum(chainMatcher));

		return newChain;
	}

	/**
	 * Parses the default policy.
	 * 
	 * @param chainMatcher
	 *            the chain matcher
	 * @return the policy
	 */
	private static Policy parseDefaultPolicy(Matcher chainMatcher)
	{
		if ("ACCEPT".equalsIgnoreCase(chainMatcher.group(2)))
		{
			return Policy.ACCEPT;
		}
		else if ("DROP".equalsIgnoreCase(chainMatcher.group(2)))
		{
			return Policy.DROP;
		}
		else
		{
			return PREDEFINED_POLICY;
		}
	}

	/**
	 * Parses the packets num.
	 * 
	 * @param chainMatcher
	 *            the chain matcher
	 * @return the long
	 */
	private static long parsePacketsNum(Matcher chainMatcher)
	{
		return Long.parseLong(chainMatcher.group(4));
	}

	/**
	 * Parses the bytes num.
	 * 
	 * @param chainMatcher
	 *            the chain matcher
	 * @return the long
	 */
	private static long parseBytesNum(Matcher chainMatcher)
	{
		return Long.parseLong(chainMatcher.group(5));
	}

	/**
	 * Set the default policy of the chain.
	 * 
	 * @param defaultPolicy
	 *            The default policy
	 */
	public void setDefaultPolicy(Policy defaultPolicy)
	{
		if (defaultPolicy == null)
		{
			throw new NullPointerException();
		}
		this.defaultPolicy = defaultPolicy;
	}

	/**
	 * Set the number of packets that matches this chain.
	 * 
	 * @param packets
	 *            the new packets num
	 */
	public void setPacketsNum(long packets)
	{
		if (packets < 0)
		{
			throw new IllegalArgumentException("The packets number cannot be less than 0, " + packets + " given");
		}
		this.packetsNum = packets;
	}

	/**
	 * Set the number of bytes that matches this chain.
	 * 
	 * @param bytes
	 *            the new bytes num
	 */
	public void setBytesNum(long bytes)
	{
		if (bytes < 0)
		{
			throw new IllegalArgumentException("The bytes number cannot be less than 0, " + bytes + " given");
		}
		this.bytesNum = bytes;
	}

	/**
	 * Gets the name.
	 * 
	 * @return The chain name
	 */
	public String getName()
	{
		return chainName;
	}

	/**
	 * Gets the packets num.
	 * 
	 * @return The number of packets that matches this chain
	 */
	public long getPacketsNum()
	{
		return packetsNum;
	}

	/**
	 * Gets the bytes num.
	 * 
	 * @return The number of bytes that matches this chain
	 */
	public long getBytesNum()
	{
		return bytesNum;
	}

	/**
	 * Gets the default policy.
	 * 
	 * @return The default policy of the chain
	 */
	public Policy getDefaultPolicy()
	{
		return defaultPolicy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.kernel.core.net.Command#getCommand()
	 */
	@Override
	public String getCommand()
	{
		StringBuilder outCommand = new StringBuilder(":" + chainName + " " + defaultPolicy);

		appendDataCounters(outCommand, packetsNum, bytesNum);
		outCommand.append('\n');

		for (Rule rule : rules)
		{
			appendDataCounters(outCommand, rule.getPacketsNum(), rule.getBytesNum());
			outCommand.append("-A " + chainName + rule.getCommand() + "\n");
		}
		return outCommand.toString();
	}

	/**
	 * Append data counters.
	 * 
	 * @param outCommand
	 *            the out command
	 * @param packetsNum
	 *            the packets num
	 * @param bytesNum
	 *            the bytes num
	 */
	private void appendDataCounters(StringBuilder outCommand, long packetsNum, long bytesNum)
	{
		if (packetsNum > -1 && bytesNum > -1)
		{
			outCommand.append(" [" + packetsNum + ":" + bytesNum + "]");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Chain clone()
	{
		try
		{
			Chain c = parse(getCommand());
			for (Rule r : rules)
			{
				c.add(r.clone());
			}
			return c;
		}
		catch (ParsingException e)
		{
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		Chain other = (Chain) obj;
		if (bytesNum != other.bytesNum)
		{
			return false;
		}
		if (chainName == null)
		{
			if (other.chainName != null)
			{
				return false;
			}
		}
		else if (!chainName.equals(other.chainName))
		{
			return false;
		}
		if (defaultPolicy != other.defaultPolicy)
		{
			return false;
		}
		if (packetsNum != other.packetsNum)
		{
			return false;
		}
		if (rules == null)
		{
			if (other.rules != null)
			{
				return false;
			}
		}
		else if (!rules.equals(other.rules))
		{
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.kernel.core.net.Command#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder out = new StringBuilder("Chain " + chainName + " (policy " + defaultPolicy + " " + packetsNum + " packets, " + bytesNum + " bytes)\n");
		for (Rule r : rules)
		{
			out.append(r.toString() + "\n");
		}
		return out.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	@Override
	public void add(int index, Rule element)
	{
		rules.add(index, element);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#add(java.lang.Object)
	 */
	@Override
	public boolean add(Rule e)
	{
		return rules.add(e);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends Rule> c)
	{
		return rules.addAll(c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	@Override
	public boolean addAll(int index, Collection<? extends Rule> c)
	{
		return rules.addAll(index, c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#clear()
	 */
	@Override
	public void clear()
	{
		rules.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o)
	{
		return rules.contains(o);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(Collection<?> c)
	{
		return rules.containsAll(c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#get(int)
	 */
	@Override
	public Rule get(int index)
	{
		return rules.get(index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return rules.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#indexOf(java.lang.Object)
	 */
	@Override
	public int indexOf(Object o)
	{
		return rules.indexOf(o);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#isEmpty()
	 */
	@Override
	public boolean isEmpty()
	{
		return rules.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#iterator()
	 */
	@Override
	public Iterator<Rule> iterator()
	{
		return rules.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#lastIndexOf(java.lang.Object)
	 */
	@Override
	public int lastIndexOf(Object o)
	{
		return rules.lastIndexOf(o);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#listIterator()
	 */
	@Override
	public ListIterator<Rule> listIterator()
	{
		return rules.listIterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#listIterator(int)
	 */
	@Override
	public ListIterator<Rule> listIterator(int index)
	{
		return rules.listIterator(index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#remove(int)
	 */
	@Override
	public Rule remove(int index)
	{
		return rules.remove(index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o)
	{
		return rules.remove(o);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#removeAll(java.util.Collection)
	 */
	@Override
	public boolean removeAll(Collection<?> c)
	{
		return rules.removeAll(c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#retainAll(java.util.Collection)
	 */
	@Override
	public boolean retainAll(Collection<?> c)
	{
		return rules.retainAll(c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#set(int, java.lang.Object)
	 */
	@Override
	public Rule set(int index, Rule element)
	{
		return rules.set(index, element);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#size()
	 */
	@Override
	public int size()
	{
		return rules.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#subList(int, int)
	 */
	@Override
	public List<Rule> subList(int fromIndex, int toIndex)
	{
		return rules.subList(fromIndex, toIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#toArray()
	 */
	@Override
	public Object[] toArray()
	{
		return rules.toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#toArray(T[])
	 */
	@Override
	public <T> T[] toArray(T[] a)
	{
		return rules.toArray(a);
	}
}
