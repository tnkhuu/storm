/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.storm.api.services.TopologyService;
import org.storm.nexus.api.EndPoint;
import org.storm.nexus.api.Role;

/**
 * The Map of known active servers running in the cloud, along with their roles.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TopologyServiceImpl implements TopologyService
{

	private Map<Role, List<EndPoint>> deploymentTopology = new HashMap<Role, List<EndPoint>>();
	private ReentrantLock lock = new ReentrantLock();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.TopologyService#findServersWithRole(org.storm.
	 * api.kernel.Role)
	 */
	@Override
	public List<EndPoint> findServersWithRole(Role role)
	{
		lock.lock();
		try
		{
			List<org.storm.nexus.api.EndPoint> points = deploymentTopology.get(role);
			return points;
		}
		finally
		{
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.TopologyService#addEndpoint(org.storm.api.kernel
	 * .Role, org.storm.api.EndPoint)
	 */
	@Override
	public void addEndpoint(Role role, EndPoint point)
	{
		lock.lock();
		try
		{
			List<EndPoint> points = deploymentTopology.get(role);
			if (points == null)
			{
				points = new ArrayList<>();
				
			}
			points.add(point);
			deploymentTopology.put(role, points);
		}
		finally
		{
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.TopologyService#removeEndpoint(org.storm.api.kernel
	 * .Role, org.storm.api.EndPoint)
	 */
	@Override
	public boolean removeEndpoint(Role role, EndPoint point)
	{
		lock.lock();
		try
		{
			List<EndPoint> points = deploymentTopology.get(role);
			if (points != null)
			{
				return points.remove(point);
			}
			return false;
		}
		finally
		{
			lock.unlock();
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.TopologyService#lock()
	 */
	public void lock()
	{
		lock.lock();
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.TopologyService#unlock()
	 */
	public void unlock()
	{
		lock.unlock();
	}
}
