/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core.ssh;

import java.io.InputStream;

import org.storm.api.services.vm.VirtualMachine;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/**
 * The Class RemoteSSH.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RemoteSSH
{
	private final JSch jsch;
	private static final String STRICTLY_HOST_CHECKING = "StrictHostKeyChecking";
	private static final String NO = "no";
	private static final String DEFAULT_USER = "ubuntu";
	private static final String EXEC_SHELL = "exec";
	private static final String SUDO = "sudo ";
	
	/**
	 * Instantiates a new remote ssh.
	 */
	public RemoteSSH()
	{
		jsch = new JSch();
		JSch.setConfig(STRICTLY_HOST_CHECKING, NO);
	}

	/**
	 * 
	 * @param host
	 * @param command
	 * @return
	 */
	public static String execute(VirtualMachine host, String command)
	{
		return new RemoteSSH().doExecute(host, command);
	}
	
	/**
	 * 
	 * @param host
	 * @param pem
	 * @param command
	 * @return
	 */
	public static String execute(String host, String pem, String command)
	{
		return new RemoteSSH().doExecute(host, pem, command);
	}
	/**
	 * Execute.
	 * 
	 * @param host
	 *            the host
	 * @param command
	 *            the command
	 * @return the string
	 */
	private String doExecute(String host, String pem, String command)
	{
		String res = null;
		try
		{
			Session session = jsch.getSession(DEFAULT_USER, host);
			jsch.addIdentity(pem);
			session.connect();
			Channel channel = session.openChannel(EXEC_SHELL);

			((ChannelExec) channel).setCommand(SUDO + command);
			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true)
			{
				while (in.available() > 0)
				{
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
					{
						break;
					}
					res = new String(tmp, 0, i);
				}
				if (channel.isClosed())
				{
					break;
				}
				try
				{
					Thread.sleep(1000);
				}
				catch (Exception ee)
				{
				}
			}
			channel.disconnect();
			session.disconnect();
		}
		catch (Exception e)
		{
			System.out.println(e);

		}

		return res;
	}

	/**
	 * Execute.
	 * 
	 * @param host
	 *            the host
	 * @param command
	 *            the command
	 * @return the string
	 */
	private String doExecute(VirtualMachine host, String command)
	{
		String res = null;
		try
		{
			String[] ips = host.getIpAddress().toArray(new String[host.getIpAddress().size()]);
			if (ips.length > 0)
			{
				Session session = jsch.getSession(DEFAULT_USER, ips[0]);
				jsch.addIdentity(host.getPrivateKeyPair());
				session.connect();
				Channel channel = session.openChannel(EXEC_SHELL);
				((ChannelExec) channel).setCommand(SUDO + command);
				InputStream in = channel.getInputStream();
				channel.connect();
				byte[] tmp = new byte[1024];
				while (true)
				{
					while (in.available() > 0)
					{
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
						{
							break;
						}
						res = new String(tmp, 0, i);
					}
					if (channel.isClosed())
					{
						break;
					}
					try
					{
						Thread.sleep(1000);
					}
					catch (Exception ee)
					{
					}
				}
				channel.disconnect();
				session.disconnect();
			}
		}
		catch (Exception e)
		{
			System.out.println(e);

		}

		return res;
	}
}
