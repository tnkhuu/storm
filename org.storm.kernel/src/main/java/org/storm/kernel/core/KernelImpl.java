/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.felix.bundlerepository.RepositoryService;
import org.apache.felix.fileinstall.internal.FileInstall;
import org.fusesource.hawtdispatch.Dispatcher;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.EventAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Kernel;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.kernel.Actor;
import org.storm.api.services.BlobStoreService;
import org.storm.api.services.BundleInstallService;
import org.storm.api.services.MetaDataService;
import org.storm.api.services.ReplicationService;
import org.storm.api.services.RoutingTableService;
import org.storm.api.services.SSHKeyPairService;
import org.storm.api.services.TopologyService;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.kernel.core.listener.CallbackListener;
import org.storm.nexus.api.Role;

/**
 * The Storm Platform Kernel.
 * 
 * The barebone's minimum form of the Storm platform is it's kernel. From here,
 * it is able to dynamically grab and install the features required to perform
 * requested task, replicate itself across to other nodes, and launch the
 * appropriate back end wiring to form a cloud network. It also is responsible
 * for
 * 
 * - Managing the system internals of the node this kernel is running on. Takes
 * care of memory, disk, network and underlying system resources.
 * 
 * - Handling the life cycle of the platform, including the synchronization of
 * applications, versions and configuring the platform with the required purpose
 * a node was meant to serve.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public final class KernelImpl extends Actor implements Platform, Kernel
{
	private final static Logger s_log = LoggerFactory.getLogger(KernelImpl.class);
	private double VERSION = 1.0;
	private ExecutorService executor = Executors.newCachedThreadPool();
	private ConcurrentHashMap<VirtualMachine, Role> vms = new ConcurrentHashMap<VirtualMachine, Role>();
	private transient volatile BundleContext context;
	private transient volatile CloudProvider provider;
	private transient volatile TopologyService topologyService;
	private transient volatile MetaDataService metaDataService;
	private transient volatile FileInstall fileInstallService;
	private transient volatile ReplicationService replicationService;
	private transient volatile RoutingTableService routingTableService;
	private transient volatile RepositoryService repositoryService;
	private transient volatile EventAdmin eventAdminService;
	private transient volatile SSHKeyPairService keypairService;
	private transient volatile BlobStoreService<?> blobStore;
	private transient volatile Dispatcher dispatcherService;
	
	public void init() {
		switch(getCloudProvider()) {
		case AMAZON:
		{
		/*	executor.execute(new CloudOperations(this, new CallbackListener<TopologyService>()
					{
						@Override
						public void onComplete(TopologyService topology)
						{
							KernelImpl.this.topologyService = topology;
							context.registerService(TopologyService.class, KernelImpl.this.topologyService, null);
							
						}
					}, getOutboundAddress()));*/
			executor.execute(new EnsembleLauncher(this));
			break;
		}
		case VIRTUALBOX: {
			executor.execute(new Operations(this, new CallbackListener<TopologyService>()
					{
						@Override
						public void onComplete(TopologyService topology)
						{
							KernelImpl.this.topologyService = topology;
							context.registerService(TopologyService.class, KernelImpl.this.topologyService, null);
							
						}
					}));
			break;
		}
		default: {
			s_log.error("No Cloud Provider was found during the instantiation of this kernel.");
			break;
		}
		}
	}

	

	/**
	 * Gets the version
	 * 
	 * @return the version
	 */
	@Override
	public double getVersion()
	{
		return VERSION;
	}

	/**
	 * Gets the BundleInstallService
	 * 
	 * @return the BundleInstallService
	 */
	@Override
	public BundleInstallService getBundleInstallService()
	{
		return fileInstallService;
	}

	/**
	 * Gets the RepositoryService
	 * 
	 * @return the RepositoryService
	 */
	@Override
	public RepositoryService getRepositoryService()
	{
		return repositoryService;
	}

	/**
	 * Gets the routing table service.
	 * 
	 * @return the routing service
	 */
	@Override
	public ReplicationService getReplicationService()
	{
		return replicationService;
	}

	/**
	 * Gets the routing table service.
	 * 
	 * @return the routing service
	 */
	@Override
	public RoutingTableService getRoutingTableService()
	{
		return routingTableService;
	}
	
	/**
	 * Gets the BundleContext
	 * 
	 * @return the BundleContext
	 */
	public BundleContext getBundleContext() {
		return context;
	}
	
	/**
	 * Gets the topology service.
	 * 
	 * @return the topology service
	 */
	public TopologyService getTopologyService() {
		return topologyService;
	}

	/**
	 * Return the event admin service.
	 * 
	 * @return the dispatcher service.
	 */
	@Override
	public EventAdmin getEventAdminService()
	{
		return eventAdminService;
	}
	
	/**
	 * Return the dispatcher service.
	 * 
	 * @return the dispatcher service.
	 */
	public Dispatcher getDispatcherService() {
		return dispatcherService;
	}
	
	/**
	 * Gets the SSH key pair service.
	 * 
	 * @return the SSH key pair service
	 */
	public SSHKeyPairService getSSHKeyPairService() {
		return keypairService;
	}
	
	/**
	 * Retrieve the meta data service.
	 * 
	 * @return the meta data service.
	 */
	public MetaDataService getMetaDataService() {
		return metaDataService;
	}
	
	/**
	 * Retrieve a role to a vm. 
	 * 
	 */
	public void register(Role role, VirtualMachine virtualMachine) {
		vms.putIfAbsent(virtualMachine, role);
	}

	/**
	 * Retrieve the underlying cloud provider.
	 * 
	 * @return {@link CloudProvider} the underlying cloud provider.
	 */
	@Override
	public CloudProvider getCloudProvider()
	{
		if(provider == null)
		{
			/*boolean isOnCloud = metaDataService.isOnCloud();
            provider = isOnCloud ? CloudProvider.AMAZON : CloudProvider.VIRTUALBOX;
           */ 
            provider = CloudProvider.VIRTUALBOX;
		}
		return provider;
	}



	/**
	 * @return the blobStore
	 */
	public BlobStoreService<?> getBlobStore()
	{
		return blobStore;
	}



	@Override
	public <S> S getService(Class<S> service)
	{
		S instance = null;
		ServiceReference<S> sr = context.getServiceReference(service);
		if(sr != null) {
			instance = context.getService(sr);
		}
		return instance;
	}
}
