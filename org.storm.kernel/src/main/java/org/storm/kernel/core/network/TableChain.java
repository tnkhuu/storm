/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.kernel.core.network;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.storm.api.network.ChainType;

/**
 * A Table of chains.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TableChain implements Iterable<Chain>
{

	private final String name;

	/** The chain associated with this table. */
	protected Map<String, Chain> chains = new HashMap<String, Chain>();

	/**
	 * Create a new table.
	 * 
	 * @param name
	 *            The name of the table
	 */
	public TableChain(String name)
	{
		if (name == null || name.isEmpty())
		{
			throw new IllegalArgumentException("Invalid table name");
		}
		this.name = name;
	}

	/**
	 * Gets the name.
	 * 
	 * @return The table name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the chains.
	 * 
	 * @return The chains associated with this table
	 */
	public Collection<Chain> getChains()
	{
		return Collections.unmodifiableCollection(chains.values());
	}

	/**
	 * Gets the chain.
	 * 
	 * @param chainName
	 *            the chain name
	 * @return The specified chain, returns null if the chain doesn't exist
	 */
	public Chain getChain(String chainName)
	{
		if (chainName == null)
		{
			throw new NullPointerException();
		}
		return chains.get(chainName);
	}
	
	/**
	 * Gets the chain.
	 * 
	 * @param chainName
	 *            the chain name
	 * @return The specified chain, returns null if the chain doesn't exist
	 */
	public Chain getChain(ChainType chain)
	{
		
		return chains.get(chain.name());
	}

	/**
	 * Add the specified chain to this table.
	 * 
	 * @param chain
	 *            the chain
	 */
	public void addChain(Chain chain)
	{
		if (chain == null)
		{
			throw new NullPointerException();
		}
		if (!chains.containsKey(chain))
		{
			chains.put(chain.getName(), chain);
		}
	}

	/**
	 * Gets the command.
	 * 
	 * @return The corresponding iptables command
	 */
	public String getCommand()
	{
		if (chains.isEmpty())
		{
			return "";
		}
		StringBuilder out = new StringBuilder("*" + name + "\n");
		for (Chain c : this)
		{
			out.append(c.getCommand() + "\n");
		}
		return out.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder out = new StringBuilder(name + " table:\n");
		for (Chain c : chains.values())
		{
			out.append(c.toString() + "\n");
		}
		return out.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Chain> iterator()
	{
		return chains.values().iterator();
	}
}