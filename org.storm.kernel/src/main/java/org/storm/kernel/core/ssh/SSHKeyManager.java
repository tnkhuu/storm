/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core.ssh;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.storm.api.Storm;
import org.storm.api.security.Encryption;
import org.storm.api.security.SSHKeyPair;
import org.storm.api.services.SSHKeyPairService;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;

/**
 * SSH Key Manager.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SSHKeyManager implements SSHKeyPairService
{

	private JSch shell;
	private int DEFAULT_KEY_STRENGTH = 2048;
	private String DEFAULT_KEY_STORE = System.getProperty(Storm.STORM_HOME) + File.separator + "/keys";
	private File storeDir;
	private static String LOCALHOST;

	/**
	 * Instantiates a new sSH key manager.
	 */
	public SSHKeyManager()
	{
		shell = new JSch();
		storeDir = new File(DEFAULT_KEY_STORE);
		if (!storeDir.exists())
		{
			if (!storeDir.mkdirs())
			{
				throw new IllegalStateException("Could not create required key store directory at " + DEFAULT_KEY_STORE);
			}
		}
		try
		{
			LOCALHOST = InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e)
		{
			throw new IllegalStateException("Unable to determine localhost.");
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.core.ssh.SSHKeyPairService#getKeyPair(java.lang.String)
	 */
	@Override
	public SSHKeyPair getKeyPair(String name)
	{
		try
		{
			return KeyPair.load(shell, name);
		}
		catch (JSchException e)
		{
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.core.ssh.SSHKeyPairService#createKeyPair(com.jcraft.jsch.Type, int)
	 */
	@Override
	public SSHKeyPair createKeyPair(Encryption type, int strength)
	{
		try
		{
			return KeyPair.genKeyPair(shell, type, strength);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.core.ssh.SSHKeyPairService#createKeyPair(com.jcraft.jsch.Type)
	 */
	@Override
	public SSHKeyPair createKeyPair(Encryption type)
	{
		try
		{
			return KeyPair.genKeyPair(shell, type, DEFAULT_KEY_STRENGTH);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.core.ssh.SSHKeyPairService#saveKeyPair(java.lang.String, com.jcraft.jsch.KeyPair)
	 */
	@Override
	public void saveKeyPair(String name, SSHKeyPair kp) throws IOException
	{

		File f = new File(DEFAULT_KEY_STORE + File.separator + name);
		if (f.exists())
		{
			throw new IllegalArgumentException("Keypair " + kp + " already exists");
		}
		if (f.createNewFile())
		{
			kp.writePrivateKey(new FileOutputStream(f));
			System.out.println(kp.getPrivateKey());
			
		}
		
		File pubKey = new File(DEFAULT_KEY_STORE + File.separator + name + ".pub");
		if (pubKey.exists())
		{
			throw new IllegalArgumentException("Keypair " + kp + " already exists");
		}
		
		if (pubKey.createNewFile())
		{
			kp.writePublicKey(new FileOutputStream(pubKey), generateComment());
			System.out.println(kp.getPublicKey());
			
		}

	}
	
	public static String generateComment() {
		String user = System.getProperty("user.name");
		StringBuilder b = new StringBuilder();
		b.append(user).append("@").append(LOCALHOST);
		return b.toString();
		
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.core.ssh.SSHKeyPairService#removeKeyPair(java.lang.String)
	 */
	@Override
	public boolean removeKeyPair(String name) throws IOException
	{
		boolean success = false;
		File privateKey = new File(DEFAULT_KEY_STORE + File.separator + name);
		File pubKey = new File(DEFAULT_KEY_STORE + File.separator + name + ".pub");
		if (privateKey.exists() && pubKey.exists())
		{
			success = privateKey.delete() && pubKey.delete();
		}
		return success;
	}
}
