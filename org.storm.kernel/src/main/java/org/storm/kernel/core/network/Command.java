/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.kernel.core.network;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * A command to execute against on the {@link IPTableService}
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class Command
{

	private final Map<String, String> options = new LinkedHashMap<String, String>();
	private final Set<String> negatedOptions = new HashSet<String>();

	/**
	 * Gets the command.
	 * 
	 * @return The commands for import in iptables
	 */
	public String getCommand()
	{
		StringBuilder out = new StringBuilder();
		for (Entry<String, String> entry : options.entrySet())
		{
			String option = entry.getKey();
			out.append(negOp(option));
			out.append(' ');
			out.append(option);
			out.append(' ');
			out.append(entry.getValue());
		}
		return out.toString();
	}

	/**
	 * Set and option and return the old value.
	 * 
	 * @param option
	 *            The option name
	 * @param value
	 *            The option value
	 * @return The old value
	 */
	public String setOption(String option, String value)
	{
		if (option == null || value == null)
		{
			throw new NullPointerException();
		}
		String old = options.put(option, value);
		if (old == null)
		{
			return "";
		}
		return old;
	}

	/**
	 * Set and option and his negation status, return the old value.
	 * 
	 * @param option
	 *            The option name
	 * @param value
	 *            The option value
	 * @param isNegated
	 *            The negation status, true means the option is negated
	 * @return The old value
	 */
	public String setOption(String option, String value, boolean isNegated)
	{
		setNegated(option, isNegated);
		return setOption(option, value);
	}

	/**
	 * Unset the specified option and return the old value.
	 * 
	 * @param option
	 *            the option
	 * @return The old value
	 */
	public String unsetOption(String option)
	{
		if (option == null)
		{
			throw new NullPointerException();
		}
		String old = options.remove(option);
		if (old == null)
		{
			return "";
		}
		return old;
	}

	/**
	 * Check if an option is setted.
	 * 
	 * @param option
	 *            the option
	 * @return True if the option is setted
	 */
	public boolean isOptionSetted(String option)
	{
		if (option == null)
		{
			throw new NullPointerException();
		}
		return options.containsKey(option);
	}

	/**
	 * Get the value of the specified option, returns empty string if the
	 * specified option is not setted.
	 * 
	 * @param option
	 *            the option
	 * @return the option
	 */
	public String getOption(String option)
	{
		if (option == null)
		{
			throw new NullPointerException();
		}
		String o = options.get(option);
		if (o == null)
		{
			return "";
		}
		return o;
	}

	/**
	 * Gets the options.
	 * 
	 * @return The map of the options
	 */
	public Map<String, String> getOptions()
	{
		return Collections.unmodifiableMap(options);
	}

	/**
	 * Set an option as negated with the negated operator ( ! option ).
	 * 
	 * @param option
	 *            The option to negate
	 * @param isNegated
	 *            The negation status of the option, the default is false. To
	 *            negate the option set isNegated to true.
	 */
	public void setNegated(String option, boolean isNegated)
	{
		if (option == null)
		{
			throw new NullPointerException();
		}
		if (isNegated)
		{
			negatedOptions.add(option);
		}
		else
		{
			negatedOptions.remove(option);
		}
	}

	/**
	 * Checks if is negated.
	 * 
	 * @param option
	 *            the option
	 * @return True if the specified option is set as negated
	 */
	public boolean isNegated(String option)
	{
		if (option == null)
		{
			throw new NullPointerException();
		}
		return negatedOptions.contains(option);
	}

	/**
	 * Gets the negated options.
	 * 
	 * @return The set of negated options
	 */
	public Set<String> getNegatedOptions()
	{
		return Collections.unmodifiableSet(negatedOptions);
	}

	/**
	 * Convenience method to add the negated operator in output if an option is
	 * negated.
	 * 
	 * @param option
	 *            the option
	 * @return The " !" string if the option is negated, empty string otherwise
	 */
	protected String negOp(String option)
	{
		if (option == null)
		{
			throw new NullPointerException();
		}
		return isNegated(option) ? " !" : "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder out = new StringBuilder();

		for (Entry<String, String> e : getOptions().entrySet())
		{
			out.append(e.getKey());

			String value = e.getValue();
			out.append('=' + value + ", ");
		}

		if (out.length() > 2)
		{
			out.delete(out.length() - 2, out.length());
		}

		Iterator<String> i = getNegatedOptions().iterator();

		if (!i.hasNext())
		{
			return out.toString();
		}

		out.append(", Neg(" + i.next());

		while (i.hasNext())
		{
			out.append(',' + i.next());
		}

		out.append(')');
		return out.toString();
	}
}
