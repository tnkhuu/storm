/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.kernel.core.network;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A rule for the firewall. You can specify the generic rule options, a protocol
 * matcher(-p option), a target (-j option) and additional matcher(-m option)
 * 
 */
public class Rule extends Command implements Cloneable
{

	private static final Pattern COUNTER = Pattern.compile("\\[(\\d*):(\\d*)\\]");
	private static final Pattern ARGUMENTS = Pattern.compile("(!?)[\\s]*([-]{1,2}[-a-zA-Z0-9]*)([\\s|\"]*)([\\w|.|?|=|&|+|\\s|:|,|\\\\|/]*)([\\s|\"]|$)");
	protected String chainname = "";
	private long packets;
	private long bytes;

	public Rule()
	{
	}

	/**
	 * @return A rule created from the passed command
	 * 
	 * @throws IllegalArgumentException
	 *             If the passed command is null or is empty
	 */
	public static Rule buildFromCommand(String ruleCommand)
	{
		if (ruleCommand == null || ruleCommand.isEmpty())
		{
			throw new IllegalArgumentException("Invalid rule command");
		}

		Rule rule = new Rule();
		rule.initRule(ruleCommand);
		return rule;
	}

	/**
	 * Initialize the passed rule with the string command
	 * 
	 * @throws IllegalArgumentException
	 *             If the passed ruleCommand is invalid
	 */
	private void initRule(String ruleCommand)
	{
		if (ruleCommand == null || ruleCommand.isEmpty())
		{
			throw new IllegalArgumentException("Invalid rule command");
		}

		parseArguments(ruleCommand);
		parseDataCounters(ruleCommand);
	}

	private void parseArguments(String ruleCommand)
	{
		Matcher argumentMatcher = ARGUMENTS.matcher(ruleCommand);

		while (argumentMatcher.find())
		{
			String argumentName = argumentMatcher.group(2).trim();
			String argumentValue = argumentMatcher.group(4).trim();
			boolean isNegated = argumentMatcher.group(1) != null && !argumentMatcher.group(1).isEmpty();

			if (isChainNameArgument(argumentName))
			{
				chainname = argumentValue;
			}
			else
			{
				setOption(argumentName, argumentValue, isNegated);
			}
		}
	}

	private boolean isChainNameArgument(String argumentName)
	{
		return "-A".equals(argumentName) || "--append".equals(argumentName);
	}

	private void parseDataCounters(String ruleCommand)
	{
		Matcher counterMatcher = COUNTER.matcher(ruleCommand);
		if (counterMatcher.find())
		{
			setPacketsNum(Long.parseLong(counterMatcher.group(1)));
			setBytesNum(Long.parseLong(counterMatcher.group(2)));
		}
	}

	/**
	 * @return The name of the associated chain
	 */
	public String getChainName()
	{
		return chainname;
	}

	/**
	 * Set the number of packets that have matched this rule
	 * 
	 * @throws IllegalArgumentException
	 *             If the passed number of packets is lesser of 0
	 * 
	 */
	public void setPacketsNum(long packets)
	{
		if (packets < 0)
		{
			throw new IllegalArgumentException("The packets number cannot be less than 0, " + packets + " given");
		}
		this.packets = packets;
	}

	/**
	 * Set the number of bytes that have matched this rule
	 * 
	 * @throws IllegalArgumentException
	 *             If the passed number of bytes is lesser of 0
	 */
	public void setBytesNum(long bytes)
	{
		if (bytes < 0)
		{
			throw new IllegalArgumentException("The bytes number cannot be less than 0, " + bytes + " given");
		}
		this.bytes = bytes;
	}

	/**
	 * @return The number of packets that have matched this rule
	 */
	public long getPacketsNum()
	{
		return packets;
	}

	/**
	 * @return The number of bytes that have matched this rule
	 */
	public long getBytesNum()
	{
		return bytes;
	}

	@Override
	public String getCommand()
	{
		return super.getCommand() + getDataCounter();
	}

	private String getDataCounter()
	{
		if (getPacketsNum() > 0 || getBytesNum() > 0)
		{
			return " -c " + getPacketsNum() + " " + getBytesNum();
		}
		return "";
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof Rule)) return false;
		Rule comparator = (Rule)obj;
		return toString().equals(comparator.toString());
	}
	
	@Override
	public int hashCode()
	{
		return toString().hashCode();
	}

	/**
	 * @return A copy of this rule
	 */
	@Override
	public Rule clone()
	{
		return buildFromCommand(getCommand());
	}

	@Override
	public String toString()
	{
		return "Rule " + super.toString();
	}
}
