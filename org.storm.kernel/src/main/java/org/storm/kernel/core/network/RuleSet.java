/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.kernel.core.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Iterator;

import org.storm.api.network.TableType;

/**
 * The Class RuleSet.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RuleSet implements Iterable<TableChain>
{

	private RuleSetData data = new RuleSetData();

	/**
	 * Create an empty RuleSet.
	 */
	public RuleSet()
	{
		data.filterTable = new TableChain("filter");
		data.natTable = new TableChain("nat");
		data.mangleTable = new TableChain("mangle");
		data.rawTable = new TableChain("raw");
	}

	/**
	 * Try to parse the input stream as iptables rules.
	 * 
	 * @param ruleStream
	 *            the rule stream
	 * @return The parsed RuleSet
	 * @throws ParsingException
	 *             If some parsing error occurs
	 * @throws IOException
	 *             If some I/O error occurs in reading the input stream
	 */
	public static RuleSet parse(InputStream ruleStream) throws ParsingException, IOException
	{
		if (ruleStream == null)
		{
			throw new NullPointerException();
		}
		BufferedReader b = new BufferedReader(new InputStreamReader(ruleStream));
		StringBuilder s = new StringBuilder();
		for (String line = b.readLine(); line != null; line = b.readLine())
		{
			s.append(line + "\n");
		}
		return RuleSet.parse(s.toString());
	}
	
	public static RuleSet getOutput(InputStream in) throws IOException, ParsingException
	{
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader bi = new BufferedReader(reader);
		StringBuilder builder = new StringBuilder();
		for (String s = bi.readLine(); s != null; s = bi.readLine())
		{
			builder.append(s + "\n");
		}
		return RuleSet.parse(builder.toString());

	}

	/**
	 * Try to parse the input as iptables rules.
	 * 
	 * @param rules
	 *            the rules
	 * @return The parsed RuleSet
	 * @throws ParsingException
	 *             If the operation cannot be completed, for example for
	 *             insufficient unix privileges
	 */
	public static RuleSet parse(String rules) throws ParsingException
	{
		if (rules == null)
		{
			throw new NullPointerException();
		}

		RuleSet parsedRules = new RuleSet();
		BufferedReader r = new BufferedReader(new StringReader(rules));
		TableChain currentTable = parsedRules.data.filterTable;

		int lineNum = 1;
		String line;
		try
		{
			while ((line = r.readLine()) != null)
			{
				line = line.trim();
				if (isTableLine(line))
				{
					currentTable = getCurrentTable(parsedRules, line);
				}
				else if (!isCommentLine(line))
				{
					parseLine(line, currentTable);
				}
				lineNum++;
			}
		}
		catch (ParsingException e)
		{
			throw new ParsingException(lineNum, e.getParsingMessage());
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return parsedRules;

	}

	/**
	 * Checks if is comment line.
	 * 
	 * @param line
	 *            the line
	 * @return true, if is comment line
	 */
	private static boolean isCommentLine(String line)
	{
		return line.charAt(0) == '#';
	}

	/**
	 * Checks if is table line.
	 * 
	 * @param line
	 *            the line
	 * @return true, if is table line
	 */
	private static boolean isTableLine(String line)
	{
		return line.charAt(0) == '*';
	}

	/**
	 * Gets the current table.
	 * 
	 * @param ruleSet
	 *            the rule set
	 * @param tableLine
	 *            the table line
	 * @return the current table
	 * @throws ParsingException
	 *             the parsing exception
	 */
	private static TableChain getCurrentTable(RuleSet ruleSet, String tableLine) throws ParsingException
	{
		if ("filter".equals(tableLine.substring(1)))
		{
			return ruleSet.data.filterTable;
		}
		else if ("nat".equals(tableLine.substring(1)))
		{
			return ruleSet.data.natTable;
		}
		else if ("mangle".equals(tableLine.substring(1)))
		{
			return ruleSet.data.mangleTable;
		}
		else if ("raw".equals(tableLine.substring(1)))
		{
			return ruleSet.data.rawTable;
		}
		else
		{
			throw new ParsingException("Invalid table name " + tableLine.substring(1));
		}
	}

	/**
	 * Gets the table.
	 * 
	 * @param type
	 *            the type
	 * @return The table corresponding to the specified table type or null if no
	 *         table matches
	 */
	public TableChain getTable(TableType type)
	{
		switch (type)
		{
		case FILTER:
			return data.filterTable;
		case MANGLE:
			return data.mangleTable;
		case NAT:
			return data.natTable;
		case RAW:
			return data.rawTable;
		}
		return null;
	}

	/**
	 * Gets the tables.
	 * 
	 * @return The available tables
	 */
	public TableChain[] getTables()
	{
		TableChain[] t = new TableChain[4];
		t[0] = data.filterTable;
		t[1] = data.mangleTable;
		t[2] = data.natTable;
		t[3] = data.rawTable;
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<TableChain> iterator()
	{
		return Arrays.asList(getTables()).iterator();
	}

	/**
	 * Gets the export rules.
	 * 
	 * @return The rules in iptables format
	 */
	public String getExportRules()
	{
		return data.filterTable.getCommand() + data.natTable.getCommand() + data.mangleTable.getCommand() + data.rawTable.getCommand() + "COMMIT\n";
	}

	/**
	 * Parse a generic command line.
	 * 
	 * @param rule
	 *            the rule
	 * @param table
	 *            the table
	 * @throws ParsingException
	 *             the parsing exception
	 */
	private static void parseLine(String rule, TableChain table) throws ParsingException
	{
		if (rule.charAt(0) == ':')
		{
			parseChain(rule, table);
		}
		else
		{
			parseCommand(rule, table);
		}
	}

	/**
	 * Parse a chain command.
	 * 
	 * @param chain
	 *            the chain
	 * @param table
	 *            the table
	 * @throws ParsingException
	 *             the parsing exception
	 */
	private static void parseChain(String chain, TableChain table) throws ParsingException
	{
		table.addChain(Chain.parse(chain));
	}

	/**
	 * Parse a generic command (not chain).
	 * 
	 * @param rule
	 *            the rule
	 * @param table
	 *            the table
	 * @throws ParsingException
	 *             the parsing exception
	 */
	private static void parseCommand(String rule, TableChain table) throws ParsingException
	{

		if (!rule.startsWith("-A") && rule.charAt(0) != '[')
		{
			return;
		}

		Rule r = Rule.buildFromCommand(rule);
		if (r == null)
		{
			return;
		}
		String chainName = r.getChainName();
		Chain c = table.getChain(chainName);
		if (c == null)
		{
			throw new ParsingException("Undefined chain " + chainName);
		}

		c.add(r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return data.filterTable.toString() + "\n" + data.natTable.toString() + "\n" + data.mangleTable.toString();
	}
}