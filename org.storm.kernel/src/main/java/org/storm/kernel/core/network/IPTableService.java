/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.kernel.core.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;
import org.storm.api.network.TableType;
import org.storm.api.services.RoutingTableService;

/**
 * The Class IPTableService.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class IPTableService implements RoutingTableService
{
	private final static String IPTABLES_COMMAND = "sudo /sbin/iptables";
	private final static String EXPORT_COMMAND = "sudo /sbin/iptables-save";
	private final static String IMPORT_COMMAND = "sudo /sbin/iptables-restore";

	/**
	 * Gets the current rules.
	 * 
	 * @return the current rules
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static RuleSet getCurrentRules() throws IOException
	{
		Process p = Runtime.getRuntime().exec(IPTableService.EXPORT_COMMAND + " -c");
		readError(p.getErrorStream());
		try
		{
			return RuleSet.getOutput(p.getInputStream());
		}
		catch (ParsingException e)
		{
			throw new IOException("Invalid iptables format. " + e.getParsingMessage());
		}
	}

	/**
	 * Apply rules.
	 * 
	 * @param set
	 *            the set
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void applyRules(RuleSet set) throws IOException
	{
		if (set == null)
		{
			throw new NullPointerException();
		}
		Process p = Runtime.getRuntime().exec(IPTableService.IMPORT_COMMAND);
		OutputStream o = p.getOutputStream();
		o.write(set.getExportRules().getBytes());
		o.close();
		readError(p.getErrorStream());
	}

	/**
	 * Insert rule.
	 * 
	 * @param rule
	 *            the rule
	 * @param ruleNum
	 *            the rule num
	 * @param table
	 *            the table
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void insertRule(Rule rule, int ruleNum, TableType table) throws IOException
	{
		if (rule == null)
		{
			throw new NullPointerException();
		}

		if (ruleNum < 1)
		{
			throw new IllegalArgumentException("The rule number cannot be less than 1, " + ruleNum + " given");
		}

		if (rule.getChainName().isEmpty())
		{
			throw new IllegalArgumentException("Undefined chain for the passed rule");
		}

		if (table == null)
		{
			table = TableType.FILTER;
		}

		runIPTablesCommand("-t " + table.getName() + " -I " + rule.getChainName() + " " + " " + ruleNum + rule.getCommand());
	}

	/**
	 * Run ip tables command.
	 * 
	 * @param options
	 *            the options
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private static void runIPTablesCommand(String options) throws IOException
	{
		Process p = Runtime.getRuntime().exec(IPTABLES_COMMAND + ' ' + options);
		readError(p.getErrorStream());
	}
	
	private void runCommand(String cmd) throws IOException
	{
		Process p = Runtime.getRuntime().exec("sudo " + cmd);
		readError(p.getErrorStream());
	}

	/**
	 * Replace rule.
	 * 
	 * @param newRule
	 *            the new rule
	 * @param ruleNum
	 *            the rule num
	 * @param table
	 *            the table
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void replaceRule(Rule newRule, int ruleNum, TableType table) throws IOException
	{
		if (newRule == null)
		{
			throw new NullPointerException();
		}
		if (ruleNum < 1)
		{
			throw new IllegalArgumentException("The rule number cannot be less than 1, " + ruleNum + " given");
		}

		if (newRule.getChainName().isEmpty())
		{
			throw new IllegalArgumentException("Undefined chain for the passed rule");
		}

		if (table == null)
		{
			table = TableType.FILTER;
		}

		String counter = "";
		if (newRule.getPacketsNum() > 0 || newRule.getBytesNum() > 0)
		{
			long packets = newRule.getPacketsNum();
			newRule.setPacketsNum(0);
			long bytes = newRule.getBytesNum();
			newRule.setBytesNum(0);
			counter = " -c " + packets + " " + bytes;
		}

		runIPTablesCommand("-t " + table.getName() + " -R " + newRule.getChainName() + " " + ruleNum + " " + newRule.getCommand() + counter);
	}

	/**
	 * Append rule.
	 * 
	 * @param rule
	 *            the rule
	 * @param table
	 *            the table
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void appendRule(Rule rule, TableType table) throws IOException
	{
		if (rule == null)
		{
			throw new NullPointerException();
		}

		if (rule.getChainName().isEmpty())
		{
			throw new IllegalArgumentException("Undefined chain for the passed rule");
		}

		if (table == null)
		{
			table = TableType.FILTER;
		}

		String counter = "";
		if (rule.getPacketsNum() > 0 || rule.getBytesNum() > 0)
		{
			long packets = rule.getPacketsNum();
			rule.setPacketsNum(0);
			long bytes = rule.getBytesNum();
			rule.setBytesNum(0);
			counter = " -c " + packets + " " + bytes;
		}

		runIPTablesCommand("-t " + table.getName() + " -A " + rule.getChainName() + " " + rule.getCommand() + counter);
	}

	/**
	 * Delete rule.
	 * 
	 * @param rule
	 *            the rule
	 * @param table
	 *            the table
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void deleteRule(Rule rule, TableType table) throws IOException
	{
		if (rule == null)
		{
			throw new NullPointerException();
		}

		if (rule.getChainName().isEmpty())
		{
			throw new IllegalArgumentException("Undefined chain for the passed rule");
		}

		if (table == null)
		{
			table = TableType.FILTER;
		}

		runIPTablesCommand("-t " + table.getName() + " -D " + rule.getChainName() + " " + rule.getCommand());
	}

	/**
	 * Delete rule.
	 * 
	 * @param ruleNum
	 *            the rule num
	 * @param table
	 *            the table
	 * @param chain
	 *            the chain
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void deleteRule(int ruleNum, TableType table, String chain) throws IOException
	{
		if (ruleNum < 1)
		{
			throw new IllegalArgumentException("The rule number cannot be less than 1, " + ruleNum + " given");
		}

		if (chain == null || chain.isEmpty())
		{
			throw new IllegalArgumentException("Invalid chain name");
		}

		if (table == null)
		{
			table = TableType.FILTER;
		}
		runIPTablesCommand("-t " + table.getName() + " -D " + chain + " " + ruleNum);
	}

	/**
	 * Read error.
	 * 
	 * @param errorStream
	 *            the error stream
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private static void readError(InputStream errorStream) throws IOException
	{
		BufferedReader b = new BufferedReader(new InputStreamReader(errorStream));
		String error = "";
		for (String line = b.readLine(); line != null; line = b.readLine())
		{
			error += line;
		}
		if (error.length() > 0)
		{
			throw new IOException(error);
		}
	}

	/**
	 * Route ports.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param type
	 *            the type
	 * @param dport
	 *            the dport
	 * @param toPort
	 *            the to port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void routePorts(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, int toPort) throws IOException
	{
		if (!containsRoute(protocol, type, chain, networkDevice, dport, toPort))
		{
			Rule rule = RuleFactory.forwardPortRule(protocol, chain, networkDevice, dport, toPort);
			IPTableService.appendRule(rule, type);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.RoutingTableService#routePorts(org.storm.api.Protocol
	 * , org.storm.api.TableType, int, int)
	 */
	@Override
	public void routePorts(Protocol protocol, TableType type, ChainType chain, int dport, int toPort) throws IOException
	{
		routePorts(protocol, type, chain, "eth0", dport, toPort);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.RoutingTableService#routePorts(org.storm.api.Protocol
	 * , org.storm.api.TableType, int, int, int)
	 */
	@Override
	public void routePorts(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, String toIp, int toPort) throws IOException
	{
		if (!containsRoute(protocol, type, chain, networkDevice, dport, toPort))
		{
			Rule rule = RuleFactory.forwardPortRule(protocol, chain, networkDevice, dport, toIp, toPort);
			IPTableService.appendRule(rule, type);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.RoutingTableService#containsRoute(org.storm.api
	 * .Protocol, org.storm.api.TableType, org.storm.api.ChainType, int, int,
	 * int)
	 */
	@Override
	public boolean containsRoute(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, int toPort) throws IOException
	{
		Rule rule = RuleFactory.forwardPortRule(protocol, chain, networkDevice, dport, toPort);
		RuleSet rs = IPTableService.getCurrentRules();
		TableChain tableChain = rs.getTable(TableType.NAT);
		Chain targetChain = tableChain.getChain(chain);
		if (targetChain == null)
		{
			return false;
		}
		return targetChain.contains(rule);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.RoutingTableService#DeleteRoute(org.storm.api.
	 * Protocol, org.storm.api.TableType, org.storm.api.ChainType, int, int,
	 * int)
	 */
	@Override
	public boolean deleteRoute(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, int toPort) throws IOException
	{
		if (containsRoute(protocol, type, chain, networkDevice, dport, toPort))
		{
			Rule rule = RuleFactory.forwardPortRule(protocol, chain, networkDevice, dport, toPort);
			IPTableService.deleteRule(rule, type);
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteRoute(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, String destHost, int toPort) throws IOException
	{
		if (containsRoute(protocol, type, chain, networkDevice, dport,  toPort))
		{
			Rule rule = RuleFactory.forwardPortRule(protocol, chain, networkDevice, dport, destHost, toPort);
			IPTableService.deleteRule(rule, type);
			return true;
		}
		return false;
	}

	@Override
	public void setPermissions(String perm, String file)
	{
		try
		{
			runCommand("chmod " + perm + " " + file);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		
	}
}
