/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core.replicate.command;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.felix.service.command.Descriptor;
import org.storm.api.services.ReplicationService;
import org.storm.kernel.core.replicate.LocalReplicator;

import aQute.bnd.annotation.component.Component;

/**
 * The Class LocalReplicatorCommand.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component(properties = { "osgi.command.scope=storm", "osgi.command.function=replicate" }, provide = Object.class)
public class LocalReplicatorCommand
{

	private static final String FILE_PROTOCOL = "file://";

	/**
	 * Replicate.
	 * 
	 * @param directory
	 *            the directory
	 */
	@Descriptor("Create a new storm cloud agent in the specified cloud provider using the specified credentials")
	public void replicate(String directory)
	{

		ReplicationService replicator = new LocalReplicator();
		try
		{
			replicator.replicate(new URL(FILE_PROTOCOL + directory));
		}
		catch (MalformedURLException e)
		{
			throw new RuntimeException(e);
		}
	}
}
