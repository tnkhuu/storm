/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Storm;
import org.storm.api.kernel.Actor;
import org.storm.nexus.api.Role;
import org.storm.nexus.api.Trait;
import org.storm.tools.http.Download;

/**
 * A specific grouping of function that a {@link Actor} 
 * has.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TraitImpl implements Trait
{
	
	private static final Logger s_log = LoggerFactory.getLogger(TraitImpl.class);
	private String[] traits;
	private Role role;

	/**
	 * Instantiates a new trait impl.
	 * 
	 * @param traits
	 *            the traits
	 * @param role
	 *            the role
	 */
	public TraitImpl(String[] traits, Role role)
	{
		this.traits = traits;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.kernel.Trait#getTraits()
	 */
	@Override
	public String[] getTraits()
	{
		return traits;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.kernel.Trait#inheritTraits()
	 */
	@Override
	public boolean inheritTraits()
	{
		String dir = System.getProperty(Storm.STORM_HOME);
		Role r = role;
		String[] traits = r.getTraits();
		boolean success = true;
		for (String trait : traits)
		{
			try
			{
				File f = new File(dir + "/drop/" + getTraitName(trait));
				if (!f.exists())
				{
					Download.file(new URL("http://artifacts.stormclowd.com:8080/artifacts/" + getTraitName(trait)), f, 1);
				}
			}
			catch (MalformedURLException e)
			{
				success = false;
				s_log.error(e.getMessage(), e);
			}
		}
		return success;
	}

	/**
	 * Gets the trait name.
	 * 
	 * @param trait
	 *            the trait
	 * @return the trait name
	 */
	private String getTraitName(String trait)
	{
		String version = System.getProperty(Storm.VERSION);
		return trait + "-" + version + ".jar";
	}

}
