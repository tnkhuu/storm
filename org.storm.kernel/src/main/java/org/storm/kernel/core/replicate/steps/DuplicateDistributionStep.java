/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core.replicate.steps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.compress.archivers.ArchiveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Storm;
import org.storm.api.services.vm.build.BuildContext;
import org.storm.api.services.vm.build.Goal;
import org.storm.api.services.vm.build.Step;
import org.storm.tools.compression.Extract;

/**
 * The Class DuplicateDistributionStep.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DuplicateDistributionStep implements Step
{

	/** The Constant s_log. */
	private static final Logger s_log = LoggerFactory.getLogger(DuplicateDistributionStep.class);

	/** The Constant STORM_DIST_TAR. */
	private static final String STORM_DIST = "storm-cloud-";

	/** The Constant STORM_FOLDER. */
	private static final String STORM_DIST_MIN = "storm-cloud-min-";

	/** tar extension. */
	private static final String TAR = ".tar.gz";

	/** The root dir. */
	private String rootDir;

	/** The root home. */
	private String rootHome;

	/** The kernel version. */
	private String kernelVersion;

	/**
	 * Instantiates a new duplicate distribution step.
	 * 
	 * @param location
	 *            the location
	 * @param kernelVersion
	 *            the kernel version
	 */
	public DuplicateDistributionStep(URL location, String kernelVersion)
	{
		this.rootDir = location.getPath();
		this.rootHome = rootDir + getDistFolder();
		this.kernelVersion = kernelVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.vm.build.Step#getName()
	 */
	@Override
	public String getName()
	{
		return "Duplicating Distribution";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.vm.build.Step#execute(org.storm.api.services.vm
	 * .build.ConfigContext, org.storm.api.services.vm.build.Result)
	 */
	@Override
	public boolean execute(BuildContext context, Goal<?> result)
	{
		boolean success = false;
		String distPath = getDistTar();
		File root = new File(rootDir);
		try
		{
			if (!root.exists())
			{
				root.mkdirs();
			}
			// "/home/tkhuu/dev/src/java/storm/org.storm.deploy/target/storm-cloud-1.0.0.tar.gz"
			Extract.targz(new File(distPath), root);
			run(new String[] { "bash", "-c", "./start.sh" }, new File(rootHome + "/bin"));
			success = true;
		}
		catch (FileNotFoundException e)
		{
			s_log.error(e.getMessage(), e);
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
		catch (ArchiveException e)
		{
			s_log.error(e.getMessage(), e);
		}

		return success;
	}

	/**
	 * Gets the output.
	 * 
	 * @param in
	 *            the in
	 * @return the output
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private String getOutput(InputStream in) throws IOException
	{
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader bi = new BufferedReader(reader);
		StringBuilder builder = new StringBuilder();
		for (String s = bi.readLine(); s != null; s = bi.readLine())
		{
			builder.append(s);
		}
		return builder.toString();

	}

	/**
	 * Run.
	 * 
	 * @param commands
	 *            the commands
	 * @param workingDir
	 *            the working dir
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private String run(String[] commands, File workingDir) throws IOException
	{
		Process process = Runtime.getRuntime().exec(commands, null, workingDir);
		String result = getOutput(process.getInputStream());
		return result;
	}

	/**
	 * Gets the dist tar.
	 * 
	 * @return the dist tar
	 */
	public String getDistTar()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(System.getProperty(Storm.STORM_HOME));
		builder.append(File.separator);
		builder.append("dist").append(File.separator).append(STORM_DIST).append(kernelVersion).append(TAR);

		return builder.toString();
	}

	/**
	 * Gets the dist tar min.
	 * 
	 * @return the dist tar min
	 */
	public String getDistTarMin()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(System.getProperty(Storm.STORM_HOME));
		builder.append(File.separator);
		builder.append("dist").append(File.separator).append(STORM_DIST_MIN).append(kernelVersion).append(TAR);

		return builder.toString();
	}

	/**
	 * Gets the dist folder.
	 * 
	 * @return the dist folder
	 */
	public String getDistFolder()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(File.separator);
		builder.append(STORM_DIST).append(kernelVersion);

		return builder.toString();
	}

	/**
	 * Gets the dist min folder.
	 * 
	 * @return the dist min folder
	 */
	public String getDistMinFolder()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(File.separator);
		builder.append(STORM_DIST_MIN).append(kernelVersion);

		return builder.toString();
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws MalformedURLException
	 *             the malformed url exception
	 */
	public static void main(String[] args) throws MalformedURLException
	{
		new DuplicateDistributionStep(new URL("/home/tkhuu/dev/src/java/storm/org.storm.deploy/target/"), "1.0.0").execute(null, null);
	}

}
