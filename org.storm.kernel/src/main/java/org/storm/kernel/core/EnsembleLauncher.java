package org.storm.kernel.core;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Kernel;
import org.storm.api.Storm;
import org.storm.api.services.ComputeService;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.kernel.core.ssh.RemoteSSH;
import org.storm.nexus.api.Role;

public class EnsembleLauncher implements Runnable
{
	private static final Logger s_log = LoggerFactory.getLogger(EnsembleLauncher.class);
	private Kernel kernel;
	private ComputeService computeService;
	public EnsembleLauncher(Kernel kernel)
	{
		this.kernel = kernel;
	}

	@Override
	public void run()
	{
		try
		{
			
			ServiceReference<ComputeService> computeServiceReference = kernel.getBundleContext().getServiceReference(ComputeService.class);
			computeService = kernel.getBundleContext().getService(computeServiceReference);
			VirtualMachine[] vms =	computeService.createInstance(Role.ENSEMBLE, 3, getUserData(), HypervisorDriver.LXC, HypervisorType.DEDICATED);

			for(int i = 0; i < vms.length; i++) {
				RemoteSSH.execute(vms[i], "sudo echo " + i +" > /var/lib/zookeeper/myid");
				RemoteSSH.execute(vms[i], "sudo sed -i s/#server.1=zookeeper1:2888:3888/server.1=" + vms[0].getFirstIpAddress() + ":2888:3888/g /etc/zookeeper/conf/zoo.cfg");
				RemoteSSH.execute(vms[i], "sudo sed -i s/#server.2=zookeeper2:2888:3888/server.2=" + vms[1].getFirstIpAddress() + ":2888:3888/g /etc/zookeeper/conf/zoo.cfg");
				RemoteSSH.execute(vms[i], "sudo sed -i s/#server.3=zookeeper3:2888:3888/server.3=" + vms[2].getFirstIpAddress() + ":2888:3888/g /etc/zookeeper/conf/zoo.cfg");
				RemoteSSH.execute(vms[i], "sudo /usr/share/zookeeper/bin/zkServer.sh start");
			}
			
		}
		catch(Exception e) {
			s_log.error(e.getMessage(), e);
		}
		

	}


	private byte[] getUserData() throws IOException
	{
		File script = new File(System.getProperty(Storm.STORM_HOME) + "/scripts/setup/setup-ensemble.sh" );
		return FileUtils.readFileToByteArray(script);
	}
}
