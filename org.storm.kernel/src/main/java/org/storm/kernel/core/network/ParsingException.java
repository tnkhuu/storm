/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.kernel.core.network;

/**
 * The Class ParsingException.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ParsingException extends Exception
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The line. */
	private int line;

	/** The parsing message. */
	private String parsingMessage;

	/**
	 * Create an exception using only the line number where the parsing error
	 * occurs.
	 * 
	 * @param line
	 *            the line
	 */
	ParsingException(int line)
	{
		super("Parsing error at line " + line);
	}

	/**
	 * Create an exception using only a message to describe the error.
	 * 
	 * @param message
	 *            the message
	 */
	ParsingException(String message)
	{
		super("Parsing error: " + message);
		parsingMessage = message;
	}

	/**
	 * Create an exception using the line number where the parsing error occurs
	 * and an error description message.
	 * 
	 * @param line
	 *            the line
	 * @param message
	 *            the message
	 */
	ParsingException(int line, String message)
	{
		super("Parsing error at line " + line + ": " + message);
		this.line = line;
		parsingMessage = message;
	}

	/**
	 * Get the line where the parsing error occurs.
	 * 
	 * @return the line
	 */
	public int getLine()
	{
		return line;
	}

	/**
	 * Get the parsing error message.
	 * 
	 * @return the parsing message
	 */
	public String getParsingMessage()
	{
		return parsingMessage;
	}
}