package org.storm.kernel.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.felix.bundlerepository.RepositoryService;
import org.apache.felix.bundlerepository.impl.ObrCommandImpl;
import org.apache.felix.bundlerepository.impl.ObrURLStreamHandlerService;
import org.apache.felix.bundlerepository.impl.PullParser;
import org.apache.felix.bundlerepository.impl.RepositoryAdminImpl;
import org.apache.felix.bundlerepository.impl.RepositoryParser;
import org.apache.felix.bundlerepository.impl.wrapper.Wrapper;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.fileinstall.internal.DirectoryWatcher;
import org.apache.felix.fileinstall.internal.FileInstall;
import org.apache.felix.shell.Command;
import org.fusesource.hawtdispatch.Dispatcher;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.url.URLConstants;
import org.osgi.service.url.URLStreamHandlerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Constants;
import org.storm.api.Kernel;
import org.storm.api.Storm;
import org.storm.api.services.BlobStoreService;
import org.storm.api.services.ComputeService;
import org.storm.api.services.MetaDataService;
import org.storm.api.services.ReplicationService;
import org.storm.api.services.RoutingTableService;
import org.storm.api.services.SSHKeyPairService;
import org.storm.kernel.config.Configuration;
import org.storm.kernel.core.network.IPTableService;
import org.storm.kernel.core.replicate.LocalReplicator;
import org.storm.kernel.core.ssh.SSHKeyManager;
import org.storm.kernel.log.LoggingAdapter;
import org.storm.nexus.api.Role;
import org.storm.tools.system.SystemUtils;
@SuppressWarnings("unused")
public class ComponentActivator extends DependencyActivatorBase
{
		private static Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);
		private static final String FILE_PREFIX = "file:/";
		private static final String URL_SEP = "/";
		private static final String EMPTY_STR = "";
		private RepositoryService repositoryService = null;
		private DependencyManager manager;
		private BundleContext context = null;
		private FileInstall installer;
		private RoutingTableService routingTableService;
		private Kernel kernel;
		private EventAdmin eventService;
		private Dispatcher dispatcher;
		private volatile boolean bootSuccess = false;
		private volatile boolean bootStarted = false;
		private final Object mutex = new Object();

		/**
		 * Initialize the kernel boot sequence.
		 * 
		 * @return the activator
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		private ComponentActivator boot() throws IOException
		{
            URL entry = context.getBundle().getEntry(Configuration.KERNEL_PROPERTIES);
			Properties props = new Properties();
			props.load(entry.openStream());
			String platformVersion = props.getProperty(Storm.VERSION);
			System.setProperty(Storm.VERSION, platformVersion);
			System.setProperty(Constants.IS_LINUX, Boolean.valueOf(SystemUtils.IS_OS_LINUX).toString());
			System.setProperty(Constants.IS_WINDOWS, Boolean.valueOf(SystemUtils.IS_OS_WINDOWS).toString());
			System.setProperty(Constants.IS_OSX, Boolean.valueOf(SystemUtils.IS_OS_MAC).toString());
            s_log.info("Storm Home : {}", System.getProperty(Storm.STORM_HOME));
            s_log.info("Found kernel configuration at : " + entry);
			return this;
		}

		

		/**
		 * Initializes and registers the Kernel.
		 *
		 * @return the activator
		 */
		private ComponentActivator kernel()
		{
			manager.add(createComponent().setInterface(Kernel.class.getName(), null)
					.setImplementation(KernelImpl.class)
						.add(createServiceDependency()
								.setService(ReplicationService.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(FileInstall.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(EventAdmin.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(RoutingTableService.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(RepositoryService.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(MetaDataService.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(ComputeService.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(BlobStoreService.class)
								.setRequired(true))
						.add(createServiceDependency()
								.setService(SSHKeyPairService.class)
								.setRequired(true)));

			return this;
		}

		/**
		 * Registers the system routing service.
		 * 
		 * @return this ComponentActivator
		 */
		private ComponentActivator routingService()
		{
			manager.add(createComponent().setInterface(RoutingTableService.class.getName(), null)
					 .setImplementation(new IPTableService()));
			return this;
		}

		/**
		 * True if the system booted ok.
		 * 
		 * @return true, if successful
		 */
		private boolean bootSuccess()
		{
			return bootSuccess;
		}

		/**
		 * Register kernel administrative/provisioning service.
		 *
		 * @return the activator
		 * @throws InstantiationException
		 *             the instantiation exception
		 * @throws IllegalAccessException
		 *             the illegal access exception
		 * @throws ClassNotFoundException
		 *             the class not found exception
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private ComponentActivator adminServices() throws InstantiationException, IllegalAccessException, ClassNotFoundException
		{
			RepositoryParser repoParser;
			String parser = context.getProperty(Configuration.OBR_PARSER_CLASS) == null ? System.getProperty(Configuration.OBR_PARSER_CLASS) : context
					.getProperty(Configuration.OBR_PARSER_CLASS);
			if (parser == null)
			{
				repoParser = new PullParser();
			}
			else
			{
				repoParser = loadInstance(parser);
			}
			repositoryService = new RepositoryAdminImpl(context, new LoggingAdapter(context), repoParser);
			Hashtable dict = new Hashtable();
			dict.put(URLConstants.URL_HANDLER_PROTOCOL, Configuration.OBR_PROTOCOL);
			manager.add(createComponent().setInterface(RepositoryService.class.getName(), dict)
					 .setImplementation(repositoryService));

			manager.add(createComponent().setInterface(org.osgi.service.obr.RepositoryAdmin.class.getName(), null)
					 .setImplementation(Wrapper.wrap(repositoryService)));
			
			
			manager.add(createComponent().setInterface(Command.class.getName(), null)
					 .setImplementation(new ObrCommandImpl(context, repositoryService)));
			
			manager.add(createComponent().setInterface(URLStreamHandlerService.class.getName(),dict)
					 .setImplementation(new ObrURLStreamHandlerService(context, repositoryService)));
			
			return this;
		}

		/**
		 * Registers the local system replicator.
		 * 
		 * @return the activator
		 */
		private ComponentActivator systemReplicator()
		{
			manager.add(createComponent().setInterface(ReplicationService.class.getName(), null)
					 .setImplementation(new LocalReplicator()));
			return this;
		}

		/**
		 * Register the kernel bundle install service.
		 *
		 * @return the activator
		 */
		private ComponentActivator provisioner()
		{
			String home = getFrameworkHome(context.getBundle());
			System.setProperty(Configuration.STORM_HOME, home);
			Hashtable<String, Object> config = new Hashtable<>();
			config.put(DirectoryWatcher.DIR, home + Configuration.BUNDLE_DROPIN_FOLDER);
			config.put(DirectoryWatcher.POLL, Configuration.BUNDLE_WATCH_POLL_DELAY);
			config.put(DirectoryWatcher.NO_INITIAL_DELAY, Configuration.NO_DELAY);
			config.put(DirectoryWatcher.START_LEVEL, Configuration.DEFAULT_START_LEVEL);
			installer = new FileInstall(config);
			manager.add(createComponent().setInterface(FileInstall.class.getName(), config)
										 .setImplementation(installer));
			try
			{
				installer.start(context);
			}
			catch (Exception e)
			{
				s_log.info("Could not start bundle install services", e);
			}
			bootSuccess = true;
			return this;
		}
		
		private ComponentActivator keyPairService() {
			manager.add(createComponent().setInterface(SSHKeyPairService.class.getName(), null)
					 .setImplementation(new SSHKeyManager()));
			return this;
		}


		/**
		 * Gets the framework home.
		 * 
		 * @param bundle
		 *            the bundle
		 * @return the framework home
		 */
		private String getFrameworkHome(Bundle bundle)
		{
			String location = bundle.getLocation();
			location = location.replace(FILE_PREFIX, EMPTY_STR);
			String[] path = location.split(URL_SEP);
			StringBuilder builder = new StringBuilder();
			builder.append(URL_SEP);
			if (path != null && path.length > 2)
			{
				for (int i = 0; i < path.length - 2; i++)
				{
					builder.append(path[i]);
					if (i < path.length - 3)
					{
						builder.append(URL_SEP);
					}
				}
			}
			return builder.toString();
		}

		/**
		 * Load instance.
		 * 
		 * @param <T>
		 *            the generic type
		 * @param clazz
		 *            the clazz
		 * @return the t
		 * @throws InstantiationException
		 *             the instantiation exception
		 * @throws IllegalAccessException
		 *             the illegal access exception
		 * @throws ClassNotFoundException
		 *             the class not found exception
		 */
		@SuppressWarnings("unchecked")
		private <T> T loadInstance(String clazz) throws InstantiationException, IllegalAccessException, ClassNotFoundException
		{
			T instance = null;
			if (clazz != null && clazz.length() > 0)
			{
				instance = (T) Class.forName(clazz).newInstance();
			}
			return instance;
		}

		private String getRole() throws IOException
		{
			String ip = null;
			File role = new File("/role");
			if (role.exists())
			{
				InputStreamReader reader = new InputStreamReader(new FileInputStream(role));
				try (BufferedReader breader = new BufferedReader(reader))
				{
					ip = breader.readLine();
				}
				finally
				{
                    reader.close();
				}
			}
			return ip;
		}

		@Override
		public void init(BundleContext context, DependencyManager manager) throws Exception
		{
			this.manager = manager;
			this.context = context;
			if (!bootStarted)
			{
				synchronized (mutex)
				{
					bootStarted = true;
					try
					{
						String role = getRole();
						if (role != null)
						{
							System.setProperty(Storm.STORM_ROLE, Role.valueOf(role).name());
						}
						s_log.info("This container is configured as a {}", role);
						boot().adminServices().systemReplicator().keyPairService().routingService().provisioner().kernel();
					}
					catch (Throwable t)
					{
						s_log.error(t.getMessage(), t);
					}
					finally
					{
						if (!bootSuccess())
						{
							s_log.error("A Fatal Error occurred when initializing the system. Shutting down.");
							System.exit(1);
						}
					}
				}
			}
		}

		@Override
		public void destroy(BundleContext context, DependencyManager manager) throws Exception
		{
			if (repositoryService != null)
			{
				repositoryService.dispose();
			}
			if (installer != null)
			{
				try
				{
					installer.stop(context);
				}
				catch (Exception e)
				{
					s_log.error(e.getMessage(), e);
				}
			}
			
		}

	}