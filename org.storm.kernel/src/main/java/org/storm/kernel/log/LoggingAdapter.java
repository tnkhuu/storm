/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.log;

import org.apache.felix.utils.log.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.slf4j.LoggerFactory;

/**
 * A logging adapter for the felix logger.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class LoggingAdapter extends Logger
{

	private static final org.slf4j.Logger s_log = LoggerFactory.getLogger(LoggingAdapter.class);

	public LoggingAdapter(BundleContext context)
	{
		super(context);
	}

	@Override
	public void log(int level, String message)
	{
		log(level, message, null);
	}

	/**
	 * @see LogService#log(int, String, Throwable)
	 */
	@Override
	public void log(int level, String message, Throwable exception)
	{
		switch (level)
		{
		case Logger.LOG_INFO:
		{
			s_log.info(message, exception);
			break;
		}
		case Logger.LOG_ERROR:
		{
			s_log.error(message, exception);
			break;
		}
		case Logger.LOG_WARNING:
		{
			s_log.warn(message, exception);
			break;
		}
		case Logger.LOG_DEBUG:
		{
			s_log.debug(message, exception);
			break;
		}
		default:
		{
			s_log.info(message, exception);
		}
		}
	}

}
