package org.storm.kernel.core;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.storm.api.Kernel;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.cloud.Region;
import org.storm.api.security.Credentials;
import org.storm.api.services.ComputeService;
import org.storm.cloud.compute.ComputeServiceImpl;

@Ignore
public class EnsembleLauncherTest
{
	
	@SuppressWarnings("unused")
	private ComputeService computeService;
	@Mock private Kernel kernel;
	@Mock private BundleContext context;
	
	@Before
	public void beforeTest() {
		ComputeService computeService = new ComputeServiceImpl(new Credentials("", "", CloudProvider.AMAZON), CloudProvider.AMAZON, Region.AP_SOUTHEAST_2, context);
		when(kernel.getService(ComputeService.class)).thenReturn(computeService);
	}
	
	
	@Test
	public void testLaunchAmazonZookeeperEnsemble() {
		
	}
}
