package org.storm.kernel.core;

import org.storm.api.services.LogicalVolumeService;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.lvm.LogicalVolumnServiceImpl;
import org.storm.lxc.LxcProvider;
import org.storm.nexus.api.Role;

public class LxcPerfTests
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		LogicalVolumeService lvs = new LogicalVolumnServiceImpl();
		LxcProvider lxc = new LxcProvider(null);
		long start = System.currentTimeMillis();
		//lvs.createLxcBaseLogicalVolumn("storm-lxc", Role.DEFAULT.name(), "8.8.8.8", 1500);
	//	long end = System.currentTimeMillis() - start;
//		System.out.println("base: " + end);
	//	start = System.currentTimeMillis();
		String snapshot = lvs.createSnapshotVolumn("copy1", Role.DNS.name(), 1500, "/dev/lxc/storm-lxc", "lxc");
		
		lxc.createContainer(HypervisorDriver.LXC, Role.DNS.name(), "copy1", "8.8.8.8", snapshot);
	
		String ip = lxc.startContainer("copy1");
	    long end = System.currentTimeMillis() - start;
		
		System.out.println("ss: " + end);
		System.out.println(ip);
	}

}
