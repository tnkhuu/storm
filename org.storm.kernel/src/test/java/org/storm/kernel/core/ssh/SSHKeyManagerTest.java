package org.storm.kernel.core.ssh;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.storm.api.security.Encryption;
import org.storm.api.security.SSHKeyPair;
import org.storm.api.services.SSHKeyPairService;
import org.storm.tools.system.SystemUtils;

import com.jcraft.jsch.KeyPair;

@Ignore
public class SSHKeyManagerTest
{

	private SSHKeyPairService manager;
	private static final File TMPDIR = SystemUtils.getJavaIoTmpDir();

	@Before
	public void before()
	{
		manager = new SSHKeyManager();
	}

	@Test
	public void testGenerateKeyPair() throws IOException
	{
		SSHKeyPair kp = manager.createKeyPair(Encryption.RSA);
		Assert.assertEquals(KeyPair.RSA, kp.getKeyType());
		kp.writePrivateKey(new FileOutputStream(new File(TMPDIR.getAbsolutePath() + "/testGenerateKeyPair")));
		kp.setPassphrase("");
		kp.writePublicKey(new FileOutputStream(new File(TMPDIR.getAbsolutePath() + "/testGenerateKeyPair.pub")), "");

		
		
		manager.saveKeyPair("test", kp);
		
		
		
		File pubKey = new File(TMPDIR.getAbsolutePath() + "/testGenerateKeyPair.pub");

		Assert.assertTrue(pubKey.exists());
		System.out.println(System.getProperty("user.name"));
		System.out.println(InetAddress.getLocalHost().getHostName());
	}
}
