/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.kernel.core.net;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.Ignore;
import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;
import org.storm.api.network.TableType;
import org.storm.api.services.RoutingTableService;
import org.storm.kernel.core.network.Chain;
import org.storm.kernel.core.network.IPTableService;
import org.storm.kernel.core.network.Rule;
import org.storm.kernel.core.network.RuleSet;
import org.storm.kernel.core.network.TableChain;

/**
 * The Class IPTablesTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Ignore
public class IPTablesTest
{

	/**
	 * Test build delete udp rules.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testBuildDeleteUDPRules() throws IOException
	{

		Rule r = Rule.buildFromCommand("-A PREROUTING -i eth0 -p udp -m udp --dport 53 -j REDIRECT --to-ports 8053");

		IPTableService.appendRule(r, TableType.NAT);

		RuleSet rs = IPTableService.getCurrentRules();
		TableChain chain = rs.getTable(TableType.NAT);

		Chain c = chain.getChain(ChainType.PREROUTING);
		Assert.assertTrue(c.contains(r));

		IPTableService.deleteRule(r, TableType.NAT);
		rs = IPTableService.getCurrentRules();
		chain = rs.getTable(TableType.NAT);
		c = chain.getChain(ChainType.PREROUTING);
		if (c != null)
		{
		//	Assert.assertFalse(c.contains(r));
		}
	}

	/**
	 * Test build delete tcp rules.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testBuildDeleteTCPRules() throws IOException
	{

		Rule r = Rule.buildFromCommand("-A PREROUTING -i eth0 -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 8080");

		IPTableService.appendRule(r, TableType.NAT);

		RuleSet rs = IPTableService.getCurrentRules();
		TableChain chain = rs.getTable(TableType.NAT);

		Chain c = chain.getChain(ChainType.PREROUTING);
		Assert.assertTrue(c.contains(r));

		IPTableService.deleteRule(r, TableType.NAT);
		rs = IPTableService.getCurrentRules();
		chain = rs.getTable(TableType.NAT);
		c = chain.getChain(ChainType.PREROUTING);
		if (c != null)
		{
			Assert.assertFalse(c.contains(r));
		}

	}
	
	/**
	 * Test service.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testRoutingService() throws IOException
	{

		RoutingTableService service = new IPTableService();
		service.routePorts(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, 80, 8080);
		Assert.assertTrue(service.containsRoute(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, "eth0", 80, 8080));
		service.deleteRoute(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, "eth0", 80, 8080);
		Assert.assertFalse(service.containsRoute(Protocol.TCP, TableType.NAT, ChainType.PREROUTING, "eth0", 80, 8080));
		

	}
}
