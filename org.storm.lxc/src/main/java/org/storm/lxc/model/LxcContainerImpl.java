/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.lxc.model;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import org.msgpack.annotation.Message;
import org.storm.api.services.vm.ContainerState;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.LxcContainer;

/**
 * A Lxc Container
 *
 * @author Trung Khuu
 * @since 1.0
 */
@Message
public class LxcContainerImpl implements LxcContainer
{

	private final String hostname;
	private String ipAddress;
	private final String installedPath;
	private final ContainerState state;
	private HypervisorDriver type = HypervisorDriver.LXC;
	private final Properties config;
	
	/** The this container is an ephemeraled container (A container which is only ever booted up once). */
	private final boolean isEphemeraled;
	
	public LxcContainerImpl(String hostname, String state, String installedPath, boolean isEphemeraled, Map<String, String> env) {
		this(hostname,null,state, installedPath, isEphemeraled, env);
		
	}
	
	public LxcContainerImpl(String hostname, String ip, String state, String installedPath, boolean isEphemeraled, Map<String, String> env) {
		this.hostname = hostname;
		this.ipAddress = ip;
		this.installedPath = installedPath;
		this.isEphemeraled = isEphemeraled;
		this.state = ContainerState.valueOf(state);
		this.config = new Properties();
		if(env != null)
		{
			config.putAll(env);
		}
		
	}
	
	/* (non-Javadoc)
	 * @see org.storm.api.services.vm.LxcContainer#getHostName()
	 */
	@Override
	public String getHostName()
	{
		return hostname;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.vm.LxcContainer#getIpAddress()
	 */
	@Override
	public String getIpAddress()
	{
		return ipAddress;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.vm.LxcContainer#getInstalledPath()
	 */
	@Override
	public String getInstalledPath()
	{
		return installedPath;
	}


	/* (non-Javadoc)
	 * @see org.storm.api.services.vm.LxcContainer#getState()
	 */
	@Override
	public ContainerState getState()
	{
		return state;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.vm.LxcContainer#isEphemeral()
	 */
	@Override
	public boolean isEphemeral()
	{
		return isEphemeraled;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.vm.LxcContainer#getConfiguration()
	 */
	@Override
	public Properties getConfiguration()
	{
		return config;
	}
	
	@Override
	public HypervisorDriver getType()
	{
		return type;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.vm.LxcContainer#start()
	 */
	@Override
	public boolean start()
	{
		return false;
	}

	@Override
	public boolean start(File runScript)
	{
		return false;
	}

	@Override
	public boolean stop()
	{
		
		return false;
	}

	@Override
	public boolean destroy()
	{
		return false;
	}

	@Override
	public String toString()
	{
		return "LxcContainerImpl [hostname=" + hostname + ", ipAddress="
				+ ipAddress + ", installedPath=" + installedPath + ", state="
				+ state + ", isEphemeraled=" + isEphemeraled + ", config="
				+ config + "]";
	}
	
	

}
