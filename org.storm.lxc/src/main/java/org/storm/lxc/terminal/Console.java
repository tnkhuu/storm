/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.terminal;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.storm.tools.system.SystemUtils;

/**
 * The Shell Console.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Console
{
	private static final String SUDO = "sudo ";

	public String execute(String command) throws ExecuteException, IOException
	{
		return execute(command, new File("."));
	}

	public String executeWithSudo(String command) throws ExecuteException, IOException
	{
		return execute(SUDO + command, new File("."));
	}

	public String execute(String command, File workingDirectory) throws ExecuteException, IOException
	{
		return execute(command, new HashMap<>(), workingDirectory);
	}

	@SuppressWarnings("rawtypes")
	public String execute(String command, Map environment, File workingDirectory) throws ExecuteException, IOException
	{
		Executor exec = new DefaultExecutor();
		exec.setWorkingDirectory(workingDirectory);
		PumpStreamHandler handler = new PumpStreamHandler();
		InputStream in = new ByteArrayInputStream(new byte[1024]);
		handler.setProcessOutputStream(in);
		CommandLine cl = CommandLine.parse(command);
		int status = exec.execute(cl, environment);
		String result = getOutput(in);
		if (status < 0)
		{
			throw new ExecuteException(result, status);
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	public String execute(String command, String[] args, Map environment, File workingDirectory) throws ExecuteException, IOException
	{
		Executor exec = new DefaultExecutor();
		exec.setWorkingDirectory(workingDirectory);

		PumpStreamHandler handler = new PumpStreamHandler();
		InputStream in = new ByteArrayInputStream(new byte[1024]);
		handler.setProcessOutputStream(in);
		CommandLine cl = new CommandLine(command);
		cl.addArguments(args);
		int status = exec.execute(cl, environment);
		String result = getOutput(in);
		if (status < 0)
		{
			throw new ExecuteException(result, status);
		}
		return result;
	}

	public static String getOutput(InputStream in) throws IOException
	{
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader bi = new BufferedReader(reader);
		StringBuilder builder = new StringBuilder();
		for (String s = bi.readLine(); s != null; s = bi.readLine())
		{
			builder.append(s + "\n");
		}
		return builder.toString();

	}

	public String execute(String[] commands, File workingDir) throws IOException
	{
		Process process = Runtime.getRuntime().exec(commands, null, workingDir);
		String result = Console.getOutput(process.getInputStream());
		return result;
	}

	public static boolean commandExists(String command) throws ExecuteException, IOException
	{
		if (SystemUtils.IS_OS_LINUX)
		{

			Process process = Runtime.getRuntime().exec(new String[] { "bash", "-c", "which " + command + " > /dev/null && echo \"Found\" || echo \"Not Found\"" });
			String result = Console.getOutput(process.getInputStream());

			if (result != null && result.startsWith("Found"))
			{
				return true;
			}
			System.out.println("WARNING: Command " + command + " Not found. Storm will not be able to run properly");
		}

		return false;
	}
}
