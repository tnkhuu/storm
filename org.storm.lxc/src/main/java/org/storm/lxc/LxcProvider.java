/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.exec.ExecuteException;
import org.apache.commons.io.FileUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceStrategy;
import org.apache.felix.scr.annotations.Service;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Storm;
import org.storm.api.services.LogicalVolumeService;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.Container;
import org.storm.api.services.vm.ContainerState;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.lxc.exception.ContainerNotFoundException;
import org.storm.lxc.model.LxcContainerImpl;
import org.storm.lxc.terminal.Console;
import org.storm.tools.system.StormPlatform;

/**
 * A Virtualization Provider that performs the lxc type 2 container level hypervisor virtualization.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component
@Service(
{ VirtualizationProvider.class })
public class LxcProvider implements VirtualizationProvider, LxcConstants
{
	private static final String USER_HOME = "user.home";
	private static final String STORM_WORKING_DIR = ".storm";
	private static final String LXC_BASE = "/var/lib/lxc";
	private static final String TEMPLATES = "/scripts/templates";
	private static final String SYSTEM_BOOT_LXC = "storm-lxc";
	private static final String LXC_CONFIG = "config";
	private static final String LXC_FSTAB = "fstab";
	private static final String SUDO = "sudo";
	private static final String SLASH = "/";
	private static final Logger s_log = LoggerFactory.getLogger(LxcProvider.class);
	private Console terminal = new Console();
	private VelocityEngine velocity;
	private String stormHome;
	@Reference(strategy = ReferenceStrategy.EVENT)
	private LogicalVolumeService lvmService;

	public static enum Command
	{
		CREATE, DESTROY, STOP, START
	}

	/**
	 * Instantiates a new lxc provider.
	 */
	public LxcProvider()
	{

	}

	/**
	 * Instantiates a new lxc provider.
	 * 
	 * @param context
	 *            the context
	 */
	public LxcProvider(final BundleContext context)
	{
		this.stormHome = "/home/tkhuu/dev/src/java/storm/org.storm.deploy";//System.getProperty(Storm.STORM_HOME);
		this.velocity = new VelocityEngine(VelocityConfig.createDefault(stormHome + TEMPLATES));
		String userHome = System.getProperty(USER_HOME);
		File working = new File(userHome + File.separator + STORM_WORKING_DIR);
		if (!working.exists())
		{
			throw new IllegalStateException("Storm image and distribution binaries not found. Has storm been setup correctly ?");
		}
	}

	/**
	 * Gets the hypervisor driver.
	 * 
	 * @return the hypervisor driver
	 */
	@Override
	public HypervisorDriver getHypervisorDriver()
	{
		return HypervisorDriver.LXC;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	@Override
	public HypervisorType getType()
	{
		return HypervisorType.PARAVIRTUALIZED;
	}

	@Activate
	protected void activate()
	{
		this.stormHome = System.getProperty(Storm.STORM_HOME);
		this.velocity = new VelocityEngine(VelocityConfig.createDefault(stormHome + TEMPLATES));
		String userHome = System.getProperty(USER_HOME);
		File working = new File(userHome + File.separator + STORM_WORKING_DIR);
		if (!working.exists())
		{
			throw new IllegalStateException("Storm image and distribution binaries not found. Has storm been setup correctly ?");
		}
		s_log.info("LVMService: " + lvmService);
	}

	/**
	 * Bind lvm service.
	 * 
	 * @param lvmService
	 *            the lvm service
	 */
	protected void bindLvmService(LogicalVolumeService lvmService)
	{
		this.lvmService = lvmService;
	}

	/**
	 * Unbind lvm service.
	 * 
	 * @param lvmService
	 *            the lvm service
	 */
	protected void unbindLvmService(LogicalVolumeService lvmService)
	{
		this.lvmService = lvmService;
	}

	/**
	 * Creates the container.
	 * 
	 * @param driver
	 *            the driver
	 * @param role
	 *            the role
	 * @param domainname
	 *            the domainname
	 * @param dns
	 *            the dns
	 * @param options
	 *            the options
	 * @return true, if successful
	 */
	@Override
	public boolean createContainer(HypervisorDriver driver, String role, String domainname, String dns, String... options)
	{
		boolean success = false;
		try
		{
			String hostname = sanitize(domainname);
			if (options.length > 0)
			{
				success = createLVMBackedContainer(driver, role, hostname, options[0], dns);
			}
			else
			{
				if (!contains(hostname))
				{
					s_log.info("Creating container '{}' ...", hostname);
					String[] commandStr = new String[]
					{ SUDO, "lxc-create", "-n", hostname, "-t", LxcConstants.LXC_TYPE_UBUNTU_STORM, "--", LxcConstants.LXC_TARBALL_OPT, StormPlatform.INSTANCE.getStormImage(),
							LxcConstants.LXC_AUTH_KEY_OPT, StormPlatform.INSTANCE.getSSHPublicKey(), "--role", role, "--dns", dns, ">", "/dev/null" };

					terminal.execute(commandStr, new File("."));
					success = true;
				}
			}

			s_log.info("Container '{}' created. [Login Credentials: ubuntu/ubuntu]", domainname);
		}
		catch (ExecuteException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return success;
	}

	/**
	 * Creates the lvm backed container.
	 * 
	 * @param driver
	 *            the driver
	 * @param role
	 *            the role
	 * @param hostname
	 *            the hostname
	 * @param linkedFS
	 *            the linked fs
	 * @param dns
	 *            the dns
	 * @return true, if successful
	 */
	public boolean createLVMBackedContainer(HypervisorDriver driver, String role, String hostname, String linkedFS, String dns)
	{
		boolean success = false;
		try
		{
			String domainname = sanitize(hostname);
			String containerRoot = LXC_BASE + SLASH + domainname;
			File container = new File(containerRoot);
			if (container.exists())
			{
				s_log.debug("Container {} exists. Ignoring");
			}
			else
			{
				File CWD = new File(".");
				terminal.execute(new String[]
				{ SUDO, "mkdir", "-p", containerRoot + "/rootfs" }, CWD);

				terminal.execute(new String[]
				{ SUDO, "touch", containerRoot + SLASH + LXC_FSTAB }, CWD);

				terminal.execute(new String[]
				{ SUDO, "chmod", "777", containerRoot + SLASH + LXC_FSTAB }, CWD);

				terminal.execute(new String[]
				{ SUDO, "touch", containerRoot + SLASH + LXC_CONFIG }, CWD);

				terminal.execute(new String[]
				{ SUDO, "chmod", "777", containerRoot + SLASH + LXC_CONFIG }, CWD);

				Template config = velocity.getTemplate(LXC_CONFIG);
				Template fstab = velocity.getTemplate(LXC_FSTAB);
				VelocityContext context = new VelocityContext();
				context.put("rootfs", linkedFS);
				context.put("mount", containerRoot + SLASH + LXC_FSTAB);
				context.put("host", domainname);
				context.put("macaddr", StormPlatform.INSTANCE.createRandomMacAddress());
				StringWriter writer = new StringWriter();
				StringWriter fstabWriter = new StringWriter();
				config.merge(context, writer);
				fstab.merge(context, fstabWriter);
				File configOut = new File(containerRoot + SLASH + LXC_CONFIG);
				File fstabOut = new File(containerRoot + SLASH + LXC_FSTAB);
				FileUtils.write(configOut, writer.toString());
				FileUtils.write(fstabOut, fstabWriter.toString());
				File rclocal = new File(LogicalVolumeService.DEFAULT_MNT_DIR + "/" + domainname + "/etc/rc.local");
				if (!rclocal.exists())
				{

					if (!rclocal.getParentFile().exists())
					{
						rclocal.getParentFile().mkdirs();
					}

					rclocal.createNewFile();
				}
				String nameServer = "#!/bin/sh -e\necho \"nameserver " + dns + "\" > /etc/resolv.conf";
				String setHost = "\necho \"127.0.0.1 " + domainname + "\" >> /etc/hosts";
				String setLocalhost = "\necho \"127.0.0.1 localhost" + "\" >> /etc/hosts";
				FileUtils.write(rclocal, nameServer + setHost + setLocalhost);
				success = true;
			}
		}
		catch (ExecuteException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return success;
	}

	/**
	 * Gets the container ip.
	 * 
	 * @param driver
	 *            the driver
	 * @param hostname
	 *            the hostname
	 * @return the container ip
	 */
	@Override
	public String getContainerIp(HypervisorDriver driver, String hostname)
	{
		String ip = LxcConstants.NONE_ASSIGNED;
		try
		{
			ip = getContainerIP(hostname);
		}
		catch (IOException e)
		{
			s_log.info("Error occured: {}", e.getMessage());
		}
		return ip;
	}

	/**
	 * Contains.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public boolean contains(String name)
	{
		boolean exists = false;
		try
		{
			List<Container> containers = listContainers();
			for (Container c : containers)
			{
				if (c.getHostName().equals(name))
				{
					exists = true;
					break;
				}
			}
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
		return exists;
	}

	/**
	 * Destroy container.
	 * 
	 * @param driver
	 *            the driver
	 * @param domainname
	 *            the domainname
	 * @return true, if successful
	 */
	@Override
	public boolean destroyContainer(HypervisorDriver driver, String domainname)
	{
		String hostname = sanitize(domainname);
		boolean success = false;
		try
		{
			if (contains(hostname))
			{

				terminal.execute(new String[]
				{ SUDO, "umount", "/mnt/storm/lxc/" + hostname }, new File("."));
				terminal.executeWithSudo(getCommand(Command.DESTROY, hostname, null));
				success = true;
			}
		}
		catch (ExecuteException e)
		{
			s_log.debug(e.getMessage(), e);
			// throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			s_log.debug(e.getMessage(), e);
			// throw new RuntimeException(e);
		}
		if (!success)
		{
			s_log.info("No existing container found for {}. Continuing. ", hostname);
		}
		return success;
	}

	/**
	 * Stop container.
	 * 
	 * @param driver
	 *            the driver
	 * @param domainname
	 *            the domainname
	 * @return true, if successful
	 */
	@Override
	public boolean stopContainer(HypervisorDriver driver, String domainname)
	{
		String hostname = sanitize(domainname);
		boolean success = false;
		try
		{
			if (contains(hostname))
			{
				terminal.executeWithSudo(getCommand(Command.STOP, hostname, null));
				success = true;
			}
		}
		catch (ExecuteException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		return success;
	}

	/**
	 * Start container.
	 * 
	 * @param domainname
	 *            the domainname
	 * @return the string
	 */
	public String startContainer(String domainname)
	{
		String hostname = sanitize(domainname);
		String ip = null;
		try
		{
			if (contains(hostname))
			{
				terminal.executeWithSudo(getCommand(Command.START, hostname, null));
				ip = getContainerIP(domainname);
			}
			else
			{
				throw new ContainerNotFoundException("The container " + hostname + " could not be found");
			}
		}
		catch (ExecuteException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		return ip;
	}

	/**
	 * Contains.
	 * 
	 * @param driver
	 *            the driver
	 * @param domainname
	 *            the domainname
	 * @return true, if successful
	 */
	@Override
	public boolean contains(HypervisorDriver driver, String domainname)
	{
		String containerName = sanitize(domainname);
		File base = new File(LXC_BASE);
		String[] children = base.list();
		for (String child : children)
		{
			if (containerName.equals(child))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * List containers.
	 * 
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public List<Container> listContainers() throws IOException
	{
		List<Container> containers = new ArrayList<>();
		File base = new File(LXC_BASE);
		String[] children = base.list();
		for (String child : children)
		{
			if (!SYSTEM_BOOT_LXC.equals(child.trim()))
			{
				ContainerState state = getContainerState(child);
				String ip = NONE_ASSIGNED;

				if (state == ContainerState.RUNNING)
				{
					ip = getContainerIP(child);
				}
				Container con = new LxcContainerImpl(child, ip, state.name(), LXC_BASE + File.separator + child, true, null);
				containers.add(con);
			}
		}
		return containers;
	}

	/**
	 * Gets the container state.
	 * 
	 * @param domainname
	 *            the domainname
	 * @return the container state
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public ContainerState getContainerState(String domainname) throws IOException
	{
		String containerName = sanitize(domainname);
		ContainerState state = null;
		String result = terminal.execute(new String[]
		{ SUDO, "lxc-info", "-n", containerName, "-s" }, new File("."));
		String[] split = result.split(COLON);
		String stateStr = split[1].replaceAll(SPACE, NOSPACE);
		if (stateStr != null)
		{
			state = ContainerState.valueOf(stateStr.trim());
		}
		else
		{
			state = ContainerState.UNKNOWN;
		}
		return state;
	}

	/**
	 * Gets the container ip.
	 * 
	 * @param containerName
	 *            the container name
	 * @return the container ip
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String getContainerIP(String containerName) throws IOException
	{
		String res = null;
		terminal.executeWithSudo("lxc-ip " + containerName);
		File f = new File("/tmp/" + containerName + ".ip");
		do
		{
			try
			{
				if (f.exists())
				{
					res = FileUtils.readFileToString(f);
					if(res != null)
					{
						res = res.trim();
					}
				}
			}
			catch (IOException e)
			{
				// ignore
			}
		}
		while (!f.exists());

		terminal.executeWithSudo("rm -rf /tmp/" + containerName + ".ip");
		return res;

	}

	public String getMacAddressByHostName(String hostname) throws FileNotFoundException, IOException
	{
		File f = new File(LXC_BASE + "/" + hostname + "/config");
		Properties props = new Properties();
		props.load(new FileInputStream(f));
		return props.getProperty("lxc.network.hwaddr").trim();
	}

	/**
	 * Gets the command.
	 * 
	 * @param type
	 *            the type
	 * @param name
	 *            the name
	 * @param containerType
	 *            the container type
	 * @return the command
	 */
	protected String getCommand(Command type, String name, String containerType)
	{
		StringBuilder builder = new StringBuilder();
		switch (type)
		{
		case CREATE:
		{
			builder.append(LXC_CREATE);
			builder.append(SPACE);
			builder.append(LXC_NAME);
			builder.append(SPACE);
			builder.append(name);
			builder.append(SPACE);
			builder.append(LXC_CREATE_TYPE);
			builder.append(SPACE);
			builder.append(containerType);
			builder.append(SPACE);
			builder.append(LXC_ADDITIONAL_OPTS);
			builder.append(SPACE);
			builder.append(LXC_TARBALL_OPT);
			builder.append(SPACE);
			builder.append(StormPlatform.INSTANCE.getStormImage());
			builder.append(SPACE);
			builder.append(LXC_AUTH_KEY_OPT);
			builder.append(SPACE);
			builder.append(StormPlatform.INSTANCE.getSSHPublicKey());
			builder.append(NO_OUTPUT);
			break;
		}
		case DESTROY:
		{
			builder.append(LXC_DESTROY);
			builder.append(SPACE);
			builder.append(LXC_FORCE);
			builder.append(SPACE);
			builder.append(LXC_NAME);
			builder.append(SPACE);
			builder.append(name);
			break;
		}
		case STOP:
		{
			builder.append(LXC_STOP);
			builder.append(SPACE);
			builder.append(LXC_NAME);
			builder.append(SPACE);
			builder.append(name);
			break;
		}
		case START:
		{
			builder.append(LXC_START);
			builder.append(SPACE);
			builder.append(LXC_NAME);
			builder.append(SPACE);
			builder.append(name);
			builder.append(SPACE);
			builder.append(LXC_DAEMONIZE);
			break;
		}
		}
		return builder.toString();
	}

	/**
	 * Sanitize.
	 * 
	 * @param domain
	 *            the domain
	 * @return the string
	 */
	private String sanitize(String domain)
	{
		return domain;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws IOException
	{
		System.out.println(new LxcProvider().getContainerIP("DNS"));
	}

}
