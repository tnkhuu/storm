/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.lxc.io;

import java.io.IOException;

import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.template.Template;
import org.msgpack.unpacker.Unpacker;
import org.storm.api.services.vm.ContainerState;
import org.storm.api.services.vm.LxcContainer;
import org.storm.lxc.model.LxcContainerImpl;


/**
 * {@link LxcContainer} serializer.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class LxcContainerTemplate implements Template<LxcContainer>
{

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#write(org.msgpack.packer.Packer, java.lang.Object)
	 */
	@Override
	public void write(Packer pk, LxcContainer v) throws IOException
	{
		
		pk.write(v.getHostName());
		pk.write(v.getInstalledPath());
		pk.write(v.getIpAddress());
		pk.write(v.isEphemeral());
		pk.write(v.getState().name());
		
	}

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#write(org.msgpack.packer.Packer, java.lang.Object, boolean)
	 */
	@Override
	public void write(Packer pk, LxcContainer v, boolean required) throws IOException
	{
		if (v == null) {
            if (required) {
                throw new MessageTypeException("Attempted to write null");
            }
            pk.writeNil();
            return;
        }
		write(pk,v);
		
	}

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#read(org.msgpack.unpacker.Unpacker, java.lang.Object)
	 */
	@Override
	public LxcContainer read(Unpacker u, LxcContainer to) throws IOException
	{
		String hostName = u.readString();
		String path = u.readString();
		String ip = u.readString();
		boolean isEmphemeraled = u.readBoolean();
		LxcContainer container = new LxcContainerImpl(hostName, ip, ContainerState.STOPPED.name(), path, isEmphemeraled, null);
		return container;
	}

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#read(org.msgpack.unpacker.Unpacker, java.lang.Object, boolean)
	 */
	@Override
	public LxcContainer read(Unpacker u, LxcContainer to, boolean required) throws IOException
	{
		if (!required && u.trySkipNil()) {
            return null;
        }
		return read(u,to);
	}

}
