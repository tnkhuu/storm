/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.lxc;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.msgpack.template.Template;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.DistributedService;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.LxcContainer;
import org.storm.lxc.io.LxcContainerTemplate;
import org.storm.lxc.terminal.Console;
import org.storm.nexus.api.Services;
import org.storm.tools.system.SystemUtils;

/**
 * The LXC Bundle Activator.
 *
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator extends DependencyActivatorBase implements BundleActivator
{

	private static final Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);
	private LxcProvider provider;
	

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception
	{
		if (SystemUtils.IS_OS_LINUX && Console.commandExists("sudo") && Console.commandExists("lxc-create"))
		{
			provider = new LxcProvider(context);
			Dictionary<String, Object> props =	new Hashtable<String,Object>();
			props.put(Services.TEMPLATE_IMPL, LxcContainer.class.getName());
			context.registerService(new String[]{Template.class.getName()},  new LxcContainerTemplate(), props);
			
			manager.add(createComponent().setInterface(new String[]{DistributedService.class.getName(), VirtualizationProvider.class.getName()}, null)
										 .setImplementation(provider));
			s_log.info("LXC Virtualization Provider registered.");
		}
		
	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception
	{
		
		
	}
	

}
