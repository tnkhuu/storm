/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc;

import java.util.regex.Pattern;

/**
 * LXC Constants.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface LxcConstants
{
	Pattern IPV4 = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
	Pattern MACADDRESS = Pattern.compile("(([0-9a-fA-F]){1,2}[-:]){5}([0-9a-fA-F]){1,2}");
	Pattern LEASE = Pattern.compile("\\d*");
	
	String LXC_HOME = "/var/lib/lxc";
	
	String LXC_CREATE = "lxc-create";
	
	String LXC_CREATE_TYPE = "-t";
	
	String LXC_FORCE =  "-f";
	
	String LXC_NAME =  "-n";
	
	String LXC_IP = "&& lxc-ip";
	
	String LXC_DESTROY = "lxc-destroy";
	
	String LXC_STOP = "lxc-stop";
	
	String LXC_SUSPEND = "lxc-suspend";
	
	String LXC_START = "lxc-start";
	
	String LXC_TYPE_UBUNTU = "ubuntu-cloud";
	
	String LXC_TYPE_UBUNTU_STORM = "ubuntu-storm";
	
	String LXC_ADDITIONAL_OPTS = "--";
	
	String LXC_TARBALL_OPT = "-T";
	
	String LXC_DAEMONIZE = "-d";
	
	String LXC_IPADDRESS = "--ipaddress";
	
	String LXC_AUTH_KEY_OPT = "--auth-key";
	
	String LXC_AUTH_KEY_DEFAULT = "~/.ssh/id_rsa.pub";
	
	String RELEASE_UBUNTU_PRECISE = "precise";
	
	String NO_OUTPUT = " > /dev/null";
	
	String COLON = ":";
	
	String SPACE = " ";
	
	String NOSPACE = "";
	
	String NONE_ASSIGNED = "offline";
}
