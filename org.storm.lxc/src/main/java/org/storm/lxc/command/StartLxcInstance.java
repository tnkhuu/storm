package org.storm.lxc.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.VirtualizationProvider;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

@Component(properties = { "osgi.command.scope=felix", "osgi.command.function=startlx" }, provide = Object.class)
public class StartLxcInstance
{
	private static Logger s_log = LoggerFactory.getLogger(StartLxcInstance.class);;

	private VirtualizationProvider provider;

	@Reference
	public void setProvider(VirtualizationProvider provider)
	{
		this.provider = provider;
	}

	public void startlx(String name)
	{
		
		String ip = provider.startContainer(name);
		if (ip == null)
		{
			s_log.error("Could not start or determine the ip address of {}", name);
		}
		else
		{
			s_log.info("Container {} is now online with assigned address {}",name, ip);
		}

	}
}
