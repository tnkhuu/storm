/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.command;

import java.io.IOException;
import java.util.List;

import org.apache.felix.service.command.Descriptor;
import org.fusesource.jansi.Ansi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.Container;
import org.storm.api.services.vm.ContainerState;
import org.storm.console.ui.framework.component.ASCIITable;
import org.storm.console.ui.framework.component.ASCIITableFactory;
import org.storm.console.ui.framework.component.ASCIITableHeader;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

/**
 * The Class ListLxcInstances.
 *
 * @author Trung Khuu
 * @since 1.0
 */
@Component(properties = { "osgi.command.scope=felix", "osgi.command.function=lslx" }, provide = Object.class)
public class ListLxcInstances
{
	private static Logger s_log = LoggerFactory.getLogger(ListLxcInstances.class);
	private static String[][] EMPTY_TABLE;


	/** The provider. */
	private VirtualizationProvider provider;
	
	static
	{
		EMPTY_TABLE = new String[1][5];
		EMPTY_TABLE[0][0] = "-";
		EMPTY_TABLE[0][1] = "-";
		EMPTY_TABLE[0][2] = "-";
		EMPTY_TABLE[0][3] = "-";
		EMPTY_TABLE[0][4] = "-";
	}
	
	
	/**
	 * Sets the provider.
	 *
	 * @param provider the new provider
	 */
	@Reference
	public void setProvider(VirtualizationProvider provider) {
		this.provider = provider;
	}

	
	/**
	 * Lxclist.
	 */
	@Descriptor("List all lxc instances")
	public void lslx()
	{
		ASCIITable table = null;
		ASCIITableHeader[] headerObjs = {
				new ASCIITableHeader("Host", ASCIITable.ALIGN_CENTER), 
				new ASCIITableHeader("Address", ASCIITable.ALIGN_CENTER), 
				new ASCIITableHeader("State", ASCIITable.ALIGN_CENTER),
				new ASCIITableHeader("Path", ASCIITable.ALIGN_CENTER), 
				new ASCIITableHeader("Type", ASCIITable.ALIGN_CENTER), };
		
		
		try
		{
			CharSequence[][] containerData;
			List<Container> containers = provider.listContainers();
			if(containers.size() == 0) {
				containerData = EMPTY_TABLE;
			}
			else
			{
				//filter system lxc.
				containerData = new String[containers.size()][5];
				for(int i = 0; i < containers.size(); i++) {
					
					Container c = containers.get(i);
					
						containerData[i][0] = c.getHostName();
						containerData[i][1] = c.getIpAddress();
						ContainerState state = c.getState();
						containerData[i][2] = state.name();
						containerData[i][3] = c.getInstalledPath();
						containerData[i][4] = c.getType().name();
					
					
				}
			}

			table = ASCIITableFactory.createTable(headerObjs, containerData, Ansi.Color.GREEN);
			table.printTable();
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
		
		
	}
	
	public static void main(String[] args)
	{
		new ListLxcInstances().lslx();
	}
}
