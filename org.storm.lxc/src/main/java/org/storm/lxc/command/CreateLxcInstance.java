/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.nexus.api.Role;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

/**
 * Creates a new lxc instance
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component(properties = { "osgi.command.scope=felix", "osgi.command.function=mklx" }, provide = Object.class)
public class CreateLxcInstance
{
	private static Logger s_log = LoggerFactory.getLogger(CreateLxcInstance.class);
	private VirtualizationProvider provider;
	
	@Reference
	public void setProvider(VirtualizationProvider provider) {
		this.provider = provider;
	}
	
	
	/**
	 * Creates the inst.
	 *
	 * @param hostname the hostname
	 * @param ipAddress the ip address
	 */
	public void mklx(String hostname, String ip, String dns)
	{
		s_log.debug("> createInst({})", hostname);
		provider.createContainer(HypervisorDriver.LXC, Role.DEFAULT.name(), hostname, dns);
		s_log.debug("< createInst()");
	}
}
