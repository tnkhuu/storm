/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.HypervisorDriver;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

/**
 * The Class DestroyLxcInstance.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component(properties = { "osgi.command.scope=felix", "osgi.command.function=rmlx" }, provide = Object.class)
public class DestroyLxcInstance
{
	
	/** The s_log. */
	private static Logger s_log = LoggerFactory.getLogger(DestroyLxcInstance.class);;

	/** The provider. */
	private VirtualizationProvider provider;
	
	/**
	 * Sets the provider.
	 * 
	 * @param provider
	 *            the new provider
	 */
	@Reference
	public void setProvider(VirtualizationProvider provider) {
		this.provider = provider;
	}
	
	/**
	 * Des.
	 * 
	 * @param name
	 *            the name
	 */
	public void rmlx(String name) {
		if(provider.destroyContainer(HypervisorDriver.LXC, name)) {
			s_log.info("Container {} destroyed", name);
		}
	}
}
