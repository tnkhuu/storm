package org.storm.lxc.config;

import java.io.StringWriter;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class VelocityConfigTest
{

	//@Test
	public void testTemplate() {
		Properties props = new Properties();
		props.put("resource.loader", "file");
		props.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
		props.put("file.resource.loader.path", "/home/tkhuu/dev/src/java/storm-master/org.storm.lxc/src/main/resources/templates");
		props.put("file.resource.loader.cache", "true");
		props.put("jar.resource.loader.path", "");
		
	
		VelocityEngine ve = new VelocityEngine();
		ve.init(props);
		Template t = ve.getTemplate("config");
		VelocityContext ctx = new VelocityContext();
		ctx.put("host", "sandbox");
		ctx.put("rootfs", "/mnt/storm/lxc/test");
		ctx.put("mount", "/mnt/storm/lxc/test");
		StringWriter writer = new StringWriter();
		t.merge( ctx, writer );
		System.out.println( writer.toString() );   
		
		System.out.println(System.getProperty("user.home"));
	}
}
