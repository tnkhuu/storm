/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc;

import java.io.IOException;
import java.util.Random;

import junit.framework.Assert;

import org.apache.commons.exec.ExecuteException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.lxc.terminal.Console;
import org.storm.tools.system.SystemUtils;

public class LxcProviderTest
{
	private static VirtualizationProvider	provider	= new LxcProvider();

	private Random							random		= new Random();
	private static String					lxcName		= "test04";

	@BeforeClass
	public void beforeClass() throws ExecuteException, IOException
	{
		lxcName += random.nextInt(1000);
		if (SystemUtils.IS_OS_LINUX && Console.commandExists("sudo") && Console.commandExists("lxc-create"))
		{
			/*Container container = provider.createContainer(HypervisorDriver.LXC, Role.DEFAULT.name(), lxcName, "8.8.8.8");
			Assert.assertEquals(lxcName, container.getHostName());
			Assert.assertEquals(ContainerState.STOPPED, container.getState());*/
		}
	}

	@Before
	public void before() throws ExecuteException, IOException
	{

	}

	@AfterClass
	public static void testDestroyContainer() throws ExecuteException, IOException
	{
		if (SystemUtils.IS_OS_LINUX && Console.commandExists("sudo") && Console.commandExists("lxc-destroy"))
		{
			boolean destroyed = provider.destroyContainer(HypervisorDriver.LXC, lxcName);
			Assert.assertTrue(destroyed);
		}
	}

	public static void main(String[] args)
  	{
//		Container container = provider.createContainer(HypervisorDriver.LXC, Role.DEFAULT.name(), lxcName, "8.8.8.8");
//		Assert.assertEquals(lxcName, container.getHostName());
//		Assert.assertEquals(ContainerState.STOPPED, container.getState());
	}
}
