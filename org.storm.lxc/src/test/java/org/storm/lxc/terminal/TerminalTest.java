/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *//*
package org.storm.lxc.terminal;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.exec.ExecuteException;
import org.junit.Test;
import org.storm.lxc.SystemUtils;

*//**
 * The Class TerminalTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 *//*
public class TerminalTest
{
	@Test
	public void testHasJava() throws IOException
	{
		if (SystemUtils.IS_OS_LINUX && commandExists("java"))
		{
			Terminal terminal = new Terminal();
			String result = terminal.execute("java -version");
			Assert.assertNotNull(result);
		}
	}

	*//**
	 * Test the terminal execution.
	 * 
	 * @throws ExecuteException
	 *             the execute exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 *//*
	@Test
	public void testCanExecuteSudoTerminal() throws ExecuteException, IOException
	{
		if (SystemUtils.IS_OS_LINUX && commandExists("java") && commandExists("sudo") )
		{
			Terminal terminal = new Terminal();
			Map<String, String> env = new HashMap<String, String>();
			String result = terminal.execute("sudo java -version", env, new File("."));
			Assert.assertNotNull(result);
		}
	}

	@Test
	public void testCanCreateLxc() throws IOException
	{
		if (SystemUtils.IS_OS_LINUX && commandExists("sudo") && commandExists("lxc-create") )
		{
			Terminal terminal = new Terminal();
			String result = terminal.execute("sudo lxc-create -t ubuntu-cloud -n test");
			Assert.assertNotNull(result);
		}
	}

	*//**
	 * Test generic.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 *//*
	@Test
	public void testCanDestroyLxc() throws IOException
	{
		if (SystemUtils.IS_OS_LINUX && commandExists("sudo") && commandExists("lxc-destroy"))
		{
			Terminal terminal = new Terminal();
			String result = terminal.execute("sudo lxc-destroy -f -n test");
			Assert.assertNotNull(result);
		}
	}

	public boolean commandExists(String command) throws ExecuteException, IOException
	{
		if (SystemUtils.IS_OS_LINUX)
		{
			
			Process process = Runtime.getRuntime().exec(new String[]{"bash", "-c", "which " + command + " > /dev/null && echo \"Found\" || echo \"Not Found\""});
			String result = Terminal.getOutput(process.getInputStream());

			if("Found".equals(result)) {
				return true;
			}
			System.out.println("WARNING: Command " + command + " Not found. Storm will not be able to run properly");
		}
		
		return false;
	}

}
*/