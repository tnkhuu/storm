/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.jclouds.compute.RunNodesException;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.BlockDeviceMapping;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.EbsBlockDevice;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.ec2.model.LaunchSpecification;
import com.amazonaws.services.ec2.model.RequestSpotInstancesRequest;
import com.amazonaws.services.ec2.model.RequestSpotInstancesResult;
import com.amazonaws.services.ec2.model.SpotInstanceRequest;
import com.amazonaws.services.ec2.model.SpotInstanceStatus;
import com.amazonaws.services.ec2.model.SpotInstanceType;

/**
 * The Class Launch.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Launch
{
	private static String DEFAULT_SECURITY_GROUP = "storm-prod";
	private static String DEFAULT_REGION = "ap-southeast-2";
	private static LaunchCredentials credentials;

	/**
	 * @param args
	 * @throws IOException
	 * @throws RunNodesException
	 */
	public static void main(String[] args) throws IOException, RunNodesException
	{

		String val = launch();
		System.out.println("val: " + val);

		if (args.length != 7)
		{
			System.out.println("Error : Invalid Arguments");
			System.out.println("	Usage: java org.storm.cloud.Launch <endpoint> <region>  <accesskey> <secret> <ssh-public-key> <spotprice> <user-datascript>");
			System.out
					.println("	e.g : java org.storm.cloud.Launch http://ec2.ap-southeast-2.amazonaws.com ap-southeast-2 earxwVL0DSSp1vZ0m3tIhkyxMZvIkc0Uq4MES4IU KKIAIJHJMLIOCQD4HU4Q /home/ubuntu/.ssh/id_rsa.pub 0.09f /tmp/userdata.sh");
			System.exit(0);
		}
		else
		{
			/*
			 * String spotPrice = args[5]; String userData = args[6]; File ud = new File(userData); byte[] data = IOUtils.toByteArray(new FileInputStream(ud), ud.length()); float
			 * bid = Float.valueOf(spotPrice); credentials = new LaunchCredentials(args[2], args[3]); ComputeServiceContext context =
			 * ContextBuilder.newBuilder("aws-ec2").endpoint(args[0]).credentials(credentials.getAccessKey(), credentials.getSecret()).build(ComputeServiceContext.class);
			 * 
			 * TemplateBuilder templateBuilder = context.getComputeService().templateBuilder(); Template template =
			 * templateBuilder.minCores(4).minRam(12000).os64Bit(true).osFamily(OsFamily.UBUNTU).locationId(args[1]).imageNameMatches(".*12\\.04.*").build();
			 * 
			 * 
			 * template.getOptions().as(AWSEC2TemplateOptions.class) .spotPrice(bid) .runAsRoot(true) .userData(data) .blockUntilRunning(true) .mapNewVolumeToDeviceName("/dev/sdb",
			 * 10, true) .authorizePublicKey(Files.toString(new File(args[4]), Charsets.UTF_8));
			 * 
			 * template.getImage().getUserMetadata().put("rootDeviceType", "ebs"); NodeMetadata node =
			 * Iterables.getOnlyElement(context.getComputeService().createNodesInGroup(DEFAULT_SECURITY_GROUP, 1, template)); System.out.println("Node: " + node.getHostname() +
			 * " with ip " + node.getPublicAddresses() + " launched.");
			 */}

	}

	public static String launch() throws FileNotFoundException, IOException
	{
		AmazonEC2Client c = new AmazonEC2Client(new BasicAWSCredentials("AKIAIJHJMLIOVQD4HU4Q", "daruwVL0DSSp1vZ0m3tIhkyxMZvIkc0Uq4MES4IU"));
		c.setEndpoint("http://ec2.ap-southeast-2.amazonaws.com");
		c.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_2));
		RequestSpotInstancesResult result = c.requestSpotInstances(new RequestSpotInstancesRequest("0.09f")
				.withInstanceCount(1)
				.withType(SpotInstanceType.OneTime)
				.withLaunchSpecification(
				new LaunchSpecification()
					.withInstanceType(InstanceType.M1Xlarge)
					.withImageId("ami-9d4bdba7")
					.withSecurityGroups("storm-prod")
					.withKeyName("storm")
					.withUserData(new String(Base64.encodeBase64("sudo apt-get update && sudo apt-get upgrade -y".getBytes()), "UTF-8"))
					.withBlockDeviceMappings(
					new BlockDeviceMapping()
						.withDeviceName("/dev/sdb")
						.withEbs(
						new EbsBlockDevice()
							.withVolumeSize(10)))));
		
		SpotInstanceRequest re = result.getSpotInstanceRequests().get(0);
		SpotInstanceStatus st = re.getStatus();
		String id = re.getSpotInstanceRequestId();
		int att = 10;String instanceId = null;
		while (att-- > 0)
		{

			DescribeSpotInstanceRequestsResult res = c.describeSpotInstanceRequests(new DescribeSpotInstanceRequestsRequest().withSpotInstanceRequestIds(id));
			List<SpotInstanceRequest> reqs = res.getSpotInstanceRequests();
			if (reqs != null && reqs.size() > 0)
			{
				System.out.println(new Date() + " " + reqs.get(0).getState());
				System.out.println(new Date() + " " + reqs.get(0).getStatus());
				System.out.println(new Date() + " " + reqs.get(0).getSpotInstanceRequestId());
				String code = reqs.get(0).getStatus().getCode();
				
				instanceId = reqs.get(0).getInstanceId();
				if(instanceId != null) {
					break;
				}
				try
				{
					Thread.sleep(20000);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	
		// CreateImageResult crash = c.createImage(new CreateImageRequest(instanceId, "storm-image"));
		return instanceId;

	}

}
