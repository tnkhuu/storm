/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud;



/**
 * An Credential tied to a session .
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public final class LaunchCredentials
{

	/** The access key. */
	private final String accessKey;

	/** The secret. */
	private final String secret;

	/**
	 * Instantiates a new credentials.
	 * 
	 * @param accessKey
	 *            the access key
	 * @param secret
	 *            the secret
	 * @param provider
	 *            the provider
	 */
	public LaunchCredentials(String accessKey, String secret)
	{
		this.accessKey = accessKey;
		this.secret = secret;
	}

	/**
	 * Gets the access key.
	 * 
	 * @return the access key
	 */
	public String getAccessKey()
	{
		return accessKey;
	}

	/**
	 * Gets the secret.
	 * 
	 * @return the secret
	 */
	public String getSecret()
	{
		return secret;
	}

	

}
