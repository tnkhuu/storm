/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;
import org.msgpack.MessagePack;

public class TestRoleSerialization
{

	/**
	 * Test role serialization.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testRoleSerialization() throws IOException {
		Role r = Role.AGENT;
		
		MessagePack p = new MessagePack();
		p.register(Role.class);
		byte[] res =	p.write(r);
		
		Role read =	(Role) p.read(res, Role.class);
		Assert.assertEquals(read, r);
	}
}
