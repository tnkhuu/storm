/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

/**
 * A mapping of known domain names to active ip addresses
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Endpoints
{
	String		ORIGIN				= "stormclowd.com";
	EndPoint	ZONE				= new EndPoint(ORIGIN);
	EndPoint	DNS					= new EndPoint("dns.stormclowd.com");
	EndPoint	DNS_ABS				= new EndPoint("dns.stormclowd.com.");
	EndPoint	PROXY				= new EndPoint("stormclowd.com");
	EndPoint	PROXY_ABS			= new EndPoint("stormclowd.com.");
	EndPoint	STORM_WWW			= new EndPoint("www.stormclowd.com.");
	EndPoint	ENSEMBLE			= new EndPoint("gateway.stormclowd.com");
	EndPoint	ENSEMBLE_ABS		= new EndPoint("gateway.stormclowd.com.");
	EndPoint	ROUTING				= new EndPoint("route.stormclowd.com");
	EndPoint	ROUTING_ABS			= new EndPoint("route.stormclowd.com.");
	EndPoint	DB					= new EndPoint("db.stormclowd.com");
	EndPoint	DB_ABS				= new EndPoint("db.stormclowd.com.");
	EndPoint	JETTY				= new EndPoint("server.stormclowd.com");
	EndPoint	JETTY_ABS			= new EndPoint("server.stormclowd.com.");
	EndPoint	REGISTRATION_ABS	= new EndPoint("reg.stormclowd.com.");
	EndPoint	SERVICES_ABS		= new EndPoint("services.stormclowd.com.");
	EndPoint	ARTIFACTS_ABS		= new EndPoint("artifacts.stormclowd.com.");
	EndPoint	ARTIFACTS			= new EndPoint("artifacts.stormclowd.com");
	EndPoint	CONTAINER_ABS		= new EndPoint("container.stormclowd.com.");
	EndPoint	CONTAINER			= new EndPoint("container.stormclowd.com");
	EndPoint	AGENT				= new EndPoint("agent.stormclowd.com");
	EndPoint	AGENT_ABS			= new EndPoint("agent.stormclowd.com.");
	EndPoint	DEFAULT				= new EndPoint("artifacts.stormclowd.com");
	EndPoint	DEFAULT_ABS			= new EndPoint("artifacts.stormclowd.com.");
	int			ENSEMBLE_PORT		= 2181;
	String		UPSTREAM_DNS		= "8.8.8.8";
}
