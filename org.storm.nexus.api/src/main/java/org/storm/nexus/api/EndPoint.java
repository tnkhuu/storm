/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;



/**
 * A EndPoint in the storm cloud.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class EndPoint
{
	
	/** The domain. */
	private final String domain;
	
	/** The ip. */
	private final String[] ips;
	
	/** The records origin. */
	private final Domain origin;
	
	/**
	 * Gets the domain.
	 * 
	 * @return the domain
	 */
	public String getDomain()
	{
		return domain;
	}
	
	/**
	 * Gets the ip.
	 * 
	 * @return the ip
	 */
	public String[] getIps()
	{
		return ips;
	}
	
	/**
	 * Return the origin.
	 * 
	 * @return
	 */
	public Domain getOrigin()
	{
		return origin;
	}
	
	/**
	 * Instantiates a new end point.
	 * 
	 * @param domain
	 *            the domain
	 * @param ip
	 *            the ip
	 */
	public EndPoint(String domain, String ...ips)
	{
		this.domain = domain;
		this.ips = ips;
		this.origin = null;
	}
	
	public EndPoint(String domain, Domain Origin, String ...ips)
	{
		this.domain = domain;
		this.ips = ips;
		this.origin = Origin;
	}
}
