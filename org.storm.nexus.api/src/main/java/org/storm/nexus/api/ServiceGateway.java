/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

import javax.management.ServiceNotFoundException;

import org.storm.nexus.api.exception.AgentUnavailableException;

/**
 * A service factory, which is attached to the nexus gateway and can lookup and
 * retrieve distributed services provided by other nodes on the cloud.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface ServiceGateway
{

	/**
	 * Lookup a distributed service on the nexus ensemble. If their are multiple
	 * agents providing the same service, one will be picked at random.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param service
	 *            the service
	 * @return a proxied, remote service.
	 * 
	 * @throws {@link ServiceNotFoundException} if the service could not be
	 *         found
	 */
	<T> T lookup(Class<T> service) throws ServiceNotFoundException;

	/**
	 * Lookup a distributed service on a specific agent in the ensemble.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param service
	 *            the service
	 * @param targetAgent
	 *            the target agent
	 * @return a proxied, remote service.
	 * 
	 * @throws {@link ServiceNotFoundException} if the service could not be
	 *         found
	 * @throws {@link AgentUnavailableException} if the agent is no longer
	 *         available.
	 */
	<T> T lookup(Class<T> service, Agent targetAgent) throws ServiceNotFoundException, AgentUnavailableException;
}
