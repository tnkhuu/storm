/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.api.exception;

import java.io.IOException;

import org.msgpack.packer.Packer;
import org.msgpack.unpacker.Unpacker;

/**
 * The Class NoMethodError.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class NoMethodException extends RemoteRPCException
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new no method error.
	 */
	public NoMethodException()
	{
		super();
	}

	/**
	 * Instantiates a new no method error.
	 * 
	 * @param message
	 *            the message
	 */
	public NoMethodException(String message)
	{
		super(message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.error.RemoteError#writeTo(org.msgpack.packer
	 * .Packer)
	 */
	@Override
	public void writeTo(Packer pk) throws IOException
	{
		pk.writeArrayBegin(1);
		pk.write(getMessage());
		pk.writeArrayEnd();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.error.RemoteError#readFrom(org.msgpack.
	 * unpacker.Unpacker)
	 */
	@Override
	public void readFrom(Unpacker u) throws IOException
	{
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.error.RemoteError#messagePack(org.msgpack
	 * .packer.Packer)
	 */
	@Override
	public void messagePack(Packer pk) throws IOException
	{
		writeTo(pk);
	}

	/** The Constant CODE. */
	public static final String CODE = "RemoteError.NoMethodError";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.agent.service.error.RemoteError#getCode()
	 */
	@Override
	public String getCode()
	{
		return CODE;
	}
}
