/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

import java.util.List;

/**
 * A agent is what keeps a container connected,wired up and synchronized on the
 * storm cloud. It is the hook into the zookeeper ensemblem, and always exists
 * across every node.
 * 
 * It provides a gateway to access services within the cloud and is responsible
 * for managing the state and service pipeline of the node that its currently
 * running on. It registers against the nexus ensemble via a direct DNS lookup
 * and publishes all known services the node was configured to provide.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Agent
{
	String AGENT_GROUPS = "/agents";
	String AGENT_DESCRIPTION = "description";
	String NEXUS_PORT = "nexus.port";
	String RPC_PORT = "rpc.port";
	String PATH_SEP = "/";
	String PORT_SEP = ":";
	int DEFAULT_RPC_PORT = 1338;
	int DEFAULT_NEXUS_PORT = 2181;

	/**
	 * Gets the agent name.
	 * 
	 * @return the agent name
	 */
	String getAgentName();

	/**
	 * Gets the server ip address.
	 * 
	 * @return the server ip address
	 */
	String getServerIpAddress();

	/**
	 * Gets the children.
	 * 
	 * @param path
	 *            the path
	 * @param watch
	 *            the watch
	 * @return the children
	 * @throws Exception
	 *             the exception
	 */
	List<String> getChildren(String path, boolean watch) throws Exception;

	/**
	 * Receive.
	 * 
	 * @param message
	 *            the message
	 */
	void receive(Message message);

	/**
	 * Gets the port.
	 * 
	 * @return the port
	 */
	int getPort();

	/**
	 * Gets the rpc port.
	 * 
	 * @return the rpc port
	 */
	int getRpcPort();

	/**
	 * Gets the.
	 * 
	 * @param path
	 *            the path
	 * @param watch
	 *            the watch
	 * @return the byte[]
	 * @throws Exception
	 *             the exception
	 */
	byte[] get(String path, boolean watch) throws Exception;

	/**
	 * Exists.
	 * 
	 * @param path
	 *            the path
	 * @return true, if successful
	 */
	boolean exists(String path);

	/**
	 * Checks for service.
	 * 
	 * @param serviceName
	 *            the service name
	 * @return true, if successful
	 */
	boolean hasService(String serviceName);

	/**
	 * Creates the.
	 * 
	 * @param path
	 *            the path
	 * @param data
	 *            the data
	 */
	void create(String path, byte[] data);

	/**
	 * Ensure path.
	 * 
	 * @param path
	 *            the path
	 */
	void ensurePath(String path);

	/**
	 * Ensure path.
	 * 
	 * @param path
	 *            the path
	 * @param lastNodeEphemerald
	 *            the last node ephemerald
	 * @param data
	 *            the data
	 */
	void ensurePath(String path, boolean lastNodeEphemerald, byte[] data);

	/**
	 * Delete.
	 * 
	 * @param path
	 *            the path
	 */
	void delete(String path);

	/**
	 * Gets the servers with role.
	 * 
	 * @param role
	 *            the role
	 * @return the servers with role
	 */
	String getServersWithRole(Role role);

	/**
	 * Wait for agent.
	 * 
	 * @param <V>
	 *            the value type
	 * @param role
	 *            the role
	 * @param callback
	 *            the callback
	 * @return the v
	 */
	<V> V waitForAgent(Role role, AgentCallback<V> callback);

}
