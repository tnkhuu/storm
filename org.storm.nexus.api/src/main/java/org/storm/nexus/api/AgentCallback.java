/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

import java.util.concurrent.Callable;

/**
 * The Zookeeper callback for zookeeper watch events.
 * @param <V>
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class AgentCallback<V> implements Callable<V>
{
	
	/** The agent address. */
	private final String agentAddress;
	
	
	/**
	 * Instantiates a new agent callback.
	 * 
	 * @param agentAddress
	 *            the agent address
	 * @param callback
	 *            the callback
	 */
	public AgentCallback(String agentAddress) {
		this.agentAddress = agentAddress;
	}

	/**
	 * Gets the agent address.
	 * 
	 * @return the agent address
	 */
	public String getAgentAddress()
	{
		return agentAddress;
	}

	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof AgentCallback)) return false;
		AgentCallback<?> comp = (AgentCallback<?>) obj;
		return hashCode() == comp.hashCode();
	}
	
	@Override
	public int hashCode()
	{
		return agentAddress.hashCode();
	}
}
