/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

import org.msgpack.annotation.Message;
import org.msgpack.annotation.OrdinalEnum;

/**
 * The Enum Role.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Message
@OrdinalEnum
public enum Role
{
	/** The various roles a node can take. */
	DNS
	{
		@Override
		public String[] getTraits()
		{
			return new String[] { "org.storm.dns.client", 
							      "org.storm.dns.server", 
							      "org.storm.zookeeper",
							      "org.storm.nexus.agent"};
		}

		@Override
		public String getDomain(boolean getAbsolute)
		{
				return Endpoints.DNS_ABS.getDomain();

		}
	},
	SERVER
	{
		@Override
		public String[] getTraits()
		{
			return new String[] { 
		  			  			  "org.storm.nexus.agent", 
		  			  			  "org.storm.zookeeper",
		  			  			  "org.storm.jetty",
							      "org.storm.dns.client"};
		}

		@Override
		public String getDomain(boolean getAbsolute)
		{
				return Endpoints.JETTY_ABS.getDomain();

		}
	},
	PROXY
	{
		@Override
		public String[] getTraits()
		{
			return new String[] { 
								  "org.storm.nexus", 
					  			  "org.storm.nexus.agent",
					  			  "org.storm.zookeeper",
					  			  "org.storm.xstream",
					  			  "org.storm.json", 
					  			  "org.storm.dns.client", 
					  			  "org.storm.disruptor",
					  			  "org.storm.server",
					  			  "org.storm.jetty"};
		}

		@Override
		public String getDomain(boolean getAbsolute)
		{
			return Endpoints.PROXY_ABS.getDomain();
		}
	},
	AGENT
	{
		@Override
		public String[] getTraits()
		{
			return new String[] { "org.storm.nexus.agent", 
								  "org.storm.zookeeper",
								  "org.storm.openflowj", 
								  "org.storm.jgit", 
								  "org.storm.libvirt", 
								  "org.storm.dns.client"};
		}

		@Override
		public String getDomain(boolean getAbsolute)
		{
			return Endpoints.AGENT_ABS.getDomain();
		}
	},
	ENSEMBLE
	{
		@Override
		public String[] getTraits()
		{
			return new String[] { "org.storm.dns.client",
								  "org.storm.nexus", 
								  "org.storm.nexus.agent", 
								  "org.storm.zookeeper",
								  "org.storm.xstream",
								  "org.storm.json"};
		}

		@Override
		public String getDomain(boolean getAbsolute)
		{
			return Endpoints.ENSEMBLE_ABS.getDomain();

		}
	},
	SWITCH {
		@Override
		public String getDomain(boolean getAbsolute)
		{
			return Endpoints.ROUTING_ABS.getDomain();
		}
		
		@Override
		public String[] getTraits()
		{
			return new String[] {  "org.storm.dns.client",
					  			   "org.storm.nexus.agent", 
					  			   "org.storm.zookeeper",
					  			   "org.storm.xstream",
					  			   "org.storm.json",
					  			   "org.storm.floodlight"};
		}
	},
	DB {
		@Override
		public String getDomain(boolean getAbsolute)
		{
			return Endpoints.DB.getDomain();
		}
		
		@Override
		public String[] getTraits()
		{
			return new String[]  {"org.storm.nexus.agent",
				      			  "org.storm.zookeeper",
				      			  "org.storm.dns.client"};
		}
	},
	DEFAULT
	{
		@Override
		public String[] getTraits()
		{
			return new String[]  {"org.storm.nexus.agent",
							      "org.storm.zookeeper",
							      "org.storm.dns.client"};
		}

		@Override
		public String getDomain(boolean getAbsolute)
		{
				return Endpoints.DEFAULT_ABS.getDomain();

		}
	},
	INITIAL

	{
		@Override
		public String[] getTraits()
		{
			return new String[0];
		}

		@Override
		public String getDomain(boolean getAbsolute)
		{
				return Endpoints.DEFAULT_ABS.getDomain();

		}
	};
	
	/**
	 * Gets the traits.
	 * 
	 * @return the traits
	 */
	public abstract String[] getTraits();
	
	/**
	 * Gets the domain.
	 * 
	 * @param getAbsolute
	 *            the get absolute
	 * @return the domain
	 */
	public abstract String getDomain(boolean getAbsolute);
	public static final String LOCATION = "/role";
}
