/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.api.exception;

import java.io.IOException;

import org.msgpack.MessagePackable;
import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.type.Value;
import org.msgpack.type.ValueFactory;
import org.msgpack.unpacker.Unpacker;

/**
 * The Class RemoteError.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RemoteRPCException extends RPCException implements MessagePackable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The data. */
	private Value data;

	/**
	 * Instantiates a new remote error.
	 */
	public RemoteRPCException()
	{
		super();
		this.data = ValueFactory.createArrayValue(new Value[] { ValueFactory.createRawValue("unknown error") });
	}

	/**
	 * Instantiates a new remote error.
	 * 
	 * @param message
	 *            the message
	 */
	public RemoteRPCException(String message)
	{
		super(message);
		this.data = ValueFactory.createArrayValue(new Value[] { ValueFactory.createRawValue(message) });
	}

	/**
	 * Instantiates a new remote error.
	 * 
	 * @param data
	 *            the data
	 */
	public RemoteRPCException(Value data)
	{
		super(loadMessage(data));
		this.data = data;
	}

	/**
	 * Gets the data.
	 * 
	 * @return the data
	 */
	public Value getData()
	{
		return data;
	}

	/**
	 * Message pack.
	 * 
	 * @param pk
	 *            the pk
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void messagePack(Packer pk) throws IOException
	{
		pk.write(data);
	}

	/**
	 * Load message.
	 * 
	 * @param data
	 *            the data
	 * @return the string
	 */
	private static String loadMessage(Value data)
	{
		try
		{
			if (data.isRawValue())
			{
				return data.asRawValue().getString();
			}
			else
			{
				return data.asArrayValue().getElementArray()[0].asRawValue().getString();
			}
		}
		catch (MessageTypeException e)
		{
			return "unknown error: " + data;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msgpack.MessagePackable#writeTo(org.msgpack.packer.Packer)
	 */
	@Override
	public void writeTo(Packer pk) throws IOException
	{
		pk.write(data);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msgpack.MessagePackable#readFrom(org.msgpack.unpacker.Unpacker)
	 */
	@Override
	public void readFrom(Unpacker u) throws IOException
	{
		data = u.readValue();
	}

	/** The Constant CODE. */
	public static final String CODE = "RemoteError";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.agent.service.error.RPCError#getCode()
	 */
	@Override
	public String getCode()
	{
		return CODE;
	}
}
