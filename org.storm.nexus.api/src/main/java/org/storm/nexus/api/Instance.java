/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * A representation of a node descriptor within a cloud of instances.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@JsonRootName("instance")
public final class Instance
{

	/** The name. */
	private final String name;

	/** The description. */
	private final String description;

	/** The server address. */
	private final String serverAddress;

	/** The port. */
	private final int port;

	/** The configured rpc server port */
	private final int rpcPort;

	/**
	 * Instantiates a new instance.
	 * 
	 * @param name
	 *            the name
	 * @param description
	 *            the description
	 * @param serverAddress
	 *            the server address
	 * @param port
	 *            the port
	 */
	@JsonCreator
	public Instance(@JsonProperty("name") String name, @JsonProperty("description") String description, @JsonProperty("serverAddress") String serverAddress,
			@JsonProperty("port") int port, @JsonProperty("rpcPort") int rpcPort)
	{
		this.name = name;
		this.description = description;
		this.serverAddress = serverAddress;
		this.port = port;
		this.rpcPort = rpcPort;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the server address.
	 * 
	 * @return the server address
	 */
	public String getServerAddress()
	{
		return serverAddress;
	}

	/**
	 * Gets the port.
	 * 
	 * @return the port
	 */
	public int getPort()
	{
		return port;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the rpcPort
	 */
	public int getRpcPort()
	{
		return rpcPort;
	}
}
