/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.api;

import java.util.List;


/**
 * The Class RemoteAgent.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class RemoteAgent implements Agent
{

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#getAgentName()
	 */
	@Override
	public String getAgentName()
	{
		return getServerIpAddress();
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#getPort()
	 */
	@Override
	public int getPort()
	{
		throw new UnsupportedOperationException();
	}
	

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#getChildren(java.lang.String, boolean)
	 */
	@Override
	public List<String> getChildren(String path, boolean watch) throws Exception
	{
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#receive(org.storm.nexus.api.Message)
	 */
	@Override
	public void receive(Message message)
	{
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#get(java.lang.String, boolean)
	 */
	@Override
	public byte[] get(String path, boolean watch) throws Exception
	{
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#exists(java.lang.String)
	 */
	@Override
	public boolean exists(String path)
	{
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#hasService(java.lang.String)
	 */
	@Override
	public boolean hasService(String serviceName)
	{
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#create(java.lang.String, byte[])
	 */
	@Override
	public void create(String path, byte[] data)
	{
		throw new UnsupportedOperationException();

	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#ensurePath(java.lang.String)
	 */
	@Override
	public void ensurePath(String path)
	{
		throw new UnsupportedOperationException();

	}
	
	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#ensurePath(java.lang.String, boolean, byte[])
	 */
	public void ensurePath(String path, boolean lastNodeEphemerald, byte[] data)
	{
		throw new UnsupportedOperationException();

	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#delete(java.lang.String)
	 */
	@Override
	public void delete(String path)
	{
		throw new UnsupportedOperationException();

	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#getServersWithRole(org.storm.nexus.api.Role)
	 */
	@Override
	public String getServersWithRole(Role role)
	{
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.api.Agent#waitForAgent(org.storm.nexus.api.Role, org.storm.nexus.api.AgentCallback)
	 */
	@Override
	public <V> V waitForAgent(Role role, AgentCallback<V> callback)
	{
		throw new UnsupportedOperationException();
	}

}
