/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.kernel;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.storm.api.dns.DNSAsyncClient;
import org.storm.api.dns.MessageCallback;
import org.storm.api.factories.FilterFactory;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.api.Role;

/**
 * A actor represents a node in the cloud that is preconfigured with some specific purpose. The {@link Role} it is assigned represents the type of function it will perform in the
 * cloud.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class Actor
{
	private static final String OUTBOUND_ETHERNET_NAME = "eth0";
	private Role role;

	/**
	 * Gets the role of this node.
	 * 
	 * @return the role
	 */
	public Role getRole()
	{
		if (role == null)
		{
			role = Roles.getRole();
		}
		return role;
	}

	/**
	 * Gets the outbound address.
	 * 
	 * @return the outbound address
	 */
	protected String getOutboundAddress()
	{
		String address = null;
		try
		{
			NetworkInterface ni = NetworkInterface.getByName(OUTBOUND_ETHERNET_NAME);
			Enumeration<InetAddress> addresses = ni.getInetAddresses();

			while (addresses.hasMoreElements())
			{
				InetAddress addr = addresses.nextElement();
				if (addr instanceof Inet4Address)
				{
					address = addr.getHostAddress();
					break;
				}
			}
		}
		catch (Exception e)
		{
			// don't care.
		}
		if (address == null)
		{
			try
			{
				address = InetAddress.getLocalHost().getHostAddress();
			}
			catch (UnknownHostException e)
			{
				// should never happen
			}
		}
		return address;
	}

	/**
	 * Register a ARecord entry into the cloud dns with the specified domain name and role.
	 * 
	 * @param context
	 *            the context
	 */
	@SuppressWarnings("rawtypes")
	protected void addDNSRecord(BundleContext context)
	{
		if (getRole() != Role.INITIAL)
		{
			try
			{
				ServiceReference<DNSAsyncClient> sr = context.getServiceReference(DNSAsyncClient.class);
				if (sr != null)
				{
					updateDNS(context.getService(sr));
				}
				else
				{
					context.addServiceListener(new ActorRegistrationListener(context), FilterFactory.createServiceFilterString(DNSAsyncClient.class));
				}

			}
			catch (InvalidSyntaxException e)
			{
				throw new RuntimeException(e);
			}
		}
	}

	protected void removeDNSRecord(BundleContext context)
	{

	}

	protected void removeDNSRecord(BundleContext context, String domain)
	{

	}

	/**
	 * Register a ARecord entry into the cloud dns with the specified domain name and role.
	 * 
	 * @param context
	 *            the context
	 * @param domain
	 *            the domain
	 */
	@SuppressWarnings("rawtypes")
	protected void addDNSRecord(BundleContext context, String domain)
	{
		if (getRole() != Role.INITIAL)
		{
			try
			{
				ServiceReference<DNSAsyncClient> sr = context.getServiceReference(DNSAsyncClient.class);
				if (sr != null)
				{
					updateDNS(context.getService(sr), domain);
				}
				else
				{
					context.addServiceListener(new ActorRegistrationListener(context, domain), FilterFactory.createServiceFilterString(DNSAsyncClient.class));
				}

			}
			catch (InvalidSyntaxException e)
			{
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * The listener interface for receiving actorRegistration events. The class that is interested in processing a actorRegistration event implements this interface, and the object
	 * created with that class is registered with a component using the component's <code>addActorRegistrationListener<code> method. When
	 * the actorRegistration event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see ActorRegistrationEvent
	 */
	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	protected class ActorRegistrationListener implements ServiceListener
	{

		/** The context. */
		private final BundleContext context;

		/** The domain. */
		private String domain;

		/**
		 * Instantiates a new actor registration listener.
		 * 
		 * @param context
		 *            the context
		 */
		protected ActorRegistrationListener(BundleContext context)
		{
			this.context = context;
		}

		/**
		 * Instantiates a new actor registration listener.
		 * 
		 * @param context
		 *            the context
		 * @param domain
		 *            the domain
		 */
		protected ActorRegistrationListener(BundleContext context, String domain)
		{
			this.context = context;
			this.domain = domain;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.osgi.framework.ServiceListener#serviceChanged(org.osgi.framework.ServiceEvent)
		 */
		@Override
		public void serviceChanged(ServiceEvent event)
		{
			ServiceReference<DNSAsyncClient> sr = (ServiceReference<DNSAsyncClient>) event.getServiceReference();
			switch (event.getType())
			{
			case ServiceEvent.REGISTERED:
			{
				DNSAsyncClient client = context.getService(sr);
				updateDNS(client, domain == null ? getRole().getDomain(true) : domain);
				break;
			}
			case ServiceEvent.UNREGISTERING:
			{
				break;
			}
			}

		}
	}

	/**
	 * Update dns.
	 * 
	 * @param client
	 *            the client
	 */
	@SuppressWarnings(
	{ "rawtypes" })
	private void updateDNS(DNSAsyncClient client)
	{
		updateDNS(client, getRole().getDomain(true));
	}

	/**
	 * Update dns.
	 * 
	 * @param client
	 *            the client
	 * @param domain
	 *            the domain
	 */
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	private void updateDNS(DNSAsyncClient client, String domain)
	{
		try
		{
			client.update(Endpoints.ZONE.getDomain(), domain, getOutboundAddress(), new MessageCallback()
			{

				@Override
				public void onFail(Throwable t)
				{
					t.printStackTrace(System.out);

				}

				@Override
				public void onComplete(Object packet)
				{
					System.out.println("Registered an node with role " + getRole() + " into the DNS with address " + getOutboundAddress());

				}
			});
			final String hostname = getHostName();
			if (hostname != null)
			{
				client.update(Endpoints.ZONE.getDomain(), hostname, getOutboundAddress(), new MessageCallback()
				{

					@Override
					public void onFail(Throwable t)
					{
						t.printStackTrace(System.out);

					}

					@Override
					public void onComplete(Object packet)
					{
						System.out.println("Registered an hostname with name " + hostname + " and role " + getRole() + " into the DNS with address " + getOutboundAddress());

					}
				});
			}
		}
		catch (Exception e)
		{
			e.printStackTrace(System.out);
		}
	}

	private String getHostName()
	{
		try
		{
			return InetAddress.getLocalHost().getHostName() + ".";
		}
		catch (UnknownHostException e)
		{
			return null;
		}
	}

}
