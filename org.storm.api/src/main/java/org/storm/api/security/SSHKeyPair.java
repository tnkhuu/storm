/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.security;



/**
 * A SSH Key Pair.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface SSHKeyPair {

	/**
	 * Gets the signature.
	 * 
	 * @param data
	 *            the data
	 * @return the signature
	 */
	public abstract byte[] getSignature(byte[] data);


	/**
	 * For ssh agent.
	 * 
	 * @return the byte[]
	 * @throws Exception
	 *             the exception
	 */
	public abstract byte[] forSSHAgent() throws Exception;

	/**
	 * Gets the public key comment.
	 * 
	 * @return the public key comment
	 */
	public abstract String getPublicKeyComment();
	
	/**
	 * Gets the public key.
	 * 
	 * @param comment
	 *            the comment
	 * @return the public key
	 */
	public abstract String getPublicKey();
	
	
	/**
	 * Gets the private key.
	 * 
	 * @return the private key
	 */
	public abstract String getPrivateKey();

	/**
	 * Sets the public key comment.
	 * 
	 * @param publicKeyComment
	 *            the new public key comment
	 */
	public abstract void setPublicKeyComment(String publicKeyComment);

	/**
	 * Write private key.
	 * 
	 * @param out
	 *            the out
	 */
	public abstract void writePrivateKey(java.io.OutputStream out);

	/**
	 * Gets the key type.
	 * 
	 * @return the key type
	 */
	public abstract int getKeyType();

	/**
	 * Gets the public key blob.
	 * 
	 * @return the public key blob
	 */
	public abstract byte[] getPublicKeyBlob();

	/**
	 * Write public key.
	 * 
	 * @param out
	 *            the out
	 * @param comment
	 *            the comment
	 */
	public abstract void writePublicKey(java.io.OutputStream out, String comment);
	
	/**
	 * Write public key.
	 * 
	 * @param out
	 *            the out
	 * @param comment
	 *            the comment
	 * @param convert
	 *            the convert
	 */
	public abstract void writePublicKey(java.io.OutputStream out, String comment, boolean convert);

	/**
	 * Write public key.
	 * 
	 * @param name
	 *            the name
	 * @param comment
	 *            the comment
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public abstract void writePublicKey(String name, String comment)
			throws java.io.FileNotFoundException, java.io.IOException;

	/**
	 * Write secsh public key.
	 * 
	 * @param out
	 *            the out
	 * @param comment
	 *            the comment
	 */
	public abstract void writeSECSHPublicKey(java.io.OutputStream out,
			String comment);

	/**
	 * Write secsh public key.
	 * 
	 * @param name
	 *            the name
	 * @param comment
	 *            the comment
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public abstract void writeSECSHPublicKey(String name, String comment)
			throws java.io.FileNotFoundException, java.io.IOException;

	/**
	 * Write private key.
	 * 
	 * @param name
	 *            the name
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public abstract void writePrivateKey(String name)
			throws java.io.FileNotFoundException, java.io.IOException;

	/**
	 * Sets the passphrase.
	 * 
	 * @param passphrase
	 *            the new passphrase
	 */
	public abstract void setPassphrase(String passphrase);

	/**
	 * Sets the passphrase.
	 * 
	 * @param passphrase
	 *            the new passphrase
	 */
	public abstract void setPassphrase(byte[] passphrase);

	/**
	 * Checks if is encrypted.
	 * 
	 * @return true, if is encrypted
	 */
	public abstract boolean isEncrypted();

	/**
	 * Decrypt.
	 * 
	 * @param _passphrase
	 *            the _passphrase
	 * @return true, if successful
	 */
	public abstract boolean decrypt(String _passphrase);

	/**
	 * Decrypt.
	 * 
	 * @param _passphrase
	 *            the _passphrase
	 * @return true, if successful
	 */
	public abstract boolean decrypt(byte[] _passphrase);

}