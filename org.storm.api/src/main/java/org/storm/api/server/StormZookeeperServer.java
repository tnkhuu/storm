/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.server;

import java.io.IOException;

/**
 * The Storm Zookeeper Server.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface StormZookeeperServer
{

	/**
	 * Take snapshot.
	 */
	public void takeSnapshot();

	/**
	 * This should be called from a synchronized block on this!.
	 * 
	 * @return the zxid
	 */
	public long getZxid();

	/**
	 * Close session.
	 * 
	 * @param sessionId
	 *            the session id
	 */
	public void closeSession(long sessionId);

	/**
	 * Startup.
	 */
	public void startup();

	/**
	 * Checks if is running.
	 * 
	 * @return true, if is running
	 */
	public boolean isRunning();

	/**
	 * Shutdown.
	 */
	public void shutdown();

	/**
	 * Gets the in process.
	 * 
	 * @return the in process
	 */
	public int getInProcess();

	/**
	 * Gets the server id.
	 * 
	 * @return the server id
	 */
	public long getServerId();

	/**
	 * Gets the global outstanding limit.
	 * 
	 * @return the global outstanding limit
	 */
	public int getGlobalOutstandingLimit();

	/**
	 * return the last proceesed id from the datatree.
	 * 
	 * @return the last processed zxid
	 */
	public long getLastProcessedZxid();

	/**
	 * return the outstanding requests in the queue, which havent been processed
	 * yet.
	 * 
	 * @return the outstanding requests
	 */
	public long getOutstandingRequests();

	/**
	 * trunccate the log to get in sync with others if in a quorum.
	 * 
	 * @param zxid
	 *            the zxid that it needs to get in sync with others
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void truncateLog(long zxid) throws IOException;

	/**
	 * Gets the min session timeout.
	 * 
	 * @return the min session timeout
	 */
	public int getMinSessionTimeout();

	/**
	 * Gets the max session timeout.
	 * 
	 * @return the max session timeout
	 */
	public int getMaxSessionTimeout();

	/**
	 * Gets the client port.
	 * 
	 * @return the client port
	 */
	public int getClientPort();

	/**
	 * Gets the state.
	 * 
	 * @return the state
	 */
	public String getState();

	/**
	 * return the total number of client connections that are alive to this
	 * server.
	 * 
	 * @return the num alive connections
	 */
	public int getNumAliveConnections();

}