package org.storm.api.server;

import org.msgpack.annotation.Message;
import org.msgpack.annotation.OrdinalEnum;

/**
 * The Load on a server.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Message
@OrdinalEnum
public enum Load {
	NONE,
	MINIMAL,
	MEDIUM,
	HEAVY
}