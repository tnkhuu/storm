/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.server;

import org.storm.api.services.DistributedService;

/**
 * A Distributed Server is a node who's purpose is to host applications and 
 * help contribute to the scalability of the cloud ensemble.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface DistributedServer extends DistributedService
{

	/**
	 * Deploy a war to a node.
	 * 
	 * @param war
	 * @param contextPath
	 * @return
	 */
	int deploy(String domainName, String war, String contextPath);
	
	/**
	 * Determine if a war is running on a node.
	 * 
	 * @param war
	 * @return
	 */
	boolean isRunning(String war);
	
	/**
	 * Stop a server on the node.
	 * 
	 * @param war
	 * @return
	 */
	boolean stop(String war);
	
	/**
	 * Terminate the node
	 * 
	 * @return
	 */
	boolean destroy();
	
	/**
	 * Stop the node.
	 * 
	 * @return
	 */
	boolean stop();
	
	/**
	 * The current load of the none.
	 * 
	 * @return the load
	 */
	Load currentLoad();
	
	/**
	 * The server ip of the node.
	 * 
	 * @return server ip of the node.
	 */
	String address();

	
	/**
	 * Server load.
	 */

}
