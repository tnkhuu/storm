/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.application;

import java.net.URI;

/**
 * A model of some application currently deployed and running on the cloud.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Application
{

	/**
	 * Gets the domain name.
	 * 
	 * @return the domain name
	 */
	String getDomainName();

	/**
	 * Return the proxy address for this application. (The container hosting the
	 * app).
	 * 
	 * @return
	 */
	String getProxiedAddress();

	/**
	 * Checks if is running.
	 * 
	 * @return true, if is running
	 */
	boolean running();

	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	URI getURI();

	/**
	 * The application binaries.
	 * 
	 * @return
	 */
	String getAppBinaries();
}
