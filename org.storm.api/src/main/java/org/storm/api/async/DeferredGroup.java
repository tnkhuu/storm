/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.async;

import java.util.Collection;
import java.util.ArrayList;

/**
 * Groups multiple {@link Deferred}s into a single one.
 * <p>
 * This is just a helper class, see {@link Deferred#group} for more details.
 * 
 * @author Code taken from https://github.com/OpenTSDB/async.git
 * @since 1.0
 */
final class DeferredGroup<T> {

  /**
   * The Deferred we'll callback when all Deferreds in the group have been
   * called back.
   */
  private final Deferred<ArrayList<Object>> parent =
    new Deferred<ArrayList<Object>>();

  /** How many results do we expect?  */
  private final int nresults;

  /**
   * All the results for each Deferred we're grouping.
   * Need to acquires this' monitor before changing.
   */
  private final ArrayList<Object> results;

  /**
   * Constructor.
   * @param deferreds All the {@link Deferred}s we want to group.
   */
  public DeferredGroup(final Collection<Deferred<T>> deferreds) {
    nresults = deferreds.size();
    results = new ArrayList<Object>(nresults);

    if (nresults == 0) {
      parent.callback(results);
      return;
    }

    @SuppressWarnings("hiding")
	final class Notify<T> implements Callback<T, T> {
      public T call(final T arg) {
        recordCompletion(arg);
        return arg;
      }
      public String toString() {
        return "notify DeferredGroup@" + DeferredGroup.super.hashCode();
      }
    };

    final Notify<T> notify = new Notify<T>();

    for (final Deferred<T> d : deferreds) {
      d.addBoth(notify);
    }
  }

  /**
   * Returns the parent {@link Deferred} of the group.
   */
  public Deferred<ArrayList<Object>> getDeferred() {
    return parent;
  }

  /**
   * Called back when one of the {@link Deferred} in the group completes.
   * @param result The result of the deferred.
   */
  private void recordCompletion(final Object result) {
    int size;
    synchronized (this) {
      results.add(result);
      size = results.size();
    }
    if (size == nresults) {
      // From this point on, we no longer need to synchronize in order to
      // access `results' since we know we're done, so no other thread is
      // going to call this method on this instance again.
      for (final Object r : results) {
        if (r instanceof Exception) {
          parent.callback(new DeferredGroupException(results, (Exception) r));
          return;
        }
      }
      parent.callback(results);
    }
  }

  public String toString() {
    return "DeferredGroup"
      + "(parent=" + parent
      + ", # results=" + results.size() + " / " + nresults
      + ')';
  }

}
