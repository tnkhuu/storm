/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.async;

/**
 * Exception raised when too many callbacks are chained together.
 * <p>
 * You are not encouraged to catch this exception.  That is why it
 * inherits from {@link StackOverflowError}, because it generally
 * indicates a programming error that would have led to an actual
 * stack overflow had the code been written with sequential code
 * (without callbacks).
 * 
 * @author Code taken from https://github.com/OpenTSDB/async.git
 * @since 1.0
 */
public final class CallbackOverflowError extends StackOverflowError {

  public CallbackOverflowError(final String msg) {
    super(msg);
  }

  private static final long serialVersionUID = 1350030042;
}
