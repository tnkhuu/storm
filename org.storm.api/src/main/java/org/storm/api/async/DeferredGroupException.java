/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.async;

import java.util.ArrayList;

/**
 * Exception used when one the {@link Deferred}s in a group failed.
 * <p>
 * You can create a group of {@link Deferred}s using {@link Deferred#group}.
 * 
 * @author Code taken from https://github.com/OpenTSDB/async.git
 * @since 1.0
 */
public final class DeferredGroupException extends RuntimeException {

  private final ArrayList<Object> results;

  /**
   * Constructor.
   * @param results All the results of the {@link DeferredGroup}.
   * @param first The first exception among those results.
   */
  DeferredGroupException(final ArrayList<Object> results,
                         final Exception first) {
    super("At least one of the Deferreds failed, first exception:", first);
    this.results = results;
  }

  /**
   * Returns all the results of the group of {@link Deferred}s.
   */
  public ArrayList<Object> results() {
    return results;
  }

  private static final long serialVersionUID = 1281980542;

}
