/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.api.async;


/**
 * Async timeout
 * 
 * @author Code taken from https://github.com/OpenTSDB/async.git
 * @since 1.0
 */
public final class TimeoutException extends RuntimeException {

  /**
   * Package-private constructor.
   * @param d The Deferred on which we timed out.
   * @param timeout The original timeout in milliseconds.
   */
TimeoutException(final Deferred<?> d, final long timeout) {
    super("Timed out after " + timeout + "ms when joining " + d);
  }

  private static final long serialVersionUID = 1316924542;

}
