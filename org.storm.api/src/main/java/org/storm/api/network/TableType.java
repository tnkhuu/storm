package org.storm.api.network;

/**
 * The IPTable supported tables.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public enum TableType
{

	FILTER,
	NAT,
	MANGLE,
	RAW;

	/**
	 * Gets the name of the table enum.
	 * 
	 * @return The table type as string
	 */
	public String getName()
	{
		switch (this)
		{
		case FILTER:
			return "filter";
		case NAT:
			return "nat";
		case MANGLE:
			return "mangle";
		case RAW:
			return "raw";
		}
		return "";
	}

	/**
	 * Get the table type from string.
	 * 
	 * @param table
	 *            the table
	 * @return The corresponding table type or null if no type corresponds
	 */
	public static TableType getType(String table)
	{
		if ("filter".equalsIgnoreCase(table))
		{
			return FILTER;
		}
		else if ("nat".equalsIgnoreCase(table))
		{
			return NAT;
		}
		else if ("mangle".equalsIgnoreCase(table))
		{
			return MANGLE;
		}
		else if ("raw".equalsIgnoreCase(table))
		{
			return RAW;
		}
		else
		{
			return null;
		}
	}
}