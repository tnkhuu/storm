/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services.vm;

/**
 * The Class DiskSpace.
 */
public class Metric
{
	
	/** The amount. */
	private final int amount;
	
	/** The metric. */
	private final Storage metric;
	
	/**
	 * Instantiates a new disk space.
	 * 
	 * @param amount
	 *            the amount
	 * @param metric
	 *            the metric
	 */
	private Metric(int amount, Storage metric) {
		this.amount = amount;
		this.metric = metric;
	}
	
	/**
	 * Creates a new disk space metric.
	 * 
	 * @param amount
	 *            the amount
	 * @param metric
	 *            the metric
	 * @return the disk space
	 */
	public static Metric create(int amount, Storage metric) {
		return new Metric(amount, metric);
	}
	
	/**
	 * Gets the amount.
	 * 
	 * @return the amount
	 */
	public int getAmount()
	{
		return amount;
	}
	
	/**
	 * Gets the metric.
	 * 
	 * @return the metric
	 */
	public Storage getMetric()
	{
		return metric;
	}
}
