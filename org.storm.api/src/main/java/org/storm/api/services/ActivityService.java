/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

/**
 * A service which reports on a containers usage and last
 * activity periods.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface ActivityService extends DistributedService
{

	/**
	 * Gets the inactivety period in milliseconds for the current node.
	 * 
	 * @return the inactivety period
	 */
	long getInactivetyPeriod();
	
	/**
	 * Sets the last active.
	 */
	void setLastActive();
	
	/**
	 * Sets the last active.
	 * 
	 * @param lastActive
	 *            the new last active
	 */
	void setLastActive(long lastActive);
	
	/**
	 * Gets the application name.
	 * 
	 * @return the application name
	 */
	String getApplicationName();
}
