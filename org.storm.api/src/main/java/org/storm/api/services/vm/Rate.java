/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services.vm;

import java.util.concurrent.TimeUnit;

/**
 * Defines a rate. i.e. 1 Megabyte/sec.
 * 
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Rate
{
	private final Metric metric;
	
	private final TimeUnit time;

	/**
	 * Instantiates a new rate.
	 * 
	 * @param metric
	 *            the metric
	 * @param time
	 *            the time
	 */
	public Rate(Metric metric, TimeUnit time)
	{
		this.metric = metric;
		this.time = time;
	}

	public Metric getMetric()
	{
		return metric;
	}

	public TimeUnit getTime()
	{
		return time;
	}
	

}
