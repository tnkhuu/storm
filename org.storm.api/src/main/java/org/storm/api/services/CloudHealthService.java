/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.util.List;

import org.storm.api.services.vm.Container;
import org.storm.api.services.vm.Memory;
import org.storm.api.services.vm.Metric;
import org.storm.api.services.vm.Rate;

/**
 * The service which monitor's an instances disk space, memory, iops and network
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface CloudHealthService extends DistributedService, Runnable
{

	
	/**
	 * Gets the active containers.
	 * 
	 * @return the active containers
	 */
	List<Container> getActiveContainers();
	
	/**
	 * Gets the available diskspace.
	 * 
	 * @return the available diskspace
	 */
	Metric getAvailableDiskspace();
	
	/**
	 * Returns the system load as a percentage over the past
	 * 15 minutes.
	 * 
	 * @return the system load as a percentage over the past 15minutes
	 */
	int getSystemLoad();
	
	/**
	 * Return the percentage of used memory in the system.
	 * 
	 * @return  the percentage of used memory in the system.
	 */
	Memory getUsedMemory();
	
	/**
	 * Returns a average in the number of disk read/writes 
	 * for the current node in the past 5 minutes;
	 * 
	 * @return
	 */
	Rate getDiskIOPS();
	
	/**
	 * Returns the average network utilisation as a rate
	 * in the last 5 minutes. 
	 * 
	 * @return
	 */
	Rate getNetworkTraffic();
}
