/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import org.storm.api.network.Protocol;


/**
 * A Virtual Switch Provider using the underlying openswitch library.
 * Requires openswitch to be installed. 
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface SwitchService extends DistributedService
{
	/**
	 * Adds a port to a switch (bridge).
	 *
	 * @param bridgeName the bridge name
	 * @param portName the port name
	 * @return true, if successful
	 */
	boolean addPort(String bridgeName, String portName);
	
	/**
	 * Delete a port from a switch.
	 *
	 * @param bridgeName the bridge name
	 * @param portName the port name
	 * @return true, if successful
	 */
	boolean delPort(String bridgeName, String portName);
	

	/**
	 * Creates the an virtual switch (bridge).
	 *
	 * @param switchName the bridge name
	 * @return true, if successful
	 */
	boolean createSwitch(String switchName);
	
	/**
	 * Checks for an existence of a switch.
	 *
	 * @param name the name
	 * @return true, if the switch exists
	 */
	boolean hasSwitch(String name);
	
	/**
	 * Delete a switch.
	 *
	 * @param name the name
	 * @return true, if successful
	 */
	boolean deleteSwitch(String name);
	
	/**
	 * Sets the controller of a bridge to a specific target
	 *
	 * @param bridgeName the name of the bridge
	 * @param protocol the protocol to use. Valid protocols are Protocol.TCP and Protocol.SSL
	 * @param target the target controller address in ip:port or just ip form.
	 * @return true, if successful
	 */
	boolean setController(String bridgeName, Protocol protocol, String target);
	
	/**
	 * Deletes a controller from a bridge
	 *
	 * @param bridgeName the name
	 * @return true, if successful
	 */
	boolean delController(String bridgeName);
	
	/**
	 * Gets the info about a controller on a bridge
	 *
	 * @param name the name
	 * @return the controller
	 */
	String getController(String bridgeName);
}
