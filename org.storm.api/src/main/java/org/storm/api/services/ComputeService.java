/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.util.Set;

import org.storm.api.cloud.CloudProvider;
import org.storm.api.cloud.Region;
import org.storm.api.exception.OperationFailedException;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.nexus.api.Role;

/**
 * A Compute Service.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface ComputeService extends DistributedService
{
	String DEFAULT_IMAGE_ID = "ap-southeast-2/ami-e2ba2cd8";
	String DEFAULT_REGION = "ap-southeast-2";
	String DEFAULT_SECURITY_GROUP = "storm-prod";
	String HTTP = "https://";
	
	/**
	 * Creates the instance.
	 * 
	 * @param role
	 *            the role
	 * @param driver
	 *            the driver
	 * @param type
	 *            the type
	 * @return the virtual machine
	 * @throws OperationFailedException
	 *             the operation failed exception
	 */
	public VirtualMachine createInstance(Role role, byte[] userData, HypervisorDriver driver, HypervisorType type) throws OperationFailedException;
	
	/**
	 * Creates the spot instance.
	 * 
	 * @param price
	 *            the price
	 * @param role
	 *            the role
	 * @param driver
	 *            the driver
	 * @param type
	 *            the type
	 * @return the virtual machine
	 * @throws OperationFailedException
	 *             the operation failed exception
	 */
	public VirtualMachine createSpotInstance(float price, Role role, HypervisorDriver driver, HypervisorType type) throws OperationFailedException;
	
	
	/**
	 * Creates the spot instance.
	 * 
	 * @param price
	 *            the price
	 * @param role
	 *            the role
	 * @param driver
	 *            the driver
	 * @param type
	 *            the type
	 * @return the virtual machine
	 * @throws OperationFailedException
	 *             the operation failed exception
	 */
	public VirtualMachine[] createInstance(Role role, int instances, byte[] userData, HypervisorDriver driver, HypervisorType type) throws OperationFailedException;
	/**
	 * Creates the instance.
	 * 
	 * @param role
	 *            the role
	 * @param openInBoundPorts
	 *            the open in bound ports
	 * @param script
	 *            the script
	 * @param driver
	 *            the driver
	 * @param type
	 *            the type
	 * @param blockingUntilRunning
	 *            the blocking until running
	 * @return the virtual machine
	 * @throws OperationFailedException
	 *             the operation failed exception
	 */
	public VirtualMachine createInstance(Role role, int[] openInBoundPorts, String script,HypervisorDriver driver, HypervisorType type, boolean blockingUntilRunning) throws OperationFailedException;

	/**
	 * Creates the initial instance.
	 * 
	 * @param role
	 *            the role
	 * @param driver
	 *            the driver
	 * @param userData
	 *            the user data
	 * @param type
	 *            the type
	 * @param blockingUntilRunning
	 *            the blocking until running
	 * @return the virtual machine
	 * @throws OperationFailedException
	 *             the operation failed exception
	 */
	public VirtualMachine createInitialInstance(Role role, HypervisorDriver driver, byte[] userData, HypervisorType type, boolean blockingUntilRunning) throws OperationFailedException;
	
	/**
	 * Creates the instance.
	 * 
	 * @param region
	 *            the region
	 * @param amiID
	 *            the ami id
	 * @return the virtual machine
	 */
	public VirtualMachine createInstance(Region region, String amiID);
	
	/**
	 * Reboot instance.
	 * 
	 * @param instanceId
	 *            the instance id
	 * @return true, if successful
	 */
	boolean rebootInstance(String instanceId);
	
	/**
	 * Terminate instance.
	 * 
	 * @param instanceId
	 *            the instance id
	 * @return true, if successful
	 */
	boolean terminateInstance(String instanceId);

	/**
	 * Gets the running instances.
	 * 
	 * @return the running instances
	 */
	public Set<VirtualMachine> getRunningInstances();
	
	/**
	 * Gets the servicing region.
	 * 
	 * @return the servicing region
	 */
	public Region getServicingRegion();
	
	/**
	 * Gets the backing provider.
	 * 
	 * @return the backing provider
	 */
	public CloudProvider getBackingProvider();

	
}
