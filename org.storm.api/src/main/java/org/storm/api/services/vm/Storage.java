package org.storm.api.services.vm;

public enum Storage
{
	BYTE,
	KILOBYTE,
	MEGABYTE,
	TERABYTE;
}
