/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services.vm;

import java.io.File;

/**
 * A single container vm instance.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Container
{

	/**
	 * Gets the host name.
	 * 
	 * @return the host name
	 */
	String getHostName();

	/**
	 * Gets the ip address.
	 * 
	 * @return the ip address
	 */
	String getIpAddress();

	/**
	 * Gets the state.
	 * 
	 * @return the state
	 */
	ContainerState getState();

	/**
	 * Gets the installed path.
	 * 
	 * @return the installed path
	 */
	String getInstalledPath();

	/**
	 * Start.
	 * 
	 * @return true, if successful
	 */
	boolean start();

	/**
	 * Start.
	 * 
	 * @param runScript
	 *            the run script
	 * @return true, if successful
	 */
	boolean start(File runScript);

	/**
	 * Stop.
	 * 
	 * @return true, if successful
	 */
	boolean stop();

	/**
	 * Destroy.
	 * 
	 * @return true, if successful
	 */
	boolean destroy();

	/**
	 * The container type.
	 */
	HypervisorDriver getType();
}
