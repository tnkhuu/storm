/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services.vm;

/**
 * The metric for used and reserved virtual memory.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Memory
{
	
	/** The total memory. */
	private final long totalMemory;
	
	/** The used memory. */
	private final long usedMemory;
	
	/** The reserved memory. */
	private final long reservedMemory;
	
	/**
	 * Instantiates a new memory.
	 * 
	 * @param totalMemory
	 *            the total memory
	 * @param usedMemory
	 *            the used memory
	 * @param reservedMemory
	 *            the reserved memory
	 */
	public Memory(long totalMemory, long usedMemory, long reservedMemory)
	{
		this.totalMemory = totalMemory;
		this.usedMemory = usedMemory;
		this.reservedMemory = reservedMemory;
	}

	/**
	 * Gets the total memory.
	 * 
	 * @return the total memory
	 */
	public long getTotalMemory()
	{
		return totalMemory;
	}

	/**
	 * Gets the used memory.
	 * 
	 * @return the used memory
	 */
	public long getUsedMemory()
	{
		return usedMemory;
	}

	/**
	 * Gets the reserved memory.
	 * 
	 * @return the reserved memory
	 */
	public long getReservedMemory()
	{
		return reservedMemory;
	}
	
	
}
