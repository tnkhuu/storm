/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.io.IOException;
import java.util.List;

import org.storm.api.services.vm.Container;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;

/**
 * A abstraction to some concrete virtualization provider the kernel will use in
 * its strategy to optimally run/provision and manage the cloud.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface VirtualizationProvider extends DistributedService
{

	/**
	 * Gets the hyper visor driver.
	 * 
	 * @return the hypervisor driver
	 */
	HypervisorDriver getHypervisorDriver();

	/**
	 * Gets the hyper visor type.
	 * 
	 * @return the type
	 */
	HypervisorType getType();

	/**
	 * Creates a new virtualized container configured with the specified
	 * {@link Spec} with the provided hostname
	 * 
	 * @param driver
	 * @param hostname
	 * @return
	 */
	boolean createContainer(HypervisorDriver driver, String role, String hostname, String dns, String... options);
	
	/**
	 * 
	 * @param driver
	 * @param role
	 * @param domainname
	 * @param linkedFS
	 * @param dns
	 */
	boolean createLVMBackedContainer(HypervisorDriver driver, String role, String domainname, String linkedFS, String dns);

	/**
	 * Destroy a container with the given driver and name
	 * 
	 * @param driver
	 * @param hostname
	 * @return true if the container was removed.
	 */
	boolean destroyContainer(HypervisorDriver driver, String hostname);
	
	/**
	 * Stops a container, but does not destroy it.
	 * 
	 * @param driver
	 * @param hostname
	 * @return
	 */
	boolean stopContainer(HypervisorDriver driver, String hostname);
	
	/**
	 * Checks to see if a container exists.
	 * 
	 * @param driver
	 * @param hostname
	 * @return true if a container with name <code>containerName</code> exists.
	 */
	boolean contains(HypervisorDriver driver, String containerName);

	/**
	 * Start a container with the given driver and name
	 * 
	 * @param driver
	 * @param hostname
	 * @return the ip address of the container.
	 */
	String startContainer(String hostname);
	
	/**
	 * Start a container with the given driver and name
	 * 
	 * @param driver
	 * @param hostname
	 * @return true if the container was removed.
	 */
	String getContainerIp(HypervisorDriver driver, String hostname);

	/**
	 * List all containers for the provider.
	 * 
	 * @param driver
	 * @param hostname
	 * @return true if the container was removed.
	 */
	List<Container> listContainers() throws IOException;
	
	

}
