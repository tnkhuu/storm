/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services.vm;

import java.util.Set;

/**
 * A Virtual Machine.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class VirtualMachine
{
	private final Set<String> ipAddresses;
	private final String hostName;
	private final InstanceType type;
	private final String id;
	private final String imageId;
	private final String state;
	private final String privateKeyPair;
	
	/**
	 * Instantiates a new virtual machine.
	 * 
	 * @param ipAddress
	 *            the ip address
	 * @param hostName
	 *            the host name
	 * @param instanceType
	 *            the instance type
	 * @param id
	 *            the id
	 * @param imageId
	 *            the image id
	 * @param state
	 *            the state
	 */
	public VirtualMachine(Set<String> ipAddress, String hostName, InstanceType instanceType, String id, String imageId, String state, String privateKeyPair) {
		this.ipAddresses = ipAddress;
		this.hostName = hostName;
		this.type = instanceType;
		this.id = id;
		this.imageId = imageId;
		this.state = state;
		this.privateKeyPair = privateKeyPair;
	}
	
	/**
	 * Gets the ip address.
	 * 
	 * @return the ip address
	 */
	public Set<String> getIpAddress() {
		return ipAddresses;
	}
	
	/**
	 * Gets the ip address.
	 * 
	 * @return the ip address
	 */
	public String getFirstIpAddress() {
		return ipAddresses != null && ipAddresses.size() > 0 ? ipAddresses.toArray(new String[ipAddresses.size()])[0] : null;
	}

	/**
	 * Gets the host name.
	 * 
	 * @return the host name
	 */
	public String getHostName()
	{
		return hostName;
	}

	

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public InstanceType getType()
	{
		return type;
	}

	/**
	 * Gets the image id.
	 * 
	 * @return the imageId
	 */
	public String getImageId()
	{
		return imageId;
	}

	/**
	 * Gets the state.
	 * 
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object object) {
		if(object == null) return false;
		if(!(object instanceof VirtualMachine)) return false;
		return this.id.equals(((VirtualMachine)object).getId());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return id.hashCode();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "VirtualMachine [ipAddresses=" + ipAddresses + ", hostName=" + hostName + ", type=" + type + ", id=" + id + ", imageId=" + imageId + ", state=" + state + "]";
	}

	/**
	 * @return the privateKeyPair
	 */
	public String getPrivateKeyPair()
	{
		return privateKeyPair;
	}
	
}
