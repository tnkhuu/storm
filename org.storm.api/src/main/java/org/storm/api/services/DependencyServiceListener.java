/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.storm.api.factories.FilterFactory;

/**
 * A service listener which delays a bundle's instantiation until all the
 * specified services are online.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DependencyServiceListener implements ServiceListener
{

	private List<String> dependencies;
	private Runnable callback;
	private BundleContext context;
	private volatile boolean ran = false;

	/**
	 * Instantiates a new dependency service listener that runs the specified
	 * <code>callback</code> after all the services specified in
	 * <code>services</code> have become available to the bundle.
	 * 
	 * @param context
	 *            the bundle context
	 * @param callback
	 *            the runnable to call
	 * @param services
	 *            the list of services to wait to become available.
	 */
	public DependencyServiceListener(BundleContext context, Runnable callback, Class<?>... services)
	{
		this.callback = callback;
		this.context = context;
		this.dependencies = new ArrayList<>(services.length);
		for (Class<?> service : services)
		{
			try
			{
				ServiceReference<?> ref = context.getServiceReference(service);
				// service not yet available.
				if (ref == null)
				{
					context.addServiceListener(new ServiceListener()
					{
						
						@Override
						public void serviceChanged(ServiceEvent event)
						{
							DependencyServiceListener.this.serviceChanged(event);
							
						}
					}, FilterFactory.createServiceFilterString(service));
					dependencies.add(service.getName());
				}
			}
			catch (InvalidSyntaxException e)
			{
				// should never happen.
			}
		}
		// if all services are available, we run straight away.
		if (dependencies.isEmpty())
		{
			if (!ran)
			{
				synchronized (this)
				{
					if (!ran)
					{
						try
						{
							callback.run();
						}
						finally
						{
							ran = true;
							context.removeServiceListener(this);
						}
					}
				}

			}
		}
	}

	/**
	 * A callback for some event that occurred with the service life cycle of a
	 * service were interested in knowing about.
	 * 
	 * @param event
	 *            the event specifics.
	 */
	@Override
	public void serviceChanged(ServiceEvent event)
	{
		if (event.getType() == ServiceEvent.REGISTERED)
		{
			ServiceReference<?> reference = event.getServiceReference();
			String[] objectClass = (String[]) reference.getProperty(Constants.OBJECTCLASS);
			if (objectClass != null)
			{
				for (String service : objectClass)
				{
					synchronized (dependencies)
					{

						if (dependencies.contains(service))
						{
							dependencies.remove(service);
						}
						if (!dependencies.isEmpty())
						{
							for (int i = 0; i < dependencies.size(); i++)
							{
								ServiceReference<?> ref = context.getServiceReference(dependencies.get(i));
								if (ref != null)
								{
									dependencies.remove(i);
								}
							}
						}
						// if all services are now available. Run the callback.
						if (dependencies.isEmpty())
						{
							if (!ran)
							{
								synchronized (this)
								{
									if (!ran)
									{
										try
										{
											callback.run();
										}
										finally
										{
											ran = true;
											context.removeServiceListener(this);
										}
									}
								}
							}

						}
					}

				}
			}

		}

	}
}
