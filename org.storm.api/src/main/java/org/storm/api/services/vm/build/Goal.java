/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services.vm.build;

/**
 * The result of some chained sequence of step executions
 * 
 * @param <T>
 *            the generic type
 * @author Trung Khuu
 * @since 1.0
 */
public interface Goal<T>
{

	/**
	 * Gets the result.
	 * 
	 * @return the result
	 */
	public T getResult();

	/**
	 * Checks if is complete.
	 * 
	 * @return true, if is complete
	 */
	boolean isComplete();

	/**
	 * Checks if is running.
	 * 
	 * @return true, if is running
	 */
	boolean isRunning();
}
