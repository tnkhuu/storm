/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

/**
 * The Interface MetaDataService.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface MetaDataService
{

	/**
	 * Checks if current instance is on a cloud.
	 * 
	 * @return true, if is on cloud
	 */
	boolean isOnCloud();

	/**
	 * Gets the ip4 address.
	 * 
	 * @return the ip4 address
	 */
	String getIp4Address();

	/**
	 * Gets the mac address.
	 * 
	 * @return the mac address
	 */
	String getMacAddress();

	/**
	 * Gets the instance id.
	 * 
	 * @return the instance id
	 */
	String getInstanceId();

	/**
	 * Gets the image id.
	 * 
	 * @return the image id
	 */
	String getImageID();

	/**
	 * Gets the instance host name.
	 * 
	 * @return the instance host name
	 */
	String getHostName();

	/**
	 * Gets the reservation id.
	 * 
	 * @return the reservation id
	 */
	String getReservationID();

	/**
	 * Gets the network interfaces.
	 * 
	 * @return the network interfaces
	 */
	String getNetworkInterfaces();

	/**
	 * Gets the launch index.
	 * 
	 * @return the launch index
	 */
	String getLaunchIndex();

}