/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.io.IOException;

import org.storm.api.security.Encryption;
import org.storm.api.security.SSHKeyPair;



/**
 * A manager of key pairs for ssh.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface SSHKeyPairService {

	String DEFAULT_VM_KEYPAIR = "/keys/storm";
	/**
	 * Gets the key pair.
	 * 
	 * @param name
	 *            the name
	 * @return the key pair
	 */
	public abstract SSHKeyPair getKeyPair(String name);

	/**
	 * Creates the key pair.
	 * 
	 * @param type
	 *            the type
	 * @param strength
	 *            the strength
	 * @return the key pair
	 */
	public abstract SSHKeyPair createKeyPair(Encryption type, int strength);

	/**
	 * Creates the key pair.
	 * 
	 * @param type
	 *            the type
	 * @return the key pair
	 */
	public abstract SSHKeyPair createKeyPair(Encryption type);

	/**
	 * Save key pair.
	 * 
	 * @param name
	 *            the name
	 * @param kp
	 *            the kp
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public abstract void saveKeyPair(String name, SSHKeyPair kp)
			throws IOException;

	/**
	 * Delete key pair.
	 * 
	 * @param name
	 *            the name
	 * @param kp
	 *            the kp
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public abstract boolean removeKeyPair(String name) throws IOException;

}