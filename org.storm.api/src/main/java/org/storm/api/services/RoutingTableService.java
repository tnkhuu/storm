/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.io.IOException;

import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;
import org.storm.api.network.TableType;

/**
 * A hook into the kernels IPTable's.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface RoutingTableService
{

	/**
	 * Route ports.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param type
	 *            the type
	 * @param dport
	 *            the dport
	 * @param toPort
	 *            the to port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void routePorts(Protocol protocol, TableType type, ChainType chain, int dport, int toPort) throws IOException;
	
	/**
	 * Route ports.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param type
	 *            the type
	 * @param dport
	 *            the dport
	 * @param toPort
	 *            the to port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void routePorts(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, int toPort) throws IOException;
	
	/**
	 * Route ports.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param type
	 *            the type
	 * @param dport
	 *            the dport
	 * @param toIp
	 *            the to ip
	 * @param toPort
	 *            the to port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void routePorts(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, String toIp, int toPort) throws IOException;
	
	/**
	 * Contains route.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param type
	 *            the type
	 * @param dport
	 *            the dport
	 * @param toIp
	 *            the to ip
	 * @param toPort
	 *            the to port
	 * @return true, if successful
	 */
	public boolean containsRoute(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, int toPort) throws IOException;
	
	/**
	 * Delete route.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param type
	 *            the type
	 * @param dport
	 *            the dport
	 * @param toIp
	 *            the to ip
	 * @param toPort
	 *            the to port
	 * @return true, if successful
	 */
	public boolean deleteRoute(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, int toPort) throws IOException;
	
	/**
	 * Delete route.
	 * 
	 * @param protocol
	 *            the protocol
	 * @param type
	 *            the type
	 * @param dport
	 *            the dport
	 * @param toIp
	 *            the to ip
	 * @param toPort
	 *            the to port
	 * @return true, if successful
	 */
	public boolean deleteRoute(Protocol protocol, TableType type, ChainType chain, String networkDevice, int dport, String destHost, int toPort) throws IOException;
	
	/**
	 * Set permissions on a given file in the underlying fs.
	 * 
	 * @param perm - the chmod permissions i.e. 777
	 * @param file the path of the file to set
	 */
	public void setPermissions(String perm, String file);
}
