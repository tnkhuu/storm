/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.util.List;

import org.storm.nexus.api.EndPoint;
import org.storm.nexus.api.Role;

/**
 * The Map of known active servers running in the cloud, along with their roles.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface TopologyService
{

	/**
	 * Find servers with role.
	 * 
	 * @param role
	 *            the role
	 * @return the list
	 */
	List<EndPoint> findServersWithRole(Role role);
	
	/**
	 * Adds the endpoint.
	 * 
	 * @param role
	 *            the role
	 * @param point
	 *            the point
	 */
	void addEndpoint(Role role, EndPoint point);
	
	/**
	 * Removes the endpoint.
	 * 
	 * @param role
	 *            the role
	 * @param point
	 *            the point
	 * @return true, if successful
	 */
	boolean removeEndpoint(Role role, EndPoint point);
	
	/**
	 * Lock.
	 */
	 void lock();
	
	/**
	 * Unlock.
	 */
	 void unlock();
}
