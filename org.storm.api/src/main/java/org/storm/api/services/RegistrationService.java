/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import org.storm.nexus.api.Role;

/**
 * A registration service for the DNS. 
 * Maps commonly known aliases to the available set
 * of running nodes, services or containers. 
 * 
 * @author tkhuu
 * @since 1.0
 */
public interface RegistrationService extends DistributedService
{
	/**
	 * Register a specific domain for a given ip.
	 * 
	 * @param domain
	 * @param ip
	 */
	void register(String domain, String ip);
	
	/**
	 * Add an additional provider, for a given role. 
	 * 
	 * @param domain
	 * @param ip
	 */
	void register(Role role, String ip);
	
	/**
	 * Remove a domain, for a given ip. 
	 * 
	 * @param domain
	 * @param ip
	 */
	void unregister(String domain, String ip);
	
	/**
	 * Remove the ip, for the given role. 
	 * 
	 * @param domain
	 * @param ip
	 */
	void unregister(Role domain, String ip);
	
	/**
	 * Checks if is a domain is registered with the given ip.
	 * 
	 * @param domain
	 *            the domain
	 * @param ip
	 *            the ip
	 * @return true, if is register
	 */
	boolean isregistered(String domain, String ip);
	
	/**
	 * Checks if is a ip is registered to the given role.
	 * 
	 * @param domain
	 *            the domain
	 * @param ip
	 *            the ip
	 * @return true, if is register
	 */
	boolean isregistered(Role domain, String ip);
}
