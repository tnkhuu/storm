/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services.vm.build;

/**
 * A typical implementation of some {@link Goal}
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Result implements Goal<Boolean>
{

	/** Flag indicating if the execution succeeded. */
	private boolean success;

	/** Flag indicating if the execution completed. */
	private boolean isComplete;

	/** Flag indicating if the execution is currently running. */
	private boolean isRunning;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.lxc.img.vm.build.Result#getResult()
	 */
	@Override
	public Boolean getResult()
	{
		return success;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.lxc.img.vm.build.Result#isComplete()
	 */
	@Override
	public boolean isComplete()
	{
		return isComplete;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.lxc.img.vm.build.Result#isRunning()
	 */
	@Override
	public boolean isRunning()
	{
		return isRunning;
	}

}
