/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import org.storm.api.exception.LogicalVolumeException;

/**
 * A service which connects storm to the underlying LVM partitioning
 * features on the file system. 
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface LogicalVolumeService extends DistributedService
{
	public static final String DEFAULT_MNT_DIR = "/mnt/storm/lxc";
	public static final String BASE_LVM_IMAGE_NAME = "storm-lxc";
	public static final String BASE_LVM_DIR = "/dev/lxc";
	public static final String LXC_VOLUMN_NAME = "lxc";
	public static final String BASE_LXC_LVM_DIR = BASE_LVM_DIR + "/" + BASE_LVM_IMAGE_NAME;
	
	/**
	 * Checks if is ready.
	 * 
	 * @return true, if is ready
	 */
	boolean isReady();
	
	/**
	 * Destroy logical volumn.
	 * 
	 * @param name
	 *            the name
	 * @param volumnGroup
	 *            the volumn group
	 * @return true, if successful
	 * @throws LogicalVolumeException
	 *             the logical volume exception
	 */
	boolean destroyLogicalVolumn(String name, String volumnGroup) throws LogicalVolumeException;
	
	/**
	 * Destroy volumn group.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 * @throws LogicalVolumeException
	 *             the logical volume exception
	 */
	boolean destroyVolumnGroup(String name) throws LogicalVolumeException;
	
	/**
	 * Logical volumn exists.
	 * 
	 * @param name
	 *            the name
	 * @param volumnGroup
	 *            the volumn group
	 * @return true, if successful
	 */
	boolean logicalVolumnExists(String name, String volumnGroup);
	
	/**
	 * Creates the volumn group.
	 * 
	 * @param name
	 *            the name
	 * @param devices
	 *            the devices
	 * @return the string
	 * @throws LogicalVolumeException
	 *             the logical volume exception
	 */
	String createVolumnGroup(String name, String... devices) throws LogicalVolumeException;
	
	/**
	 * Creates the logical volumn.
	 * 
	 * @param name
	 *            the name
	 * @param sizeInMB
	 *            the size in mb
	 * @param volumnGroup
	 *            the volumn group
	 * @return the string
	 * @throws LogicalVolumeException
	 *             the logical volume exception
	 */
	String createLogicalVolumn(String name,int sizeInMB, String volumnGroup) throws LogicalVolumeException;
	
	/**
	 * Create a LXC backed snapshot volumn.
	 * 
	 * @param name
	 * @param role
	 * @param dns
	 * @param sizeInMB
	 * @return
	 */
	String createLxcBaseLogicalVolumn(String name, String role, String dns, int sizeInMB);
	
	/**
	 * 
	 * @param dns
	 */
	void setBaseLxcDNS(String dns);
	
	/**
	 * Returns true if the base lxc lvm image exists.
	 * 
	 * @return
	 */
	boolean baseLxcLogicalVolumnExists();
	
	/**
	 * Creates the snapshot volumn.
	 * 
	 * @param newLogicalSnapshotName
	 *            the new logical snapshot name
	 * @param sizeInMB
	 *            the size in mb
	 * @param snapshotTarget
	 *            the snapshot target
	 * @param volumnGroup
	 *            the volumn group
	 * @return the string
	 * @throws LogicalVolumeException
	 *             the logical volume exception
	 */
	String createSnapshotVolumn(String newLogicalSnapshotName, String role, int sizeInMB, String snapshotTarget, String volumnGroup) throws LogicalVolumeException;
	
	
}
