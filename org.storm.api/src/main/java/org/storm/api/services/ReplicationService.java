/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.services;

import java.net.URL;

import org.storm.api.services.vm.build.Plan;

/**
 * Launches multiple processes or containers containing the a storm cloud
 * instance.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface ReplicationService extends DistributedService
{

	/**
	 * Replicate another instance of this storm platform to the given URL
	 * target.
	 * 
	 * @param target
	 *            the target
	 * @return a plan on how to do replicate.
	 */
	Plan replicate(URL target);
}
