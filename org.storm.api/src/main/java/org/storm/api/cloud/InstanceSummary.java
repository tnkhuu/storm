/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.cloud;

/**
 * A virtual managed node in the cloud.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public final class InstanceSummary
{
	/** The instance id. */
	private String id;

	/** The name. */
	private final String name;

	/** The address. */
	private final String address;

	/** The type. */
	private final String type;

	/**
	 * Instantiates a new node.
	 * 
	 * @param name
	 *            the name
	 * @param address
	 *            the address
	 * @param type
	 *            the type
	 */
	public InstanceSummary(String id, String name, String address, String state)
	{
		this.id = id;
		this.name = name;
		this.address = address;
		this.type = state;
	}

	/**
	 * Gets the id
	 * 
	 * @return the id
	 */
	public String getId()
	{
		return this.id;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the address.
	 * 
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	@Override
	public String toString()
	{
		return "InstanceSummary [id=" + id + ", name=" + name + ", address=" + address + ", type=" + type + "]";
	}

}
