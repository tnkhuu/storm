/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.cloud;

import org.msgpack.annotation.Message;
import org.msgpack.annotation.OrdinalEnum;


/**
 * Amazon Region name and domain addresses.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Message
@OrdinalEnum
public enum Region
{
	
	US_EAST_1("us-east-1","ec2.us-east-1.amazonaws.com", AvailabilityZone.builder(new String[]{"us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"})),
	US_EAST_2("us-east-2","ec2.us-east-2.amazonaws.com", AvailabilityZone.builder(new String[]{"us-east-2a", "us-east-2b", "us-east-2c", "us-east-2d"})),
	US_WEST_1("us-west-1","ec2.us-west-1.amazonaws.com", AvailabilityZone.builder(new String[]{"us-west-1a", "us-west-1b", "us-west-1c", "us-west-1d"})),
	US_WEST_2("us-west-2","ec2.us-west-2.amazonaws.com", AvailabilityZone.builder(new String[]{"us-west-2a", "us-west-2b", "us-west-2c", "us-west-2d"})),
	AP_NORTHEAST("ap-northeast-1","ec2.ap-northeast-1.amazonaws.com", AvailabilityZone.builder(new String[]{"ap-northeast-1a", "ap-northeast-1b"})),
	AP_SOUTHEAST_1("ap-southeast-1","ec2.ap-southeast-1.amazonaws.com", AvailabilityZone.builder(new String[]{"ap-southeast-1a","ap-southeast-1b"})),
	AP_SOUTHEAST_2("ap-southeast-2","ec2.ap-southeast-2.amazonaws.com", AvailabilityZone.builder(new String[]{"ap-southeast-2a","ap-southeast-2b"})),
	SA_EAST("sa-east-1","ec2.sa-east-1.amazonaws.com", AvailabilityZone.builder(new String[]{"sa-east-2a", "sa-east-2b"}));
	
	private final String name;
	private final String address;
	private final AvailabilityZone zones;
	
	/**
	 * Instantiates a new region.
	 *
	 * @param name the name
	 * @param address the address
	 */
	private Region(String name, String address, AvailabilityZone zone) {
		this.name = name;
		this.address = address;
		this.zones = zone;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @return the zones
	 */
	public AvailabilityZone getZones()
	{
		return zones;
	}
	
	
}
