/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.cloud;

import org.msgpack.annotation.Message;
import org.msgpack.annotation.OrdinalEnum;

/**
 * Supported CloudProviders
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Message
@OrdinalEnum
public enum CloudProvider
{

	/** An Amazon backed cloud */
	AMAZON("aws-ec2"),
	VIRTUALBOX("localhost"),
	GOOGLE("google"),
	RACKSPACE("rackspace"),
	GOGRID("gogrid"),
	CLOUDSTACK("cloudstack"),
	CLOUDSIGMA("cloudsigma"),
	CLOUDFILES("cloudfiles"),
	OPENSTACK("openstack");
	
	private String id;
	private CloudProvider(String id) {
		this.id = id;
	}
	
	public String getId()
	{
		return id;
	}
}
