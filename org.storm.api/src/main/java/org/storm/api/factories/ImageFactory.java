/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.factories;

import java.net.URL;
import java.util.concurrent.Future;

import org.msgpack.annotation.OrdinalEnum;
import org.storm.api.services.DistributedService;

/**
 * A factory for creating LXC Image distributions with storm integration.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface ImageFactory<T> extends DistributedService
{

	/**
	 * The Linux Distribution type.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	@OrdinalEnum
	public enum Distribution
	{

		/** The ubuntu. */
		UBUNTU,

		/** The debian. */
		DEBIAN,

		/** The fedora. */
		FEDORA,

		/** The centos. */
		CENTOS
	}

	/**
	 * Creates a new LXC tarball image using the given distribution and storm
	 * specification. The tarball image will be persisted to
	 * <code>imageLocation</code>
	 * 
	 * @param <T>
	 * 
	 * @param distributionType
	 *            the distribution type
	 * @param specification
	 *            the specification
	 * @param imageLocation
	 *            the image location
	 */
	Future<T> createImage(Distribution distributionType, URL specification, String imageLocation);
}
