/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.factories;

import java.util.Dictionary;
import java.util.Map;

import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.ServiceReference;

/**
 * A factory for creating ServiceFilter objects.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class FilterFactory
{

	/**
	 * Instantiates a new service filter factory.
	 */
	public FilterFactory()
	{

	}

	/**
	 * Creates a new ServiceFilter object.
	 * 
	 * @param iface
	 *            the iface
	 * @return the filter
	 */
	public static Filter createServiceFilter(Class<?> iface)
	{
		if (!iface.isInterface())
		{
			throw new IllegalArgumentException("The service " + iface + " is not an interface");
		}
		return new ServiceFilterImpl(iface);
	}
	
	public static Filter createServiceFilter(Class<?>... iface)
	{
		for (Class<?> c : iface)
		{
			if (!c.isInterface())
			{
				throw new IllegalArgumentException("The service " + iface + " is not an interface");
			}
		}
		return new ServiceFilterImpl(iface);
	}

	/**
	 * 
	 * @param iface
	 * @return
	 */
	public static String createServiceFilterString(Class<?> iface)
	{
		StringBuilder builder = new StringBuilder();
		builder.append("(objectClass=");
		builder.append(iface.getName());
		builder.append(")");
		return builder.toString();
	}

	/**
	 * The Class ServiceFilterImpl.
	 */
	public static class ServiceFilterImpl implements Filter
	{

		/** The service. */
		private final Class<?> service[];

		/**
		 * Instantiates a new service filter impl.
		 * 
		 * @param clazz
		 *            the clazz
		 */
		public ServiceFilterImpl(Class<?>... clazz)
		{
			this.service = clazz;
		}
		

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.osgi.framework.Filter#match(org.osgi.framework.ServiceReference)
		 */
		@Override
		public boolean match(ServiceReference<?> reference)
		{

			String[] classes = (String[]) reference.getProperty(Constants.OBJECTCLASS);
			return matchesClass(classes);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.osgi.framework.Filter#match(java.util.Dictionary)
		 */
		@Override
		public boolean match(Dictionary<String, ?> dictionary)
		{
			String[] classes = (String[]) dictionary.get(Constants.OBJECTCLASS);
			return matchesClass(classes);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.osgi.framework.Filter#matchCase(java.util.Dictionary)
		 */
		@Override
		public boolean matchCase(Dictionary<String, ?> dictionary)
		{
			return match(dictionary);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.osgi.framework.Filter#matches(java.util.Map)
		 */
		@Override
		public boolean matches(Map<String, ?> map)
		{
			String[] classes = (String[]) map.get(Constants.OBJECTCLASS);
			return matchesClass(classes);
		}

		/**
		 * Matches class.
		 * 
		 * @param classes
		 *            the classes
		 * @return true, if successful
		 */
		private boolean matchesClass(String[] classes)
		{
			boolean match = false;
			for (String iface : classes)
			{
				for(Class<?> c : service) {
					if (iface.equals(c.getName()))
					{
						match = true;
						break;
					}
				}
				
			}
			return match;
		}

	}
}
