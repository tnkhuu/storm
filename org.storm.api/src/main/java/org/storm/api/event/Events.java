/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.event;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

/**
 * The Generic Event Helper class.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Events
{

	/**
	 * Register callback.
	 * 
	 * @param eventChannel
	 *            the event channel
	 * @param context
	 *            the context
	 * @param handler
	 *            the handler
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void registerCallback(String eventChannel, BundleContext context, EventHandler handler)
	{
		String[] topics = new String[] { eventChannel };
		Dictionary props = new Hashtable();
		props.put(EventConstants.EVENT_TOPIC, topics);
		context.registerService(EventHandler.class.getName(), handler, props);
	}
}
