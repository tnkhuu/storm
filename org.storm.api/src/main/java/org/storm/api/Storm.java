/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api;

import java.util.concurrent.Future;

import org.storm.api.application.Application;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.exception.AppDeployException;
import org.storm.api.exception.VMProvisionException;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.nexus.api.Role;

/**
 * The Storm Platform Server.
 * 
 * This one of the core functional aspects of the storm platform. The Storm
 * implementation represents the outer layer of the storm framework. It
 * intercepts all incomming traffic, and maps an incomming request to the
 * appropiate application handler. Application handlers are mapped by sub domain
 * name.
 * 
 * In the case that a request is made to a subdomain which has a registered
 * application but no active instances, storm will provision the required
 * resources/instances as required on the fly.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Storm
{
	
	String APPLICATIONS = "/apps/";
	String APP_SEPERATOR = "/";
	String DOMAIN_SEPERATOR = "\\.";
	String NO_APP_EXISTS = "404.stormclowd.com";
	String APP_ERROR = "error.stormclowd.com";
	String STORM_HOME = "STORM_HOME";
	String STORM_ROLE = "STORM_ROLE";
	String VERSION = "kernel.version";
	String LVM_VG = "lxc";
	String STORM_DIST_TAR = "storm-cloud-1.0.0-lxc-rootfs.tar.gz";

	/**
	 * Resolve and deploy a application bound to the given domain.
	 *
	 * @param domain the domain
	 * @return the future
	 * @throws AppDeployException the app deploy exception
	 */
	Future<Application> resolve(String domain) throws AppDeployException;
	
	/**
	 * Provisions a new virtual machine for the cloud ensemble.
	 *
	 * @return the future
	 * @throws VMProvisionException the vM provision exception
	 */
	Future<VirtualMachine> provision(CloudProvider cloudProvider, Role role, HypervisorDriver driver, HypervisorType type) throws VMProvisionException;
}
