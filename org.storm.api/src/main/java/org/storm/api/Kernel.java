/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api;

import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.services.BlobStoreService;
import org.storm.api.services.BundleInstallService;
import org.storm.api.services.MetaDataService;
import org.storm.api.services.ReplicationService;
import org.storm.api.services.RepositoryService;
import org.storm.api.services.RoutingTableService;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.nexus.api.Role;

/**
 * The Storm Platform Kernel.
 * 
 * The barebone's minimum form of the Storm platform is it's kernel. From here,
 * it is able to dynamically grab and install the features required to perform
 * requested task, replicate itself across to other nodes, and launch the
 * appropriate back end wiring to form a cloud network. It also is responsible
 * for
 * 
 * - Managing the system internals of the node this kernel is running on. Takes
 * care of memory, disk, network and underlying system resources.
 * 
 * - Handling the life cycle of the platform, including the synchronization of
 * applications, versions and configuring the platform with the required purpose
 * a node was meant to serve.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Kernel
{

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	double getVersion();
	
	/**
	 * Return the kernels bundle context.
	 * 
	 * @return the kernels bundle context.
	 */
	BundleContext getBundleContext();

	/**
	 * Gets the Bundle Install Service
	 * 
	 * @return Bundle Install Service
	 */
	BundleInstallService getBundleInstallService();

	/**
	 * Gets the Bundle Install Service
	 * 
	 * @return the Bundle Install Service
	 */
	RepositoryService getRepositoryService();

	/**
	 * Gets the Replication Service.
	 * 
	 * @return the Replication Service.
	 */
	ReplicationService getReplicationService();
	
	/**
	 * Retrieve the kernel's routing ip table
	 * service. 
	 * @return  kernel's routing ip table service.
	 */
	RoutingTableService getRoutingTableService();
	
	/**
	 * Retrieve the OSGI event admin service
	 * 
	 * @return the OSGI event admin service
	 */
	EventAdmin getEventAdminService();
	
	/**
	 * 
	 * @param service
	 * @return
	 */
	<S> S getService(Class<S> service);
	
	/**
	 * 
	 * @return
	 */
	MetaDataService getMetaDataService();
	
	/**
	 * Register a vm to the kernel.
	 * 
	 * @param role
	 * @param virtualMachine
	 */
	void register(Role role, VirtualMachine virtualMachine);
	
	/**
	 * 
	 * @return
	 */
	CloudProvider getCloudProvider();
	
	/**
	 * 
	 * @return
	 */
	BlobStoreService<?> getBlobStore();

}