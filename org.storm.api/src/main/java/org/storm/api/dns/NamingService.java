/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.dns;

import org.storm.api.services.DistributedService;

/**
 * A DNS Client provided as a OSGI Service.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface NamingService extends DistributedService
{
	/**
	 * Max length of a DNS packet in bytes as detailed in the RFC Specification.
	 */
	int DNS_PACKET_SIZE = 512;

	/**
	 * Resolves host names to ip addresses.
	 * 
	 * @param hostname
	 *            the host name to resolve
	 * @return the 4 byte ip address as an byte array.
	 */
	byte[] resolve(String hostname);

	/**
	 * Resolve the host name as return the result in its compacted integer form
	 * 
	 * @param hostname
	 *            the hostname
	 * @return the int
	 */
	int resolveAsInt(String hostname);

	/**
	 * Resolve the host name as return the result in its dot seperated string
	 * form.
	 * 
	 * @param hostname
	 *            the hostname
	 * @return the string
	 */
	String resolveAsString(String hostname);
}
