/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.api.dns;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * The DNS Client
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface DNSAsyncClient<M, T>
{

	/**
	 * Send.
	 * 
	 * @param message
	 *            the message
	 * @param cb
	 *            the cb
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	void send(M message, MessageCallback<T> cb) throws IOException;
	
	/**
	 * Update.
	 * 
	 * @param zone
	 *            the zone
	 * @param domain
	 *            the domain
	 * @param ipAddress
	 *            the ip address
	 * @param cb
	 *            the cb
	 * @throws TextParseException
	 *             the text parse exception
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	void update(String zone, String domain, String ipAddress, MessageCallback<T> cb) throws  UnknownHostException;
	
	/**
	 * Question.
	 * 
	 * @param domain
	 *            the domain
	 * @param cb
	 *            the cb
	 * @throws TextParseException
	 *             the text parse exception
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	void question(String domain, MessageCallback<T> cb) throws  UnknownHostException;
	
	
	/**
	 * Performs a reverse dns lookup.
	 * 
	 * @param domain
	 *            the domain
	 * @param cb
	 *            the cb
	 * @throws TextParseException
	 *             the text parse exception
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	void reverseLookup(String ip, MessageCallback<T> cb) throws  UnknownHostException;
	
}
