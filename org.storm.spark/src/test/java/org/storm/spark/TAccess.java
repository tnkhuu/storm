package org.storm.spark;

import org.storm.spark.Spark;

public class TAccess {

    public static void clearRoutes() {
        Spark.clearRoutes();
    }
    
    public static void stop() {
    	Spark.stop();
    }
    
}
