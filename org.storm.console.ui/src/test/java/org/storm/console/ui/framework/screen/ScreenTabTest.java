/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.screen;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.screen.ScreenCharacterStyle;
import org.storm.console.ui.framework.screen.ScreenWriter;
import org.storm.console.ui.framework.screen.TabBehaviour;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalPosition;

/**
 * The Class ScreenTabTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ScreenTabTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	public static void main(String[] args) throws InterruptedException
	{
		new ScreenTabTest(args);
	}

	/** The screen. */
	private Screen screen;

	/**
	 * Instantiates a new screen tab test.
	 * 
	 * @param args
	 *            the args
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	public ScreenTabTest(String[] args) throws InterruptedException
	{
		screen = new TestTerminalFactory(args).createScreen();
		screen.startScreen();
		screen.setCursorPosition(new TerminalPosition(0, 0));
		drawStrings("Trying out some tabs!");

		long now = System.currentTimeMillis();
		while (System.currentTimeMillis() - now < 20 * 1000)
		{
			Thread.yield();
		}
		screen.stopScreen();
	}

	/**
	 * Draw strings.
	 * 
	 * @param topTitle
	 *            the top title
	 */
	private void drawStrings(String topTitle)
	{
		ScreenWriter writer = new ScreenWriter(screen);
		writer.setForegroundColor(Terminal.Color.DEFAULT);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.fillScreen(' ');

		writer.setForegroundColor(Terminal.Color.DEFAULT);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(0, 0, topTitle, ScreenCharacterStyle.BLINK);
		screen.setTabBehaviour(TabBehaviour.CONVERT_TO_ONE_SPACE);
		writer.drawString(10, 1, "TabBehaviour.CONVERT_TO_ONE_SPACE:    |\t|\t|\t|\t|");
		screen.setTabBehaviour(TabBehaviour.CONVERT_TO_FOUR_SPACES);
		writer.drawString(10, 2, "TabBehaviour.CONVERT_TO_FOUR_SPACES:  |\t|\t|\t|\t|");
		screen.setTabBehaviour(TabBehaviour.CONVERT_TO_EIGHT_SPACES);
		writer.drawString(10, 3, "TabBehaviour.CONVERT_TO_EIGHT_SPACES: |\t|\t|\t|\t|");
		screen.setTabBehaviour(TabBehaviour.ALIGN_TO_COLUMN_4);
		writer.drawString(10, 4, "TabBehaviour.ALIGN_TO_COLUMN_4:       |\t|\t|\t|\t|");
		screen.setTabBehaviour(TabBehaviour.ALIGN_TO_COLUMN_8);
		writer.drawString(10, 5, "TabBehaviour.ALIGN_TO_COLUMN_8:       |\t|\t|\t|\t|");

		screen.refresh();
	}
}
