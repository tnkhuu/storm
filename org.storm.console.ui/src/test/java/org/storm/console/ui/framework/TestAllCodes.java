/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

/**
 * The Class TestAllCodes.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TestAllCodes
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String[] args) throws Exception
	{
		System.out.write(new byte[] { (byte) 0x1B, 0x28, 0x30 });
		for (int i = 0; i < 200; i++)
		{
			System.out.write((i + " = " + (char) i + "\n").getBytes());
		}
		System.out.write(new byte[] { (byte) 0x1B, 0x28, 0x42 });
		// System.out.write(new byte[] { (byte)0x1B, (byte)0x21, (byte)0x40, 15
		// });
	}
}
