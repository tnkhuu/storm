/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.component.TextArea;
import org.storm.console.ui.framework.component.TextBox;
import org.storm.console.ui.framework.dialog.DialogButtons;
import org.storm.console.ui.framework.dialog.MessageBox;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class TextAreaTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TextAreaTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		final Window window1 = new Window("Text window");
		// window1.addComponent(new Widget(1, 1));

		final TextArea textArea = new TextArea("TextArea");
		textArea.setMaximumSize(new TerminalSize(80, 10));
		window1.addComponent(textArea);

		window1.addComponent(new EmptySpace(1, 1));

		Panel appendPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		((LinearLayout) appendPanel.getLayoutManager()).setPadding(1);
		final TextBox appendBox = new TextBox("", 30);
		Button appendButton = new Button("Append", new Action()
		{
			@Override
			public void doAction()
			{
				textArea.appendLine(appendBox.getText());
			}
		});
		appendPanel.addComponent(appendBox);
		appendPanel.addComponent(appendButton);
		window1.addComponent(appendPanel);

		Panel removePanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		((LinearLayout) removePanel.getLayoutManager()).setPadding(1);
		final TextBox removeBox = new TextBox("0", 5);
		Button removeButton = new Button("Remove line", new Action()
		{
			@Override
			public void doAction()
			{
				try
				{
					int lineNumber = Integer.parseInt(removeBox.getText());
					textArea.removeLine(lineNumber);
				}
				catch (Exception e)
				{
					MessageBox.showMessageBox(guiScreen, e.getClass().getSimpleName(), e.getMessage(), DialogButtons.OK);
				}
			}
		});
		Button clearButton = new Button("Clear text", new Action()
		{
			@Override
			public void doAction()
			{
				textArea.clear();
			}
		});
		removePanel.addComponent(removeBox);
		removePanel.addComponent(removeButton);
		removePanel.addComponent(clearButton);
		window1.addComponent(removePanel);

		window1.addComponent(new EmptySpace(1, 1));

		Panel lastPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		lastPanel.addComponent(new EmptySpace(1, 1), LinearLayout.GROWS_HORIZONTALLY);
		lastPanel.addComponent(exitButton);
		window1.addComponent(lastPanel);
		guiScreen.showWindow(window1, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}
}
