/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.terminal.Terminal;

/**
 * The Class BlinkTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BlinkTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Terminal rawTerminal = new TestTerminalFactory(args).createTerminal();
		rawTerminal.enterPrivateMode();
		rawTerminal.clearScreen();
		rawTerminal.applyForegroundColor(Terminal.Color.RED);
		rawTerminal.applySGR(Terminal.SGR.ENTER_BLINK);
		rawTerminal.moveCursor(10, 10);
		rawTerminal.putCharacter('H');
		rawTerminal.putCharacter('e');
		rawTerminal.putCharacter('l');
		rawTerminal.putCharacter('l');
		rawTerminal.putCharacter('o');
		rawTerminal.putCharacter('!');
		rawTerminal.moveCursor(0, 0);
		rawTerminal.flush();
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
		}
		rawTerminal.exitPrivateMode();
	}
}
