/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.gui;

import java.nio.charset.Charset;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.dialog.MessageBox;
import org.storm.console.ui.framework.impl.DefaultBackgroundRenderer;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.text.UnixTerminal;

/**
 * The Class ShortcutTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ShortcutTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Terminal terminal = new TestTerminalFactory(args).createTerminal();
		if (terminal instanceof UnixTerminal)
		{
			terminal = new UnixTerminal(System.in, System.out, Charset.forName("UTF-8"), null, UnixTerminal.Behaviour.DEFAULT);
		}
		final GUIScreen guiScreen = new GUIScreen(new Screen(terminal));
		guiScreen.getScreen().startScreen();
		guiScreen.setBackgroundRenderer(new DefaultBackgroundRenderer("GUI Test"));

		final Window mainWindow = new Window("Window with panels");
		mainWindow.addComponent(new Label("Shortcuts to try:"));
		mainWindow.addComponent(new Label("m"));
		mainWindow.addComponent(new Label("ctrl + c"));
		mainWindow.addComponent(new Label("alt + v"));
		mainWindow.addComponent(new Label("ctrl + alt + x"));
		Panel buttonPanel = new Panel(Panel.Orientation.HORIZONTAL);
		Button button1 = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				mainWindow.close();
			}
		});
		button1.setAlignment(Component.Alignment.CENTER);
		buttonPanel.addComponent(button1, LinearLayout.GROWS_HORIZONTALLY);
		buttonPanel.addShortcut(Kind.Home, new Action()
		{
			@Override
			public void doAction()
			{
				MessageBox.showMessageBox(guiScreen, "Shortcut triggered", "You triggered a shortcut by pressing home!");
			}
		});
		buttonPanel.addShortcut('m', false, false, new Action()
		{
			@Override
			public void doAction()
			{
				MessageBox.showMessageBox(guiScreen, "Shortcut triggered", "You triggered a shortcut by pressing 'm'!");
			}
		});
		buttonPanel.addShortcut('c', true, false, new Action()
		{
			@Override
			public void doAction()
			{
				MessageBox.showMessageBox(guiScreen, "Shortcut triggered", "You triggered a shortcut by pressing ctrl+c!");
			}
		});
		buttonPanel.addShortcut('v', false, true, new Action()
		{
			@Override
			public void doAction()
			{
				MessageBox.showMessageBox(guiScreen, "Shortcut triggered", "You triggered a shortcut by pressing alt+v!");
			}
		});
		buttonPanel.addShortcut('x', true, true, new Action()
		{
			@Override
			public void doAction()
			{
				MessageBox.showMessageBox(guiScreen, "Shortcut triggered", "You triggered a shortcut by pressing ctrl+alt+x!");
			}
		});
		mainWindow.addComponent(buttonPanel, LinearLayout.GROWS_HORIZONTALLY);

		guiScreen.showWindow(mainWindow, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}
}
