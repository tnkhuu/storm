/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import org.storm.console.ui.framework.TerminalFacade;
import org.storm.console.ui.framework.terminal.ACS;
import org.storm.console.ui.framework.terminal.Terminal;

/**
 * The Class SwingTerminalTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SwingTerminalTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	public static void main(String[] args) throws InterruptedException
	{
		Terminal terminal = TerminalFacade.createSwingTerminal();
		terminal.enterPrivateMode();
		terminal.clearScreen();
		terminal.moveCursor(10, 5);
		terminal.putCharacter('H');
		terminal.putCharacter('e');
		terminal.putCharacter('l');
		terminal.putCharacter('l');
		terminal.putCharacter('o');
		terminal.putCharacter('!');
		terminal.putCharacter(' ');
		terminal.putCharacter(ACS.HEART);
		terminal.putCharacter(ACS.SPADES);
		terminal.putCharacter(ACS.CLUB);
		terminal.putCharacter(ACS.DIAMOND);
		terminal.putCharacter(ACS.DOUBLE_LINE_CROSS);
		terminal.putCharacter(ACS.SINGLE_LINE_CROSS);
		terminal.putCharacter(ACS.DOUBLE_LINE_T_DOWN);
		terminal.putCharacter(ACS.SINGLE_LINE_VERTICAL);
		terminal.putCharacter(ACS.SINGLE_LINE_HORIZONTAL);
		terminal.moveCursor(10, 7);
		terminal.applySGR(Terminal.SGR.ENTER_BOLD);
		terminal.putCharacter('H');
		terminal.putCharacter('e');
		terminal.putCharacter('l');
		terminal.putCharacter('l');
		terminal.putCharacter('o');
		terminal.putCharacter('!');
		terminal.putCharacter(' ');
		terminal.putCharacter(ACS.HEART);
		terminal.putCharacter(ACS.SPADES);
		terminal.putCharacter(ACS.CLUB);
		terminal.putCharacter(ACS.DIAMOND);
		terminal.putCharacter(ACS.DOUBLE_LINE_CROSS);
		terminal.putCharacter(ACS.SINGLE_LINE_CROSS);
		terminal.putCharacter(ACS.DOUBLE_LINE_T_DOWN);
		terminal.putCharacter(ACS.SINGLE_LINE_VERTICAL);
		terminal.putCharacter(ACS.SINGLE_LINE_HORIZONTAL);
		terminal.moveCursor(10, 9);
		terminal.applySGR(Terminal.SGR.ENTER_UNDERLINE);
		terminal.putCharacter('H');
		terminal.putCharacter('e');
		terminal.applySGR(Terminal.SGR.EXIT_BOLD);
		terminal.putCharacter('l');
		terminal.applySGR(Terminal.SGR.EXIT_UNDERLINE);
		terminal.putCharacter('l');
		terminal.putCharacter('o');
		terminal.applySGR(Terminal.SGR.ENTER_UNDERLINE);
		terminal.putCharacter('!');
		terminal.putCharacter(' ');
		terminal.putCharacter(ACS.HEART);
		terminal.putCharacter(ACS.SPADES);
		terminal.putCharacter(ACS.CLUB);
		terminal.putCharacter(ACS.DIAMOND);
		terminal.putCharacter(ACS.DOUBLE_LINE_CROSS);
		terminal.putCharacter(ACS.SINGLE_LINE_CROSS);
		terminal.putCharacter(ACS.DOUBLE_LINE_T_DOWN);
		terminal.putCharacter(ACS.SINGLE_LINE_VERTICAL);
		terminal.putCharacter(ACS.SINGLE_LINE_HORIZONTAL);
		terminal.applySGR(Terminal.SGR.RESET_ALL);
		terminal.moveCursor(0, 0);

		Thread.sleep(5000);
		terminal.exitPrivateMode();
	}
}
