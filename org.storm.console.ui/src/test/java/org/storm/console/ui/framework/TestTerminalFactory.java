/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TerminalFacade;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.terminal.Terminal;

/**
 * A factory for creating TestTerminal objects.
 */
public class TestTerminalFactory
{

	/** The force unix terminal. */
	private final boolean forceUnixTerminal;

	/**
	 * Instantiates a new test terminal factory.
	 * 
	 * @param args
	 *            the args
	 */
	public TestTerminalFactory(String[] args)
	{
		forceUnixTerminal = args.length > 0 && "--no-swing".equals(args[0]);
	}

	/**
	 * Creates a new TestTerminal object.
	 * 
	 * @return the terminal
	 */
	public Terminal createTerminal()
	{
		if (forceUnixTerminal)
		{
			return TerminalFacade.createTextTerminal();
		}
		else
		{
			return TerminalFacade.createTerminal();
		}
	}

	/**
	 * Creates a new TestTerminal object.
	 * 
	 * @return the screen
	 */
	public Screen createScreen()
	{
		return TerminalFacade.createScreen(createTerminal());
	}

	/**
	 * Creates a new TestTerminal object.
	 * 
	 * @return the GUI screen
	 */
	public GUIScreen createGUIScreen()
	{
		return TerminalFacade.createGUIScreen(createScreen());
	}
}
