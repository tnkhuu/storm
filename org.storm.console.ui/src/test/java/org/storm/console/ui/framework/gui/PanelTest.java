/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.AbstractComponent;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.impl.DefaultBackgroundRenderer;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class PanelTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class PanelTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		guiScreen.setBackgroundRenderer(new DefaultBackgroundRenderer("GUI Test"));
		final Window mainWindow = new Window("Window with panels");
		TextFillComponent oneComponent = new TextFillComponent(5, 5, '1');
		TextFillComponent xComponent = new TextFillComponent(5, 5, 'X');
		TextFillComponent twoComponent = new TextFillComponent(5, 5, '2');
		Panel componentPanel = new Panel(Panel.Orientation.VERTICAL);
		componentPanel.addComponent(oneComponent);
		componentPanel.addComponent(xComponent, LinearLayout.MAXIMIZES_VERTICALLY);
		componentPanel.addComponent(twoComponent);
		mainWindow.addComponent(componentPanel, LinearLayout.MAXIMIZES_VERTICALLY);
		mainWindow.addComponent(new Button("Close", new Action()
		{
			@Override
			public void doAction()
			{
				mainWindow.close();
			}
		}));

		guiScreen.showWindow(mainWindow, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}

	/**
	 * The Class TextFillComponent.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class TextFillComponent extends AbstractComponent
	{

		/** The preferred size. */
		private final TerminalSize preferredSize;

		/** The fill character. */
		private final char fillCharacter;

		/**
		 * Instantiates a new text fill component.
		 * 
		 * @param width
		 *            the width
		 * @param height
		 *            the height
		 * @param fillCharacter
		 *            the fill character
		 */
		public TextFillComponent(int width, int height, char fillCharacter)
		{
			this.preferredSize = new TerminalSize(width, height);
			this.fillCharacter = fillCharacter;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
		 * calculatePreferredSize()
		 */
		@Override
		public TerminalSize calculatePreferredSize()
		{
			return new TerminalSize(preferredSize);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.component.AbstractComponent#isScrollable
		 * ()
		 */
		@Override
		public boolean isScrollable()
		{
			return true;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
		 * .gui.TextGraphics)
		 */
		@Override
		public void repaint(TextGraphics graphics)
		{
			StringBuilder sb = new StringBuilder();
			graphics.applyTheme(Category.DIALOG_AREA);
			for (int i = 0; i < graphics.getWidth(); i++)
			{
				sb.append(fillCharacter);
			}
			for (int i = 0; i < graphics.getHeight(); i++)
			{
				graphics.drawString(0, i, sb.toString());
			}
		}
	}
}
