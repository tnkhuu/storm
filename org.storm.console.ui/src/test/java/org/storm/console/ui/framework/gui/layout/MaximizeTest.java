/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.gui.layout;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.impl.DefaultBackgroundRenderer;
import org.storm.console.ui.framework.layout.LinearLayout;

/**
 * The Class MaximizeTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MaximizeTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		guiScreen.setBackgroundRenderer(new DefaultBackgroundRenderer("GUI Test"));

		final Window mainWindow = new Window("Window");

		Label maximizedLabel = new Label("This label is taking up all horizontal space in the window");
		Panel horizontalPanel = new Panel(Panel.Orientation.HORIZONTAL);
		horizontalPanel.addComponent(maximizedLabel, LinearLayout.MAXIMIZES_HORIZONTALLY);
		mainWindow.addComponent(horizontalPanel);

		Panel buttonPanel = new Panel(Panel.Orientation.HORIZONTAL);
		buttonPanel.addComponent(new EmptySpace(), LinearLayout.GROWS_HORIZONTALLY);
		Button button1 = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				mainWindow.close();
			}
		});
		buttonPanel.addComponent(button1);
		mainWindow.addComponent(buttonPanel);

		guiScreen.showWindow(mainWindow, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}
}
