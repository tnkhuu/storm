/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.terminal.ACS;

/**
 * The Class TestACS.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TestACS
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("FACE_WHITE = " + ACS.FACE_WHITE);
		System.out.println("FACE_BLACK = " + ACS.FACE_BLACK);
		System.out.println("HEART = " + ACS.HEART);
		System.out.println("CLUB = " + ACS.CLUB);
		System.out.println("DIAMOND = " + ACS.DIAMOND);
		System.out.println("SPADES = " + ACS.SPADES);
		System.out.println("DOT = " + ACS.DOT);
		System.out.println();
		System.out.println("ARROW_UP = " + ACS.ARROW_UP);
		System.out.println("ARROW_DOWN = " + ACS.ARROW_DOWN);
		System.out.println("ARROW_RIGHT = " + ACS.ARROW_RIGHT);
		System.out.println("ARROW_LEFT = " + ACS.ARROW_LEFT);
		System.out.println();
		System.out.println("BLOCK_SOLID = " + ACS.BLOCK_SOLID);
		System.out.println("BLOCK_DENSE = " + ACS.BLOCK_DENSE);
		System.out.println("BLOCK_MIDDLE = " + ACS.BLOCK_MIDDLE);
		System.out.println("BLOCK_SPARSE = " + ACS.BLOCK_SPARSE);
		System.out.println();
		System.out.println("SINGLE_LINE_HORIZONTAL = " + ACS.SINGLE_LINE_HORIZONTAL);
		System.out.println("DOUBLE_LINE_HORIZONTAL = " + ACS.DOUBLE_LINE_HORIZONTAL);
		System.out.println("SINGLE_LINE_VERTICAL = " + ACS.SINGLE_LINE_VERTICAL);
		System.out.println("DOUBLE_LINE_VERTICAL = " + ACS.DOUBLE_LINE_VERTICAL);
		System.out.println();
		System.out.println("SINGLE_LINE_UP_LEFT_CORNER = " + ACS.SINGLE_LINE_UP_LEFT_CORNER);
		System.out.println("DOUBLE_LINE_UP_LEFT_CORNER = " + ACS.DOUBLE_LINE_UP_LEFT_CORNER);
		System.out.println("SINGLE_LINE_UP_RIGHT_CORNER = " + ACS.SINGLE_LINE_UP_RIGHT_CORNER);
		System.out.println("DOUBLE_LINE_UP_RIGHT_CORNER = " + ACS.DOUBLE_LINE_UP_RIGHT_CORNER);
		System.out.println();
		System.out.println("SINGLE_LINE_LOW_LEFT_CORNER = " + ACS.SINGLE_LINE_LOW_LEFT_CORNER);
		System.out.println("DOUBLE_LINE_LOW_LEFT_CORNER = " + ACS.DOUBLE_LINE_LOW_LEFT_CORNER);
		System.out.println("SINGLE_LINE_LOW_RIGHT_CORNER = " + ACS.SINGLE_LINE_LOW_RIGHT_CORNER);
		System.out.println("DOUBLE_LINE_LOW_RIGHT_CORNER = " + ACS.DOUBLE_LINE_LOW_RIGHT_CORNER);
		System.out.println();
		System.out.println("SINGLE_LINE_CROSS = " + ACS.SINGLE_LINE_CROSS);
		System.out.println("DOUBLE_LINE_CROSS = " + ACS.DOUBLE_LINE_CROSS);
		System.out.println();
		System.out.println("SINGLE_LINE_T_UP = " + ACS.SINGLE_LINE_T_UP);
		System.out.println("SINGLE_LINE_T_DOWN = " + ACS.SINGLE_LINE_T_DOWN);
		System.out.println("SINGLE_LINE_T_RIGHT = " + ACS.SINGLE_LINE_T_RIGHT);
		System.out.println("SINGLE_LINE_T_LEFT = " + ACS.SINGLE_LINE_T_LEFT);
		System.out.println();
		System.out.println("SINGLE_LINE_T_DOUBLE_UP = " + ACS.SINGLE_LINE_T_DOUBLE_UP);
		System.out.println("SINGLE_LINE_T_DOUBLE_DOWN = " + ACS.SINGLE_LINE_T_DOUBLE_DOWN);
		System.out.println("SINGLE_LINE_T_DOUBLE_RIGHT = " + ACS.SINGLE_LINE_T_DOUBLE_RIGHT);
		System.out.println("SINGLE_LINE_T_DOUBLE_LEFT = " + ACS.SINGLE_LINE_T_DOUBLE_LEFT);
		System.out.println();
		System.out.println("DOUBLE_LINE_T_UP = " + ACS.DOUBLE_LINE_T_UP);
		System.out.println("DOUBLE_LINE_T_DOWN = " + ACS.DOUBLE_LINE_T_DOWN);
		System.out.println("DOUBLE_LINE_T_RIGHT = " + ACS.DOUBLE_LINE_T_RIGHT);
		System.out.println("DOUBLE_LINE_T_LEFT = " + ACS.DOUBLE_LINE_T_LEFT);
		System.out.println();
		System.out.println("DOUBLE_LINE_T_SINGLE_UP = " + ACS.DOUBLE_LINE_T_SINGLE_UP);
		System.out.println("DOUBLE_LINE_T_SINGLE_DOWN = " + ACS.DOUBLE_LINE_T_SINGLE_DOWN);
		System.out.println("DOUBLE_LINE_T_SINGLE_RIGHT = " + ACS.DOUBLE_LINE_T_SINGLE_RIGHT);
		System.out.println("DOUBLE_LINE_T_SINGLE_LEFT = " + ACS.DOUBLE_LINE_T_SINGLE_LEFT);
	}
}
