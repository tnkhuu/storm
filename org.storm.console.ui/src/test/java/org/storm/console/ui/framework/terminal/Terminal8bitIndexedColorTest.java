/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import java.util.Random;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalSize;
import org.storm.console.ui.framework.terminal.XTerm8bitIndexedColorUtils;

/**
 * The Class Terminal8bitIndexedColorTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Terminal8bitIndexedColorTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final String string = "Hello!";
		Random random = new Random();
		Terminal terminal = new TestTerminalFactory(args).createTerminal();
		terminal.enterPrivateMode();
		terminal.clearScreen();
		TerminalSize size = terminal.getTerminalSize();

		while (true)
		{
			if (terminal.readInput() != null)
			{
				terminal.exitPrivateMode();
				return;
			}

			int foregroundIndex = XTerm8bitIndexedColorUtils.getClosestColor(random.nextInt(255), random.nextInt(255), random.nextInt(255));
			int backgroundIndex = XTerm8bitIndexedColorUtils.getClosestColor(random.nextInt(255), random.nextInt(255), random.nextInt(255));

			terminal.applyForegroundColor(foregroundIndex);
			terminal.applyBackgroundColor(backgroundIndex);
			terminal.moveCursor(random.nextInt(size.getColumns() - string.length()), random.nextInt(size.getRows()));
			printString(terminal, string);

			try
			{
				Thread.sleep(200);
			}
			catch (InterruptedException e)
			{
			}
		}
	}

	/**
	 * Prints the string.
	 * 
	 * @param terminal
	 *            the terminal
	 * @param string
	 *            the string
	 */
	private static void printString(Terminal terminal, String string)
	{
		for (int i = 0; i < string.length(); i++)
		{
			terminal.putCharacter(string.charAt(i));
		}
	}
}
