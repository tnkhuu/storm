/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.gui;

import java.util.Random;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.CheckBox;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.component.ProgressBar;
import org.storm.console.ui.framework.component.Table;
import org.storm.console.ui.framework.component.TextBox;
import org.storm.console.ui.framework.layout.LinearLayout;

/**
 * The Class TableTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TableTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		final Window window1 = new Window("Text box window");

		final Table table = new Table(5, "My Test Table");
		table.setColumnPaddingSize(1);
		table.addRow(new Label("Column 1 "), new Label("Column 2 "), new Label("Column 3 "), new Label("Column 4 "), new Label("Column 5"));
		table.addRow(new TextBox("Here's a text box"), new CheckBox("checkbox", false), new ProgressBar(10), new EmptySpace(), new Button("Progress", new Action()
		{
			@Override
			public void doAction()
			{
				((ProgressBar) table.getRow(1)[2]).setProgress(new Random().nextDouble());
			}
		}));

		window1.addComponent(table);
		Panel buttonPanel = new Panel(Panel.Orientation.HORIZONTAL);
		buttonPanel.addComponent(new EmptySpace(1, 1), LinearLayout.MAXIMIZES_HORIZONTALLY);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		buttonPanel.addComponent(exitButton);
		window1.addComponent(buttonPanel);
		guiScreen.showWindow(window1, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}
}
