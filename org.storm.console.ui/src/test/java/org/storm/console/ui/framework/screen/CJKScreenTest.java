/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.screen;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.screen.ScreenWriter;
import org.storm.console.ui.framework.terminal.Terminal;

/**
 * The Class CJKScreenTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CJKScreenTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Screen screen = new TestTerminalFactory(args).createScreen();
		screen.startScreen();

		ScreenWriter writer = new ScreenWriter(screen);
		writer.setForegroundColor(Terminal.Color.DEFAULT);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(5, 5, "Chinese (simplified):  斯瓦尔巴群岛是位于北极地区的群岛，为挪威最北界的国土范围。");
		writer.drawString(5, 7, "Chinese (traditional): 斯瓦巴群島是位於北極地區的群島，為挪威最北界的國土範圍。");
		writer.drawString(5, 9, "Japanese:              スヴァールバル諸島は、北極圏のバレンツ海にある群島。");
		writer.drawString(5, 11, "Korean:                스발바르 제도 는 유럽 본토의 북부, 대서양에 위치한 군도이다.");
		screen.refresh();

		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
		}
		screen.stopScreen();
	}
}
