/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.screen;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.screen.ScreenWriter;

/**
 * The Class TerminalTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TerminalTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Screen screen = new TestTerminalFactory(args).createScreen();
		ScreenWriter writer = new ScreenWriter(screen);
		screen.startScreen();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "Hello world!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, " ello world!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "  llo world!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "   lo world!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "    o world!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "      world!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "       orld!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "        rld!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "         ld!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "          d!");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "           !");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		writer.drawString(10, 10, "            ");
		screen.refresh();
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
		}
		screen.stopScreen();
	}
}
