package org.storm.console.ui.framework.table;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.BLACK;
import static org.fusesource.jansi.Ansi.Color.BLUE;
import static org.fusesource.jansi.Ansi.Color.CYAN;
import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.MAGENTA;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.Color.WHITE;
import static org.fusesource.jansi.Ansi.Color.YELLOW;

import org.fusesource.jansi.Ansi.Color;
import org.junit.Test;
import org.storm.console.ui.framework.component.ASCIITable;
import org.storm.console.ui.framework.component.ASCIITableFactory;
import org.storm.console.ui.framework.component.ASCIITableHeader;

public class TableTest
{
	private Color[] colors = new Color[]{BLACK,BLUE,RED,MAGENTA,CYAN, YELLOW, WHITE,GREEN};
	@Test
	public void basicTests() throws InterruptedException
	{
		System.out.println("Testing ASCII tables and Dynmic Color Changes:");
		String[][] data = {
				{
						"Trung", "2000", "Developer", "#99, All your bases belong to me", "1111" }, {
						"Khuu", "12000", "Developer", "No me", "22222" }, {
						"Adriana", "42000", "Lead", "#66, Ha.", "333333" }, {
						"Kim", "132000", "QA", "#22, Bleh", "4444444" }, {
						"Tony", "62000", "Developer", "#3-3, My Gawd" }, {
						"Andrew", "2000", "Manager" }, {
						"Mo", "62000" }, {
					"Anne With Some Uber Name" }, };

		ASCIITableHeader[] headerObjs = {
				new ASCIITableHeader("User Name", ASCIITable.ALIGN_LEFT), new ASCIITableHeader("Salary"), new ASCIITableHeader("Designation", ASCIITable.ALIGN_CENTER),
				new ASCIITableHeader("Address", ASCIITable.ALIGN_RIGHT), new ASCIITableHeader("Lucky#", ASCIITable.ALIGN_LEFT), };

		ASCIITable table = ASCIITableFactory.createTable(headerObjs, data);
		
		table.printTable();
		for (int i = 0; i < colors.length; i++)
		{
			table.setColor(getColor(i));
			System.out.print(ansi().cursorLeft(table.getWidth()).cursorUp(table.getLength() + 1));
			table.printTable();
			Thread.sleep(1000);
		}
		// TableFactory.createTable(header, data).printTable();

	}
	
	public Color getColor(int i) {
		return colors[i % colors.length];
	}

}
