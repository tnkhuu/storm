/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * The Class TestShellCommand.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TestShellCommand
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String[] args) throws Exception
	{
		ProcessBuilder pb = new ProcessBuilder(args);
		Process process = pb.start();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream stdout = process.getInputStream();
		int readByte = stdout.read();
		while (readByte >= 0)
		{
			baos.write(readByte);
			readByte = stdout.read();
		}
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		BufferedReader reader = new BufferedReader(new InputStreamReader(bais));
		StringBuilder builder = new StringBuilder();
		while (reader.ready())
		{
			builder.append(reader.readLine());
		}
		reader.close();
		System.out.println(builder.toString());
	}
}
