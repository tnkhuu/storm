/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.terminal.Terminal;

/**
 * The Class RawTerminalTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RawTerminalTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	public static void main(String[] args) throws InterruptedException
	{
		Terminal terminal = new TestTerminalFactory(args).createTerminal();
		terminal.enterPrivateMode();
		terminal.clearScreen();
		terminal.moveCursor(10, 5);
		terminal.putCharacter('H');
		terminal.putCharacter('e');
		terminal.putCharacter('l');
		terminal.putCharacter('l');
		terminal.putCharacter('o');
		terminal.putCharacter('!');
		terminal.moveCursor(0, 0);

		Thread.sleep(5000);
		terminal.exitPrivateMode();
	}
}
