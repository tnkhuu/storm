/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;

/**
 * The Class FullScreenWindowTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class FullScreenWindowTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		final Window window1 = new Window("Full screen window");
		window1.setBorder(new Border.Invisible());

		window1.addComponent(new EmptySpace(1, 10));
		window1.addComponent(new Label("Fullscreen window"));
		window1.addComponent(new EmptySpace(1, 10));

		Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		buttonPanel.addComponent(new EmptySpace(50, 1));
		buttonPanel.addComponent(exitButton);
		window1.addComponent(buttonPanel);
		guiScreen.showWindow(window1, GUIScreen.Position.FULL_SCREEN);
		guiScreen.getScreen().stopScreen();
	}
}
