/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.component.RadioCheckBoxList;

/**
 * The Class RadioCheckBoxTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RadioCheckBoxTest
{

	/** The cancel thread. */
	public static boolean cancelThread = false;

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();

		final Window window1 = new Window("List box window");
		// window1.addComponent(new Widget(1, 1));

		Panel mainPanel = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		final RadioCheckBoxList listBox = new RadioCheckBoxList();

		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				for (int i = 0; i < 30; i++)
				{
					try
					{
						Thread.sleep(250);
					}
					catch (InterruptedException e)
					{
					}
					final Integer count = i + 1;
					guiScreen.runInEventThread(new Action()
					{
						@Override
						public void doAction()
						{
							listBox.addItem("Item #" + count.intValue());
						}
					});
					if (cancelThread)
					{
						break;
					}
				}
			}
		};

		mainPanel.addComponent(listBox);
		window1.addComponent(mainPanel);

		Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		buttonPanel.addComponent(new EmptySpace(20, 1));
		buttonPanel.addComponent(exitButton);
		window1.addComponent(buttonPanel);
		thread.start();
		guiScreen.showWindow(window1, GUIScreen.Position.CENTER);
		cancelThread = true;
		guiScreen.getScreen().stopScreen();
	}
}
