/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.ActionListBox;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.dialog.MessageBox;

/**
 * The Class ActionListTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ActionListTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		final Window window1 = new Window("Text box window");
		// window1.addComponent(new Widget(1, 1));

		Panel mainPanel = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		ActionListBox actionListBox = new ActionListBox();
		for (int i = 0; i < 5; i++)
		{
			actionListBox.addAction(new ActionListBoxItem(guiScreen));
		}

		mainPanel.addComponent(actionListBox);
		window1.addComponent(mainPanel);

		Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		buttonPanel.addComponent(new EmptySpace(20, 1));
		buttonPanel.addComponent(exitButton);
		window1.addComponent(buttonPanel);
		guiScreen.showWindow(window1, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}

	/**
	 * The Class ActionListBoxItem.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class ActionListBoxItem implements Action
	{

		/** The counter. */
		private static int counter = 1;

		/** The owner. */
		private GUIScreen owner;

		/** The nr. */
		private int nr;

		/**
		 * Instantiates a new action list box item.
		 * 
		 * @param owner
		 *            the owner
		 */
		public ActionListBoxItem(GUIScreen owner)
		{
			this.nr = counter++;
			this.owner = owner;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return "ActionListBox item #" + nr;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Action#doAction()
		 */
		@Override
		public void doAction()
		{
			MessageBox.showMessageBox(owner, "Action", "Selected " + toString());
		}
	}
}

class MyWindow extends Window
{
	public MyWindow()
	{
		super("My Window!");
		Panel horisontalPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Panel leftPanel = new Panel(new Border.Bevel(true), Panel.Orientation.HORIZONTAL);
		Panel middlePanel = new Panel(new Border.Bevel(true), Panel.Orientation.HORIZONTAL);
		Panel rightPanel = new Panel(new Border.Bevel(true), Panel.Orientation.HORIZONTAL);

		horisontalPanel.addComponent(leftPanel);
		horisontalPanel.addComponent(middlePanel);
		horisontalPanel.addComponent(rightPanel);

		addComponent(horisontalPanel);
	}
}
