/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import java.nio.charset.Charset;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.component.AbstractComponent;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.impl.DefaultBackgroundRenderer;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalSize;
import org.storm.console.ui.framework.terminal.text.UnixTerminal;

/**
 * The Class ButtonTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ButtonTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Terminal terminal = new TestTerminalFactory(args).createTerminal();
		if (terminal instanceof UnixTerminal)
		{
			terminal = new UnixTerminal(System.in, System.out, Charset.forName("UTF-8"), null, UnixTerminal.Behaviour.CTRL_C_KILLS_APPLICATION);
		}
		final GUIScreen guiScreen = new GUIScreen(new Screen(terminal));
		guiScreen.getScreen().startScreen();
		guiScreen.setBackgroundRenderer(new DefaultBackgroundRenderer("GUI Test"));

		final Window mainWindow = new Window("Window with panels");
		mainWindow.addComponent(new AbstractComponent()
		{
			@Override
			public void repaint(TextGraphics graphics)
			{
				graphics.applyTheme(graphics.getTheme().getDefinition(Category.SHADOW));
				for (int y = 0; y < graphics.getHeight(); y++)
				{
					for (int x = 0; x < graphics.getWidth(); x++)
					{
						graphics.drawString(x, y, "X");
					}
				}
			}

			@Override
			protected TerminalSize calculatePreferredSize()
			{
				return new TerminalSize(20, 6);
			}
		});
		Panel buttonPanel = new Panel(Panel.Orientation.HORIZONTAL);
		Button button1 = new Button("Button1", new Action()
		{
			@Override
			public void doAction()
			{
				mainWindow.close();
			}
		});
		Button button2 = new Button("Button2");
		Button button3 = new Button("Button3");
		buttonPanel.addComponent(button1);
		buttonPanel.addComponent(button2);
		buttonPanel.addComponent(button3);
		mainWindow.addComponent(buttonPanel);

		guiScreen.showWindow(mainWindow, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}
}
