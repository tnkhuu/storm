/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.ActionListBox;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.CheckBoxList;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.component.RadioCheckBoxList;
import org.storm.console.ui.framework.dialog.MessageBox;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class DifferentKindsOfListBoxesTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DifferentKindsOfListBoxesTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();

		final Window window1 = new Window("List boxes window");

		Panel mainPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		((LinearLayout) mainPanel.getLayoutManager()).setPadding(1);

		final CheckBoxList checkBoxList = new CheckBoxList();
		checkBoxList.addItem("First");
		checkBoxList.addItem("Second");
		checkBoxList.addItem("Third");
		checkBoxList.addItem("Fourth");
		checkBoxList.addItem("Fifth");
		checkBoxList.addItem("Första");
		checkBoxList.addItem("Andra");
		checkBoxList.addItem("Tredje");
		checkBoxList.addItem("Fjärde");
		checkBoxList.addItem("Femte");
		checkBoxList.addItem("Ichi");
		checkBoxList.addItem("Ni");
		checkBoxList.addItem("San");
		checkBoxList.addItem("Yon");
		checkBoxList.addItem("Go");
		checkBoxList.setPreferredSize(new TerminalSize(16, 8));

		RadioCheckBoxList radioCheckBoxList = new RadioCheckBoxList();
		radioCheckBoxList.addItem("First");
		radioCheckBoxList.addItem("Second");
		radioCheckBoxList.addItem("Third");
		radioCheckBoxList.addItem("Fourth");
		radioCheckBoxList.addItem("Fifth");
		radioCheckBoxList.addItem("Första");
		radioCheckBoxList.addItem("Andra");
		radioCheckBoxList.addItem("Tredje");
		radioCheckBoxList.addItem("Fjärde");
		radioCheckBoxList.addItem("Femte");
		radioCheckBoxList.addItem("Ichi");
		radioCheckBoxList.addItem("Ni");
		radioCheckBoxList.addItem("San");
		radioCheckBoxList.addItem("Yon");
		radioCheckBoxList.addItem("Go");
		radioCheckBoxList.setPreferredSize(new TerminalSize(16, 8));

		ActionListBox actionListBox = new ActionListBox();
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.addAction(new RandomAction(guiScreen));
		actionListBox.setPreferredSize(new TerminalSize(16, 8));

		mainPanel.addComponent(checkBoxList);
		mainPanel.addComponent(radioCheckBoxList);
		mainPanel.addComponent(actionListBox);
		window1.addComponent(mainPanel);
		window1.addComponent(new EmptySpace());

		Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		buttonPanel.addComponent(new EmptySpace(1, 1), LinearLayout.GROWS_HORIZONTALLY);
		buttonPanel.addComponent(exitButton);
		window1.addComponent(buttonPanel, LinearLayout.GROWS_HORIZONTALLY);
		guiScreen.showWindow(window1, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}

	/**
	 * The Class RandomAction.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class RandomAction implements Action
	{

		/** The counter. */
		private static int counter = 1;

		/** The label. */
		private final String label;

		/** The gui screen. */
		private final GUIScreen guiScreen;

		/**
		 * Instantiates a new random action.
		 * 
		 * @param guiScreen
		 *            the gui screen
		 */
		public RandomAction(GUIScreen guiScreen)
		{
			this.label = "Action #" + counter++;
			this.guiScreen = guiScreen;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Action#doAction()
		 */
		@Override
		public void doAction()
		{
			MessageBox.showMessageBox(guiScreen, "Action", label + " selected");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return label;
		}
	}
}
