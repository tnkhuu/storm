/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.ActionListBox;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.layout.LinearLayout;

/**
 * The Class MultilevelCloseWindow.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MultilevelCloseWindow
{

	/** The Constant WINDOWS. */
	private static final List<Window> WINDOWS = new ArrayList<Window>();

	/** The Constant WINDOW_COUNTER. */
	private static final AtomicInteger WINDOW_COUNTER = new AtomicInteger(0);

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		guiScreen.showWindow(new MultiCloseWindow());
		guiScreen.getScreen().stopScreen();
	}

	/**
	 * The Class MultiCloseWindow.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class MultiCloseWindow extends Window
	{

		/**
		 * Instantiates a new multi close window.
		 */
		public MultiCloseWindow()
		{
			super("Window " + WINDOW_COUNTER.incrementAndGet());
			WINDOWS.add(this);
			ActionListBox actionListBox = new ActionListBox();
			for (final Window window : WINDOWS)
			{
				actionListBox.addAction("Close " + window.toString(), new Action()
				{
					@Override
					public void doAction()
					{
						window.close();
					}
				});
			}
			addComponent(actionListBox);

			Panel buttonPanel = new Panel(Panel.Orientation.HORIZONTAL);
			buttonPanel.addComponent(new EmptySpace(), LinearLayout.GROWS_HORIZONTALLY);
			buttonPanel.addComponent(new Button("New window", new Action()
			{
				@Override
				public void doAction()
				{
					MultiCloseWindow multiCloseWindow = new MultiCloseWindow();
					getOwner().showWindow(multiCloseWindow);
				}
			}));
			buttonPanel.addComponent(new Button("Close", new Action()
			{
				@Override
				public void doAction()
				{
					close();
				}
			}));
			addComponent(buttonPanel);
		}
	}
}
