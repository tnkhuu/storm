/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.ActionListBox;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.dialog.MessageBox;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.terminal.swing.SwingTerminal;
import org.storm.console.ui.framework.terminal.swing.TerminalPalette;

/**
 * The Class SwingChangePaletteTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SwingChangePaletteTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String[] args) throws Exception
	{
		final SwingTerminal swingTerminal = new SwingTerminal();
		final GUIScreen guiScreen = new GUIScreen(new Screen(swingTerminal));
		guiScreen.getScreen().startScreen();
		final Window window1 = new Window("Palette Switcher");

		Panel mainPanel = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		ActionListBox actionListBox = new ActionListBox();

		Field[] fields = TerminalPalette.class.getFields();
		for (Field field : fields)
		{
			if (field.getType() != TerminalPalette.class)
			{
				continue;
			}

			if ((field.getModifiers() & Modifier.STATIC) != 0)
			{
				actionListBox.addAction(new ActionListBoxItem(guiScreen, field));
			}
		}

		mainPanel.addComponent(actionListBox);
		window1.addComponent(mainPanel);

		Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		buttonPanel.addComponent(new EmptySpace(20, 1));
		buttonPanel.addComponent(exitButton);
		window1.addComponent(buttonPanel);
		guiScreen.showWindow(window1, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}

	/**
	 * The Class ActionListBoxItem.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class ActionListBoxItem implements Action
	{

		/** The owner. */
		private final GUIScreen owner;

		/** The palette. */
		private final TerminalPalette palette;

		/** The label. */
		private final String label;

		/**
		 * Instantiates a new action list box item.
		 * 
		 * @param owner
		 *            the owner
		 * @param field
		 *            the field
		 * @throws Exception
		 *             the exception
		 */
		private ActionListBoxItem(GUIScreen owner, Field field) throws Exception
		{
			this.owner = owner;
			this.label = field.getName();
			this.palette = (TerminalPalette) field.get(null);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return label;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Action#doAction()
		 */
		@Override
		public void doAction()
		{
			MessageBox.showMessageBox(owner, "Palette", "Will change palette to " + label + "...");
			((SwingTerminal) owner.getScreen().getTerminal()).setTerminalPalette(palette);
			MessageBox.showMessageBox(owner, "Palette", "Palette changed to " + label + "!");
		}
	}
}
