/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.gui.layout;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.dialog.ActionListDialog;
import org.storm.console.ui.framework.gui.MockComponent;
import org.storm.console.ui.framework.impl.DefaultBackgroundRenderer;
import org.storm.console.ui.framework.layout.BorderLayout;
import org.storm.console.ui.framework.layout.LayoutParameter;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class BorderLayoutTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BorderLayoutTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		guiScreen.setBackgroundRenderer(new DefaultBackgroundRenderer("GUI Test"));

		final Window mainWindow = new Window("Window with BorderLayout");
		final Panel borderLayoutPanel = new Panel("BorderLayout");
		borderLayoutPanel.setLayoutManager(new BorderLayout());
		final Component topComponent = new MockComponent('T', new TerminalSize(50, 3));
		final Component centerComponent = new MockComponent('C', new TerminalSize(5, 5));
		final Component leftComponent = new MockComponent('L', new TerminalSize(6, 3));
		final Component rightComponent = new MockComponent('R', new TerminalSize(6, 3));
		final Component bottomComponent = new MockComponent('B', new TerminalSize(50, 3));

		borderLayoutPanel.addComponent(topComponent, BorderLayout.TOP);
		borderLayoutPanel.addComponent(centerComponent, BorderLayout.CENTER);
		borderLayoutPanel.addComponent(bottomComponent, BorderLayout.BOTTOM);
		borderLayoutPanel.addComponent(leftComponent, BorderLayout.LEFT);
		borderLayoutPanel.addComponent(rightComponent, BorderLayout.RIGHT);
		mainWindow.addComponent(borderLayoutPanel);

		Panel buttonPanel = new Panel(Panel.Orientation.HORIZONTAL);
		buttonPanel.addComponent(new EmptySpace(), LinearLayout.GROWS_HORIZONTALLY);
		Button toggleButton = new Button("Toggle components", new Action()
		{
			@Override
			public void doAction()
			{
				ActionListDialog.showActionListDialog(guiScreen, "Toggle component", "Choose a component to show/hide", new ToggleAction("Top", borderLayoutPanel, topComponent,
						BorderLayout.TOP), new ToggleAction("Left", borderLayoutPanel, leftComponent, BorderLayout.LEFT), new ToggleAction("Center", borderLayoutPanel,
						centerComponent, BorderLayout.CENTER), new ToggleAction("Right", borderLayoutPanel, rightComponent, BorderLayout.RIGHT), new ToggleAction("Bottom",
						borderLayoutPanel, bottomComponent, BorderLayout.BOTTOM));
			}
		});
		buttonPanel.addComponent(toggleButton);
		Button closeButton = new Button("Close", new Action()
		{
			@Override
			public void doAction()
			{
				mainWindow.close();
			}
		});
		buttonPanel.addComponent(closeButton);
		mainWindow.addComponent(buttonPanel, LinearLayout.GROWS_HORIZONTALLY);

		guiScreen.showWindow(mainWindow, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}

	/**
	 * The Class ToggleAction.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class ToggleAction implements Action
	{

		/** The description. */
		private final String description;

		/** The panel. */
		private final Panel panel;

		/** The component. */
		private final Component component;

		/** The layout parameter. */
		private final LayoutParameter layoutParameter;

		/**
		 * Instantiates a new toggle action.
		 * 
		 * @param description
		 *            the description
		 * @param panel
		 *            the panel
		 * @param component
		 *            the component
		 * @param layoutParameter
		 *            the layout parameter
		 */
		public ToggleAction(String description, Panel panel, Component component, LayoutParameter layoutParameter)
		{
			this.description = description;
			this.panel = panel;
			this.component = component;
			this.layoutParameter = layoutParameter;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Action#doAction()
		 */
		@Override
		public void doAction()
		{
			if (panel.containsComponent(component))
			{
				panel.removeComponent(component);
			}
			else
			{
				panel.addComponent(component, layoutParameter);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return description;
		}
	}
}
