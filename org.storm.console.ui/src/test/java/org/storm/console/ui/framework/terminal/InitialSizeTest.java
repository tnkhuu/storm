/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.terminal;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class InitialSizeTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class InitialSizeTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final Terminal rawTerminal = new TestTerminalFactory(args).createTerminal();
		rawTerminal.enterPrivateMode();
		rawTerminal.clearScreen();

		rawTerminal.moveCursor(5, 5);
		printString(rawTerminal, "Waiting for initial size...");
		rawTerminal.flush();

		TerminalSize initialSize = rawTerminal.getTerminalSize();
		rawTerminal.clearScreen();
		rawTerminal.moveCursor(5, 5);
		printString(rawTerminal, "Initial size: ");
		rawTerminal.applySGR(Terminal.SGR.ENTER_BOLD);
		printString(rawTerminal, initialSize.toString());
		rawTerminal.applySGR(Terminal.SGR.RESET_ALL);
		rawTerminal.flush();

		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
		}
		rawTerminal.exitPrivateMode();
	}

	/**
	 * Prints the string.
	 * 
	 * @param rawTerminal
	 *            the raw terminal
	 * @param string
	 *            the string
	 */
	private static void printString(Terminal rawTerminal, String string)
	{
		for (int i = 0; i < string.length(); i++)
		{
			rawTerminal.putCharacter(string.charAt(i));
		}
	}
}
