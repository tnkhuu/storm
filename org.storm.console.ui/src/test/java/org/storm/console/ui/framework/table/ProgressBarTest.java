/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.console.ui.framework.table;

import org.junit.Test;
import org.storm.console.ui.framework.component.ASCIIProgressBar;

public class ProgressBarTest
{

	@Test
	public void testProgressBar() throws InterruptedException {
		System.out.print("Testing Progress Bar: ");
		ASCIIProgressBar	bar = new ASCIIProgressBar(0,0);
		bar.setSize(10.0);
		for(int i = 0; i <= bar.getSize(); i++) {

			bar.print();
			bar.increment(1.0);
			Thread.sleep(1000);
		}
		System.out.println();
	}
}
