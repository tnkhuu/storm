/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.screen;

import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.screen.ScreenCharacterStyle;
import org.storm.console.ui.framework.screen.ScreenWriter;
import org.storm.console.ui.framework.terminal.Terminal;

/**
 * The Class ScreenClearTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ScreenClearTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Screen screen = new TestTerminalFactory(args).createScreen();
		screen.startScreen();
		drawText(screen);
		screen.refresh();
		sleep(1000);
		screen.clear();
		screen.refresh();
		sleep(300);
		drawText(screen);
		screen.refresh();
		sleep(1000);
		screen.clear();
		screen.refresh();
		sleep(300);
		drawText(screen);
		screen.refresh();
		sleep(1000);
		screen.clear();
		screen.refresh();
		screen.stopScreen();
	}

	/**
	 * Draw text.
	 * 
	 * @param screen
	 *            the screen
	 */
	private static void drawText(Screen screen)
	{
		ScreenWriter writer = new ScreenWriter(screen);
		writer.setForegroundColor(Terminal.Color.DEFAULT);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(10, 1, "Hello World");

		writer.setForegroundColor(Terminal.Color.BLACK);
		writer.setBackgroundColor(Terminal.Color.WHITE);
		writer.drawString(11, 2, "Hello World");
		writer.setForegroundColor(Terminal.Color.WHITE);
		writer.setBackgroundColor(Terminal.Color.BLACK);
		writer.drawString(12, 3, "Hello World");
		writer.setForegroundColor(Terminal.Color.BLACK);
		writer.setBackgroundColor(Terminal.Color.WHITE);
		writer.drawString(13, 4, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.WHITE);
		writer.setBackgroundColor(Terminal.Color.BLACK);
		writer.drawString(14, 5, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.DEFAULT);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(15, 6, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.DEFAULT);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(16, 7, "Hello World");

		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(10, 10, "Hello World");
		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.WHITE);
		writer.drawString(11, 11, "Hello World");
		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.BLACK);
		writer.drawString(12, 12, "Hello World");
		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.MAGENTA);
		writer.drawString(13, 13, "Hello World");
		writer.setForegroundColor(Terminal.Color.GREEN);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(14, 14, "Hello World");
		writer.setForegroundColor(Terminal.Color.GREEN);
		writer.setBackgroundColor(Terminal.Color.WHITE);
		writer.drawString(15, 15, "Hello World");
		writer.setForegroundColor(Terminal.Color.GREEN);
		writer.setBackgroundColor(Terminal.Color.BLACK);
		writer.drawString(16, 16, "Hello World");
		writer.setForegroundColor(Terminal.Color.GREEN);
		writer.setBackgroundColor(Terminal.Color.MAGENTA);
		writer.drawString(17, 17, "Hello World");

		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(10, 20, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.WHITE);
		writer.drawString(11, 21, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.BLACK);
		writer.drawString(12, 22, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.BLUE);
		writer.setBackgroundColor(Terminal.Color.MAGENTA);
		writer.drawString(13, 23, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.GREEN);
		writer.setBackgroundColor(Terminal.Color.DEFAULT);
		writer.drawString(14, 24, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.GREEN);
		writer.setBackgroundColor(Terminal.Color.WHITE);
		writer.drawString(15, 25, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.GREEN);
		writer.setBackgroundColor(Terminal.Color.BLACK);
		writer.drawString(16, 26, "Hello World", ScreenCharacterStyle.BOLD);
		writer.setForegroundColor(Terminal.Color.CYAN);
		writer.setBackgroundColor(Terminal.Color.BLUE);
		writer.drawString(17, 27, "Hello World", ScreenCharacterStyle.BOLD);
	}

	/**
	 * Sleep.
	 * 
	 * @param i
	 *            the i
	 */
	private static void sleep(int i)
	{
		try
		{
			Thread.sleep(i);
		}
		catch (InterruptedException e)
		{
		}
	}
}
