/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.component.TextBox;

/**
 * The Class TextBoxTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TextBoxTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		final Window window1 = new Window("Text box window");
		// window1.addComponent(new Widget(1, 1));

		Panel mainPanel = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		for (int i = 0; i < 5; i++)
		{
			Panel editPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
			editPanel.addComponent(new Label("TextBox " + (i + 1) + ":"));
			editPanel.addComponent(new TextBox("", 20));
			mainPanel.addComponent(editPanel);
		}
		window1.addComponent(mainPanel);

		Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				window1.close();
			}
		});
		buttonPanel.addComponent(new EmptySpace(20, 1));
		buttonPanel.addComponent(exitButton);
		window1.addComponent(buttonPanel);
		guiScreen.showWindow(window1, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}
}
