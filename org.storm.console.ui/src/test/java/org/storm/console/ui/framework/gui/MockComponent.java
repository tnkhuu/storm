/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.component.AbstractComponent;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class MockComponent.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MockComponent extends AbstractComponent
{

	/** The fill character. */
	private final char fillCharacter;

	/**
	 * Instantiates a new mock component.
	 * 
	 * @param fillCharacter
	 *            the fill character
	 * @param preferredSize
	 *            the preferred size
	 */
	public MockComponent(char fillCharacter, TerminalSize preferredSize)
	{
		this.fillCharacter = fillCharacter;
		setPreferredSize(preferredSize);
	}

	// Should never be called
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		return new TerminalSize(1, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		graphics.fillArea(fillCharacter);
	}
}
