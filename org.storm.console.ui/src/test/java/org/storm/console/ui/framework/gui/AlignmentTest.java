/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.gui;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TestTerminalFactory;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.dialog.ListSelectDialog;
import org.storm.console.ui.framework.impl.DefaultBackgroundRenderer;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class AlignmentTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class AlignmentTest
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		final GUIScreen guiScreen = new TestTerminalFactory(args).createGUIScreen();
		guiScreen.getScreen().startScreen();
		guiScreen.setBackgroundRenderer(new DefaultBackgroundRenderer("GUI Test"));

		final Window mainWindow = new Window("Alignment Test");
		final Label label = new Label("Aligned Label");
		label.setPreferredSize(new TerminalSize(40, 10));
		mainWindow.addComponent(label);
		Panel buttonPanel = new Panel(Panel.Orientation.HORIZONTAL);
		Button changeAlignmentButton = new Button("Change alignment", new Action()
		{
			@Override
			public void doAction()
			{
				Component.Alignment a = ListSelectDialog.<Component.Alignment> showDialog(guiScreen, "Choose alignment", "Please choose an alignment for the label above",
						Component.Alignment.values());
				if (a != null)
				{
					label.setAlignment(a);
				}
			}
		});
		changeAlignmentButton.setAlignment(Component.Alignment.RIGHT_CENTER);
		Button exitButton = new Button("Exit", new Action()
		{
			@Override
			public void doAction()
			{
				mainWindow.close();
			}
		});
		buttonPanel.addComponent(changeAlignmentButton, LinearLayout.GROWS_HORIZONTALLY);
		buttonPanel.addComponent(exitButton);
		mainWindow.addComponent(buttonPanel, LinearLayout.GROWS_HORIZONTALLY);

		guiScreen.showWindow(mainWindow, GUIScreen.Position.CENTER);
		guiScreen.getScreen().stopScreen();
	}
}
