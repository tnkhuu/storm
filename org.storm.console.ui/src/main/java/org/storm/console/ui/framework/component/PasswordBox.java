/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

/**
 * The Class PasswordBox.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class PasswordBox extends TextBox
{

	/**
	 * Instantiates a new password box.
	 */
	public PasswordBox()
	{
		this("", 0);
	}

	/**
	 * Instantiates a new password box.
	 * 
	 * @param initialContent
	 *            the initial content
	 * @param width
	 *            the width
	 */
	public PasswordBox(String initialContent, int width)
	{
		super(initialContent, width);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.TextBox#prerenderTransformation
	 * (java.lang.String)
	 */
	@Override
	protected String prerenderTransformation(String textboxString)
	{
		String newString = "";
		for (int i = 0; i < textboxString.length(); i++)
		{
			newString += "*";
		}
		return newString;
	}
}
