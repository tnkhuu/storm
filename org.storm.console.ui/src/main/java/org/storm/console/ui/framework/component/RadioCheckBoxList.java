/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.Interactable;
import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class RadioCheckBoxList.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RadioCheckBoxList extends AbstractListBox
{

	/** The checked index. */
	private int checkedIndex;

	/**
	 * Instantiates a new radio check box list.
	 */
	public RadioCheckBoxList()
	{
		this(null);
	}

	/**
	 * Instantiates a new radio check box list.
	 * 
	 * @param preferredSize
	 *            the preferred size
	 */
	public RadioCheckBoxList(TerminalSize preferredSize)
	{
		super(preferredSize);
		this.checkedIndex = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#clearItems()
	 */
	@Override
	public void clearItems()
	{
		checkedIndex = -1;
		super.clearItems();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#addItem(java.lang
	 * .Object)
	 */
	@Override
	public void addItem(Object item)
	{
		super.addItem(item);
	}

	/**
	 * Checks if is checked.
	 * 
	 * @param object
	 *            the object
	 * @return the boolean
	 */
	public Boolean isChecked(Object object)
	{
		if (object == null)
		{
			return null;
		}

		if (indexOf(object) == -1)
		{
			return null;
		}

		return checkedIndex == indexOf(object);
	}

	/**
	 * Checks if is checked.
	 * 
	 * @param index
	 *            the index
	 * @return the boolean
	 */
	public Boolean isChecked(int index)
	{
		if (index < 0 || index >= getNrOfItems())
		{
			return null;
		}

		return checkedIndex == index;
	}

	/**
	 * Sets the checked item index.
	 * 
	 * @param index
	 *            the new checked item index
	 */
	public void setCheckedItemIndex(int index)
	{
		if (index < -1 || index >= getNrOfItems())
		{
			return;
		}

		checkedIndex = index;
		invalidate();
	}

	/**
	 * Gets the checked item index.
	 * 
	 * @return the checked item index
	 */
	public int getCheckedItemIndex()
	{
		return checkedIndex;
	}

	/**
	 * Gets the checked item.
	 * 
	 * @return the checked item
	 */
	public Object getCheckedItem()
	{
		if (checkedIndex == -1 || checkedIndex >= getNrOfItems())
		{
			return null;
		}

		return getItemAt(checkedIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#unhandledKeyboardEvent
	 * (org.storm.console.ui.framework.input.Key)
	 */
	@Override
	protected Interactable.Result unhandledKeyboardEvent(Key key)
	{
		if (getSelectedIndex() == -1)
		{
			return Interactable.Result.EVENT_NOT_HANDLED;
		}

		if (key.getKind() == Kind.Enter || key.getCharacter() == ' ')
		{
			checkedIndex = getSelectedIndex();
			return Result.EVENT_HANDLED;
		}
		return Result.EVENT_NOT_HANDLED;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getHotSpotPositionOnLine(int)
	 */
	@Override
	protected int getHotSpotPositionOnLine(int selectedIndex)
	{
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#createItemString
	 * (int)
	 */
	@Override
	protected String createItemString(int index)
	{
		String check = " ";
		if (checkedIndex == index)
		{
			check = "o";
		}

		String text = getItemAt(index).toString();
		return "<" + check + "> " + text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getListItemThemeDefinition(org.storm.console.ui.framework.gui.Theme)
	 */
	@Override
	protected Theme.Definition getListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Theme.Category.TEXTBOX_FOCUSED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getSelectedListItemThemeDefinition(org.storm.console.ui.framework.gui.Theme)
	 */
	@Override
	protected Theme.Definition getSelectedListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Theme.Category.TEXTBOX_FOCUSED);
	}
}
