/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.layout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class BorderLayout.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class BorderLayout implements LayoutManager
{

	/** The Constant CENTER. */
	public static final LayoutParameter CENTER = new LayoutParameter("BorderLayout.CENTER");

	/** The Constant LEFT. */
	public static final LayoutParameter LEFT = new LayoutParameter("BorderLayout.LEFT");

	/** The Constant RIGHT. */
	public static final LayoutParameter RIGHT = new LayoutParameter("BorderLayout.RIGHT");

	/** The Constant TOP. */
	public static final LayoutParameter TOP = new LayoutParameter("BorderLayout.TOP");

	/** The Constant BOTTOM. */
	public static final LayoutParameter BOTTOM = new LayoutParameter("BorderLayout.BOTTOM");

	/** The Constant BORDER_LAYOUT_POSITIONS. */
	private static final Set<LayoutParameter> BORDER_LAYOUT_POSITIONS = Collections.unmodifiableSet(new HashSet<LayoutParameter>(Arrays.asList(CENTER, LEFT, RIGHT, TOP, BOTTOM)));

	/** The components. */
	private final Map<LayoutParameter, Component> components;

	/**
	 * Instantiates a new border layout.
	 */
	public BorderLayout()
	{
		components = new IdentityHashMap<LayoutParameter, Component>(5);
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager#addComponent(org.storm.console.ui.framework.Component, org.storm.console.ui.framework.layout.LayoutParameter[])
	 */
	@Override
	public void addComponent(Component component, LayoutParameter... parameters)
	{
		if (parameters.length == 0)
		{
			parameters = new LayoutParameter[] { CENTER };
		}
		else if (parameters.length > 1)
		{
			throw new IllegalArgumentException("Calling BorderLayout.addComponent with more than one " + "layout parameter");
		}
		else if (!BORDER_LAYOUT_POSITIONS.contains(parameters[0]))
		{
			throw new IllegalArgumentException("Calling BorderLayout.addComponent layout parameter " + parameters[0] + " is not allowed");
		}
		else if (component == null)
		{
			throw new IllegalArgumentException("Calling BorderLayout.addComponent component parameter " + "set to null is not allowed");
		}

		// Make sure we don't add the same component twice
		removeComponent(component);
		synchronized (components)
		{
			components.put(parameters[0], component);
		}
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager#removeComponent(org.storm.console.ui.framework.Component)
	 */
	@Override
	public void removeComponent(Component component)
	{
		synchronized (components)
		{
			for (LayoutParameter parameter : BORDER_LAYOUT_POSITIONS)
			{
				if (components.get(parameter) == component)
				{
					components.remove(parameter);
					return;
				}
			}
		}
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager#getPreferredSize()
	 */
	@Override
	public TerminalSize getPreferredSize()
	{
		synchronized (components)
		{
			int preferredHeight = (components.containsKey(TOP) ? components.get(TOP).getPreferredSize().getRows() : 0)
					+ Math.max(
							components.containsKey(LEFT) ? components.get(LEFT).getPreferredSize().getRows() : 0,
							Math.max(components.containsKey(CENTER) ? components.get(CENTER).getPreferredSize().getRows() : 0, components.containsKey(RIGHT) ? components
									.get(RIGHT).getPreferredSize().getRows() : 0)) + (components.containsKey(BOTTOM) ? components.get(BOTTOM).getPreferredSize().getRows() : 0);

			int preferredWidth = Math.max(
					(components.containsKey(LEFT) ? components.get(LEFT).getPreferredSize().getColumns() : 0)
							+ (components.containsKey(CENTER) ? components.get(CENTER).getPreferredSize().getColumns() : 0)
							+ (components.containsKey(RIGHT) ? components.get(RIGHT).getPreferredSize().getColumns() : 0),
					Math.max(components.containsKey(TOP) ? components.get(TOP).getPreferredSize().getColumns() : 0, components.containsKey(BOTTOM) ? components.get(BOTTOM)
							.getPreferredSize().getColumns() : 0));
			return new TerminalSize(preferredWidth, preferredHeight);
		}
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager#layout(org.storm.console.ui.framework.terminal.TerminalSize)
	 */
	@Override
	public List<? extends LaidOutComponent> layout(TerminalSize layoutArea)
	{
		int availableHorizontalSpace = layoutArea.getColumns();
		int availableVerticalSpace = layoutArea.getRows();
		List<LaidOutComponent> finalLayout = new ArrayList<LaidOutComponent>();

		synchronized (components)
		{
			// We'll need this later on
			int topComponentHeight = 0;
			int leftComponentWidth = 0;

			// First allocate the top
			if (components.containsKey(TOP))
			{
				Component topComponent = components.get(TOP);
				topComponentHeight = Math.min(topComponent.getPreferredSize().getRows(), availableVerticalSpace);
				finalLayout.add(new DefaultLaidOutComponent(topComponent, new TerminalSize(availableHorizontalSpace, topComponentHeight), new TerminalPosition(0, 0)));
				availableVerticalSpace -= topComponentHeight;
			}

			// Next allocate the bottom
			if (components.containsKey(BOTTOM))
			{
				Component bottomComponent = components.get(BOTTOM);
				int bottomComponentHeight = Math.min(bottomComponent.getPreferredSize().getRows(), availableVerticalSpace);
				finalLayout.add(new DefaultLaidOutComponent(bottomComponent, new TerminalSize(availableHorizontalSpace, bottomComponentHeight), new TerminalPosition(0, layoutArea
						.getRows() - bottomComponentHeight)));
				availableVerticalSpace -= bottomComponentHeight;
			}

			// Now divide the remaining space between LEFT, CENTER and RIGHT
			if (components.containsKey(LEFT))
			{
				Component leftComponent = components.get(LEFT);
				leftComponentWidth = Math.min(leftComponent.getPreferredSize().getColumns(), availableHorizontalSpace);
				finalLayout.add(new DefaultLaidOutComponent(leftComponent, new TerminalSize(leftComponentWidth, availableVerticalSpace),
						new TerminalPosition(0, topComponentHeight)));
				availableHorizontalSpace -= leftComponentWidth;
			}
			if (components.containsKey(RIGHT))
			{
				Component rightComponent = components.get(RIGHT);
				int rightComponentWidth = Math.min(rightComponent.getPreferredSize().getColumns(), availableHorizontalSpace);
				finalLayout.add(new DefaultLaidOutComponent(rightComponent, new TerminalSize(rightComponentWidth, availableVerticalSpace), new TerminalPosition(layoutArea
						.getColumns() - rightComponentWidth, topComponentHeight)));
				availableHorizontalSpace -= rightComponentWidth;
			}
			if (components.containsKey(CENTER))
			{
				Component centerComponent = components.get(CENTER);
				finalLayout.add(new DefaultLaidOutComponent(centerComponent, new TerminalSize(availableHorizontalSpace, availableVerticalSpace), new TerminalPosition(
						leftComponentWidth, topComponentHeight)));
			}
		}
		return finalLayout;
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager#maximisesVertically()
	 */
	@Override
	public boolean maximisesVertically()
	{
		return false;
	}

	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager#maximisesHorisontally()
	 */
	@Override
	public boolean maximisesHorisontally()
	{
		return false;
	}
}
