/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class TextBox.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TextBox extends AbstractInteractableComponent
{

	/** The force width. */
	private final int forceWidth;

	/** The backend. */
	private String backend;

	/** The edit position. */
	private int editPosition;

	/** The visible left position. */
	private int visibleLeftPosition;

	/** The last known width. */
	private int lastKnownWidth;

	/**
	 * Instantiates a new text box.
	 */
	public TextBox()
	{
		this("");
	}

	/**
	 * Instantiates a new text box.
	 * 
	 * @param initialContent
	 *            the initial content
	 */
	public TextBox(String initialContent)
	{
		this(initialContent, 0);
	}

	/**
	 * Instantiates a new text box.
	 * 
	 * @param initialContent
	 *            the initial content
	 * @param width
	 *            the width
	 */
	public TextBox(String initialContent, int width)
	{
		if (initialContent == null)
		{
			initialContent = "";
		}
		if (width <= 0)
		{
			if (initialContent.length() > 10)
			{
				width = initialContent.length();
			}
			else
			{
				width = 10;
			}
		}

		this.forceWidth = width;
		this.backend = initialContent;
		this.editPosition = initialContent.length();
		this.visibleLeftPosition = 0;
		this.lastKnownWidth = 0;
	}

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	public String getText()
	{
		return backend;
	}

	/**
	 * Sets the text.
	 * 
	 * @param text
	 *            the new text
	 */
	public void setText(String text)
	{
		backend = text;
		editPosition = backend.length();
		invalidate();
	}

	/**
	 * Sets the edits the position.
	 * 
	 * @param editPosition
	 *            the new edits the position
	 */
	public void setEditPosition(int editPosition)
	{
		if (editPosition < 0)
		{
			editPosition = 0;
		}
		if (editPosition > backend.length())
		{
			editPosition = backend.length();
		}

		this.editPosition = editPosition;
		invalidate();
	}

	/**
	 * Gets the edits the position.
	 * 
	 * @return the edits the position
	 */
	public int getEditPosition()
	{
		return editPosition;
	}

	/**
	 * Prerender transformation.
	 * 
	 * @param textboxString
	 *            the textbox string
	 * @return the string
	 */
	protected String prerenderTransformation(String textboxString)
	{
		return textboxString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		if (hasFocus())
		{
			graphics.applyTheme(Category.TEXTBOX_FOCUSED);
		}
		else
		{
			graphics.applyTheme(Category.TEXTBOX);
		}

		graphics.fillArea(' ');
		String displayString = prerenderTransformation(backend).substring(visibleLeftPosition);
		if (displayString.length() > graphics.getWidth())
		{
			displayString = displayString.substring(0, graphics.getWidth() - 1);
		}
		graphics.drawString(0, 0, displayString);
		setHotspot(graphics.translateToGlobalCoordinates(new TerminalPosition(editPosition - visibleLeftPosition, 0)));
		lastKnownWidth = graphics.getWidth();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		return new TerminalSize(forceWidth, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Interactable#keyboardInteraction(com.googlecode
	 * .lanterna.input.Key)
	 */
	@Override
	public Result keyboardInteraction(Key key)
	{
		try
		{
			switch (key.getKind())
			{
			case Tab:
			case Enter:
				return Result.NEXT_INTERACTABLE_RIGHT;

			case ArrowDown:
				return Result.NEXT_INTERACTABLE_DOWN;

			case ReverseTab:
				return Result.PREVIOUS_INTERACTABLE_LEFT;

			case ArrowUp:
				return Result.PREVIOUS_INTERACTABLE_UP;

			case ArrowRight:
				if (editPosition == backend.length())
				{
					break;
				}
				editPosition++;
				if (editPosition - visibleLeftPosition >= lastKnownWidth)
				{
					visibleLeftPosition++;
				}
				break;

			case ArrowLeft:
				if (editPosition == 0)
				{
					break;
				}
				editPosition--;
				if (editPosition - visibleLeftPosition < 0)
				{
					visibleLeftPosition--;
				}
				break;

			case End:
				editPosition = backend.length();
				if (editPosition - visibleLeftPosition >= lastKnownWidth)
				{
					visibleLeftPosition = editPosition - lastKnownWidth + 1;
				}
				break;

			case Home:
				editPosition = 0;
				visibleLeftPosition = 0;
				break;

			case Delete:
				if (editPosition == backend.length())
				{
					break;
				}
				backend = backend.substring(0, editPosition) + backend.substring(editPosition + 1);
				break;

			case Backspace:
				if (editPosition == 0)
				{
					break;
				}
				editPosition--;
				if (editPosition - visibleLeftPosition < 0)
				{
					visibleLeftPosition--;
				}
				backend = backend.substring(0, editPosition) + backend.substring(editPosition + 1);
				break;

			case NormalKey:
				// Add character
				if (Character.isISOControl(key.getCharacter()) || key.getCharacter() > 127)
				{
					break;
				}

				backend = backend.substring(0, editPosition) + key.getCharacter() + backend.substring(editPosition);
				editPosition++;
				if (editPosition - visibleLeftPosition >= lastKnownWidth)
				{
					visibleLeftPosition++;
				}
				break;

			default:
				return Result.EVENT_NOT_HANDLED;
			}
			return Result.EVENT_HANDLED;
		}
		finally
		{
			invalidate();
		}
	}
}
