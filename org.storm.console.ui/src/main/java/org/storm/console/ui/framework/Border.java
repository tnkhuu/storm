/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.terminal.ACS;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class Border.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class Border
{

	/**
	 * Draw border.
	 * 
	 * @param graphics
	 *            the graphics
	 * @param actualSize
	 *            the actual size
	 * @param title
	 *            the title
	 */
	public abstract void drawBorder(TextGraphics graphics, TerminalSize actualSize, String title);

	/**
	 * Gets the inner area size.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @return the inner area size
	 */
	public abstract TerminalSize getInnerAreaSize(int width, int height);

	/**
	 * Gets the inner area location.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @return the inner area location
	 */
	public abstract TerminalPosition getInnerAreaLocation(int width, int height);

	/**
	 * Surround area size.
	 * 
	 * @param TerminalSize
	 *            the terminal size
	 * @return the terminal size
	 */
	public abstract TerminalSize surroundAreaSize(TerminalSize TerminalSize);

	/**
	 * The Class Standard.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class Standard extends Border
	{

		/**
		 * Instantiates a new standard.
		 */
		public Standard()
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.Border#drawBorder(org.storm.console.ui.framework
		 * .gui.TextGraphics, org.storm.console.ui.framework.terminal.TerminalSize,
		 * java.lang.String)
		 */
		@Override
		public void drawBorder(TextGraphics graphics, TerminalSize actualSize, String title)
		{
			graphics.applyTheme(graphics.getTheme().getDefinition(Theme.Category.BORDER));

			final int columnsWidth = actualSize.getColumns();
			final int rowsHeight = actualSize.getRows();

			// Top
			graphics.drawString(0, 0, ACS.ULCORNER + "");
			for (int x = 1; x < columnsWidth - 1; x++)
			{
				graphics.drawString(x, 0, ACS.HLINE + "");
			}
			graphics.drawString(columnsWidth - 1, 0, ACS.URCORNER + "");

			// Each row
			for (int i = 1; i < rowsHeight - 1; i++)
			{
				graphics.drawString(0, i, ACS.VLINE + "");
				graphics.drawString(0 + columnsWidth - 1, i, ACS.VLINE + "");
			}

			// Bottom
			graphics.drawString(0, rowsHeight - 1, ACS.LLCORNER + "");
			for (int x = 1; x < columnsWidth - 1; x++)
			{
				graphics.drawString(x, rowsHeight - 1, ACS.HLINE + "");
			}
			graphics.drawString(columnsWidth - 1, rowsHeight - 1, ACS.LRCORNER + "");

			// Title
			graphics.applyTheme(graphics.getTheme().getDefinition(Theme.Category.DIALOG_AREA));
			graphics.setBoldMask(true);
			graphics.drawString(2, 0, title);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Border#getInnerAreaLocation(int,
		 * int)
		 */
		@Override
		public TerminalPosition getInnerAreaLocation(int width, int height)
		{
			if (width > 2 && height > 2)
			{
				return new TerminalPosition(2, 1);
			}
			else
			{
				return new TerminalPosition(0, 0);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Border#getInnerAreaSize(int, int)
		 */
		@Override
		public TerminalSize getInnerAreaSize(int width, int height)
		{
			if (width > 2 && height > 2)
			{
				return new TerminalSize(width - 4, height - 2);
			}
			else
			{
				return new TerminalSize(width, height);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.Border#surroundAreaSize(com.googlecode
		 * .lanterna.terminal.TerminalSize)
		 */
		@Override
		public TerminalSize surroundAreaSize(TerminalSize terminalSize)
		{
			final int surroundColumnStretch = 4;
			final int surroundRowStretch = 2;

			int terminalSizeColumns = terminalSize.getColumns();
			int terminalSizeRows = terminalSize.getRows();

			int surroundSizeColumns;
			if (terminalSizeColumns == Integer.MAX_VALUE)
			{
				surroundSizeColumns = terminalSizeColumns;
			}
			else
			{
				surroundSizeColumns = terminalSizeColumns + surroundColumnStretch;
			}

			int surroundSizeRows;
			if (terminalSizeRows == Integer.MAX_VALUE)
			{
				surroundSizeRows = terminalSizeRows;
			}
			else
			{
				surroundSizeRows = terminalSizeRows + surroundRowStretch;
			}

			TerminalSize surroundAreaTerminalSize = new TerminalSize(surroundSizeColumns, surroundSizeRows);

			return surroundAreaTerminalSize;
		}
	}

	/**
	 * The Class Bevel.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class Bevel extends Border
	{

		/** The is raised. */
		private boolean isRaised;

		/**
		 * Instantiates a new bevel.
		 * 
		 * @param isRaised
		 *            the is raised
		 */
		public Bevel(boolean isRaised)
		{
			this.isRaised = isRaised;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.Border#drawBorder(org.storm.console.ui.framework
		 * .gui.TextGraphics, org.storm.console.ui.framework.terminal.TerminalSize,
		 * java.lang.String)
		 */
		@Override
		public void drawBorder(TextGraphics graphics, TerminalSize actualSize, String title)
		{
			// Record current terminal size
			final int columnsWidth = actualSize.getColumns();
			final int rowsHeight = actualSize.getRows();

			// Select current overall theme
			final Theme theme = graphics.getTheme();

			// Select the current theme's definition of upper-left and
			// lower-right borders rendering, considering whether they should be
			// displayed with raised looks as well
			final Theme.Definition upperLeftTheme;
			final Theme.Definition lowerRightTheme;
			if (isRaised)
			{
				upperLeftTheme = theme.getDefinition(Theme.Category.RAISED_BORDER);
				lowerRightTheme = theme.getDefinition(Theme.Category.BORDER);
			}
			else
			{
				upperLeftTheme = theme.getDefinition(Theme.Category.BORDER);
				lowerRightTheme = theme.getDefinition(Theme.Category.RAISED_BORDER);
			}

			// Select the current theme's dialog area style definition
			final Theme.Definition dialogAreaTheme = theme.getDefinition(Theme.Category.DIALOG_AREA);

			// Top
			graphics.applyTheme(upperLeftTheme);
			graphics.drawString(0, 0, ACS.ULCORNER + "");
			for (int i = 1; i < columnsWidth - 1; i++)
			{
				graphics.drawString(i, 0, ACS.HLINE + "");
			}
			graphics.applyTheme(lowerRightTheme);
			graphics.drawString(columnsWidth - 1, 0, ACS.URCORNER + "");

			// Each row
			for (int i = 1; i < rowsHeight - 1; i++)
			{
				graphics.applyTheme(upperLeftTheme);
				graphics.drawString(0, i, ACS.VLINE + "");
				graphics.applyTheme(lowerRightTheme);
				graphics.drawString(columnsWidth - 1, i, ACS.VLINE + "");
			}

			// Bottom
			graphics.applyTheme(upperLeftTheme);
			graphics.drawString(0, rowsHeight - 1, ACS.LLCORNER + "");
			graphics.applyTheme(lowerRightTheme);
			for (int i = 1; i < columnsWidth - 1; i++)
			{
				graphics.drawString(i, rowsHeight - 1, ACS.HLINE + "");
			}
			graphics.drawString(columnsWidth - 1, rowsHeight - 1, ACS.LRCORNER + "");

			// Title
			graphics.applyTheme(dialogAreaTheme);
			graphics.setBoldMask(true);
			graphics.drawString(2, 0, title);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Border#getInnerAreaLocation(int,
		 * int)
		 */
		@Override
		public TerminalPosition getInnerAreaLocation(int width, int height)
		{
			if (width > 2 && height > 2)
			{
				return new TerminalPosition(2, 1);
			}
			else
			{
				return new TerminalPosition(0, 0);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Border#getInnerAreaSize(int, int)
		 */
		@Override
		public TerminalSize getInnerAreaSize(int width, int height)
		{
			if (width > 2 && height > 2)
			{
				return new TerminalSize(width - 4, height - 2);
			}
			else
			{
				return new TerminalSize(width, height);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.Border#surroundAreaSize(com.googlecode
		 * .lanterna.terminal.TerminalSize)
		 */
		@Override
		public TerminalSize surroundAreaSize(TerminalSize terminalSize)
		{
			final int surroundColumnStretch = 4;
			final int surroundRowStretch = 2;

			int terminalSizeColumns = terminalSize.getColumns();
			int terminalSizeRows = terminalSize.getRows();

			int surroundSizeColumns;
			if (terminalSizeColumns == Integer.MAX_VALUE)
			{
				surroundSizeColumns = terminalSizeColumns;
			}
			else
			{
				surroundSizeColumns = terminalSizeColumns + surroundColumnStretch;
			}

			int surroundSizeRows;
			if (terminalSizeRows == Integer.MAX_VALUE)
			{
				surroundSizeRows = terminalSizeRows;
			}
			else
			{
				surroundSizeRows = terminalSizeRows + surroundRowStretch;
			}

			TerminalSize surroundAreaTerminalSize = new TerminalSize(surroundSizeColumns, surroundSizeRows);

			return surroundAreaTerminalSize;
		}
	}

	/**
	 * The Class Invisible.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class Invisible extends Border
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.Border#drawBorder(org.storm.console.ui.framework
		 * .gui.TextGraphics, org.storm.console.ui.framework.terminal.TerminalSize,
		 * java.lang.String)
		 */
		@Override
		public void drawBorder(TextGraphics graphics, TerminalSize actualSize, String title)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Border#getInnerAreaLocation(int,
		 * int)
		 */
		@Override
		public TerminalPosition getInnerAreaLocation(int width, int height)
		{
			return new TerminalPosition(0, 0);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.Border#getInnerAreaSize(int, int)
		 */
		@Override
		public TerminalSize getInnerAreaSize(int width, int height)
		{
			return new TerminalSize(width, height);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.Border#surroundAreaSize(com.googlecode
		 * .lanterna.terminal.TerminalSize)
		 */
		@Override
		public TerminalSize surroundAreaSize(TerminalSize TerminalSize)
		{
			return TerminalSize;
		}
	}
}
