/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.screen;

import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class Screen.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Screen
{

	/** The mutex. */
	private final Object mutex;

	/** The terminal. */
	private final Terminal terminal;

	/** The resize queue. */
	private final LinkedList<TerminalSize> resizeQueue;

	/** The cursor position. */
	private TerminalPosition cursorPosition;

	/** The terminal size. */
	private TerminalSize terminalSize;

	/** The visible screen. */
	private ScreenCharacter[][] visibleScreen;

	/** The backbuffer. */
	private ScreenCharacter[][] backbuffer;

	/** The padding character. */
	private ScreenCharacter paddingCharacter;

	/** The whole screen invalid. */
	private boolean wholeScreenInvalid;

	/** The has been activated. */
	private boolean hasBeenActivated;

	// How to deal with \t characters
	/** The tab behaviour. */
	private TabBehaviour tabBehaviour;

	/**
	 * Instantiates a new screen.
	 * 
	 * @param terminal
	 *            the terminal
	 */
	@SuppressWarnings("deprecation")
	public Screen(Terminal terminal)
	{
		this(terminal, terminal.queryTerminalSize());
	}

	/**
	 * Instantiates a new screen.
	 * 
	 * @param terminal
	 *            the terminal
	 * @param terminalSize
	 *            the terminal size
	 */
	public Screen(Terminal terminal, TerminalSize terminalSize)
	{
		this(terminal, terminalSize.getColumns(), terminalSize.getRows());
	}

	/**
	 * Instantiates a new screen.
	 * 
	 * @param terminal
	 *            the terminal
	 * @param terminalWidth
	 *            the terminal width
	 * @param terminalHeight
	 *            the terminal height
	 */
	public Screen(Terminal terminal, int terminalWidth, int terminalHeight)
	{
		this.mutex = new Object();
		this.terminal = terminal;
		this.terminalSize = new TerminalSize(terminalWidth, terminalHeight);
		this.visibleScreen = new ScreenCharacter[terminalHeight][terminalWidth];
		this.backbuffer = new ScreenCharacter[terminalHeight][terminalWidth];
		this.paddingCharacter = new ScreenCharacter('X', Terminal.Color.GREEN, Terminal.Color.BLACK);
		this.resizeQueue = new LinkedList<TerminalSize>();
		this.wholeScreenInvalid = false;
		this.hasBeenActivated = false;
		this.cursorPosition = new TerminalPosition(0, 0);
		this.tabBehaviour = TabBehaviour.ALIGN_TO_COLUMN_8;

		this.terminal.addResizeListener(new TerminalResizeListener());

		// Initialize the screen
		clear();
	}

	/**
	 * Gets the terminal.
	 * 
	 * @return the terminal
	 */
	public Terminal getTerminal()
	{
		return terminal;
	}

	/**
	 * Gets the cursor position.
	 * 
	 * @return the cursor position
	 */
	public TerminalPosition getCursorPosition()
	{
		return cursorPosition;
	}

	/**
	 * Sets the cursor position.
	 * 
	 * @param position
	 *            the new cursor position
	 */
	public void setCursorPosition(TerminalPosition position)
	{
		if (position != null)
		{
			// TerminalPosition isn't immutable, so make a copy
			this.cursorPosition = new TerminalPosition(position);
		}
		else
		{
			this.cursorPosition = null;
		}
	}

	/**
	 * Sets the cursor position.
	 * 
	 * @param column
	 *            the column
	 * @param row
	 *            the row
	 */
	public void setCursorPosition(int column, int row)
	{
		synchronized (mutex)
		{
			if (column >= 0 && column < terminalSize.getColumns() && row >= 0 && row < terminalSize.getRows())
			{
				setCursorPosition(new TerminalPosition(column, row));
			}
		}
	}

	/**
	 * Sets the tab behaviour.
	 * 
	 * @param tabBehaviour
	 *            the new tab behaviour
	 */
	public void setTabBehaviour(TabBehaviour tabBehaviour)
	{
		if (tabBehaviour != null)
		{
			this.tabBehaviour = tabBehaviour;
		}
	}

	/**
	 * Sets the padding character.
	 * 
	 * @param character
	 *            the character
	 * @param foregroundColor
	 *            the foreground color
	 * @param backgroundColor
	 *            the background color
	 * @param style
	 *            the style
	 */
	public void setPaddingCharacter(char character, Terminal.Color foregroundColor, Terminal.Color backgroundColor, ScreenCharacterStyle... style)
	{

		this.paddingCharacter = new ScreenCharacter(character, foregroundColor, backgroundColor, new HashSet<ScreenCharacterStyle>(Arrays.asList(style)));
	}

	/**
	 * Gets the tab behaviour.
	 * 
	 * @return the tab behaviour
	 */
	public TabBehaviour getTabBehaviour()
	{
		return tabBehaviour;
	}

	/**
	 * Read input.
	 * 
	 * @return the key
	 */
	public Key readInput()
	{
		return terminal.readInput();
	}

	/**
	 * Gets the terminal size.
	 * 
	 * @return the terminal size
	 */
	public TerminalSize getTerminalSize()
	{
		synchronized (mutex)
		{
			return terminalSize;
		}
	}

	/**
	 * Start screen.
	 */
	public void startScreen()
	{
		if (hasBeenActivated)
		{
			return;
		}

		hasBeenActivated = true;
		terminal.enterPrivateMode();
		terminal.clearScreen();
		if (cursorPosition != null)
		{
			terminal.setCursorVisible(true);
			terminal.moveCursor(cursorPosition.getY(), cursorPosition.getX());
		}
		else
		{
			terminal.setCursorVisible(false);
		}
		refresh();
	}

	/**
	 * Stop screen.
	 */
	public void stopScreen()
	{
		if (!hasBeenActivated)
		{
			return;
		}

		terminal.exitPrivateMode();
		hasBeenActivated = false;
	}

	/**
	 * Clear.
	 */
	public void clear()
	{
		// ScreenCharacter is immutable, so we can use it for every element
		ScreenCharacter background = new ScreenCharacter(' ');

		synchronized (mutex)
		{
			for (int y = 0; y < terminalSize.getRows(); y++)
			{
				for (int x = 0; x < terminalSize.getColumns(); x++)
				{
					backbuffer[y][x] = background;
				}
			}
		}
	}

	/**
	 * Put string.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param string
	 *            the string
	 * @param foregroundColor
	 *            the foreground color
	 * @param backgroundColor
	 *            the background color
	 * @param styles
	 *            the styles
	 */
	public void putString(int x, int y, String string, Terminal.Color foregroundColor, Terminal.Color backgroundColor, ScreenCharacterStyle... styles)
	{
		Set<ScreenCharacterStyle> drawStyle = EnumSet.noneOf(ScreenCharacterStyle.class);
		drawStyle.addAll(Arrays.asList(styles));
		putString(x, y, string, foregroundColor, backgroundColor, drawStyle);
	}

	/**
	 * Put string.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param string
	 *            the string
	 * @param foregroundColor
	 *            the foreground color
	 * @param backgroundColor
	 *            the background color
	 * @param styles
	 *            the styles
	 */
	public void putString(int x, int y, String string, Terminal.Color foregroundColor, Terminal.Color backgroundColor, Set<ScreenCharacterStyle> styles)
	{
		string = tabBehaviour.replaceTabs(string, x);
		for (int i = 0; i < string.length(); i++)
		{
			putCharacter(x + i, y, new ScreenCharacter(string.charAt(i), foregroundColor, backgroundColor, styles));
		}
	}

	/**
	 * Put character.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param character
	 *            the character
	 */
	void putCharacter(int x, int y, ScreenCharacter character)
	{
		synchronized (mutex)
		{
			if (y < 0 || y >= backbuffer.length || x < 0 || x >= backbuffer[0].length)
			{
				return;
			}

			// Only create a new character if the
			if (!backbuffer[x][y].equals(character))
			{
				backbuffer[x][y] = new ScreenCharacter(character);
			}
		}
	}

	/**
	 * Resize pending.
	 * 
	 * @return true, if successful
	 */
	public boolean resizePending()
	{
		synchronized (resizeQueue)
		{
			return !resizeQueue.isEmpty();
		}
	}

	/**
	 * Refresh.
	 */
	public void refresh()
	{
		if (!hasBeenActivated)
		{
			return;
		}

		synchronized (mutex)
		{
			// If any resize operations are in the queue, execute them
			resizeScreenIfNeeded();

			Map<TerminalPosition, ScreenCharacter> updateMap = new TreeMap<TerminalPosition, ScreenCharacter>(new ScreenPointComparator());

			for (int y = 0; y < terminalSize.getRows(); y++)
			{
				for (int x = 0; x < terminalSize.getColumns(); x++)
				{
					ScreenCharacter c = backbuffer[y][x];
					if (!c.equals(visibleScreen[y][x]) || wholeScreenInvalid)
					{
						visibleScreen[y][x] = c; // Remember, ScreenCharacter is
													// immutable, we don't need
													// to worry about it being
													// modified
						updateMap.put(new TerminalPosition(x, y), c);
					}
				}
			}

			Writer terminalWriter = new Writer();
			terminalWriter.reset();
			TerminalPosition previousPoint = null;
			for (TerminalPosition nextUpdate : updateMap.keySet())
			{
				if (previousPoint == null || previousPoint.getX() != nextUpdate.getX() || previousPoint.getY() + 1 != nextUpdate.getY())
				{
					terminalWriter.setCursorPosition(nextUpdate.getY(), nextUpdate.getX());
				}
				terminalWriter.writeCharacter(updateMap.get(nextUpdate));
				previousPoint = nextUpdate;
			}
			if (cursorPosition != null)
			{
				terminalWriter.setCursorVisible(true);
				terminalWriter.setCursorPosition(cursorPosition.getY(), cursorPosition.getX());
			}
			else
			{
				terminalWriter.setCursorVisible(false);
			}
			wholeScreenInvalid = false;
		}
		terminal.flush();
	}

	// WARNING!!! Should only be called in a block synchronized on mutex! See
	// refresh()
	/**
	 * Resize screen if needed.
	 */
	private void resizeScreenIfNeeded()
	{
		TerminalSize newSize;
		synchronized (resizeQueue)
		{
			if (resizeQueue.isEmpty())
			{
				return;
			}

			newSize = resizeQueue.getLast();
			resizeQueue.clear();
		}

		int height = newSize.getRows();
		int width = newSize.getColumns();
		ScreenCharacter[][] newBackBuffer = new ScreenCharacter[height][width];
		ScreenCharacter[][] newVisibleScreen = new ScreenCharacter[height][width];
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				if (backbuffer.length > 0 && x < backbuffer[0].length && y < backbuffer.length)
				{
					newBackBuffer[y][x] = backbuffer[y][x];
				}
				else
				{
					newBackBuffer[y][x] = new ScreenCharacter(paddingCharacter);
				}

				if (visibleScreen.length > 0 && x < visibleScreen[0].length && y < visibleScreen.length)
				{
					newVisibleScreen[y][x] = visibleScreen[y][x];
				}
				else
				{
					newVisibleScreen[y][x] = new ScreenCharacter(paddingCharacter);
				}
			}
		}

		backbuffer = newBackBuffer;
		visibleScreen = newVisibleScreen;
		wholeScreenInvalid = true;
		terminalSize = new TerminalSize(newSize);
	}

	/**
	 * The Class ScreenPointComparator.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class ScreenPointComparator implements Comparator<TerminalPosition>
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(TerminalPosition o1, TerminalPosition o2)
		{
			if (o1.getX() == o2.getX())
			{
				if (o1.getY() == o2.getY())
				{
					return 0;
				}
				else
				{
					return new Integer(o1.getY()).compareTo(o2.getY());
				}
			}
			else
			{
				return new Integer(o1.getX()).compareTo(o2.getX());
			}
		}
	}

	/**
	 * The listener interface for receiving terminalResize events. The class
	 * that is interested in processing a terminalResize event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addTerminalResizeListener<code> method. When
	 * the terminalResize event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see TerminalResizeEvent
	 */
	private class TerminalResizeListener implements Terminal.ResizeListener
	{

		
		@Override
		public void onResized(TerminalSize newSize)
		{
			synchronized (resizeQueue)
			{
				resizeQueue.add(newSize);
			}
		}
	}

	/**
	 * The Class Writer.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class Writer
	{

		/** The current foreground color. */
		private Terminal.Color currentForegroundColor;

		/** The current background color. */
		private Terminal.Color currentBackgroundColor;

		/** The currently is bold. */
		private boolean currentlyIsBold;

		/** The currently is underline. */
		private boolean currentlyIsUnderline;

		/** The currently is negative. */
		private boolean currentlyIsNegative;

		/** The currently is blinking. */
		private boolean currentlyIsBlinking;

		/**
		 * Instantiates a new writer.
		 */
		public Writer()
		{
			currentForegroundColor = Terminal.Color.DEFAULT;
			currentBackgroundColor = Terminal.Color.DEFAULT;
			currentlyIsBold = false;
			currentlyIsUnderline = false;
			currentlyIsNegative = false;
			currentlyIsBlinking = false;
		}

		/**
		 * Sets the cursor position.
		 * 
		 * @param x
		 *            the x
		 * @param y
		 *            the y
		 */
		void setCursorPosition(int x, int y)
		{
			terminal.moveCursor(x, y);
		}

		/**
		 * Sets the cursor visible.
		 * 
		 * @param visible
		 *            the new cursor visible
		 */
		private void setCursorVisible(boolean visible)
		{
			terminal.setCursorVisible(visible);
		}

		/**
		 * Write character.
		 * 
		 * @param character
		 *            the character
		 */
		void writeCharacter(ScreenCharacter character)
		{
			if (currentlyIsBlinking != character.isBlinking())
			{
				if (character.isBlinking())
				{
					terminal.applySGR(Terminal.SGR.ENTER_BLINK);
					currentlyIsBlinking = true;
				}
				else
				{
					terminal.applySGR(Terminal.SGR.RESET_ALL);
					terminal.applyBackgroundColor(character.getBackgroundColor());
					terminal.applyForegroundColor(character.getForegroundColor());

					// emulating "stop_blink_mode" so that previous formatting
					// is preserved
					currentlyIsBold = false;
					currentlyIsUnderline = false;
					currentlyIsNegative = false;
					currentlyIsBlinking = false;
				}
			}
			if (currentForegroundColor != character.getForegroundColor())
			{
				terminal.applyForegroundColor(character.getForegroundColor());
				currentForegroundColor = character.getForegroundColor();
			}
			if (currentBackgroundColor != character.getBackgroundColor())
			{
				terminal.applyBackgroundColor(character.getBackgroundColor());
				currentBackgroundColor = character.getBackgroundColor();
			}
			if (currentlyIsBold != character.isBold())
			{
				if (character.isBold())
				{
					terminal.applySGR(Terminal.SGR.ENTER_BOLD);
					currentlyIsBold = true;
				}
				else
				{
					terminal.applySGR(Terminal.SGR.EXIT_BOLD);
					currentlyIsBold = false;
				}
			}
			if (currentlyIsUnderline != character.isUnderline())
			{
				if (character.isUnderline())
				{
					terminal.applySGR(Terminal.SGR.ENTER_UNDERLINE);
					currentlyIsUnderline = true;
				}
				else
				{
					terminal.applySGR(Terminal.SGR.EXIT_UNDERLINE);
					currentlyIsUnderline = false;
				}
			}
			if (currentlyIsNegative != character.isNegative())
			{
				if (character.isNegative())
				{
					terminal.applySGR(Terminal.SGR.ENTER_REVERSE);
					currentlyIsNegative = true;
				}
				else
				{
					terminal.applySGR(Terminal.SGR.EXIT_REVERSE);
					currentlyIsNegative = false;
				}
			}
			terminal.putCharacter(character.getCharacter());
		}

		/**
		 * Reset.
		 */
		void reset()
		{
			terminal.applySGR(Terminal.SGR.RESET_ALL);
			terminal.moveCursor(0, 0);

			currentBackgroundColor = Terminal.Color.DEFAULT;
			currentForegroundColor = Terminal.Color.DEFAULT;
			currentlyIsBold = false;
			currentlyIsNegative = false;
			currentlyIsUnderline = false;
		}
	}
}
