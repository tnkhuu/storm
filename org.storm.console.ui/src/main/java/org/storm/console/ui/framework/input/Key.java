/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.input;

import org.storm.api.foundation.Visitable;
import org.storm.api.foundation.Visitor;

/**
 * The Class Key.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Key implements Visitable
{

	/** The kind. */
	private final Kind kind;

	/** The character. */
	private final char character;

	/** The alt pressed. */
	private final boolean altPressed;

	/** The ctrl pressed. */
	private final boolean ctrlPressed;

	/**
	 * Instantiates a new key.
	 * 
	 * @param character
	 *            the character
	 */
	public Key(char character)
	{
		this(character, false, false);
	}

	/**
	 * Instantiates a new key.
	 * 
	 * @param character
	 *            the character
	 * @param ctrlPressed
	 *            the ctrl pressed
	 * @param altPressed
	 *            the alt pressed
	 */
	public Key(char character, boolean ctrlPressed, boolean altPressed)
	{
		this.character = character;
		this.kind = Kind.NormalKey;
		this.ctrlPressed = ctrlPressed;
		this.altPressed = altPressed;
	}

	/**
	 * Instantiates a new key.
	 * 
	 * @param kind
	 *            the kind
	 */
	public Key(Kind kind)
	{
		this.kind = kind;
		this.character = kind.getRepresentationKey();
		this.altPressed = false;
		this.ctrlPressed = false;
	}

	/**
	 * Instantiates a new key.
	 * 
	 * @param kind
	 *            the kind
	 * @param ctrlPressed
	 *            the ctrl pressed
	 * @param altPressed
	 *            the alt pressed
	 */
	public Key(Kind kind, boolean ctrlPressed, boolean altPressed)
	{
		this.kind = kind;
		this.character = kind.getRepresentationKey();
		this.altPressed = altPressed;
		this.ctrlPressed = ctrlPressed;
	}

	/**
	 * Gets the kind.
	 * 
	 * @return the kind
	 */
	public Kind getKind()
	{
		return kind;
	}

	/**
	 * Gets the character.
	 * 
	 * @return the character
	 */
	public char getCharacter()
	{
		return character;
	}

	/**
	 * Checks if is alt pressed.
	 * 
	 * @return true, if is alt pressed
	 */
	public boolean isAltPressed()
	{
		return altPressed;
	}

	/**
	 * Checks if is ctrl pressed.
	 * 
	 * @return true, if is ctrl pressed
	 */
	public boolean isCtrlPressed()
	{
		return ctrlPressed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return getKind().toString() + (getKind() == Kind.NormalKey ? ": " + character : "") + (ctrlPressed ? " (ctrl)" : "") + (altPressed ? " (alt)" : "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (obj.getClass() != getClass())
		{
			return false;
		}
		return character == ((Key) obj).character;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 73 * hash + this.character;
		return hash;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object accept(Visitor visitor)
	{
		return visitor.visit(this);
	}
}
