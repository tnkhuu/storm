/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.input;

import java.util.Arrays;
import java.util.List;

/**
 * The Class BasicCharacterPattern.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class BasicCharacterPattern implements CharacterPattern
{

	/** The result. */
	private Key result;

	/** The pattern. */
	private char[] pattern;

	/**
	 * Instantiates a new basic character pattern.
	 * 
	 * @param result
	 *            the result
	 * @param pattern
	 *            the pattern
	 */
	BasicCharacterPattern(Key result, char... pattern)
	{
		this.result = result;
		this.pattern = pattern;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#matches(java.util.List)
	 */
	@Override
	public boolean matches(List<Character> currentMatching)
	{
		int minSize = Math.min(currentMatching.size(), pattern.length);
		for (int i = 0; i < minSize; i++)
		{
			if (pattern[i] != currentMatching.get(i).charValue())
			{
				return false;
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#getResult(java.util.List)
	 */
	@Override
	public Key getResult(List<Character> matching)
	{
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#isCompleteMatch(java.util
	 * .List)
	 */
	@Override
	public boolean isCompleteMatch(List<Character> currentMatching)
	{
		return pattern.length == currentMatching.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof BasicCharacterPattern == false)
		{
			return false;
		}

		BasicCharacterPattern other = (BasicCharacterPattern) obj;
		return Arrays.equals(pattern, other.pattern);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 53 * hash + Arrays.hashCode(this.pattern);
		return hash;
	}
}
