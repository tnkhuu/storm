/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.dialog;

import java.util.ArrayList;
import java.util.List;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.GUIScreen;

/**
 * The Class ListSelectDialog.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ListSelectDialog
{

	/**
	 * Instantiates a new list select dialog.
	 */
	private ListSelectDialog()
	{
	}

	/**
	 * Show dialog.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param items
	 *            the items
	 * @return the t
	 */
	@SuppressWarnings("unchecked")
	public static <T> T showDialog(final GUIScreen owner, final String title, final String description, final T... items)
	{
		return showDialog(owner, title, description, 0, items);
	}

	/**
	 * Show dialog.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param listWidth
	 *            the list width
	 * @param items
	 *            the items
	 * @return the t
	 */
	@SuppressWarnings("unchecked")
	public static <T> T showDialog(final GUIScreen owner, final String title, final String description, final int listWidth, final T... items)
	{
		final List<T> result = new ArrayList<T>();
		Action[] actionItems = new Action[items.length];
		for (int i = 0; i < items.length; i++)
		{
			final T item = items[i];
			actionItems[i] = new Action()
			{
				@Override
				public void doAction()
				{
					result.add(item);
				}

				@Override
				public String toString()
				{
					return item.toString();
				}
			};
		}

		ActionListDialog.showActionListDialog(owner, title, description, listWidth, actionItems);
		if (result.isEmpty())
		{
			return null;
		}
		else
		{
			return result.get(0);
		}
	}
}
