/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.Container;
import org.storm.console.ui.framework.Interactable;
import org.storm.console.ui.framework.InteractableContainer;
import org.storm.console.ui.framework.Shortcuts;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.layout.LayoutParameter;
import org.storm.console.ui.framework.listener.ContainerListener;

/**
 * The Class AbstractContainer.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class AbstractContainer extends AbstractComponent implements InteractableContainer, Container
{

	/** The container listeners. */
	private final List<ContainerListener> containerListeners;

	/** The components. */
	private final List<Component> components;

	/** The shortcut helper. */
	private final Shortcuts shortcutHelper;

	/**
	 * Instantiates a new abstract container.
	 */
	protected AbstractContainer()
	{
		components = new ArrayList<Component>();
		containerListeners = new LinkedList<ContainerListener>();
		shortcutHelper = new Shortcuts();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Container#addComponent(org.storm.console.ui.framework
	 * .gui.Component, org.storm.console.ui.framework.gui.layout.LayoutParameter[])
	 */
	@Override
	public void addComponent(Component component, LayoutParameter... layoutParameters)
	{
		if (component == null)
		{
			return;
		}

		synchronized (components)
		{
			components.add(component);
		}

		if (component instanceof AbstractComponent)
		{
			((AbstractComponent) component).setParent(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Container#getComponentAt(int)
	 */
	@Override
	public Component getComponentAt(int index)
	{
		synchronized (components)
		{
			return components.get(index);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Container#containsComponent(com.googlecode
	 * .lanterna.gui.Component)
	 */
	@Override
	public boolean containsComponent(Component component)
	{
		synchronized (components)
		{
			return components.contains(component);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Container#getComponentCount()
	 */
	@Override
	public int getComponentCount()
	{
		synchronized (components)
		{
			return components.size();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Container#removeComponent(com.googlecode.
	 * lanterna.gui.Component)
	 */
	@Override
	public boolean removeComponent(Component component)
	{
		if (component == null)
		{
			return false;
		}

		synchronized (components)
		{
			return components.remove(component);
		}
	}

	/**
	 * Removes the all components.
	 */
	public void removeAllComponents()
	{
		synchronized (components)
		{
			while (getComponentCount() > 0)
			{
				removeComponent(getComponentAt(0));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractComponent#isScrollable()
	 */
	@Override
	public boolean isScrollable()
	{
		for (Component component : components)
		{
			if (component.isScrollable())
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Components.
	 * 
	 * @return the iterable
	 */
	protected Iterable<Component> components()
	{
		return components;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Container#addContainerListener(com.googlecode
	 * .lanterna.gui.listener.ContainerListener)
	 */
	@Override
	public void addContainerListener(ContainerListener cl)
	{
		if (cl != null)
		{
			containerListeners.add(cl);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Container#removeContainerListener(com.googlecode
	 * .lanterna.gui.listener.ContainerListener)
	 */
	@Override
	public void removeContainerListener(ContainerListener cl)
	{
		if (cl != null)
		{
			containerListeners.remove(cl);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#hasInteractable(com
	 * .googlecode.lanterna.gui.Interactable)
	 */
	@Override
	public boolean hasInteractable(Interactable interactable)
	{
		for (Component component : components())
		{
			if (component instanceof InteractableContainer)
			{
				if (((InteractableContainer) component).hasInteractable(interactable))
				{
					return true;
				}
			}
			if (component == interactable)
			{
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#nextFocus(com.googlecode
	 * .lanterna.gui.Interactable)
	 */
	@Override
	public Interactable nextFocus(Interactable previous)
	{
		boolean chooseNextAvailable = previous == null;

		for (Component component : components())
		{
			if (chooseNextAvailable)
			{
				if (!component.isVisible())
				{
					continue;
				}
				if (component instanceof Interactable)
				{
					return (Interactable) component;
				}
				if (component instanceof InteractableContainer)
				{
					Interactable firstInteractable = ((InteractableContainer) component).nextFocus(null);
					if (firstInteractable != null)
					{
						return firstInteractable;
					}
				}
				continue;
			}

			if (component == previous)
			{
				chooseNextAvailable = true;
				continue;
			}

			if (component instanceof InteractableContainer)
			{
				InteractableContainer ic = (InteractableContainer) component;
				if (ic.hasInteractable(previous))
				{
					Interactable next = ic.nextFocus(previous);
					if (next == null)
					{
						chooseNextAvailable = true;
						continue;
					}
					else
					{
						return next;
					}
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.InteractableContainer#previousFocus(com.
	 * googlecode.lanterna.gui.Interactable)
	 */
	@Override
	public Interactable previousFocus(Interactable fromThis)
	{
		boolean chooseNextAvailable = fromThis == null;

		List<Component> revComponents = new ArrayList<Component>(components);
		Collections.reverse(revComponents);

		for (Component component : revComponents)
		{
			if (chooseNextAvailable)
			{
				if (!component.isVisible())
				{
					continue;
				}
				if (component instanceof Interactable)
				{
					return (Interactable) component;
				}
				if (component instanceof InteractableContainer)
				{
					Interactable lastInteractable = ((InteractableContainer) component).previousFocus(null);
					if (lastInteractable != null)
					{
						return lastInteractable;
					}
				}
				continue;
			}

			if (component == fromThis)
			{
				chooseNextAvailable = true;
				continue;
			}

			if (component instanceof InteractableContainer)
			{
				InteractableContainer ic = (InteractableContainer) component;
				if (ic.hasInteractable(fromThis))
				{
					Interactable next = ic.previousFocus(fromThis);
					if (next == null)
					{
						chooseNextAvailable = true;
						continue;
					}
					else
					{
						return next;
					}
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#addShortcut(com.googlecode
	 * .lanterna.input.Key.Kind, org.storm.console.ui.framework.gui.Action)
	 */
	@Override
	public void addShortcut(Kind key, Action action)
	{
		shortcutHelper.addShortcut(key, action);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.InteractableContainer#addShortcut(char,
	 * boolean, boolean, org.storm.console.ui.framework.gui.Action)
	 */
	@Override
	public void addShortcut(char character, boolean withCtrl, boolean withAlt, Action action)
	{
		shortcutHelper.addShortcut(character, withCtrl, withAlt, action);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#triggerShortcut(com
	 * .googlecode.lanterna.input.Key)
	 */
	@Override
	public boolean triggerShortcut(Key key)
	{
		return shortcutHelper.triggerShortcut(key);
	}
}
