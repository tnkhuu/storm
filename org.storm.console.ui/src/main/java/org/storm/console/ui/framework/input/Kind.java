package org.storm.console.ui.framework.input;

/**
 * The Enum Kind.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public enum Kind
{

	/** The Normal key. */
	NormalKey('N'),

	/** The Escape. */
	Escape('\\'),

	/** The Backspace. */
	Backspace('B'),

	/** The Arrow left. */
	ArrowLeft('L'),

	/** The Arrow right. */
	ArrowRight('R'),

	/** The Arrow up. */
	ArrowUp('U'),

	/** The Arrow down. */
	ArrowDown('D'),

	/** The Insert. */
	Insert('I'),

	/** The Delete. */
	Delete('T'),

	/** The Home. */
	Home('H'),

	/** The End. */
	End('E'),

	/** The Page up. */
	PageUp('P'),

	/** The Page down. */
	PageDown('O'),

	/** The Tab. */
	Tab('\t'),

	/** The Reverse tab. */
	ReverseTab('/'),

	/** The Enter. */
	Enter('\n'),

	/** The F1. */
	F1('1'),

	/** The F2. */
	F2('2'),

	/** The F3. */
	F3('3'),

	/** The F4. */
	F4('4'),

	/** The F5. */
	F5('5'),

	/** The F6. */
	F6('6'),

	/** The F7. */
	F7('7'),

	/** The F8. */
	F8('8'),

	/** The F9. */
	F9('9'),

	/** The F10. */
	F10('Q'), // No idea what to pick here, but it doesn't really matter
	/** The F11. */
	F11('W'), // No idea what to pick here, but it doesn't really matter
	/** The F12. */
	F12('Y'), // No idea what to pick here, but it doesn't really matter
	/** The Unknown. */
	Unknown('!'),

	/** The Cursor location. */
	CursorLocation('£');

	/** The representation key. */
	private char representationKey;

	/**
	 * Instantiates a new kind.
	 * 
	 * @param representationKey
	 *            the representation key
	 */
	private Kind(char representationKey)
	{
		this.representationKey = representationKey;
	}

	/**
	 * Gets the representation key.
	 * 
	 * @return the representation key
	 */
	public char getRepresentationKey()
	{
		return representationKey;
	}
}