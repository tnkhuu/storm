/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import org.storm.console.ui.framework.ConsoleException;
import org.storm.console.ui.framework.input.InputDecoder;
import org.storm.console.ui.framework.input.InputProvider;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.KeyMappingProfile;
import org.storm.console.ui.framework.input.Kind;

/**
 * The Class InputEnabledAbstractTerminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class InputEnabledAbstractTerminal extends AbstractTerminal implements InputProvider
{

	/** The input decoder. */
	private final InputDecoder inputDecoder;

	/** The key queue. */
	private final Queue<Key> keyQueue;

	/** The read mutex. */
	private final Object readMutex;

	/**
	 * Instantiates a new input enabled abstract terminal.
	 * 
	 * @param inputDecoder
	 *            the input decoder
	 */
	public InputEnabledAbstractTerminal(InputDecoder inputDecoder)
	{
		this.inputDecoder = inputDecoder;
		this.keyQueue = new LinkedList<Key>();
		this.readMutex = new Object();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.InputProvider#addInputProfile(com.googlecode
	 * .lanterna.input.KeyMappingProfile)
	 */
	@Override
	public void addInputProfile(KeyMappingProfile profile)
	{
		inputDecoder.addProfile(profile);
	}

	/**
	 * Wait for terminal size report.
	 * 
	 * @param timeoutMs
	 *            the timeout ms
	 * @return the terminal size
	 */
	protected TerminalSize waitForTerminalSizeReport(int timeoutMs)
	{
		long startTime = System.currentTimeMillis();
		synchronized (readMutex)
		{
			while (System.currentTimeMillis() - startTime < timeoutMs)
			{
				Key key = inputDecoder.getNextCharacter();
				if (key == null)
				{
					try
					{
						Thread.sleep(1);
					}
					catch (InterruptedException e)
					{
					}
					continue;
				}

				if (key.getKind() != Kind.CursorLocation)
				{
					keyQueue.add(key);
				}
				else
				{
					TerminalPosition reportedTerminalPosition = inputDecoder.getLastReportedTerminalPosition();
					if (reportedTerminalPosition != null)
					{
						onResized(reportedTerminalPosition.getY(), reportedTerminalPosition.getX());
					}
					else
					{
						throw new ConsoleException(new IOException("Unexpected: inputDecoder.getLastReportedTerminalPosition() " + "returned null after position was reported"));
					}
					return new TerminalSize(reportedTerminalPosition.getY(), reportedTerminalPosition.getX());
				}
			}
		}
		throw new ConsoleException(new IOException("Timeout while waiting for terminal size report! " + "Maybe your terminal doesn't support cursor position report, please "
				+ "consider using a custom size querier"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.input.InputProvider#readInput()
	 */
	@Override
	public Key readInput()
	{
		synchronized (readMutex)
		{
			if (!keyQueue.isEmpty())
			{
				return keyQueue.poll();
			}

			Key key = inputDecoder.getNextCharacter();
			if (key != null && key.getKind() == Kind.CursorLocation)
			{
				TerminalPosition reportedTerminalPosition = inputDecoder.getLastReportedTerminalPosition();
				if (reportedTerminalPosition != null)
				{
					onResized(reportedTerminalPosition.getY(), reportedTerminalPosition.getX());
				}

				return readInput();
			}
			else
			{
				return key;
			}
		}
	}
}
