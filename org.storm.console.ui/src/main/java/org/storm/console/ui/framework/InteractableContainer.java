/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;

/**
 * The Interface InteractableContainer.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface InteractableContainer
{

	/**
	 * Checks for interactable.
	 * 
	 * @param interactable
	 *            the interactable
	 * @return true, if successful
	 */
	boolean hasInteractable(Interactable interactable);

	/**
	 * Next focus.
	 * 
	 * @param fromThis
	 *            the from this
	 * @return the interactable
	 */
	Interactable nextFocus(Interactable fromThis);

	/**
	 * Previous focus.
	 * 
	 * @param fromThis
	 *            the from this
	 * @return the interactable
	 */
	Interactable previousFocus(Interactable fromThis);

	/**
	 * Adds the shortcut.
	 * 
	 * @param key
	 *            the key
	 * @param action
	 *            the action
	 */
	void addShortcut(Kind key, Action action);

	/**
	 * Adds the shortcut.
	 * 
	 * @param character
	 *            the character
	 * @param withCtrl
	 *            the with ctrl
	 * @param withAlt
	 *            the with alt
	 * @param action
	 *            the action
	 */
	void addShortcut(char character, boolean withCtrl, boolean withAlt, Action action);

	/**
	 * Trigger shortcut.
	 * 
	 * @param key
	 *            the key
	 * @return true, if successful
	 */
	boolean triggerShortcut(Key key);
}
