/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.console.ui.framework.impl;

import java.io.PrintStream;

import org.storm.console.ui.framework.Buffer;

public class FrameBuffer implements Buffer
{
	private final char[][] buffer;
	private int width;
	private int height;
	@SuppressWarnings("unused")
	private int position = 0;
	public FrameBuffer(int width, int height) {
		this.width = width;
		this.height = height;
		buffer = new char[width][height];
	}
	
	@SuppressWarnings("unused")
	private void fillBuffer() {
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				buffer[y][x] = '*';
			}
		}
	}
	
	public void append(String output) {
		
		position+= output.length();
		checkBuffer();
	}
	public void append(char output) {
		position++;
		checkBuffer();
	}
	public void append(char[] output) {
		position+=output.length;
		checkBuffer();
	}
	public void append(CharSequence output) {
		position+= output.length();
		checkBuffer();
	}
	public void write(PrintStream out) {
		
		out.print(buffer);
		position = 0;
	}

	private void checkBuffer() throws FrameBufferOverflowException {
		
	}

}
