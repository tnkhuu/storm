/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.listener.ComponentListener;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Interface Component.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Component
{

	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
	Container getParent();

	/**
	 * Gets the window.
	 * 
	 * @return the window
	 */
	Window getWindow();

	/**
	 * Adds the component listener.
	 * 
	 * @param cl
	 *            the cl
	 */
	void addComponentListener(ComponentListener cl);

	/**
	 * Removes the component listener.
	 * 
	 * @param cl
	 *            the cl
	 */
	void removeComponentListener(ComponentListener cl);

	/**
	 * Repaint.
	 * 
	 * @param graphics
	 *            the graphics
	 */
	void repaint(TextGraphics graphics);

	/**
	 * Sets the visible.
	 * 
	 * @param visible
	 *            the new visible
	 */
	void setVisible(boolean visible);

	/**
	 * Checks if is visible.
	 * 
	 * @return true, if is visible
	 */
	boolean isVisible();

	/**
	 * Checks if is scrollable.
	 * 
	 * @return true, if is scrollable
	 */
	boolean isScrollable();

	/**
	 * Gets the preferred size.
	 * 
	 * @return the preferred size
	 */
	TerminalSize getPreferredSize();

	/**
	 * Gets the minimum size.
	 * 
	 * @return the minimum size
	 */
	TerminalSize getMinimumSize();

	/**
	 * Sets the preferred size.
	 * 
	 * @param preferredSizeOverride
	 *            the new preferred size
	 */
	void setPreferredSize(TerminalSize preferredSizeOverride);

	/**
	 * Sets the alignment.
	 * 
	 * @param alignment
	 *            the new alignment
	 */
	void setAlignment(Alignment alignment);

	/**
	 * Gets the alignment.
	 * 
	 * @return the alignment
	 */
	Alignment getAlignment();

	/**
	 * The Enum Alignment.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum Alignment
	{

		/** The top center. */
		TOP_CENTER,

		/** The botton center. */
		BOTTON_CENTER,

		/** The left center. */
		LEFT_CENTER,

		/** The right center. */
		RIGHT_CENTER,

		/** The center. */
		CENTER,

		/** The fill. */
		FILL,

		/** The top left. */
		TOP_LEFT,

		/** The top right. */
		TOP_RIGHT,

		/** The bottom left. */
		BOTTOM_LEFT,

		/** The bottom right. */
		BOTTOM_RIGHT,
	}
}
