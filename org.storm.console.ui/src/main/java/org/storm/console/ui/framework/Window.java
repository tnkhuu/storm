/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import java.util.ArrayList;
import java.util.List;

import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.layout.LayoutParameter;
import org.storm.console.ui.framework.listener.ComponentAdapter;
import org.storm.console.ui.framework.listener.ContainerListener;
import org.storm.console.ui.framework.listener.WindowListener;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class Window.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Window
{

	/** The window listeners. */
	private final List<WindowListener> windowListeners;

	/** The invalidator alerts. */
	private final List<ComponentInvalidatorAlert> invalidatorAlerts;

	/** The owner. */
	private GUIScreen owner;

	/** The content pane. */
	private final WindowContentPane contentPane;

	/** The currently in focus. */
	private Interactable currentlyInFocus;

	/** The window size override. */
	private TerminalSize windowSizeOverride;

	/** The solo window. */
	private boolean soloWindow;

	/**
	 * Instantiates a new window.
	 * 
	 * @param title
	 *            the title
	 */
	public Window(String title)
	{
		this.windowListeners = new ArrayList<WindowListener>();
		this.invalidatorAlerts = new ArrayList<ComponentInvalidatorAlert>();
		this.owner = null;
		this.contentPane = new WindowContentPane(title);
		this.currentlyInFocus = null;
		this.soloWindow = false;
		this.windowSizeOverride = null;
	}

	/**
	 * Adds the window listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addWindowListener(WindowListener listener)
	{
		windowListeners.add(listener);
	}

	/**
	 * Gets the owner.
	 * 
	 * @return the owner
	 */
	public GUIScreen getOwner()
	{
		return owner;
	}

	/**
	 * Sets the owner.
	 * 
	 * @param owner
	 *            the new owner
	 */
	void setOwner(GUIScreen owner)
	{
		this.owner = owner;
	}

	/**
	 * Gets the border.
	 * 
	 * @return the border
	 */
	public Border getBorder()
	{
		return contentPane.getBorder();
	}

	/**
	 * Sets the border.
	 * 
	 * @param border
	 *            the new border
	 */
	public void setBorder(Border border)
	{
		if (border != null)
		{
			contentPane.setBorder(border);
		}
	}

	/**
	 * Gets the window size override.
	 * 
	 * @return the window size override
	 */
	public TerminalSize getWindowSizeOverride()
	{
		return windowSizeOverride;
	}

	/**
	 * Sets the window size override.
	 * 
	 * @param windowSizeOverride
	 *            the new window size override
	 */
	public void setWindowSizeOverride(TerminalSize windowSizeOverride)
	{
		this.windowSizeOverride = windowSizeOverride;
	}

	/**
	 * Gets the preferred size.
	 * 
	 * @return the preferred size
	 */
	TerminalSize getPreferredSize()
	{
		if (windowSizeOverride != null)
		{
			return windowSizeOverride;
		}
		else
		{
			return contentPane.getPreferredSize(); // Automatically calculate
													// the size
		}
	}

	/**
	 * Repaint.
	 * 
	 * @param graphics
	 *            the graphics
	 */
	void repaint(TextGraphics graphics)
	{
		graphics.applyTheme(graphics.getTheme().getDefinition(Category.DIALOG_AREA));
		graphics.fillRectangle(' ', new TerminalPosition(0, 0), new TerminalSize(graphics.getWidth(), graphics.getHeight()));
		contentPane.repaint(graphics);
	}

	/**
	 * Invalidate.
	 */
	private void invalidate()
	{
		for (WindowListener listener : windowListeners)
		{
			listener.onWindowInvalidated(this);
		}
	}

	/**
	 * Adds the empty line.
	 */
	@Deprecated
	public void addEmptyLine()
	{
		addComponent(new EmptySpace(1, 1));
	}

	/**
	 * Adds the component.
	 * 
	 * @param component
	 *            the component
	 * @param layoutParameters
	 *            the layout parameters
	 */
	public void addComponent(Component component, LayoutParameter... layoutParameters)
	{
		if (component == null)
		{
			return;
		}

		contentPane.addComponent(component, layoutParameters);
		ComponentInvalidatorAlert invalidatorAlert = new ComponentInvalidatorAlert(component);
		invalidatorAlerts.add(invalidatorAlert);
		component.addComponentListener(invalidatorAlert);

		if (currentlyInFocus == null)
		{
			setFocus(contentPane.nextFocus(null));
		}

		invalidate();
	}

	/**
	 * Adds the container listener.
	 * 
	 * @param cl
	 *            the cl
	 */
	public void addContainerListener(ContainerListener cl)
	{
		contentPane.addContainerListener(cl);
	}

	/**
	 * Removes the container listener.
	 * 
	 * @param cl
	 *            the cl
	 */
	public void removeContainerListener(ContainerListener cl)
	{
		contentPane.removeContainerListener(cl);
	}

	/**
	 * Gets the component at.
	 * 
	 * @param index
	 *            the index
	 * @return the component at
	 */
	public Component getComponentAt(int index)
	{
		return contentPane.getComponentAt(index);
	}

	/**
	 * Gets the component count.
	 * 
	 * @return the component count
	 */
	public int getComponentCount()
	{
		return contentPane.getComponentCount();
	}

	/**
	 * Removes the component.
	 * 
	 * @param component
	 *            the component
	 */
	public void removeComponent(Component component)
	{
		if (component instanceof InteractableContainer)
		{
			InteractableContainer container = (InteractableContainer) component;
			if (container.hasInteractable(currentlyInFocus))
			{
				Interactable original = currentlyInFocus;
				Interactable current = contentPane.nextFocus(original);
				while (container.hasInteractable(current) && original != current)
				{
					current = contentPane.nextFocus(current);
				}
				if (container.hasInteractable(current))
				{
					setFocus(null);
				}
				else
				{
					setFocus(current);
				}
			}
		}
		else if (component == currentlyInFocus)
		{
			setFocus(contentPane.nextFocus(currentlyInFocus));
		}

		contentPane.removeComponent(component);

		for (ComponentInvalidatorAlert invalidatorAlert : invalidatorAlerts)
		{
			if (component == invalidatorAlert.component)
			{
				component.removeComponentListener(invalidatorAlert);
				invalidatorAlerts.remove(invalidatorAlert);
				break;
			}
		}
	}

	/**
	 * Removes the all components.
	 */
	public void removeAllComponents()
	{
		while (getComponentCount() > 0)
		{
			removeComponent(getComponentAt(0));
		}
	}

	/**
	 * Gets the window hotspot position.
	 * 
	 * @return the window hotspot position
	 */
	TerminalPosition getWindowHotspotPosition()
	{
		if (currentlyInFocus == null)
		{
			return null;
		}
		else
		{
			return currentlyInFocus.getHotspot();
		}
	}

	/**
	 * On key pressed.
	 * 
	 * @param key
	 *            the key
	 */
	public void onKeyPressed(Key key)
	{
		if (currentlyInFocus != null)
		{
			Interactable.Result result = currentlyInFocus.keyboardInteraction(key);
			if (result.isNextInteractable())
			{
				Interactable nextItem = contentPane.nextFocus(currentlyInFocus);
				if (nextItem == null)
				{
					nextItem = contentPane.nextFocus(null);
				}
				setFocus(nextItem, result.asFocusChangeDirection());
			}
			else if (result.isPreviousInteractable())
			{
				Interactable prevItem = contentPane.previousFocus(currentlyInFocus);
				if (prevItem == null)
				{
					prevItem = contentPane.previousFocus(null);
				}
				setFocus(prevItem, result.asFocusChangeDirection());
			}
			else if (result == Interactable.Result.EVENT_NOT_HANDLED)
			{
				// Try to find a shortcut
				if (currentlyInFocus instanceof Component)
				{
					Container parentContainer = ((Component) currentlyInFocus).getParent();
					while (parentContainer != null)
					{
						if (parentContainer instanceof InteractableContainer)
						{
							// If we could fire off a shortcut, stop here
							if (((InteractableContainer) parentContainer).triggerShortcut(key))
							{
								return;
							}
						}
						// Otherwise try one level higher
						parentContainer = parentContainer.getParent();
					}
				}

				// There was no shortcut
				onUnhandledKeyPress(key);
			}
		}
		else
		{
			onUnhandledKeyPress(key);
		}
	}

	/**
	 * On unhandled key press.
	 * 
	 * @param key
	 *            the key
	 */
	protected void onUnhandledKeyPress(Key key)
	{
		for (WindowListener listener : windowListeners)
		{
			listener.onUnhandledKeyboardInteraction(this, key);
		}
	}

	/**
	 * Checks if is solo window.
	 * 
	 * @return true, if is solo window
	 */
	public boolean isSoloWindow()
	{
		return soloWindow;
	}

	/**
	 * Sets the solo window.
	 * 
	 * @param soloWindow
	 *            the new solo window
	 */
	public void setSoloWindow(boolean soloWindow)
	{
		this.soloWindow = soloWindow;
	}

	/**
	 * Maximises vertically.
	 * 
	 * @return true, if successful
	 */
	protected boolean maximisesVertically()
	{
		return contentPane.maximisesVertically();
	}

	/**
	 * Maximises horisontally.
	 * 
	 * @return true, if successful
	 */
	protected boolean maximisesHorisontally()
	{
		return contentPane.maximisesHorisontally();
	}

	/**
	 * Sets the focus.
	 * 
	 * @param newFocus
	 *            the new focus
	 */
	protected void setFocus(Interactable newFocus)
	{
		setFocus(newFocus, null);
	}

	/**
	 * Sets the focus.
	 * 
	 * @param newFocus
	 *            the new focus
	 * @param direction
	 *            the direction
	 */
	protected void setFocus(Interactable newFocus, Interactable.FocusChangeDirection direction)
	{
		if (currentlyInFocus != null)
		{
			currentlyInFocus.onLeaveFocus(direction);
		}

		// Fire the focus changed event
		for (WindowListener listener : windowListeners)
		{
			listener.onFocusChanged(this, currentlyInFocus, newFocus);
		}

		currentlyInFocus = newFocus;
		if (currentlyInFocus != null)
		{
			currentlyInFocus.onEnterFocus(direction);
		}
		invalidate();
	}

	/**
	 * Close.
	 */
	public void close()
	{
		if (owner != null)
		{
			owner.closeWindow(this);
		}
	}

	/**
	 * On visible.
	 */
	protected void onVisible()
	{
		for (WindowListener listener : windowListeners)
		{
			listener.onWindowShown(this);
		}
	}

	/**
	 * On closed.
	 */
	protected void onClosed()
	{
		for (WindowListener listener : windowListeners)
		{
			listener.onWindowClosed(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return contentPane.getTitle();
	}

	/**
	 * The Class ComponentInvalidatorAlert.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class ComponentInvalidatorAlert extends ComponentAdapter
	{

		/** The component. */
		private Component component;

		/**
		 * Instantiates a new component invalidator alert.
		 * 
		 * @param component
		 *            the component
		 */
		public ComponentInvalidatorAlert(Component component)
		{
			this.component = component;
		}

		/**
		 * Gets the component.
		 * 
		 * @return the component
		 */
		@SuppressWarnings("unused")
		public Component getComponent()
		{
			return component;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.listener.ComponentAdapter#
		 * onComponentInvalidated(org.storm.console.ui.framework.gui.Component)
		 */
		@Override
		public void onComponentInvalidated(Component component)
		{
			invalidate();
		}
	}

	/**
	 * The Class WindowContentPane.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class WindowContentPane extends Panel
	{

		/**
		 * Instantiates a new window content pane.
		 * 
		 * @param title
		 *            the title
		 */
		public WindowContentPane(String title)
		{
			super(title);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.component.AbstractComponent#getWindow()
		 */
		@Override
		public Window getWindow()
		{
			return Window.this;
		}
	}
}
