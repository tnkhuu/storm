/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.Container;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.listener.ComponentListener;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class AbstractComponent.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class AbstractComponent implements Component
{

	/** The component listeners. */
	private final List<ComponentListener> componentListeners;

	/** The parent. */
	private Container parent;

	/** The preferred size override. */
	private TerminalSize preferredSizeOverride;

	/** The visible. */
	private boolean visible;

	/** The alignment. */
	private Alignment alignment;

	/**
	 * Instantiates a new abstract component.
	 */
	public AbstractComponent()
	{
		componentListeners = new LinkedList<ComponentListener>();
		parent = null;
		visible = true;
		preferredSizeOverride = null;
		alignment = Alignment.CENTER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#getParent()
	 */
	@Override
	public Container getParent()
	{
		return parent;
	}

	/**
	 * Sets the parent.
	 * 
	 * @param parent
	 *            the new parent
	 */
	protected void setParent(Container parent)
	{
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#addComponentListener(com.googlecode
	 * .lanterna.gui.listener.ComponentListener)
	 */
	@Override
	public void addComponentListener(ComponentListener cl)
	{
		if (cl != null)
		{
			componentListeners.add(cl);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#removeComponentListener(com.googlecode
	 * .lanterna.gui.listener.ComponentListener)
	 */
	@Override
	public void removeComponentListener(ComponentListener cl)
	{
		componentListeners.remove(cl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#isVisible()
	 */
	@Override
	public boolean isVisible()
	{
		return visible;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#isScrollable()
	 */
	@Override
	public boolean isScrollable()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#setPreferredSize(com.googlecode
	 * .lanterna.terminal.TerminalSize)
	 */
	@Override
	public void setPreferredSize(TerminalSize preferredSizeOverride)
	{
		this.preferredSizeOverride = preferredSizeOverride;
		invalidate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#getPreferredSize()
	 */
	@Override
	public TerminalSize getPreferredSize()
	{
		if (preferredSizeOverride != null)
		{
			return preferredSizeOverride;
		}
		else
		{
			return calculatePreferredSize();
		}
	}

	/**
	 * Calculate preferred size.
	 * 
	 * @return the terminal size
	 */
	protected abstract TerminalSize calculatePreferredSize();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#getMinimumSize()
	 */
	@Override
	public TerminalSize getMinimumSize()
	{
		return new TerminalSize(1, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#getAlignment()
	 */
	@Override
	public Alignment getAlignment()
	{
		return alignment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#setAlignment(org.storm.console.ui.framework
	 * .gui.Component.Alignment)
	 */
	@Override
	public void setAlignment(Alignment alignment)
	{
		if (alignment == null)
		{
			throw new IllegalArgumentException("Alignment argument to " + "AbstractComponent.setAlignment(...) cannot be null");
		}
		this.alignment = alignment;
	}

	/**
	 * Invalidate.
	 */
	protected void invalidate()
	{
		for (ComponentListener cl : componentListeners)
		{
			cl.onComponentInvalidated(this);
		}

		if (parent != null && parent instanceof AbstractContainer)
		{
			((AbstractContainer) parent).invalidate();
		}
	}

	/**
	 * Gets the component listeners.
	 * 
	 * @return the component listeners
	 */
	protected List<ComponentListener> getComponentListeners()
	{
		// This isn't thread safe either, but at least the list can't be
		// modified
		return Collections.unmodifiableList(componentListeners);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Component#getWindow()
	 */
	@Override
	public Window getWindow()
	{
		Container container = getParent();
		if (container != null)
		{
			return container.getWindow();
		}
		return null;
	}

	/**
	 * Gets the gUI screen.
	 * 
	 * @return the gUI screen
	 */
	protected GUIScreen getGUIScreen()
	{
		Window window = getWindow();
		if (window == null)
		{
			return null;
		}
		return window.getOwner();
	}

	/**
	 * Transform according to alignment.
	 * 
	 * @param graphics
	 *            the graphics
	 * @param preferredSize
	 *            the preferred size
	 * @return the text graphics
	 */
	protected TextGraphics transformAccordingToAlignment(TextGraphics graphics, TerminalSize preferredSize)
	{
		if (graphics.getWidth() <= preferredSize.getColumns() && graphics.getHeight() <= preferredSize.getRows())
		{

			// Don't do anything, return the original TextGraphics
			return graphics;
		}
		if (alignment == Alignment.FILL)
		{
			// For FILL, we also want to return it like it is
			return graphics;
		}

		int leftPosition = 0;
		if (alignment == Alignment.TOP_CENTER || alignment == Alignment.CENTER || alignment == Alignment.BOTTON_CENTER)
		{
			leftPosition = (graphics.getWidth() - preferredSize.getColumns()) / 2;
		}
		else if (alignment == Alignment.TOP_RIGHT || alignment == Alignment.RIGHT_CENTER || alignment == Alignment.BOTTOM_RIGHT)
		{
			leftPosition = graphics.getWidth() - preferredSize.getColumns();
		}

		int topPosition = 0;
		if (alignment == Alignment.LEFT_CENTER || alignment == Alignment.CENTER || alignment == Alignment.RIGHT_CENTER)
		{
			topPosition = (graphics.getHeight() - preferredSize.getRows()) / 2;
		}
		else if (alignment == Alignment.BOTTOM_LEFT || alignment == Alignment.BOTTON_CENTER || alignment == Alignment.BOTTOM_RIGHT)
		{
			topPosition = graphics.getHeight() - preferredSize.getRows();
		}
		return graphics.subAreaGraphics(new TerminalPosition(leftPosition, topPosition), preferredSize);
	}
}
