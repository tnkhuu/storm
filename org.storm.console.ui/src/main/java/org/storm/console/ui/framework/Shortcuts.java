/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;

/**
 * The Class Shortcuts.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Shortcuts
{

	/** The special keys shortcut. */
	private final Map<Kind, Action> specialKeysShortcut;

	/** The normal keys shortcut. */
	private final Map<NormalKeyShortcut, Action> normalKeysShortcut;

	/**
	 * Instantiates a new shortcut helper.
	 */
	public Shortcuts()
	{
		specialKeysShortcut = new EnumMap<Kind, Action>(Kind.class);
		normalKeysShortcut = new HashMap<NormalKeyShortcut, Action>();
	}

	/**
	 * Adds the shortcut.
	 * 
	 * @param kindOfKey
	 *            the kind of key
	 * @param action
	 *            the action
	 */
	public void addShortcut(Kind kindOfKey, Action action)
	{
		if (kindOfKey == Kind.NormalKey)
		{
			throw new IllegalArgumentException("Can't bind a normal key shortcut using this method, " + "please use addShortcut(char, boolean, boolean, Action) instead");
		}
		if (kindOfKey == null || action == null)
		{
			throw new IllegalArgumentException("Called addShortcut with either a null kind or null action");
		}

		synchronized (specialKeysShortcut)
		{
			specialKeysShortcut.put(kindOfKey, action);
		}
	}

	/**
	 * Adds the shortcut.
	 * 
	 * @param character
	 *            the character
	 * @param withCtrl
	 *            the with ctrl
	 * @param withAlt
	 *            the with alt
	 * @param action
	 *            the action
	 */
	public void addShortcut(char character, boolean withCtrl, boolean withAlt, Action action)
	{
		if (action == null)
		{
			throw new IllegalArgumentException("Called addShortcut with a null action");
		}
		synchronized (normalKeysShortcut)
		{
			normalKeysShortcut.put(new NormalKeyShortcut(character, withCtrl, withAlt), action);
		}
	}

	/**
	 * Trigger shortcut.
	 * 
	 * @param key
	 *            the key
	 * @return true, if successful
	 */
	public boolean triggerShortcut(Key key)
	{
		if (key.getKind() == Kind.NormalKey)
		{
			NormalKeyShortcut asShortcutKey = new NormalKeyShortcut(key.getCharacter(), key.isCtrlPressed(), key.isAltPressed());
			synchronized (normalKeysShortcut)
			{
				if (normalKeysShortcut.containsKey(asShortcutKey))
				{
					normalKeysShortcut.get(asShortcutKey).doAction();
					return true;
				}
				return false;
			}
		}
		else
		{
			synchronized (specialKeysShortcut)
			{
				if (specialKeysShortcut.containsKey(key.getKind()))
				{
					specialKeysShortcut.get(key.getKind()).doAction();
					return true;
				}
				return false;
			}
		}
	}

	/**
	 * The Class NormalKeyShortcut.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class NormalKeyShortcut
	{

		/** The character. */
		private final char character;

		/** The with ctrl. */
		private final boolean withCtrl;

		/** The with alt. */
		private final boolean withAlt;

		/**
		 * Instantiates a new normal key shortcut.
		 * 
		 * @param character
		 *            the character
		 * @param withCtrl
		 *            the with ctrl
		 * @param withAlt
		 *            the with alt
		 */
		public NormalKeyShortcut(char character, boolean withCtrl, boolean withAlt)
		{
			this.character = character;
			this.withCtrl = withCtrl;
			this.withAlt = withAlt;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode()
		{
			int hash = 3;
			hash = 71 * hash + this.character;
			hash = 71 * hash + (this.withCtrl ? 1 : 0);
			hash = 71 * hash + (this.withAlt ? 1 : 0);
			return hash;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (getClass() != obj.getClass())
			{
				return false;
			}
			final NormalKeyShortcut other = (NormalKeyShortcut) obj;
			if (this.character != other.character)
			{
				return false;
			}
			if (this.withCtrl != other.withCtrl)
			{
				return false;
			}
			if (this.withAlt != other.withAlt)
			{
				return false;
			}
			return true;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return (withCtrl ? "ctrl + " : "") + (withAlt ? "alt + " : "") + character;
		}
	}
}
