/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.ArrayList;
import java.util.List;

import org.storm.console.ui.framework.Interactable;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.terminal.ACS;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class AbstractListBox.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class AbstractListBox extends AbstractInteractableComponent
{

	/** The items. */
	private final List<Object> items;

	/** The preferred size override. */
	private TerminalSize preferredSizeOverride;

	/** The selected index. */
	private int selectedIndex;

	/** The scroll top index. */
	private int scrollTopIndex;

	/**
	 * Instantiates a new abstract list box.
	 */
	public AbstractListBox()
	{
		this(null);
	}

	/**
	 * Instantiates a new abstract list box.
	 * 
	 * @param preferredSize
	 *            the preferred size
	 */
	public AbstractListBox(TerminalSize preferredSize)
	{
		this.items = new ArrayList<Object>();
		this.preferredSizeOverride = preferredSize;
		this.selectedIndex = -1;
		this.scrollTopIndex = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		if (preferredSizeOverride != null)
		{
			return preferredSizeOverride;
		}

		int maxWidth = 5; // Set it to something...
		for (int i = 0; i < items.size(); i++)
		{
			String itemString = createItemString(i);
			if (itemString.length() > maxWidth)
			{
				maxWidth = itemString.length();
			}
		}
		return new TerminalSize(maxWidth + 1, items.size());
	}

	/**
	 * Adds the item.
	 * 
	 * @param item
	 *            the item
	 */
	protected void addItem(Object item)
	{
		if (item == null)
		{
			return;
		}

		items.add(item);
		if (selectedIndex == -1)
		{
			selectedIndex = 0;
		}
		invalidate();
	}

	/**
	 * Clear items.
	 */
	public void clearItems()
	{
		items.clear();
		selectedIndex = -1;
		invalidate();
	}

	/**
	 * Index of.
	 * 
	 * @param item
	 *            the item
	 * @return the int
	 */
	public int indexOf(Object item)
	{
		return items.indexOf(item);
	}

	/**
	 * Gets the size.
	 * 
	 * @return the size
	 */
	public int getSize()
	{
		return items.size();
	}

	/**
	 * Gets the item at.
	 * 
	 * @param index
	 *            the index
	 * @return the item at
	 */
	public Object getItemAt(int index)
	{
		return items.get(index);
	}

	/**
	 * Gets the nr of items.
	 * 
	 * @return the nr of items
	 */
	public int getNrOfItems()
	{
		return items.size();
	}

	/**
	 * Sets the selected item.
	 * 
	 * @param index
	 *            the new selected item
	 */
	public void setSelectedItem(int index)
	{
		selectedIndex = index;
		invalidate();
	}

	/**
	 * Gets the selected index.
	 * 
	 * @return the selected index
	 */
	public int getSelectedIndex()
	{
		return selectedIndex;
	}

	/**
	 * Gets the selected item.
	 * 
	 * @return the selected item
	 */
	public Object getSelectedItem()
	{
		if (selectedIndex == -1)
		{
			return null;
		}
		else
		{
			return items.get(selectedIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractComponent#isScrollable()
	 */
	@Override
	public boolean isScrollable()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		if (selectedIndex != -1)
		{
			if (selectedIndex < scrollTopIndex)
			{
				scrollTopIndex = selectedIndex;
			}
			else if (selectedIndex >= graphics.getHeight() + scrollTopIndex)
			{
				scrollTopIndex = selectedIndex - graphics.getHeight() + 1;
			}
		}

		// Do we need to recalculate the scroll position?
		// This code would be triggered by resizing the window when the scroll
		// position is at the bottom
		if (items.size() > graphics.getHeight() && items.size() - scrollTopIndex < graphics.getHeight())
		{
			scrollTopIndex = items.size() - graphics.getHeight();
		}

		graphics.applyTheme(getListItemThemeDefinition(graphics.getTheme()));
		graphics.fillArea(' ');

		for (int i = scrollTopIndex; i < items.size(); i++)
		{
			if (i - scrollTopIndex >= graphics.getHeight())
			{
				break;
			}

			if (i == selectedIndex && hasFocus())
			{
				graphics.applyTheme(getSelectedListItemThemeDefinition(graphics.getTheme()));
			}
			else
			{
				graphics.applyTheme(getListItemThemeDefinition(graphics.getTheme()));
			}
			printItem(graphics, 0, 0 + i - scrollTopIndex, i);
		}

		if (items.size() > graphics.getHeight())
		{
			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			graphics.drawString(graphics.getWidth() - 1, 0, ACS.ARROW_UP + "");

			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			for (int i = 1; i < graphics.getHeight() - 1; i++)
			{
				graphics.drawString(graphics.getWidth() - 1, i, ACS.BLOCK_MIDDLE + "");
			}

			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			graphics.drawString(graphics.getWidth() - 1, graphics.getHeight() - 1, ACS.ARROW_DOWN + "");

			// Finally print the 'tick'
			int scrollableSize = items.size() - graphics.getHeight();
			double position = (double) scrollTopIndex / (double) scrollableSize;
			int tickPosition = (int) ((graphics.getHeight() - 3.0) * position);

			graphics.applyTheme(Theme.Category.SHADOW);
			graphics.drawString(graphics.getWidth() - 1, 1 + tickPosition, " ");
		}
		if (selectedIndex == -1 || items.isEmpty())
		{
			setHotspot(new TerminalPosition(0, 0));
		}
		else
		{
			setHotspot(graphics.translateToGlobalCoordinates(new TerminalPosition(getHotSpotPositionOnLine(selectedIndex), selectedIndex - scrollTopIndex)));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractInteractableComponent#
	 * afterEnteredFocus
	 * (org.storm.console.ui.framework.gui.Interactable.FocusChangeDirection)
	 */
	@Override
	protected void afterEnteredFocus(FocusChangeDirection direction)
	{
		if (items.isEmpty())
		{
			return;
		}

		if (direction == FocusChangeDirection.DOWN)
		{
			selectedIndex = 0;
		}
		else if (direction == FocusChangeDirection.UP)
		{
			selectedIndex = items.size() - 1;
		}
	}

	/**
	 * Gets the selected list item theme definition.
	 * 
	 * @param theme
	 *            the theme
	 * @return the selected list item theme definition
	 */
	protected Theme.Definition getSelectedListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Theme.Category.LIST_ITEM_SELECTED);
	}

	/**
	 * Gets the list item theme definition.
	 * 
	 * @param theme
	 *            the theme
	 * @return the list item theme definition
	 */
	protected Theme.Definition getListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Category.LIST_ITEM);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Interactable#keyboardInteraction(com.googlecode
	 * .lanterna.input.Key)
	 */
	@Override
	public Result keyboardInteraction(Key key)
	{
		try
		{
			switch (key.getKind())
			{
			case Tab:
			case ArrowRight:
				return Result.NEXT_INTERACTABLE_RIGHT;

			case ReverseTab:
			case ArrowLeft:
				return Result.PREVIOUS_INTERACTABLE_LEFT;

			case ArrowDown:
				if (items.isEmpty() || selectedIndex == items.size() - 1)
				{
					return Result.NEXT_INTERACTABLE_DOWN;
				}

				selectedIndex++;
				break;

			case ArrowUp:
				if (items.isEmpty() || selectedIndex == 0)
				{
					return Result.PREVIOUS_INTERACTABLE_UP;
				}

				selectedIndex--;
				if (selectedIndex - scrollTopIndex < 0)
				{
					scrollTopIndex--;
				}
				break;

			default:
				return unhandledKeyboardEvent(key);
			}
			return Result.EVENT_HANDLED;
		}
		finally
		{
			invalidate();
		}
	}

	/**
	 * Prints the item.
	 * 
	 * @param graphics
	 *            the graphics
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param index
	 *            the index
	 */
	protected void printItem(TextGraphics graphics, int x, int y, int index)
	{
		String asText = createItemString(index);
		if (asText.length() > graphics.getWidth())
		{
			asText = asText.substring(0, graphics.getWidth());
		}
		graphics.drawString(x, y, asText);
	}

	/**
	 * Unhandled keyboard event.
	 * 
	 * @param key
	 *            the key
	 * @return the interactable. result
	 */
	protected Interactable.Result unhandledKeyboardEvent(Key key)
	{
		return Result.EVENT_NOT_HANDLED;
	}

	/**
	 * Gets the hot spot position on line.
	 * 
	 * @param selectedIndex
	 *            the selected index
	 * @return the hot spot position on line
	 */
	protected int getHotSpotPositionOnLine(int selectedIndex)
	{
		return 0;
	}

	/**
	 * Creates the item string.
	 * 
	 * @param index
	 *            the index
	 * @return the string
	 */
	protected abstract String createItemString(int index);
}
