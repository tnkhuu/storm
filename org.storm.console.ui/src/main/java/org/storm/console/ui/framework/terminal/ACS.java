/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

/**
 * The Class ACS.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ACS
{

	/**
	 * Instantiates a new acs.
	 */
	private ACS()
	{
	}

	/** The Constant ULCORNER. */
	public static final char ULCORNER = 0x250C;

	/** The Constant URCORNER. */
	public static final char URCORNER = 0x2510;

	/** The Constant LLCORNER. */
	public static final char LLCORNER = 0x2514;

	/** The Constant LRCORNER. */
	public static final char LRCORNER = 0x2518;

	/** The Constant HLINE. */
	public static final char HLINE = 0x2500;

	/** The Constant VLINE. */
	public static final char VLINE = 0x2502;

	/** The Constant FACE_WHITE. */
	public static final char FACE_WHITE = 0x263A;

	/** The Constant FACE_BLACK. */
	public static final char FACE_BLACK = 0x263B;

	/** The Constant HEART. */
	public static final char HEART = 0x2665;

	/** The Constant CLUB. */
	public static final char CLUB = 0x2663;

	/** The Constant DIAMOND. */
	public static final char DIAMOND = 0x2666;

	/** The Constant SPADES. */
	public static final char SPADES = 0x2660;

	/** The Constant DOT. */
	public static final char DOT = 0x2022;

	/** The Constant ARROW_UP. */
	public static final char ARROW_UP = 0x2191;

	/** The Constant ARROW_DOWN. */
	public static final char ARROW_DOWN = 0x2193;

	/** The Constant ARROW_RIGHT. */
	public static final char ARROW_RIGHT = 0x2192;

	/** The Constant ARROW_LEFT. */
	public static final char ARROW_LEFT = 0x2190;

	/** The Constant BLOCK_SOLID. */
	public static final char BLOCK_SOLID = 0x2588;

	/** The Constant BLOCK_DENSE. */
	public static final char BLOCK_DENSE = 0x2593;

	/** The Constant BLOCK_MIDDLE. */
	public static final char BLOCK_MIDDLE = 0x2592;

	/** The Constant BLOCK_SPARSE. */
	public static final char BLOCK_SPARSE = 0x2591;

	/** The Constant SINGLE_LINE_HORIZONTAL. */
	public static final char SINGLE_LINE_HORIZONTAL = HLINE;

	/** The Constant DOUBLE_LINE_HORIZONTAL. */
	public static final char DOUBLE_LINE_HORIZONTAL = 0x2550;

	/** The Constant SINGLE_LINE_VERTICAL. */
	public static final char SINGLE_LINE_VERTICAL = VLINE;

	/** The Constant DOUBLE_LINE_VERTICAL. */
	public static final char DOUBLE_LINE_VERTICAL = 0x2551;

	/** The Constant SINGLE_LINE_UP_LEFT_CORNER. */
	public static final char SINGLE_LINE_UP_LEFT_CORNER = ULCORNER;

	/** The Constant DOUBLE_LINE_UP_LEFT_CORNER. */
	public static final char DOUBLE_LINE_UP_LEFT_CORNER = 0x2554;

	/** The Constant SINGLE_LINE_UP_RIGHT_CORNER. */
	public static final char SINGLE_LINE_UP_RIGHT_CORNER = URCORNER;

	/** The Constant DOUBLE_LINE_UP_RIGHT_CORNER. */
	public static final char DOUBLE_LINE_UP_RIGHT_CORNER = 0x2557;

	/** The Constant SINGLE_LINE_LOW_LEFT_CORNER. */
	public static final char SINGLE_LINE_LOW_LEFT_CORNER = LLCORNER;

	/** The Constant DOUBLE_LINE_LOW_LEFT_CORNER. */
	public static final char DOUBLE_LINE_LOW_LEFT_CORNER = 0x255A;

	/** The Constant SINGLE_LINE_LOW_RIGHT_CORNER. */
	public static final char SINGLE_LINE_LOW_RIGHT_CORNER = LRCORNER;

	/** The Constant DOUBLE_LINE_LOW_RIGHT_CORNER. */
	public static final char DOUBLE_LINE_LOW_RIGHT_CORNER = 0x255D;

	/** The Constant SINGLE_LINE_CROSS. */
	public static final char SINGLE_LINE_CROSS = 0x253C;

	/** The Constant DOUBLE_LINE_CROSS. */
	public static final char DOUBLE_LINE_CROSS = 0x256C;

	/** The Constant SINGLE_LINE_T_UP. */
	public static final char SINGLE_LINE_T_UP = 0x2534;

	/** The Constant SINGLE_LINE_T_DOWN. */
	public static final char SINGLE_LINE_T_DOWN = 0x252C;

	/** The Constant SINGLE_LINE_T_RIGHT. */
	public static final char SINGLE_LINE_T_RIGHT = 0x251c;

	/** The Constant SINGLE_LINE_T_LEFT. */
	public static final char SINGLE_LINE_T_LEFT = 0x2524;

	/** The Constant SINGLE_LINE_T_DOUBLE_UP. */
	public static final char SINGLE_LINE_T_DOUBLE_UP = 0x256B;

	/** The Constant SINGLE_LINE_T_DOUBLE_DOWN. */
	public static final char SINGLE_LINE_T_DOUBLE_DOWN = 0x2565;

	/** The Constant SINGLE_LINE_T_DOUBLE_RIGHT. */
	public static final char SINGLE_LINE_T_DOUBLE_RIGHT = 0x255E;

	/** The Constant SINGLE_LINE_T_DOUBLE_LEFT. */
	public static final char SINGLE_LINE_T_DOUBLE_LEFT = 0x2561;

	/** The Constant DOUBLE_LINE_T_UP. */
	public static final char DOUBLE_LINE_T_UP = 0x2569;

	/** The Constant DOUBLE_LINE_T_DOWN. */
	public static final char DOUBLE_LINE_T_DOWN = 0x2566;

	/** The Constant DOUBLE_LINE_T_RIGHT. */
	public static final char DOUBLE_LINE_T_RIGHT = 0x2560;

	/** The Constant DOUBLE_LINE_T_LEFT. */
	public static final char DOUBLE_LINE_T_LEFT = 0x2563;

	/** The Constant DOUBLE_LINE_T_SINGLE_UP. */
	public static final char DOUBLE_LINE_T_SINGLE_UP = 0x2567;

	/** The Constant DOUBLE_LINE_T_SINGLE_DOWN. */
	public static final char DOUBLE_LINE_T_SINGLE_DOWN = 0x2564;

	/** The Constant DOUBLE_LINE_T_SINGLE_RIGHT. */
	public static final char DOUBLE_LINE_T_SINGLE_RIGHT = 0x255F;

	/** The Constant DOUBLE_LINE_T_SINGLE_LEFT. */
	public static final char DOUBLE_LINE_T_SINGLE_LEFT = 0x2562;
}
