/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.input;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.storm.console.ui.framework.terminal.TerminalPosition;

/**
 * The Class ScreenInfoCharacterPattern.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ScreenInfoCharacterPattern implements CharacterPattern
{

	/** The Constant REPORT_CURSOR_PATTERN. */
	private static final Pattern REPORT_CURSOR_PATTERN = Pattern.compile("\\[([0-9]+);([0-9]+)R");

	/**
	 * Instantiates a new screen info character pattern.
	 */
	public ScreenInfoCharacterPattern()
	{
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#getResult(java.util.List)
	 */
	@Override
	public Key getResult(List<Character> matching)
	{
		return new Key(Kind.CursorLocation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#isCompleteMatch(java.util
	 * .List)
	 */
	@Override
	public boolean isCompleteMatch(List<Character> currentMatching)
	{
		if (currentMatching.isEmpty())
		{
			return false;
		}

		if (currentMatching.get(0) != KeyMappingProfile.ESC_CODE)
		{
			return false;
		}

		String asString = "";
		for (int i = 1; i < currentMatching.size(); i++)
		{
			asString += currentMatching.get(i);
		}

		Matcher matcher = REPORT_CURSOR_PATTERN.matcher(asString);
		if (!matcher.matches())
		{
			return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#matches(java.util.List)
	 */
	@Override
	public boolean matches(List<Character> currentMatching)
	{
		if (currentMatching.isEmpty())
		{
			return true;
		}

		if (currentMatching.get(0) != KeyMappingProfile.ESC_CODE)
		{
			return false;
		}
		if (currentMatching.size() == 1)
		{
			return true;
		}

		if (currentMatching.get(1) != '[')
		{
			return false;
		}
		if (currentMatching.size() == 2)
		{
			return true;
		}

		int i = 2;
		for (i = 2; i < currentMatching.size(); i++)
		{
			if (!Character.isDigit(currentMatching.get(i)) && ';' != currentMatching.get(i))
			{
				return false;
			}

			if (';' == currentMatching.get(i))
			{
				break;
			}
		}

		if (i == currentMatching.size())
		{
			return true;
		}

		for (i = i + 1; i < currentMatching.size(); i++)
		{
			if (!Character.isDigit(currentMatching.get(i)) && 'R' != currentMatching.get(i))
			{
				return false;
			}

			if ('R' == currentMatching.get(i))
			{
				break;
			}
		}

		return true;
	}

	/**
	 * Gets the cursor position.
	 * 
	 * @param currentMatching
	 *            the current matching
	 * @return the cursor position
	 */
	public static TerminalPosition getCursorPosition(List<Character> currentMatching)
	{
		if (currentMatching.isEmpty())
		{
			return null;
		}

		if (currentMatching.get(0) != KeyMappingProfile.ESC_CODE)
		{
			return null;
		}

		String asString = "";
		for (int i = 1; i < currentMatching.size(); i++)
		{
			asString += currentMatching.get(i);
		}

		Matcher matcher = REPORT_CURSOR_PATTERN.matcher(asString);
		if (!matcher.matches())
		{
			return null;
		}

		return new TerminalPosition(Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(1)));
	}
}
