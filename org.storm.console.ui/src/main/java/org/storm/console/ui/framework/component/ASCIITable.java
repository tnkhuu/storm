/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import static org.fusesource.jansi.Ansi.ansi;

import java.util.ArrayList;
import java.util.List;

import org.fusesource.jansi.Ansi;
import org.storm.console.ui.framework.Drawable;
import org.storm.console.ui.framework.Frame;
import org.storm.console.ui.framework.Position;
import org.storm.console.ui.framework.impl.FrameBuffer;
import org.storm.console.ui.framework.terminal.TerminalPosition;
public class ASCIITable implements Drawable
{
	public static final int ALIGN_LEFT = -1;
	public static final int ALIGN_CENTER = 0;
	public static final int ALIGN_RIGHT = 1;
	public static final int DEFAULT_HEADER_ALIGN = ALIGN_CENTER;
	public static final int DEFAULT_DATA_ALIGN = ALIGN_RIGHT;
	protected ASCIITableHeader[] headers;
	private int width;
	private int length;
	protected CharSequence[][] data;
	private Position position;
	private Ansi.Color color = Ansi.Color.GREEN;

	protected ASCIITable(String[] headers, String[][] data)
	{

		List<ASCIITableHeader> heads = new ArrayList<>();
		for (String header : headers)
		{
			heads.add(new ASCIITableHeader(header));
		}
		this.headers = heads.toArray(new ASCIITableHeader[headers.length]);
		this.data = data;
	}

	protected ASCIITable(ASCIITableHeader[] headers, String[][] data)
	{
		this.headers = headers;
		this.data = data;
	}
	
	protected ASCIITable(ASCIITableHeader[] headers, CharSequence[][] data, Ansi.Color color)
	{
		this.headers = headers;
		this.data = data;
		this.color = color;
	}

	public ASCIITable()
	{

	}
	
	@Override
	public void draw(Frame canvas)
	{
		canvas.draw(this);
		
	}

	@Override
	public void write(FrameBuffer buffer)
	{
	buffer.append(getTable(headers, data));
		
	}

	@Override
	public Position getPosition()
	{
		return position == null ? new TerminalPosition(0, 0) : position;
	}

	public void printTable()
	{
		System.out.println(getTable(headers, data));
	}

	private String getTable(ASCIITableHeader[] headerObjs, CharSequence[][] data)
	{

		if (data == null || data.length == 0)
		{
			throw new IllegalArgumentException("Please provide valid data : " + data);
		}

		/**
		 * Table String buffer
		 */
		StringBuilder tableBuf = new StringBuilder();

		/**
		 * Get maximum number of columns across all rows
		 */
		String[] header = getHeaders(headerObjs);
		int colCount = getMaxColumns(header, data);

		/**
		 * Get max length of data in each column
		 */
		List<Integer> colMaxLenList = getMaxColLengths(colCount, header, data);

		/**
		 * Check for the existence of header
		 */
		if (header != null && header.length > 0)
		{
			/**
			 * 1. Row line
			 */
			tableBuf.append(getRowLineBuf(colCount, colMaxLenList, data));

			/**
			 * 2. Header line
			 */
			tableBuf.append(getRowDataBuf(colCount, colMaxLenList, header, headerObjs, true));
		}

		/**
		 * 3. Data Row lines
		 */
		tableBuf.append(getRowLineBuf(colCount, colMaxLenList, data));
		CharSequence[] rowData = null;

		// Build row data buffer by iterating through all rows
		for (int i = 0; i < data.length; i++)
		{

			// Build cell data in each row
			rowData = new String[colCount];
			for (int j = 0; j < colCount; j++)
			{

				if (j < data[i].length)
				{
					rowData[j] = data[i][j];
				}
				else
				{
					rowData[j] = "";
				}
			}

			tableBuf.append(getRowDataBuf(colCount, colMaxLenList, rowData, headerObjs, false));
		}

		/**
		 * 4. Row line
		 */
		tableBuf.append(getRowLineBuf(colCount, colMaxLenList, data));
		length = data.length + 4;
		return tableBuf.toString();
	}

	public int getWidth()
	{
		return width;
	}

	public int getLength()
	{
		return length;
	}

	private String getRowDataBuf(int colCount, List<Integer> colMaxLenList, CharSequence[] row, ASCIITableHeader[] headerObjs, boolean isHeader)
	{

		StringBuilder rowBuilder = new StringBuilder();
		String formattedData = null;
		int align;

		for (int i = 0; i < colCount; i++)
		{

			align = isHeader ? DEFAULT_HEADER_ALIGN : DEFAULT_DATA_ALIGN;

			if (headerObjs != null && i < headerObjs.length)
			{
				if (isHeader)
				{
					align = headerObjs[i].getHeaderAlign();
				}
				else
				{
					align = headerObjs[i].getDataAlign();
				}
			}

			formattedData = i < row.length ? row[i].toString() : "";

			// format = "| %" + colFormat.get(i) + "s ";
			formattedData = "| " + getFormattedData(colMaxLenList.get(i), formattedData, align) + " ";

			if (i + 1 == colCount)
			{
				formattedData = formattedData.toString() + "|";
			}

			rowBuilder.append(formattedData);
		}

		return rowBuilder.append("\n").toString();
	}

	private CharSequence getFormattedData(int maxLength, CharSequence data, int align)
	{

		if (data.length() > maxLength)
		{
			return data;
		}

		boolean toggle = true;

		while (data.length() < maxLength)
		{

			if (align == ALIGN_LEFT)
			{
				data = data + " ";
			}
			else if (align == ALIGN_RIGHT)
			{
				data = " " + data;
			}
			else if (align == ALIGN_CENTER)
			{
				if (toggle)
				{
					data = " " + data;
					toggle = false;
				}
				else
				{
					data = data + " ";
					toggle = true;
				}
			}
		}

		return ansi().fg(color).a(data).reset().toString();
	}

	/**
	 * Each string item rendering requires the border and a space on both sides.
	 * 
	 * 12 3 12 3 12 34 +----- +-------- +------+ abc something last
	 * 
	 * @param colCount
	 * @param colMaxLenList
	 * @param data
	 * @return
	 */
	private String getRowLineBuf(int colCount, List<Integer> colMaxLenList, CharSequence[][] data)
	{

		StringBuilder rowBuilder = new StringBuilder();
		int colWidth = 0;

		for (int i = 0; i < colCount; i++)
		{

			colWidth = colMaxLenList.get(i) + 3;

			for (int j = 0; j < colWidth; j++)
			{
				if (j == 0)
				{
					rowBuilder.append("+");
				}
				else if ((i + 1 == colCount && j + 1 == colWidth))
				{// for last column close the border
					rowBuilder.append("-+");
				}
				else
				{
					rowBuilder.append("-");
				}
			}
		}
		String ret = rowBuilder.append("\n").toString();
		width = ret.length();
		return ret;
	}

	private int getMaxItemLength(List<String> colData)
	{
		int maxLength = 0;
		for (int i = 0; i < colData.size(); i++)
		{
			
			maxLength = Math.max(colData.get(i).length(), maxLength);
		}
		return maxLength;
	}

	private int getMaxColumns(CharSequence[] header, CharSequence[][] data)
	{
		int maxColumns = 0;
		for (int i = 0; i < data.length; i++)
		{
			maxColumns = Math.max(data[i].length, maxColumns);
		}
		maxColumns = Math.max(header.length, maxColumns);
		return maxColumns;
	}

	private List<Integer> getMaxColLengths(int colCount, String[] header, CharSequence[][] data)
	{

		List<Integer> colMaxLenList = new ArrayList<Integer>(colCount);
		List<String> colData = null;
		int maxLength;

		for (int i = 0; i < colCount; i++)
		{
			colData = new ArrayList<String>();

			if (header != null && i < header.length)
			{
				colData.add(header[i]);
			}

			for (int j = 0; j < data.length; j++)
			{
				if (i < data[j].length)
				{
					colData.add(data[j][i].toString());
				}
				else
				{
					colData.add("");
				}
			}

			maxLength = getMaxItemLength(colData);
			colMaxLenList.add(maxLength);
		}

		return colMaxLenList;
	}

	private String[] getHeaders(ASCIITableHeader[] headerObjs)
	{
		String[] header = new String[0];
		if (headerObjs != null && headerObjs.length > 0)
		{
			header = new String[headerObjs.length];
			for (int i = 0; i < headerObjs.length; i++)
			{
				header[i] = headerObjs[i].getHeaderName();
			}
		}

		return header;
	}

	public void setColor(Ansi.Color color) {
		this.color = color;
	}

}
