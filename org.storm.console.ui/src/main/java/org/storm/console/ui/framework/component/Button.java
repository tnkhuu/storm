/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Interactable;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class Button.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Button extends AbstractInteractableComponent
{

	/** The button label. */
	private Label buttonLabel;

	/** The on press event. */
	private Action onPressEvent;

	/**
	 * Instantiates a new button.
	 * 
	 * @param text
	 *            the text
	 */
	public Button(String text)
	{
		this(text, new Action()
		{
			@Override
			public void doAction()
			{
			}
		});
	}

	/**
	 * Instantiates a new button.
	 * 
	 * @param text
	 *            the text
	 * @param onPressEvent
	 *            the on press event
	 */
	public Button(String text, Action onPressEvent)
	{
		this.onPressEvent = onPressEvent;
		this.buttonLabel = new Label(text);
		this.buttonLabel.setStyle(Theme.Category.BUTTON_LABEL_INACTIVE);

		if (this.onPressEvent == null)
		{
			this.onPressEvent = new Action()
			{
				@Override
				public void doAction()
				{
				}
			};
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		if (hasFocus())
		{
			graphics.applyTheme(graphics.getTheme().getDefinition(Theme.Category.BUTTON_ACTIVE));
		}
		else
		{
			graphics.applyTheme(graphics.getTheme().getDefinition(Theme.Category.BUTTON_INACTIVE));
		}

		TerminalSize preferredSize = calculatePreferredSize();
		graphics = transformAccordingToAlignment(graphics, preferredSize);

		if (graphics.getWidth() < preferredSize.getColumns())
		{
			graphics.drawString(0, 0, "< ");
			graphics.drawString(graphics.getWidth() - 2, 0, " >");

			int allowedSize = graphics.getWidth() - 4;
			if (allowedSize > 0)
			{
				TextGraphics subGraphics = graphics.subAreaGraphics(new TerminalPosition(2, 0), new TerminalSize(allowedSize, buttonLabel.getPreferredSize().getRows()));
				buttonLabel.repaint(subGraphics);
			}
		}
		else
		{
			int leftPosition = (graphics.getWidth() - preferredSize.getColumns()) / 2;
			graphics.drawString(leftPosition, 0, "< ");
			final TerminalSize labelPrefSize = buttonLabel.getPreferredSize();
			TextGraphics subGraphics = graphics.subAreaGraphics(new TerminalPosition(leftPosition + 2, 0), new TerminalSize(labelPrefSize.getColumns(), labelPrefSize.getRows()));
			buttonLabel.repaint(subGraphics);
			graphics.drawString(leftPosition + 2 + labelPrefSize.getColumns(), 0, " >");
		}
		setHotspot(null);
	}

	/**
	 * Sets the text.
	 * 
	 * @param text
	 *            the new text
	 */
	public void setText(String text)
	{
		this.buttonLabel.setText(text);
	}

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	public String getText()
	{
		return buttonLabel.getText();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		TerminalSize labelSize = buttonLabel.getPreferredSize();
		return new TerminalSize(labelSize.getColumns() + 2 + 2, labelSize.getRows());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractInteractableComponent#
	 * afterEnteredFocus
	 * (org.storm.console.ui.framework.gui.Interactable.FocusChangeDirection)
	 */
	@Override
	public void afterEnteredFocus(FocusChangeDirection direction)
	{
		buttonLabel.setStyle(Theme.Category.BUTTON_LABEL_ACTIVE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractInteractableComponent#
	 * afterLeftFocus
	 * (org.storm.console.ui.framework.gui.Interactable.FocusChangeDirection)
	 */
	@Override
	public void afterLeftFocus(FocusChangeDirection direction)
	{
		buttonLabel.setStyle(Theme.Category.BUTTON_LABEL_INACTIVE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Interactable#keyboardInteraction(com.googlecode
	 * .lanterna.input.Key)
	 */
	@Override
	public Interactable.Result keyboardInteraction(Key key)
	{
		switch (key.getKind())
		{
		case Enter:
			onPressEvent.doAction();
			return Result.EVENT_HANDLED;

		case ArrowDown:
			return Result.NEXT_INTERACTABLE_DOWN;

		case ArrowRight:
		case Tab:
			return Result.NEXT_INTERACTABLE_RIGHT;

		case ArrowUp:
			return Result.PREVIOUS_INTERACTABLE_UP;

		case ArrowLeft:
		case ReverseTab:
			return Result.PREVIOUS_INTERACTABLE_LEFT;

		default:
			return Result.EVENT_NOT_HANDLED;
		}
	}
}
