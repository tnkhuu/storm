/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal.swing;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;

import org.storm.console.ui.framework.input.InputProvider;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.KeyMappingProfile;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.terminal.AbstractTerminal;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;
import org.storm.console.ui.framework.terminal.XTerm8bitIndexedColorUtils;

/**
 * The Class SwingTerminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SwingTerminal extends AbstractTerminal implements InputProvider
{

	/** The terminal renderer. */
	private final TerminalRenderer terminalRenderer;

	/** The blink timer. */
	private final Timer blinkTimer;

	/** The terminal frame. */
	private JFrame terminalFrame;

	/** The appearance. */
	private TerminalAppearance appearance;

	/** The character map. */
	private TerminalCharacter[][] characterMap;

	/** The text position. */
	private TerminalPosition textPosition;

	/** The current foreground color. */
	private TerminalCharacterColor currentForegroundColor;

	/** The current background color. */
	private TerminalCharacterColor currentBackgroundColor;

	/** The currently bold. */
	private boolean currentlyBold;

	/** The currently blinking. */
	private boolean currentlyBlinking;

	/** The currently underlined. */
	private boolean currentlyUnderlined;

	/** The blink visible. */
	private boolean blinkVisible;

	/** The cursor visible. */
	private boolean cursorVisible;

	/** The key queue. */
	private Queue<Key> keyQueue;

	/** The resize mutex. */
	private final Object resizeMutex;

	/**
	 * Instantiates a new swing terminal.
	 */
	public SwingTerminal()
	{
		this(160, 40); // By default, create a 160x40 terminal (normal size * 2)
	}

	/**
	 * Instantiates a new swing terminal.
	 * 
	 * @param terminalSize
	 *            the terminal size
	 */
	public SwingTerminal(TerminalSize terminalSize)
	{
		this(terminalSize.getColumns(), terminalSize.getRows());
	}

	/**
	 * Instantiates a new swing terminal.
	 * 
	 * @param widthInColumns
	 *            the width in columns
	 * @param heightInRows
	 *            the height in rows
	 */
	public SwingTerminal(int widthInColumns, int heightInRows)
	{
		this(TerminalAppearance.DEFAULT_APPEARANCE, widthInColumns, heightInRows);
	}

	/**
	 * Instantiates a new swing terminal.
	 * 
	 * @param appearance
	 *            the appearance
	 */
	public SwingTerminal(TerminalAppearance appearance)
	{
		this(appearance, 160, 40);
	}

	/**
	 * Instantiates a new swing terminal.
	 * 
	 * @param appearance
	 *            the appearance
	 * @param widthInColumns
	 *            the width in columns
	 * @param heightInRows
	 *            the height in rows
	 */
	public SwingTerminal(TerminalAppearance appearance, int widthInColumns, int heightInRows)
	{
		this.appearance = appearance;
		this.terminalRenderer = new TerminalRenderer();
		this.blinkTimer = new Timer(500, new BlinkAction());
		this.textPosition = new TerminalPosition(0, 0);
		this.characterMap = new TerminalCharacter[heightInRows][widthInColumns];
		this.currentForegroundColor = new CharacterANSIColor(Color.WHITE);
		this.currentBackgroundColor = new CharacterANSIColor(Color.BLACK);
		this.currentlyBold = false;
		this.currentlyBlinking = false;
		this.currentlyUnderlined = false;
		this.blinkVisible = false;
		this.cursorVisible = true;
		this.keyQueue = new ConcurrentLinkedQueue<Key>();
		this.resizeMutex = new Object();

		onResized(widthInColumns, heightInRows);
		clearScreen();
	}

	/**
	 * Gets the j frame.
	 * 
	 * @return the j frame
	 */
	public JFrame getJFrame()
	{
		return terminalFrame;
	}

	/**
	 * The Class BlinkAction.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class BlinkAction implements ActionListener
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(ActionEvent e)
		{
			blinkVisible = !blinkVisible;
			terminalRenderer.repaint();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.InputProvider#addInputProfile(com.googlecode
	 * .lanterna.input.KeyMappingProfile)
	 */
	@Override
	public void addInputProfile(KeyMappingProfile profile)
	{
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.terminal.Terminal#applyBackgroundColor(com.googlecode
	 * .lanterna.terminal.Terminal.Color)
	 */
	@Override
	public void applyBackgroundColor(Color color)
	{
		currentBackgroundColor = new CharacterANSIColor(color);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyBackgroundColor(int,
	 * int, int)
	 */
	@Override
	public void applyBackgroundColor(int r, int g, int b)
	{
		currentBackgroundColor = new Character24bitColor(new java.awt.Color(r, g, b));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyBackgroundColor(int)
	 */
	@Override
	public void applyBackgroundColor(int index)
	{
		currentBackgroundColor = new CharacterIndexedColor(index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.terminal.Terminal#applyForegroundColor(com.googlecode
	 * .lanterna.terminal.Terminal.Color)
	 */
	@Override
	public void applyForegroundColor(Color color)
	{
		currentForegroundColor = new CharacterANSIColor(color);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyForegroundColor(int,
	 * int, int)
	 */
	@Override
	public void applyForegroundColor(int r, int g, int b)
	{
		currentForegroundColor = new Character24bitColor(new java.awt.Color(r, g, b));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyForegroundColor(int)
	 */
	@Override
	public void applyForegroundColor(int index)
	{
		currentForegroundColor = new CharacterIndexedColor(index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.terminal.Terminal#applySGR(org.storm.console.ui.framework
	 * .terminal.Terminal.SGR[])
	 */
	@Override
	public void applySGR(SGR... options)
	{
		for (SGR sgr : options)
		{
			if (sgr == SGR.RESET_ALL)
			{
				currentlyBold = false;
				currentlyBlinking = false;
				currentlyUnderlined = false;
				currentForegroundColor = new CharacterANSIColor(Color.DEFAULT);
				currentBackgroundColor = new CharacterANSIColor(Color.BLACK);
			}
			else if (sgr == SGR.ENTER_BOLD)
			{
				currentlyBold = true;
			}
			else if (sgr == SGR.EXIT_BOLD)
			{
				currentlyBold = false;
			}
			else if (sgr == SGR.ENTER_BLINK)
			{
				currentlyBlinking = true;
			}
			else if (sgr == SGR.EXIT_BLINK)
			{
				currentlyBlinking = false;
			}
			else if (sgr == SGR.ENTER_UNDERLINE)
			{
				currentlyUnderlined = true;
			}
			else if (sgr == SGR.EXIT_UNDERLINE)
			{
				currentlyUnderlined = false;
			}
		}
	}


	@Override
	public void clearScreen()
	{
		synchronized (resizeMutex)
		{
			for (int y = 0; y < size().getRows(); y++)
			{
				for (int x = 0; x < size().getColumns(); x++)
				{
					this.characterMap[y][x] = new TerminalCharacter(' ', new CharacterANSIColor(Color.DEFAULT), new CharacterANSIColor(Color.BLACK), false, false, false);
				}
			}
			moveCursor(0, 0);
		}
	}

	
	@Override
	public void enterPrivateMode()
	{
		terminalFrame = new JFrame("Terminal");
		terminalFrame.addComponentListener(new FrameResizeListener());
		terminalFrame.getContentPane().setLayout(new BorderLayout());
		terminalFrame.getContentPane().add(terminalRenderer, BorderLayout.CENTER);
		terminalFrame.addKeyListener(new KeyCapturer());
		terminalFrame.pack();
		terminalFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		terminalFrame.setLocationByPlatform(true);
		terminalFrame.setVisible(true);
		terminalFrame.setFocusTraversalKeysEnabled(false);
		// terminalEmulator.setSize(terminalEmulator.getPreferredSize());
		terminalFrame.pack();
		blinkTimer.start();
	}

	
	@Override
	public void exitPrivateMode()
	{
		if (terminalFrame == null)
		{
			return;
		}

		blinkTimer.stop();
		terminalFrame.setVisible(false);
		terminalFrame.dispose();
	}

	@Override
	public void moveCursor(int x, int y)
	{
		if (x < 0)
		{
			x = 0;
		}
		if (x >= size().getColumns())
		{
			x = size().getColumns() - 1;
		}
		if (y < 0)
		{
			y = 0;
		}
		if (y >= size().getRows())
		{
			y = size().getRows() - 1;
		}

		textPosition.setY(x);
		textPosition.setX(y);
		refreshScreen();
	}
	
	public TerminalPosition getTextPosition() {
		return textPosition;
	}


	@Override
	public void setCursorVisible(boolean visible)
	{
		this.cursorVisible = visible;
		refreshScreen();
	}

	@Override
	public synchronized void putCharacter(char c)
	{
		characterMap[textPosition.getX()][textPosition.getY()] = new TerminalCharacter(c, currentForegroundColor, currentBackgroundColor, currentlyBold, currentlyBlinking,
				currentlyUnderlined);
		if (textPosition.getY() == size().getColumns() - 1 && textPosition.getX() == size().getRows() - 1)
		{
			moveCursor(0, textPosition.getX());
		}
		if (textPosition.getY() == size().getColumns() - 1)
		{
			moveCursor(0, textPosition.getX() + 1);
		}
		else
		{
			moveCursor(textPosition.getY() + 1, textPosition.getX());
		}
	}


	@Override
	public TerminalSize queryTerminalSize()
	{
		// Just bypass to getTerminalSize()
		return getTerminalSize();
	}


	@Override
	public TerminalSize getTerminalSize()
	{
		return size();
	}

	/**
	 * Resize.
	 * 
	 * @param newSizeColumns
	 *            the new size columns
	 * @param newSizeRows
	 *            the new size rows
	 */
	private synchronized void resize(int newSizeColumns, int newSizeRows)
	{
		TerminalCharacter[][] newCharacterMap = new TerminalCharacter[newSizeRows][newSizeColumns];
		for (int y = 0; y < newSizeRows; y++)
		{
			for (int x = 0; x < newSizeColumns; x++)
			{
				newCharacterMap[y][x] = new TerminalCharacter(' ', new CharacterANSIColor(Color.WHITE), new CharacterANSIColor(Color.BLACK), false, false, false);
			}
		}

		synchronized (resizeMutex)
		{
			for (int y = 0; y < size().getRows() && y < newSizeRows; y++)
			{
				for (int x = 0; x < size().getColumns() && x < newSizeColumns; x++)
				{
					newCharacterMap[y][x] = this.characterMap[y][x];
				}
			}

			this.characterMap = newCharacterMap;
			SwingUtilities.invokeLater(new Runnable()
			{
				@Override
				public void run()
				{
					terminalFrame.pack();
				}
			});

			onResized(newSizeColumns, newSizeRows);
		}
	}


	@Override
	public Key readInput()
	{
		return keyQueue.poll();
	}


	@Override
	public void flush()
	{
		// Not needed
	}

	/**
	 * Sets the terminal palette.
	 * 
	 * @param palette
	 *            the new terminal palette
	 */
	public void setTerminalPalette(TerminalPalette palette)
	{
		appearance = appearance.withPalette(palette);
		refreshScreen();
	}

	/**
	 * Refresh screen.
	 */
	private void refreshScreen()
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				terminalRenderer.repaint();
			}
		});
	}

	/**
	 * Size.
	 * 
	 * @return the terminal size
	 */
	private TerminalSize size()
	{
		return getLastKnownSize();
	}

	/**
	 * The Class KeyCapturer.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class KeyCapturer extends KeyAdapter
	{

		/** The typed ignore. */
		private Set<Character> typedIgnore = new HashSet<Character>(Arrays.asList('\n', '\t', '\r', '\b', '\33'));

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.event.KeyAdapter#keyTyped(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyTyped(KeyEvent e)
		{
			char character = e.getKeyChar();
			boolean altDown = (e.getModifiersEx() & InputEvent.ALT_DOWN_MASK) != 0;
			boolean ctrlDown = (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0;

			if (!typedIgnore.contains(character))
			{
				if (ctrlDown)
				{
					// We need to re-adjust the character if ctrl is pressed,
					// just like for the AnsiTerminal
					character = (char) ('a' - 1 + character);
				}
				keyQueue.add(new Key(character, ctrlDown, altDown));
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyPressed(KeyEvent e)
		{
			if (e.getKeyCode() == KeyEvent.VK_ENTER)
			{
				keyQueue.add(new Key(Kind.Enter));
			}
			else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
			{
				keyQueue.add(new Key(Kind.Escape));
			}
			else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
			{
				keyQueue.add(new Key(Kind.Backspace));
			}
			else if (e.getKeyCode() == KeyEvent.VK_LEFT)
			{
				keyQueue.add(new Key(Kind.ArrowLeft));
			}
			else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			{
				keyQueue.add(new Key(Kind.ArrowRight));
			}
			else if (e.getKeyCode() == KeyEvent.VK_UP)
			{
				keyQueue.add(new Key(Kind.ArrowUp));
			}
			else if (e.getKeyCode() == KeyEvent.VK_DOWN)
			{
				keyQueue.add(new Key(Kind.ArrowDown));
			}
			else if (e.getKeyCode() == KeyEvent.VK_INSERT)
			{
				keyQueue.add(new Key(Kind.Insert));
			}
			else if (e.getKeyCode() == KeyEvent.VK_DELETE)
			{
				keyQueue.add(new Key(Kind.Delete));
			}
			else if (e.getKeyCode() == KeyEvent.VK_HOME)
			{
				keyQueue.add(new Key(Kind.Home));
			}
			else if (e.getKeyCode() == KeyEvent.VK_END)
			{
				keyQueue.add(new Key(Kind.End));
			}
			else if (e.getKeyCode() == KeyEvent.VK_PAGE_UP)
			{
				keyQueue.add(new Key(Kind.PageUp));
			}
			else if (e.getKeyCode() == KeyEvent.VK_PAGE_DOWN)
			{
				keyQueue.add(new Key(Kind.PageDown));
			}
			else if (e.getKeyCode() == KeyEvent.VK_TAB)
			{
				if (e.isShiftDown())
				{
					keyQueue.add(new Key(Kind.ReverseTab));
				}
				else
				{
					keyQueue.add(new Key(Kind.Tab));
				}
			}
			else
			{
				// keyTyped doesn't catch this scenario (for whatever reason...)
				// so we have to do it here
				boolean altDown = (e.getModifiersEx() & InputEvent.ALT_DOWN_MASK) != 0;
				boolean ctrlDown = (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0;
				if (altDown && ctrlDown && e.getKeyCode() >= 'A' && e.getKeyCode() <= 'Z')
				{
					char asLowerCase = Character.toLowerCase((char) e.getKeyCode());
					keyQueue.add(new Key(asLowerCase, true, true));
				}
			}
		}
	}

	/**
	 * The listener interface for receiving frameResize events. The class that
	 * is interested in processing a frameResize event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addFrameResizeListener<code> method. When
	 * the frameResize event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see FrameResizeEvent
	 */
	private class FrameResizeListener extends ComponentAdapter
	{

		/** The last width. */
		private int lastWidth = -1;

		/** The last height. */
		private int lastHeight = -1;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.event.ComponentAdapter#componentResized(java.awt.event.
		 * ComponentEvent)
		 */
		@Override
		public void componentResized(ComponentEvent e)
		{
			if (e.getComponent() == null || e.getComponent() instanceof JFrame == false)
			{
				return;
			}

			JFrame frame = (JFrame) e.getComponent();
			Container contentPane = frame.getContentPane();
			int newWidth = contentPane.getWidth();
			int newHeight = contentPane.getHeight();

			FontMetrics fontMetrics = frame.getGraphics().getFontMetrics(appearance.getNormalTextFont());
			int consoleWidth = newWidth / fontMetrics.charWidth(' ');
			int consoleHeight = newHeight / fontMetrics.getHeight();

			if (consoleWidth == lastWidth && consoleHeight == lastHeight)
			{
				return;
			}

			lastWidth = consoleWidth;
			lastHeight = consoleHeight;

			resize(consoleWidth, consoleHeight);
		}
	}

	/**
	 * The Class TerminalRenderer.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class TerminalRenderer extends JComponent
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Instantiates a new terminal renderer.
		 */
		public TerminalRenderer()
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.JComponent#getPreferredSize()
		 */
		@Override
		public Dimension getPreferredSize()
		{
			FontMetrics fontMetrics = getGraphics().getFontMetrics(appearance.getNormalTextFont());
			final int screenWidth = SwingTerminal.this.size().getColumns() * fontMetrics.charWidth(' ');
			final int screenHeight = SwingTerminal.this.size().getRows() * fontMetrics.getHeight();
			return new Dimension(screenWidth, screenHeight);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
		 */
		@Override
		protected void paintComponent(Graphics g)
		{
			final Graphics2D graphics2D = (Graphics2D) g.create();
			graphics2D.setFont(appearance.getNormalTextFont());
			graphics2D.setColor(java.awt.Color.BLACK);
			graphics2D.fillRect(0, 0, getWidth(), getHeight());
			final FontMetrics fontMetrics = getGraphics().getFontMetrics(appearance.getNormalTextFont());
			final int charWidth = fontMetrics.charWidth(' ');
			final int charHeight = fontMetrics.getHeight();

			for (int row = 0; row < SwingTerminal.this.size().getRows(); row++)
			{
				for (int col = 0; col < SwingTerminal.this.size().getColumns(); col++)
				{
					boolean needToResetFont = false;
					TerminalCharacter character = characterMap[row][col];
					if (cursorVisible && row == textPosition.getX() && col == textPosition.getY())
					{
						graphics2D.setColor(character.getForegroundAsAWTColor(appearance.useBrightColorsOnBold()));
					}
					else
					{
						graphics2D.setColor(character.getBackgroundAsAWTColor());
					}
					graphics2D.fillRect(col * charWidth, row * charHeight, charWidth, charHeight);
					if (cursorVisible && row == textPosition.getX() && col == textPosition.getY() || character.isBlinking() && !blinkVisible)
					{
						graphics2D.setColor(character.getBackgroundAsAWTColor());
					}
					else
					{
						graphics2D.setColor(character.getForegroundAsAWTColor(appearance.useBrightColorsOnBold()));
					}

					if (character.isBold())
					{
						graphics2D.setFont(appearance.getBoldTextFont());
						needToResetFont = true;
					}

					if (character.isUnderlined())
					{
						graphics2D.drawLine(col * charWidth, (row + 1) * charHeight - 1, (col + 1) * charWidth, (row + 1) * charHeight - 1);
					}

					if (!graphics2D.getFont().canDisplay(character.character))
					{
						graphics2D.setFont(appearance.getCJKFont());
						needToResetFont = true;
					}

					graphics2D.drawString(character.toString(), col * charWidth, (row + 1) * charHeight - fontMetrics.getDescent());

					if (needToResetFont)
					{
						graphics2D.setFont(appearance.getNormalTextFont()); // Restore
																			// the
																			// original
																			// font
					}
				}
			}
			graphics2D.dispose();
		}
	}

	/**
	 * The Class TerminalCharacter.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class TerminalCharacter
	{

		/** The character. */
		private final char character;

		/** The foreground. */
		private final TerminalCharacterColor foreground;

		/** The background. */
		private final TerminalCharacterColor background;

		/** The bold. */
		private final boolean bold;

		/** The blinking. */
		private final boolean blinking;

		/** The underlined. */
		private final boolean underlined;

		/**
		 * Instantiates a new terminal character.
		 * 
		 * @param character
		 *            the character
		 * @param foreground
		 *            the foreground
		 * @param background
		 *            the background
		 * @param bold
		 *            the bold
		 * @param blinking
		 *            the blinking
		 * @param underlined
		 *            the underlined
		 */
		TerminalCharacter(char character, TerminalCharacterColor foreground, TerminalCharacterColor background, boolean bold, boolean blinking, boolean underlined)
		{
			this.character = character;
			this.foreground = foreground;
			this.background = background;
			this.bold = bold;
			this.blinking = blinking;
			this.underlined = underlined;
		}

		/**
		 * Checks if is bold.
		 * 
		 * @return true, if is bold
		 */
		public boolean isBold()
		{
			return bold;
		}

		/**
		 * Checks if is blinking.
		 * 
		 * @return true, if is blinking
		 */
		public boolean isBlinking()
		{
			return blinking;
		}

		/**
		 * Checks if is underlined.
		 * 
		 * @return true, if is underlined
		 */
		public boolean isUnderlined()
		{
			return underlined;
		}

		/**
		 * Gets the foreground as awt color.
		 * 
		 * @param useBrightOnBold
		 *            the use bright on bold
		 * @return the foreground as awt color
		 */
		private java.awt.Color getForegroundAsAWTColor(boolean useBrightOnBold)
		{
			return foreground.getColor(isBold() && useBrightOnBold, true);
		}

		/**
		 * Gets the background as awt color.
		 * 
		 * @return the background as awt color
		 */
		private java.awt.Color getBackgroundAsAWTColor()
		{
			return background.getColor(false, false);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return Character.toString(character);
		}
	}

	/**
	 * The Class TerminalCharacterColor.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static abstract class TerminalCharacterColor
	{

		/**
		 * Gets the color.
		 * 
		 * @param brightHint
		 *            the bright hint
		 * @param foregroundHint
		 *            the foreground hint
		 * @return the color
		 */
		public abstract java.awt.Color getColor(boolean brightHint, boolean foregroundHint);
	}

	/**
	 * The Class CharacterANSIColor.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class CharacterANSIColor extends TerminalCharacterColor
	{

		/** The color. */
		private final Color color;

		/**
		 * Instantiates a new character ansi color.
		 * 
		 * @param color
		 *            the color
		 */
		CharacterANSIColor(Color color)
		{
			this.color = color;
		}

		
		@Override
		public java.awt.Color getColor(boolean brightHint, boolean foregroundHint)
		{
			switch (color)
			{
			case BLACK:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightBlack();
				}
				else
				{
					return appearance.getColorPalette().getNormalBlack();
				}

			case BLUE:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightBlue();
				}
				else
				{
					return appearance.getColorPalette().getNormalBlue();
				}

			case CYAN:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightCyan();
				}
				else
				{
					return appearance.getColorPalette().getNormalCyan();
				}

			case DEFAULT:
				if (foregroundHint)
				{
					if (brightHint)
					{
						return appearance.getColorPalette().getDefaultBrightColor();
					}
					else
					{
						return appearance.getColorPalette().getDefaultColor();
					}
				}
				else
				{
					return appearance.getColorPalette().getNormalBlack();
				}

			case GREEN:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightGreen();
				}
				else
				{
					return appearance.getColorPalette().getNormalGreen();
				}

			case MAGENTA:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightMagenta();
				}
				else
				{
					return appearance.getColorPalette().getNormalMagenta();
				}

			case RED:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightRed();
				}
				else
				{
					return appearance.getColorPalette().getNormalRed();
				}

			case WHITE:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightWhite();
				}
				else
				{
					return appearance.getColorPalette().getNormalWhite();
				}

			case YELLOW:
				if (brightHint)
				{
					return appearance.getColorPalette().getBrightYellow();
				}
				else
				{
					return appearance.getColorPalette().getNormalYellow();
				}
			}
			return java.awt.Color.PINK;
		}
	}

	/**
	 * The Class CharacterIndexedColor.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class CharacterIndexedColor extends TerminalCharacterColor
	{

		/** The index. */
		private final int index;

		/**
		 * Instantiates a new character indexed color.
		 * 
		 * @param index
		 *            the index
		 */
		CharacterIndexedColor(int index)
		{
			this.index = index;
		}

		
		@Override
		public java.awt.Color getColor(boolean brightHint, boolean foregroundHint)
		{
			return XTerm8bitIndexedColorUtils.getAWTColor(index, appearance.getColorPalette());
		}
	}

	/**
	 * The Class Character24bitColor.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class Character24bitColor extends TerminalCharacterColor
	{

		/** The c. */
		private final java.awt.Color c;

		/**
		 * Instantiates a new character24bit color.
		 * 
		 * @param c
		 *            the c
		 */
		Character24bitColor(java.awt.Color c)
		{
			this.c = c;
		}

	
		@Override
		public java.awt.Color getColor(boolean brightHint, boolean foregroundHint)
		{
			return c;
		}
	}

	@Override
	public void newLine(int y)
	{
		int newY = textPosition.getX() + y;
		
		if (!(newY >= size().getRows()))
		{
			textPosition.setX(newY);
			textPosition.setY(0);
			refreshScreen();
		}
		
	}
	
	@Override
	public void deleteLeft()
	{
		System.out.println("dleft");
		
	}

	@Override
	public void incrementXCursor(int x)
	{
		int newX = textPosition.getX() +x;
		if(! (newX >= size().getColumns())) {
			textPosition.setX(newX);
			refreshScreen();
		}
		
	}
}
