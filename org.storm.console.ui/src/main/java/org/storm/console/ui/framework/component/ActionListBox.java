/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class ActionListBox.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ActionListBox extends AbstractListBox
{

	/**
	 * Instantiates a new action list box.
	 */
	public ActionListBox()
	{
		this(null);
	}

	/**
	 * Instantiates a new action list box.
	 * 
	 * @param preferredSize
	 *            the preferred size
	 */
	public ActionListBox(TerminalSize preferredSize)
	{
		super(preferredSize);
	}

	/**
	 * Adds the action.
	 * 
	 * @param action
	 *            the action
	 */
	public void addAction(final Action action)
	{
		addAction(action.toString(), action);
	}

	/**
	 * Adds the action.
	 * 
	 * @param label
	 *            the label
	 * @param action
	 *            the action
	 */
	public void addAction(final String label, final Action action)
	{
		super.addItem(new Item()
		{
			@Override
			public String getTitle()
			{
				return label;
			}

			@Override
			public void doAction()
			{
				action.doAction();
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#unhandledKeyboardEvent
	 * (org.storm.console.ui.framework.input.Key)
	 */
	@Override
	protected Result unhandledKeyboardEvent(Key key)
	{
		if (key.getKind() == Kind.Enter)
		{
			((Item) getSelectedItem()).doAction();
			return Result.EVENT_HANDLED;
		}
		return Result.EVENT_NOT_HANDLED;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#createItemString
	 * (int)
	 */
	@Override
	protected String createItemString(int index)
	{
		return ((Item) getItemAt(index)).getTitle();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractInteractableComponent#
	 * getHotspot()
	 */
	@Override
	public TerminalPosition getHotspot()
	{
		return null; // No hotspot for ActionListBox:es
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getListItemThemeDefinition(org.storm.console.ui.framework.gui.Theme)
	 */
	@Override
	protected Theme.Definition getListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Theme.Category.DIALOG_AREA);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getSelectedListItemThemeDefinition(org.storm.console.ui.framework.gui.Theme)
	 */
	@Override
	protected Theme.Definition getSelectedListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Theme.Category.TEXTBOX_FOCUSED);
	}

	/**
	 * The Interface Item.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static interface Item
	{

		/**
		 * Gets the title.
		 * 
		 * @return the title
		 */
		public String getTitle();

		/**
		 * Do action.
		 */
		public void doAction();
	}
}
