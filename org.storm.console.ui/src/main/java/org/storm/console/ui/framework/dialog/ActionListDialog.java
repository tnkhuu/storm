/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.dialog;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.Border.Invisible;
import org.storm.console.ui.framework.component.ActionListBox;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class ActionListDialog.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ActionListDialog extends Window
{

	/** The action list box. */
	private final ActionListBox actionListBox;

	/**
	 * Instantiates a new action list dialog.
	 * 
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param actionListBoxWidth
	 *            the action list box width
	 */
	private ActionListDialog(String title, String description, int actionListBoxWidth)
	{
		super(title);

		if (description != null)
		{
			addComponent(new Label(description));
		}

		actionListBox = new ActionListBox(new TerminalSize(actionListBoxWidth, 0));
		addComponent(actionListBox);
		Panel cancelPanel = new Panel(new Invisible(), Panel.Orientation.HORIZONTAL);
		cancelPanel.addComponent(new Label("                "));
		cancelPanel.addComponent(new Button("Close", new Action()
		{
			@Override
			public void doAction()
			{
				close();
			}
		}));
		addComponent(cancelPanel);
	}

	/**
	 * Adds the action.
	 * 
	 * @param title
	 *            the title
	 * @param action
	 *            the action
	 */
	private void addAction(final String title, final Action action)
	{
		actionListBox.addAction(title, new Action()
		{
			@Override
			public void doAction()
			{
				action.doAction();
				close();
			}
		});
		actionListBox.setPreferredSize(new TerminalSize(actionListBox.getPreferredSize().getColumns(), actionListBox.getPreferredSize().getRows() + 1));
	}

	/**
	 * Show action list dialog.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param actions
	 *            the actions
	 */
	public static void showActionListDialog(GUIScreen owner, String title, String description, Action... actions)
	{
		int maxLength = 0;
		for (Action action : actions)
		{
			if (action.toString().length() > maxLength)
			{
				maxLength = action.toString().length();
			}
		}

		showActionListDialog(owner, title, description, maxLength, actions);
	}

	/**
	 * Show action list dialog.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param itemWidth
	 *            the item width
	 * @param actions
	 *            the actions
	 */
	public static void showActionListDialog(GUIScreen owner, String title, String description, int itemWidth, Action... actions)
	{
		// Autodetect width?
		if (itemWidth == 0)
		{
			showActionListDialog(owner, title, description, actions);
			return;
		}

		ActionListDialog actionListDialog = new ActionListDialog(title, description, itemWidth);
		for (Action action : actions)
		{
			actionListDialog.addAction(action.toString(), action);
		}
		owner.showWindow(actionListDialog, GUIScreen.Position.CENTER);
	}
}
