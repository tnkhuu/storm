/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.ArrayList;
import java.util.List;

import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.Theme.Definition;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class CheckBoxList.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CheckBoxList extends AbstractListBox
{

	/** The item status. */
	private final List<Boolean> itemStatus;

	/**
	 * Instantiates a new check box list.
	 */
	public CheckBoxList()
	{
		this(null);
	}

	/**
	 * Instantiates a new check box list.
	 * 
	 * @param preferredSize
	 *            the preferred size
	 */
	public CheckBoxList(TerminalSize preferredSize)
	{
		super(preferredSize);
		this.itemStatus = new ArrayList<Boolean>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#clearItems()
	 */
	@Override
	public void clearItems()
	{
		itemStatus.clear();
		super.clearItems();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#addItem(java.lang
	 * .Object)
	 */
	@Override
	public void addItem(Object object)
	{
		itemStatus.add(Boolean.FALSE);
		super.addItem(object);
	}

	/**
	 * Checks if is checked.
	 * 
	 * @param object
	 *            the object
	 * @return the boolean
	 */
	public Boolean isChecked(Object object)
	{
		if (indexOf(object) == -1)
		{
			return null;
		}

		return itemStatus.get(indexOf(object));
	}

	/**
	 * Checks if is checked.
	 * 
	 * @param index
	 *            the index
	 * @return the boolean
	 */
	public Boolean isChecked(int index)
	{
		if (index < 0 || index >= itemStatus.size())
		{
			return null;
		}

		return itemStatus.get(index);
	}

	/**
	 * Sets the checked.
	 * 
	 * @param object
	 *            the object
	 * @param checked
	 *            the checked
	 */
	public void setChecked(Object object, boolean checked)
	{
		if (indexOf(object) == -1)
		{
			return;
		}

		itemStatus.set(indexOf(object), checked);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#unhandledKeyboardEvent
	 * (org.storm.console.ui.framework.input.Key)
	 */
	@Override
	protected Result unhandledKeyboardEvent(Key key)
	{
		if (getSelectedIndex() == -1)
		{
			return Result.EVENT_NOT_HANDLED;
		}

		if (key.getKind() == Kind.Enter || key.getCharacter() == ' ')
		{
			if (itemStatus.get(getSelectedIndex()) == true)
			{
				itemStatus.set(getSelectedIndex(), Boolean.FALSE);
			}
			else
			{
				itemStatus.set(getSelectedIndex(), Boolean.TRUE);
			}
			return Result.EVENT_HANDLED;
		}
		return Result.EVENT_NOT_HANDLED;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getHotSpotPositionOnLine(int)
	 */
	@Override
	protected int getHotSpotPositionOnLine(int selectedIndex)
	{
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractListBox#createItemString
	 * (int)
	 */
	@Override
	protected String createItemString(int index)
	{
		String check = " ";
		if (itemStatus.get(index))
		{
			check = "x";
		}

		String text = getItemAt(index).toString();
		return "[" + check + "] " + text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getListItemThemeDefinition(org.storm.console.ui.framework.gui.Theme)
	 */
	@Override
	protected Definition getListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Theme.Category.TEXTBOX_FOCUSED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractListBox#
	 * getSelectedListItemThemeDefinition(org.storm.console.ui.framework.gui.Theme)
	 */
	@Override
	protected Definition getSelectedListItemThemeDefinition(Theme theme)
	{
		return theme.getDefinition(Theme.Category.TEXTBOX_FOCUSED);
	}
}
