/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.input;

import java.util.Collection;

/**
 * The Class GnomeTerminalProfile.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class GnomeTerminalProfile extends CommonProfile
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.input.CommonProfile#getPatterns()
	 */
	@Override
	Collection<CharacterPattern> getPatterns()
	{
		Collection<CharacterPattern> gnomePatterns = super.getPatterns();
		gnomePatterns.add(new BasicCharacterPattern(new Key(Kind.Home), ESC_CODE, 'O', 'H'));
		gnomePatterns.add(new BasicCharacterPattern(new Key(Kind.End), ESC_CODE, 'O', 'F'));
		return gnomePatterns;
	}
}
