/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.component;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.BLUE;

import org.storm.console.ui.framework.impl.FrameBuffer;

public class ASCIISpinner extends AbstractDrawable implements EscapeChars
{
	private static char[] spins = new char[] {'/', '-', '\\', '|' };
	private int state = 0;

	public ASCIISpinner(int x, int y)
	{
		super(x, y);
	}

	@Override
	public void write(FrameBuffer buffer)
	{
		buffer.append(FIRST_ESC_CHAR + SECOND_ESC_CHAR + "D1");
		buffer.append(spins[state++]);
		if (state > 3) state = 0;
	}

	public void print()
	{
		System.out.print(FIRST_ESC_CHAR);
		System.out.print(SECOND_ESC_CHAR);
		System.out.print("1D");
		System.out.print(ansi().fg(BLUE).bold().a(spins[state++]).reset());
		if (state > 3) state = 0;
	}
}
