/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal.swing;

import java.awt.Font;
import java.awt.image.BufferedImage;

/**
 * The Class TerminalAppearance.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TerminalAppearance
{

	/** The Constant DEFAULT_NORMAL_FONT. */
	public static final Font DEFAULT_NORMAL_FONT = createDefaultNormalFont();

	/** The Constant DEFAULT_BOLD_FONT. */
	public static final Font DEFAULT_BOLD_FONT = createDefaultBoldFont();

	/** The Constant DEFAULT_APPEARANCE. */
	public static final TerminalAppearance DEFAULT_APPEARANCE = new TerminalAppearance(DEFAULT_NORMAL_FONT, DEFAULT_BOLD_FONT, TerminalPalette.DEFAULT, true);

	/**
	 * Creates the default normal font.
	 * 
	 * @return the font
	 */
	private static Font createDefaultNormalFont()
	{
		if (System.getProperty("os.name", "").toLowerCase().indexOf("win") >= 0)
		{
			return new Font("Courier New", Font.PLAIN, 14); // Monospaced can
															// look pretty bad
															// on Windows, so
															// let's override it
		}
		else
		{
			return new Font("Liberation Mono", Font.PLAIN, 14);
		}
	}

	/**
	 * Creates the default bold font.
	 * 
	 * @return the font
	 */
	private static Font createDefaultBoldFont()
	{
		if (System.getProperty("os.name", "").toLowerCase().indexOf("win") >= 0)
		{
			return new Font("Courier New", Font.BOLD, 14); // Monospaced can
															// look pretty bad
															// on Windows, so
															// let's override it
		}
		else
		{
			return new Font("Liberation Mono", Font.BOLD, 14);
		}
	}

	/** The normal text font. */
	private final Font normalTextFont;

	/** The bold text font. */
	private final Font boldTextFont;

	/** The cjk font. */
	private final Font cjkFont;

	/** The color palette. */
	private final TerminalPalette colorPalette;

	/** The use bright colors on bold. */
	private final boolean useBrightColorsOnBold;

	/**
	 * Instantiates a new terminal appearance.
	 * 
	 * @param normalTextFont
	 *            the normal text font
	 * @param boldTextFont
	 *            the bold text font
	 * @param colorPalette
	 *            the color palette
	 * @param useBrightColorsOnBold
	 *            the use bright colors on bold
	 */
	public TerminalAppearance(Font normalTextFont, Font boldTextFont, TerminalPalette colorPalette, boolean useBrightColorsOnBold)
	{

		this.normalTextFont = normalTextFont;
		this.boldTextFont = boldTextFont;
		this.colorPalette = colorPalette;
		this.useBrightColorsOnBold = useBrightColorsOnBold;
		this.cjkFont = deriveCJKFont('桜');
	}

	/**
	 * Derive cjk font.
	 * 
	 * @param character
	 *            the character
	 * @return the font
	 */
	private Font deriveCJKFont(char character)
	{
		BufferedImage bi = new BufferedImage(5, 5, BufferedImage.TYPE_INT_RGB);
		int targetWidth = bi.getGraphics().getFontMetrics(normalTextFont).charWidth('W');
		int startSize = normalTextFont.getSize();
		for (int size = startSize; size > 0; size--)
		{
			Font font = new Font("Monospaced", Font.PLAIN, size);
			int characterWidth = bi.getGraphics().getFontMetrics(font).charWidth(character);
			if (characterWidth <= targetWidth)
			{
				return font;
			}
		}
		return normalTextFont;
	}

	/**
	 * Gets the normal text font.
	 * 
	 * @return the normal text font
	 */
	public Font getNormalTextFont()
	{
		return normalTextFont;
	}

	/**
	 * Gets the bold text font.
	 * 
	 * @return the bold text font
	 */
	public Font getBoldTextFont()
	{
		return boldTextFont;
	}

	/**
	 * Gets the cJK font.
	 * 
	 * @return the cJK font
	 */
	public Font getCJKFont()
	{
		return cjkFont;
	}

	/**
	 * Gets the color palette.
	 * 
	 * @return the color palette
	 */
	public TerminalPalette getColorPalette()
	{
		return colorPalette;
	}

	/**
	 * Use bright colors on bold.
	 * 
	 * @return true, if successful
	 */
	public boolean useBrightColorsOnBold()
	{
		return useBrightColorsOnBold;
	}

	/**
	 * With font.
	 * 
	 * @param textFont
	 *            the text font
	 * @return the terminal appearance
	 */
	public TerminalAppearance withFont(Font textFont)
	{
		return withFont(textFont, textFont);
	}

	/**
	 * With font.
	 * 
	 * @param normalTextFont
	 *            the normal text font
	 * @param boldTextFont
	 *            the bold text font
	 * @return the terminal appearance
	 */
	public TerminalAppearance withFont(Font normalTextFont, Font boldTextFont)
	{
		return new TerminalAppearance(normalTextFont, boldTextFont, colorPalette, useBrightColorsOnBold);
	}

	/**
	 * With palette.
	 * 
	 * @param palette
	 *            the palette
	 * @return the terminal appearance
	 */
	public TerminalAppearance withPalette(TerminalPalette palette)
	{
		return new TerminalAppearance(normalTextFont, boldTextFont, palette, useBrightColorsOnBold);
	}

	/**
	 * With use bright colors.
	 * 
	 * @param useBrightColorsOnBold
	 *            the use bright colors on bold
	 * @return the terminal appearance
	 */
	public TerminalAppearance withUseBrightColors(boolean useBrightColorsOnBold)
	{
		return new TerminalAppearance(normalTextFont, boldTextFont, colorPalette, useBrightColorsOnBold);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final TerminalAppearance other = (TerminalAppearance) obj;
		if (this.normalTextFont != other.normalTextFont && (this.normalTextFont == null || !this.normalTextFont.equals(other.normalTextFont)))
		{
			return false;
		}
		if (this.boldTextFont != other.boldTextFont && (this.boldTextFont == null || !this.boldTextFont.equals(other.boldTextFont)))
		{
			return false;
		}
		if (this.colorPalette != other.colorPalette && (this.colorPalette == null || !this.colorPalette.equals(other.colorPalette)))
		{
			return false;
		}
		if (this.useBrightColorsOnBold != other.useBrightColorsOnBold)
		{
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 37 * hash + (this.normalTextFont != null ? this.normalTextFont.hashCode() : 0);
		hash = 37 * hash + (this.boldTextFont != null ? this.boldTextFont.hashCode() : 0);
		hash = 37 * hash + (this.colorPalette != null ? this.colorPalette.hashCode() : 0);
		hash = 37 * hash + (this.useBrightColorsOnBold ? 1 : 0);
		return hash;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "TerminalAppearance{" + "normalTextFont=" + normalTextFont + ", boldTextFont=" + boldTextFont + ", colorPalette=" + colorPalette + ", useBrightColorsOnBold="
				+ useBrightColorsOnBold + '}';
	}
}
