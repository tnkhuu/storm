/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.input;

import java.util.Collection;

/**
 * The Class KeyMappingProfile.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class KeyMappingProfile
{

	/** The Constant ESC_CODE. */
	public static final char ESC_CODE = (char) 0x1b;

	/**
	 * Gets the patterns.
	 * 
	 * @return the patterns
	 */
	abstract Collection<CharacterPattern> getPatterns();
}
