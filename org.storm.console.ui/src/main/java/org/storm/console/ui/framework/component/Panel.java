/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.List;

import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.layout.HorisontalLayout;
import org.storm.console.ui.framework.layout.LayoutManager;
import org.storm.console.ui.framework.layout.LayoutParameter;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.layout.VerticalLayout;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class Panel.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Panel extends AbstractContainer
{

	/** The border. */
	private Border border;

	/** The layout manager. */
	private LayoutManager layoutManager;

	/** The title. */
	private String title;

	/**
	 * Instantiates a new panel.
	 */
	public Panel()
	{
		this(Orientation.VERTICAL);
	}

	/**
	 * Instantiates a new panel.
	 * 
	 * @param title
	 *            the title
	 */
	public Panel(String title)
	{
		this(title, Orientation.VERTICAL);
	}

	/**
	 * Instantiates a new panel.
	 * 
	 * @param panelOrientation
	 *            the panel orientation
	 */
	public Panel(Orientation panelOrientation)
	{
		this(new Border.Invisible(), panelOrientation);
	}

	/**
	 * Instantiates a new panel.
	 * 
	 * @param title
	 *            the title
	 * @param panelOrientation
	 *            the panel orientation
	 */
	public Panel(String title, Orientation panelOrientation)
	{
		this(title, new Border.Bevel(true), panelOrientation);
	}

	/**
	 * Instantiates a new panel.
	 * 
	 * @param border
	 *            the border
	 * @param panelOrientation
	 *            the panel orientation
	 */
	public Panel(Border border, Orientation panelOrientation)
	{
		this("", border, panelOrientation);
	}

	/**
	 * Instantiates a new panel.
	 * 
	 * @param title
	 *            the title
	 * @param border
	 *            the border
	 * @param panelOrientation
	 *            the panel orientation
	 */
	public Panel(String title, Border border, Orientation panelOrientation)
	{
		this.border = border;
		if (panelOrientation == Orientation.HORIZONTAL)
		{
			layoutManager = new HorisontalLayout();
		}
		else
		{
			layoutManager = new VerticalLayout();
		}

		this.title = title != null ? title : "";
	}

	/**
	 * Gets the border.
	 * 
	 * @return the border
	 */
	public Border getBorder()
	{
		return border;
	}

	/**
	 * Sets the border.
	 * 
	 * @param border
	 *            the new border
	 */
	public void setBorder(Border border)
	{
		if (border != null)
		{
			this.border = border;
		}
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title)
	{
		this.title = title != null ? title : "";
	}

	/**
	 * Sets the layout manager.
	 * 
	 * @param layoutManager
	 *            the new layout manager
	 */
	public void setLayoutManager(LayoutManager layoutManager)
	{
		this.layoutManager = layoutManager;
	}

	/**
	 * Gets the layout manager.
	 * 
	 * @return the layout manager
	 */
	public LayoutManager getLayoutManager()
	{
		return layoutManager;
	}

	/**
	 * Maximises vertically.
	 * 
	 * @return true, if successful
	 */
	public boolean maximisesVertically()
	{
		return layoutManager.maximisesVertically();
	}

	/**
	 * Maximises horisontally.
	 * 
	 * @return true, if successful
	 */
	public boolean maximisesHorisontally()
	{
		return layoutManager.maximisesHorisontally();
	}

	/**
	 * Sets the between components padding.
	 * 
	 * @param paddingSize
	 *            the new between components padding
	 */
	@Deprecated
	public void setBetweenComponentsPadding(int paddingSize)
	{
		if (paddingSize < 0)
		{
			paddingSize = 0;
		}

		if (layoutManager instanceof LinearLayout)
		{
			((LinearLayout) layoutManager).setPadding(paddingSize);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		border.drawBorder(graphics, new TerminalSize(graphics.getWidth(), graphics.getHeight()), title);
		TerminalPosition contentPaneTopLeft = border.getInnerAreaLocation(graphics.getWidth(), graphics.getHeight());
		TerminalSize contentPaneSize = border.getInnerAreaSize(graphics.getWidth(), graphics.getHeight());
		TextGraphics subGraphics = graphics.subAreaGraphics(contentPaneTopLeft, contentPaneSize);

		List<? extends LayoutManager.LaidOutComponent> laidOutComponents = layoutManager.layout(contentPaneSize);
		for (LayoutManager.LaidOutComponent laidOutComponent : laidOutComponents)
		{
			TextGraphics subSubGraphics = subGraphics.subAreaGraphics(laidOutComponent.getTopLeftPosition(), laidOutComponent.getSize());

			if (laidOutComponent.getComponent().isVisible())
			{
				laidOutComponent.getComponent().repaint(subSubGraphics);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		TerminalSize preferredSize = border.surroundAreaSize(layoutManager.getPreferredSize());
		if (title.length() + 4 > preferredSize.getColumns())
		{
			preferredSize.setColumns(title.length() + 4);
		}
		return preferredSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractContainer#addComponent(
	 * org.storm.console.ui.framework.gui.Component,
	 * org.storm.console.ui.framework.gui.layout.LayoutParameter[])
	 */
	@Override
	public void addComponent(Component component, LayoutParameter... layoutParameters)
	{
		super.addComponent(component);
		layoutManager.addComponent(component, layoutParameters);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractContainer#removeComponent
	 * (org.storm.console.ui.framework.gui.Component)
	 */
	@Override
	public boolean removeComponent(Component component)
	{
		if (super.removeComponent(component))
		{
			layoutManager.removeComponent(component);
			return true;
		}
		return false;
	}

	/**
	 * The Enum Orientation.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public enum Orientation
	{

		/** The horizontal. */
		HORIZONTAL,

		/** The vertical. */
		VERTICAL
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Panel with " + getComponentCount() + " components";
	}
}
