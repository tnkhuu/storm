/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal.text;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.storm.console.ui.framework.ConsoleException;
import org.storm.console.ui.framework.input.GnomeTerminalProfile;
import org.storm.console.ui.framework.input.PuttyProfile;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class CygwinTerminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CygwinTerminal extends ANSITerminal
{

	/** The Constant STTY_SIZE_PATTERN. */
	private static final Pattern STTY_SIZE_PATTERN = Pattern.compile(".*rows ([0-9]+);.*columns ([0-9]+);.*");

	/** The resize check timer. */
	private Timer resizeCheckTimer;

	/**
	 * Instantiates a new cygwin terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 */
	public CygwinTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset)
	{
		super(terminalInput, terminalOutput, terminalCharset);
		addInputProfile(new GnomeTerminalProfile());
		addInputProfile(new PuttyProfile());

		// Make sure to set an initial size
		onResized(80, 20);
		resizeCheckTimer = null;
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#getTerminalSize()
	 */
	@Override
	public TerminalSize getTerminalSize()
	{
		try
		{
			String stty = exec(findSTTY(), "-F", "/dev/pty0", "-a"); // exec(findShell(),
																		// "-c",
																		// "echo $PPID");
			Matcher matcher = STTY_SIZE_PATTERN.matcher(stty);
			if (matcher.matches())
			{
				return new TerminalSize(Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(1)));
			}
			else
			{
				return new TerminalSize(80, 20);
			}
		}
		catch (ConsoleException e)
		{
			return new TerminalSize(80, 20);
		}
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#enterPrivateMode()
	 */
	@Override
	public void enterPrivateMode()
	{
		super.enterPrivateMode();
		setCBreak(true);
		setEcho(false);
		sttyMinimumCharacterForRead(1);
		resizeCheckTimer = new Timer("CygwinTerminalResizeChecker");
		resizeCheckTimer.scheduleAtFixedRate(new TimerTask()
		{
			@Override
			public void run()
			{
				// queryTerminalSize();
			}
		}, 1000, 1000);
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#exitPrivateMode()
	 */
	@Override
	public void exitPrivateMode()
	{
		resizeCheckTimer.cancel();
		setEcho(true);
		super.exitPrivateMode();
		setCBreak(false);
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#setCBreak(boolean)
	 */
	@Override
	public void setCBreak(boolean cbreakOn)
	{
		sttyCBreak(cbreakOn);
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#setEcho(boolean)
	 */
	@Override
	public void setEcho(boolean echoOn)
	{
		sttyKeyEcho(echoOn);
	}

	/**
	 * Stty key echo.
	 * 
	 * @param enable
	 *            the enable
	 */
	private static void sttyKeyEcho(final boolean enable)
	{
		exec(findSTTY(), "-F", "/dev/pty0", enable ? "echo" : "-echo");
	}

	/**
	 * Stty minimum character for read.
	 * 
	 * @param nrCharacters
	 *            the nr characters
	 */
	private static void sttyMinimumCharacterForRead(final int nrCharacters)
	{
		exec(findSTTY(), "-F", "/dev/pty0", "min", nrCharacters + "");
	}

	/**
	 * Stty c break.
	 * 
	 * @param enable
	 *            the enable
	 */
	private static void sttyCBreak(final boolean enable)
	{
		exec(findSTTY(), "-F", "/dev/pty0", enable ? "cbreak" : "icanon");
	}

	/**
	 * Find stty.
	 * 
	 * @return the string
	 */
	private static String findSTTY()
	{
		return findProgram("stty.exe");
	}

	/**
	 * Find program.
	 * 
	 * @param programName
	 *            the program name
	 * @return the string
	 */
	private static String findProgram(String programName)
	{
		String[] paths = System.getProperty("java.library.path").split(";");
		for (String path : paths)
		{
			File shBin = new File(path, programName);
			if (shBin.exists())
			{
				return shBin.getAbsolutePath();
			}
		}
		return programName;
	}

	/**
	 * Exec.
	 * 
	 * @param cmd
	 *            the cmd
	 * @return the string
	 */
	private static String exec(String... cmd)
	{
		try
		{
			ProcessBuilder pb = new ProcessBuilder(cmd);
			Process process = pb.start();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream stdout = process.getInputStream();
			int readByte = stdout.read();
			while (readByte >= 0)
			{
				baos.write(readByte);
				readByte = stdout.read();
			}
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			BufferedReader reader = new BufferedReader(new InputStreamReader(bais));
			StringBuilder builder = new StringBuilder();
			while (reader.ready())
			{
				builder.append(reader.readLine());
			}
			reader.close();
			return builder.toString();
		}
		catch (IOException e)
		{
			throw new ConsoleException(e);
		}
	}


	@Override
	public void newLine(int y)
	{
		// TODO Auto-generated method stub
		
	}


	@Override
	public void incrementXCursor(int y)
	{
		// TODO Auto-generated method stub
		
	}
}
