/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.storm.console.ui.framework.impl.DefaultBackgroundRenderer;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.listener.WindowAdapter;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class GUIScreen.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class GUIScreen implements Scene
{

	/** The screen. */
	private final Screen screen;

	/** The window stack. */
	private final LinkedList<WindowPlacement> windowStack;

	/** The action to run in event thread. */
	protected final Queue<Action> actionToRunInEventThread;

	/** The background renderer. */
	private GUIScreenBackgroundRenderer backgroundRenderer;

	/** The gui theme. */
	private Theme guiTheme;

	/** The needs refresh. */
	private boolean needsRefresh;

	/** The event thread. */
	private Thread eventThread;

	/**
	 * Instantiates a new gUI screen.
	 * 
	 * @param screen
	 *            the screen
	 */
	public GUIScreen(Screen screen)
	{
		this(screen, "");
	}

	/**
	 * Instantiates a new gUI screen.
	 * 
	 * @param screen
	 *            the screen
	 * @param title
	 *            the title
	 */
	public GUIScreen(Screen screen, String title)
	{
		this(screen, new DefaultBackgroundRenderer(title));
	}

	/**
	 * Instantiates a new gUI screen.
	 * 
	 * @param screen
	 *            the screen
	 * @param backgroundRenderer
	 *            the background renderer
	 */
	public GUIScreen(Screen screen, GUIScreenBackgroundRenderer backgroundRenderer)
	{
		if (backgroundRenderer == null)
		{
			throw new IllegalArgumentException("backgroundRenderer cannot be null");
		}

		this.backgroundRenderer = backgroundRenderer;
		this.screen = screen;
		this.guiTheme = Theme.getDefaultTheme();
		this.windowStack = new LinkedList<WindowPlacement>();
		this.actionToRunInEventThread = new LinkedList<Action>();
		this.needsRefresh = false;
		this.eventThread = Thread.currentThread(); // We'll be expecting the
													// thread who created us is
													// the same as will be the
													// event thread later
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	@Deprecated
	public void setTitle(String title)
	{
		if (title == null)
		{
			title = "";
		}

		if (backgroundRenderer instanceof DefaultBackgroundRenderer)
		{
			((DefaultBackgroundRenderer) backgroundRenderer).setTitle(title);
		}
	}

	/**
	 * Sets the theme.
	 * 
	 * @param newTheme
	 *            the new theme
	 */
	public void setTheme(Theme newTheme)
	{
		if (newTheme == null)
		{
			return;
		}

		this.guiTheme = newTheme;
		needsRefresh = true;
	}

	/**
	 * Sets the background renderer.
	 * 
	 * @param backgroundRenderer
	 *            the new background renderer
	 */
	public void setBackgroundRenderer(GUIScreenBackgroundRenderer backgroundRenderer)
	{
		if (backgroundRenderer == null)
		{
			throw new IllegalArgumentException("backgroundRenderer cannot be null");
		}

		this.backgroundRenderer = backgroundRenderer;
		needsRefresh = true;
	}

	/**
	 * Gets the background renderer.
	 * 
	 * @return the background renderer
	 */
	public GUIScreenBackgroundRenderer getBackgroundRenderer()
	{
		return backgroundRenderer;
	}

	/**
	 * Gets the screen.
	 * 
	 * @return the screen
	 */
	public Screen getScreen()
	{
		return screen;
	}

	/**
	 * Repaint.
	 */
	public synchronized void play()
	{
		if (screen.resizePending())
		{
			screen.refresh(); // Do an initial refresh if there are any resizes
								// in the queue
		}

		final TextGraphics textGraphics = new TextGraphicsImpl(new TerminalPosition(0, 0), new TerminalSize(screen.getTerminalSize()), screen, guiTheme);

		backgroundRenderer.drawBackground(textGraphics);

		int screenSizeColumns = screen.getTerminalSize().getColumns();
		int screenSizeRows = screen.getTerminalSize().getRows();

		// Go through the windows
		for (WindowPlacement windowPlacement : windowStack)
		{
			if (hasSoloWindowAbove(windowPlacement))
			{
				continue;
			}
			if (hasFullScreenWindowAbove(windowPlacement))
			{
				continue;
			}

			TerminalPosition topLeft = windowPlacement.getTopLeft();
			TerminalSize preferredSize;
			if (windowPlacement.getPositionPolicy() == Position.FULL_SCREEN)
			{
				preferredSize = new TerminalSize(screenSizeColumns, screenSizeRows);
			}
			else
			{
				preferredSize = windowPlacement.getWindow().getPreferredSize();
			}

			if (windowPlacement.positionPolicy == Position.CENTER)
			{
				if (windowPlacement.getWindow().maximisesHorisontally())
				{
					topLeft.setY(2);
				}
				else
				{
					topLeft.setY(screenSizeColumns / 2 - preferredSize.getColumns() / 2);
				}

				if (windowPlacement.getWindow().maximisesVertically())
				{
					topLeft.setX(1);
				}
				else
				{
					topLeft.setX(screenSizeRows / 2 - preferredSize.getRows() / 2);
				}
			}
			int maxSizeWidth = screenSizeColumns - windowPlacement.getTopLeft().getY() - 1;
			int maxSizeHeight = screenSizeRows - windowPlacement.getTopLeft().getX() - 1;

			if (preferredSize.getColumns() > maxSizeWidth || windowPlacement.getWindow().maximisesHorisontally())
			{
				preferredSize.setColumns(maxSizeWidth);
			}
			if (preferredSize.getRows() > maxSizeHeight || windowPlacement.getWindow().maximisesVertically())
			{
				preferredSize.setRows(maxSizeHeight);
			}

			if (windowPlacement.getPositionPolicy() == Position.FULL_SCREEN)
			{
				preferredSize.setColumns(preferredSize.getColumns() + 1);
				preferredSize.setRows(preferredSize.getRows() + 1);
			}

			if (topLeft.getY() < 0)
			{
				topLeft.setY(0);
			}
			if (topLeft.getX() < 0)
			{
				topLeft.setX(0);
			}

			TextGraphics subGraphics = textGraphics.subAreaGraphics(topLeft, new TerminalSize(preferredSize.getColumns(), preferredSize.getRows()));

			// First draw the shadow
			textGraphics.applyTheme(guiTheme.getDefinition(Theme.Category.SHADOW));
			textGraphics.fillRectangle(' ', new TerminalPosition(topLeft.getY() + 2, topLeft.getX() + 1), new TerminalSize(subGraphics.getWidth(), subGraphics.getHeight()));

			// Then draw the window
			windowPlacement.getWindow().repaint(subGraphics);
		}

		if (windowStack.size() > 0 && windowStack.getLast().getWindow().getWindowHotspotPosition() != null)
		{
			screen.setCursorPosition(windowStack.getLast().getWindow().getWindowHotspotPosition());
		}
		else
		{
			screen.setCursorPosition(null);
		}
		screen.refresh();
	}

	/**
	 * Update.
	 * 
	 * @return true, if successful
	 */
	protected boolean update()
	{
		if (needsRefresh || screen.resizePending())
		{
			play();
			needsRefresh = false;
			return true;
		}
		return false;
	}

	/**
	 * Gets the actions to run in event thread queue.
	 * 
	 * @return the actions to run in event thread queue
	 */
	protected Queue<Action> getActionsToRunInEventThreadQueue()
	{
		return actionToRunInEventThread;
	}

	/**
	 * Invalidate.
	 */
	public void invalidate()
	{
		needsRefresh = true;
	}

	/**
	 * Do event loop.
	 */
	protected void doEventLoop()
	{
		int currentStackLength = windowStack.size();
		if (currentStackLength == 0)
		{
			return;
		}

		while (true)
		{
			if (currentStackLength > windowStack.size())
			{
				// The window was removed from the stack ( = it was closed)
				break;
			}

			try
			{
				synchronized (actionToRunInEventThread)
				{
					List<Action> actions = new ArrayList<Action>(actionToRunInEventThread);
					actionToRunInEventThread.clear();
					for (Action nextAction : actions)
					{
						nextAction.doAction();
					}
				}

				boolean repainted = update();

				Key nextKey = screen.readInput();
				if (nextKey != null)
				{
					windowStack.getLast().window.onKeyPressed(nextKey);
					invalidate();
				}
				else
				{
					if (!repainted)
					{
						try
						{
							Thread.sleep(1);
						}
						catch (InterruptedException e)
						{
						}
					}
				}
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Show window.
	 * 
	 * @param window
	 *            the window
	 */
	public void showWindow(Window window)
	{
		showWindow(window, Position.OVERLAPPING);
	}

	/**
	 * Show window.
	 * 
	 * @param window
	 *            the window
	 * @param position
	 *            the position
	 */
	public void showWindow(Window window, Position position)
	{
		if (window == null)
		{
			return;
		}
		if (position == null)
		{
			position = Position.OVERLAPPING;
		}

		int newWindowX = 2;
		int newWindowY = 1;

		if (position == Position.OVERLAPPING && windowStack.size() > 0)
		{
			WindowPlacement lastWindow = windowStack.getLast();
			if (lastWindow.getPositionPolicy() != Position.CENTER)
			{
				newWindowX = lastWindow.getTopLeft().getY() + 2;
				newWindowY = lastWindow.getTopLeft().getX() + 1;
			}
		}

		window.addWindowListener(new WindowAdapter()
		{
			@Override
			public void onWindowInvalidated(Window window)
			{
				needsRefresh = true;
			}
		});
		windowStack.add(new WindowPlacement(window, position, new TerminalPosition(newWindowX, newWindowY)));
		window.setOwner(this);
		window.onVisible();
		needsRefresh = true;
		doEventLoop();
	}

	/**
	 * Close window.
	 */
	@Deprecated
	public void closeWindow()
	{
		Window activeWindow = getActiveWindow();
		if (activeWindow != null)
		{
			activeWindow.close();
		}
	}

	/**
	 * Close window.
	 * 
	 * @param window
	 *            the window
	 */
	protected void closeWindow(Window window)
	{
		if (windowStack.size() == 0)
		{
			return;
		}

		for (Iterator<WindowPlacement> iterator = windowStack.iterator(); iterator.hasNext();)
		{
			WindowPlacement placement = iterator.next();
			if (placement.window == window)
			{
				iterator.remove();
				placement.window.onClosed();
				return;
			}
		}
	}

	/**
	 * Gets the active window.
	 * 
	 * @return the active window
	 */
	public Window getActiveWindow()
	{
		if (windowStack.isEmpty())
		{
			return null;
		}
		else
		{
			return windowStack.getLast().getWindow();
		}
	}

	/**
	 * Run in event thread.
	 * 
	 * @param codeToRun
	 *            the code to run
	 */
	public void runInEventThread(Action codeToRun)
	{
		synchronized (actionToRunInEventThread)
		{
			actionToRunInEventThread.add(codeToRun);
		}
	}

	/**
	 * Checks if is in event thread.
	 * 
	 * @return true, if is in event thread
	 */
	public boolean isInEventThread()
	{
		return eventThread == Thread.currentThread();
	}

	/**
	 * The Enum Position.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public enum Position
	{

		/** The overlapping. */
		OVERLAPPING,

		/** The new corner window. */
		NEW_CORNER_WINDOW,

		/** The center. */
		CENTER,

		/** The full screen. */
		FULL_SCREEN, ;
	}

	/**
	 * Checks for solo window above.
	 * 
	 * @param windowPlacement
	 *            the window placement
	 * @return true, if successful
	 */
	private boolean hasSoloWindowAbove(WindowPlacement windowPlacement)
	{
		int index = windowStack.indexOf(windowPlacement);
		for (int i = index + 1; i < windowStack.size(); i++)
		{
			if (windowStack.get(i).window.isSoloWindow())
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for full screen window above.
	 * 
	 * @param windowPlacement
	 *            the window placement
	 * @return true, if successful
	 */
	private boolean hasFullScreenWindowAbove(WindowPlacement windowPlacement)
	{
		int index = windowStack.indexOf(windowPlacement);
		for (int i = index + 1; i < windowStack.size(); i++)
		{
			if (windowStack.get(i).positionPolicy == Position.FULL_SCREEN)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * The Class WindowPlacement.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class WindowPlacement
	{

		/** The window. */
		private Window window;

		/** The position policy. */
		private Position positionPolicy;

		/** The top left. */
		private TerminalPosition topLeft;

		/**
		 * Instantiates a new window placement.
		 * 
		 * @param window
		 *            the window
		 * @param positionPolicy
		 *            the position policy
		 * @param topLeft
		 *            the top left
		 */
		public WindowPlacement(Window window, Position positionPolicy, TerminalPosition topLeft)
		{
			this.window = window;
			this.positionPolicy = positionPolicy;
			this.topLeft = topLeft;
		}

		/**
		 * Gets the top left.
		 * 
		 * @return the top left
		 */
		public TerminalPosition getTopLeft()
		{
			if (positionPolicy == Position.FULL_SCREEN)
			{
				return new TerminalPosition(0, 0);
			}
			return topLeft;
		}

		/**
		 * Sets the top left.
		 * 
		 * @param topLeft
		 *            the new top left
		 */
		@SuppressWarnings("unused")
		public void setTopLeft(TerminalPosition topLeft)
		{
			this.topLeft = topLeft;
		}

		/**
		 * Gets the window.
		 * 
		 * @return the window
		 */
		public Window getWindow()
		{
			return window;
		}

		/**
		 * Sets the window.
		 * 
		 * @param window
		 *            the new window
		 */
		@SuppressWarnings("unused")
		public void setWindow(Window window)
		{
			this.window = window;
		}

		/**
		 * Gets the position policy.
		 * 
		 * @return the position policy
		 */
		public Position getPositionPolicy()
		{
			return positionPolicy;
		}
	}

	@Override
	public void stop()
	{
		if (screen != null)
		{
			screen.stopScreen();
		}

	}
}
