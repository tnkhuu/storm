/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.dialog;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;

/**
 * The Class MessageBox.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MessageBox extends Window
{

	/** The dialog result. */
	private DialogResult dialogResult;

	/**
	 * Instantiates a new message box.
	 * 
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 * @param buttons
	 *            the buttons
	 */
	@SuppressWarnings("deprecation")
	protected MessageBox(String title, String message, DialogButtons buttons)
	{
		super(title);
		dialogResult = DialogResult.CANCEL;

		Label messageBoxLabel = new Label(message);
		addComponent(messageBoxLabel);
		addEmptyLine();

		Button okButton = new Button("OK", new Action()
		{
			@Override
			public void doAction()
			{
				dialogResult = DialogResult.OK;
				close();
			}
		});
		Button cancelButton = new Button("Cancel", new Action()
		{
			@Override
			public void doAction()
			{
				dialogResult = DialogResult.CANCEL;
				close();
			}
		});
		Button yesButton = new Button("Yes", new Action()
		{
			@Override
			public void doAction()
			{
				dialogResult = DialogResult.YES;
				close();
			}
		});
		Button noButton = new Button("No", new Action()
		{
			@Override
			public void doAction()
			{
				dialogResult = DialogResult.NO;
				close();
			}
		});

		int labelWidth = messageBoxLabel.getPreferredSize().getColumns();
		if (buttons == DialogButtons.OK)
		{
			Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
			int leftPadding = 0;
			int buttonsWidth = okButton.getPreferredSize().getColumns();
			if (buttonsWidth < labelWidth)
			{
				leftPadding = (labelWidth - buttonsWidth) / 2;
			}
			if (leftPadding > 0)
			{
				buttonPanel.addComponent(new EmptySpace(leftPadding, 1));
			}
			buttonPanel.addComponent(okButton);
			addComponent(buttonPanel);
		}
		else if (buttons == DialogButtons.OK_CANCEL)
		{
			Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
			int leftPadding = 0;
			int buttonsWidth = okButton.getPreferredSize().getColumns() + cancelButton.getPreferredSize().getColumns() + 1;
			if (buttonsWidth < labelWidth)
			{
				leftPadding = (labelWidth - buttonsWidth) / 2;
			}
			if (leftPadding > 0)
			{
				buttonPanel.addComponent(new EmptySpace(leftPadding, 1));
			}
			buttonPanel.addComponent(okButton);
			buttonPanel.addComponent(cancelButton);
			addComponent(buttonPanel);
			setFocus(cancelButton);
		}
		else if (buttons == DialogButtons.YES_NO)
		{
			Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
			int leftPadding = 0;
			int buttonsWidth = yesButton.getPreferredSize().getColumns() + noButton.getPreferredSize().getColumns() + 1;
			if (buttonsWidth < labelWidth)
			{
				leftPadding = (labelWidth - buttonsWidth) / 2;
			}
			if (leftPadding > 0)
			{
				buttonPanel.addComponent(new EmptySpace(leftPadding, 1));
			}
			buttonPanel.addComponent(yesButton);
			buttonPanel.addComponent(noButton);
			addComponent(buttonPanel);
			setFocus(noButton);
		}
		else if (buttons == DialogButtons.YES_NO_CANCEL)
		{
			Panel buttonPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
			int leftPadding = 0;
			int buttonsWidth = yesButton.getPreferredSize().getColumns() + noButton.getPreferredSize().getColumns() + 1 + cancelButton.getPreferredSize().getColumns();
			if (buttonsWidth < labelWidth)
			{
				leftPadding = (labelWidth - buttonsWidth) / 2;
			}
			if (leftPadding > 0)
			{
				buttonPanel.addComponent(new EmptySpace(leftPadding, 1));
			}
			buttonPanel.addComponent(yesButton);
			buttonPanel.addComponent(noButton);
			buttonPanel.addComponent(cancelButton);
			addComponent(buttonPanel);
			setFocus(cancelButton);
		}
	}

	/**
	 * Show message box.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 * @return the dialog result
	 */
	public static DialogResult showMessageBox(final GUIScreen owner, final String title, final String message)
	{
		return showMessageBox(owner, title, message, DialogButtons.OK);
	}

	/**
	 * Show message box.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 * @param buttons
	 *            the buttons
	 * @return the dialog result
	 */
	public static DialogResult showMessageBox(final GUIScreen owner, final String title, final String message, final DialogButtons buttons)
	{
		MessageBox messageBox = new MessageBox(title, message, buttons);
		owner.showWindow(messageBox, GUIScreen.Position.CENTER);
		return messageBox.dialogResult;
	}
}
