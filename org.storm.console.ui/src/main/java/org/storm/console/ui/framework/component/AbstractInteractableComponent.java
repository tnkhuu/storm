/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.listener.ComponentListener;
import org.storm.console.ui.framework.terminal.TerminalPosition;

/**
 * The Class AbstractInteractableComponent.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class AbstractInteractableComponent extends AbstractComponent implements InteractableComponent
{

	/** The has focus. */
	private boolean hasFocus;

	/** The hotspot. */
	private TerminalPosition hotspot;

	/**
	 * Instantiates a new abstract interactable component.
	 */
	public AbstractInteractableComponent()
	{
		hotspot = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Interactable#onEnterFocus(com.googlecode.
	 * lanterna.gui.Interactable.FocusChangeDirection)
	 */
	@Override
	public final void onEnterFocus(FocusChangeDirection direction)
	{
		hasFocus = true;
		for (ComponentListener cl : getComponentListeners())
		{
			cl.onComponentReceivedFocus(this);
		}
		afterEnteredFocus(direction);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Interactable#onLeaveFocus(com.googlecode.
	 * lanterna.gui.Interactable.FocusChangeDirection)
	 */
	@Override
	public final void onLeaveFocus(FocusChangeDirection direction)
	{
		hasFocus = false;
		for (ComponentListener cl : getComponentListeners())
		{
			cl.onComponentLostFocus(this);
		}
		afterLeftFocus(direction);
	}

	/**
	 * After entered focus.
	 * 
	 * @param direction
	 *            the direction
	 */
	protected void afterEnteredFocus(FocusChangeDirection direction)
	{
	}

	/**
	 * After left focus.
	 * 
	 * @param direction
	 *            the direction
	 */
	protected void afterLeftFocus(FocusChangeDirection direction)
	{
	}

	/**
	 * Checks for focus.
	 * 
	 * @return true, if successful
	 */
	public boolean hasFocus()
	{
		return hasFocus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Interactable#getHotspot()
	 */
	@Override
	public TerminalPosition getHotspot()
	{
		return hotspot;
	}

	/**
	 * Sets the hotspot.
	 * 
	 * @param point
	 *            the new hotspot
	 */
	protected void setHotspot(TerminalPosition point)
	{
		this.hotspot = point;
	}
}
