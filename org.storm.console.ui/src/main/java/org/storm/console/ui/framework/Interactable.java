/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.terminal.TerminalPosition;

/**
 * The Interface Interactable.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Interactable
{

	/**
	 * Keyboard interaction.
	 * 
	 * @param key
	 *            the key
	 * @return the result
	 */
	public Result keyboardInteraction(Key key);

	/**
	 * On enter focus.
	 * 
	 * @param direction
	 *            the direction
	 */
	public void onEnterFocus(FocusChangeDirection direction);

	/**
	 * On leave focus.
	 * 
	 * @param direction
	 *            the direction
	 */
	public void onLeaveFocus(FocusChangeDirection direction);

	/**
	 * Gets the hotspot.
	 * 
	 * @return the hotspot
	 */
	public TerminalPosition getHotspot();

	/**
	 * The Enum Result.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public enum Result
	{

		/** The event handled. */
		EVENT_HANDLED,

		/** The event not handled. */
		EVENT_NOT_HANDLED,

		/** The next interactable down. */
		NEXT_INTERACTABLE_DOWN,

		/** The next interactable right. */
		NEXT_INTERACTABLE_RIGHT,

		/** The previous interactable up. */
		PREVIOUS_INTERACTABLE_UP,

		/** The previous interactable left. */
		PREVIOUS_INTERACTABLE_LEFT, ;

		/**
		 * Checks if is next interactable.
		 * 
		 * @return true, if is next interactable
		 */
		boolean isNextInteractable()
		{
			return this == NEXT_INTERACTABLE_DOWN || this == NEXT_INTERACTABLE_RIGHT;
		}

		/**
		 * Checks if is previous interactable.
		 * 
		 * @return true, if is previous interactable
		 */
		boolean isPreviousInteractable()
		{
			return this == PREVIOUS_INTERACTABLE_LEFT || this == PREVIOUS_INTERACTABLE_UP;
		}

		/**
		 * As focus change direction.
		 * 
		 * @return the focus change direction
		 */
		FocusChangeDirection asFocusChangeDirection()
		{
			if (this == NEXT_INTERACTABLE_DOWN)
			{
				return FocusChangeDirection.DOWN;
			}
			if (this == NEXT_INTERACTABLE_RIGHT)
			{
				return FocusChangeDirection.RIGHT;
			}
			if (this == PREVIOUS_INTERACTABLE_LEFT)
			{
				return FocusChangeDirection.LEFT;
			}
			if (this == PREVIOUS_INTERACTABLE_UP)
			{
				return FocusChangeDirection.UP;
			}
			return null;
		}
	}

	/**
	 * The Enum FocusChangeDirection.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public enum FocusChangeDirection
	{

		/** The down. */
		DOWN,

		/** The right. */
		RIGHT,

		/** The up. */
		UP,

		/** The left. */
		LEFT,

	}
}
