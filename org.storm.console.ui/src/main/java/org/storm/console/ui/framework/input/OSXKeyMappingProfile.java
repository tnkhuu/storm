/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * The Class OSXKeyMappingProfile.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class OSXKeyMappingProfile extends KeyMappingProfile
{

	/** The Constant OSX_PATTERNS. */
	private static final List<CharacterPattern> OSX_PATTERNS = new ArrayList<CharacterPattern>(Arrays.asList(new CharacterPattern[] { new BasicCharacterPattern(new Key(
			Kind.Enter), '\r', '\u0000') }));

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.input.KeyMappingProfile#getPatterns()
	 */
	@Override
	Collection<CharacterPattern> getPatterns()
	{
		return OSX_PATTERNS;
	}
}
