/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import org.storm.console.ui.framework.input.InputProvider;

/**
 * The Interface Terminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Terminal extends InputProvider
{

	/**
	 * Enter private mode.
	 */
	public void enterPrivateMode();

	/**
	 * Exit private mode.
	 */
	public void exitPrivateMode();

	/**
	 * Clear screen.
	 */
	public void clearScreen();

	/**
	 * Move cursor.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void moveCursor(int x, int y);
	
	/**
	 * Move cursor.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void newLine(int y);
	
	public void deleteLeft();
	
	public void incrementXCursor(int y);

	/**
	 * Sets the cursor visible.
	 * 
	 * @param visible
	 *            the new cursor visible
	 */
	public void setCursorVisible(boolean visible);

	/**
	 * Put character.
	 * 
	 * @param c
	 *            the c
	 */
	public void putCharacter(char c);

	/**
	 * Apply sgr.
	 * 
	 * @param options
	 *            the options
	 */
	public void applySGR(SGR... options);

	/**
	 * Apply foreground color.
	 * 
	 * @param color
	 *            the color
	 */
	public void applyForegroundColor(Color color);

	/**
	 * Apply foreground color.
	 * 
	 * @param r
	 *            the r
	 * @param g
	 *            the g
	 * @param b
	 *            the b
	 */
	public void applyForegroundColor(int r, int g, int b);

	/**
	 * Apply foreground color.
	 * 
	 * @param index
	 *            the index
	 */
	public void applyForegroundColor(int index);

	/**
	 * Apply background color.
	 * 
	 * @param color
	 *            the color
	 */
	public void applyBackgroundColor(Color color);

	/**
	 * Apply background color.
	 * 
	 * @param r
	 *            the r
	 * @param g
	 *            the g
	 * @param b
	 *            the b
	 */
	public void applyBackgroundColor(int r, int g, int b);

	/**
	 * Apply background color.
	 * 
	 * @param index
	 *            the index
	 */
	public void applyBackgroundColor(int index);

	/**
	 * Adds the resize listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addResizeListener(ResizeListener listener);

	/**
	 * Removes the resize listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void removeResizeListener(ResizeListener listener);

	/**
	 * Query terminal size.
	 * 
	 * @return the terminal size
	 */
	@Deprecated
	public TerminalSize queryTerminalSize();

	/**
	 * Gets the terminal size.
	 * 
	 * @return the terminal size
	 */
	public TerminalSize getTerminalSize();

	/**
	 * Flush.
	 */
	public void flush();

	/**
	 * The Enum SGR.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public enum SGR
	{

		/** The reset all. */
		RESET_ALL,

		/** The enter bold. */
		ENTER_BOLD,

		/** The enter reverse. */
		ENTER_REVERSE,

		/** The enter underline. */
		ENTER_UNDERLINE,

		/** The enter blink. */
		ENTER_BLINK,

		/** The exit bold. */
		EXIT_BOLD,

		/** The exit reverse. */
		EXIT_REVERSE,

		/** The exit underline. */
		EXIT_UNDERLINE,

		/** The exit blink. */
		EXIT_BLINK
	}

	/**
	 * The Enum Color.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public enum Color
	{

		/** The black. */
		BLACK(0),

		/** The red. */
		RED(1),

		/** The green. */
		GREEN(2),

		/** The yellow. */
		YELLOW(3),

		/** The blue. */
		BLUE(4),

		/** The magenta. */
		MAGENTA(5),

		/** The cyan. */
		CYAN(6),

		/** The white. */
		WHITE(7),

		/** The default. */
		DEFAULT(9);

		/** The index. */
		private int index;

		/**
		 * Instantiates a new color.
		 * 
		 * @param index
		 *            the index
		 */
		private Color(int index)
		{
			this.index = index;
		}

		/**
		 * Gets the index.
		 * 
		 * @return the index
		 */
		public int getIndex()
		{
			return index;
		}
	}

	/**
	 * The listener interface for receiving resize events. The class that is
	 * interested in processing a resize event implements this interface, and
	 * the object created with that class is registered with a component using
	 * the component's <code>addResizeListener<code> method. When
	 * the resize event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see ResizeEvent
	 */
	public interface ResizeListener
	{

		/**
		 * On resized.
		 * 
		 * @param newSize
		 *            the new size
		 */
		public void onResized(TerminalSize newSize);
	}
}
