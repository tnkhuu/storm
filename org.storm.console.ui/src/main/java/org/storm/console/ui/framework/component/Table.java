/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.Container;
import org.storm.console.ui.framework.Interactable;
import org.storm.console.ui.framework.InteractableContainer;
import org.storm.console.ui.framework.Shortcuts;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.layout.LinearLayout;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class Table.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Table extends AbstractComponent implements InteractableContainer
{

	/** The shortcut helper. */
	private final Shortcuts shortcutHelper;

	/** The main panel. */
	private final Panel mainPanel;

	/** The rows. */
	private final List<Component[]> rows;

	/** The columns. */
	private Panel[] columns;

	/**
	 * Instantiates a new table.
	 */
	public Table()
	{
		this(1);
	}

	/**
	 * Instantiates a new table.
	 * 
	 * @param title
	 *            the title
	 */
	public Table(String title)
	{
		this(1, title);
	}

	/**
	 * Instantiates a new table.
	 * 
	 * @param nrOfColumns
	 *            the nr of columns
	 */
	public Table(int nrOfColumns)
	{
		this(nrOfColumns, null);
	}

	/**
	 * Instantiates a new table.
	 * 
	 * @param nrOfColumns
	 *            the nr of columns
	 * @param title
	 *            the title
	 */
	public Table(int nrOfColumns, String title)
	{
		if (title == null)
		{
			mainPanel = new Panel(Panel.Orientation.HORIZONTAL);
		}
		else
		{
			mainPanel = new Panel(title, Panel.Orientation.HORIZONTAL);
		}

		shortcutHelper = new Shortcuts();
		rows = new ArrayList<Component[]>();

		// Initialize to something to avoid null pointer exceptions
		columns = new Panel[0];
		alterTableStructure(nrOfColumns);
	}

	/**
	 * Sets the column padding size.
	 * 
	 * @param size
	 *            the new column padding size
	 */
	public void setColumnPaddingSize(int size)
	{
		((LinearLayout) mainPanel.getLayoutManager()).setPadding(size);
	}

	/**
	 * Adds the row.
	 * 
	 * @param components
	 *            the components
	 */
	public void addRow(Component... components)
	{
		Component[] newRow = new Component[columns.length];
		for (int i = 0; i < columns.length; i++)
		{
			if (i >= components.length)
			{
				newRow[i] = new EmptySpace(1, 1);
			}
			else
			{
				newRow[i] = components[i];
			}
		}
		rows.add(newRow);
		for (int i = 0; i < columns.length; i++)
		{
			columns[i].addComponent(newRow[i]);
		}
		invalidate();
	}

	/**
	 * Gets the nr of rows.
	 * 
	 * @return the nr of rows
	 */
	public int getNrOfRows()
	{
		return rows.size();
	}

	/**
	 * Gets the row.
	 * 
	 * @param index
	 *            the index
	 * @return the row
	 */
	public Component[] getRow(int index)
	{
		return Arrays.copyOf(rows.get(index), columns.length);
	}

	/**
	 * Removes the row.
	 * 
	 * @param index
	 *            the index
	 */
	public void removeRow(int index)
	{
		Component[] row = getRow(index);
		rows.remove(index);
		for (int i = 0; i < columns.length; i++)
		{
			columns[i].removeComponent(row[i]);
		}

		invalidate();
	}

	/**
	 * Alter table structure.
	 * 
	 * @param nrOfColumns
	 *            the nr of columns
	 */
	public final void alterTableStructure(int nrOfColumns)
	{
		removeAllRows();
		mainPanel.removeAllComponents();
		columns = new Panel[nrOfColumns];
		for (int i = 0; i < nrOfColumns; i++)
		{
			columns[i] = new Panel(Panel.Orientation.VERTICAL);
			mainPanel.addComponent(columns[i]);
		}
	}

	/**
	 * Removes the all rows.
	 */
	public void removeAllRows()
	{
		rows.clear();
		for (int i = 0; i < columns.length; i++)
		{
			columns[i].removeAllComponents();
		}

		invalidate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		return mainPanel.getPreferredSize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		mainPanel.repaint(graphics);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractComponent#setParent(com
	 * .googlecode.lanterna.gui.Container)
	 */
	@Override
	protected void setParent(Container container)
	{
		super.setParent(container);

		// Link the parent past the table
		mainPanel.setParent(getParent());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#hasInteractable(com
	 * .googlecode.lanterna.gui.Interactable)
	 */
	@Override
	public boolean hasInteractable(Interactable interactable)
	{
		return mainPanel.hasInteractable(interactable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#nextFocus(com.googlecode
	 * .lanterna.gui.Interactable)
	 */
	@Override
	public Interactable nextFocus(Interactable fromThis)
	{
		return mainPanel.nextFocus(fromThis);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.InteractableContainer#previousFocus(com.
	 * googlecode.lanterna.gui.Interactable)
	 */
	@Override
	public Interactable previousFocus(Interactable fromThis)
	{
		return mainPanel.previousFocus(fromThis);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#addShortcut(com.googlecode
	 * .lanterna.input.Key.Kind, org.storm.console.ui.framework.gui.Action)
	 */
	@Override
	public void addShortcut(Kind key, Action action)
	{
		shortcutHelper.addShortcut(key, action);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.InteractableContainer#addShortcut(char,
	 * boolean, boolean, org.storm.console.ui.framework.gui.Action)
	 */
	@Override
	public void addShortcut(char character, boolean withCtrl, boolean withAlt, Action action)
	{
		shortcutHelper.addShortcut(character, withCtrl, withAlt, action);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.InteractableContainer#triggerShortcut(com
	 * .googlecode.lanterna.input.Key)
	 */
	@Override
	public boolean triggerShortcut(Key key)
	{
		return shortcutHelper.triggerShortcut(key);
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle()
	{
		return mainPanel.getTitle();
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title)
	{
		mainPanel.setTitle(title);
	}
}
