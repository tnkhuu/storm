/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.component;

import java.util.HashSet;
import java.util.Set;

import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class SpinningActivityIndicator.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SpinningActivityIndicator extends ActivityIndicator
{

	/** The bars. */
	public static char[] BARS = new char[] { '-', '\\', '|', '/' };

	/** The chevrons. */
	public static char[] CHEVRONS = new char[] { '^', '>', 'V', '<' };

	/** The dice. */
	public static char[] DICE = new char[] { 0x2680, 0x2681, 0x2682, 0x2683, 0x2684, 0x2685 };

	/** The trigrams. */
	public static char[] TRIGRAMS = new char[] { 0x2630, 0x2631, 0x2632, 0x2633, 0x2634, 0x2635, 0x2636, 0x2637 };

	/** The states. */
	private static char[] states = BARS;

	/** The index. */
	private static int index = 0;

	/**
	 * Instantiates a new spinning activity indicator.
	 */
	public SpinningActivityIndicator()
	{
		this(BARS);
	}

	/**
	 * Instantiates a new spinning activity indicator.
	 * 
	 * @param chars
	 *            the chars
	 */
	public SpinningActivityIndicator(char[] chars)
	{
		Set<Character> set = new HashSet<Character>();
		for (Character ch : chars)
		{
			set.add(ch);
		}

		if (set.size() < 2)
		{
			throw new IllegalArgumentException("you must use at least two different characters");
		}

		states = chars;
		index = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.ActivityIndicator#tick()
	 */
	@Override
	public void tick()
	{
		if (++index >= states.length)
		{
			index = 0;
		}

		invalidate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.ActivityIndicator#clear()
	 */
	@Override
	public void clear()
	{
		index = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		if (index >= 0)
		{
			graphics.drawString(0, 0, states[index] + "");
		}
		else
		{
			graphics.drawString(0, 0, " ");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		return new TerminalSize(1, 1);
	}
}
