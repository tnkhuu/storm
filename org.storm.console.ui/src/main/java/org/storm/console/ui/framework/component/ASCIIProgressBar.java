/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.component;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

import java.text.MessageFormat;

import org.storm.console.ui.framework.impl.FrameBuffer;

public class ASCIIProgressBar extends AbstractDrawable implements EscapeChars
{

	private static final char DEFAULT_OPENING_CHAR = '[';
	private static final char DEFAULT_ENDING_CHAR = ']';
	private static final double DEFAULT_BAR_SIZE = 10;
	private static final char DEFAULT_FILLING_CHAR = '*';
	private static final char WHITE_SPACE = ' ';
	private char openingChar = DEFAULT_OPENING_CHAR;
	private char endingChar = DEFAULT_ENDING_CHAR;
	private double size = DEFAULT_BAR_SIZE;
	private char fillingChar = DEFAULT_FILLING_CHAR;
	private double value = 0;
	private double progressBarSize = DEFAULT_BAR_SIZE - 2;
	private int barSize = -1;

	public ASCIIProgressBar(int x, int y)
	{
		super(x, y);
	}

	public char getOpeningChar()
	{
		return openingChar;
	}

	public void setOpeningChar(char openingChar)
	{
		this.openingChar = openingChar;
	}

	public char getEndingChar()
	{
		return endingChar;
	}

	public void setEndingChar(char endingChar)
	{
		this.endingChar = endingChar;
	}

	public double getSize()
	{
		return size;
	}

	public void setSize(double size)
	{
		this.size = size;
		this.progressBarSize = size - 2;
	}

	public char getFillingChar()
	{
		return fillingChar;
	}

	public void setFillingChar(char fillingChar)
	{
		this.fillingChar = fillingChar;
	}

	public double getValue()
	{
		return value;
	}

	public void setValue(double value)
	{
		if (value < 0)
		{
			this.value = 0;
		}
		else if (value > progressBarSize)
		{
			this.value = progressBarSize;
		}
		else
		{
			this.value = value;
		}
	}

	public void increment(double incrementAmount)
	{
		value += incrementAmount;
	}

	public double getProgressBarSize()
	{
		return progressBarSize;
	}

	@Override
	public void write(FrameBuffer buffer)
	{
		buffer.append(openingChar);

		// Print the bar
		for (int i = 0; i < value; i++)
		{
			buffer.append(fillingChar);
		}
		// Print remaining chars as spaces
		for (int i = 0; i <= (size - value); i++)
		{
			buffer.append(WHITE_SPACE);
		}
		buffer.append(endingChar);

	}

	public void print()
	{

		if (barSize > 0)
		{
			System.out.print(ansi().cursorLeft(barSize).reset());
		}

		StringBuilder builder = new StringBuilder();
		builder.append(openingChar);

		// Print the bar
		for (int i = 0; i < value; i++)
		{
			builder.append(fillingChar);
		}
		// Print remaining chars as spaces
		for (int i = 0; i < (size - value); i++)
		{
			builder.append(WHITE_SPACE);
		}

		builder.append(endingChar);
		builder.append(" Percent: " + MessageFormat.format("{0,number,percent}", getValue() / getSize()));
		barSize = builder.length();
		System.out.print(ansi().fg(RED).a(builder.toString()).reset());
	}
}
