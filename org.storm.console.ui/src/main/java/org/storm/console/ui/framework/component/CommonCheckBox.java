/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.Interactable;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class CommonCheckBox.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class CommonCheckBox extends AbstractInteractableComponent
{

	/** The label. */
	private final Label label;

	/**
	 * Instantiates a new common check box.
	 * 
	 * @param label
	 *            the label
	 */
	public CommonCheckBox(final String label)
	{
		this.label = new Label(label);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		TerminalSize labelSize = label.getPreferredSize();
		labelSize.setColumns(labelSize.getColumns() + 4);
		return labelSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		graphics.applyTheme(Category.CHECKBOX);

		if (hasFocus())
		{
			graphics.applyTheme(Category.CHECKBOX_SELECTED);
		}
		else
		{
			graphics.applyTheme(Category.CHECKBOX);
		}

		char check = ' ';
		if (isSelected())
		{
			check = getSelectionCharacter();
		}

		graphics.drawString(0, 0, surroundCharacter(check));
		graphics.applyTheme(Category.CHECKBOX);
		graphics.drawString(3, 0, " ");
		TextGraphics subArea = graphics.subAreaGraphics(new TerminalPosition(4, 0));
		label.repaint(subArea);

		setHotspot(graphics.translateToGlobalCoordinates(new TerminalPosition(1, 0)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Interactable#keyboardInteraction(com.googlecode
	 * .lanterna.input.Key)
	 */
	@Override
	public Interactable.Result keyboardInteraction(Key key)
	{
		try
		{
			switch (key.getKind())
			{
			case ArrowDown:
				return Result.NEXT_INTERACTABLE_DOWN;

			case Tab:
			case ArrowRight:
				return Result.NEXT_INTERACTABLE_RIGHT;

			case ArrowUp:
				return Result.PREVIOUS_INTERACTABLE_UP;

			case ReverseTab:
			case ArrowLeft:
				return Result.PREVIOUS_INTERACTABLE_LEFT;

			case Enter:
				onActivated();
				return Result.EVENT_HANDLED;

			default:
				if (key.getCharacter() == ' ' || key.getCharacter() == 'x')
				{
					onActivated();
					return Result.EVENT_HANDLED;
				}
				return Result.EVENT_NOT_HANDLED;
			}
		}
		finally
		{
			invalidate();
		}
	}

	/**
	 * Checks if is selected.
	 * 
	 * @return true, if is selected
	 */
	public abstract boolean isSelected();

	/**
	 * Gets the selection character.
	 * 
	 * @return the selection character
	 */
	protected abstract char getSelectionCharacter();

	/**
	 * Surround character.
	 * 
	 * @param character
	 *            the character
	 * @return the string
	 */
	protected abstract String surroundCharacter(char character);

	/**
	 * On activated.
	 */
	protected abstract void onActivated();

	/**
	 * Select.
	 */
	public void select()
	{
		onActivated();
	}
}
