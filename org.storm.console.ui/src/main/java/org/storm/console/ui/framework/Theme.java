/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import java.util.EnumMap;
import java.util.Map;

import org.storm.console.ui.framework.terminal.Terminal.Color;

/**
 * The Class Theme.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Theme
{

	/** The Constant DEFAULT. */
	private static final Definition DEFAULT = new Definition(Color.BLACK, Color.WHITE, false);

	/** The Constant SELECTED. */
	private static final Definition SELECTED = new Definition(Color.WHITE, Color.BLUE, true);

	/** The styles. */
	private Map<Category, Definition> styles = new EnumMap<Category, Definition>(Category.class);

	/** The Constant DEFAULT_INSTANCE. */
	private static final Theme DEFAULT_INSTANCE = new Theme();

	/**
	 * The Enum Category.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public enum Category
	{

		/** The dialog area. */
		DIALOG_AREA,

		/** The screen background. */
		SCREEN_BACKGROUND,

		/** The shadow. */
		SHADOW,

		/** The raised border. */
		RAISED_BORDER,

		/** The border. */
		BORDER,

		/** The button active. */
		BUTTON_ACTIVE,

		/** The button inactive. */
		BUTTON_INACTIVE,

		/** The button label inactive. */
		BUTTON_LABEL_INACTIVE,

		/** The button label active. */
		BUTTON_LABEL_ACTIVE,

		/** The list item. */
		LIST_ITEM,

		/** The list item selected. */
		LIST_ITEM_SELECTED,

		/** The checkbox. */
		CHECKBOX,

		/** The checkbox selected. */
		CHECKBOX_SELECTED,

		/** The textbox. */
		TEXTBOX,

		/** The textbox focused. */
		TEXTBOX_FOCUSED,

		/** The progress bar completed. */
		PROGRESS_BAR_COMPLETED,

		/** The progress bar remaining. */
		PROGRESS_BAR_REMAINING
	}

	/**
	 * Instantiates a new theme.
	 */
	protected Theme()
	{
		setDefinition(Category.DIALOG_AREA, DEFAULT);
		setDefinition(Category.SCREEN_BACKGROUND, new Definition(Color.CYAN, Color.BLUE, true));
		setDefinition(Category.SHADOW, new Definition(Color.BLACK, Color.BLACK, true));
		setDefinition(Category.BORDER, new Definition(Color.BLACK, Color.WHITE, true));
		setDefinition(Category.RAISED_BORDER, new Definition(Color.WHITE, Color.WHITE, true));
		setDefinition(Category.BUTTON_LABEL_ACTIVE, new Definition(Color.YELLOW, Color.BLUE, true));
		setDefinition(Category.BUTTON_LABEL_INACTIVE, new Definition(Color.BLACK, Color.WHITE, true));
		setDefinition(Category.BUTTON_ACTIVE, SELECTED);
		setDefinition(Category.BUTTON_INACTIVE, DEFAULT);
		setDefinition(Category.LIST_ITEM, DEFAULT);
		setDefinition(Category.LIST_ITEM_SELECTED, SELECTED);
		setDefinition(Category.CHECKBOX, DEFAULT);
		setDefinition(Category.CHECKBOX_SELECTED, SELECTED);
		setDefinition(Category.TEXTBOX, SELECTED);
		setDefinition(Category.TEXTBOX_FOCUSED, new Definition(Color.YELLOW, Color.BLUE, true));
		setDefinition(Category.PROGRESS_BAR_COMPLETED, new Definition(Color.GREEN, Color.BLACK, false));
		setDefinition(Category.PROGRESS_BAR_REMAINING, new Definition(Color.RED, Color.BLACK, false));
	}

	/**
	 * Gets the definition.
	 * 
	 * @param category
	 *            the category
	 * @return the definition
	 */
	public Theme.Definition getDefinition(Category category)
	{
		if (styles.containsKey(category) && styles.get(category) != null)
		{
			return styles.get(category);
		}

		return getDefault();
	}

	/**
	 * Sets the definition.
	 * 
	 * @param category
	 *            the category
	 * @param def
	 *            the def
	 */
	protected void setDefinition(Category category, Definition def)
	{
		if (def == null)
		{
			styles.remove(category);
		}
		else
		{
			styles.put(category, def);
		}
	}

	/**
	 * Gets the default style.
	 * 
	 * @return the default style
	 */
	protected Definition getDefaultStyle()
	{
		return DEFAULT;
	}

	/**
	 * Gets the default.
	 * 
	 * @return the default
	 */
	@Deprecated
	protected Definition getDefault()
	{
		return getDefaultStyle();
	}

	/**
	 * Gets the dialog empty area.
	 * 
	 * @return the dialog empty area
	 */
	@Deprecated
	protected Definition getDialogEmptyArea()
	{
		return getDefinition(Category.DIALOG_AREA);
	}

	/**
	 * Gets the screen background.
	 * 
	 * @return the screen background
	 */
	@Deprecated
	protected Definition getScreenBackground()
	{
		return getDefinition(Category.SCREEN_BACKGROUND);
	}

	/**
	 * Gets the shadow.
	 * 
	 * @return the shadow
	 */
	@Deprecated
	protected Definition getShadow()
	{
		return getDefinition(Category.SHADOW);
	}

	/**
	 * Gets the border.
	 * 
	 * @return the border
	 */
	@Deprecated
	protected Definition getBorder()
	{
		return getDefinition(Category.BORDER);
	}

	/**
	 * Gets the raised border.
	 * 
	 * @return the raised border
	 */
	@Deprecated
	protected Definition getRaisedBorder()
	{
		return getDefinition(Category.RAISED_BORDER);
	}

	/**
	 * Gets the button label active.
	 * 
	 * @return the button label active
	 */
	@Deprecated
	protected Definition getButtonLabelActive()
	{
		return getDefinition(Category.BUTTON_LABEL_ACTIVE);
	}

	/**
	 * Gets the button label inactive.
	 * 
	 * @return the button label inactive
	 */
	@Deprecated
	protected Definition getButtonLabelInactive()
	{
		return getDefinition(Category.BUTTON_LABEL_INACTIVE);
	}

	/**
	 * Gets the button active.
	 * 
	 * @return the button active
	 */
	@Deprecated
	protected Definition getButtonActive()
	{
		return getDefinition(Category.BUTTON_ACTIVE);
	}

	/**
	 * Gets the button inactive.
	 * 
	 * @return the button inactive
	 */
	@Deprecated
	protected Definition getButtonInactive()
	{
		return getDefinition(Category.BUTTON_INACTIVE);
	}

	/**
	 * Gets the item.
	 * 
	 * @return the item
	 */
	@Deprecated
	protected Definition getItem()
	{
		return getDefinition(Category.LIST_ITEM);
	}

	/**
	 * Gets the item selected.
	 * 
	 * @return the item selected
	 */
	@Deprecated
	protected Definition getItemSelected()
	{
		return getDefinition(Category.LIST_ITEM_SELECTED);
	}

	/**
	 * Gets the check box.
	 * 
	 * @return the check box
	 */
	@Deprecated
	protected Definition getCheckBox()
	{
		return getDefinition(Category.CHECKBOX);
	}

	/**
	 * Gets the check box selected.
	 * 
	 * @return the check box selected
	 */
	@Deprecated
	protected Definition getCheckBoxSelected()
	{
		return getDefinition(Category.CHECKBOX_SELECTED);
	}

	/**
	 * Gets the text box focused.
	 * 
	 * @return the text box focused
	 */
	@Deprecated
	protected Definition getTextBoxFocused()
	{
		return getDefinition(Category.TEXTBOX_FOCUSED);
	}

	/**
	 * Gets the text box.
	 * 
	 * @return the text box
	 */
	@Deprecated
	protected Definition getTextBox()
	{
		return getDefinition(Category.TEXTBOX);
	}

	/**
	 * Gets the default theme.
	 * 
	 * @return the default theme
	 */
	public static Theme getDefaultTheme()
	{
		return DEFAULT_INSTANCE;
	}

	/**
	 * The Class Definition.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class Definition
	{

		/** The foreground. */
		private Color foreground;

		/** The background. */
		private Color background;

		/** The highlighted. */
		private boolean highlighted;

		/** The underlined. */
		private boolean underlined;

		/**
		 * Instantiates a new definition.
		 * 
		 * @param foreground
		 *            the foreground
		 * @param background
		 *            the background
		 */
		public Definition(Color foreground, Color background)
		{
			this(foreground, background, false);
		}

		/**
		 * Instantiates a new definition.
		 * 
		 * @param foreground
		 *            the foreground
		 * @param background
		 *            the background
		 * @param highlighted
		 *            the highlighted
		 */
		public Definition(Color foreground, Color background, boolean highlighted)
		{
			this(foreground, background, highlighted, false);
		}

		/**
		 * Instantiates a new definition.
		 * 
		 * @param foreground
		 *            the foreground
		 * @param background
		 *            the background
		 * @param highlighted
		 *            the highlighted
		 * @param underlined
		 *            the underlined
		 */
		public Definition(Color foreground, Color background, boolean highlighted, boolean underlined)
		{
			if (foreground == null)
			{
				throw new IllegalArgumentException("foreground color cannot be null");
			}

			if (background == null)
			{
				throw new IllegalArgumentException("background color cannot be null");
			}

			this.foreground = foreground;
			this.background = background;
			this.highlighted = highlighted;
			this.underlined = underlined;
		}

		/**
		 * Foreground.
		 * 
		 * @return the color
		 */
		public Color foreground()
		{
			return foreground;
		}

		/**
		 * Background.
		 * 
		 * @return the color
		 */
		public Color background()
		{
			return background;
		}

		/**
		 * Checks if is highlighted.
		 * 
		 * @return true, if is highlighted
		 */
		public boolean isHighlighted()
		{
			return highlighted;
		}

		/**
		 * Checks if is underlined.
		 * 
		 * @return true, if is underlined
		 */
		public boolean isUnderlined()
		{
			return underlined;
		}
	}
}
