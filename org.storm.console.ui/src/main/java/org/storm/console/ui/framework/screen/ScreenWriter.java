/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.screen;

import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.Terminal.Color;

/**
 * The Class ScreenWriter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ScreenWriter
{

	/** The target screen. */
	private final Screen targetScreen;

	/** The current position. */
	private final TerminalPosition currentPosition;

	/** The foreground color. */
	private Terminal.Color foregroundColor;

	/** The background color. */
	private Terminal.Color backgroundColor;

	/**
	 * Instantiates a new screen writer.
	 * 
	 * @param targetScreen
	 *            the target screen
	 */
	public ScreenWriter(final Screen targetScreen)
	{
		this.foregroundColor = Color.DEFAULT;
		this.backgroundColor = Color.DEFAULT;
		this.targetScreen = targetScreen;
		this.currentPosition = new TerminalPosition(0, 0);
	}

	/**
	 * Gets the background color.
	 * 
	 * @return the background color
	 */
	public Color getBackgroundColor()
	{
		return backgroundColor;
	}

	/**
	 * Sets the background color.
	 * 
	 * @param backgroundColor
	 *            the new background color
	 */
	public void setBackgroundColor(final Color backgroundColor)
	{
		this.backgroundColor = backgroundColor;
	}

	/**
	 * Gets the foreground color.
	 * 
	 * @return the foreground color
	 */
	public Color getForegroundColor()
	{
		return foregroundColor;
	}

	/**
	 * Sets the foreground color.
	 * 
	 * @param foregroundColor
	 *            the new foreground color
	 */
	public void setForegroundColor(final Color foregroundColor)
	{
		this.foregroundColor = foregroundColor;
	}

	/**
	 * Fill screen.
	 * 
	 * @param c
	 *            the c
	 */
	public void fillScreen(char c)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < targetScreen.getTerminalSize().getColumns(); i++)
		{
			sb.append(c);
		}

		String line = sb.toString();
		for (int i = 0; i < targetScreen.getTerminalSize().getRows(); i++)
		{
			drawString(0, i, line);
		}
	}

	/**
	 * Draw string.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param string
	 *            the string
	 * @param styles
	 *            the styles
	 */
	public void drawString(final int x, final int y, final String string, final ScreenCharacterStyle... styles)
	{
		currentPosition.setY(x);
		currentPosition.setX(y);
		targetScreen.putString(x, y, string, foregroundColor, backgroundColor, styles);
		currentPosition.setY(currentPosition.getY() + string.length());
	}
	
	/**
	 * Draw string.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param string
	 *            the string
	 * @param styles
	 *            the styles
	 */
	public void drawStringSync(final int x, final int y, final String string, final ScreenCharacterStyle... styles)
	{
		currentPosition.setY(y);
		currentPosition.setX(x);
		targetScreen.putString(x, y, string, foregroundColor, backgroundColor, styles);
		currentPosition.setY(currentPosition.getY() + string.length());
		targetScreen.getTerminal().moveCursor(string.length(), currentPosition.getY());
		targetScreen.setCursorPosition(new TerminalPosition(targetScreen.getCursorPosition().getX() + string.length(), targetScreen.getCursorPosition().getY()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return targetScreen.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof ScreenWriter == false)
		{
			return false;
		}

		return targetScreen.equals(((ScreenWriter) obj).targetScreen);
	}
}
