/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.terminal.Terminal.Color;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Interface TextGraphics.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface TextGraphics extends Canvas
{

	/**
	 * Apply theme.
	 * 
	 * @param category
	 *            the category
	 */
	void applyTheme(Theme.Category category);

	/**
	 * Apply theme.
	 * 
	 * @param themeItem
	 *            the theme item
	 */
	void applyTheme(Theme.Definition themeItem);


	/**
	 * Gets the background color.
	 * 
	 * @return the background color
	 */
	Color getBackgroundColor();

	/**
	 * Gets the foreground color.
	 * 
	 * @return the foreground color
	 */
	Color getForegroundColor();



	/**
	 * Gets the size.
	 * 
	 * @return the size
	 */
	TerminalSize getSize();

	/**
	 * Gets the theme.
	 * 
	 * @return the theme
	 */
	Theme getTheme();



	/**
	 * Sets the background color.
	 * 
	 * @param backgroundColor
	 *            the new background color
	 */
	void setBackgroundColor(Color backgroundColor);

	/**
	 * Sets the bold mask.
	 * 
	 * @param enabledBoldMask
	 *            the new bold mask
	 */
	void setBoldMask(boolean enabledBoldMask);

	/**
	 * Sets the foreground color.
	 * 
	 * @param foregroundColor
	 *            the new foreground color
	 */
	void setForegroundColor(Color foregroundColor);

	/**
	 * Sub area graphics.
	 * 
	 * @param terminalPosition
	 *            the terminal position
	 * @return the text graphics
	 */
	TextGraphics subAreaGraphics(final TerminalPosition terminalPosition);

	/**
	 * Sub area graphics.
	 * 
	 * @param topLeft
	 *            the top left
	 * @param subAreaSize
	 *            the sub area size
	 * @return the text graphics
	 */
	TextGraphics subAreaGraphics(final TerminalPosition topLeft, final TerminalSize subAreaSize);

	/**
	 * Translate to global coordinates.
	 * 
	 * @param pointInArea
	 *            the point in area
	 * @return the terminal position
	 */
	TerminalPosition translateToGlobalCoordinates(TerminalPosition pointInArea);
}
