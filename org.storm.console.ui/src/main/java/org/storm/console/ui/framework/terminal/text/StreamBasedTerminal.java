/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal.text;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import org.storm.console.ui.framework.ConsoleException;
import org.storm.console.ui.framework.input.InputDecoder;
import org.storm.console.ui.framework.terminal.ACS;
import org.storm.console.ui.framework.terminal.InputEnabledAbstractTerminal;

/**
 * The Class StreamBasedTerminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class StreamBasedTerminal extends InputEnabledAbstractTerminal
{

	/** The UT f8_ reference. */
	private static Charset UTF8_REFERENCE;
	static
	{
		try
		{
			UTF8_REFERENCE = Charset.forName("UTF-8");
		}
		catch (Exception e)
		{
			UTF8_REFERENCE = null;
		}
	}

	/** The terminal output. */
	private final OutputStream terminalOutput;

	/** The terminal charset. */
	private final Charset terminalCharset;

	/** The writer mutex. */
	protected final Object writerMutex;

	/**
	 * Instantiates a new stream based terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 */
	public StreamBasedTerminal(final InputStream terminalInput, final OutputStream terminalOutput, final Charset terminalCharset)
	{
		super(new InputDecoder(new InputStreamReader(terminalInput, terminalCharset)));
		this.writerMutex = new Object();
		this.terminalOutput = terminalOutput;
		if (terminalCharset == null)
		{
			this.terminalCharset = Charset.defaultCharset();
		}
		else
		{
			this.terminalCharset = terminalCharset;
		}
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#putCharacter(char)
	 */
	@Override
	public void putCharacter(char c)
	{
		synchronized (writerMutex)
		{
			writeToTerminal(translateCharacter(c));
		}
	}

	/**
	 * Write to terminal.
	 * 
	 * @param bytes
	 *            the bytes
	 */
	protected void writeToTerminal(final byte... bytes)
	{
		try
		{
			terminalOutput.write(bytes);
		}
		catch (IOException e)
		{
			throw new ConsoleException(e);
		}
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#flush()
	 */
	@Override
	public void flush()
	{
		try
		{
			terminalOutput.flush();
		}
		catch (IOException e)
		{
			throw new ConsoleException(e);
		}
	}

	/**
	 * Translate character.
	 * 
	 * @param input
	 *            the input
	 * @return the byte[]
	 */
	protected byte[] translateCharacter(char input)
	{
		if (UTF8_REFERENCE != null && UTF8_REFERENCE == terminalCharset)
		{
			return convertToCharset(input);
		}
		// Convert ACS to ordinary terminal codes
		switch (input)
		{
		case ACS.ARROW_DOWN:
			return convertToVT100('v');
		case ACS.ARROW_LEFT:
			return convertToVT100('<');
		case ACS.ARROW_RIGHT:
			return convertToVT100('>');
		case ACS.ARROW_UP:
			return convertToVT100('^');
		case ACS.BLOCK_DENSE:
		case ACS.BLOCK_MIDDLE:
		case ACS.BLOCK_SOLID:
		case ACS.BLOCK_SPARSE:
			return convertToVT100((char) 97);
		case ACS.HEART:
		case ACS.CLUB:
		case ACS.SPADES:
			return convertToVT100('?');
		case ACS.FACE_BLACK:
		case ACS.FACE_WHITE:
		case ACS.DIAMOND:
			return convertToVT100((char) 96);
		case ACS.DOT:
			return convertToVT100((char) 102);
		case ACS.DOUBLE_LINE_CROSS:
		case ACS.SINGLE_LINE_CROSS:
			return convertToVT100((char) 110);
		case ACS.DOUBLE_LINE_HORIZONTAL:
		case ACS.SINGLE_LINE_HORIZONTAL:
			return convertToVT100((char) 113);
		case ACS.DOUBLE_LINE_LOW_LEFT_CORNER:
		case ACS.SINGLE_LINE_LOW_LEFT_CORNER:
			return convertToVT100((char) 109);
		case ACS.DOUBLE_LINE_LOW_RIGHT_CORNER:
		case ACS.SINGLE_LINE_LOW_RIGHT_CORNER:
			return convertToVT100((char) 106);
		case ACS.DOUBLE_LINE_T_DOWN:
		case ACS.SINGLE_LINE_T_DOWN:
		case ACS.DOUBLE_LINE_T_SINGLE_DOWN:
		case ACS.SINGLE_LINE_T_DOUBLE_DOWN:
			return convertToVT100((char) 119);
		case ACS.DOUBLE_LINE_T_LEFT:
		case ACS.SINGLE_LINE_T_LEFT:
		case ACS.DOUBLE_LINE_T_SINGLE_LEFT:
		case ACS.SINGLE_LINE_T_DOUBLE_LEFT:
			return convertToVT100((char) 117);
		case ACS.DOUBLE_LINE_T_RIGHT:
		case ACS.SINGLE_LINE_T_RIGHT:
		case ACS.DOUBLE_LINE_T_SINGLE_RIGHT:
		case ACS.SINGLE_LINE_T_DOUBLE_RIGHT:
			return convertToVT100((char) 116);
		case ACS.DOUBLE_LINE_T_UP:
		case ACS.SINGLE_LINE_T_UP:
		case ACS.DOUBLE_LINE_T_SINGLE_UP:
		case ACS.SINGLE_LINE_T_DOUBLE_UP:
			return convertToVT100((char) 118);
		case ACS.DOUBLE_LINE_UP_LEFT_CORNER:
		case ACS.SINGLE_LINE_UP_LEFT_CORNER:
			return convertToVT100((char) 108);
		case ACS.DOUBLE_LINE_UP_RIGHT_CORNER:
		case ACS.SINGLE_LINE_UP_RIGHT_CORNER:
			return convertToVT100((char) 107);
		case ACS.DOUBLE_LINE_VERTICAL:
		case ACS.SINGLE_LINE_VERTICAL:
			return convertToVT100((char) 120);
		default:
			return convertToCharset(input);
		}
	}

	/**
	 * Convert to v t100.
	 * 
	 * @param code
	 *            the code
	 * @return the byte[]
	 */
	private byte[] convertToVT100(char code)
	{
		// Warning! This might be terminal type specific!!!!
		// So far it's worked everywhere I've tried it (xterm, gnome-terminal,
		// putty)
		return new byte[] { 27, 40, 48, (byte) code, 27, 40, 66 };
	}

	/**
	 * Convert to charset.
	 * 
	 * @param input
	 *            the input
	 * @return the byte[]
	 */
	private byte[] convertToCharset(char input)
	{
		// TODO: This is a silly way to do it, improve?
		final char[] buffer = new char[1];
		buffer[0] = input;
		return terminalCharset.encode(CharBuffer.wrap(buffer)).array();
	}

}
