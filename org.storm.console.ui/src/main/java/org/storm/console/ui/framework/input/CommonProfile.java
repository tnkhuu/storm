/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * The Class CommonProfile.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CommonProfile extends KeyMappingProfile
{

	/** The Constant COMMON_PATTERNS. */
	private static final List<CharacterPattern> COMMON_PATTERNS = new ArrayList<CharacterPattern>(Arrays.asList(new CharacterPattern[] {
			new BasicCharacterPattern(new Key(Kind.ArrowUp), ESC_CODE, '[', 'A'),
			new BasicCharacterPattern(new Key(Kind.ArrowUp, false, true), ESC_CODE, ESC_CODE, '[', 'A'),
			new BasicCharacterPattern(new Key(Kind.ArrowDown), ESC_CODE, '[', 'B'),
			new BasicCharacterPattern(new Key(Kind.ArrowDown, false, true), ESC_CODE, ESC_CODE, '[', 'B'),
			new BasicCharacterPattern(new Key(Kind.ArrowRight), ESC_CODE, '[', 'C'),
			new BasicCharacterPattern(new Key(Kind.ArrowRight, false, true), ESC_CODE, ESC_CODE, '[', 'C'),
			new BasicCharacterPattern(new Key(Kind.ArrowLeft), ESC_CODE, '[', 'D'),
			new BasicCharacterPattern(new Key(Kind.ArrowLeft, false, true), ESC_CODE, ESC_CODE, '[', 'D'),
			new BasicCharacterPattern(new Key(Kind.Tab), '\t'),
			new BasicCharacterPattern(new Key(Kind.Enter), '\n'),
			new BasicCharacterPattern(new Key(Kind.ReverseTab), ESC_CODE, '[', 'Z'),
			new BasicCharacterPattern(new Key(Kind.Backspace), (char) 0x7f),
			new BasicCharacterPattern(new Key(Kind.Insert), ESC_CODE, '[', '2', '~'),
			new BasicCharacterPattern(new Key(Kind.Insert, false, true), ESC_CODE, ESC_CODE, '[', '2', '~'),
			new BasicCharacterPattern(new Key(Kind.Delete), ESC_CODE, '[', '3', '~'),
			new BasicCharacterPattern(new Key(Kind.Delete, false, true), ESC_CODE, ESC_CODE, '[', '3', '~'),
			new BasicCharacterPattern(new Key(Kind.Home), ESC_CODE, '[', 'H'),
			new BasicCharacterPattern(new Key(Kind.Home, false, true), ESC_CODE, ESC_CODE, '[', 'H'),
			new BasicCharacterPattern(new Key(Kind.End), ESC_CODE, '[', 'F'),
			new BasicCharacterPattern(new Key(Kind.End, false, true), ESC_CODE, ESC_CODE, '[', 'F'),
			new BasicCharacterPattern(new Key(Kind.PageUp), ESC_CODE, '[', '5', '~'),
			new BasicCharacterPattern(new Key(Kind.PageUp, false, true), ESC_CODE, ESC_CODE, '[', '5', '~'),
			new BasicCharacterPattern(new Key(Kind.PageDown), ESC_CODE, '[', '6', '~'),
			new BasicCharacterPattern(new Key(Kind.PageDown, false, true), ESC_CODE, ESC_CODE, '[', '6', '~'),
			new BasicCharacterPattern(new Key(Kind.F1), ESC_CODE, 'O', 'P'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F1), ESC_CODE, '[', '1', '1', '~'),
			new BasicCharacterPattern(new Key(Kind.F2), ESC_CODE, 'O', 'Q'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F2), ESC_CODE, '[', '1', '2', '~'),
			new BasicCharacterPattern(new Key(Kind.F3), ESC_CODE, 'O', 'R'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F3), ESC_CODE, '[', '1', '3', '~'),
			new BasicCharacterPattern(new Key(Kind.F4), ESC_CODE, 'O', 'S'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F4), ESC_CODE, '[', '1', '4', '~'),
			new BasicCharacterPattern(new Key(Kind.F5), ESC_CODE, '[', '1', '5', '~'),
			new BasicCharacterPattern(new Key(Kind.F6), ESC_CODE, '[', '1', '7', '~'),
			new BasicCharacterPattern(new Key(Kind.F7), ESC_CODE, '[', '1', '8', '~'),
			new BasicCharacterPattern(new Key(Kind.F8), ESC_CODE, '[', '1', '9', '~'),
			new BasicCharacterPattern(new Key(Kind.F9), ESC_CODE, '[', '2', '0', '~'),
			new BasicCharacterPattern(new Key(Kind.F10), ESC_CODE, '[', '2', '1', '~'),
			new BasicCharacterPattern(new Key(Kind.F11), ESC_CODE, '[', '2', '3', '~'),
			new BasicCharacterPattern(new Key(Kind.F12), ESC_CODE, '[', '2', '4', '~'),
			// Function keys with alt
			new BasicCharacterPattern(new Key(Kind.F1, false, true), ESC_CODE, ESC_CODE, 'O', 'P'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F1, false, true), ESC_CODE, ESC_CODE, '[', '1', '1', '~'),
			new BasicCharacterPattern(new Key(Kind.F2, false, true), ESC_CODE, ESC_CODE, 'O', 'Q'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F2, false, true), ESC_CODE, ESC_CODE, '[', '1', '2', '~'),
			new BasicCharacterPattern(new Key(Kind.F3, false, true), ESC_CODE, ESC_CODE, 'O', 'R'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F3, false, true), ESC_CODE, ESC_CODE, '[', '1', '3', '~'),
			new BasicCharacterPattern(new Key(Kind.F4, false, true), ESC_CODE, ESC_CODE, 'O', 'S'), // Cygwin
			new BasicCharacterPattern(new Key(Kind.F4, false, true), ESC_CODE, ESC_CODE, '[', '1', '4', '~'),
			new BasicCharacterPattern(new Key(Kind.F5, false, true), ESC_CODE, ESC_CODE, '[', '1', '5', '~'),
			new BasicCharacterPattern(new Key(Kind.F6, false, true), ESC_CODE, ESC_CODE, '[', '1', '7', '~'),
			new BasicCharacterPattern(new Key(Kind.F7, false, true), ESC_CODE, ESC_CODE, '[', '1', '8', '~'),
			new BasicCharacterPattern(new Key(Kind.F8, false, true), ESC_CODE, ESC_CODE, '[', '1', '9', '~'),
			new BasicCharacterPattern(new Key(Kind.F9, false, true), ESC_CODE, ESC_CODE, '[', '2', '0', '~'),
			new BasicCharacterPattern(new Key(Kind.F10, false, true), ESC_CODE, ESC_CODE, '[', '2', '1', '~'),
			new BasicCharacterPattern(new Key(Kind.F11, false, true), ESC_CODE, ESC_CODE, '[', '2', '3', '~'),
			new BasicCharacterPattern(new Key(Kind.F12, false, true), ESC_CODE, ESC_CODE, '[', '2', '4', '~'), new AltAndCharacterPattern(), new CtrlAndCharacterPattern(),
			new CtrlAltAndCharacterPattern(), new ScreenInfoCharacterPattern() }));

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.input.KeyMappingProfile#getPatterns()
	 */
	@Override
	Collection<CharacterPattern> getPatterns()
	{
		return new ArrayList<CharacterPattern>(COMMON_PATTERNS);
	}

}
