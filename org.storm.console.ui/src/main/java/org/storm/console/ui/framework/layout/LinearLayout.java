/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.layout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class LinearLayout.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class LinearLayout implements LayoutManager
{

	/** The Constant MAXIMIZES_HORIZONTALLY. */
	public static final LayoutParameter MAXIMIZES_HORIZONTALLY = new LayoutParameter("LinearLayout.MAXIMIZES_HORIZONTALLY");

	/** The Constant MAXIMIZES_VERTICALLY. */
	public static final LayoutParameter MAXIMIZES_VERTICALLY = new LayoutParameter("LinearLayout.MAXIMIZES_VERTICALLY");

	/** The Constant GROWS_HORIZONTALLY. */
	public static final LayoutParameter GROWS_HORIZONTALLY = new LayoutParameter("LinearLayout.GROWS_HORIZONTALLY");

	/** The Constant GROWS_VERTICALLY. */
	public static final LayoutParameter GROWS_VERTICALLY = new LayoutParameter("LinearLayout.GROWS_VERTICALLY");

	/** The component list. */
	private final List<LinearLayoutComponent> componentList;

	/** The padding. */
	private int padding;

	/**
	 * Instantiates a new linear layout.
	 */
	LinearLayout()
	{
		this.componentList = new ArrayList<LinearLayoutComponent>();
		this.padding = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LayoutManager#addComponent(com.googlecode
	 * .lanterna.gui.Component,
	 * org.storm.console.ui.framework.gui.layout.LayoutParameter[])
	 */
	@Override
	public void addComponent(Component component, LayoutParameter... layoutParameters)
	{
		Set<LayoutParameter> asSet = new HashSet<LayoutParameter>(Arrays.asList(layoutParameters));
		if (asSet.contains(MAXIMIZES_HORIZONTALLY) && asSet.contains(GROWS_HORIZONTALLY))
		{
			throw new IllegalArgumentException("Component " + component + " cannot be both maximizing and growing horizontally at the same time");
		}

		if (asSet.contains(MAXIMIZES_VERTICALLY) && asSet.contains(GROWS_VERTICALLY))
		{
			throw new IllegalArgumentException("Component " + component + " cannot be both maximizing and growing vertically at the same time");
		}

		componentList.add(new LinearLayoutComponent(component, asSet));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LayoutManager#removeComponent(com.
	 * googlecode.lanterna.gui.Component)
	 */
	@Override
	public void removeComponent(Component component)
	{
		Iterator<LinearLayoutComponent> iterator = componentList.iterator();
		while (iterator.hasNext())
		{
			if (iterator.next().component == component)
			{
				iterator.remove();
				return;
			}
		}
	}

	/**
	 * Sets the padding.
	 * 
	 * @param padding
	 *            the new padding
	 */
	public void setPadding(int padding)
	{
		this.padding = padding;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.layout.LayoutManager#getPreferredSize()
	 */
	@Override
	public TerminalSize getPreferredSize()
	{
		if (componentList.isEmpty())
		{
			return new TerminalSize(0, 0);
		}

		TerminalSize preferredSize = new TerminalSize(0, 0);
		for (LinearLayoutComponent axisLayoutComponent : componentList)
		{
			final TerminalSize componentPreferredSize = axisLayoutComponent.component.getPreferredSize();
			setMajorAxis(preferredSize, getMajorAxis(preferredSize) + getMajorAxis(componentPreferredSize));
			setMinorAxis(preferredSize, Math.max(getMinorAxis(preferredSize), getMinorAxis(componentPreferredSize)));
		}
		setMajorAxis(preferredSize, getMajorAxis(preferredSize) + padding * (componentList.size() - 1));
		return preferredSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LayoutManager#layout(com.googlecode
	 * .lanterna.terminal.TerminalSize)
	 */
	@Override
	public List<? extends LaidOutComponent> layout(TerminalSize layoutArea)
	{
		List<DefaultLaidOutComponent> result = new ArrayList<DefaultLaidOutComponent>();
		Map<Component, TerminalSize> minimumSizeMap = new IdentityHashMap<Component, TerminalSize>();
		Map<Component, TerminalSize> preferredSizeMap = new IdentityHashMap<Component, TerminalSize>();
		Map<Component, Set<LayoutParameter>> layoutParameterMap = new IdentityHashMap<Component, Set<LayoutParameter>>();
		for (LinearLayoutComponent llc : componentList)
		{
			minimumSizeMap.put(llc.component, llc.component.getMinimumSize());
			preferredSizeMap.put(llc.component, llc.component.getPreferredSize());
			layoutParameterMap.put(llc.component, llc.layoutParameters);
		}

		int availableMajorAxisSpace = getMajorAxis(layoutArea);
		int availableMinorAxisSpace = getMinorAxis(layoutArea);

		for (LinearLayoutComponent llc : componentList)
		{
			result.add(new DefaultLaidOutComponent(llc.component, new TerminalSize(0, 0), new TerminalPosition(0, 0)));
		}

		// Set minor axis - easy!
		for (DefaultLaidOutComponent lloc : result)
		{
			if (layoutParameterMap.get(lloc.component).contains(getMinorMaximizesParameter()) || layoutParameterMap.get(lloc.component).contains(getMinorGrowingParameter())
					|| lloc.component instanceof Panel && maximisesOnMinorAxis((Panel) lloc.component))
			{
				setMinorAxis(lloc.size, availableMinorAxisSpace);
			}
			else
			{
				int preferred = getMinorAxis(preferredSizeMap.get(lloc.component));
				setMinorAxis(lloc.size, preferred <= availableMinorAxisSpace ? preferred : availableMinorAxisSpace);
			}
		}

		// Start dividing the major axis - hard!
		while (availableMajorAxisSpace > 0)
		{
			boolean changedSomething = false;
			for (DefaultLaidOutComponent lloc : result)
			{
				int preferred = getMajorAxis(preferredSizeMap.get(lloc.component));
				if (availableMajorAxisSpace > 0 && preferred > getMajorAxis(lloc.getSize()))
				{
					availableMajorAxisSpace--;
					setMajorAxis(lloc.getSize(), getMajorAxis(lloc.getSize()) + 1);
					changedSomething = true;
				}
			}
			if (!changedSomething)
			{
				break;
			}
		}

		// Add padding, if any (this could cause availableMajorAxisSpace to go
		// negative!! Beware!)
		availableMajorAxisSpace -= (result.size() - 1) * padding;

		// Now try to accomodate the growing major axis components
		List<DefaultLaidOutComponent> growingComponents = new ArrayList<DefaultLaidOutComponent>();
		for (DefaultLaidOutComponent lloc : result)
		{
			if (layoutParameterMap.get(lloc.component).contains(getMajorMaximizesParameter()) || layoutParameterMap.get(lloc.component).contains(getMajorGrowingParameter()))
			{
				growingComponents.add(lloc);
			}

			if (lloc.component instanceof Panel && maximisesOnMajorAxis((Panel) lloc.component))
			{
				growingComponents.add(lloc);
			}
		}

		while (availableMajorAxisSpace > 0 && !growingComponents.isEmpty())
		{
			for (DefaultLaidOutComponent lloc : growingComponents)
			{
				if (availableMajorAxisSpace > 0)
				{
					availableMajorAxisSpace--;
					setMajorAxis(lloc.getSize(), getMajorAxis(lloc.getSize()) + 1);
				}
			}
		}

		// Finally, recalculate the topLeft position of each component
		int nextMajorPosition = 0;
		for (DefaultLaidOutComponent laidOutComponent : result)
		{
			setMajorAxis(laidOutComponent.topLeftPosition, nextMajorPosition);

			// Make sure not to add padding to the last component
			if (result.get(result.size() - 1) != laidOutComponent)
			{
				nextMajorPosition += getMajorAxis(laidOutComponent.size) + padding;
			}
			else
			{
				nextMajorPosition += getMajorAxis(laidOutComponent.size);
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LayoutManager#maximisesHorisontally()
	 */
	@Override
	public boolean maximisesHorisontally()
	{
		for (LinearLayoutComponent llc : componentList)
		{
			if (llc.layoutParameters.contains(MAXIMIZES_HORIZONTALLY))
			{
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LayoutManager#maximisesVertically()
	 */
	@Override
	public boolean maximisesVertically()
	{
		for (LinearLayoutComponent llc : componentList)
		{
			if (llc.layoutParameters.contains(MAXIMIZES_VERTICALLY))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Maximises on major axis.
	 * 
	 * @param panel
	 *            the panel
	 * @return true, if successful
	 */
	protected abstract boolean maximisesOnMajorAxis(Panel panel);

	/**
	 * Maximises on minor axis.
	 * 
	 * @param panel
	 *            the panel
	 * @return true, if successful
	 */
	protected abstract boolean maximisesOnMinorAxis(Panel panel);

	/**
	 * Sets the major axis.
	 * 
	 * @param terminalSize
	 *            the terminal size
	 * @param majorAxisValue
	 *            the major axis value
	 */
	protected abstract void setMajorAxis(TerminalSize terminalSize, int majorAxisValue);

	/**
	 * Sets the minor axis.
	 * 
	 * @param terminalSize
	 *            the terminal size
	 * @param minorAxisValue
	 *            the minor axis value
	 */
	protected abstract void setMinorAxis(TerminalSize terminalSize, int minorAxisValue);

	/**
	 * Sets the major axis.
	 * 
	 * @param terminalPosition
	 *            the terminal position
	 * @param majorAxisValue
	 *            the major axis value
	 */
	protected abstract void setMajorAxis(TerminalPosition terminalPosition, int majorAxisValue);

	/**
	 * Gets the major axis.
	 * 
	 * @param terminalSize
	 *            the terminal size
	 * @return the major axis
	 */
	protected abstract int getMajorAxis(TerminalSize terminalSize);

	/**
	 * Gets the minor axis.
	 * 
	 * @param terminalSize
	 *            the terminal size
	 * @return the minor axis
	 */
	protected abstract int getMinorAxis(TerminalSize terminalSize);

	/**
	 * Gets the major maximizes parameter.
	 * 
	 * @return the major maximizes parameter
	 */
	protected abstract LayoutParameter getMajorMaximizesParameter();

	/**
	 * Gets the minor maximizes parameter.
	 * 
	 * @return the minor maximizes parameter
	 */
	protected abstract LayoutParameter getMinorMaximizesParameter();

	/**
	 * Gets the major growing parameter.
	 * 
	 * @return the major growing parameter
	 */
	protected abstract LayoutParameter getMajorGrowingParameter();

	/**
	 * Gets the minor growing parameter.
	 * 
	 * @return the minor growing parameter
	 */
	protected abstract LayoutParameter getMinorGrowingParameter();

	/**
	 * Gets the sub panels.
	 * 
	 * @return the sub panels
	 */
	protected List<Panel> getSubPanels()
	{
		List<Panel> subPanels = new ArrayList<Panel>();
		for (LinearLayoutComponent axisLayoutComponent : componentList)
		{
			if (axisLayoutComponent.component instanceof Panel)
			{
				subPanels.add((Panel) axisLayoutComponent.component);
			}
		}
		return subPanels;
	}

	/**
	 * The Class LinearLayoutComponent.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	protected static class LinearLayoutComponent
	{

		/** The component. */
		public Component component;

		/** The layout parameters. */
		public Set<LayoutParameter> layoutParameters;

		/**
		 * Instantiates a new linear layout component.
		 * 
		 * @param component
		 *            the component
		 * @param layoutParameters
		 *            the layout parameters
		 */
		public LinearLayoutComponent(Component component, Set<LayoutParameter> layoutParameters)
		{
			this.component = component;
			this.layoutParameters = layoutParameters;
		}
	}
}
