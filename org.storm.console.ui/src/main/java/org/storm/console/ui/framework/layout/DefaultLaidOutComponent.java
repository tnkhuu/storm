/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.layout;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class DefaultLaidOutComponent.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class DefaultLaidOutComponent implements LayoutManager.LaidOutComponent
{

	/** The component. */
	final Component component;

	/** The size. */
	final TerminalSize size;

	/** The top left position. */
	final TerminalPosition topLeftPosition;

	/**
	 * Instantiates a new default laid out component.
	 * 
	 * @param component
	 *            the component
	 * @param size
	 *            the size
	 * @param topLeftPosition
	 *            the top left position
	 */
	public DefaultLaidOutComponent(Component component, TerminalSize size, TerminalPosition topLeftPosition)
	{
		this.component = component;
		this.size = size;
		this.topLeftPosition = topLeftPosition;
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager.LaidOutComponent#getComponent()
	 */
	@Override
	public Component getComponent()
	{
		return component;
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager.LaidOutComponent#getSize()
	 */
	@Override
	public TerminalSize getSize()
	{
		return size;
	}

	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.layout.LayoutManager.LaidOutComponent#getTopLeftPosition()
	 */
	@Override
	public TerminalPosition getTopLeftPosition()
	{
		return topLeftPosition;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[" + component + " @ " + topLeftPosition + " size " + size + "]";
	}

}
