/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.screen;

/**
 * The Enum TabBehaviour.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public enum TabBehaviour
{

	/** The convert to one space. */
	CONVERT_TO_ONE_SPACE,

	/** The convert to four spaces. */
	CONVERT_TO_FOUR_SPACES,

	/** The convert to eight spaces. */
	CONVERT_TO_EIGHT_SPACES,

	/** The ALIG n_ t o_ colum n_4. */
	ALIGN_TO_COLUMN_4,

	/** The ALIG n_ t o_ colum n_8. */
	ALIGN_TO_COLUMN_8, ;

	/**
	 * Replace tabs.
	 * 
	 * @param string
	 *            the string
	 * @param x
	 *            the x
	 * @return the string
	 */
	public String replaceTabs(String string, int x)
	{
		int tabPosition = string.indexOf('\t');
		while (tabPosition != -1)
		{
			String tabReplacementHere = getTabReplacement(x);
			string = string.substring(0, tabPosition) + tabReplacementHere + string.substring(tabPosition + 1);
			tabPosition += tabReplacementHere.length();
			tabPosition = string.indexOf('\t', tabPosition);
		}
		return string;
	}

	/**
	 * Gets the tab replacement.
	 * 
	 * @param x
	 *            the x
	 * @return the tab replacement
	 */
	private String getTabReplacement(int x)
	{
		int align = 0;
		switch (this)
		{
		case CONVERT_TO_ONE_SPACE:
			return " ";
		case CONVERT_TO_FOUR_SPACES:
			return "    ";
		case CONVERT_TO_EIGHT_SPACES:
			return "        ";
		case ALIGN_TO_COLUMN_4:
			align = 4 - x % 4;
			break;
		case ALIGN_TO_COLUMN_8:
			align = 8 - x % 8;
			break;
		}
		StringBuilder replace = new StringBuilder();
		for (int i = 0; i < align; i++)
		{
			replace.append(" ");
		}
		return replace.toString();
	}
}
