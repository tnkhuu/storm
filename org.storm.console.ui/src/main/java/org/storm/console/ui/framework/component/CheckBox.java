/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

/**
 * The Class CheckBox.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CheckBox extends CommonCheckBox
{

	/** The selected. */
	private boolean selected;

	/**
	 * Instantiates a new check box.
	 * 
	 * @param label
	 *            the label
	 * @param initiallyChecked
	 *            the initially checked
	 */
	public CheckBox(final String label, final boolean initiallyChecked)
	{
		super(label);
		this.selected = initiallyChecked;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.CommonCheckBox#getSelectionCharacter
	 * ()
	 */
	@Override
	protected char getSelectionCharacter()
	{
		return 'x';
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.CommonCheckBox#isSelected()
	 */
	@Override
	public boolean isSelected()
	{
		return selected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.CommonCheckBox#onActivated()
	 */
	@Override
	protected void onActivated()
	{
		selected = !selected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.CommonCheckBox#surroundCharacter
	 * (char)
	 */
	@Override
	protected String surroundCharacter(char character)
	{
		return "[" + character + "]";
	}

}
