/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import org.storm.console.ui.framework.layout.LayoutParameter;
import org.storm.console.ui.framework.listener.ContainerListener;

/**
 * The Interface Container.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Container extends Component
{

	/**
	 * Adds the container listener.
	 * 
	 * @param cl
	 *            the cl
	 */
	void addContainerListener(ContainerListener cl);

	/**
	 * Removes the container listener.
	 * 
	 * @param cl
	 *            the cl
	 */
	void removeContainerListener(ContainerListener cl);

	/**
	 * Adds the component.
	 * 
	 * @param component
	 *            the component
	 * @param layoutParameters
	 *            the layout parameters
	 */
	void addComponent(Component component, LayoutParameter... layoutParameters);

	/**
	 * Removes the component.
	 * 
	 * @param component
	 *            the component
	 * @return true, if successful
	 */
	boolean removeComponent(Component component);

	/**
	 * Gets the component count.
	 * 
	 * @return the component count
	 */
	int getComponentCount();

	/**
	 * Gets the component at.
	 * 
	 * @param index
	 *            the index
	 * @return the component at
	 */
	Component getComponentAt(int index);

	/**
	 * Contains component.
	 * 
	 * @param component
	 *            the component
	 * @return true, if successful
	 */
	boolean containsComponent(Component component);
}
