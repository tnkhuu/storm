/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal.text;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.nio.charset.Charset;

import org.storm.console.ui.framework.ConsoleException;
import org.storm.console.ui.framework.input.GnomeTerminalProfile;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.OSXKeyMappingProfile;
import org.storm.console.ui.framework.input.PuttyProfile;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class UnixTerminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class UnixTerminal extends ANSITerminal
{

	/** The terminal size querier. */
	private final UnixTerminalSizeQuerier terminalSizeQuerier;

	/**
	 * The Enum Behaviour.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum Behaviour
	{

		/** The default. */
		DEFAULT,

		/** The ctrl c kills application. */
		CTRL_C_KILLS_APPLICATION,
	}

	/** The terminal behaviour. */
	private final Behaviour terminalBehaviour;

	/** The stty status to restore. */
	private String sttyStatusToRestore;

	/**
	 * Instantiates a new unix terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 */
	public UnixTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset)
	{
		this(terminalInput, terminalOutput, terminalCharset, null);
	}

	/**
	 * Instantiates a new unix terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 * @param customSizeQuerier
	 *            the custom size querier
	 */
	public UnixTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset, UnixTerminalSizeQuerier customSizeQuerier)
	{
		this(terminalInput, terminalOutput, terminalCharset, customSizeQuerier, Behaviour.CTRL_C_KILLS_APPLICATION);
	}

	/**
	 * Instantiates a new unix terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 * @param customSizeQuerier
	 *            the custom size querier
	 * @param terminalBehaviour
	 *            the terminal behaviour
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public UnixTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset, UnixTerminalSizeQuerier customSizeQuerier, Behaviour terminalBehaviour)
	{
		super(terminalInput, terminalOutput, terminalCharset);
		this.terminalSizeQuerier = customSizeQuerier;
		this.terminalBehaviour = terminalBehaviour;
		this.sttyStatusToRestore = null;
		addInputProfile(new GnomeTerminalProfile());
		addInputProfile(new PuttyProfile());
		addInputProfile(new OSXKeyMappingProfile());

		// Make sure to set an initial size
		onResized(80, 20);
		try
		{
			Class signalClass = Class.forName("sun.misc.Signal");
			for (Method m : signalClass.getDeclaredMethods())
			{
				if ("handle".equals(m.getName()))
				{
					Object windowResizeHandler = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Class.forName("sun.misc.SignalHandler") },
							new InvocationHandler()
							{
								@Override
								public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
								{
									if ("handle".equals(method.getName()))
									{
										queryTerminalSize();
									}
									return null;
								}
							});
					m.invoke(null, signalClass.getConstructor(String.class).newInstance("WINCH"), windowResizeHandler);
				}
			}
		}
		catch (Throwable e)
		{
			System.err.println(e.getMessage());
		}
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#queryTerminalSize()
	 */
	@Deprecated
	@Override
	public TerminalSize queryTerminalSize()
	{
		if (terminalSizeQuerier != null)
		{
			return terminalSizeQuerier.queryTerminalSize();
		}

		return super.queryTerminalSize();
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#getTerminalSize()
	 */
	@Override
	public TerminalSize getTerminalSize()
	{
		if (terminalSizeQuerier != null)
		{
			return terminalSizeQuerier.queryTerminalSize();
		}

		return super.getTerminalSize();
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.InputEnabledAbstractTerminal#readInput()
	 */
	@Override
	public Key readInput()
	{
		// Check if we have ctrl+c coming
		Key key = super.readInput();
		if (key != null && terminalBehaviour == Behaviour.CTRL_C_KILLS_APPLICATION && key.getCharacter() == 'c' && !key.isAltPressed() && key.isCtrlPressed())
		{

			exitPrivateMode();
			System.exit(1);
		}
		return key;
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#enterPrivateMode()
	 */
	@Override
	public void enterPrivateMode()
	{
		super.enterPrivateMode();
		saveSTTY();
		setCBreak(true);
		setEcho(false);
		sttyMinimumCharacterForRead(1);
		disableSpecialCharacters();
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#exitPrivateMode()
	 */
	@Override
	public void exitPrivateMode()
	{
		super.exitPrivateMode();
		restoreSTTY();
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#setCBreak(boolean)
	 */
	@Override
	public void setCBreak(boolean cbreakOn)
	{
		sttyICanon(cbreakOn);
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.text.ANSITerminal#setEcho(boolean)
	 */
	@Override
	public void setEcho(boolean echoOn)
	{
		sttyKeyEcho(echoOn);
	}

	/**
	 * Stty key echo.
	 * 
	 * @param enable
	 *            the enable
	 */
	private static void sttyKeyEcho(final boolean enable)
	{
		exec("/bin/sh", "-c", "/bin/stty " + (enable ? "echo" : "-echo") + " < /dev/tty");
	}

	/**
	 * Stty minimum character for read.
	 * 
	 * @param nrCharacters
	 *            the nr characters
	 */
	private static void sttyMinimumCharacterForRead(final int nrCharacters)
	{
		exec("/bin/sh", "-c", "/bin/stty min " + nrCharacters + " < /dev/tty");
	}

	/**
	 * Stty i canon.
	 * 
	 * @param enable
	 *            the enable
	 */
	private static void sttyICanon(final boolean enable)
	{
		exec("/bin/sh", "-c", "/bin/stty " + (enable ? "-icanon" : "icanon") + " < /dev/tty");
	}


	/**
	 * Disable special characters.
	 */
	private static void disableSpecialCharacters()
	{
		exec("/bin/sh", "-c", "/bin/stty intr undef < /dev/tty");
		exec("/bin/sh", "-c", "/bin/stty start undef < /dev/tty");
		exec("/bin/sh", "-c", "/bin/stty stop undef < /dev/tty");
		exec("/bin/sh", "-c", "/bin/stty susp undef < /dev/tty");
	}

	/**
	 * Save stty.
	 */
	private void saveSTTY()
	{
		sttyStatusToRestore = exec("/bin/sh", "-c", "stty -g < /dev/tty").trim();
	}

	/**
	 * Restore stty.
	 */
	private void restoreSTTY()
	{
		if (sttyStatusToRestore == null)
		{
			// Nothing to restore
			return;
		}

		exec("/bin/sh", "-c", "stty " + sttyStatusToRestore + " < /dev/tty");
		sttyStatusToRestore = null;
	}

	/**
	 * Exec.
	 * 
	 * @param cmd
	 *            the cmd
	 * @return the string
	 */
	private static String exec(String... cmd)
	{
		try
		{
			ProcessBuilder pb = new ProcessBuilder(cmd);
			Process process = pb.start();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream stdout = process.getInputStream();
			int readByte = stdout.read();
			while (readByte >= 0)
			{
				baos.write(readByte);
				readByte = stdout.read();
			}
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			BufferedReader reader = new BufferedReader(new InputStreamReader(bais));
			StringBuilder builder = new StringBuilder();
			while (reader.ready())
			{
				builder.append(reader.readLine());
			}
			reader.close();
			return builder.toString();
		}
		catch (IOException e)
		{
			throw new ConsoleException(e);
		}
	}
}
