/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.layout;

import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class VerticalLayout.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class VerticalLayout extends LinearLayout
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#getMajorAxis(com.googlecode
	 * .lanterna.terminal.TerminalSize)
	 */
	@Override
	protected int getMajorAxis(TerminalSize terminalSize)
	{
		return terminalSize.getRows();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#getMinorAxis(com.googlecode
	 * .lanterna.terminal.TerminalSize)
	 */
	@Override
	protected int getMinorAxis(TerminalSize terminalSize)
	{
		return terminalSize.getColumns();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#setMajorAxis(com.googlecode
	 * .lanterna.terminal.TerminalSize, int)
	 */
	@Override
	protected void setMajorAxis(TerminalSize terminalSize, int majorAxisValue)
	{
		terminalSize.setRows(majorAxisValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#setMajorAxis(com.googlecode
	 * .lanterna.terminal.TerminalPosition, int)
	 */
	@Override
	protected void setMajorAxis(TerminalPosition terminalPosition, int majorAxisValue)
	{
		terminalPosition.setX(majorAxisValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#setMinorAxis(com.googlecode
	 * .lanterna.terminal.TerminalSize, int)
	 */
	@Override
	protected void setMinorAxis(TerminalSize terminalSize, int minorAxisValue)
	{
		terminalSize.setColumns(minorAxisValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#getMajorMaximizesParameter
	 * ()
	 */
	@Override
	protected LayoutParameter getMajorMaximizesParameter()
	{
		return LinearLayout.MAXIMIZES_VERTICALLY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#getMinorMaximizesParameter
	 * ()
	 */
	@Override
	protected LayoutParameter getMinorMaximizesParameter()
	{
		return LinearLayout.MAXIMIZES_HORIZONTALLY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#getMajorGrowingParameter
	 * ()
	 */
	@Override
	protected LayoutParameter getMajorGrowingParameter()
	{
		return LinearLayout.GROWS_VERTICALLY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#getMinorGrowingParameter
	 * ()
	 */
	@Override
	protected LayoutParameter getMinorGrowingParameter()
	{
		return LinearLayout.GROWS_HORIZONTALLY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#maximisesOnMajorAxis(
	 * org.storm.console.ui.framework.gui.component.Panel)
	 */
	@Override
	protected boolean maximisesOnMajorAxis(Panel panel)
	{
		return panel.maximisesVertically();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.layout.LinearLayout#maximisesOnMinorAxis(
	 * org.storm.console.ui.framework.gui.component.Panel)
	 */
	@Override
	protected boolean maximisesOnMinorAxis(Panel panel)
	{
		return panel.maximisesHorisontally();
	}
}
