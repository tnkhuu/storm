/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.screen;

import java.util.EnumSet;
import java.util.Set;

import org.storm.console.ui.framework.terminal.Terminal;

/**
 * The Class ScreenCharacter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class ScreenCharacter
{

	/** The character. */
	private final char character;

	/** The foreground color. */
	private final Terminal.Color foregroundColor;

	/** The background color. */
	private final Terminal.Color backgroundColor;

	/** The bold. */
	private final boolean bold;

	/** The underline. */
	private final boolean underline;

	/** The reverse. */
	private final boolean reverse;

	/** The blinking. */
	private final boolean blinking;

	/**
	 * Instantiates a new screen character.
	 * 
	 * @param character
	 *            the character
	 */
	ScreenCharacter(char character)
	{
		this(character, Terminal.Color.DEFAULT, Terminal.Color.DEFAULT);
	}

	/**
	 * Instantiates a new screen character.
	 * 
	 * @param character
	 *            the character
	 * @param foregroundColor
	 *            the foreground color
	 * @param backgroundColor
	 *            the background color
	 */
	ScreenCharacter(char character, Terminal.Color foregroundColor, Terminal.Color backgroundColor)
	{
		this(character, foregroundColor, backgroundColor, EnumSet.noneOf(ScreenCharacterStyle.class));
	}

	/**
	 * Instantiates a new screen character.
	 * 
	 * @param character
	 *            the character
	 * @param foregroundColor
	 *            the foreground color
	 * @param backgroundColor
	 *            the background color
	 * @param style
	 *            the style
	 */
	ScreenCharacter(char character, Terminal.Color foregroundColor, Terminal.Color backgroundColor, Set<ScreenCharacterStyle> style)
	{
		if (foregroundColor == null)
		{
			foregroundColor = Terminal.Color.DEFAULT;
		}
		if (backgroundColor == null)
		{
			backgroundColor = Terminal.Color.DEFAULT;
		}

		this.character = character;
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		this.bold = style.contains(ScreenCharacterStyle.BOLD);
		this.underline = style.contains(ScreenCharacterStyle.UNDERLINE);
		this.reverse = style.contains(ScreenCharacterStyle.REVERSE);
		this.blinking = style.contains(ScreenCharacterStyle.BLINK);
	}

	/**
	 * Instantiates a new screen character.
	 * 
	 * @param character
	 *            the character
	 */
	ScreenCharacter(final ScreenCharacter character)
	{
		this.character = character.character;
		this.foregroundColor = character.foregroundColor;
		this.backgroundColor = character.backgroundColor;
		this.bold = character.bold;
		this.underline = character.underline;
		this.reverse = character.reverse;
		this.blinking = character.blinking;
	}

	/**
	 * Gets the character.
	 * 
	 * @return the character
	 */
	char getCharacter()
	{
		return character;
	}

	/**
	 * Gets the background color.
	 * 
	 * @return the background color
	 */
	Terminal.Color getBackgroundColor()
	{
		return backgroundColor;
	}

	/**
	 * Checks if is bold.
	 * 
	 * @return true, if is bold
	 */
	boolean isBold()
	{
		return bold;
	}

	/**
	 * Gets the foreground color.
	 * 
	 * @return the foreground color
	 */
	Terminal.Color getForegroundColor()
	{
		return foregroundColor;
	}

	/**
	 * Checks if is negative.
	 * 
	 * @return true, if is negative
	 */
	boolean isNegative()
	{
		return reverse;
	}

	/**
	 * Checks if is underline.
	 * 
	 * @return true, if is underline
	 */
	boolean isUnderline()
	{
		return underline;
	}

	/**
	 * Checks if is blinking.
	 * 
	 * @return true, if is blinking
	 */
	boolean isBlinking()
	{
		return blinking;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof ScreenCharacter == false)
		{
			return false;
		}

		ScreenCharacter other = (ScreenCharacter) obj;
		return character == other.getCharacter() && getForegroundColor() == other.getForegroundColor() && getBackgroundColor() == other.getBackgroundColor()
				&& isBold() == other.isBold() && isNegative() == other.isNegative() && isUnderline() == other.isUnderline() && isBlinking() == other.isBlinking();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 71 * hash + this.character;
		return hash;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return Character.toString(character);
	}
}
