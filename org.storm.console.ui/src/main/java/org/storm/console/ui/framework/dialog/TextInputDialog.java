/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.dialog;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Border;
import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.GUIScreen;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Button;
import org.storm.console.ui.framework.component.EmptySpace;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;
import org.storm.console.ui.framework.component.PasswordBox;
import org.storm.console.ui.framework.component.TextBox;
import org.storm.console.ui.framework.layout.LinearLayout;

/**
 * The Class TextInputDialog.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TextInputDialog extends Window
{

	/** The text box. */
	private final TextBox textBox;

	/** The result. */
	private String result;

	/**
	 * Instantiates a new text input dialog.
	 * 
	 * @param textBoxFactory
	 *            the text box factory
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param initialText
	 *            the initial text
	 */
	private TextInputDialog(final TextBoxFactory textBoxFactory, final String title, final String description, final String initialText)
	{
		this(textBoxFactory, title, description, initialText, 0);
	}

	/**
	 * Instantiates a new text input dialog.
	 * 
	 * @param textBoxFactory
	 *            the text box factory
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param initialText
	 *            the initial text
	 * @param textBoxWidth
	 *            the text box width
	 */
	private TextInputDialog(final TextBoxFactory textBoxFactory, final String title, final String description, final String initialText, int textBoxWidth)
	{
		super(title);
		Label descriptionLabel = new Label(description);
		if (textBoxWidth == 0)
		{
			textBoxWidth = Math.max(descriptionLabel.getPreferredSize().getColumns(), title.length());
		}

		textBox = textBoxFactory.createTextBox(initialText, textBoxWidth);
		addComponent(descriptionLabel);
		addComponent(new EmptySpace(1, 1));
		addComponent(textBox);

		addComponent(new EmptySpace(1, 1));
		Panel okCancelPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORIZONTAL);
		Button okButton = new Button("OK", new Action()
		{
			@Override
			public void doAction()
			{
				result = textBox.getText();
				close();
			}
		});
		okButton.setAlignment(Component.Alignment.RIGHT_CENTER);
		okCancelPanel.addComponent(okButton, LinearLayout.GROWS_HORIZONTALLY);
		Button cancelButton = new Button("Cancel", new Action()
		{
			@Override
			public void doAction()
			{
				close();
			}
		});
		okCancelPanel.addComponent(cancelButton);
		addComponent(okCancelPanel, LinearLayout.GROWS_HORIZONTALLY);
	}

	/**
	 * Show text input box.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param initialText
	 *            the initial text
	 * @return the string
	 */
	public static String showTextInputBox(final GUIScreen owner, final String title, final String description, final String initialText)
	{
		return showTextInputBox(owner, title, description, initialText, 0);
	}

	/**
	 * Show text input box.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param initialText
	 *            the initial text
	 * @param textBoxWidth
	 *            the text box width
	 * @return the string
	 */
	public static String showTextInputBox(final GUIScreen owner, final String title, final String description, final String initialText, final int textBoxWidth)
	{
		final TextInputDialog textInputBox = new TextInputDialog(new NormalTextBoxFactory(), title, description, initialText, textBoxWidth);
		owner.showWindow(textInputBox, GUIScreen.Position.CENTER);
		return textInputBox.result;
	}

	/**
	 * Show password input box.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param initialText
	 *            the initial text
	 * @return the string
	 */
	public static String showPasswordInputBox(final GUIScreen owner, final String title, final String description, final String initialText)
	{
		return showPasswordInputBox(owner, title, description, initialText, 0);
	}

	/**
	 * Show password input box.
	 * 
	 * @param owner
	 *            the owner
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 * @param initialText
	 *            the initial text
	 * @param textBoxWidth
	 *            the text box width
	 * @return the string
	 */
	public static String showPasswordInputBox(final GUIScreen owner, final String title, final String description, final String initialText, final int textBoxWidth)
	{
		TextInputDialog textInputBox = new TextInputDialog(new PasswordTextBoxFactory(), title, description, initialText, textBoxWidth);
		owner.showWindow(textInputBox, GUIScreen.Position.CENTER);
		return textInputBox.result;
	}

	/**
	 * A factory for creating TextBox objects.
	 */
	private static interface TextBoxFactory
	{

		/**
		 * Creates a new TextBox object.
		 * 
		 * @param initialContent
		 *            the initial content
		 * @param width
		 *            the width
		 * @return the text box
		 */
		public TextBox createTextBox(String initialContent, int width);
	}

	/**
	 * A factory for creating NormalTextBox objects.
	 */
	private static class NormalTextBoxFactory implements TextBoxFactory
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.dialog.TextInputDialog.TextBoxFactory
		 * #createTextBox(java.lang.String, int)
		 */
		@Override
		public TextBox createTextBox(String initialContent, int width)
		{
			return new TextBox(initialContent, width);
		}
	}

	/**
	 * A factory for creating PasswordTextBox objects.
	 */
	private static class PasswordTextBoxFactory implements TextBoxFactory
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.dialog.TextInputDialog.TextBoxFactory
		 * #createTextBox(java.lang.String, int)
		 */
		@Override
		public TextBox createTextBox(String initialContent, int width)
		{
			return new PasswordBox(initialContent, width);
		}
	}
}
