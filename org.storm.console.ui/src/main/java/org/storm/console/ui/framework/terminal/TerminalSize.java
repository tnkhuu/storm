/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

/**
 * The Class TerminalSize.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TerminalSize
{

	/** The columns. */
	private int columns;

	/** The rows. */
	private int rows;

	/**
	 * Instantiates a new terminal size.
	 * 
	 * @param columns
	 *            the columns
	 * @param rows
	 *            the rows
	 */
	public TerminalSize(int columns, int rows)
	{
		setColumns(columns);
		setRows(rows);
	}

	/**
	 * Instantiates a new terminal size.
	 * 
	 * @param terminalSize
	 *            the terminal size
	 */
	public TerminalSize(TerminalSize terminalSize)
	{
		this(terminalSize.getColumns(), terminalSize.getRows());
	}

	/**
	 * Gets the columns.
	 * 
	 * @return the columns
	 */
	public int getColumns()
	{
		return columns;
	}

	/**
	 * Sets the columns.
	 * 
	 * @param columns
	 *            the new columns
	 */
	public void setColumns(int columns)
	{
		if (columns < 0)
		{
			throw new IllegalArgumentException("TerminalSize.columns cannot be less than 0!");
		}

		this.columns = columns;
	}

	/**
	 * Gets the rows.
	 * 
	 * @return the rows
	 */
	public int getRows()
	{
		return rows;
	}

	/**
	 * Sets the rows.
	 * 
	 * @param rows
	 *            the new rows
	 */
	public void setRows(int rows)
	{
		if (rows < 0)
		{
			throw new IllegalArgumentException("TerminalSize.rows cannot be less than 0!");
		}

		this.rows = rows;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "{" + columns + "x" + rows + "}";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof TerminalSize == false)
		{
			return false;
		}

		TerminalSize other = (TerminalSize) obj;
		return columns == other.columns && rows == other.rows;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 53 * hash + this.columns;
		hash = 53 * hash + this.rows;
		return hash;
	}
}
