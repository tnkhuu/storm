/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal.swing;

import java.awt.Color;

/**
 * The Class TerminalPalette.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TerminalPalette
{

	/** The Constant GNOME_TERMINAL. */
	public static final TerminalPalette GNOME_TERMINAL = new TerminalPalette(new java.awt.Color(211, 215, 207), new java.awt.Color(238, 238, 236), new java.awt.Color(46, 52, 54),
			new java.awt.Color(85, 87, 83), new java.awt.Color(204, 0, 0), new java.awt.Color(239, 41, 41), new java.awt.Color(78, 154, 6), new java.awt.Color(138, 226, 52),
			new java.awt.Color(196, 160, 0), new java.awt.Color(252, 233, 79), new java.awt.Color(52, 101, 164), new java.awt.Color(114, 159, 207),
			new java.awt.Color(117, 80, 123), new java.awt.Color(173, 127, 168), new java.awt.Color(6, 152, 154), new java.awt.Color(52, 226, 226), new java.awt.Color(211, 215,
					207), new java.awt.Color(238, 238, 236));

	/** The Constant STANDARD_VGA. */
	public static final TerminalPalette STANDARD_VGA = new TerminalPalette(new java.awt.Color(170, 170, 170), new java.awt.Color(255, 255, 255), new java.awt.Color(0, 0, 0),
			new java.awt.Color(85, 85, 85), new java.awt.Color(170, 0, 0), new java.awt.Color(255, 85, 85), new java.awt.Color(0, 170, 0), new java.awt.Color(85, 255, 85),
			new java.awt.Color(170, 85, 0), new java.awt.Color(255, 255, 85), new java.awt.Color(0, 0, 170), new java.awt.Color(85, 85, 255), new java.awt.Color(170, 0, 170),
			new java.awt.Color(255, 85, 255), new java.awt.Color(0, 170, 170), new java.awt.Color(85, 255, 255), new java.awt.Color(170, 170, 170), new java.awt.Color(255, 255,
					255));

	/** The Constant WINDOWS_XP_COMMAND_PROMPT. */
	public static final TerminalPalette WINDOWS_XP_COMMAND_PROMPT = new TerminalPalette(new java.awt.Color(192, 192, 192), new java.awt.Color(255, 255, 255), new java.awt.Color(0,
			0, 0), new java.awt.Color(128, 128, 128), new java.awt.Color(128, 0, 0), new java.awt.Color(255, 0, 0), new java.awt.Color(0, 128, 0), new java.awt.Color(0, 255, 0),
			new java.awt.Color(128, 128, 0), new java.awt.Color(255, 255, 0), new java.awt.Color(0, 0, 128), new java.awt.Color(0, 0, 255), new java.awt.Color(128, 0, 128),
			new java.awt.Color(255, 0, 255), new java.awt.Color(0, 128, 128), new java.awt.Color(0, 255, 255), new java.awt.Color(192, 192, 192), new java.awt.Color(255, 255, 255));

	/** The Constant MAC_OS_X_TERMINAL_APP. */
	public static final TerminalPalette MAC_OS_X_TERMINAL_APP = new TerminalPalette(new java.awt.Color(203, 204, 205), new java.awt.Color(233, 235, 235), new java.awt.Color(0, 0,
			0), new java.awt.Color(129, 131, 131), new java.awt.Color(194, 54, 33), new java.awt.Color(252, 57, 31), new java.awt.Color(37, 188, 36), new java.awt.Color(49, 231,
			34), new java.awt.Color(173, 173, 39), new java.awt.Color(234, 236, 35), new java.awt.Color(73, 46, 225), new java.awt.Color(88, 51, 255), new java.awt.Color(211, 56,
			211), new java.awt.Color(249, 53, 248), new java.awt.Color(51, 187, 200), new java.awt.Color(20, 240, 240), new java.awt.Color(203, 204, 205), new java.awt.Color(233,
			235, 235));

	/** The Constant PUTTY. */
	public static final TerminalPalette PUTTY = new TerminalPalette(new java.awt.Color(187, 187, 187), new java.awt.Color(255, 255, 255), new java.awt.Color(0, 0, 0),
			new java.awt.Color(85, 85, 85), new java.awt.Color(187, 0, 0), new java.awt.Color(255, 85, 85), new java.awt.Color(0, 187, 0), new java.awt.Color(85, 255, 85),
			new java.awt.Color(187, 187, 0), new java.awt.Color(255, 255, 85), new java.awt.Color(0, 0, 187), new java.awt.Color(85, 85, 255), new java.awt.Color(187, 0, 187),
			new java.awt.Color(255, 85, 255), new java.awt.Color(0, 187, 187), new java.awt.Color(85, 255, 255), new java.awt.Color(187, 187, 187), new java.awt.Color(255, 255,
					255));

	/** The Constant XTERM. */
	public static final TerminalPalette XTERM = new TerminalPalette(new java.awt.Color(229, 229, 229), new java.awt.Color(255, 255, 255), new java.awt.Color(0, 0, 0),
			new java.awt.Color(127, 127, 127), new java.awt.Color(205, 0, 0), new java.awt.Color(255, 0, 0), new java.awt.Color(0, 205, 0), new java.awt.Color(0, 255, 0),
			new java.awt.Color(205, 205, 0), new java.awt.Color(255, 255, 0), new java.awt.Color(0, 0, 238), new java.awt.Color(92, 92, 255), new java.awt.Color(205, 0, 205),
			new java.awt.Color(255, 0, 255), new java.awt.Color(0, 205, 205), new java.awt.Color(0, 255, 255), new java.awt.Color(229, 229, 229), new java.awt.Color(255, 255, 255));

	/** The Constant DEFAULT. */
	public static final TerminalPalette DEFAULT = GNOME_TERMINAL;

	/** The default color. */
	private final Color defaultColor;

	/** The default bright color. */
	private final Color defaultBrightColor;

	/** The normal black. */
	private final Color normalBlack;

	/** The bright black. */
	private final Color brightBlack;

	/** The normal red. */
	private final Color normalRed;

	/** The bright red. */
	private final Color brightRed;

	/** The normal green. */
	private final Color normalGreen;

	/** The bright green. */
	private final Color brightGreen;

	/** The normal yellow. */
	private final Color normalYellow;

	/** The bright yellow. */
	private final Color brightYellow;

	/** The normal blue. */
	private final Color normalBlue;

	/** The bright blue. */
	private final Color brightBlue;

	/** The normal magenta. */
	private final Color normalMagenta;

	/** The bright magenta. */
	private final Color brightMagenta;

	/** The normal cyan. */
	private final Color normalCyan;

	/** The bright cyan. */
	private final Color brightCyan;

	/** The normal white. */
	private final Color normalWhite;

	/** The bright white. */
	private final Color brightWhite;

	/**
	 * Instantiates a new terminal palette.
	 * 
	 * @param defaultColor
	 *            the default color
	 * @param defaultBrightColor
	 *            the default bright color
	 * @param normalBlack
	 *            the normal black
	 * @param brightBlack
	 *            the bright black
	 * @param normalRed
	 *            the normal red
	 * @param brightRed
	 *            the bright red
	 * @param normalGreen
	 *            the normal green
	 * @param brightGreen
	 *            the bright green
	 * @param normalYellow
	 *            the normal yellow
	 * @param brightYellow
	 *            the bright yellow
	 * @param normalBlue
	 *            the normal blue
	 * @param brightBlue
	 *            the bright blue
	 * @param normalMagenta
	 *            the normal magenta
	 * @param brightMagenta
	 *            the bright magenta
	 * @param normalCyan
	 *            the normal cyan
	 * @param brightCyan
	 *            the bright cyan
	 * @param normalWhite
	 *            the normal white
	 * @param brightWhite
	 *            the bright white
	 */
	public TerminalPalette(Color defaultColor, Color defaultBrightColor, Color normalBlack, Color brightBlack, Color normalRed, Color brightRed, Color normalGreen,
			Color brightGreen, Color normalYellow, Color brightYellow, Color normalBlue, Color brightBlue, Color normalMagenta, Color brightMagenta, Color normalCyan,
			Color brightCyan, Color normalWhite, Color brightWhite)
	{
		this.defaultColor = defaultColor;
		this.defaultBrightColor = defaultBrightColor;
		this.normalBlack = normalBlack;
		this.brightBlack = brightBlack;
		this.normalRed = normalRed;
		this.brightRed = brightRed;
		this.normalGreen = normalGreen;
		this.brightGreen = brightGreen;
		this.normalYellow = normalYellow;
		this.brightYellow = brightYellow;
		this.normalBlue = normalBlue;
		this.brightBlue = brightBlue;
		this.normalMagenta = normalMagenta;
		this.brightMagenta = brightMagenta;
		this.normalCyan = normalCyan;
		this.brightCyan = brightCyan;
		this.normalWhite = normalWhite;
		this.brightWhite = brightWhite;
	}

	/**
	 * Gets the bright black.
	 * 
	 * @return the bright black
	 */
	public Color getBrightBlack()
	{
		return brightBlack;
	}

	/**
	 * Gets the bright blue.
	 * 
	 * @return the bright blue
	 */
	public Color getBrightBlue()
	{
		return brightBlue;
	}

	/**
	 * Gets the bright cyan.
	 * 
	 * @return the bright cyan
	 */
	public Color getBrightCyan()
	{
		return brightCyan;
	}

	/**
	 * Gets the bright green.
	 * 
	 * @return the bright green
	 */
	public Color getBrightGreen()
	{
		return brightGreen;
	}

	/**
	 * Gets the bright magenta.
	 * 
	 * @return the bright magenta
	 */
	public Color getBrightMagenta()
	{
		return brightMagenta;
	}

	/**
	 * Gets the bright red.
	 * 
	 * @return the bright red
	 */
	public Color getBrightRed()
	{
		return brightRed;
	}

	/**
	 * Gets the bright white.
	 * 
	 * @return the bright white
	 */
	public Color getBrightWhite()
	{
		return brightWhite;
	}

	/**
	 * Gets the bright yellow.
	 * 
	 * @return the bright yellow
	 */
	public Color getBrightYellow()
	{
		return brightYellow;
	}

	/**
	 * Gets the default bright color.
	 * 
	 * @return the default bright color
	 */
	public Color getDefaultBrightColor()
	{
		return defaultBrightColor;
	}

	/**
	 * Gets the default color.
	 * 
	 * @return the default color
	 */
	public Color getDefaultColor()
	{
		return defaultColor;
	}

	/**
	 * Gets the normal black.
	 * 
	 * @return the normal black
	 */
	public Color getNormalBlack()
	{
		return normalBlack;
	}

	/**
	 * Gets the normal blue.
	 * 
	 * @return the normal blue
	 */
	public Color getNormalBlue()
	{
		return normalBlue;
	}

	/**
	 * Gets the normal cyan.
	 * 
	 * @return the normal cyan
	 */
	public Color getNormalCyan()
	{
		return normalCyan;
	}

	/**
	 * Gets the normal green.
	 * 
	 * @return the normal green
	 */
	public Color getNormalGreen()
	{
		return normalGreen;
	}

	/**
	 * Gets the normal magenta.
	 * 
	 * @return the normal magenta
	 */
	public Color getNormalMagenta()
	{
		return normalMagenta;
	}

	/**
	 * Gets the normal red.
	 * 
	 * @return the normal red
	 */
	public Color getNormalRed()
	{
		return normalRed;
	}

	/**
	 * Gets the normal white.
	 * 
	 * @return the normal white
	 */
	public Color getNormalWhite()
	{
		return normalWhite;
	}

	/**
	 * Gets the normal yellow.
	 * 
	 * @return the normal yellow
	 */
	public Color getNormalYellow()
	{
		return normalYellow;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final TerminalPalette other = (TerminalPalette) obj;
		if (this.defaultColor != other.defaultColor && (this.defaultColor == null || !this.defaultColor.equals(other.defaultColor)))
		{
			return false;
		}
		if (this.defaultBrightColor != other.defaultBrightColor && (this.defaultBrightColor == null || !this.defaultBrightColor.equals(other.defaultBrightColor)))
		{
			return false;
		}
		if (this.normalBlack != other.normalBlack && (this.normalBlack == null || !this.normalBlack.equals(other.normalBlack)))
		{
			return false;
		}
		if (this.brightBlack != other.brightBlack && (this.brightBlack == null || !this.brightBlack.equals(other.brightBlack)))
		{
			return false;
		}
		if (this.normalRed != other.normalRed && (this.normalRed == null || !this.normalRed.equals(other.normalRed)))
		{
			return false;
		}
		if (this.brightRed != other.brightRed && (this.brightRed == null || !this.brightRed.equals(other.brightRed)))
		{
			return false;
		}
		if (this.normalGreen != other.normalGreen && (this.normalGreen == null || !this.normalGreen.equals(other.normalGreen)))
		{
			return false;
		}
		if (this.brightGreen != other.brightGreen && (this.brightGreen == null || !this.brightGreen.equals(other.brightGreen)))
		{
			return false;
		}
		if (this.normalYellow != other.normalYellow && (this.normalYellow == null || !this.normalYellow.equals(other.normalYellow)))
		{
			return false;
		}
		if (this.brightYellow != other.brightYellow && (this.brightYellow == null || !this.brightYellow.equals(other.brightYellow)))
		{
			return false;
		}
		if (this.normalBlue != other.normalBlue && (this.normalBlue == null || !this.normalBlue.equals(other.normalBlue)))
		{
			return false;
		}
		if (this.brightBlue != other.brightBlue && (this.brightBlue == null || !this.brightBlue.equals(other.brightBlue)))
		{
			return false;
		}
		if (this.normalMagenta != other.normalMagenta && (this.normalMagenta == null || !this.normalMagenta.equals(other.normalMagenta)))
		{
			return false;
		}
		if (this.brightMagenta != other.brightMagenta && (this.brightMagenta == null || !this.brightMagenta.equals(other.brightMagenta)))
		{
			return false;
		}
		if (this.normalCyan != other.normalCyan && (this.normalCyan == null || !this.normalCyan.equals(other.normalCyan)))
		{
			return false;
		}
		if (this.brightCyan != other.brightCyan && (this.brightCyan == null || !this.brightCyan.equals(other.brightCyan)))
		{
			return false;
		}
		if (this.normalWhite != other.normalWhite && (this.normalWhite == null || !this.normalWhite.equals(other.normalWhite)))
		{
			return false;
		}
		if (this.brightWhite != other.brightWhite && (this.brightWhite == null || !this.brightWhite.equals(other.brightWhite)))
		{
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 43 * hash + (this.defaultColor != null ? this.defaultColor.hashCode() : 0);
		hash = 43 * hash + (this.defaultBrightColor != null ? this.defaultBrightColor.hashCode() : 0);
		hash = 43 * hash + (this.normalBlack != null ? this.normalBlack.hashCode() : 0);
		hash = 43 * hash + (this.brightBlack != null ? this.brightBlack.hashCode() : 0);
		hash = 43 * hash + (this.normalRed != null ? this.normalRed.hashCode() : 0);
		hash = 43 * hash + (this.brightRed != null ? this.brightRed.hashCode() : 0);
		hash = 43 * hash + (this.normalGreen != null ? this.normalGreen.hashCode() : 0);
		hash = 43 * hash + (this.brightGreen != null ? this.brightGreen.hashCode() : 0);
		hash = 43 * hash + (this.normalYellow != null ? this.normalYellow.hashCode() : 0);
		hash = 43 * hash + (this.brightYellow != null ? this.brightYellow.hashCode() : 0);
		hash = 43 * hash + (this.normalBlue != null ? this.normalBlue.hashCode() : 0);
		hash = 43 * hash + (this.brightBlue != null ? this.brightBlue.hashCode() : 0);
		hash = 43 * hash + (this.normalMagenta != null ? this.normalMagenta.hashCode() : 0);
		hash = 43 * hash + (this.brightMagenta != null ? this.brightMagenta.hashCode() : 0);
		hash = 43 * hash + (this.normalCyan != null ? this.normalCyan.hashCode() : 0);
		hash = 43 * hash + (this.brightCyan != null ? this.brightCyan.hashCode() : 0);
		hash = 43 * hash + (this.normalWhite != null ? this.normalWhite.hashCode() : 0);
		hash = 43 * hash + (this.brightWhite != null ? this.brightWhite.hashCode() : 0);
		return hash;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "TerminalPalette{" + "defaultColor=" + defaultColor + ", defaultBrightColor=" + defaultBrightColor + ", normalBlack=" + normalBlack + ", brightBlack=" + brightBlack
				+ ", normalRed=" + normalRed + ", brightRed=" + brightRed + ", normalGreen=" + normalGreen + ", brightGreen=" + brightGreen + ", normalYellow=" + normalYellow
				+ ", brightYellow=" + brightYellow + ", normalBlue=" + normalBlue + ", brightBlue=" + brightBlue + ", normalMagenta=" + normalMagenta + ", brightMagenta="
				+ brightMagenta + ", normalCyan=" + normalCyan + ", brightCyan=" + brightCyan + ", normalWhite=" + normalWhite + ", brightWhite=" + brightWhite + '}';
	}
}
