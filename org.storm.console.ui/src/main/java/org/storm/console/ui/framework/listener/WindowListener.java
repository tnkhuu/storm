/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.listener;

import java.awt.event.WindowEvent;

import org.storm.console.ui.framework.Interactable;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.input.Key;

/**
 * The listener interface for receiving window events. The class that is
 * interested in processing a window event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addWindowListener<code> method. When
 * the window event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see WindowEvent
 */
public interface WindowListener
{

	/**
	 * On window invalidated.
	 * 
	 * @param window
	 *            the window
	 */
	void onWindowInvalidated(Window window);

	/**
	 * On window shown.
	 * 
	 * @param window
	 *            the window
	 */
	void onWindowShown(Window window);

	/**
	 * On window closed.
	 * 
	 * @param window
	 *            the window
	 */
	void onWindowClosed(Window window);

	/**
	 * On unhandled keyboard interaction.
	 * 
	 * @param window
	 *            the window
	 * @param key
	 *            the key
	 */
	void onUnhandledKeyboardInteraction(Window window, Key key);

	/**
	 * On focus changed.
	 * 
	 * @param window
	 *            the window
	 * @param fromComponent
	 *            the from component
	 * @param toComponent
	 *            the to component
	 */
	void onFocusChanged(Window window, Interactable fromComponent, Interactable toComponent);
}
