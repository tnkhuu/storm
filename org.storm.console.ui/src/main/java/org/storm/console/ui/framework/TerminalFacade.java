/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework;

import java.awt.GraphicsEnvironment;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.swing.SwingTerminal;
import org.storm.console.ui.framework.terminal.swing.TerminalAppearance;
import org.storm.console.ui.framework.terminal.text.CygwinTerminal;
import org.storm.console.ui.framework.terminal.text.UnixTerminal;

/**
 * The Class TerminalFacade.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TerminalFacade
{

	/**
	 * Instantiates a new terminal facade.
	 */
	private TerminalFacade()
	{
	}

	/** The Constant DEFAULT_CHARSET. */
	private static final Charset DEFAULT_CHARSET = Charset.forName(System.getProperty("file.encoding"));

	/** The Constant DEFAULT_TERMINAL_COLUMN_WIDTH. */
	private static final int DEFAULT_TERMINAL_COLUMN_WIDTH = 100;

	/** The Constant DEFAULT_TERMINAL_COLUMN_HEIGHT. */
	private static final int DEFAULT_TERMINAL_COLUMN_HEIGHT = 30;

	/**
	 * Creates the terminal.
	 * 
	 * @return the terminal
	 */
	public static Terminal createTerminal()
	{
		return createTerminal(DEFAULT_CHARSET);
	}

	/**
	 * Creates the terminal.
	 * 
	 * @param terminalCharset
	 *            the terminal charset
	 * @return the terminal
	 */
	public static Terminal createTerminal(Charset terminalCharset)
	{
		return createTerminal(System.in, System.out, terminalCharset);
	}

	/**
	 * Creates the terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @return the terminal
	 */
	public static Terminal createTerminal(InputStream terminalInput, OutputStream terminalOutput)
	{
		return createTerminal(terminalInput, terminalOutput, DEFAULT_CHARSET);
	}

	/**
	 * Creates the terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 * @return the terminal
	 */
	public static Terminal createTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset)
	{
		if (GraphicsEnvironment.isHeadless())
		{
			return createTextTerminal(terminalInput, terminalOutput, terminalCharset);
		}
		else
		{
			return createSwingTerminal();
		}
	}

	/**
	 * Creates the swing terminal.
	 * 
	 * @return the swing terminal
	 */
	public static SwingTerminal createSwingTerminal()
	{
		return createSwingTerminal(DEFAULT_TERMINAL_COLUMN_WIDTH, DEFAULT_TERMINAL_COLUMN_HEIGHT);
	}

	/**
	 * Creates the swing terminal.
	 * 
	 * @param columns
	 *            the columns
	 * @param rows
	 *            the rows
	 * @return the swing terminal
	 */
	public static SwingTerminal createSwingTerminal(int columns, int rows)
	{
		return new SwingTerminal(columns, rows);
	}

	/**
	 * Creates the swing terminal.
	 * 
	 * @param appearance
	 *            the appearance
	 * @return the swing terminal
	 */
	public static SwingTerminal createSwingTerminal(TerminalAppearance appearance)
	{
		return createSwingTerminal(appearance, DEFAULT_TERMINAL_COLUMN_WIDTH, DEFAULT_TERMINAL_COLUMN_HEIGHT);
	}

	/**
	 * Creates the swing terminal.
	 * 
	 * @param appearance
	 *            the appearance
	 * @param columns
	 *            the columns
	 * @param rows
	 *            the rows
	 * @return the swing terminal
	 */
	public static SwingTerminal createSwingTerminal(TerminalAppearance appearance, int columns, int rows)
	{
		return new SwingTerminal(appearance, columns, rows);
	}

	/**
	 * Creates the unix terminal.
	 * 
	 * @return the unix terminal
	 */
	public static UnixTerminal createUnixTerminal()
	{
		return createUnixTerminal(DEFAULT_CHARSET);
	}

	/**
	 * Creates the unix terminal.
	 * 
	 * @param terminalCharset
	 *            the terminal charset
	 * @return the unix terminal
	 */
	public static UnixTerminal createUnixTerminal(Charset terminalCharset)
	{
		return createUnixTerminal(System.in, System.out, terminalCharset);
	}

	/**
	 * Creates the unix terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @return the unix terminal
	 */
	public static UnixTerminal createUnixTerminal(InputStream terminalInput, OutputStream terminalOutput)
	{
		return createUnixTerminal(terminalInput, terminalOutput, DEFAULT_CHARSET);
	}

	/**
	 * Creates the unix terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 * @return the unix terminal
	 */
	public static UnixTerminal createUnixTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset)
	{
		return new UnixTerminal(terminalInput, terminalOutput, terminalCharset);
	}

	/**
	 * Creates the cygwin terminal.
	 * 
	 * @return the cygwin terminal
	 */
	public static CygwinTerminal createCygwinTerminal()
	{
		return createCygwinTerminal(DEFAULT_CHARSET);
	}

	/**
	 * Creates the cygwin terminal.
	 * 
	 * @param terminalCharset
	 *            the terminal charset
	 * @return the cygwin terminal
	 */
	public static CygwinTerminal createCygwinTerminal(Charset terminalCharset)
	{
		return createCygwinTerminal(System.in, System.out, terminalCharset);
	}

	/**
	 * Creates the cygwin terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @return the cygwin terminal
	 */
	public static CygwinTerminal createCygwinTerminal(InputStream terminalInput, OutputStream terminalOutput)
	{
		return createCygwinTerminal(terminalInput, terminalOutput, DEFAULT_CHARSET);
	}

	/**
	 * Creates the cygwin terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 * @return the cygwin terminal
	 */
	public static CygwinTerminal createCygwinTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset)
	{
		return new CygwinTerminal(terminalInput, terminalOutput, terminalCharset);
	}

	/*
	 * Creates a suitable text terminal for the running environment.
	 */
	/**
	 * Creates the text terminal.
	 * 
	 * @return the terminal
	 */
	public static Terminal createTextTerminal()
	{
		return createTextTerminal(System.in, System.out, DEFAULT_CHARSET);
	}

	/*
	 * Creates a suitable text terminal for the running environment.
	 */
	/**
	 * Creates the text terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 * @return the terminal
	 */
	public static Terminal createTextTerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset)
	{
		if (isOperatingSystemWindows())
		{
			return createCygwinTerminal(terminalInput, terminalOutput, terminalCharset);
		}
		else
		{
			return createUnixTerminal(terminalInput, terminalOutput, terminalCharset);
		}
	}

	/**
	 * Creates the screen.
	 * 
	 * @return the screen
	 */
	public static Screen createScreen()
	{
		return createScreen(createTerminal());
	}

	/**
	 * Creates the screen.
	 * 
	 * @param terminal
	 *            the terminal
	 * @return the screen
	 */
	public static Screen createScreen(Terminal terminal)
	{
		return new Screen(terminal);
	}

	/**
	 * Creates the gui screen.
	 * 
	 * @return the gUI screen
	 */
	public static GUIScreen createGUIScreen()
	{
		return new GUIScreen(createScreen());
	}

	/**
	 * Creates the gui screen.
	 * 
	 * @param terminal
	 *            the terminal
	 * @return the gUI screen
	 */
	public static GUIScreen createGUIScreen(Terminal terminal)
	{
		return new GUIScreen(createScreen(terminal));
	}

	/**
	 * Creates the gui screen.
	 * 
	 * @param screen
	 *            the screen
	 * @return the gUI screen
	 */
	public static GUIScreen createGUIScreen(Screen screen)
	{
		return new GUIScreen(screen);
	}

	/**
	 * Checks if is operating system windows.
	 * 
	 * @return true, if is operating system windows
	 */
	public static boolean isOperatingSystemWindows()
	{
		return System.getProperty("os.name", "").toLowerCase().startsWith("windows");
	}
}
