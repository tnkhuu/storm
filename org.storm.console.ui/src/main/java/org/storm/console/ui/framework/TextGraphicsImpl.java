/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework;

import java.util.Arrays;
import java.util.EnumSet;

import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.screen.ScreenCharacterStyle;
import org.storm.console.ui.framework.screen.TabBehaviour;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;
import org.storm.console.ui.framework.terminal.Terminal.Color;

/**
 * The Class TextGraphicsImpl.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class TextGraphicsImpl implements TextGraphics
{

	/** The top left. */
	private final TerminalPosition topLeft;

	/** The area size. */
	private final TerminalSize areaSize;

	/** The screen. */
	private final Screen screen;

	/** The theme. */
	private Theme theme;

	/** The foreground color. */
	private Color foregroundColor;

	/** The background color. */
	private Color backgroundColor;

	/** The currently bold. */
	private boolean currentlyBold;

	/**
	 * Instantiates a new text graphics impl.
	 * 
	 * @param topLeft
	 *            the top left
	 * @param areaSize
	 *            the area size
	 * @param screen
	 *            the screen
	 * @param theme
	 *            the theme
	 */
	TextGraphicsImpl(final TerminalPosition topLeft, final TerminalSize areaSize, final Screen screen, final Theme theme)
	{
		this.topLeft = topLeft;
		this.areaSize = areaSize;
		this.screen = screen;
		this.theme = theme;
		this.currentlyBold = false;
		this.foregroundColor = Color.DEFAULT;
		this.backgroundColor = Color.DEFAULT;
	}

	/**
	 * Instantiates a new text graphics impl.
	 * 
	 * @param graphics
	 *            the graphics
	 * @param topLeft
	 *            the top left
	 * @param areaSize
	 *            the area size
	 */
	private TextGraphicsImpl(final TextGraphicsImpl graphics, final TerminalPosition topLeft, final TerminalSize areaSize)
	{
		this(new TerminalPosition(topLeft.getY() + graphics.topLeft.getY(), topLeft.getX() + graphics.topLeft.getX()), new TerminalSize(
				areaSize.getColumns() < graphics.getWidth() - topLeft.getY() ? areaSize.getColumns() : graphics.getWidth() - topLeft.getY(),
				areaSize.getRows() < graphics.getHeight() - topLeft.getX() ? areaSize.getRows() : graphics.getHeight() - topLeft.getX()), graphics.screen, graphics.theme);
		foregroundColor = graphics.foregroundColor;
		backgroundColor = graphics.backgroundColor;
		currentlyBold = graphics.currentlyBold;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.TextGraphics#subAreaGraphics(com.googlecode
	 * .lanterna.terminal.TerminalPosition)
	 */
	@Override
	public TextGraphics subAreaGraphics(final TerminalPosition terminalPosition)
	{
		terminalPosition.ensurePositivePosition();
		TerminalSize newArea = new TerminalSize(areaSize);
		newArea.setColumns(newArea.getColumns() - terminalPosition.getY());
		newArea.setRows(newArea.getRows() - terminalPosition.getX());
		return subAreaGraphics(terminalPosition, newArea);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.TextGraphics#subAreaGraphics(com.googlecode
	 * .lanterna.terminal.TerminalPosition,
	 * org.storm.console.ui.framework.terminal.TerminalSize)
	 */
	@Override
	public TextGraphics subAreaGraphics(final TerminalPosition topLeft, final TerminalSize subAreaSize)
	{
		if (topLeft.getY() < 0)
		{
			topLeft.setY(0);
		}
		if (topLeft.getX() < 0)
		{
			topLeft.setX(0);
		}
		if (subAreaSize.getColumns() < 0)
		{
			subAreaSize.setColumns(-subAreaSize.getColumns());
		}
		if (subAreaSize.getRows() < 0)
		{
			subAreaSize.setRows(-subAreaSize.getRows());
		}

		if (topLeft.getY() >= areaSize.getColumns() || topLeft.getX() >= areaSize.getRows())
		{
			return new NullTextGraphics(); // Return something that doesn't do
											// anything
		}

		if (topLeft.getY() + subAreaSize.getColumns() > areaSize.getColumns())
		{
			subAreaSize.setColumns(areaSize.getColumns() - topLeft.getY());
		}
		if (topLeft.getX() + subAreaSize.getRows() > areaSize.getRows())
		{
			subAreaSize.setRows(areaSize.getRows() - topLeft.getX());
		}

		return new TextGraphicsImpl(this, topLeft, subAreaSize);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#drawString(int, int,
	 * java.lang.String, org.storm.console.ui.framework.screen.ScreenCharacterStyle[])
	 */
	@Override
	public void drawString(int column, int row, String string, ScreenCharacterStyle... styles)
	{
		if (column >= areaSize.getColumns() || row >= areaSize.getRows() || string == null)
		{
			return;
		}

		string = TabBehaviour.ALIGN_TO_COLUMN_4.replaceTabs(string, column + topLeft.getY());

		if (string.length() + column > areaSize.getColumns())
		{
			string = string.substring(0, areaSize.getColumns() - column);
		}

		EnumSet<ScreenCharacterStyle> stylesSet = EnumSet.noneOf(ScreenCharacterStyle.class);
		if (styles != null && styles.length != 0)
		{
			stylesSet = EnumSet.copyOf(Arrays.asList(styles));
		}

		if (currentlyBold)
		{
			stylesSet.add(ScreenCharacterStyle.BOLD);
		}

		screen.putString(column + topLeft.getY(), row + topLeft.getX(), string, foregroundColor, backgroundColor, stylesSet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#getBackgroundColor()
	 */
	@Override
	public Color getBackgroundColor()
	{
		return backgroundColor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#getForegroundColor()
	 */
	@Override
	public Color getForegroundColor()
	{
		return foregroundColor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.TextGraphics#setBackgroundColor(com.googlecode
	 * .lanterna.terminal.Terminal.Color)
	 */
	@Override
	public void setBackgroundColor(Color backgroundColor)
	{
		this.backgroundColor = backgroundColor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.TextGraphics#setForegroundColor(com.googlecode
	 * .lanterna.terminal.Terminal.Color)
	 */
	@Override
	public void setForegroundColor(Color foregroundColor)
	{
		this.foregroundColor = foregroundColor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#getWidth()
	 */
	@Override
	public int getWidth()
	{
		return areaSize.getColumns();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#getHeight()
	 */
	@Override
	public int getHeight()
	{
		return areaSize.getRows();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#getSize()
	 */
	@Override
	public TerminalSize getSize()
	{
		return new TerminalSize(getWidth(), getHeight());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#setBoldMask(boolean)
	 */
	@Override
	public void setBoldMask(boolean enabledBoldMask)
	{
		currentlyBold = enabledBoldMask;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#getTheme()
	 */
	@Override
	public Theme getTheme()
	{
		return theme;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.TextGraphics#translateToGlobalCoordinates
	 * (org.storm.console.ui.framework.terminal.TerminalPosition)
	 */
	@Override
	public TerminalPosition translateToGlobalCoordinates(TerminalPosition pointInArea)
	{
		return new TerminalPosition(pointInArea.getY() + topLeft.getY(), pointInArea.getX() + topLeft.getX());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.TextGraphics#applyTheme(org.storm.console.ui.framework
	 * .gui.Theme.Category)
	 */
	@Override
	public void applyTheme(Theme.Category category)
	{
		applyTheme(getTheme().getDefinition(category));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.TextGraphics#applyTheme(org.storm.console.ui.framework
	 * .gui.Theme.Definition)
	 */
	@Override
	public void applyTheme(Theme.Definition themeItem)
	{
		setForegroundColor(themeItem.foreground());
		setBackgroundColor(themeItem.background());
		setBoldMask(themeItem.isHighlighted());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#fillArea(char)
	 */
	@Override
	public void fillArea(char character)
	{
		fillRectangle(character, new TerminalPosition(0, 0), new TerminalSize(areaSize));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.TextGraphics#fillRectangle(char,
	 * org.storm.console.ui.framework.terminal.TerminalPosition,
	 * org.storm.console.ui.framework.terminal.TerminalSize)
	 */
	@Override
	public void fillRectangle(char character, TerminalPosition topLeft, TerminalSize rectangleSize)
	{
		StringBuilder emptyLineBuilder = new StringBuilder();
		for (int i = 0; i < rectangleSize.getColumns(); i++)
		{
			emptyLineBuilder.append(character);
		}
		String emptyLine = emptyLineBuilder.toString();
		for (int i = 0; i < rectangleSize.getRows(); i++)
		{
			drawString(topLeft.getY(), topLeft.getX() + i, emptyLine);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "TextGraphics {topLeft: " + topLeft.toString() + ", size: " + areaSize.toString() + "}";
	}

	/**
	 * The Class NullTextGraphics.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class NullTextGraphics implements TextGraphics
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.TextGraphics#applyTheme(com.googlecode
		 * .lanterna.gui.Theme.Category)
		 */
		@Override
		public void applyTheme(Theme.Category category)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.TextGraphics#applyTheme(com.googlecode
		 * .lanterna.gui.Theme.Definition)
		 */
		@Override
		public void applyTheme(Theme.Definition themeItem)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#drawString(int, int,
		 * java.lang.String,
		 * org.storm.console.ui.framework.screen.ScreenCharacterStyle[])
		 */
		@Override
		public void drawString(int column, int row, String string, ScreenCharacterStyle... styles)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#fillArea(char)
		 */
		@Override
		public void fillArea(char character)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#fillRectangle(char,
		 * org.storm.console.ui.framework.terminal.TerminalPosition,
		 * org.storm.console.ui.framework.terminal.TerminalSize)
		 */
		@Override
		public void fillRectangle(char character, TerminalPosition topLeft, TerminalSize rectangleSize)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#getBackgroundColor()
		 */
		@Override
		public Color getBackgroundColor()
		{
			return Color.DEFAULT;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#getForegroundColor()
		 */
		@Override
		public Color getForegroundColor()
		{
			return Color.DEFAULT;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#getHeight()
		 */
		@Override
		public int getHeight()
		{
			return 0;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#getSize()
		 */
		@Override
		public TerminalSize getSize()
		{
			return new TerminalSize(0, 0);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#getTheme()
		 */
		@Override
		public Theme getTheme()
		{
			return Theme.getDefaultTheme();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#getWidth()
		 */
		@Override
		public int getWidth()
		{
			return 0;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#setBackgroundColor(com.
		 * googlecode.lanterna.terminal.Terminal.Color)
		 */
		@Override
		public void setBackgroundColor(Color backgroundColor)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#setBoldMask(boolean)
		 */
		@Override
		public void setBoldMask(boolean enabledBoldMask)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.console.ui.framework.gui.TextGraphics#setForegroundColor(com.
		 * googlecode.lanterna.terminal.Terminal.Color)
		 */
		@Override
		public void setForegroundColor(Color foregroundColor)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.TextGraphics#subAreaGraphics(com.googlecode
		 * .lanterna.terminal.TerminalPosition)
		 */
		@Override
		public TextGraphics subAreaGraphics(TerminalPosition terminalPosition)
		{
			return new NullTextGraphics();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.TextGraphics#subAreaGraphics(com.googlecode
		 * .lanterna.terminal.TerminalPosition,
		 * org.storm.console.ui.framework.terminal.TerminalSize)
		 */
		@Override
		public TextGraphics subAreaGraphics(TerminalPosition topLeft, TerminalSize subAreaSize)
		{
			return new NullTextGraphics();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.console.ui.framework.gui.TextGraphics#translateToGlobalCoordinates
		 * (org.storm.console.ui.framework.terminal.TerminalPosition)
		 */
		@Override
		public TerminalPosition translateToGlobalCoordinates(TerminalPosition pointInArea)
		{
			return new TerminalPosition(0, 0);
		}
	}
}
