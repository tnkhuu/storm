/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package org.storm.console.ui.framework.component;



public class ASCIITableHeader  {

	private String headerName;
	private int headerAlign = ASCIITable.DEFAULT_HEADER_ALIGN;
	private int dataAlign = ASCIITable.DEFAULT_DATA_ALIGN;

	public ASCIITableHeader(String headerName) {
		this.headerName = headerName;
	}

	public ASCIITableHeader(String headerName, int dataAlign) {
		this.headerName = headerName;
		this.dataAlign = dataAlign;
	}

	/**
	 * Instantiates a new table header.
	 *
	 * @param headerName the header name
	 * @param dataAlign the data align
	 * @param headerAlign the header align
	 */
	public ASCIITableHeader(String headerName, int dataAlign, int headerAlign) {
		this.headerName = headerName;
		this.dataAlign = dataAlign;
		this.headerAlign = headerAlign;
	}

	public String getHeaderName() {
		return headerName;
	}
	
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	
	public int getHeaderAlign() {
		return headerAlign;
	}
	
	public void setHeaderAlign(int headerAlign) {
		this.headerAlign = headerAlign;
	}
	
	public int getDataAlign() {
		return dataAlign;
	}
	
	public void setDataAlign(int dataAlign) {
		this.dataAlign = dataAlign;
	}
	
}
