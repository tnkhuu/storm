/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.dialog;

import org.storm.console.ui.framework.Action;
import org.storm.console.ui.framework.Window;
import org.storm.console.ui.framework.component.Label;
import org.storm.console.ui.framework.component.Panel;

/**
 * The Class WaitingDialog.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class WaitingDialog extends Window
{

	/** The spin thread. */
	private final Thread spinThread;

	/** The spin label. */
	private final Label spinLabel;

	/** The is closed. */
	private boolean isClosed;

	/**
	 * Instantiates a new waiting dialog.
	 * 
	 * @param title
	 *            the title
	 * @param description
	 *            the description
	 */
	public WaitingDialog(String title, String description)
	{
		super(title);
		spinLabel = new Label("-");
		final Panel panel = new Panel(Panel.Orientation.HORIZONTAL);
		panel.addComponent(new Label(description));
		panel.addComponent(spinLabel);
		addComponent(panel);

		isClosed = false;
		spinThread = new Thread(new SpinCode());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Window#onVisible()
	 */
	@Override
	protected void onVisible()
	{
		super.onVisible();
		spinThread.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.Window#close()
	 */
	@Override
	public void close()
	{
		isClosed = true;
		getOwner().runInEventThread(new Action()
		{
			@Override
			public void doAction()
			{
				WaitingDialog.super.close();
			}
		});
	}

	/**
	 * The Class SpinCode.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private class SpinCode implements Runnable
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			while (!isClosed)
			{
				final String currentSpin = spinLabel.getText();
				final String nextSpin;
				if (currentSpin.equals("-"))
				{
					nextSpin = "\\";
				}
				else if (currentSpin.equals("\\"))
				{
					nextSpin = "|";
				}
				else if (currentSpin.equals("|"))
				{
					nextSpin = "/";
				}
				else
				{
					nextSpin = "-";
				}
				if (getOwner() != null)
				{
					getOwner().runInEventThread(new Action()
					{
						@Override
						public void doAction()
						{
							spinLabel.setText(nextSpin);
						}
					});
				}
				try
				{
					Thread.sleep(100);
				}
				catch (InterruptedException e)
				{
				}
			}
		}
	}
}
