/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class EmptySpace.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class EmptySpace extends AbstractComponent
{

	/** The size. */
	private final TerminalSize size;

	/**
	 * Instantiates a new empty space.
	 */
	public EmptySpace()
	{
		this(1, 1);
	}

	/**
	 * Instantiates a new empty space.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 */
	public EmptySpace(final int width, final int height)
	{
		this.size = new TerminalSize(width, height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		return size;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		graphics.applyTheme(Category.DIALOG_AREA);
		graphics.fillArea(' ');
	}
}
