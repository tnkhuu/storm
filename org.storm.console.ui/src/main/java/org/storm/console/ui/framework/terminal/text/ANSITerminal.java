/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal.text;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.storm.console.ui.framework.input.CommonProfile;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class ANSITerminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class ANSITerminal extends StreamBasedTerminal
{

	/**
	 * Instantiates a new aNSI terminal.
	 * 
	 * @param terminalInput
	 *            the terminal input
	 * @param terminalOutput
	 *            the terminal output
	 * @param terminalCharset
	 *            the terminal charset
	 */
	ANSITerminal(InputStream terminalInput, OutputStream terminalOutput, Charset terminalCharset)
	{
		super(terminalInput, terminalOutput, terminalCharset);
		addInputProfile(new CommonProfile());
	}

	/**
	 * Csi.
	 */
	private void CSI()
	{
		writeToTerminal((byte) 0x1b, (byte) '[');
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.terminal.Terminal#queryTerminalSize()
	 */
	@Deprecated
	@Override
	public TerminalSize queryTerminalSize()
	{
		synchronized (writerMutex)
		{
			saveCursorPosition();
			moveCursor(5000, 5000);
			reportPosition();
			restoreCursorPosition();
		}
		return getLastKnownSize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.terminal.Terminal#getTerminalSize()
	 */
	@Override
	public TerminalSize getTerminalSize()
	{
		queryTerminalSize();
		return waitForTerminalSizeReport(1000); // Wait 1 second for the
												// terminal size report to come,
												// is this reasonable?
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.terminal.Terminal#applyBackgroundColor(com.googlecode
	 * .lanterna.terminal.Terminal.Color)
	 */
	@Override
	public void applyBackgroundColor(Color color)
	{
		synchronized (writerMutex)
		{
			CSI();
			writeToTerminal((byte) '4', (byte) (color.getIndex() + "").charAt(0), (byte) 'm');
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyBackgroundColor(int,
	 * int, int)
	 */
	@Override
	public void applyBackgroundColor(int r, int g, int b)
	{
		if (r < 0 || r > 255)
		{
			throw new IllegalArgumentException("applyForegroundColor: r is outside of valid range (0-255)");
		}
		if (g < 0 || g > 255)
		{
			throw new IllegalArgumentException("applyForegroundColor: g is outside of valid range (0-255)");
		}
		if (b < 0 || b > 255)
		{
			throw new IllegalArgumentException("applyForegroundColor: b is outside of valid range (0-255)");
		}

		synchronized (writerMutex)
		{
			CSI();
			String asString = "48;2;" + r + ";" + g + ";" + b + "m";
			for (int i = 0; i < asString.length(); i++)
			{
				writeToTerminal((byte) asString.charAt(i));
			}
		}
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyBackgroundColor(int)
	 */
	@Override
	public void applyBackgroundColor(int index)
	{
		if (index < 0 || index > 255)
		{
			throw new IllegalArgumentException("applyBackgroundColor: index is outside of valid range (0-255)");
		}

		synchronized (writerMutex)
		{
			CSI();
			String asString = "48;5;" + index + "m";
			for (int i = 0; i < asString.length(); i++)
			{
				writeToTerminal((byte) asString.charAt(i));
			}
		}
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyForegroundColor(org.storm.console.ui.framework.terminal.Terminal.Color)
	 */
	@Override
	public void applyForegroundColor(Color color)
	{
		synchronized (writerMutex)
		{
			CSI();
			writeToTerminal((byte) '3', (byte) (color.getIndex() + "").charAt(0), (byte) 'm');
		}
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyForegroundColor(int, int, int)
	 */
	@Override
	public void applyForegroundColor(int r, int g, int b)
	{
		if (r < 0 || r > 255)
		{
			throw new IllegalArgumentException("applyForegroundColor: r is outside of valid range (0-255)");
		}
		if (g < 0 || g > 255)
		{
			throw new IllegalArgumentException("applyForegroundColor: g is outside of valid range (0-255)");
		}
		if (b < 0 || b > 255)
		{
			throw new IllegalArgumentException("applyForegroundColor: b is outside of valid range (0-255)");
		}

		synchronized (writerMutex)
		{
			CSI();
			String asString = "38;2;" + r + ";" + g + ";" + b + "m";
			for (int i = 0; i < asString.length(); i++)
			{
				writeToTerminal((byte) asString.charAt(i));
			}
		}
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#applyForegroundColor(int)
	 */
	@Override
	public void applyForegroundColor(int index)
	{
		if (index < 0 || index > 255)
		{
			throw new IllegalArgumentException("applyForegroundColor: index is outside of valid range (0-255)");
		}

		synchronized (writerMutex)
		{
			CSI();
			String asString = "38;5;" + index + "m";
			for (int i = 0; i < asString.length(); i++)
			{
				writeToTerminal((byte) asString.charAt(i));
			}
		}
	}

	
	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#applySGR(org.storm.console.ui.framework.terminal.Terminal.SGR[])
	 */
	@SuppressWarnings("incomplete-switch")
	@Override
	public void applySGR(SGR... options)
	{
		synchronized (writerMutex)
		{
			CSI();
			int index = 0;
			for (SGR sgr : options)
			{
				switch (sgr)
				{
				case ENTER_BOLD:
					writeToTerminal((byte) '1');
					break;
				case ENTER_REVERSE:
					writeToTerminal((byte) '7');
					break;
				case ENTER_UNDERLINE:
					writeToTerminal((byte) '4');
					break;
				case EXIT_BOLD:
					writeToTerminal((byte) '2', (byte) '2');
					break;
				case EXIT_REVERSE:
					writeToTerminal((byte) '2', (byte) '7');
					break;
				case EXIT_UNDERLINE:
					writeToTerminal((byte) '2', (byte) '4');
					break;
				case ENTER_BLINK:
					writeToTerminal((byte) '5');
					break;
				case RESET_ALL:
					writeToTerminal((byte) '0');
					break;
				}
				if (index++ < options.length - 1)
				{
					writeToTerminal((byte) ';');
				}
			}
			writeToTerminal((byte) 'm');
		}
	}

	
	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#clearScreen()
	 */
	@Override
	public void clearScreen()
	{
		synchronized (writerMutex)
		{
			CSI();
			writeToTerminal((byte) '2', (byte) 'J');
		}
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#enterPrivateMode()
	 */
	@Override
	public void enterPrivateMode()
	{
		synchronized (writerMutex)
		{
			CSI();
			writeToTerminal((byte) '?', (byte) '1', (byte) '0', (byte) '4', (byte) '9', (byte) 'h');
		}
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#exitPrivateMode()
	 */
	@Override
	public void exitPrivateMode()
	{
		synchronized (writerMutex)
		{
			applySGR(SGR.RESET_ALL);
			setCursorVisible(true);
			CSI();
			writeToTerminal((byte) '?', (byte) '1', (byte) '0', (byte) '4', (byte) '9', (byte) 'l');
		}
	}

	/**
	 * Sets the echo.
	 * 
	 * @param echoOn
	 *            the new echo
	 */
	public abstract void setEcho(boolean echoOn);

	/**
	 * Sets the c break.
	 * 
	 * @param cbreakOn
	 *            the new c break
	 */
	public abstract void setCBreak(boolean cbreakOn);

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#moveCursor(int, int)
	 */
	@Override
	public void moveCursor(int x, int y)
	{
		synchronized (writerMutex)
		{
			CSI();
			writeToTerminal((y + 1 + "").getBytes());
			writeToTerminal((byte) ';');
			writeToTerminal((x + 1 + "").getBytes());
			writeToTerminal((byte) 'H');
		}
	}
	
	@Override
	public void newLine(int y)
	{
		synchronized (writerMutex)
		{
			
			writeToTerminal("\n".getBytes());

		}
		
	}
	
	public void deleteLeft() {
		synchronized (writerMutex)
		{
			
			CSI();
			writeToTerminal((1 + "").getBytes());
			writeToTerminal((byte) 'D');
			CSI();
			writeToTerminal((1 + "").getBytes());
			writeToTerminal((byte) 'X');
		}
	}
	
	

	@Override
	public void incrementXCursor(int y)
	{
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.terminal.Terminal#setCursorVisible(boolean)
	 */
	@Override
	public void setCursorVisible(boolean visible)
	{
		synchronized (writerMutex)
		{
			CSI();
			writeToTerminal((byte) '?');
			writeToTerminal((byte) '2');
			writeToTerminal((byte) '5');
			if (visible)
			{
				writeToTerminal((byte) 'h');
			}
			else
			{
				writeToTerminal((byte) 'l');
			}
		}
	}

	/**
	 * Report position.
	 */
	protected void reportPosition()
	{
		CSI();
		writeToTerminal("6n".getBytes());
	}

	/**
	 * Restore cursor position.
	 */
	protected void restoreCursorPosition()
	{
		CSI();
		writeToTerminal("u".getBytes());
	}

	/**
	 * Save cursor position.
	 */
	protected void saveCursorPosition()
	{
		CSI();
		writeToTerminal("s".getBytes());
	}
}
