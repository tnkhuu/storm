/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import org.storm.console.ui.framework.Position;

/**
 * The Class TerminalPosition.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TerminalPosition implements Position
{

	/** The row. */
	private int x;

	/** The column. */
	private int y;

	/**
	 * Instantiates a new terminal position.
	 * 
	 * @param position
	 *            the position
	 */
	public TerminalPosition(TerminalPosition position)
	{
		this(position.getY(), position.getX());
	}

	/**
	 * Instantiates a new terminal position.
	 * 
	 * @param column
	 *            the column
	 * @param row
	 *            the row
	 */
	public TerminalPosition(int column, int row)
	{
		this.x = row;
		this.y = column;
	}

	/**
	 * Gets the column.
	 * 
	 * @return the column
	 */
	public int getY()
	{
		return y;
	}

	/**
	 * Gets the row.
	 * 
	 * @return the row
	 */
	public int getX()
	{
		return x;
	}

	/**
	 * Sets the column.
	 * 
	 * @param column
	 *            the new column
	 */
	public void setY(int column)
	{
		this.y = column;
	}

	/**
	 * Sets the row.
	 * 
	 * @param row
	 *            the new row
	 */
	public void setX(int row)
	{
		this.x = row;
	}

	/**
	 * Ensure positive position.
	 */
	public void ensurePositivePosition()
	{
		if (x < 0)
		{
			x = 0;
		}
		if (y < 0)
		{
			y = 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[" + y + ":" + x + "]";
	}
}
