/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.input;

import java.util.List;

/**
 * The Class CtrlAndCharacterPattern.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CtrlAndCharacterPattern implements CharacterPattern
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#getResult(java.util.List)
	 */
	@Override
	public Key getResult(List<Character> matching)
	{
		int firstCode = 'a' - 1;
		return new Key((char) (firstCode + matching.get(0).charValue()), true, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#isCompleteMatch(java.util
	 * .List)
	 */
	@Override
	public boolean isCompleteMatch(List<Character> currentMatching)
	{
		return matches(currentMatching);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.input.CharacterPattern#matches(java.util.List)
	 */
	@Override
	public boolean matches(List<Character> currentMatching)
	{
		if (currentMatching.size() > 1)
		{
			return false;
		}

		if (currentMatching.get(0).charValue() == '\n' || currentMatching.get(0).charValue() == '\r' || currentMatching.get(0).charValue() == '\t')
		{
			return false;
		}

		return currentMatching.get(0).charValue() <= 26;
	}
}
