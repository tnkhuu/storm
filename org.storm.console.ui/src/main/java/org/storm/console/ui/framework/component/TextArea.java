/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.terminal.ACS;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class TextArea.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class TextArea extends AbstractInteractableComponent
{

	/** The lines. */
	private final List<String> lines;

	/** The preferred size. */
	private final TerminalSize preferredSize;

	/** The last size. */
	private TerminalSize lastSize;

	/** The maximum size. */
	private TerminalSize maximumSize;

	/** The minimum size. */
	private TerminalSize minimumSize;

	/** The longest line. */
	private int longestLine;

	/** The scroll top index. */
	private int scrollTopIndex;

	/** The scroll left index. */
	private int scrollLeftIndex;

	/**
	 * Instantiates a new text area.
	 */
	public TextArea()
	{
		this("");
	}

	/**
	 * Instantiates a new text area.
	 * 
	 * @param text
	 *            the text
	 */
	public TextArea(String text)
	{
		this(new TerminalSize(0, 0), text);
	}

	/**
	 * Instantiates a new text area.
	 * 
	 * @param preferredSize
	 *            the preferred size
	 * @param text
	 *            the text
	 */
	public TextArea(TerminalSize preferredSize, String text)
	{
		if (text == null)
		{
			text = "";
		}

		this.lines = new ArrayList<String>();
		this.preferredSize = preferredSize;
		this.scrollTopIndex = 0;
		this.scrollLeftIndex = 0;
		this.maximumSize = null;
		this.minimumSize = new TerminalSize(10, 3);
		this.lastSize = null;
		lines.addAll(Arrays.asList(text.split("\n")));
		scanForLongestLine();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		int width = preferredSize.getColumns() > 0 ? preferredSize.getColumns() : longestLine + 1;
		int height = preferredSize.getRows() > 0 ? preferredSize.getRows() : lines.size() + 1;
		if (maximumSize != null)
		{
			if (width > maximumSize.getColumns())
			{
				width = maximumSize.getColumns();
			}
			if (height > maximumSize.getRows())
			{
				height = maximumSize.getRows();
			}
		}
		if (minimumSize != null)
		{
			if (width < minimumSize.getColumns())
			{
				width = minimumSize.getColumns();
			}
			if (height < minimumSize.getRows())
			{
				height = minimumSize.getRows();
			}
		}
		return new TerminalSize(width, height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		lastSize = new TerminalSize(graphics.getWidth(), graphics.getHeight());

		// Do we need to recalculate the scroll position?
		// This code would be triggered by resizing the window when the scroll
		// position is at the bottom
		if (lines.size() > graphics.getHeight() && lines.size() - scrollTopIndex < graphics.getHeight())
		{
			scrollTopIndex = lines.size() - graphics.getHeight();
		}
		if (longestLine > graphics.getWidth() && longestLine - scrollLeftIndex < graphics.getWidth())
		{
			scrollLeftIndex = longestLine - graphics.getWidth();
		}

		if (hasFocus())
		{
			graphics.applyTheme(Theme.Category.TEXTBOX_FOCUSED);
		}
		else
		{
			graphics.applyTheme(Theme.Category.TEXTBOX);
		}
		graphics.fillArea(' ');

		for (int i = scrollTopIndex; i < lines.size(); i++)
		{
			if (i - scrollTopIndex >= graphics.getHeight())
			{
				break;
			}

			printItem(graphics, 0, 0 + i - scrollTopIndex, lines.get(i));
		}

		boolean hasSetHotSpot = false;

		if (lines.size() > graphics.getHeight())
		{
			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			graphics.drawString(graphics.getWidth() - 1, 0, ACS.ARROW_UP + "");

			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			for (int i = 1; i < graphics.getHeight() - 1; i++)
			{
				graphics.drawString(graphics.getWidth() - 1, i, ACS.BLOCK_MIDDLE + "");
			}

			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			graphics.drawString(graphics.getWidth() - 1, graphics.getHeight() - 1, ACS.ARROW_DOWN + "");

			// Finally print the 'tick'
			int scrollableSize = lines.size() - graphics.getHeight();
			double position = (double) scrollTopIndex / (double) scrollableSize;
			int tickPosition = (int) ((graphics.getHeight() - 3.0) * position);

			graphics.applyTheme(Theme.Category.SHADOW);
			graphics.drawString(graphics.getWidth() - 1, 1 + tickPosition, " ");

			if (hasFocus())
			{
				setHotspot(graphics.translateToGlobalCoordinates(new TerminalPosition(graphics.getWidth() - 1, 1 + tickPosition)));
				hasSetHotSpot = true;
			}
		}
		if (longestLine > graphics.getWidth())
		{
			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			graphics.drawString(0, graphics.getHeight() - 1, ACS.ARROW_LEFT + "");

			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			for (int i = 1; i < graphics.getWidth() - 2; i++)
			{
				graphics.drawString(i, graphics.getHeight() - 1, ACS.BLOCK_MIDDLE + "");
			}

			graphics.applyTheme(Theme.Category.DIALOG_AREA);
			graphics.drawString(graphics.getWidth() - 2, graphics.getHeight() - 1, ACS.ARROW_RIGHT + "");

			// Finally print the 'tick'
			int scrollableSize = longestLine - graphics.getWidth();
			double position = (double) scrollLeftIndex / (double) scrollableSize;
			int tickPosition = (int) ((graphics.getWidth() - 4.0) * position);

			graphics.applyTheme(Theme.Category.SHADOW);
			graphics.drawString(1 + tickPosition, graphics.getHeight() - 1, " ");
		}

		if (!hasSetHotSpot)
		{
			setHotspot(graphics.translateToGlobalCoordinates(new TerminalPosition(0, 0)));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Interactable#keyboardInteraction(com.googlecode
	 * .lanterna.input.Key)
	 */
	@Override
	public Result keyboardInteraction(Key key)
	{
		try
		{
			switch (key.getKind())
			{
			case Tab:
			case Enter:
				return Result.NEXT_INTERACTABLE_RIGHT;

			case ReverseTab:
				return Result.PREVIOUS_INTERACTABLE_LEFT;

			case ArrowRight:
				if (lastSize != null && scrollLeftIndex < longestLine - lastSize.getColumns())
				{
					scrollLeftIndex++;
				}
				break;

			case ArrowLeft:
				if (scrollLeftIndex > 0)
				{
					scrollLeftIndex--;
				}
				break;

			case ArrowDown:
				if (lastSize != null && scrollTopIndex < lines.size() - lastSize.getRows())
				{
					scrollTopIndex++;
				}
				break;

			case ArrowUp:
				if (scrollTopIndex > 0)
				{
					scrollTopIndex--;
				}
				break;

			case PageUp:
				scrollTopIndex -= lastSize.getRows();
				if (scrollTopIndex < 0)
				{
					scrollTopIndex = 0;
				}
				break;

			case PageDown:
				scrollTopIndex += lastSize.getRows();
				if (scrollTopIndex >= lines.size() - lastSize.getRows())
				{
					scrollTopIndex = lines.size() - lastSize.getRows();
				}
				break;

			case Home:
				scrollTopIndex = 0;
				break;

			case End:
				scrollTopIndex = lines.size() - lastSize.getRows();
				break;

			default:
				return Result.EVENT_NOT_HANDLED;
			}
			return Result.EVENT_HANDLED;
		}
		finally
		{
			invalidate();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractComponent#isScrollable()
	 */
	@Override
	public boolean isScrollable()
	{
		return true;
	}

	/**
	 * Sets the maximum size.
	 * 
	 * @param maximumSize
	 *            the new maximum size
	 */
	public void setMaximumSize(TerminalSize maximumSize)
	{
		this.maximumSize = maximumSize;
	}

	/**
	 * Gets the maximum size.
	 * 
	 * @return the maximum size
	 */
	public TerminalSize getMaximumSize()
	{
		return maximumSize;
	}

	/**
	 * Sets the minimum size.
	 * 
	 * @param minimumSize
	 *            the new minimum size
	 */
	public void setMinimumSize(TerminalSize minimumSize)
	{
		this.minimumSize = minimumSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.component.AbstractComponent#getMinimumSize()
	 */
	@Override
	public TerminalSize getMinimumSize()
	{
		return minimumSize;
	}

	/**
	 * Clear.
	 */
	public void clear()
	{
		lines.clear();
		lines.add("");
		invalidate();
	}

	/**
	 * Gets the line.
	 * 
	 * @param index
	 *            the index
	 * @return the line
	 */
	public String getLine(int index)
	{
		return lines.get(index);
	}

	/**
	 * Append line.
	 * 
	 * @param line
	 *            the line
	 */
	public void appendLine(String line)
	{
		lines.add(line);
		if (line.length() > longestLine)
		{
			longestLine = line.length();
		}
		invalidate();
	}

	/**
	 * Insert line.
	 * 
	 * @param index
	 *            the index
	 * @param line
	 *            the line
	 */
	public void insertLine(int index, String line)
	{
		lines.add(index, line);
		if (line.length() > longestLine)
		{
			longestLine = line.length();
		}
		invalidate();
	}

	/**
	 * Sets the line.
	 * 
	 * @param index
	 *            the index
	 * @param line
	 *            the line
	 */
	public void setLine(int index, String line)
	{
		String oldLine = lines.set(index, line);
		if (oldLine.length() == longestLine)
		{
			scanForLongestLine();
		}
		else
		{
			if (line.length() > longestLine)
			{
				longestLine = line.length();
			}
		}
		invalidate();
	}

	/**
	 * Removes the line.
	 * 
	 * @param lineNumber
	 *            the line number
	 */
	public void removeLine(int lineNumber)
	{
		String line = lines.get(lineNumber);
		lines.remove(lineNumber);
		if (line.length() >= longestLine)
		{
			scanForLongestLine();
		}
		invalidate();
	}

	/**
	 * Prints the item.
	 * 
	 * @param graphics
	 *            the graphics
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param text
	 *            the text
	 */
	protected void printItem(TextGraphics graphics, int x, int y, String text)
	{
		// TODO: fix this
		text = text.replace("\t", "    ");

		if (scrollLeftIndex >= text.length())
		{
			text = "";
		}
		else
		{
			text = text.substring(scrollLeftIndex);
		}

		if (text.length() > graphics.getWidth())
		{
			text = text.substring(0, graphics.getWidth());
		}
		graphics.drawString(x, y, text);
	}

	/**
	 * Scan for longest line.
	 */
	private void scanForLongestLine()
	{
		longestLine = 0;
		for (String line : lines)
		{
			if (line.replace("\t", "    ").length() > longestLine)
			{
				longestLine = line.replace("\t", "    ").length();
			}
		}
	}

}
