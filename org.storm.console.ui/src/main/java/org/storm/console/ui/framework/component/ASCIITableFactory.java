/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.component;

import java.util.List;
import java.util.Vector;

import org.fusesource.jansi.Ansi;

public class ASCIITableFactory
{
	public static ASCIITable createTable(String[] headers, String[][] data)
	{
		return new ASCIITable(headers, data);
	}

	public static ASCIITable createTable(ASCIITableHeader[] headers, String[][] data)
	{
		return createTable(headers, data, Ansi.Color.DEFAULT);
	}
	
	public static ASCIITable createTable(ASCIITableHeader[] headers, CharSequence[][] data, Ansi.Color color)
	{
		return new ASCIITable(headers, data, color);
	}

	public static <T> ASCIIReflectionBuiltTable<T> createTable(List<T> records, String... headers)
	{
		return new ASCIIReflectionBuiltTable<T>(records, headers);
	}

	public static ASCIITable createTable(ASCIIReflectionBuiltTable<?> collectionTable)
	{
		ASCIITableHeader[] headerObjs = new ASCIITableHeader[0];
		String[][] data = new String[0][0];

		List<Object> rowData = null;
		ASCIITableHeader colHeader = null;

		Vector<String> rowTransData = null;
		String[] rowContent = null;
		String cellData = null;

		if (collectionTable != null)
		{

			if (collectionTable.getHeaders() != null && !collectionTable.getHeaders().isEmpty())
			{
				headerObjs = new ASCIITableHeader[collectionTable.getHeaders().size()];
				for (int i = 0; i < collectionTable.getHeaders().size(); i++)
				{
					headerObjs[i] = collectionTable.getHeaders().get(i);
				}
			}

			if (collectionTable.getData() != null && !collectionTable.getData().isEmpty())
			{
				data = new String[collectionTable.getData().size()][];

				for (int i = 0; i < collectionTable.getData().size(); i++)
				{
					// Get the row data
					rowData = collectionTable.getData().get(i);
					rowTransData = new Vector<String>(rowData.size());

					// Transform each cell in the row
					for (int j = 0; j < rowData.size(); j++)
					{
						colHeader = j < headerObjs.length ? headerObjs[j] : null;

						cellData = collectionTable.formatData(colHeader, i, j, rowData.get(j));

						if (cellData == null)
						{
							cellData = String.valueOf(rowData.get(j));
						}

						rowTransData.add(cellData);

					}// iterate all columns

					// Set the transformed content
					rowContent = new String[rowTransData.size()];
					rowTransData.copyInto(rowContent);
					data[i] = rowContent;

				}

			}
		}
		return new ASCIITable(headerObjs, data);
	}
}
