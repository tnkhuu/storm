/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.console.ui.framework.impl;

import java.io.PrintStream;

import org.storm.console.ui.framework.Canvas;
import org.storm.console.ui.framework.Cursor;
import org.storm.console.ui.framework.Drawable;
import org.storm.console.ui.framework.Frame;

public class FrameImpl implements Frame
{
	private final int width;
	private final int height;
	@SuppressWarnings("unused")
	private Cursor cursor;
	@SuppressWarnings("unused")
	private final Canvas canvas;
	private FrameBuffer buffer;
	
	public FrameImpl(int width, int height, Canvas canvas)
	{
		this.width = width;
		this.height = height;
		this.canvas = canvas;
		this.buffer = new FrameBuffer(width, height);
	}
	
	public void draw(Drawable drawable) {
		drawable.write(buffer);
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public void flush(PrintStream out)
	{
		buffer.write(out);
		
	}


}
