/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.listener;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.component.InteractableComponent;

/**
 * The Class ComponentAdapter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentAdapter implements ComponentListener
{

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.listener.ComponentListener#onComponentInvalidated(org.storm.console.ui.framework.Component)
	 */
	@Override
	public void onComponentInvalidated(Component component)
	{
	}


	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.listener.ComponentListener#onComponentLostFocus(org.storm.console.ui.framework.component.InteractableComponent)
	 */
	@Override
	public void onComponentLostFocus(InteractableComponent interactableComponent)
	{
	}

	
	/* (non-Javadoc)
	 * @see org.storm.console.ui.framework.listener.ComponentListener#onComponentReceivedFocus(org.storm.console.ui.framework.component.InteractableComponent)
	 */
	@Override
	public void onComponentReceivedFocus(InteractableComponent interactableComponent)
	{
	}
}
