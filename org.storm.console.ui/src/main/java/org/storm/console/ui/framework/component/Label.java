/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import java.util.Arrays;

import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalSize;
import org.storm.console.ui.framework.terminal.Terminal.Color;

/**
 * The Class Label.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Label extends AbstractComponent
{

	/** The text. */
	private String[] text;

	/** The height. */
	private int height;

	/** The width. */
	private int width;

	/** The force width. */
	private int forceWidth;

	/** The text bold. */
	private Boolean textBold;

	/** The text color. */
	private Terminal.Color textColor;

	/** The style. */
	private Theme.Category style;

	/**
	 * Instantiates a new label.
	 */
	public Label()
	{
		this("");
	}

	/**
	 * Instantiates a new label.
	 * 
	 * @param text
	 *            the text
	 */
	public Label(String text)
	{
		this(text, -1);
	}

	/**
	 * Instantiates a new label.
	 * 
	 * @param text
	 *            the text
	 * @param textColor
	 *            the text color
	 */
	public Label(String text, Terminal.Color textColor)
	{
		this(text, textColor, null);
	}

	/**
	 * Instantiates a new label.
	 * 
	 * @param text
	 *            the text
	 * @param textBold
	 *            the text bold
	 */
	public Label(String text, Boolean textBold)
	{
		this(text, null, textBold);
	}

	/**
	 * Instantiates a new label.
	 * 
	 * @param text
	 *            the text
	 * @param textColor
	 *            the text color
	 * @param textBold
	 *            the text bold
	 */
	public Label(String text, Terminal.Color textColor, Boolean textBold)
	{
		this(text, -1, textColor, textBold);
	}

	/**
	 * Instantiates a new label.
	 * 
	 * @param text
	 *            the text
	 * @param fixedWidth
	 *            the fixed width
	 */
	public Label(String text, int fixedWidth)
	{
		this(text, fixedWidth, null, null);
	}

	/**
	 * Instantiates a new label.
	 * 
	 * @param text
	 *            the text
	 * @param fixedWidth
	 *            the fixed width
	 * @param color
	 *            the color
	 * @param textBold
	 *            the text bold
	 */
	public Label(String text, int fixedWidth, Terminal.Color color, Boolean textBold)
	{
		if (text == null)
		{
			this.text = new String[] { "null" };
		}
		else
		{
			this.text = text.split("\n");
		}
		this.textColor = color;
		this.textBold = textBold;
		this.height = 0;
		this.width = 0;
		this.forceWidth = fixedWidth;
		this.style = Theme.Category.DIALOG_AREA;
		updateMetrics();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		if (forceWidth == -1)
		{
			return new TerminalSize(width, height);
		}
		else
		{
			return new TerminalSize(forceWidth, height);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		graphics.applyTheme(graphics.getTheme().getDefinition(style));
		graphics = transformAccordingToAlignment(graphics, calculatePreferredSize());

		if (textColor != null)
		{
			graphics.setForegroundColor(textColor);
		}
		if (textBold != null)
		{
			if (textBold)
			{
				graphics.setBoldMask(true);
			}
			else
			{
				graphics.setBoldMask(false);
			}
		}

		if (text.length == 0)
		{
			return;
		}

		int leftPosition = 0;
		for (int i = 0; i < text.length; i++)
		{
			if (forceWidth > -1)
			{
				if (text[i].length() > forceWidth)
				{
					graphics.drawString(leftPosition, i, text[i].substring(0, forceWidth - 3) + "...");
				}
				else
				{
					graphics.drawString(leftPosition, i, text[i]);
				}
			}
			else
			{
				graphics.drawString(leftPosition, i, text[i]);
			}
		}
	}

	/**
	 * Sets the text.
	 * 
	 * @param text
	 *            the new text
	 */
	public void setText(String text)
	{
		this.text = text.split("\n");
		updateMetrics();
		invalidate();
	}

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	public String getText()
	{
		StringBuilder sb = new StringBuilder();
		for (String line : text)
		{
			sb.append(line).append("\n");
		}
		sb.delete(sb.length() - 1, sb.length());
		return sb.toString();
	}

	/**
	 * Gets the lines.
	 * 
	 * @return the lines
	 */
	public String[] getLines()
	{
		return Arrays.copyOf(text, text.length);
	}

	/**
	 * Sets the style.
	 * 
	 * @param style
	 *            the new style
	 */
	public void setStyle(Category style)
	{
		this.style = style;
		invalidate();
	}

	/**
	 * Gets the style.
	 * 
	 * @return the style
	 */
	public Category getStyle()
	{
		return style;
	}

	/**
	 * Gets the text color.
	 * 
	 * @return the text color
	 */
	public Color getTextColor()
	{
		return textColor;
	}

	/**
	 * Sets the text color.
	 * 
	 * @param textColor
	 *            the new text color
	 */
	public void setTextColor(Color textColor)
	{
		this.textColor = textColor;
		invalidate();
	}

	/**
	 * Update metrics.
	 */
	private void updateMetrics()
	{
		height = text.length;
		if (height == 0)
		{
			height = 1;
		}

		width = 0;
		for (String line : text)
		{
			if (line.length() > width)
			{
				width = line.length();
			}
		}
	}
}
