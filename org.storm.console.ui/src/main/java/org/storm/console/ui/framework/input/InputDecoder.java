/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.input;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.storm.console.ui.framework.ConsoleException;
import org.storm.console.ui.framework.terminal.TerminalPosition;

/**
 * The Class InputDecoder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class InputDecoder
{

	/** The source. */
	private final Reader source;

	/** The input buffer. */
	private final Queue<Character> inputBuffer;

	/** The left over queue. */
	private final Queue<Character> leftOverQueue;

	/** The byte patterns. */
	private final Set<CharacterPattern> bytePatterns;

	/** The current matching. */
	private final List<Character> currentMatching;

	/** The last reported terminal position. */
	private TerminalPosition lastReportedTerminalPosition;

	/**
	 * Instantiates a new input decoder.
	 * 
	 * @param source
	 *            the source
	 */
	public InputDecoder(final Reader source)
	{
		this.source = source;
		this.inputBuffer = new LinkedList<Character>();
		this.leftOverQueue = new LinkedList<Character>();
		this.bytePatterns = new HashSet<CharacterPattern>();
		this.currentMatching = new ArrayList<Character>();
		this.lastReportedTerminalPosition = null;
	}

	/**
	 * Instantiates a new input decoder.
	 * 
	 * @param source
	 *            the source
	 * @param profile
	 *            the profile
	 */
	public InputDecoder(final Reader source, final KeyMappingProfile profile)
	{
		this(source);
		addProfile(profile);
	}

	/**
	 * Adds the profile.
	 * 
	 * @param profile
	 *            the profile
	 */
	public void addProfile(KeyMappingProfile profile)
	{
		for (CharacterPattern pattern : profile.getPatterns())
		{
			bytePatterns.add(pattern);
		}
	}

	/**
	 * Gets the next character.
	 * 
	 * @return the next character
	 */
	public Key getNextCharacter()
	{
		if (leftOverQueue.size() > 0)
		{
			Character first = leftOverQueue.poll();
			// HACK!!!
			if (first == 0x1b)
			{
				return new Key(Kind.Escape);
			}

			return new Key(first.charValue());
		}

		try
		{
			while (source.ready())
			{
				int readChar = source.read();
				if (readChar == -1)
				{
					return null;
				}

				inputBuffer.add((char) readChar);
			}
		}
		catch (IOException e)
		{
			throw new ConsoleException(e);
		}

		if (inputBuffer.size() == 0)
		{
			return null;
		}

		while (true)
		{
			// Check all patterns
			Character nextChar = inputBuffer.poll();
			boolean canMatchWithOneMoreChar = false;

			if (nextChar != null)
			{
				currentMatching.add(nextChar);
				for (CharacterPattern pattern : bytePatterns)
				{
					if (pattern.matches(currentMatching))
					{
						if (pattern.isCompleteMatch(currentMatching))
						{
							Key result = pattern.getResult(currentMatching);
							if (result.getKind() == Kind.CursorLocation)
							{
								lastReportedTerminalPosition = ScreenInfoCharacterPattern.getCursorPosition(currentMatching);
							}
							currentMatching.clear();
							return result;
						}
						else
						{
							canMatchWithOneMoreChar = true;
						}
					}
				}
			}
			if (!canMatchWithOneMoreChar)
			{
				for (Character c : currentMatching)
				{
					leftOverQueue.add(c);
				}
				currentMatching.clear();
				Character first = leftOverQueue.poll();

				// HACK!!!
				if (first == 0x1b)
				{
					return new Key(Kind.Escape);
				}
				return new Key(first.charValue());
			}
		}
	}

	/**
	 * Gets the last reported terminal position.
	 * 
	 * @return the last reported terminal position
	 */
	public TerminalPosition getLastReportedTerminalPosition()
	{
		return lastReportedTerminalPosition;
	}
}
