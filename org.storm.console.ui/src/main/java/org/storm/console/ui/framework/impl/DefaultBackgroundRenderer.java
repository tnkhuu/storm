/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.impl;

import org.storm.console.ui.framework.GUIScreenBackgroundRenderer;
import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme;
import org.storm.console.ui.framework.terminal.TerminalPosition;

/**
 * The Class DefaultBackgroundRenderer.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DefaultBackgroundRenderer implements GUIScreenBackgroundRenderer
{

	/** The title. */
	private String title;

	/** The show memory usage. */
	private boolean showMemoryUsage;

	/**
	 * Instantiates a new default background renderer.
	 */
	public DefaultBackgroundRenderer()
	{
		this("");
	}

	/**
	 * Instantiates a new default background renderer.
	 * 
	 * @param title
	 *            the title
	 */
	public DefaultBackgroundRenderer(String title)
	{
		this.title = title;
		this.showMemoryUsage = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.GUIScreenBackgroundRenderer#drawBackground
	 * (org.storm.console.ui.framework.gui.TextGraphics)
	 */
	@Override
	public void drawBackground(TextGraphics textGraphics)
	{

		textGraphics.applyTheme(Theme.Category.SCREEN_BACKGROUND);

		// Clear the background
		textGraphics.fillRectangle(' ', new TerminalPosition(0, 0), textGraphics.getSize());

		// Write the title
		textGraphics.drawString(3, 0, title);

		// Write memory usage
		if (showMemoryUsage)
		{
			Runtime runtime = Runtime.getRuntime();
			long freeMemory = runtime.freeMemory();
			long totalMemory = runtime.totalMemory();
			long usedMemory = totalMemory - freeMemory;

			usedMemory /= 1024 * 1024;
			totalMemory /= 1024 * 1024;

			String memUsageString = "Memory usage: " + usedMemory + " MB of " + totalMemory + " MB";
			textGraphics.drawString(textGraphics.getSize().getColumns() - memUsageString.length() - 1, textGraphics.getSize().getRows() - 1, memUsageString);
		}
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title)
	{
		if (title == null)
		{
			title = "";
		}

		this.title = title;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * Sets the show memory usage.
	 * 
	 * @param showMemoryUsage
	 *            the new show memory usage
	 */
	public void setShowMemoryUsage(boolean showMemoryUsage)
	{
		this.showMemoryUsage = showMemoryUsage;
	}

	/**
	 * Checks if is showing memory usage.
	 * 
	 * @return true, if is showing memory usage
	 */
	public boolean isShowingMemoryUsage()
	{
		return showMemoryUsage;
	}
}
