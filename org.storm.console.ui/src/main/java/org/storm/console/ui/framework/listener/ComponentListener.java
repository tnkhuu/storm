/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.listener;

import java.awt.event.ComponentEvent;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.component.InteractableComponent;

/**
 * The listener interface for receiving component events. The class that is
 * interested in processing a component event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addComponentListener<code> method. When
 * the component event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see ComponentEvent
 */
public interface ComponentListener
{

	/**
	 * On component invalidated.
	 * 
	 * @param component
	 *            the component
	 */
	void onComponentInvalidated(Component component);

	/**
	 * On component received focus.
	 * 
	 * @param interactableComponent
	 *            the interactable component
	 */
	void onComponentReceivedFocus(InteractableComponent interactableComponent);

	/**
	 * On component lost focus.
	 * 
	 * @param interactableComponent
	 *            the interactable component
	 */
	void onComponentLostFocus(InteractableComponent interactableComponent);
}
