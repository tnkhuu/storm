/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.listener;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.Container;

/**
 * The Class ContainerAdapter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ContainerAdapter implements ContainerListener
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.listener.ContainerListener#onComponentAdded
	 * (org.storm.console.ui.framework.gui.Container,
	 * org.storm.console.ui.framework.gui.Component)
	 */
	@Override
	public void onComponentAdded(Container container, Component component)
	{

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.listener.ContainerListener#onComponentRemoved
	 * (org.storm.console.ui.framework.gui.Container,
	 * org.storm.console.ui.framework.gui.Component)
	 */
	@Override
	public void onComponentRemoved(Container container, Component component)
	{
	}

}
