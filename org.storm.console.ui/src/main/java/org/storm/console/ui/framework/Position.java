/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.console.ui.framework;

/**
 * The Interface Position.
 *
 * @author Trung Khuu
 * @since 1.0
 */
public interface Position
{
	
	

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public int getX();

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(int x);
	
	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public int getY();
	
	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(int y);


}
