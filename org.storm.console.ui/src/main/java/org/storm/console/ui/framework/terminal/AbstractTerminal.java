/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.terminal;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class AbstractTerminal.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class AbstractTerminal implements Terminal
{

	/** The resize listeners. */
	private final List<ResizeListener> resizeListeners;

	/** The last known size. */
	private TerminalSize lastKnownSize;

	/**
	 * Instantiates a new abstract terminal.
	 */
	public AbstractTerminal()
	{
		this.resizeListeners = new ArrayList<ResizeListener>();
		this.lastKnownSize = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.terminal.Terminal#addResizeListener(com.googlecode
	 * .lanterna.terminal.Terminal.ResizeListener)
	 */
	@Override
	public void addResizeListener(ResizeListener listener)
	{
		if (listener != null)
		{
			resizeListeners.add(listener);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.terminal.Terminal#removeResizeListener(com.googlecode
	 * .lanterna.terminal.Terminal.ResizeListener)
	 */
	@Override
	public void removeResizeListener(ResizeListener listener)
	{
		if (listener != null)
		{
			resizeListeners.remove(listener);
		}
	}

	/**
	 * On resized.
	 * 
	 * @param columns
	 *            the columns
	 * @param rows
	 *            the rows
	 */
	protected synchronized void onResized(int columns, int rows)
	{
		TerminalSize newSize = new TerminalSize(columns, rows);
		if (lastKnownSize == null || !lastKnownSize.equals(newSize))
		{
			lastKnownSize = newSize;
			for (ResizeListener resizeListener : resizeListeners)
			{
				resizeListener.onResized(lastKnownSize);
			}
		}
	}

	/**
	 * Gets the last known size.
	 * 
	 * @return the last known size
	 */
	protected TerminalSize getLastKnownSize()
	{
		return lastKnownSize;
	}
}
