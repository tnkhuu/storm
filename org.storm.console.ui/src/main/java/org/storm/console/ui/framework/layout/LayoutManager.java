/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.ui.framework.layout;

import java.util.List;

import org.storm.console.ui.framework.Component;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Interface LayoutManager.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface LayoutManager
{

	/**
	 * Adds the component.
	 * 
	 * @param component
	 *            the component
	 * @param parameters
	 *            the parameters
	 */
	void addComponent(Component component, LayoutParameter... parameters);

	/**
	 * Removes the component.
	 * 
	 * @param component
	 *            the component
	 */
	void removeComponent(Component component);

	/**
	 * Gets the preferred size.
	 * 
	 * @return the preferred size
	 */
	TerminalSize getPreferredSize();

	/**
	 * Layout.
	 * 
	 * @param layoutArea
	 *            the layout area
	 * @return the list<? extends laid out component>
	 */
	List<? extends LaidOutComponent> layout(TerminalSize layoutArea);

	/**
	 * Maximises vertically.
	 * 
	 * @return true, if successful
	 */
	boolean maximisesVertically();

	/**
	 * Maximises horisontally.
	 * 
	 * @return true, if successful
	 */
	boolean maximisesHorisontally();

	/**
	 * The Interface LaidOutComponent.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static interface LaidOutComponent
	{

		/**
		 * Gets the component.
		 * 
		 * @return the component
		 */
		Component getComponent();

		/**
		 * Gets the size.
		 * 
		 * @return the size
		 */
		TerminalSize getSize();

		/**
		 * Gets the top left position.
		 * 
		 * @return the top left position
		 */
		TerminalPosition getTopLeftPosition();
	}
}
