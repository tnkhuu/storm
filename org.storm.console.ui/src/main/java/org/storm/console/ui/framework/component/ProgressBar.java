/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.ui.framework.component;

import org.storm.console.ui.framework.TextGraphics;
import org.storm.console.ui.framework.Theme.Category;
import org.storm.console.ui.framework.terminal.ACS;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.console.ui.framework.terminal.TerminalSize;

/**
 * The Class ProgressBar.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ProgressBar extends AbstractComponent
{

	/** The preferred width. */
	private final int preferredWidth;

	/** The progress. */
	private double progress;

	/** The fill_complete_char. */
	private char fill_complete_char = ACS.BLOCK_SOLID;

	/** The fill_remaining_char. */
	private char fill_remaining_char = ' ';

	/** The show_percentage. */
	private boolean show_percentage = true;

	/**
	 * Instantiates a new progress bar.
	 * 
	 * @param preferredWidth
	 *            the preferred width
	 */
	public ProgressBar(int preferredWidth)
	{
		this.preferredWidth = preferredWidth;
		this.progress = 0.0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.console.ui.framework.gui.component.AbstractComponent#
	 * calculatePreferredSize()
	 */
	@Override
	protected TerminalSize calculatePreferredSize()
	{
		return new TerminalSize(preferredWidth, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.console.ui.framework.gui.Component#repaint(org.storm.console.ui.framework
	 * .gui.TextGraphics)
	 */
	@Override
	public void repaint(TextGraphics graphics)
	{
		int bar_start = isCompletedPercentageShown() ? 5 : 0;
		int total_width = graphics.getWidth() - bar_start;
		int highlighted_blocks = (int) (total_width * progress);

		if (isCompletedPercentageShown())
		{
			graphics.applyTheme(Category.BUTTON_LABEL_INACTIVE);
			Integer percentage = (int) Math.round(progress * 100);
			String perc_str;
			if (percentage == 100)
			{
				perc_str = percentage + "%";
			}
			else if (percentage >= 10)
			{
				perc_str = " " + percentage + "%";
			}
			else
			{
				perc_str = "  " + percentage + "%";
			}

			graphics.drawString(0, 0, perc_str);
		}

		graphics.applyTheme(Category.PROGRESS_BAR_COMPLETED);
		graphics.fillRectangle(fill_complete_char, new TerminalPosition(bar_start, 0), new TerminalSize(bar_start + highlighted_blocks, 1));
		graphics.applyTheme(Category.PROGRESS_BAR_REMAINING);
		graphics.fillRectangle(fill_remaining_char, new TerminalPosition(bar_start + highlighted_blocks, 0), new TerminalSize(total_width - highlighted_blocks, 1));
	}

	/**
	 * Gets the progress.
	 * 
	 * @return the progress
	 */
	public double getProgress()
	{
		return progress;
	}

	/**
	 * Sets the progress.
	 * 
	 * @param progress
	 *            the new progress
	 */
	public void setProgress(double progress)
	{
		this.progress = progress;
		invalidate();
	}

	/**
	 * Sets the complete fill char.
	 * 
	 * @param fill
	 *            the new complete fill char
	 */
	public void setCompleteFillChar(char fill)
	{
		fill_complete_char = fill;
		invalidate();
	}

	/**
	 * Gets the complete fill char.
	 * 
	 * @return the complete fill char
	 */
	public char getCompleteFillChar()
	{
		return fill_complete_char;
	}

	/**
	 * Sets the remaining fill char.
	 * 
	 * @param fill
	 *            the new remaining fill char
	 */
	public void setRemainingFillChar(char fill)
	{
		fill_remaining_char = fill;
		invalidate();
	}

	/**
	 * Gets the remaining fill char.
	 * 
	 * @return the remaining fill char
	 */
	public char getRemainingFillChar()
	{
		return fill_remaining_char;
	}

	/**
	 * Sets the completed percentage shown.
	 * 
	 * @param flag
	 *            the new completed percentage shown
	 */
	public void setCompletedPercentageShown(boolean flag)
	{
		show_percentage = flag;
		invalidate();
	}

	/**
	 * Checks if is completed percentage shown.
	 * 
	 * @return true, if is completed percentage shown
	 */
	public boolean isCompletedPercentageShown()
	{
		return show_percentage;
	}

}
