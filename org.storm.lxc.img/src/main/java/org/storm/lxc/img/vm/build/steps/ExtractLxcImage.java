/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package org.storm.lxc.img.vm.build.steps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.compress.archivers.ArchiveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.vm.build.BuildContext;
import org.storm.api.services.vm.build.Goal;
import org.storm.api.services.vm.build.Step;
import org.storm.lxc.img.vm.build.BuildConstants;
import org.storm.tools.compression.Extract;

/**
 * A chain on step to the {@link DownloadLxcImages} step that Extracts a the
 * archive to the configured directory.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ExtractLxcImage implements Step, BuildConstants
{
	private static final Logger s_log = LoggerFactory.getLogger(ExtractLxcImage.class);
	/** The directory. */
	private File directory;

	/**
	 * Instantiates a new extract lxc image.
	 * 
	 * @param directory
	 *            the directory
	 */
	public ExtractLxcImage(File directory)
	{
		this.directory = new File(SetupEnvironment.STORM_HOME + "/work");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.lxc.img.vm.build.Step#getName()
	 */
	@Override
	public String getName()
	{
		return "Extracting Lxc Image";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.lxc.img.vm.build.Step#execute(org.storm.lxc.img.vm.build.
	 * ConfigContext, org.storm.lxc.img.vm.build.Result)
	 */
	@Override
	public boolean execute(BuildContext context, Goal<?> result)
	{
		boolean success = false;
		try
		{
			if (!directory.exists())
			{
				directory.mkdirs();
				String downloadedImage = (String) context.get(STORM_DOWNLOADED_IMAGE_PATH);
				Extract.targz(new File(downloadedImage), directory);

				success = true;
			}
			else
			{
				success = true;
			}
			context.put(STORM_EXTRACTED_IMAGE_PATH, directory.getAbsolutePath());
		}
		catch (ArchiveException e)
		{
			s_log.error(e.getMessage(), e);
		}
		catch (FileNotFoundException e)
		{
			s_log.error(e.getMessage(), e);
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
		return success;
	}

}
