/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package org.storm.lxc.img.vm.build.steps;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.compress.archivers.ArchiveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.vm.build.BuildContext;
import org.storm.api.services.vm.build.Goal;
import org.storm.api.services.vm.build.Step;
import org.storm.lxc.img.vm.build.BuildConstants;
import org.storm.tools.compression.Extract;
import org.storm.tools.http.Download;

/**
 * Inject the storm platform specific flavor/bundle configurations into the
 * downloaded lxc image and serve it.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unused")
public class RepackageLxcImages implements Step, BuildConstants
{
	private static final Logger s_log = LoggerFactory.getLogger(RepackageLxcImages.class);
	private static final String LXC_JAVA_DIR = "/opt/java";
	private static final String LXC_TMP_DOWNLOAD_JAVA_DIR = "/tmp/java/";
	private static final String LXC_TMP_DOWNLOAD_JAVA_FILE = "/java7.tar.gz";
	private URL spec;

	public RepackageLxcImages(URL spec)
	{
		this.spec = spec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.vm.build.Step#execute(org.storm.api.services.vm
	 * .build.ConfigContext, org.storm.api.services.vm.build.Result)
	 */
	@Override
	public boolean execute(BuildContext context, Goal<?> result)
	{
		boolean success = false;

		String extractedPath = (String) context.get(STORM_EXTRACTED_IMAGE_PATH);
		File dir = new File(extractedPath + LXC_TMP_DOWNLOAD_JAVA_DIR);
		if (!dir.exists())
		{
			dir.mkdirs();

		}
		if (!new File(dir + LXC_TMP_DOWNLOAD_JAVA_FILE).exists())
		{
			downloadJava(new File(dir + LXC_TMP_DOWNLOAD_JAVA_FILE));
		}
		if (!new File(extractedPath + LXC_JAVA_DIR).exists())
		{
			new File(extractedPath + LXC_JAVA_DIR).mkdirs();
		}

		try
		{
			Extract.targz(new File(dir + LXC_TMP_DOWNLOAD_JAVA_FILE), new File(extractedPath + LXC_JAVA_DIR));
		}
		catch (IOException | ArchiveException e)
		{
			e.printStackTrace();
		}
		// downloadSpec(spec);
		success = true;

		return success;
	}

	@Override
	public String getName()
	{
		return "Customizing Image Distribution";
	}

	private void downloadJava(File saveDir)
	{
		try
		{
			Download.file(new URL("http://javadl.sun.com/webapps/download/AutoDL?BundleId=75252"), saveDir, 4);
			System.out.println("Complete");
		}
		catch (MalformedURLException e)
		{
			s_log.error(e.getMessage(), e);
			throw new IllegalStateException(e);
		}
	}

	private void downloadSpec(URL spec)
	{
		Download.file(spec, new File(""), 1);
	}

	public static void main(String[] args)
	{
		new RepackageLxcImages(null).downloadJava(new File("/tmp/jre-7-linux-x64.tar.gz"));
	}
}
