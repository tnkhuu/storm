/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.img;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.storm.api.factories.ImageFactory;
import org.storm.api.services.vm.build.Result;
import org.storm.lxc.img.vm.build.BuildConstants;
import org.storm.lxc.img.vm.build.LxcConfiguration;
import org.storm.lxc.img.vm.build.steps.ConfigureLxcNetwork;
import org.storm.lxc.img.vm.build.steps.DownloadLxcImages;
import org.storm.lxc.img.vm.build.steps.ExtractLxcImage;
import org.storm.lxc.img.vm.build.steps.RepackageLxcImages;
import org.storm.lxc.img.vm.build.steps.SetupEnvironment;
import org.storm.lxc.img.vm.build.steps.StartLxcInstance;
import org.storm.lxc.img.vm.build.steps.VerifyLxcInstance;

/**
 * A factory for creating storm containers backed by LXC.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class LxcImageFactory implements ImageFactory<Result>, BuildConstants
{

	/** The exec. */
	private ExecutorService exec = Executors.newCachedThreadPool();

	/** The properties. */
	private final Properties properties;

	private static final String LXC_WORK_DIR = "/work";

	/**
	 * Instantiates a new lxc image factory.
	 * 
	 * @param properties
	 *            the properties
	 */
	protected LxcImageFactory(Properties properties)
	{
		this.properties = properties;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.factories.ImageFactory#createImage(org.storm.api.factories
	 * .ImageFactory.Distribution, org.storm.api.services.vm.Spec, java.io.File)
	 */
	@Override
	public Future<Result> createImage(Distribution distributionType, URL specification, String createdImage)
	{
		LxcConfiguration config = new LxcConfiguration.Builder(createdImage).addStep(new SetupEnvironment(properties)).addStep(new DownloadLxcImages(distributionType))
				.addStep(new ExtractLxcImage(new File(properties.getProperty(STORM_CONFIG_HOME) + LXC_WORK_DIR))).addStep(new RepackageLxcImages(specification))
				.addStep(new ConfigureLxcNetwork()).addStep(new StartLxcInstance()).addStep(new VerifyLxcInstance()).build();
		return exec.submit(config);

	}

	public static void main(String[] args) throws MalformedURLException, IOException
	{
		Properties prop = new Properties();
		prop.load(new URL("file:///home/tkhuu/dev/src/java/storm/org.storm.lxc.img/src/main/resources/images.properties").openStream());
		prop.load(new URL("file:///home/tkhuu/dev/src/java/storm/org.storm.lxc.img/src/main/resources/sources.properties").openStream());
		new LxcImageFactory(prop).createImage(Distribution.UBUNTU, new URL("http://www.google.com"), "/tmp/out.tar");
	}

}
