/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.img;

import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.storm.api.factories.ImageFactory;
import org.storm.api.services.DistributedService;
import org.storm.api.services.vm.build.Result;

/**
 * The Class Activator.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Activator implements BundleActivator
{
	private static final String SOURCES = "sources.properties";

	private static final String IMAGES = "images.properties";

	private ImageFactory<Result> imageFactory;

	@SuppressWarnings("rawtypes")
	private ServiceRegistration serviceReg;

	@Override
	public void start(BundleContext context) throws Exception
	{
		URL sources = context.getBundle().getResource(SOURCES);
		URL images = context.getBundle().getResource(IMAGES);
		Properties props = new Properties();
		props.load(sources.openStream());
		props.load(images.openStream());

		imageFactory = new LxcImageFactory(props);
		serviceReg = context.registerService(new String[] { ImageFactory.class.getName(), DistributedService.class.getName() }, imageFactory, new Hashtable<String, Object>());

	}

	@Override
	public void stop(BundleContext context) throws Exception
	{

		context.ungetService(serviceReg.getReference());
	}

}
