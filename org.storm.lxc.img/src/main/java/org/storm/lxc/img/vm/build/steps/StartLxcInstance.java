/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.img.vm.build.steps;

import org.storm.api.services.vm.build.BuildContext;
import org.storm.api.services.vm.build.Goal;
import org.storm.api.services.vm.build.Step;

/**
 * Boot the newly built container.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StartLxcInstance implements Step
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.vm.build.Step#execute(org.storm.api.services.vm
	 * .build.ConfigContext, org.storm.api.services.vm.build.Result)
	 */
	@Override
	public boolean execute(BuildContext context, Goal<?> result)
	{
		return true;
	}

	@Override
	public String getName()
	{
		return "Booting Container Instance";
	}

}
