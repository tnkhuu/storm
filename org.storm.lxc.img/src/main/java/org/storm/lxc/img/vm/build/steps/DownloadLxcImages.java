/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package org.storm.lxc.img.vm.build.steps;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.factories.ImageFactory.Distribution;
import org.storm.api.services.vm.build.BuildContext;
import org.storm.api.services.vm.build.Goal;
import org.storm.api.services.vm.build.Step;
import org.storm.lxc.img.vm.build.BuildConstants;
import org.storm.tools.http.Download;

/**
 * Downloads the appropiate images the storm platform is going to use to support
 * lxc.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DownloadLxcImages implements Step, BuildConstants
{
	private static Logger s_log = LoggerFactory.getLogger(DownloadLxcImages.class);
	private static int CONNECTIONS = 4;
	private final Distribution distribution;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.vm.build.Step#execute(org.storm.api.services.vm
	 * .build.ConfigContext, org.storm.api.services.vm.build.Result)
	 */

	public DownloadLxcImages(Distribution distribution)
	{
		this.distribution = distribution;
	}

	@Override
	public boolean execute(BuildContext context, Goal<?> result)
	{
		boolean success = false;
		try
		{
			String destlxcTarball = SetupEnvironment.STORM_CONFIG_IMG_CACHE;
			String fileName = (String) context.get(CLOUD_IMG_RELEASE_NAME);
			String url = (String) context.get(distribution.name().toString());

			String targetDir = destlxcTarball + "/" + fileName;
			if (!new File(targetDir).exists())
			{
				s_log.info("Downloading... {} to {}", url, targetDir);
				Download.file(new URL(url), new File(targetDir), CONNECTIONS);

				s_log.info("Downloaded {} to {}", url, targetDir);
			}
			context.put(STORM_DOWNLOADED_IMAGE_PATH, targetDir);
			success = true;
		}
		catch (MalformedURLException e)
		{
			s_log.error(e.getMessage(), e);
		}
		finally
		{
			if (success)
			{
				s_log.info("Download Complete.");
			}
		}
		return success;
	}

	@Override
	public String getName()
	{
		return "Image Distribution Download";
	}
}
