/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.img.vm.build;

import java.util.LinkedList;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.vm.build.BuildContext;
import org.storm.api.services.vm.build.Result;
import org.storm.api.services.vm.build.Step;

/**
 * A sequence of steps that are used to build a LXC container.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class LxcConfiguration implements Callable<Result>, BuildConstants
{
	private final static Logger s_log = LoggerFactory.getLogger(LxcConfiguration.class);
	private final LinkedList<Step> steps;
	private final BuildContext context;
	private Result result;

	private LxcConfiguration(Builder builder)
	{
		this.steps = builder.steps;
		this.context = builder.context;
	}

	public static class Builder
	{

		private LinkedList<Step> steps;
		private BuildContext context;

		public Builder(String buildId)
		{
			steps = new LinkedList<>();
			;
			context = new BuildContext();
			context.put(STORM_LXC_IMAGE_LOC, buildId);
		}

		public Builder addStep(Step configStep)
		{
			steps.add(configStep);
			return this;
		}

		public Builder addContext(String key, Object value)
		{
			context.put(null, value);
			return this;
		}

		public LxcConfiguration build()
		{
			return new LxcConfiguration(this);
		}

	}

	@Override
	public Result call() throws Exception
	{
		if (steps.isEmpty())
		{
			throw new IllegalStateException("No steps to execute.");
		}
		int currentStep = 1;
		for (Step step = steps.poll(); !steps.isEmpty(); step = steps.poll())
		{
			s_log.info("Running step {} of {} : {}", currentStep, steps.size(), step.getName());
			if (step.execute(context, result))
			{
				currentStep++;
				continue;
			}
			else
			{
				s_log.error("Failed on step {} : {}", currentStep, step.getName());
				break;
			}

		}
		return result;
	}
}
