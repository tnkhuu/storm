/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lxc.img.vm.build;
/**
 * The Build Constants
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface BuildConstants
{
	String USER_HOME_BASE = "user.home";
	String TMP_DIR = "java.io.tmpdir";
	String STORM_LXC_IMAGE_LOC = "storm.build.lxc.image.loc";
	String STORM_EXTRACTED_IMAGE_PATH = "storm.extracted.image.path";
	String STORM_CONFIG_HOME = "storm.config.home";
	String STORM_DOWNLOADED_IMAGE_PATH = "storm.image.path";
	String STORM_LXC_URL_IMG_CACHE = "storm.lxc.image.url.cache";
	String STORM_LXC_IMG_CACHE = "storm.lxc.image.cache";
	String STORM_LXC_URL_IMAGE_FLAVORS = "storm.lxc.image.flavors";
	String STORM_LXC_CONFIGURED_FLAVOR_TYPE = "storm.configured.flavor.type";

	String CLOUD_IMG_REPO = "lxc.cloud.image.repository";
	String CLOUD_IMG_RELEASE = "lxc.cloud.image.release";
	String CLOUD_IMG_RELEASE_DATE = "lxc.cloud.image.release.date";
	String CLOUD_IMG_RELEASE_NAME = "lxc.cloud.image.name";
	/** The file protocol. */
	String FILE_PROTOCOL = "file:///";

}
