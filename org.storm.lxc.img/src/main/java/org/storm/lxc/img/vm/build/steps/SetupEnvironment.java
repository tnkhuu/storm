/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package org.storm.lxc.img.vm.build.steps;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.vm.build.BuildContext;
import org.storm.api.services.vm.build.Goal;
import org.storm.api.services.vm.build.Step;
import org.storm.lxc.img.vm.build.BuildConstants;

/**
 * Sets up the initial environment for the storm platform.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SetupEnvironment implements Step, BuildConstants
{

	/** The s_log. */
	private static Logger s_log = LoggerFactory.getLogger(SetupEnvironment.class);

	/** The user home. */
	private static String USER_HOME = System.getProperty(USER_HOME_BASE);

	/** The storm home. */
	public static String STORM_HOME = USER_HOME + "/.storm";

	/** The storm config img cache. */
	public static String STORM_CONFIG_IMG_CACHE = STORM_HOME + "/cache";

	/** The storm config flavours. */
	private static String STORM_CONFIG_FLAVOURS = STORM_HOME + "/flavors";

	private Properties properties;

	public SetupEnvironment(Properties properties)
	{
		this.properties = properties;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.vm.build.Step#execute(org.storm.api.services.vm
	 * .build.ConfigContext, org.storm.api.services.vm.build.Result)
	 */
	@Override
	public boolean execute(BuildContext context, Goal<?> result)
	{
		boolean success = false;

		try
		{
			loadConfigContext(context);
			setupVariables(context);
			prepareDirectories(context);
			success = true;
		}
		catch (URISyntaxException e)
		{
			s_log.error(e.getMessage(), e);
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
		return success;
	}

	private void loadConfigContext(BuildContext context)
	{
		Set<String> keys = properties.stringPropertyNames();
		Iterator<String> propKeys = keys.iterator();
		while (propKeys.hasNext())
		{
			String key = propKeys.next();
			context.put(key, properties.get(key));
		}
	}

	/**
	 * Prepare directories.
	 * 
	 * @param context
	 *            the context
	 * @throws URISyntaxException
	 *             the uRI syntax exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void prepareDirectories(BuildContext context) throws URISyntaxException, IOException
	{
		if (!new File(STORM_CONFIG_IMG_CACHE).exists())
		{
			new File(STORM_CONFIG_IMG_CACHE).mkdirs();
		}
	}

	/**
	 * Sets the up variables.
	 * 
	 * @param context
	 *            the new up variables
	 */
	private void setupVariables(BuildContext context)
	{
		context.put(USER_HOME_BASE, USER_HOME);
		context.put(STORM_CONFIG_HOME, STORM_HOME);
		context.put(STORM_LXC_URL_IMAGE_FLAVORS, STORM_CONFIG_FLAVOURS);
		context.put(STORM_LXC_URL_IMG_CACHE, FILE_PROTOCOL + STORM_CONFIG_IMG_CACHE);
		context.put(STORM_LXC_IMG_CACHE, STORM_CONFIG_IMG_CACHE);
		s_log.info("LXC environment variables.");
		s_log.info(" [ user home  : {} ]", USER_HOME);
		s_log.info(" [ image cache: {} ]", STORM_CONFIG_IMG_CACHE);
		s_log.info(" [ config home: {} ]", STORM_HOME);
		s_log.info(" [ flavor home: {} ]", STORM_CONFIG_FLAVOURS);
	}

	@Override
	public String getName()
	{
		return "Environment Setup";
	}

}
