/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.io;

import java.util.Timer;
import java.util.TimerTask;

/**
 * The Class ProcessWatcher.
 */
public class ProcessWatcher
{

	private long timeout;
	private Process process;
	private boolean timedOut;
	private Timer timer;

	/**
	 * Instantiates a new process watcher.
	 *
	 * @param process the process
	 * @param timeout the timeout
	 */
	public ProcessWatcher(Process process, long timeout)
	{
		this.timeout = timeout;
		this.process = process;
	}

	/**
	 * Start.
	 */
	public void start()
	{
		timer = new Timer();
		timer.schedule(new TimerTask()
		{
			public void run()
			{
				process.destroy();
				timedOut = true;
			}
		}, timeout);
	}

	/**
	 * Cancel.
	 */
	public void cancel()
	{
		timer.cancel();
	}

	/**
	 * Timed out.
	 *
	 * @return true, if successful
	 */
	public boolean timedOut()
	{
		return timedOut;
	}

}
