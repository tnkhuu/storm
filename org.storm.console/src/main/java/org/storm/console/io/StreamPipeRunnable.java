/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * The Class StreamPipeRunnable.
 */
public class StreamPipeRunnable implements PipeRunnable
{

	private OutputStream out;
	private InputStream in;

	/**
	 * Instantiates a new stream pipe runnable.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public StreamPipeRunnable(InputStream in, OutputStream out)
	{
		this.out = out;
		this.in = in;
	}

	/**
	 *
	 *
	 */
	public void run() throws IOException
	{
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = in.read(buffer)) > -1)
		{
			out.write(buffer, 0, bytesRead);
		}
	}

}
