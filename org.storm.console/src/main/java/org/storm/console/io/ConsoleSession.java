/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.io;

import org.storm.console.command.CommandExecutor;
import org.storm.console.network.Namespaced;

/**
 * The Class ConsoleSession.
 */
public class ConsoleSession implements Namespaced
{

	private final String name;
	private final int id;
	private final CommandExecutor executor;
	
	public ConsoleSession(String name, int id)
	{
		this.name = name;
		this.id = id;
		this.executor = new CommandExecutor("/bin/bash");
	}

	/**
	 * Return the name of this name space.
	 */
	@Override
	public String getName()
	{
		return name;
	}

	/**
	 * Return the ID of this name space.
	 */
	@Override
	public int getProcessID()
	{
		return id;
	}

	/**
	 * @return the executor
	 */
	public CommandExecutor getExecutor()
	{
		return executor;
	}

}
