/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.command.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.storm.console.io.PipeRunnable;

/**
 * The Class LinePipeRunnable.
 */
public class LinePipeRunnable implements PipeRunnable
{

	private InputStream in;
	private LineHandler out;

	/**
	 * Instantiates a new line pipe runnable.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public LinePipeRunnable(InputStream in, LineHandler out)
	{
		this.in = in;
		this.out = out;
	}

	/**
	 *
	 *
	 */
	public void run() throws IOException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		String line;
		while ((line = reader.readLine()) != null)
		{
			out.handleLine(line);
		}
	}

}
