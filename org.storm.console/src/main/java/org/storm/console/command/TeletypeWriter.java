package org.storm.console.command;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TeletypeWriter
{
	private static final Executor exec = Executors.newCachedThreadPool();

	/**
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, InterruptedException
	{
		final Process p = Runtime.getRuntime().exec("/bin/sh");

		final InputStream processOutput = p.getInputStream();
		final OutputStream processInput = p.getOutputStream();

	/*	Signal.handle(new Signal("TERM"), new SignalHandler()
		{
			public void handle(Signal signal)
			{
				try
				{
					int signum = signal.getNumber();
					System.out.println("About to shutdown... sig " + signum + " received");
					Thread.sleep(5000);

					System.exit(0);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});*/

		exec.execute(new Runnable()
		{

			@Override
			public void run()
			{
				@SuppressWarnings("resource")
				Scanner sc = new Scanner(System.in);
				while (sc.hasNextLine())
				{
					String cmd = sc.nextLine();
					//System.out.println(cmd);
					try
					{
						processInput.write(cmd.getBytes());
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}
		});

		exec.execute(new Runnable()
		{
			@Override
			public void run()
			{
				while (true)
				{
					try
					{
						int avail = processOutput.available();
						if (avail > 0)
						{
							byte[] res = new byte[avail];
							processOutput.read(res, 0, avail);
							System.out.println(new String(res));
						}
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});

		Thread.currentThread().join();

	}
}
