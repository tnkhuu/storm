/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.console.command;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.storm.console.command.handler.LineHandler;
import org.storm.console.command.handler.LinePipeRunnable;
import org.storm.console.io.Pipe;
import org.storm.console.io.PipeRunnable;
import org.storm.console.io.ProcessWatcher;
import org.storm.console.io.StreamPipeRunnable;

/**
 * The Class CommandExecutor.
 */
public class CommandExecutor
{

	private List<String> cmd;
	private String standardErrorText;
	private int returnCode;
	private Long timeout;
	private String workingDirectory;

	
	/**
	 * Instantiates a new command executor.
	 *
	 * @param cmd the cmd
	 */
	public CommandExecutor(String shell)
	{
		cmd = new ArrayList<>();
		cmd.add(shell);
	}
	/**
	 * Instantiates a new command executor.
	 *
	 * @param cmd the cmd
	 */
	public CommandExecutor(List<String> cmd)
	{
		this(cmd, null, null);
	}

	/**
	 * Instantiates a new command executor.
	 *
	 * @param cmd the cmd
	 * @param timeout the timeout
	 */
	public CommandExecutor(List<String> cmd, Long timeout)
	{
		this(cmd, timeout, null);
	}

	/**
	 * Instantiates a new command executor.
	 *
	 * @param cmd the cmd
	 * @param workingDirectory the working directory
	 */
	public CommandExecutor(List<String> cmd, String workingDirectory)
	{
		this(cmd, null, workingDirectory);
	}

	/**
	 * Instantiates a new command executor.
	 *
	 * @param cmd the cmd
	 * @param timeout the timeout
	 * @param workingDirectory the working directory
	 */
	public CommandExecutor(List<String> cmd, Long timeout, String workingDirectory)
	{
		this.cmd = cmd;
		this.timeout = timeout;
		this.workingDirectory = workingDirectory;
	}

	/**
	 * Run.
	 *
	 * @param outputStream the output stream
	 */
	public void run(OutputStream outputStream)
	{
		try
		{
			Process process = launchProcess();
			captureOutput(process, new StreamPipeRunnable(process.getInputStream(), outputStream));
		}
		catch (IOException ioEx)
		{
			throw new CommandExecutorException("Command execution failed unexpectedly!", ioEx);
		}
	}

	/**
	 * Run.
	 *
	 * @param out the out
	 */
	public void run(LineHandler out)
	{
		try
		{
			Process process = launchProcess();
		
			captureOutput(process, new LinePipeRunnable(process.getInputStream(), out));
		}
		catch (IOException ioEx)
		{
			throw new CommandExecutorException("Command execution failed unexpectedly!", ioEx);
		}
	}

	/**
	 * Launch process.
	 *
	 * @return the process
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private Process launchProcess() throws IOException
	{
		ProcessBuilder processBuilder = new ProcessBuilder(cmd);
		if (workingDirectory != null)
		{
			processBuilder.directory(new File(workingDirectory));
		}
		return processBuilder.start();
	}

	/**
	 * Run.
	 *
	 * @return the string
	 */
	public String run()
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		run(out);
		try
		{
			return out.toString("UTF-8");
		}
		catch (UnsupportedEncodingException ex)
		{
			return out.toString();
		}
	}

	/**
	 * Standard error text.
	 *
	 * @return the string
	 */
	public String standardErrorText()
	{
		return standardErrorText;
	}

	/**
	 * Return code.
	 *
	 * @return the int
	 */
	public int returnCode()
	{
		return returnCode;
	}

	/**
	 * Checks if is error.
	 *
	 * @return true, if is error
	 */
	public boolean isError()
	{
		return returnCode != 0;
	}

	/**
	 * Capture output.
	 *
	 * @param process the process
	 * @param pipeRunnable the pipe runnable
	 */
	private void captureOutput(Process process, PipeRunnable pipeRunnable)
	{
		try
		{
			Pipe pipe = new Pipe(pipeRunnable);
			ByteArrayOutputStream errorOutput = new ByteArrayOutputStream();
			StreamPipeRunnable errorPipeRunnable = new StreamPipeRunnable(process.getErrorStream(), errorOutput);
			Pipe errorPipe = new Pipe(errorPipeRunnable);

			ProcessWatcher watcher = null;
			if (timeout != null)
			{
				watcher = new ProcessWatcher(process, timeout);
				watcher.start();
			}

			pipe.start();
			errorPipe.start();

			pipe.join();
			errorPipe.join();
			returnCode = process.waitFor();

			if (watcher != null)
			{
				if (watcher.timedOut())
				{
					throw new CommandExecutorException("Command (" + cmd + ") timed out (" + timeout + " msecs)!");
				}
				else
				{
					watcher.cancel();
				}
			}

			process.destroy();

			standardErrorText = errorOutput.toString();

		}
		catch (InterruptedException intEx)
		{
			throw new CommandExecutorException("Command execution failed unexpectedly!", intEx);
		}
	}
}
