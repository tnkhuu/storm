/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.command.handler;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * The Class TestLineHandler.
 */
public class TestLineHandler implements LineHandler {
  private List<String> lines = new ArrayList<String>();

  /**
   *
   *
   */
  public void handleLine(String line) {
    lines.add(line);
  }

  /**
   * Assert lines equal.
   *
   * @param expectedLines the expected lines
   */
  public void assertLinesEqual(List<String> expectedLines) {
    assertThat(lines, equalTo(expectedLines));
  }
}