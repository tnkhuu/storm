/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.io;

import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class PipeTest {

  @Test
  public void runsPipeRunnable() throws Exception {
    TestPipeRunnable runnable = new TestPipeRunnable();
    Pipe pipe = new Pipe(runnable);
    pipe.start();
    pipe.join();
    runnable.assertRunExecuted();
  }

  /**
   * Captures io exception.
   *
   * @throws Exception the exception
   */
  @Test
  public void capturesIOException() throws Exception {
    final IOException expectedException = new IOException("Fake IO Exception!");
    PipeRunnable runnable = new PipeRunnable() {
      public void run() throws IOException {
        throw expectedException;
      }
    };
    Pipe pipe = new Pipe(runnable);
    pipe.start();
    pipe.join();

    assertThat(pipe.isException(), equalTo(true));
    assertThat(pipe.exception(), equalTo(expectedException));
  }

  class TestPipeRunnable implements PipeRunnable {

    private boolean runExecuted = false;

    public void run() throws IOException {
      runExecuted = true;
    }

    public void assertRunExecuted() {
      if (!runExecuted) {
        fail("run() did not execute!");
      }
    }
  }
}
