/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.io;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * The Class ProcessWatcherTest.
 */
public class ProcessWatcherTest {

  /**
   * Destroys process if execution exceeds timeout.
   *
   * @throws Exception the exception
   */
  @Test
  public void destroysProcessIfExecutionExceedsTimeout() throws Exception {
    String[] cmd = TestProgram.cmdFor(new String[]{"sleep", "20000"});
    Process process = Runtime.getRuntime().exec(cmd);
    ProcessWatcher watcher = new ProcessWatcher(process, 100L);
    watcher.start();
    int returnCode = process.waitFor();

    assertThat(returnCode, not(equalTo(0)));
    assertThat(watcher.timedOut(), equalTo(true));
  }

  /**
   * Watcher can be canceled.
   *
   * @throws Exception the exception
   */
  @Test
  public void watcherCanBeCanceled() throws Exception {
    String[] cmd = TestProgram.cmdFor(new String[]{"sleep", "200"});
    Process process = Runtime.getRuntime().exec(cmd);
    ProcessWatcher watcher = new ProcessWatcher(process, 50L);
    watcher.start();
    watcher.cancel();
    int returnCode = process.waitFor();

    assertThat(returnCode, equalTo(0));
  }
}
