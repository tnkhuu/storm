/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.console.io;

import java.io.OutputStreamWriter;

/**
 * The Class TestProgram.
 */
public class TestProgram {

  /**
   * Cmd for.
   *
   * @param args the args
   * @return the string[]
   */
  public static String[] cmdFor(String[] args) {
    String[] cmd = new String[args.length + 4];
    cmd[0] = "java";
    cmd[1] = "-cp";
    cmd[2] = System.getProperty("java.class.path");
    cmd[3] = TestProgram.class.getName();
    for (int i = 4; i < cmd.length; i++) {
      cmd[i] = args[i - 4];
    }
    return cmd;
  }

  /**
   * The main method.
   *
   * @param args the arguments
   * @throws Exception the exception
   */
  public static void main(String[] args) throws Exception {
    if (args[0].equals("sleep")) {
      Thread.sleep(Integer.valueOf(args[1]));
    } else if (args[0].equals("echo")) {
      OutputStreamWriter writer = new OutputStreamWriter(System.out, "UTF-8");
      writer.write(args[1]);
      writer.flush();
    } else if (args[0].equals("error")) {
      OutputStreamWriter writer = new OutputStreamWriter(System.err, "UTF-8");
      writer.write(args[1]);
      writer.flush();
      System.exit(Integer.valueOf(args[2]));
    } else if (args[0].equals("chinese")) {
      OutputStreamWriter writer = new OutputStreamWriter(System.out, "UTF-8");
      writer.write("这是中文");
      writer.flush();
    } else if (args[0].equals("pwd")) {
      OutputStreamWriter writer = new OutputStreamWriter(System.out, "UTF-8");
      writer.write(System.getProperty("user.dir"));
      writer.flush();
    }
  }
}
