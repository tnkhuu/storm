/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.msgpack.template;

import java.io.IOException;
import java.net.URL;

import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.unpacker.Unpacker;

/**
 * The Class URLTemplate.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class URLTemplate extends AbstractTemplate<URL>
{

	/**
	 * Instantiates a new uRL template.
	 */
	private URLTemplate()
	{

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msgpack.template.Template#write(org.msgpack.packer.Packer,
	 * java.lang.Object, boolean)
	 */
	@Override
	public void write(Packer pk, URL target, boolean required) throws IOException
	{
		if (target == null)
		{
			if (required)
			{
				throw new MessageTypeException("Attempted to write null");
			}
			pk.writeNil();
			return;
		}
		pk.write(target.toString());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msgpack.template.Template#read(org.msgpack.unpacker.Unpacker,
	 * java.lang.Object, boolean)
	 */
	@Override
	public URL read(Unpacker u, URL to, boolean required) throws IOException
	{
		if (!required && u.trySkipNil())
		{
			return null;
		}
		return new URL(u.readString());
	}

	/**
	 * Gets the single instance of URLTemplate.
	 * 
	 * @return single instance of URLTemplate
	 */
	static public URLTemplate getInstance()
	{
		return instance;
	}

	/** The Constant instance. */
	static final URLTemplate instance = new URLTemplate();

}
