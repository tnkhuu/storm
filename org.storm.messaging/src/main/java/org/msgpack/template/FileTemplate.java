/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.msgpack.template;

import java.io.File;
import java.io.IOException;

import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.unpacker.Unpacker;

/**
 * The Class FileTemplate.
 */
public class FileTemplate extends AbstractTemplate<File>
{

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#write(org.msgpack.packer.Packer, java.lang.Object, boolean)
	 */
	@Override
	public void write(Packer pk, File target, boolean required) throws IOException
	{
		if (target == null)
		{
			if (required)
			{
				throw new MessageTypeException("Attempted to write null");
			}
			pk.writeNil();
			
			return;
		}
		
	}

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#read(org.msgpack.unpacker.Unpacker, java.lang.Object, boolean)
	 */
	@Override
	public File read(Unpacker u, File to, boolean required) throws IOException
	{
		if (!required && u.trySkipNil())
		{
			return null;
		}
		return null;
	}
	
	/**
	 * Gets the single instance of FileTemplate.
	 * 
	 * @return single instance of FileTemplate
	 */
	static public FileTemplate getInstance()
	{
		return instance;
	}

	/** The Constant instance. */
	static final FileTemplate instance = new FileTemplate();
}
