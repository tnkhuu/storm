/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.shell.engine.processor;

/**
 * The shell execution result and directive.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Result
{
	
	Result CONTINUE = new ContinuationResult();
	Result STOP = new StopProcessingResult();
	Result FORWARD = new ForwardingResult();
	Result PRINT = new PrintKeyResult();
	Result SILENT = new NoOutputResult();
	
	public static enum Directive {
		CONTINUE, STOP, FORWARD, SILENT, PRINT
	}
	
	Directive getDirective();
}
