/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.shell.engine.processor;

import org.storm.console.ui.framework.input.Key;
import org.storm.shell.engine.Keys;
import org.storm.shell.engine.processor.InputKeyProcessorChain.KeyProcessor;

/**
 * The default key processor.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StandardKeyProcessor extends KeyProcessor implements Keys
{

	

	@Override
	public boolean canHandle(Key key, Session session)
	{
		boolean canHandle = false;
	
		switch(key.getKind()) {
		case Backspace:
		case End:
		case Delete:
		case PageDown:
		case PageUp:
		case Tab:
			canHandle = true;
		default:
			break;
		}
		
	
		return canHandle;
	}

	/* (non-Javadoc)
	 * @see org.storm.shell.engine.processor.KeyProcessor#handle(org.storm.console.ui.framework.input.Key, org.storm.shell.engine.processor.Session)
	 */
	@Override
	public Result handle(Key key, Session session)
	{
		Result res = Result.CONTINUE;		
		switch(key.getKind()) {
		case Backspace:
			session.getTerminal().deleteLeft();
			session.deleteFromBuffer();
			res = Result.SILENT;
			break;
		case End:
		case Delete:
		case PageDown:
		case PageUp:
		case Tab:
		default:
			break;
		}
		
	
		return res;
		
	
	}

}
