/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.shell.engine.processor;

import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.console.ui.framework.terminal.TerminalPosition;
import org.storm.shell.engine.processor.InputKeyProcessorChain.KeyProcessor;

/**
 * The Class ArrowKeyProcessor.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ArrowKeyProcessor extends KeyProcessor
{


	/* (non-Javadoc)
	 * @see org.storm.shell.engine.processor.KeyProcessor#canHandle(org.storm.console.ui.framework.input.Key, org.storm.shell.engine.processor.Session)
	 */
	@Override
	public boolean canHandle(Key key, Session session)
	{
		switch(key.getKind()) {
		case ArrowDown:
		case ArrowLeft:
		case ArrowRight:
		case ArrowUp:
			return true;
			default : 
				return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.shell.engine.processor.KeyProcessor#handle(org.storm.console.ui.framework.input.Key, org.storm.shell.engine.processor.Session)
	 */
	@Override
	public Result handle(Key key, Session session)
	{
		Screen screen = session.getScreen();
		Terminal term = session.getTerminal();
		TerminalPosition pos = screen.getCursorPosition();
		switch(key.getKind()) {
		case ArrowDown:
			break;
		case ArrowLeft:
			if(pos.getX() > 0) {
				term.moveCursor(pos.getX() - 1, pos.getY());
				screen.setCursorPosition(pos.getY(), pos.getX() - 1);
			}
			break;
		case ArrowRight:
			if(pos.getX() < screen.getTerminalSize().getColumns()) {
				term.moveCursor(pos.getX() + 1, pos.getY());
				screen.setCursorPosition(pos.getY(), pos.getX() + 1);
			}
			break;
		case ArrowUp:
			break;
		default : 
			break;
		}
		return Result.STOP;
	}

}
