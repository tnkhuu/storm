/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.shell.engine.processor;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.storm.api.foundation.Visitor;
import org.storm.console.ui.framework.input.Key;
import org.storm.console.ui.framework.input.Kind;
import org.storm.console.ui.framework.terminal.Terminal;
import org.storm.lxc.terminal.Console;
import org.storm.shell.engine.Keys;
import org.storm.shell.engine.processor.Result.Directive;

/**
 * The system input processor.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class InputKeyProcessorChain implements Visitor<Key, Result>
{
	private Session session = null;
	private Console systerminal;

	public InputKeyProcessorChain(Session session)
	{
		this.session = session;
		this.systerminal = new Console();
	}

	public void run()
	{
		putString(session.getPrompt());
		while (true)
		{
			Key key = session.getScreen().readInput();
			if (key == null)
			{
				continue;
			}
			if (key.getKind() == Kind.Escape)
			{
				break;
			}
			else if (key.getKind() == Kind.Enter)
			{
				try
				{
					if (session.getCommandBuffer().length() == 0)
					{
						putString("\n");
						putString(session.getPrompt());
					}
					else
					{
						try
						{
							String result = systerminal.execute(session.getCommandBuffer(true).split(" "), new File("/home/tkhuu"));
							putString("\n");
							putString(result);
							putString("\n");
							putString(session.getPrompt());
						}
						catch (IOException e)
						{
							session.flushBuffer();
							putString("\n");
							putString(e.getMessage());
							putString("\n");
							putString(session.getPrompt());
						}
					}
					continue;

				}
				catch (Exception e)
				{
					session.flushBuffer();
					putString("\n");
					putString(e.getMessage());
					putString("\n");
					putString(session.getPrompt());
				}
				
			}
			else
			{
				Result res = visit(key);

				if (res == Result.PRINT)
				{
					session.addToBuffer(key.getCharacter());
					session.getTerminal().putCharacter(key.getCharacter());

				}
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.foundation.Visitor#visit(java.lang.Object)
	 */
	@Override
	public Result visit(Key visitable)
	{
		Result result = Result.PRINT;

		for (KeyProcessor kp : getProcessors())
		{
			if (kp.canHandle(visitable, session))
			{
				result = kp.handle(visitable, session);
				if (Directive.STOP == result.getDirective() || Directive.SILENT == result.getDirective())
				{
					break;
				}
			}

		}
		return result;
	}

	/**
	 * Put string.
	 * 
	 * @param rawTerminal
	 *            the raw terminal
	 * @param string
	 *            the string
	 */
	private void putString(String string)
	{
		Terminal term = session.getTerminal();
		for (int i = 0; i < string.length(); i++)
		{
			char c = string.charAt(i);
			if (c == Keys.NEWLINE)
			{
				term.newLine(1);

			}
			else
			{
				term.putCharacter(c);
			}
		}
	}

	public static abstract class KeyProcessor
	{

		public abstract boolean canHandle(Key key, Session session);

		public abstract Result handle(Key key, Session session);

	}

	public abstract List<KeyProcessor> getProcessors();

}
