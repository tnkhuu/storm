/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.shell.engine.processor;

import java.text.DateFormat;
import java.util.Date;

import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.screen.ScreenWriter;
import org.storm.console.ui.framework.terminal.Terminal;

/**
 * The Shell session
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Session
{
	
	/** The terminal. */
	private final Screen screen;
	private final String user;
	private final String host;
	private final ScreenWriter writer;
	private String prompt;
	private StringBuffer commandBuffer = new StringBuffer();
	
	public Session(Screen screen, ScreenWriter writer, String user, String host) {
		this.screen = screen;
		this.user = user;
		this.host = host;
		this.writer = writer;
	}
	
	public String getUser()
	{
		return user;
	}

	public String getHost()
	{
		return host;
	}

	public Screen getScreen() {
		return this.screen;
	}
	
	public Terminal getTerminal() {
		return screen.getTerminal();
	}
	
	public ScreenWriter getWriter()
	{
		return writer;
	}

	/**
	 * @return the prompt
	 */
	public String getPrompt()
	{
		return "[" + DateFormat.getInstance().format(new Date()) + " " + prompt + "]> ";
	}

	/**
	 * @param prompt the prompt to set
	 */
	public void setPrompt(String prompt)
	{
		this.prompt = prompt;
	}
	
	public void addToBuffer(char c) {
		commandBuffer.append(c);
	}
	
	public void addToBuffer(String string) {
		commandBuffer.append(string);
	}
	
	public void flushBuffer() {
		commandBuffer = new StringBuffer();
	}
	
	public String getCommandBuffer() {
		return getCommandBuffer(false);
	}
	
	public String getCommandBuffer(boolean flush) {
		String value = commandBuffer.toString();
		if(flush) commandBuffer = new StringBuffer();
		return value;
	}
	public void deleteFromBuffer() {
		commandBuffer.delete(commandBuffer.length() - 1, commandBuffer.length());
	}
	
	
}
