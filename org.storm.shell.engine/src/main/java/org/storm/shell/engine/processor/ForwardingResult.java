package org.storm.shell.engine.processor;

/**
 * The proxy forwarding result
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ForwardingResult implements Result
{

	@Override
	public Directive getDirective()
	{
		return Directive.FORWARD;
	}

}
