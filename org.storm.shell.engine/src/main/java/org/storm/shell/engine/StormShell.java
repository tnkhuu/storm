/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.shell.engine;

import org.storm.console.ui.framework.TerminalFacade;
import org.storm.console.ui.framework.screen.Screen;
import org.storm.console.ui.framework.screen.ScreenWriter;
import org.storm.shell.engine.processor.ArrowKeyProcessor;
import org.storm.shell.engine.processor.InputKeyProcessorChain;
import org.storm.shell.engine.processor.KeyProcessorChainFactory;
import org.storm.shell.engine.processor.Session;
import org.storm.shell.engine.processor.StandardKeyProcessor;

/**
 * The main shell.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StormShell
{

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		Screen screen = new Screen(TerminalFacade.createTerminal());
		ScreenWriter writer = new ScreenWriter(screen);
		Session session = new Session(screen, writer,"tkhuu", "localhost");
		String prompt = session.getUser() + "@" + session.getHost();
		session.setPrompt(prompt);
		try
		{
			
			screen.startScreen();
			screen.refresh();
			InputKeyProcessorChain chain = KeyProcessorChainFactory.createDefaultChain(session, new StandardKeyProcessor(), new ArrowKeyProcessor());
			chain.run();
		}
		finally
		{
			screen.stopScreen();
			System.exit(0);
		}
	}

}
