/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.nexus.server;

import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.util.Enumeration;
import java.util.concurrent.atomic.AtomicInteger;
import org.storm.nexus.api.Services;
import com.google.common.io.Files;

/**
 * The settings of a specific instance within a quorum.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class InstanceSpec
{
	
	private static final AtomicInteger nextServerId = new AtomicInteger(1);
	private static String localhost;
	private final File dataDirectory;
	private final int port;
	private final int electionPort;
	private final int quorumPort;
	private final boolean deleteDataDirectoryOnClose;
	private final int serverId;
	static
	{

		try
		{
			NetworkInterface ni = NetworkInterface.getByName(Services.OUTBOUND_ETHERNET_NAME);
			Enumeration<InetAddress> addresses = ni.getInetAddresses();

			while (addresses.hasMoreElements())
			{
				InetAddress addr = addresses.nextElement();
				if (addr instanceof Inet4Address)
				{
					localhost = addr.getHostAddress();
					break;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}



	/**
	 * New instance spec.
	 *
	 * @return the instance spec
	 */
	public static InstanceSpec newInstanceSpec()
	{
		return new InstanceSpec(null, -1, -1, -1, true, -1);
	}

	/**
	 * Gets the random port.
	 *
	 * @return the random port
	 */
	public static int getRandomPort()
	{
		ServerSocket server = null;
		try
		{
			server = new ServerSocket(0);
			return server.getLocalPort();
		}
		catch (IOException e)
		{
			throw new Error(e);
		}
		finally
		{
			if (server != null)
			{
				try
				{
					server.close();
				}
				catch (IOException ignore)
				{
					// ignore
				}
			}
		}
	}

	/**
	 * Instantiates a new instance spec.
	 *
	 * @param dataDirectory where to store data/logs/etc.
	 * @param port the port to listen on - each server in the ensemble must use a
	 * unique port
	 * @param electionPort the electionPort to listen on - each server in the ensemble
	 * must use a unique electionPort
	 * @param quorumPort the quorumPort to listen on - each server in the ensemble must
	 * use a unique quorumPort
	 * @param deleteDataDirectoryOnClose if true, the data directory will be deleted when
	 * @param serverId the server ID for the instance
	 * {@link TestingCluster#close()} is called
	 */
	public InstanceSpec(File dataDirectory, int port, int electionPort, int quorumPort, boolean deleteDataDirectoryOnClose, int serverId)
	{
		this.dataDirectory = (dataDirectory != null) ? dataDirectory : Files.createTempDir();
		this.port = (port >= 0) ? port : getRandomPort();
		this.electionPort = (electionPort >= 0) ? electionPort : getRandomPort();
		this.quorumPort = (quorumPort >= 0) ? quorumPort : getRandomPort();
		this.deleteDataDirectoryOnClose = deleteDataDirectoryOnClose;
		this.serverId = (serverId >= 0) ? serverId : nextServerId.getAndIncrement();
	}

	/**
	 * Gets the server id.
	 *
	 * @return the server id
	 */
	public int getServerId()
	{
		return serverId;
	}

	/**
	 * Gets the data directory.
	 *
	 * @return the data directory
	 */
	public File getDataDirectory()
	{
		return dataDirectory;
	}

	/**
	 * Gets the port.
	 *
	 * @return the port
	 */
	public int getPort()
	{
		return port;
	}

	/**
	 * Gets the election port.
	 *
	 * @return the election port
	 */
	public int getElectionPort()
	{
		return electionPort;
	}

	/**
	 * Gets the quorum port.
	 *
	 * @return the quorum port
	 */
	public int getQuorumPort()
	{
		return quorumPort;
	}

	/**
	 * Gets the connect string.
	 *
	 * @return the connect string
	 */
	public String getConnectString()
	{
		return localhost + ":" + port;
	}

	/**
	 * Delete data directory on close.
	 *
	 * @return true, if successful
	 */
	public boolean deleteDataDirectoryOnClose()
	{
		return deleteDataDirectoryOnClose;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "InstanceSpec{" + "dataDirectory=" + dataDirectory + ", port=" + port + ", electionPort=" + electionPort + ", quorumPort=" + quorumPort
				+ ", deleteDataDirectoryOnClose=" + deleteDataDirectoryOnClose + ", serverId=" + serverId + '}';
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		InstanceSpec that = (InstanceSpec) o;
		return port == that.port;

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return port;
	}
}
