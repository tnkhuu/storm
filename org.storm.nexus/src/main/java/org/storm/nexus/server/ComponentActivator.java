/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.nexus.server;

import java.io.File;
import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.zookeeper.server.ZKDatabase;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig.ConfigException;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Storm;
import org.storm.api.kernel.Actor;
import org.storm.api.server.StormZookeeperServer;
import org.storm.nexus.api.Role;

/**
 * The Nexus Bundle Activator and bootstrapper.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator extends Actor implements BundleActivator, ManagedService
{
	private static final Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);
	private BundleContext m_context = null;
	private ZooKeeperServer zookeeperServer;
	private QuorumPeer quorumPeer;
	private ServerCnxnFactory cnxnFactory;
	private ServiceRegistration<?> managedServiceRegistration;
	private ServiceRegistration<StormZookeeperServer> zkRegistration;
	private ServiceRegistration<Storm> stormRegistration;
	private volatile boolean isRunning = false;
	private ExecutorService executor = Executors.newFixedThreadPool(2);
	/** TODO: Integrate this into the zookeeper leader election algorithm. */
	private boolean isLeader = true;
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext )
	 */
	public void start(final BundleContext context) throws Exception
	{
		m_context = context;
		if (getRole() != Role.INITIAL)
		{
			if (isLeader)
			{
				Dictionary<String, Object> props = new Hashtable<String, Object>();
				props.put(Constants.SERVICE_PID, "org.storm.nexus.service.discovery.server");
				managedServiceRegistration = context.registerService(org.osgi.service.cm.ManagedService.class.getName(), this, props);
			}
			addDNSRecord(context);
		}
	}

	@SuppressWarnings("rawtypes")
	public synchronized void updated(final Dictionary dict) throws ConfigurationException
	{
		executor.execute(new Runnable()
		{

			@Override
			public void run()
			{
				if (!isRunning)
				{
					synchronized (this)
					{
						if (!isRunning)
						{
							boolean startedOk = true;
							try
							{
								QuorumPeerConfig config = parseConfig(dict);
								// startQuorum(config);
								startFromConfig(config);
								s_log.info("Config Admin has Zoo keeper configuration as :");
								s_log.info(config.toString());
								s_log.info(config.getInfo());
							}
							catch (Exception th)
							{
								startedOk = false;
								s_log.error("Problem applying configuration update: " + dict, th);
							}
							finally
							{
								if (startedOk)
								{
									isRunning = true;
								}

							}
						}
					}

				}

			}
		});

	}

	private void startFromConfig(final QuorumPeerConfig config) throws IOException, InterruptedException
	{
		int numServers = config.getServers().size();
		if (numServers > 1)
		{
			// main = new MyQuorumPeerMain(config);
		}
		else
		{

			zookeeperServer = new ZooKeeperServer();
			ServerCnxnFactory cnxnFactory = ServerCnxnFactory.createFactory();
			FileTxnSnapLog ftxn = new FileTxnSnapLog(new File(config.getDataLogDir()), new File(config.getDataDir()));
			zookeeperServer.setTxnLogFactory(ftxn);
			zookeeperServer.setTickTime(config.getTickTime());
			zookeeperServer.setMinSessionTimeout(config.getMinSessionTimeout());
			zookeeperServer.setMaxSessionTimeout(config.getMaxSessionTimeout());
			cnxnFactory.configure(config.getClientPortAddress(), config.getMaxClientCnxns());
			cnxnFactory.startup(zookeeperServer);
		}
		initServices();
	}

	private void initServices()
	{
		zkRegistration = m_context.registerService(StormZookeeperServer.class, zookeeperServer, new Hashtable<String, Object>());
		s_log.info("Registered this node as an ensemble into Storm ");
	}

	@SuppressWarnings("unused")
	private void startQuorum(final QuorumPeerConfig config) throws IOException
	{

		quorumPeer = new QuorumPeer();
		cnxnFactory = ServerCnxnFactory.createFactory();
		cnxnFactory.configure(config.getClientPortAddress(), config.getMaxClientCnxns());
		quorumPeer.setClientPortAddress(config.getClientPortAddress());
		quorumPeer.setTxnFactory(new FileTxnSnapLog(new File(config.getDataLogDir()), new File(config.getDataDir())));
		quorumPeer.setQuorumPeers(config.getServers());
		quorumPeer.setElectionType(config.getElectionAlg());
		quorumPeer.setMyid(config.getServerId());
		quorumPeer.setTickTime(config.getTickTime());
		quorumPeer.setMinSessionTimeout(config.getMinSessionTimeout());
		quorumPeer.setMaxSessionTimeout(config.getMaxSessionTimeout());
		quorumPeer.setInitLimit(config.getInitLimit());
		quorumPeer.setSyncLimit(config.getSyncLimit());
		quorumPeer.setQuorumVerifier(config.getQuorumVerifier());
		quorumPeer.setCnxnFactory(cnxnFactory);
		quorumPeer.setZKDatabase(new ZKDatabase(quorumPeer.getTxnFactory()));
		quorumPeer.setLearnerType(config.getPeerType());

		quorumPeer.start();
		initServices();
	}

	/**
	 * Parses the config.
	 * 
	 * @param dict
	 *            the dict
	 * @return the quorum peer config
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ConfigException
	 *             the config exception
	 */
	@SuppressWarnings("rawtypes")
	private QuorumPeerConfig parseConfig(Dictionary dict) throws IOException, ConfigException
	{
		QuorumPeerConfig config = new QuorumPeerConfig();
		if (dict == null)
		{
			dict = config.getDefaults(m_context);
		}
		Properties props = new Properties();
		for (Enumeration e = dict.keys(); e.hasMoreElements();)
		{
			Object key = e.nextElement();
			props.put(key, dict.get(key));
		}

		config.parseProperties(props);
		return config;
	}

	@SuppressWarnings(
	{ "rawtypes", "unused" })
	private QuorumPeerConfig parseConfig(Dictionary dict, int id) throws IOException, ConfigException
	{
		return parseConfig(dict, -1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception
	{
		s_log.info("Terminating all sessions. This may take a few seconds...");
		if (cnxnFactory != null) cnxnFactory.shutdown();
		if (zookeeperServer != null) zookeeperServer.shutdown();
		if (managedServiceRegistration != null) managedServiceRegistration.unregister();
		if (zkRegistration != null) zkRegistration.unregister();
		if (stormRegistration != null) stormRegistration.unregister();

	}
}
