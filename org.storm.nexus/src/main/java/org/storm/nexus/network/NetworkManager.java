/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.network;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * Network related services and utilities.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class NetworkManager
{	
	private static final String ETH0 = "eth0";

	public static String getOutboundAddress()
	{
		String address = null;
		try
		{
			NetworkInterface ni = NetworkInterface.getByName(ETH0);
			Enumeration<InetAddress> addresses = ni.getInetAddresses();

			while (addresses.hasMoreElements())
			{
				InetAddress addr = addresses.nextElement();
				if (addr instanceof Inet4Address)
				{
					address = addr.getHostAddress();
					break;
				}
			}
		}
		catch (Exception e)
		{

		}
		return address;
	}

	/**
	 * Gets the network address.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param ipType
	 *            the ip type
	 * @return the network address
	 */
	public static <T extends InetAddress> T getNetworkAddress(Class<T> ipType)
	{
		T address = null;
		try
		{
			boolean found = false;
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements())
			{
				NetworkInterface ni = interfaces.nextElement();
				if (ni.isVirtual() || ni.isLoopback() || ni.isPointToPoint() || !ni.isUp())
				{
					continue;
				}
				else
				{
					Enumeration<InetAddress> addresses = ni.getInetAddresses();
					while (addresses.hasMoreElements())
					{
						InetAddress addr = addresses.nextElement();
						if (ipType.isAssignableFrom(addr.getClass()))
						{
							address = ipType.cast(addr);
							found = true;
							break;
						}

					}
				}
				if (found)
				{
					break;
				}
			}
		}
		catch (SocketException e)
		{
			throw new IllegalStateException(e);
		}
		return address;
	}

	/**
	 * Gets the host name.
	 * 
	 * @return the host name
	 */
	public static String getHostName()
	{
		try
		{
			return InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e)
		{
			throw new IllegalStateException("Unknown host name");
		}
	}

}
