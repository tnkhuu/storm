package org.storm.nexus;

import java.util.List;

import com.netflix.curator.RetryPolicy;
import com.netflix.curator.framework.CuratorFramework;
import com.netflix.curator.framework.CuratorFrameworkFactory;
import com.netflix.curator.retry.ExponentialBackoffRetry;

public class CreateClient
{
	public static CuratorFramework createSimple(String connectionString)
	{
		ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);
		return CuratorFrameworkFactory.newClient(connectionString, retryPolicy);
	}

	public static CuratorFramework createWithOptions(String connectionString, RetryPolicy retryPolicy, int connectionTimeoutMs, int sessionTimeoutMs)
	{
		return CuratorFrameworkFactory.builder().connectString(connectionString).retryPolicy(retryPolicy).connectionTimeoutMs(connectionTimeoutMs)
				.sessionTimeoutMs(sessionTimeoutMs)
				.build();
	}

	public static void main(String[] args) throws Exception
	{

		CuratorFramework fw = CreateClient.createSimple("192.168.181.129:2181");
		fw.start();
		List<String> c = fw.getChildren().forPath("/agents");
		System.out.println(c);
		//Thread.sleep(1000000);
	}
}
