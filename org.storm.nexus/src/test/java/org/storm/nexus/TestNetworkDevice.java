package org.storm.nexus;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

public class TestNetworkDevice
{

    
    public void test() throws SocketException, UnknownHostException
    {
	NetworkInterface ni = NetworkInterface.getByName("eth0");
	Enumeration<InetAddress> addresses = ni.getInetAddresses();

	while(addresses.hasMoreElements()) {
	    InetAddress addr = addresses.nextElement();
	    if(addr instanceof Inet4Address) {
		System.out.println("ip4: " + addr);
	    }
	    else {
		System.out.println("ip6: " + addr);
	    }
	}
	
	System.out.println(InetAddress.getLocalHost().getHostAddress());
    }

}
