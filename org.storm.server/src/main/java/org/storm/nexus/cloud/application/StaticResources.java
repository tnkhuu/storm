/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.cloud.application;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * The Class StaticResources.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StaticResources
{
	public static URI NOTFOUND_URL;
	public static URI ERRORFOUND_URL;
	
	static
	{
		try
		{
			NOTFOUND_URL = new URI("http://404.stormclowd.com");
			ERRORFOUND_URL = new URI("http://app.error.stormclowd.com");
		}
		catch (URISyntaxException e)
		{

		}
	}
}
