/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.cloud.application;

import java.net.URI;
import java.util.concurrent.Future;

import org.storm.api.application.Application;

/**
 * The Application Manager 
 *
 * @author Trung Khuu
 * @since 1.0
 */
public interface ApplicationManager
{
	
	String ROOT_PATH = "/";
	
	
	/**
	 * Deploy the specified application into the nexus cloud.
	 *
	 * @param application the application
	 * @return the url
	 */
	URI deploy(Application application);
	
	/**
	 * Deploy the specified application into with the given sub domain name
	 * onto the nexus cloud.
	 *
	 * @param application the application
	 * @param callback the callback to execute when the app has been deployed.
	 * @return the application instance
	 */
	Future<Application>  deploy(String applicationName);
	
	/**
	 * Undeploy the specified application into with the given sub domain name
	 * onto the nexus cloud.
	 *
	 * @param application the application
	 * @param callback the callback to execute when the app has been deployed.
	 * @return the url
	 */
	boolean undeploy(String applicationName, AppDeployedCallback ballback );
	
	/**
	 * Returns true if an application has been
	 * registered against a specific domain.
	 * 
	 * @param domain
	 * @return
	 */
	boolean isThereAppRegisteredFor(String domain);
	
	/**
	 * A callback interface for the result of attempting
	 * to deploy a new application.
	 * 
	 * @author Trung Khuu
	 *
	 */
	public interface AppDeployedCallback {
		
		void onComplete(Application application);
		void onFail(String message, Throwable t);
	}
}
