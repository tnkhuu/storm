/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.storm.nexus.cloud;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.storm.api.Kernel;
import org.storm.api.Storm;
import org.storm.api.application.Application;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.exception.AppDeployException;
import org.storm.api.exception.VMProvisionException;
import org.storm.api.services.ComputeService;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.nexus.api.Role;
import org.storm.nexus.cloud.application.ApplicationManager;

/**
 * The Storm Platform Server.
 * 
 * This one of the core functional aspects of the storm platform. The Storm
 * implementation represents the outer layer of the storm framework. It
 * intercepts all incoming traffic, and maps an incoming request to the
 * appropriate application handler. Application handlers are mapped by sub domain
 * name.
 * 
 * In the case that a request is made to a subdomain which has a registered
 * application but no active instances, storm will provision the required
 * resources/instances as required on the fly.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StormImpl implements Storm
{
	
	private volatile Kernel kernel;
	private volatile ApplicationManager appManager;
	private volatile ComputeService computeService;
	private ExecutorService exec = Executors.newCachedThreadPool();

	/**
	 * Resolve and deploy a application bound to the given domain.
	 *
	 * @param domain the domain
	 * @return the future
	 * @throws AppDeployException the app deploy exception
	 */
	@Override
	public Future<Application> resolve(String domain) throws AppDeployException
	{
		if (appManager.isThereAppRegisteredFor(domain))
		{
			return appManager.deploy(domain);
		}

		throw new AppDeployException("No application registered for " + domain);
	}

	/**
	 * Provisions a new virtual machine for the cloud ensemble.
	 *
	 * @return the future
	 * @throws VMProvisionException the vM provision exception
	 */
	@Override
	public Future<VirtualMachine> provision(CloudProvider provider, Role role, HypervisorDriver driver, HypervisorType type) throws VMProvisionException
	{
		switch (provider)
		{
		case AMAZON:
			return provisionAmazonVm(role, driver, type);

		case GOOGLE:
			break;
		case RACKSPACE:
		{
			break;
		}
		default:
			break;
		}

		return null;
	}
	
	/**
	 * Provision amazon vm.
	 *
	 * @param role the role
	 * @param driver the driver
	 * @param type the type
	 * @return the future
	 */
	private Future<VirtualMachine> provisionAmazonVm(final Role role, final HypervisorDriver driver, final HypervisorType type) {
		return exec.submit(new Callable<VirtualMachine>()
		{
			@Override
			public VirtualMachine call() throws Exception
			{
				VirtualMachine vm =  computeService.createInstance(role,null, driver, type);
				kernel.register(role, vm);
				return vm;
			}
		});
	}

}
