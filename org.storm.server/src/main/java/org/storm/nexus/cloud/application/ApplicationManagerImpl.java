/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.storm.nexus.cloud.application;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import javax.management.ServiceNotFoundException;

import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Storm;
import org.storm.api.application.Application;
import org.storm.api.exception.MissingVirtualizationProviderException;
import org.storm.api.server.DistributedServer;
import org.storm.api.services.LogicalVolumeService;
import org.storm.api.services.ServiceGateway;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.nexus.api.Agent;
import org.storm.nexus.api.AgentCallback;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.api.RemoteAgent;
import org.storm.nexus.api.Role;
import org.storm.nexus.api.Services;

/**
 * The Class ApplicationManagerImpl.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ApplicationManagerImpl implements ApplicationManager
{
	private static final Logger s_log = LoggerFactory.getLogger(ApplicationManagerImpl.class);
	private final Map<String, URI> applicationRegistry = new HashMap<>();
	private static final ExecutorService deployExecutor = Executors.newCachedThreadPool();
	private volatile ServiceGateway serviceGateway;
	private volatile Agent agent;
	private volatile BundleContext context;

	public ApplicationManagerImpl()
	{
		try
		{
			applicationRegistry.put(Endpoints.STORM_WWW.getDomain(), new URI("http://artifacts:1331/webapps/org.storm.war-1.0.0.war"));
			applicationRegistry.put("stormclowd.com", new URI("http://artifacts:1331/webapps/org.storm.war-1.0.0.war"));
			applicationRegistry.put("www.stormclowd.com", new URI("http://artifacts:1331/webapps/org.storm.war-1.0.0.war"));
			applicationRegistry.put("sample.stormclowd.com", new URI("http://artifacts:1331/webapps/org.storm.sample.war-1.0.0.war"));
			applicationRegistry.put("www.sample.stormclowd.com", new URI("http://artifacts:1331/webapps/org.storm.sample.war-1.0.0.war"));

		}
		catch (URISyntaxException e)
		{
			// wont happen
		}
	}

	/**
	 * Returns true if an application has been
	 * registered against a specific domain.
	 * 
	 * @param domain
	 * @return
	 */
	@Override
	public boolean isThereAppRegisteredFor(String domain)
	{
		return applicationRegistry.containsKey(domain);

	}

	/**
	 * Deploy the specified application into with the given sub domain name
	 * onto the nexus cloud.
	 *
	 * @param application the application
	 * @param callback the callback to execute when the app has been deployed.
	 * @return the application instance
	 */
	@Override
	public URI deploy(Application application)
	{
		Application app = null;
		final URI appUrl = application.getURI();
		try
		{
			DistributedServer server = serviceGateway.lookup(DistributedServer.class);
			int port = server.deploy(application.getDomainName(), appUrl.toString(), ROOT_PATH);
			if (port > 0)
			{
				app = ApplicationFactory.build(application.getDomainName(), server.address(), true, appUrl, appUrl.toString());
			}
			else
			{
				s_log.error("An unknown error occured while trying to deploy {}", appUrl);
				return StaticResources.ERRORFOUND_URL;
			}
		}
		catch (ServiceNotFoundException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		return app == null ? StaticResources.NOTFOUND_URL : app.getURI();
	}

	/**
	 * Deploy the specified application into with the given sub domain name
	 * onto the nexus cloud.
	 *
	 * @param application the application
	 * @param callback the callback to execute when the app has been deployed.
	 * @return the application instance
	 */
	@Override
	public Future<Application> deploy(final String applicationName)
	{
		final String santized = applicationName.replaceAll("\\.", "-");
		FutureTask<Application> result = null;
		final URI appUrl = applicationRegistry.get(applicationName);
		if (appUrl != null)
		{
			result = new FutureTask<Application>(new Callable<Application>()
			{
				@Override
				public Application call() throws Exception
				{
					Application app = null;
					String path = Storm.APPLICATIONS + santized;

					try
					{
						DistributedServer server = serviceGateway.lookup(DistributedServer.class);
						if (server != null)
						{
							app = deployAppOnRemoteServer(server, santized, appUrl, ROOT_PATH);
						}
						else
						{
							VirtualizationProvider provider = null;
							LogicalVolumeService lvs = null;
							ServiceReference<VirtualizationProvider> sr = context.getServiceReference(VirtualizationProvider.class);
							ServiceReference<LogicalVolumeService> lsr = context.getServiceReference(LogicalVolumeService.class);
							String dns = agent.getServersWithRole(Role.DNS);
							if (dns == null || dns.length() == 0)
							{
								int wait = 10;
								while (dns == null && wait-- > 0)
								{
									dns = agent.getServersWithRole(Role.DNS);
									if (dns == null)
									{
										Thread.sleep(1000);
									}
									else
									{
										break;
									}
								}
							}
							if (lsr == null)
							{
								lvs = serviceGateway.lookup(LogicalVolumeService.class);

							}
							else
							{
								lvs = context.getService(lsr);
							}

							if (sr == null)
							{
								provider = serviceGateway.lookup(VirtualizationProvider.class);
							}
							else
							{
								provider = context.getService(sr);
							}

							if (!provider.contains(HypervisorDriver.LXC, santized))
							{
								if (lvs != null)
								{
									if (!lvs.baseLxcLogicalVolumnExists())
									{
										lvs.createLxcBaseLogicalVolumn(LogicalVolumeService.BASE_LVM_IMAGE_NAME, Role.SERVER.name(), dns, 1500);
									}

									String snapshot = lvs.createSnapshotVolumn(santized, Role.SERVER.name(), 1500, LogicalVolumeService.BASE_LXC_LVM_DIR,
											LogicalVolumeService.LXC_VOLUMN_NAME);
									provider.createLVMBackedContainer(HypervisorDriver.LXC, Role.SERVER.name(), santized, snapshot, dns);
									s_log.info("Container created for {} ", santized);
								}
								else
								{
									provider.createContainer(HypervisorDriver.LXC, Role.SERVER.name(), santized, dns);
								}
								final String ipAddress = provider.startContainer(santized);
								app = agent.waitForAgent(Role.SERVER, new AgentCallback<Application>(ipAddress)
								{

									@Override
									public Application call()
									{
										try
										{
											DistributedServer server = serviceGateway.lookup(DistributedServer.class, new RemoteAgent()
											{

												@Override
												public String getServerIpAddress()
												{
													return ipAddress;
												}

												@Override
												public int getRpcPort()
												{
													return agent.getRpcPort();
												}

											});
											return deployAppOnRemoteServer(server, santized, appUrl, ROOT_PATH);
										}
										catch (ServiceNotFoundException e)
										{
											s_log.error(e.getMessage(), e);
											throw new RuntimeException(e);
										}

									}
								});

							}
							else
							{
								throw new MissingVirtualizationProviderException("No virtualization providers was found on the ensemble");
							}

						}
					}
					catch (ServiceNotFoundException e)
					{
						s_log.error(e.getMessage(), e);
						throw new RuntimeException(e);
					}
					if (!agent.exists(path))
					{
						if (applicationRegistry.containsKey(santized))
						{

							ObjectMapper mapper = new ObjectMapper();
							agent.ensurePath(Services.SERVICES_PATH + "/" + DistributedServer.class.getName());
							agent.ensurePath(path, true, mapper.writeValueAsBytes(app));
						}
					}

					return app;
				}
			});
			deployExecutor.execute(result);
		}
		return result;
	}

	/**
	 * Do the actual deployment and make the rmi call to deploy the application.
	 * 
	 * @param server
	 * @param applicationName
	 * @param appUrl
	 * @param contextPath
	 * @return
	 */
	private Application deployAppOnRemoteServer(DistributedServer server, String applicationName, URI appUrl, String contextPath)
	{
		Application app = null;
		int port = server.deploy(applicationName, appUrl.toString(), contextPath);
		if (port > 0)
		{
			app = ApplicationFactory.build(applicationName, server.address() + ":" + port, true, appUrl, appUrl.toString());
		}
		else
		{
			s_log.error("An unknown error occured while trying to deploy {}", appUrl);
		}
		return app;
	}

	/**
	 * Undeploy the specified application into with the given sub domain name
	 * onto the nexus cloud.
	 *
	 * @param application the application
	 * @param callback the callback to execute when the app has been deployed.
	 * @return the url
	 */
	@Override
	public boolean undeploy(String applicationName, AppDeployedCallback ballback)
	{
		return false;
	}

	
	@Override
	public String toString()
	{
		return "ApplicationManagerImpl [applicationCache=" + applicationRegistry + ", serviceGateway=" + serviceGateway + "]";
	}

}
