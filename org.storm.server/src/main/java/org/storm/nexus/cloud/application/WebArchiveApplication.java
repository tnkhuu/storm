package org.storm.nexus.cloud.application;

import java.net.URI;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.storm.api.application.Application;

/**
 * A web application.
 * 
 * @author Trung Khuu
 * @since 1.0
 *
 */
public class WebArchiveApplication implements Application {
		private final String domainName;
		private final String proxiedAddress;
		private final boolean running;
		private final URI uri;
		private final String appBinaries;
		
		/**
		 * Instantiates a new web archive application.
		 * 
		 * @param domainName
		 *            the domain name
		 * @param proxiedAddr
		 *            the proxied addr
		 * @param isRunning
		 *            the is running
		 * @param uri
		 *            the uri
		 * @param appBinaries
		 *            the app binaries
		 */
		@JsonCreator
		public WebArchiveApplication(@JsonProperty("domainName")String domainName, @JsonProperty("proxiedAddr")String proxiedAddr, @JsonProperty("running")boolean running, @JsonProperty("uri")URI uri, @JsonProperty("appBinaries")String appBinaries) {
			this.domainName = domainName;
			this.proxiedAddress = proxiedAddr;
			this.running = running;
			this.uri = uri;
			this.appBinaries = appBinaries;
		}
		@Override
		public String getDomainName()
		{
			return domainName;
		}

		@Override
		public String getProxiedAddress()
		{
			return proxiedAddress;
		}

		@Override
		public boolean running()
		{
			return running;
		}

		@Override
		public URI getURI()
		{
			return uri;
		}

		@Override
		public String getAppBinaries()
		{
			return appBinaries;
		}
		@Override
		public String toString()
		{
			return "WebArchiveApplication [domainName=" + domainName + ", proxiedAddress=" + proxiedAddress + ", isRunning=" + running + ", uri=" + uri + ", appBinaries="
					+ appBinaries + "]";
		}
		
		
		
	}