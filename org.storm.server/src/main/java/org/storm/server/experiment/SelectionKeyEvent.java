/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.experiment;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * The Class SelectionKeyEvent.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SelectionKeyEvent
{

	SelectionKey key;
	SocketChannel channel;

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public SelectionKey getKey()
	{
		return key;
	}

	/**
	 * Sets the key.
	 * 
	 * @param key
	 *            the key
	 * @param channel
	 *            the channel
	 */
	public void setKey(SelectionKey key, SocketChannel channel)
	{
		this.key = key;
		this.channel = channel;
	}

	/**
	 * Gets the channel.
	 * 
	 * @return the channel
	 */
	public SocketChannel getChannel()
	{
		return this.channel;
	}

}
