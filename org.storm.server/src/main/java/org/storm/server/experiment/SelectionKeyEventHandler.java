/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.experiment;

import java.nio.CharBuffer;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;

import org.storm.disruptor.EventHandler;
/**
 * The Class SelectionKeyEventHandler.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SelectionKeyEventHandler implements EventHandler<SelectionKeyEvent>
{
	public static Code OK = new Code(200, "OK");
	public static Code BAD_REQUEST = new Code(400, "Bad Request");
	public static Code NOT_FOUND = new Code(404, "Not Found");
	public static Code METHOD_NOT_ALLOWED = new Code(405, "Method Not Allowed");
	public static String CRLF = "\r\n";
	/**
	 * A helper class which define the HTTP response codes.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class Code
	{
		public SelectableChannel sc;
		public int number;
		public String reason;
		
		public Code(int i, String r)
		{
			number = i;
			reason = r;
		}

		@Override
		public String toString()
		{
			return number + " " + reason;
		}


	}


	public SelectionKeyEventHandler()
	{

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.disruptor.EventHandler#onEvent(java.lang.Object, long,
	 * boolean)
	 */
	@Override
	public void onEvent(SelectionKeyEvent event, long sequence, boolean endOfBatch) throws Exception
	{
		CharBuffer cb = CharBuffer.allocate(1024);
		cb.put("HTTP/1.0 ").put(OK.toString()).put(CRLF);
		cb.put("Server: TrungsFastServer/0.1").put(CRLF);
		cb.put("Content-type: ").put("text/html").put(CRLF);
		cb.put("Content-length: ").put("" + "Hello".length()).put(CRLF);
		cb.put(CRLF);
		event.key.interestOps(SelectionKey.OP_WRITE);
	}

}
