/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.experiment;

import static org.storm.disruptor.RingBuffer.createSingleProducer;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.disruptor.BatchEventProcessor;
import org.storm.disruptor.RingBuffer;
import org.storm.disruptor.SequenceBarrier;
import org.storm.disruptor.YieldingWaitStrategy;

/**
 * A attempt at a implementation of a disruptor powered netty server.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Dispatcher
{
	private static final Logger s_log = LoggerFactory.getLogger(Dispatcher.class);
	private static final int BUFFER_SIZE = 1024 * 64;
	private SelectableChannel channel;
	private final Selector selector;
	private final ExecutorService exec = Executors.newCachedThreadPool();
	private final RingBuffer<SelectionKeyEvent> ringBuffer = createSingleProducer(new SelectionKeyEventFactory(), BUFFER_SIZE, new YieldingWaitStrategy());
	private final SequenceBarrier sequenceBarrier = ringBuffer.newBarrier();
	private final BatchEventProcessor<SelectionKeyEvent> batchEventProcessor = new BatchEventProcessor<SelectionKeyEvent>(ringBuffer, sequenceBarrier,
			new SelectionKeyEventHandler());
			{
				ringBuffer.addGatingSequences(batchEventProcessor.getSequence());
			}

	/**
	 * Instantiates a new dispatcher.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Dispatcher() throws IOException
	{
		selector = Selector.open();
		exec.submit(batchEventProcessor);
	}

	public void run(SocketChannel ch)
	{

		if (ch != null)
		{
			try
			{
				channel = ch;
				channel.configureBlocking(false);
				ch.register(selector, SelectionKey.OP_READ, new SelectionKeyEventHandler());
				selector.select();
				for (Iterator<?> i = selector.selectedKeys().iterator(); i.hasNext();)
				{
					long seq = ringBuffer.next();
					SelectionKey sk = (SelectionKey) i.next();
					ringBuffer.getPreallocated(seq).setKey(sk, ch);
					i.remove();
					ringBuffer.publish(seq);
				}
			}
			catch (Exception e)
			{
				s_log.error(e.getMessage(), e);
			}
		}
	}

}
