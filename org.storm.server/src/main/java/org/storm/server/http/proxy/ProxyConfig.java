/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;


/**
 * Simple class for storing configuration. We cheat here and make this all
 * static to avoid the overhead of integrating dependency injection that could
 * collide with versions of libraries programs including this library are using.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ProxyConfig
{
	private static boolean useDnsSec = false;
	private static boolean useJmx = false;


	/**
	 * Instantiates a new proxy config.
	 */
	private ProxyConfig()
	{
	}

	/**
	 * Sets whether or not to use DNSSEC to request signed records when
	 * performing DNS lookups and verifying those records if they exist.
	 * 
	 * @param useDnsSec
	 *            Whether or not to use DNSSEC.
	 */
	public static void setUseDnsSec(final boolean useDnsSec)
	{
		ProxyConfig.useDnsSec = useDnsSec;
	}

	/**
	 * Whether or not we're configured to use DNSSEC for lookups.
	 * 
	 * @return <code>true</code> if configured to use DNSSEC, otherwise
	 *         <code>false</code>.
	 */
	public static boolean isUseDnsSec()
	{
		return useDnsSec;
	}

	/**
	 * Whether or not to use JMX -- defaults to false.
	 * 
	 * @param useJmx
	 *            Whether or not to use JMX.
	 */
	public static void setUseJmx(boolean useJmx)
	{
		ProxyConfig.useJmx = useJmx;
	}

	/**
	 * Returns whether or not JMX is turned on.
	 * 
	 * @return <code>true</code> if JMX is turned on, otherwise
	 *         <code>false</code>.
	 */
	public static boolean isUseJmx()
	{
		return useJmx;
	}
}
