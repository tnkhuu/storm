/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import java.security.KeyStore;
import java.security.Security;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

/**
 * A factory for creating SslContext objects.
 */
public class SslContextFactory {

    private static final String PROTOCOL = "TLS";
    private final SSLContext SERVER_CONTEXT;
    
    /**
	 * Instantiates a new ssl context factory.
	 * 
	 * @param ksm
	 *            the ksm
	 */
    public SslContextFactory(final KeyStoreManager ksm) {
        this(ksm, null);
    }
    
    /**
	 * Instantiates a new ssl context factory.
	 * 
	 * @param ksm
	 *            the ksm
	 * @param trustManagers
	 *            the trust managers
	 */
    public SslContextFactory(final KeyStoreManager ksm, 
        final TrustManager[] trustManagers) {
        String algorithm = Security.getProperty("ssl.KeyManagerFactory.algorithm");
        if (algorithm == null) {
            algorithm = "SunX509";
        }

        SSLContext serverContext;
        try {
            final KeyStore ks = KeyStore.getInstance("JKS");
            //ks.load(new FileInputStream("keystore.jks"), "changeit".toCharArray());
            ks.load(ksm.keyStoreAsInputStream(),
                    ksm.getKeyStorePassword());

            // Set up key manager factory to use our key store
            final KeyManagerFactory kmf = 
                KeyManagerFactory.getInstance(algorithm);
            kmf.init(ks, ksm.getCertificatePassword());

            // Initialize the SSLContext to work with our key managers.
            serverContext = SSLContext.getInstance(PROTOCOL);
            serverContext.init(kmf.getKeyManagers(), trustManagers, null);
        } catch (final Exception e) {
            throw new Error(
                    "Failed to initialize the server-side SSLContext", e);
        }

        SERVER_CONTEXT = serverContext;
    }


    /**
	 * Gets the server context.
	 * 
	 * @return the server context
	 */
    public SSLContext getServerContext() {
        return SERVER_CONTEXT;
    }

}
