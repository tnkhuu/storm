/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.jboss.netty.handler.codec.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class PublicIpsOnlyRequestFilter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class PublicIpsOnlyRequestFilter implements HttpRequestFilter {
    
    private final Logger s_log = LoggerFactory.getLogger(getClass());
    
    /* (non-Javadoc)
     * @see org.storm.server.http.proxy.HttpRequestFilter#filter(org.jboss.netty.handler.codec.http.HttpRequest)
     */
    public void filter(final HttpRequest request) {
        final String host = ProxyUtils.parseHost(request);
        try {
            final InetAddress ia = InetAddress.getByName(host);
            if (NetworkUtils.isPublicAddress(ia)) {
                s_log.info("Allowing request for public address");
                return;
            } else {
                // We do this for security reasons -- we don't
                // want to allow proxies to inadvertantly expose
                // internal network services.
                s_log.warn("Request for non-public resource: {} \n full request: {}",
                    request.getUri(), request);
                throw new UnsupportedOperationException(
                    "Not a public address: "+ia);
            }
        } catch (final UnknownHostException uhe) {
            throw new UnsupportedOperationException(
                "Could not resolve host", uhe);
        }
    }

}
