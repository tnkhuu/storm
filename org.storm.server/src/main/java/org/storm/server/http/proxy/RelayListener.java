/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.handler.codec.http.HttpRequest;

/**
 * Listener for events on the HTTP traffic relayer.
 * 
 * @see RelayEvent
 */
public interface RelayListener {

    /**
	 * On relay channel close.
	 * 
	 * @param browserToProxyChannel
	 *            the browser to proxy channel
	 * @param hostAndPort
	 *            the host and port
	 * @param unansweredRequests
	 *            the unanswered requests
	 * @param closedEndsResponseBody
	 *            the closed ends response body
	 */
    void onRelayChannelClose(Channel browserToProxyChannel, String hostAndPort, 
        int unansweredRequests, boolean closedEndsResponseBody);
    
    /**
	 * On relay http response.
	 * 
	 * @param browserToProxyChannel
	 *            the browser to proxy channel
	 * @param hostAndPort
	 *            the host and port
	 * @param httpRequest
	 *            the http request
	 */
    void onRelayHttpResponse(Channel browserToProxyChannel, String hostAndPort, 
        HttpRequest httpRequest);

    /**
	 * On channel available.
	 * 
	 * @param hostAndPort
	 *            the host and port
	 * @param channelFuture
	 *            the channel future
	 */
    void onChannelAvailable(String hostAndPort, ChannelFuture channelFuture);

    /**
	 * Adds the interest ops listener.
	 * 
	 * @param opsListener
	 *            the ops listener
	 */
    void addInterestOpsListener(InterestOpsListener opsListener);

}
