/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;

/**
 * The Class ProxyHttpResponse.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ProxyHttpResponse {

    private final Object response;
    private final HttpRequest httpRequest;
    private final HttpResponse httpResponse;

    /**
	 * Instantiates a new proxy http response.
	 * 
	 * @param httpRequest
	 *            the http request
	 * @param httpResponse
	 *            the http response
	 * @param response
	 *            the response
	 */
    public ProxyHttpResponse(final HttpRequest httpRequest, 
        final HttpResponse httpResponse, final Object response) {
        this.httpRequest = httpRequest;
        this.httpResponse = httpResponse;
        this.response = response;
    }

    /**
	 * Gets the response.
	 * 
	 * @return the response
	 */
    public Object getResponse() {
        return response;
    }

    /**
	 * Gets the http request.
	 * 
	 * @return the http request
	 */
    public HttpRequest getHttpRequest() {
        return this.httpRequest;
    }

    /**
	 * Gets the http response.
	 * 
	 * @return the http response
	 */
    public HttpResponse getHttpResponse() {
        return httpResponse;
    }

}
