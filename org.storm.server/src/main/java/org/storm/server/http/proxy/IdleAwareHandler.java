package org.storm.server.http.proxy;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.timeout.IdleState;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This handles idle sockets.
 */
public class IdleAwareHandler extends IdleStateAwareChannelHandler {

    private static final Logger s_log = 
        LoggerFactory.getLogger(IdleAwareHandler.class);
    private final String handlerName;
    
    public IdleAwareHandler(final String handlerName) {
        this.handlerName = handlerName;
    }

    @Override
    public void channelIdle(final ChannelHandlerContext ctx, 
        final IdleStateEvent e) {
        if (e.getState() == IdleState.READER_IDLE) {
            s_log.debug("Got reader idle -- closing -- "+this);
            e.getChannel().close();
        } else if (e.getState() == IdleState.WRITER_IDLE) {
            s_log.debug("Got writer idle -- closing connection -- "+this);
            e.getChannel().close();
        }
    }
    
    @Override
    public String toString() {
        return "IdleAwareHandler [handlerName=" + handlerName + "]";
    }
}
