/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.util.Timer;

/**
 * A factory for creating DefaultRelayPipelineFactory objects.
 */
public class DefaultRelayPipelineFactoryFactory 
    implements RelayPipelineFactoryFactory {
    
    private final ChainProxyManager chainProxyManager;
    private final ChannelGroup channelGroup;
    private final HttpRequestFilter requestFilter;
    private final HttpResponseFilters responseFilters;
    private final Timer timer;

    /**
	 * Instantiates a new default relay pipeline factory factory.
	 * 
	 * @param chainProxyManager
	 *            the chain proxy manager
	 * @param responseFilters
	 *            the response filters
	 * @param requestFilter
	 *            the request filter
	 * @param channelGroup
	 *            the channel group
	 * @param timer
	 *            the timer
	 */
    public DefaultRelayPipelineFactoryFactory(
        final ChainProxyManager chainProxyManager, 
        final HttpResponseFilters responseFilters, 
        final HttpRequestFilter requestFilter, 
        final ChannelGroup channelGroup, final Timer timer) {
        this.chainProxyManager = chainProxyManager;
        this.responseFilters = responseFilters;
        this.channelGroup = channelGroup;
        this.requestFilter = requestFilter;
        this.timer = timer;
    }
    
    /* (non-Javadoc)
     * @see org.storm.server.http.proxy.RelayPipelineFactoryFactory#getRelayPipelineFactory(org.jboss.netty.handler.codec.http.HttpRequest, org.jboss.netty.channel.Channel, org.storm.server.http.proxy.RelayListener)
     */
    public ChannelPipelineFactory getRelayPipelineFactory(
        final HttpRequest httpRequest, final Channel browserToProxyChannel,
        final RelayListener relayListener) {
	
        String hostAndPort = chainProxyManager == null
            ? null : chainProxyManager.getChainProxy(httpRequest);
        if (hostAndPort == null) {
            hostAndPort = ProxyUtils.parseHostAndPort(httpRequest);
        }
        
        return new DefaultRelayPipelineFactory(hostAndPort, httpRequest, 
            relayListener, browserToProxyChannel, channelGroup, responseFilters, 
            requestFilter, chainProxyManager, this.timer);
    }
    
}