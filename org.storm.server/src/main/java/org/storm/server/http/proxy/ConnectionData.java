/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import javax.management.MXBean;

/**
 * Interface for JMX data on a single connection.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@MXBean(true)
public interface ConnectionData {
    
    /**
	 * Gets the client connections.
	 * 
	 * @return the client connections
	 */
    int getClientConnections();
    
    /**
	 * Gets the total client connections.
	 * 
	 * @return the total client connections
	 */
    int getTotalClientConnections();
    
    /**
	 * Gets the outgoing connections.
	 * 
	 * @return the outgoing connections
	 */
    int getOutgoingConnections();
    
    /**
	 * Gets the requests sent.
	 * 
	 * @return the requests sent
	 */
    int getRequestsSent();
    
    /**
	 * Gets the responses received.
	 * 
	 * @return the responses received
	 */
    int getResponsesReceived();
    
    /**
	 * Gets the unanswered requests.
	 * 
	 * @return the unanswered requests
	 */
    String getUnansweredRequests();
    
    /**
	 * Gets the answered reqeusts.
	 * 
	 * @return the answered reqeusts
	 */
    String getAnsweredReqeusts();
    
}
