/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import org.jboss.netty.handler.codec.http.HttpRequest;

/**
 * The listener interface for receiving proxy events. The class that is interested in processing a proxy event implements this interface, and the object created with that class is
 * registered with a component using the component's <code>addProxyListener<code> method. When
 * the proxy event occurs, that object's appropriate
 * method is invoked.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface ProxyListener
{
	
	/**
	 * Resolve.
	 * 
	 * @param req
	 *            the req
	 * @return the string
	 */
	String resolve(HttpRequest req);
}
