/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.handler.ssl.SslHandler;

/**
 * The Class SslHandshakeHandler.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SslHandshakeHandler implements HandshakeHandler {

    private final String id;
    private final SslHandler sslHandler;

    /**
	 * Instantiates a new ssl handshake handler.
	 * 
	 * @param id
	 *            the id
	 * @param sslHandler
	 *            the ssl handler
	 */
    public SslHandshakeHandler(final String id, final SslHandler sslHandler) {
        this.id = id;
        this.sslHandler = sslHandler;
    }

    /* (non-Javadoc)
     * @see org.storm.server.http.proxy.HandshakeHandler#getChannelHandler()
     */
    @Override
    public ChannelHandler getChannelHandler() {
        return this.sslHandler;
    }

    /* (non-Javadoc)
     * @see org.storm.server.http.proxy.HandshakeHandler#getId()
     */
    @Override
    public String getId() {
        return this.id;
    }

}
