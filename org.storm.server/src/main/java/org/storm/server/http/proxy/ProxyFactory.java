/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.storm.server.http.proxy;

import org.jboss.netty.handler.codec.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A factory for creating Proxy servers.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ProxyFactory
{

	private static Logger s_log = LoggerFactory.getLogger(ProxyFactory.class);

	/**
	 * Start proxy server.
	 * 
	 * @param port
	 *            the port
	 * @param chainProxyHostAndPort
	 *            the chain proxy host and port
	 * @return the http proxy server
	 */
	public static HttpProxyServer startProxyServer(int port, final String chainProxyHostAndPort)
	{
		final DefaultHttpProxyServer proxyServer = new DefaultHttpProxyServer(port, null, new ChainProxyManager() {
            public String getChainProxy(HttpRequest httpRequest) {
                return chainProxyHostAndPort;
            }

            public void onCommunicationError(String hostAndPort) {
            }
        }, null, null);
		proxyServer.start();
		return proxyServer;
	}

	public static HttpProxyServer create(final int localPort, final ProxyListener listener)
	{
		return new DefaultHttpProxyServer(localPort, null, new ChainProxyManager() {
            public String getChainProxy(HttpRequest httpRequest) {
                return listener.resolve(httpRequest);
            }

            public void onCommunicationError(String hostAndPort) {
            	s_log.error("error occured while proxying " + hostAndPort);
            }
        }, null, null);
	}
}
