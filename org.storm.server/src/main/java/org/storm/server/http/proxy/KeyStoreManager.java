/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import java.io.InputStream;

/**
 * The Interface KeyStoreManager.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface KeyStoreManager {

    /**
	 * Key store as input stream.
	 * 
	 * @return the input stream
	 */
    InputStream keyStoreAsInputStream();
    
    /**
	 * Gets the certificate password.
	 * 
	 * @return the certificate password
	 */
    char[] getCertificatePassword();
    
    /**
	 * Gets the key store password.
	 * 
	 * @return the key store password
	 */
    char[] getKeyStorePassword();

    /**
	 * Gets the base64 cert.
	 * 
	 * @param id
	 *            the id
	 * @return the base64 cert
	 */
    String getBase64Cert(String id);

}
