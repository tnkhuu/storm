/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import javax.net.ssl.SSLEngine;

import org.jboss.netty.handler.ssl.SslHandler;

/**
 * A factory for creating SslHandshakeHandler objects.
 */
public class SslHandshakeHandlerFactory implements HandshakeHandlerFactory {

    private final KeyStoreManager ksm;
    
    /**
	 * Instantiates a new ssl handshake handler factory.
	 * 
	 * @param ksm
	 *            the ksm
	 */
    public SslHandshakeHandlerFactory(final KeyStoreManager ksm) {
        this.ksm = ksm;
    }
        
    /* (non-Javadoc)
     * @see org.storm.server.http.proxy.HandshakeHandlerFactory#newHandshakeHandler()
     */
    @Override
    public HandshakeHandler newHandshakeHandler() {
        final SslContextFactory scf = new SslContextFactory(ksm);
        final SSLEngine engine = scf.getServerContext().createSSLEngine();
        engine.setUseClientMode(false);
        return new SslHandshakeHandler("ssl", new SslHandler(engine));
    }
}
