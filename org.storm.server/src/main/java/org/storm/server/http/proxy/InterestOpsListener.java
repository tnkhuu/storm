/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;

/**
 * The listener interface for receiving interestOps events. The class that is interested in processing a interestOps event implements this interface, and the object created with
 * that class is registered with a component using the component's <code>addInterestOpsListener<code> method. When
 * the interestOps event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see InterestOpsEvent
 */
public interface InterestOpsListener {

    /**
	 * Channel writable.
	 * 
	 * @param ctx
	 *            the ctx
	 * @param cse
	 *            the cse
	 */
    void channelWritable(ChannelHandlerContext ctx, ChannelStateEvent cse);

}
