/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server;

import org.storm.api.dns.DNSAsyncClient;
import org.storm.server.http.proxy.HttpProxyServer;

/**
 * The Interface StormServer.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface StormServer
{
	char COLON = ':';
	int HTTP_PORT = 80;
	String HOST_HEADER = "host";
	
	/**
	 * 
	 * @return
	 */
	DNSAsyncClient<?, ?> getDNSClient();

	/**
	 * @return the server
	 */
	HttpProxyServer getServer();

}