/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.jboss.netty.handler.codec.http.HttpRequest;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.Storm;
import org.storm.api.application.Application;
import org.storm.api.dns.DNSAsyncClient;
import org.storm.api.kernel.Actor;
import org.storm.api.services.ServiceGateway;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.cloud.application.ApplicationManager;
import org.storm.server.http.proxy.HttpProxyServer;
import org.storm.server.http.proxy.ProxyFactory;
import org.storm.server.http.proxy.ProxyListener;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * The Class StormServerImpl.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StormServerImpl extends Actor implements StormServer
{
	private static Logger s_log = LoggerFactory.getLogger(StormServerImpl.class);

	private volatile ApplicationManager applicationManager;
	private volatile ServiceGateway serviceGateway;
	private volatile BundleContext context;
	private volatile Storm stormCloud;
	private volatile DNSAsyncClient<?, ?> dnsClient;
	private HttpProxyServer server;
	private ConcurrentHashMap<String, String> applications;
	private Cache<String, Application> applicationDeployingCache;

	/** Server Initialization. */
	public void init()
	{
		applications = new ConcurrentHashMap<>();
		applicationDeployingCache = CacheBuilder.newBuilder().build();
		server = createServer();
		addDNSRecord(context);
		addDNSRecord(context, Endpoints.STORM_WWW.getDomain());
	}

	/** Server destruction. */
	public void destroy()
	{
		removeDNSRecord(context);
		removeDNSRecord(context, Endpoints.STORM_WWW.getDomain());
	}


	/** Server Starting . */
	public void start()
	{
		server.start();
		s_log.info("Manager: {} Cloud: {} Service Gateway: {}", applicationManager, stormCloud, serviceGateway);
		s_log.info("Started Storm ProxyServer @ 0.0.0.0:{}", HTTP_PORT);
	}
	

	/** Server stopping. */
	public void stop()
	{
		server.stop();
	}

	private HttpProxyServer createServer()
	{
		return ProxyFactory.create(HTTP_PORT, new ProxyListener()
		{

			@Override
			public String resolve(HttpRequest req)
			{
				String host = req.getHeader(HOST_HEADER);
				if (host.lastIndexOf(COLON) > 0)
				{
					host = host.substring(0, host.lastIndexOf(COLON));
				}

				if (applications.containsKey(host))
				{
					return applications.get(host);
				}
				else
				{
					Application app = null;
					long time = System.currentTimeMillis();
					try
					{
						final String domain = host;
						app = applicationDeployingCache.get(domain, new Callable<Application>()
						{
							@Override
							public Application call() throws Exception
							{
								Future<Application> result = stormCloud.resolve(domain);
								return result == null ? null : result.get();
							}
						});
					}
					catch (ExecutionException e)
					{
						s_log.error(e.getMessage(), e);
					}
					if (app != null)
					{
						String url = app.getProxiedAddress();
						applications.put(host, url);
						long end = System.currentTimeMillis() - time;
						s_log.info("Deployed {} in {} seconds", host, end / 1000);
						return url;
					}
					return Storm.NO_APP_EXISTS;
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.server.StormServer#getDNSClient()
	 */
	@Override
	public DNSAsyncClient<?, ?> getDNSClient()
	{
		return dnsClient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.server.StormServer#getServer()
	 */
	@Override
	public HttpProxyServer getServer()
	{
		return server;
	}
}
