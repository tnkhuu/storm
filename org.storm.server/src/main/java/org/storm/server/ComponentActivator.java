/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.fusesource.hawtdispatch.Dispatcher;
import org.osgi.framework.BundleContext;
import org.storm.api.Kernel;
import org.storm.api.Storm;
import org.storm.api.dns.DNSAsyncClient;
import org.storm.api.kernel.Roles;
import org.storm.api.services.ComputeService;
import org.storm.api.services.ServiceGateway;
import org.storm.nexus.api.Agent;
import org.storm.nexus.api.Role;
import org.storm.nexus.cloud.StormImpl;
import org.storm.nexus.cloud.application.ApplicationManager;
import org.storm.nexus.cloud.application.ApplicationManagerImpl;

/**
 * The Component Activator for this bundle.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator extends DependencyActivatorBase
{

	
	/**
	 * Core Component Service Initialization and Dependency Injection.
	 */
	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception
	{
		if(Roles.getRole() == Role.PROXY) {
			
		manager.add(createComponent().setInterface(ApplicationManager.class.getName(), null)
									 .add(createServiceDependency().setService(Kernel.class).setRequired(true))
									 .add(createServiceDependency().setService(ServiceGateway.class).setRequired(true))
									 .add(createServiceDependency().setService(Agent.class).setRequired(true))
									 .setImplementation(ApplicationManagerImpl.class));
		
		manager.add(createComponent().setInterface(Storm.class.getName(), null)
				 					 .add(createServiceDependency().setService(Kernel.class).setRequired(true))
				 					 .add(createServiceDependency().setService(ComputeService.class).setRequired(true))
				 					 .add(createServiceDependency().setService(ApplicationManager.class).setRequired(true))
				 					 .setImplementation(StormImpl.class));
		
		manager.add(createComponent().setInterface(StormServer.class.getName(), null)
									 .add(createServiceDependency().setService(Kernel.class).setRequired(true))
									 .add(createServiceDependency().setService(ComputeService.class).setRequired(true))
									 .add(createServiceDependency().setService(Dispatcher.class).setRequired(true))
									 .add(createServiceDependency().setService(ServiceGateway.class).setRequired(true))
									 .add(createServiceDependency().setService(DNSAsyncClient.class).setRequired(true))
									 .add(createServiceDependency().setService(Storm.class).setRequired(true))
									 .setImplementation(StormServerImpl.class));
		}

	}

	/**
	 * Core Component Service de-construction and clean up.
	 */
	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception
	{
		

	}

}
