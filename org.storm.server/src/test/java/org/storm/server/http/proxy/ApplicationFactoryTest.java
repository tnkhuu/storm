/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.proxy;

import java.net.URI;

import junit.framework.Assert;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.storm.api.application.Application;
import org.storm.nexus.cloud.application.ApplicationFactory;
import org.storm.nexus.cloud.application.WebArchiveApplication;

public class ApplicationFactoryTest
{
	private String JSON_APP = "{\"domainName\":\"www.stormclowd.com\",\"proxiedAddress\":\"10.0.3.10\",\"appBinaries\":\"http://artifacts.stormclowd.com/drop/stormclowd.war\",\"uri\":\"http://artifacts.stormclowd.com/drop/stormclowd.war\",\"running\":true}";
	
	/**
	 * Test can serialize application.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testCanSerializeApplication() throws Exception {
		Application app = ApplicationFactory.build("www.stormclowd.com", "10.0.3.10", true, new URI("http://artifacts.stormclowd.com/drop/stormclowd.war"), "http://artifacts.stormclowd.com/drop/stormclowd.war");
		
		ObjectMapper mapper = new ObjectMapper();
		String value = mapper.writeValueAsString(app);
		Assert.assertNotNull(value);
	}
	
	@Test
	public void testCanDeserializeApplication() throws Exception {
		Application app = ApplicationFactory.build("www.stormclowd.com", "10.0.3.10", true, new URI("http://artifacts.stormclowd.com/drop/stormclowd.war"), "http://artifacts.stormclowd.com/drop/stormclowd.war");
		
		ObjectMapper mapper = new ObjectMapper();
		Application value = mapper.readValue(JSON_APP, WebArchiveApplication.class);
		Assert.assertEquals(app.getAppBinaries(), value.getAppBinaries());
		Assert.assertEquals(app.getDomainName(), value.getDomainName());
		Assert.assertEquals(app.getProxiedAddress(), value.getProxiedAddress());
		Assert.assertEquals(app.getURI(), value.getURI());
		Assert.assertEquals(app.running(), value.running());
	}
}
