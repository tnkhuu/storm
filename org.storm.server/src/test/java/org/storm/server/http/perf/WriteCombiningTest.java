/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.server.http.perf;

import static java.lang.System.out;

/**
 * Test of the effects of write combining on cpu cache lines.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public final class WriteCombiningTest
{

	/** The Constant ITERATIONS. */
	private static final int ITERATIONS = Integer.MAX_VALUE;

	/** The Constant ITEMS. */
	private static final int ITEMS = 1 << 24;

	/** The Constant MASK. */
	private static final int MASK = ITEMS - 1;

	/** The Constant arrayA. */
	private static final byte[] arrayA = new byte[ITEMS];

	/** The Constant arrayB. */
	private static final byte[] arrayB = new byte[ITEMS];

	/** The Constant arrayC. */
	private static final byte[] arrayC = new byte[ITEMS];

	/** The Constant arrayD. */
	private static final byte[] arrayD = new byte[ITEMS];

	/** The Constant arrayE. */
	private static final byte[] arrayE = new byte[ITEMS];

	/** The Constant arrayF. */
	private static final byte[] arrayF = new byte[ITEMS];

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		for (int i = 1; i <= 3; i++)
		{
			out.println(i + " SingleLoop duration (ns) = " + runCaseOne());
			out.println(i + " SplitLoop  duration (ns) = " + runCaseTwo());
		}

		for (int i = 1; i <= 3; i++)
		{
			out.println(i + " SingleLoop duration (ns) = " + runCaseOne());
			out.println(i + " SplitLoop  duration (ns) = " + runCaseTwo());
		}

		int result = arrayA[1] + arrayB[2] + arrayC[3] + arrayD[4] + arrayE[5] + arrayF[6];
		out.println("result = " + result);
	}

	/**
	 * Run case one.
	 * 
	 * @return the long
	 */
	public static long runCaseOne()
	{
		long start = System.nanoTime();

		int i = ITERATIONS;
		while (--i != 0)
		{
			int slot = i & MASK;
			byte b = (byte) i;
			arrayA[slot] = b;
			arrayB[slot] = b;
			arrayC[slot] = b;
			arrayD[slot] = b;
			arrayE[slot] = b;
			arrayF[slot] = b;
		}

		return System.nanoTime() - start;
	}

	/**
	 * Run case two.
	 * 
	 * @return the long
	 */
	public static long runCaseTwo()
	{
		long start = System.nanoTime();

		int i = ITERATIONS;
		while (--i != 0)
		{
			int slot = i & MASK;
			byte b = (byte) i;
			arrayA[slot] = b;
			arrayB[slot] = b;
			arrayC[slot] = b;
		}

		i = ITERATIONS;
		while (--i != 0)
		{
			int slot = i & MASK;
			byte b = (byte) i;
			arrayD[slot] = b;
			arrayE[slot] = b;
			arrayF[slot] = b;
		}

		return System.nanoTime() - start;
	}
}