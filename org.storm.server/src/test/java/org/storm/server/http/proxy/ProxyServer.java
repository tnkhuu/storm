package org.storm.server.http.proxy;

import org.jboss.netty.handler.codec.http.HttpRequest;

public class ProxyServer
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
	HttpProxyServer serv =	ProxyFactory.create(8080, new ProxyListener()
		{

			@Override
			public String resolve(HttpRequest req)
			{
				return "google.com:80";
			}
		});
	serv.start();
	while(true) {
		
	}
	}

}
