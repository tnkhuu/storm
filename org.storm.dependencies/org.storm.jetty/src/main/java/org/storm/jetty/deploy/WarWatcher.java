/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.jetty.deploy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The  WarWatcher.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class WarWatcher
{
	private static final Logger s_log = LoggerFactory.getLogger(WarWatcher.class);

	/**
	 * Instantiates a new war watcher.
	 * 
	 * @param watchedDir
	 *            the watched dir
	 * @param watchService
	 *            the watch service
	 */
	public WarWatcher(String watchedDir, WatchService watchService)
	{
		File watchDirFile = new File(watchedDir);
		Path watchDirPath = watchDirFile.toPath();
		try
		{
			watchDirPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
		}
	}

}
