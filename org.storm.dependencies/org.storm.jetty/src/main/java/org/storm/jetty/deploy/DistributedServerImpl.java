/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.jetty.deploy;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.eclipse.jetty.osgi.boot.warurl.WarUrlStreamHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.WebAppClassLoader;
import org.eclipse.jetty.webapp.WebAppContext;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.service.url.URLConstants;
import org.osgi.service.url.URLStreamHandlerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.kernel.Actor;
import org.storm.api.server.DistributedServer;
import org.storm.api.server.Load;
import org.storm.api.services.DistributedService;
import org.storm.nexus.api.Role;

/**
 * A Distributed Server.
 * 
 * @author Trung Khuu
 * @since 1.0
 * 
 */
public class DistributedServerImpl extends Actor implements DistributedServer, DistributedService, BundleActivator
{
	private static final Logger s_log = LoggerFactory.getLogger(DistributedServerImpl.class);
	private BundleContext context;
	private Map<String, Server> servers = new HashMap<String, Server>();
	private Load currentLoad = Load.NONE;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jetty.server.DistributedServer#deploy(java.io.File)
	 */
	@Override
	public int deploy(String domainName, String war, String contextPath)
	{
		try
		{
			Server server = new Server(getUnusedPort());
			ClassLoader cl = context.getBundle().adapt(BundleWiring.class).getClassLoader();
			s_log.info("Deploying WAR {}", war);
			WebAppContext webapp = new WebAppContext();
			WebAppClassLoader classloader = new WebAppClassLoader(cl, webapp);
			webapp.setContextPath(contextPath);
			webapp.setWar(war);
			webapp.setClassLoader(classloader);
			server.setHandler(webapp);
			server.start();
			servers.put(war, server);
			ServerConnector conn = (ServerConnector) server.getServer().getConnectors()[0];

			addDNSRecord(context, domainName + ".");
			return conn.getPort();
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
			return -1;

		}

	}

	@Override
	public boolean destroy()
	{
		stop();
		return false;
	}

	@Override
	public boolean stop()
	{
		boolean success = true;
		if (servers != null)
		{
			for (Server s : servers.values())
			{
				try
				{
					s.stop();
				}
				catch (Exception e)
				{
					success = false;
					s_log.error(e.getMessage(), e);
				}
			}
		}
		return success;
	}

	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	@Override
	public void start(final BundleContext context) throws Exception
	{
		if (getRole() == Role.SERVER)
		{
			this.context = context;

			context.registerService(new String[]
			{ DistributedService.class.getName(), DistributedServer.class.getName() }, this, null);
			Dictionary props = new Hashtable();
			props.put(URLConstants.URL_HANDLER_PROTOCOL, new String[]
			{ "war" });
			context.registerService(URLStreamHandlerService.class.getName(), new WarUrlStreamHandler(), props);
		}

	}

	@Override
	public void stop(BundleContext context) throws Exception
	{
		stop();
	}

	@Override
	public String address()
	{
		return getOutboundAddress();
	}

	public static int getUnusedPort()
	{
		while (true)
		{
			int port = (int) (65536 * Math.random());
			try
			{
				Socket s = new Socket("localhost", port);
				s.close();
			}
			catch (ConnectException e)
			{
				return port;
			}
			catch (IOException e)
			{

			}
		}
	}

	@Override
	public boolean isRunning(String war)
	{
		return servers.containsKey(war);
	}

	@Override
	public boolean stop(String war)
	{
		boolean success = false;
		if (isRunning(war))
		{
			try
			{
				servers.get(war).stop();
				servers.remove(war);
				success = true;
			}
			catch (Exception e)
			{
				s_log.error(e.getMessage(), e);
			}
		}
		return success;
	}

	@Override
	public Load currentLoad()
	{
		return currentLoad;
	}
}
