/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.jetty.deploy;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.jetty.osgi.boot.OSGiServerConstants;
import org.eclipse.jetty.osgi.boot.internal.serverfactory.JettyServerServiceTracker;
import org.eclipse.jetty.webapp.WebAppContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.BundleTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * WAR Deployer
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
public class StormJettyDeployer
{

	private static final Logger s_log = LoggerFactory.getLogger(StormJettyDeployer.class);
	private BundleContext context;
	private FileSystem filesystem = FileSystems.getDefault();
	private ExecutorService executor = Executors.newCachedThreadPool();
	private WebAppBundleTracker deployer;
	private BundleTracker webappTracker;
	private WarWatcher watcher;
	private WarDeployer warDeployer;
	private WatchService watchService;
	private JettyServerServiceTracker jettyServerService;
	private volatile boolean isRunning = false;

	/**
	 * Instantiates a new storm jetty deployer.
	 * 
	 * @param context
	 *            the context
	 * @throws Exception
	 */

	protected StormJettyDeployer(BundleContext context, JettyServerServiceTracker tracker) throws Exception
	{
		this.context = context;
		this.isRunning = true;
		this.deployer = new WebAppBundleTracker();
		this.jettyServerService = tracker;
		this.webappTracker = new BundleTracker(context, Bundle.ACTIVE | Bundle.STOPPING, deployer);
		this.watchService = getWatchService();
		String home = System.getProperty("STORM_HOME");
		this.watcher = new WarWatcher(home + "/webapps", watchService);
		this.warDeployer = new WarDeployer();
	}

	private class WarDeployer implements Runnable
	{

		@Override
		public void run()
		{
			while (isRunning)
			{
				WatchKey keyevent = watchService.poll();
				if (keyevent == null)
				{
					continue;
				}
				List<WatchEvent<?>> events = keyevent.pollEvents();
				if ((events != null) && (events.size() > 0))
				{
					for (WatchEvent<?> watch : events)
					{
						if (watch.kind() == (ENTRY_CREATE))
						{
							Path eventPath = (Path) watch.context();
							if (eventPath != null)
							{
								WebAppContext webapp = new WebAppContext();
								webapp.setContextPath("/");
								webapp.setWar(eventPath.toFile().getAbsolutePath());
								jettyServerService.getServerInstanceWrapper(OSGiServerConstants.MANAGED_JETTY_SERVER_DEFAULT_NAME).getServer().setHandler(webapp);
							}
						}
						else if (watch.kind() == ENTRY_DELETE)
						{

						}
						else if (watch.kind() == ENTRY_MODIFY)
						{

						}
					}
				}
			}

		}

	}

	/**
	 * Retrieve the native watch service provided by the nio package.
	 * 
	 * @return the native watch service.
	 */
	public WatchService getWatchService()
	{
		try
		{
			return filesystem.newWatchService();
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}

}
