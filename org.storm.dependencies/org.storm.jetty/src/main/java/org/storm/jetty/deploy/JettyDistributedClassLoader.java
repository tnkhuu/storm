/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.jetty.deploy;

import java.io.IOException;

import org.osgi.framework.Bundle;
import org.storm.api.server.DistributedServerClassLoader;

/**
 * 
 * @author Trung Khuu
 * @since 1.0
 *
 */
public class JettyDistributedClassLoader implements DistributedServerClassLoader {

	
	private ClassLoader parent;
	
	private Bundle contribBundle;
	/**
	 * Instantiates a new jetty distributed class loader.
	 * 
	 * @param parent
	 *            the parent
	 * @param webAppContext
	 *            the web app context
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JettyDistributedClassLoader(ClassLoader parent, Bundle contribBundle)
			throws IOException {
		this.parent = parent;
		this.contribBundle = contribBundle;
	}

	@Override
	public ClassLoader getJettyClassLoader() {
		return parent;
	}
	
	public Bundle getContribBundle() {
		return contribBundle;
	}

}
