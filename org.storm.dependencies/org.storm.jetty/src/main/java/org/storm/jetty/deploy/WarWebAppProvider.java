//
//  ========================================================================
//  Copyright (c) 1995-2013 Mort Bay Consulting Pty. Ltd.
//  ------------------------------------------------------------------------
//  All rights reserved. This program and the accompanying materials
//  are made available under the terms of the Eclipse Public License v1.0
//  and Apache License v2.0 which accompanies this distribution.
//
//      The Eclipse Public License is available at
//      http://www.eclipse.org/legal/epl-v10.html
//
//      The Apache License v2.0 is available at
//      http://www.opensource.org/licenses/apache2.0.php
//
//  You may elect to redistribute this code under either of these licenses.
//  ========================================================================
//

package org.storm.jetty.deploy;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.eclipse.jetty.deploy.App;
import org.eclipse.jetty.osgi.boot.AbstractWebAppProvider;
import org.eclipse.jetty.osgi.boot.BundleProvider;
import org.eclipse.jetty.osgi.boot.OSGiServerConstants;
import org.eclipse.jetty.osgi.boot.OSGiWebappConstants;
import org.eclipse.jetty.osgi.boot.internal.serverfactory.ServerInstanceWrapper;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;

/**
 * BundleWebAppProvider
 * 
 * A Jetty Provider that knows how to deploy a WebApp contained inside a Bundle.
 * The Bundle Activator.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({ "rawtypes" })
public class WarWebAppProvider extends AbstractWebAppProvider implements BundleProvider
{
	private static final Logger s_log = Log.getLogger(AbstractWebAppProvider.class);
	private Map<Bundle, App> bundleMap = new HashMap<Bundle, App>();
	private ServiceRegistration serviceRegForBundles;
	
	public WarWebAppProvider(ServerInstanceWrapper wrapper)
	{
		super(wrapper);
	}


	@Override
	protected void doStart() throws Exception
	{
		Dictionary<String, String> properties = new Hashtable<String, String>();
		properties.put(OSGiServerConstants.MANAGED_JETTY_SERVER_NAME, getServerInstanceWrapper().getManagedServerName());
		serviceRegForBundles = FrameworkUtil.getBundle(this.getClass()).getBundleContext().registerService(BundleProvider.class.getName(), this, properties);
		super.doStart();
	}


	@Override
	protected void doStop() throws Exception
	{
		if (serviceRegForBundles != null)
		{
			try
			{
				serviceRegForBundles.unregister();
				serviceRegForBundles = null;
			}
			catch (Exception e)
			{
				s_log.warn(e);
			}
		}

		super.doStop();
	}

	/* ------------------------------------------------------------ */
	/**
	 * A bundle has been added that could be a webapp
	 * 
	 * @param bundle
	 */
	@Override
	public boolean bundleAdded(Bundle bundle) throws Exception
	{
		if (bundle == null)
		{
			return false;
		}

		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(getServerInstanceWrapper().getParentClassLoaderForWebapps());
		String contextPath = null;
		try
		{
			Dictionary headers = bundle.getHeaders();

			// does the bundle have a OSGiWebappConstants.JETTY_WAR_FOLDER_PATH
			if (headers.get(OSGiWebappConstants.JETTY_WAR_FOLDER_PATH) != null)
			{
				String base = (String) headers.get(OSGiWebappConstants.JETTY_WAR_FOLDER_PATH);
				contextPath = getContextPath(bundle);
				String originId = getOriginId(bundle, base);

				// TODO : we don't know whether an app is actually deployed, as
				// deploymentManager swallows all
				// exceptions inside the impl of addApp. Need to send the Event
				// and also register as a service
				// only if the deployment succeeded
				OSGiApp app = new OSGiApp(getDeploymentManager(), this, bundle, originId);
				app.setWebAppPath(base);
				app.setContextPath(contextPath);
				bundleMap.put(bundle, app);
				getDeploymentManager().addApp(app);
				return true;
			}

			// does the bundle have a WEB-INF/web.xml
			if (bundle.getEntry("/WEB-INF/web.xml") != null)
			{
				String base = ".";
				contextPath = getContextPath(bundle);
				String originId = getOriginId(bundle, base);

				OSGiApp app = new OSGiApp(getDeploymentManager(), this, bundle, originId);
				app.setContextPath(contextPath);
				app.setWebAppPath(base);
				bundleMap.put(bundle, app);
				getDeploymentManager().addApp(app);
				return true;
			}

			// does the bundle define a
			// OSGiWebappConstants.RFC66_WEB_CONTEXTPATH
			if (headers.get(OSGiWebappConstants.RFC66_WEB_CONTEXTPATH) != null)
			{
				// Could be a static webapp with no web.xml
				String base = ".";
				contextPath = (String) headers.get(OSGiWebappConstants.RFC66_WEB_CONTEXTPATH);
				String originId = getOriginId(bundle, base);

				OSGiApp app = new OSGiApp(getDeploymentManager(), this, bundle, originId);
				app.setContextPath(contextPath);
				app.setWebAppPath(base);
				bundleMap.put(bundle, app);
				getDeploymentManager().addApp(app);
				return true;
			}

			return false;
		}
		catch (Exception e)
		{

			throw e;
		}
		finally
		{
			Thread.currentThread().setContextClassLoader(cl);
		}
	}

	/* ------------------------------------------------------------ */
	/**
	 * Bundle has been removed. If it was a webapp we deployed, undeploy it.
	 * 
	 * @param bundle
	 * 
	 * @return true if this was a webapp we had deployed, false otherwise
	 */
	@Override
	public boolean bundleRemoved(Bundle bundle) throws Exception
	{
		App app = bundleMap.remove(bundle);
		if (app != null)
		{
			getDeploymentManager().removeApp(app);
			return true;
		}
		return false;
	}

	/* ------------------------------------------------------------ */
	private static String getContextPath(Bundle bundle)
	{
		Dictionary<?, ?> headers = bundle.getHeaders();
		String contextPath = (String) headers.get(OSGiWebappConstants.RFC66_WEB_CONTEXTPATH);
		if (contextPath == null)
		{
			// extract from the last token of the bundle's location:
			// (really ?could consider processing the symbolic name as an
			// alternative
			// the location will often reflect the version.
			// maybe this is relevant when the file is a war)
			String location = bundle.getLocation();
			String toks[] = location.replace('\\', '/').split("/");
			contextPath = toks[toks.length - 1];
			// remove .jar, .war etc:
			int lastDot = contextPath.lastIndexOf('.');
			if (lastDot != -1)
			{
				contextPath = contextPath.substring(0, lastDot);
			}
		}
		if (!contextPath.startsWith("/"))
		{
			contextPath = "/" + contextPath;
		}

		return contextPath;
	}

}
