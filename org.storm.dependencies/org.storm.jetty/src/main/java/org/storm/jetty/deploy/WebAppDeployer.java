/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.jetty.deploy;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jetty.osgi.boot.BundleProvider;
import org.eclipse.jetty.osgi.boot.OSGiServerConstants;
import org.eclipse.jetty.osgi.boot.utils.WebappRegistrationCustomizer;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.BundleTracker;
import org.osgi.util.tracker.BundleTrackerCustomizer;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * War deployer.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("rawtypes")
public class WebAppDeployer implements BundleTrackerCustomizer
{
	private Logger s_log = LoggerFactory.getLogger(WebAppDeployer.class);
	public static Collection<WebappRegistrationCustomizer> JSP_REGISTRATION_HELPERS = new ArrayList<WebappRegistrationCustomizer>();
	public static final String FILTER = "(&(objectclass=" + BundleProvider.class.getName() + ")" + "(" + OSGiServerConstants.MANAGED_JETTY_SERVER_NAME + "="
			+ OSGiServerConstants.MANAGED_JETTY_SERVER_DEFAULT_NAME + "))";
	private BundleTracker bundleTracker;
	private ServiceTracker serviceTracker;
	
	@SuppressWarnings("unchecked")
	public WebAppDeployer() throws InvalidSyntaxException {
		
		Bundle myBundle = FrameworkUtil.getBundle(this.getClass());
		serviceTracker	= new ServiceTracker(myBundle.getBundleContext(), FrameworkUtil.createFilter(FILTER), null)
		{
			@Override
			public Object addingService(ServiceReference reference)
			{
				Object object = super.addingService(reference);
				openBundleTracker();
				return object;
			}
		};
		serviceTracker.open();
	}
	
	@Override
	public Object addingBundle(Bundle bundle, BundleEvent event)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void modifiedBundle(Bundle bundle, BundleEvent event, Object object)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removedBundle(Bundle bundle, BundleEvent event, Object object)
	{
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Track.
	 * 
	 * @param tracker
	 *            the tracker
	 */
	public void track(BundleTracker tracker) {
		if(this.bundleTracker == null)
		{
			this.bundleTracker = tracker;
			openBundleTracker();
		}
	}
	
	private void openBundleTracker()
	{
		if (bundleTracker != null && serviceTracker.getServices() != null && serviceTracker.getServices().length > 0)
		{
			bundleTracker.open();
			s_log.debug("Bundle tracker has been opened");
		}
	}

}
