/**
 * Copyright (C) 2012 FuseSource, Inc.
 * http://fusesource.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fusesource.hawtdispatch.internal;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.fusesource.hawtdispatch.Dispatcher;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * OSGi integration point.
 * 
 */
public class ComponentActivator extends DependencyActivatorBase implements BundleActivator
{

	private Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);


	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception
	{
		try
		{
			DispatcherConfig.getDefaultDispatcher().restart();
		}
		catch (IllegalStateException ignore)
		{
			// dispatchers initial state is running.. so a restart
			// only works after it's been shutdown.
		}

		manager.add(createComponent().setInterface(Dispatcher.class.getName(), null).setImplementation(HawtDispatcher.class));
		s_log.info("Global Dispatcher Registered into the Dependency Manager");

	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception
	{
		

	}

}
