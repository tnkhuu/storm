/**
 * 
 */
package org.openflow.io;

import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.openflow.protocol.OFHello;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.factory.BasicFactory;

/**
 * @author Rob Sherwood (rob.sherwood@stanford.edu)
 * 
 */
public class OFMessageAsyncStreamTest extends TestCase
{
	public void testMarshalling() throws Exception
	{
		OFMessage h = new OFHello();

		ServerSocketChannel serverSC = ServerSocketChannel.open();
		serverSC.socket().bind(new java.net.InetSocketAddress(0));
		serverSC.configureBlocking(false);

		SocketChannel client = SocketChannel.open(new InetSocketAddress("localhost", serverSC.socket().getLocalPort()));
		SocketChannel server = serverSC.accept();
		OFMessageAsyncStream clientStream = new OFMessageAsyncStream(client, new BasicFactory());
		OFMessageAsyncStream serverStream = new OFMessageAsyncStream(server, new BasicFactory());

		clientStream.write(h);
		while (clientStream.needsFlush())
		{
			clientStream.flush();
		}
		List<OFMessage> l = serverStream.read();
		Assert.assertEquals(l.size(), 1);
		OFMessage m = l.get(0);
		Assert.assertEquals(m.getLength(), h.getLength());
		Assert.assertEquals(m.getVersion(), h.getVersion());
		Assert.assertEquals(m.getType(), h.getType());
		Assert.assertEquals(m.getType(), h.getType());
	}
}
