package org.openflow.protocol;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

public class OFTypeTest extends TestCase
{

	public void testOFTypeCreate() throws Exception
	{
		OFType foo = OFType.HELLO;
		Class<? extends OFMessage> c = foo.toClass();
		Assert.assertEquals(c, OFHello.class);
	}

	@Test
	public void testMapping() throws Exception
	{
		Assert.assertEquals(OFType.HELLO, OFType.valueOf((byte) 0));
		Assert.assertEquals(OFType.BARRIER_REPLY, OFType.valueOf((byte) 19));
	}
}
