package org.openflow.protocol.queue;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

public class OFQueuePropertyTypeTest extends TestCase
{
	@Test
	public void testMapping() throws Exception
	{
		Assert.assertEquals(OFQueuePropertyType.NONE, OFQueuePropertyType.valueOf((short) 0));
		Assert.assertEquals(OFQueuePropertyType.MIN_RATE, OFQueuePropertyType.valueOf((short) 1));
	}
}
