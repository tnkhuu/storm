package org.openflow.protocol;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;
import org.openflow.protocol.action.OFActionType;

public class OFActionTypeTest extends TestCase
{
	@Test
	public void testMapping() throws Exception
	{
		Assert.assertEquals(OFActionType.OUTPUT, OFActionType.valueOf((short) 0));
		Assert.assertEquals(OFActionType.OPAQUE_ENQUEUE, OFActionType.valueOf((short) 11));
		Assert.assertEquals(OFActionType.VENDOR, OFActionType.valueOf((short) 0xffff));
	}
}
