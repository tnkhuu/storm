package org.openflow.util;

import junit.framework.Assert;
import junit.framework.TestCase;

public class U8Test extends TestCase
{
	/**
	 * Tests that we correctly translate unsigned values in and out of a byte
	 * 
	 * @throws Exception
	 */
	public void test() throws Exception
	{
		short val = 0xff;
		Assert.assertEquals(-1, U8.t(val));
		Assert.assertEquals(val, U8.f((byte) -1));
	}
}
