/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt.jna;

import com.sun.jna.Structure;

/**
 * The Class virDomainJobInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class virDomainJobInfo extends Structure
{

	/** The type. */
	public int type;

	/** The time elapsed. */
	public long timeElapsed;

	/** The time remaining. */
	public long timeRemaining;

	/** The data total. */
	public long dataTotal;

	/** The data processed. */
	public long dataProcessed;

	/** The data remaining. */
	public long dataRemaining;

	/** The mem total. */
	public long memTotal;

	/** The mem processed. */
	public long memProcessed;

	/** The mem remaining. */
	public long memRemaining;

	/** The file total. */
	public long fileTotal;

	/** The file processed. */
	public long fileProcessed;

	/** The file remaining. */
	public long fileRemaining;
}
