/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virDomainJobInfo;

/**
 * The Class DomainJobInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DomainJobInfo
{

	/** The type. */
	protected int type;

	/** The time elapsed. */
	protected long timeElapsed;

	/** The time remaining. */
	protected long timeRemaining;

	/** The data total. */
	protected long dataTotal;

	/** The data processed. */
	protected long dataProcessed;

	/** The data remaining. */
	protected long dataRemaining;

	/** The mem total. */
	protected long memTotal;

	/** The mem processed. */
	protected long memProcessed;

	/** The mem remaining. */
	protected long memRemaining;

	/** The file total. */
	protected long fileTotal;

	/** The file processed. */
	protected long fileProcessed;

	/** The file remaining. */
	protected long fileRemaining;

	/**
	 * Instantiates a new domain job info.
	 * 
	 * @param info
	 *            the info
	 */
	public DomainJobInfo(virDomainJobInfo info)
	{
		type = info.type;
		timeElapsed = info.timeElapsed;
		timeRemaining = info.timeRemaining;
		dataTotal = info.dataTotal;
		dataProcessed = info.dataProcessed;
		dataRemaining = info.dataRemaining;
		memTotal = info.memTotal;
		memProcessed = info.memProcessed;
		memRemaining = info.memRemaining;
		fileTotal = info.fileTotal;
		fileProcessed = info.fileProcessed;
		fileRemaining = info.fileRemaining;
	}

	/**
	 * Gets the data processed.
	 * 
	 * @return the data processed
	 */
	public long getDataProcessed()
	{
		return dataProcessed;
	}

	/**
	 * Gets the data remaining.
	 * 
	 * @return the data remaining
	 */
	public long getDataRemaining()
	{
		return dataRemaining;
	}

	/**
	 * Gets the data total.
	 * 
	 * @return the data total
	 */
	public long getDataTotal()
	{
		return dataTotal;
	}

	/**
	 * Gets the file processed.
	 * 
	 * @return the file processed
	 */
	public long getFileProcessed()
	{
		return fileProcessed;
	}

	/**
	 * Gets the file remaining.
	 * 
	 * @return the file remaining
	 */
	public long getFileRemaining()
	{
		return fileRemaining;
	}

	/**
	 * Gets the file total.
	 * 
	 * @return the file total
	 */
	public long getFileTotal()
	{
		return fileTotal;
	}

	/**
	 * Gets the mem processed.
	 * 
	 * @return the mem processed
	 */
	public long getMemProcessed()
	{
		return memProcessed;
	}

	/**
	 * Gets the mem remaining.
	 * 
	 * @return the mem remaining
	 */
	public long getMemRemaining()
	{
		return memRemaining;
	}

	/**
	 * Gets the mem total.
	 * 
	 * @return the mem total
	 */
	public long getMemTotal()
	{
		return memTotal;
	}

	/**
	 * Gets the time elapsed.
	 * 
	 * @return the time elapsed
	 */
	public long getTimeElapsed()
	{
		return timeElapsed;
	}

	/**
	 * Gets the time remaining.
	 * 
	 * @return the time remaining
	 */
	public long getTimeRemaining()
	{
		return timeRemaining;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public int getType()
	{
		return type;
	}

	/**
	 * Sets the data processed.
	 * 
	 * @param dataProcessed
	 *            the new data processed
	 */
	public void setDataProcessed(long dataProcessed)
	{
		this.dataProcessed = dataProcessed;
	}

	/**
	 * Sets the data remaining.
	 * 
	 * @param dataRemaining
	 *            the new data remaining
	 */
	public void setDataRemaining(long dataRemaining)
	{
		this.dataRemaining = dataRemaining;
	}

	/**
	 * Sets the data total.
	 * 
	 * @param dataTotal
	 *            the new data total
	 */
	public void setDataTotal(long dataTotal)
	{
		this.dataTotal = dataTotal;
	}

	/**
	 * Sets the file processed.
	 * 
	 * @param fileProcessed
	 *            the new file processed
	 */
	public void setFileProcessed(long fileProcessed)
	{
		this.fileProcessed = fileProcessed;
	}

	/**
	 * Sets the file remaining.
	 * 
	 * @param fileRemaining
	 *            the new file remaining
	 */
	public void setFileRemaining(long fileRemaining)
	{
		this.fileRemaining = fileRemaining;
	}

	/**
	 * Sets the file total.
	 * 
	 * @param fileTotal
	 *            the new file total
	 */
	public void setFileTotal(long fileTotal)
	{
		this.fileTotal = fileTotal;
	}

	/**
	 * Sets the mem processed.
	 * 
	 * @param memProcessed
	 *            the new mem processed
	 */
	public void setMemProcessed(long memProcessed)
	{
		this.memProcessed = memProcessed;
	}

	/**
	 * Sets the mem remaining.
	 * 
	 * @param memRemaining
	 *            the new mem remaining
	 */
	public void setMemRemaining(long memRemaining)
	{
		this.memRemaining = memRemaining;
	}

	/**
	 * Sets the mem total.
	 * 
	 * @param memTotal
	 *            the new mem total
	 */
	public void setMemTotal(long memTotal)
	{
		this.memTotal = memTotal;
	}

	/**
	 * Sets the time elapsed.
	 * 
	 * @param timeElapsed
	 *            the new time elapsed
	 */
	public void setTimeElapsed(long timeElapsed)
	{
		this.timeElapsed = timeElapsed;
	}

	/**
	 * Sets the time remaining.
	 * 
	 * @param timeRemaining
	 *            the new time remaining
	 */
	public void setTimeRemaining(long timeRemaining)
	{
		this.timeRemaining = timeRemaining;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(int type)
	{
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String
				.format("type:%d%ntimeElapsed:%d%ntimeRemaining:%d%ndataTotal:%d%ndataProcessed:%d%ndataRemaining:%d%nmemTotal:%d%nmemProcessed:%d%nmemRemaining:%d%nfileTotal:%d%nfileProcessed:%d%nfileRemaining:%d%n",
						type, timeElapsed, timeRemaining, dataTotal, dataProcessed, dataRemaining, memTotal, memProcessed, memRemaining, fileTotal, fileProcessed, fileRemaining);
	}
}
