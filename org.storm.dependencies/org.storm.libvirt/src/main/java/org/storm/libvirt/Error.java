/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import java.io.Serializable;

import org.storm.libvirt.jna.ConnectionPointer;
import org.storm.libvirt.jna.DomainPointer;
import org.storm.libvirt.jna.NetworkPointer;
import org.storm.libvirt.jna.virError;

/**
 * The Class Error.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Error implements Serializable
{

	/**
	 * The Enum ErrorDomain.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum ErrorDomain
	{

		/** The vir from none. */
		VIR_FROM_NONE,
		/** The vir from xen. */
		VIR_FROM_XEN, /* Error at Xen hypervisor layer */
		/** The vir from xend. */
		VIR_FROM_XEND, /* Error at connection with xend daemon */
		/** The vir from xenstore. */
		VIR_FROM_XENSTORE, /* Error at connection with xen store */
		/** The vir from sexpr. */
		VIR_FROM_SEXPR, /* Error in the S-Expression code */
		/** The vir from xml. */
		VIR_FROM_XML, /* Error in the XML code */
		/** The vir from dom. */
		VIR_FROM_DOM, /* Error when operating on a domain */
		/** The vir from rpc. */
		VIR_FROM_RPC, /* Error in the XML-RPC code */
		/** The vir from proxy. */
		VIR_FROM_PROXY, /* Error in the proxy code */
		/** The vir from conf. */
		VIR_FROM_CONF, /* Error in the configuration file handling */
		/** The vir from qemu. */
		VIR_FROM_QEMU, /* Error at the QEMU daemon */
		/** The vir from net. */
		VIR_FROM_NET, /* Error when operating on a network */
		/** The vir from test. */
		VIR_FROM_TEST, /* Error from test driver */
		/** The vir from remote. */
		VIR_FROM_REMOTE, /* Error from remote driver */
		/** The vir from openvz. */
		VIR_FROM_OPENVZ, /* Error from OpenVZ driver */
		/** The vir from xenxm. */
		VIR_FROM_XENXM, /* Error at Xen XM layer */
		/** The vir from stats linux. */
		VIR_FROM_STATS_LINUX, /* Error in the Linux Stats code */
		/** The vir from lxc. */
		VIR_FROM_LXC, /* Error from Linux Container driver */
		/** The vir from storage. */
		VIR_FROM_STORAGE, /* Error from storage driver */
		/** The vir from network. */
		VIR_FROM_NETWORK, /* Error from network config */
		/** The vir from domain. */
		VIR_FROM_DOMAIN, /* Error from domain config */
		/** The vir from uml. */
		VIR_FROM_UML, /* Error at the UML driver */
		/** The vir from nodedev. */
		VIR_FROM_NODEDEV, /* Error from node device monitor */
		/** The vir from xen inotify. */
		VIR_FROM_XEN_INOTIFY, /* Error from xen inotify layer */
		/** The vir from security. */
		VIR_FROM_SECURITY, /* Error from security framework */
		/** The vir from vbox. */
		VIR_FROM_VBOX, /* Error from VirtualBox driver */
		/** The vir from interface. */
		VIR_FROM_INTERFACE, /* Error when operating on an interface */
		/** The vir from one. */
		VIR_FROM_ONE, /* Error from OpenNebula driver */
		/** The vir from esx. */
		VIR_FROM_ESX, /* Error from ESX driver */
		/** The vir from phyp. */
		VIR_FROM_PHYP, /* Error from IBM power hypervisor */
		/** The vir from secret. */
		VIR_FROM_SECRET
		/* Error from secret storage */
	}

	/**
	 * The Enum ErrorLevel.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum ErrorLevel
	{

		/** The vir err none. */
		VIR_ERR_NONE,

		/** The vir err warning. */
		VIR_ERR_WARNING,

		/** The vir err error. */
		VIR_ERR_ERROR
	}

	/**
	 * The Enum ErrorNumber.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum ErrorNumber
	{

		/** The vir err ok. */
		VIR_ERR_OK,
		/** The vir err internal error. */
		VIR_ERR_INTERNAL_ERROR, /* internal error */
		/** The vir err no memory. */
		VIR_ERR_NO_MEMORY, /* memory allocation failure */
		/** The vir err no support. */
		VIR_ERR_NO_SUPPORT, /* no support for this function */
		/** The vir err unknown host. */
		VIR_ERR_UNKNOWN_HOST, /* could not resolve hostname */
		/** The vir err no connect. */
		VIR_ERR_NO_CONNECT, /* can't connect to hypervisor */
		/** The vir err invalid conn. */
		VIR_ERR_INVALID_CONN, /* invalid connection object */
		/** The vir err invalid domain. */
		VIR_ERR_INVALID_DOMAIN, /* invalid domain object */
		/** The vir err invalid arg. */
		VIR_ERR_INVALID_ARG, /* invalid function argument */
		/** The vir err operation failed. */
		VIR_ERR_OPERATION_FAILED, /* a command to hypervisor failed */
		/** The vir err get failed. */
		VIR_ERR_GET_FAILED, /* a HTTP GET command to failed */
		/** The vir err post failed. */
		VIR_ERR_POST_FAILED, /* a HTTP POST command to failed */
		/** The vir err http error. */
		VIR_ERR_HTTP_ERROR, /* unexpected HTTP error code */
		/** The vir err sexpr serial. */
		VIR_ERR_SEXPR_SERIAL, /* failure to serialize an S-Expr */
		/** The vir err no xen. */
		VIR_ERR_NO_XEN, /* could not open Xen hypervisor control */
		/** The vir err xen call. */
		VIR_ERR_XEN_CALL, /* failure doing an hypervisor call */
		/** The vir err os type. */
		VIR_ERR_OS_TYPE, /* unknown OS type */
		/** The vir err no kernel. */
		VIR_ERR_NO_KERNEL, /* missing kernel information */
		/** The vir err no root. */
		VIR_ERR_NO_ROOT, /* missing root device information */
		/** The vir err no source. */
		VIR_ERR_NO_SOURCE, /* missing source device information */
		/** The vir err no target. */
		VIR_ERR_NO_TARGET, /* missing target device information */
		/** The vir err no name. */
		VIR_ERR_NO_NAME, /* missing domain name information */
		/** The vir err no os. */
		VIR_ERR_NO_OS, /* missing domain OS information */
		/** The vir err no device. */
		VIR_ERR_NO_DEVICE, /* missing domain devices information */
		/** The vir err no xenstore. */
		VIR_ERR_NO_XENSTORE, /* could not open Xen Store control */
		/** The vir err driver full. */
		VIR_ERR_DRIVER_FULL, /* too many drivers registered */
		/** The vir err call failed. */
		VIR_ERR_CALL_FAILED, /* not supported by the drivers (DEPRECATED) */
		/** The vir err xml error. */
		VIR_ERR_XML_ERROR, /* an XML description is not well formed or broken */
		/** The vir err dom exist. */
		VIR_ERR_DOM_EXIST, /* the domain already exist */
		/** The vir err operation denied. */
		VIR_ERR_OPERATION_DENIED, /*
								 * operation forbidden on read-only connections
								 */
		/** The vir err open failed. */
		VIR_ERR_OPEN_FAILED, /* failed to open a conf file */
		/** The vir err read failed. */
		VIR_ERR_READ_FAILED, /* failed to read a conf file */
		/** The vir err parse failed. */
		VIR_ERR_PARSE_FAILED, /* failed to parse a conf file */
		/** The vir err conf syntax. */
		VIR_ERR_CONF_SYNTAX, /* failed to parse the syntax of a conf file */
		/** The vir err write failed. */
		VIR_ERR_WRITE_FAILED, /* failed to write a conf file */
		/** The vir err xml detail. */
		VIR_ERR_XML_DETAIL, /* detail of an XML error */
		/** The vir err invalid network. */
		VIR_ERR_INVALID_NETWORK, /* invalid network object */
		/** The vir err network exist. */
		VIR_ERR_NETWORK_EXIST, /* the network already exist */
		/** The vir err system error. */
		VIR_ERR_SYSTEM_ERROR, /* general system call failure */
		/** The vir err rpc. */
		VIR_ERR_RPC, /* some sort of RPC error */
		/** The vir err gnutls error. */
		VIR_ERR_GNUTLS_ERROR, /* error from a GNUTLS call */
		/** The vir war no network. */
		VIR_WAR_NO_NETWORK, /* failed to start network */
		/** The vir err no domain. */
		VIR_ERR_NO_DOMAIN, /* domain not found or unexpectedly disappeared */
		/** The vir err no network. */
		VIR_ERR_NO_NETWORK, /* network not found */
		/** The vir err invalid mac. */
		VIR_ERR_INVALID_MAC, /* invalid MAC address */
		/** The vir err auth failed. */
		VIR_ERR_AUTH_FAILED, /* authentication failed */
		/** The vir err invalid storage pool. */
		VIR_ERR_INVALID_STORAGE_POOL, /* invalid storage pool object */
		/** The vir err invalid storage vol. */
		VIR_ERR_INVALID_STORAGE_VOL, /* invalid storage vol object */
		/** The vir war no storage. */
		VIR_WAR_NO_STORAGE, /* failed to start storage */
		/** The vir err no storage pool. */
		VIR_ERR_NO_STORAGE_POOL, /* storage pool not found */
		/** The vir err no storage vol. */
		VIR_ERR_NO_STORAGE_VOL, /* storage pool not found */
		/** The vir war no node. */
		VIR_WAR_NO_NODE, /* failed to start node driver */
		/** The vir err invalid node device. */
		VIR_ERR_INVALID_NODE_DEVICE, /* invalid node device object */
		/** The vir err no node device. */
		VIR_ERR_NO_NODE_DEVICE, /* node device not found */
		/** The vir err no security model. */
		VIR_ERR_NO_SECURITY_MODEL, /* security model not found */
		/** The vir err operation invalid. */
		VIR_ERR_OPERATION_INVALID, /* operation is not applicable at this time */
		/** The vir war no interface. */
		VIR_WAR_NO_INTERFACE, /* failed to start interface driver */
		/** The vir err no interface. */
		VIR_ERR_NO_INTERFACE, /* interface driver not running */
		/** The vir err invalid interface. */
		VIR_ERR_INVALID_INTERFACE, /* invalid interface object */
		/** The vir err multiple interfaces. */
		VIR_ERR_MULTIPLE_INTERFACES, /* more than one matching interface found */
		/** The vir war no secret. */
		VIR_WAR_NO_SECRET, /* failed to start secret storage */
		/** The vir err invalid secret. */
		VIR_ERR_INVALID_SECRET, /* invalid secret */
		/** The vir err no secret. */
		VIR_ERR_NO_SECRET
		/* secret not found */

	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4780109197014633842L;

	/** The code. */
	ErrorNumber code;

	/** The domain. */
	ErrorDomain domain;

	/** The message. */
	String message;

	/** The level. */
	ErrorLevel level;

	/** The vcp. */
	ConnectionPointer VCP; /* Deprecated */

	/** The vdp. */
	DomainPointer VDP; /* Deprecated */

	/** The str1. */
	String str1;

	/** The str2. */
	String str2;

	/** The str3. */
	String str3;

	/** The int1. */
	int int1;

	/** The int2. */
	int int2;

	/** The vnp. */
	NetworkPointer VNP; /* Deprecated */

	/**
	 * Instantiates a new error.
	 */
	public Error()
	{

	}

	/**
	 * Instantiates a new error.
	 * 
	 * @param vError
	 *            the v error
	 */
	public Error(virError vError)
	{
		code = ErrorNumber.values()[vError.code];
		domain = ErrorDomain.values()[vError.domain];
		level = ErrorLevel.values()[vError.level];
		message = vError.message;
		str1 = vError.str1;
		str2 = vError.str2;
		str3 = vError.str3;
		int1 = vError.int1;
		int2 = vError.int2;
		VCP = vError.conn;
		VDP = vError.dom;
		VNP = vError.net;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public ErrorNumber getCode()
	{
		return code;
	}

	/**
	 * Gets the conn.
	 * 
	 * @return the conn
	 * @throws ErrorException
	 *             the error exception
	 */
	@Deprecated
	public Connect getConn() throws ErrorException
	{
		throw new ErrorException("No Connect object available");
	}

	/**
	 * Gets the dom.
	 * 
	 * @return the dom
	 * @throws ErrorException
	 *             the error exception
	 */
	@Deprecated
	public Domain getDom() throws ErrorException
	{
		throw new ErrorException("No Domain object available");
	}

	/**
	 * Gets the domain.
	 * 
	 * @return the domain
	 */
	public ErrorDomain getDomain()
	{
		return domain;
	}

	/**
	 * Gets the int1.
	 * 
	 * @return the int1
	 */
	public int getInt1()
	{
		return int1;
	}

	/**
	 * Gets the int2.
	 * 
	 * @return the int2
	 */
	public int getInt2()
	{
		return int2;
	}

	/**
	 * Gets the level.
	 * 
	 * @return the level
	 */
	public ErrorLevel getLevel()
	{
		return level;
	}

	/**
	 * Gets the message.
	 * 
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * Gets the net.
	 * 
	 * @return the net
	 * @throws ErrorException
	 *             the error exception
	 */
	@Deprecated
	public Network getNet() throws ErrorException
	{
		throw new ErrorException("No Network object available");
	}

	/**
	 * Gets the str1.
	 * 
	 * @return the str1
	 */
	public String getStr1()
	{
		return str1;
	}

	/**
	 * Gets the str2.
	 * 
	 * @return the str2
	 */
	public String getStr2()
	{
		return str2;
	}

	/**
	 * Gets the str3.
	 * 
	 * @return the str3
	 */
	public String getStr3()
	{
		return str3;
	}

	/**
	 * Checks for conn.
	 * 
	 * @return true, if successful
	 */
	public boolean hasConn()
	{
		return false;
	}

	/**
	 * Checks for dom.
	 * 
	 * @return true, if successful
	 */
	public boolean hasDom()
	{
		return false;
	}

	/**
	 * Checks for net.
	 * 
	 * @return true, if successful
	 */
	public boolean hasNet()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("level:%s%ncode:%s%ndomain:%s%nhasConn:%b%nhasDom:%b%nhasNet:%b%nmessage:%s%nstr1:%s%nstr2:%s%nstr3:%s%nint1:%d%nint2:%d%n", level, code, domain,
				hasConn(), hasDom(), hasNet(), message, str1, str2, str3, int1, int2);
	}
}
