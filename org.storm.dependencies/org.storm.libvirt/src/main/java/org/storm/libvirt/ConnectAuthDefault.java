/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * The Class ConnectAuthDefault.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public final class ConnectAuthDefault extends ConnectAuth
{

	/**
	 * Instantiates a new connect auth default.
	 */
	public ConnectAuthDefault()
	{
		credType = new CredentialType[] { CredentialType.VIR_CRED_AUTHNAME, CredentialType.VIR_CRED_ECHOPROMPT, CredentialType.VIR_CRED_REALM, CredentialType.VIR_CRED_PASSPHRASE,
				CredentialType.VIR_CRED_NOECHOPROMPT };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.libvirt.ConnectAuth#callback(org.storm.libvirt.ConnectAuth.
	 * Credential[])
	 */
	@Override
	public int callback(Credential[] cred)
	{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try
		{
			for (Credential c : cred)
			{
				String response = "";
				switch (c.type)
				{
				case VIR_CRED_USERNAME:
				case VIR_CRED_AUTHNAME:
				case VIR_CRED_ECHOPROMPT:
				case VIR_CRED_REALM:
					System.out.println(c.prompt);
					response = in.readLine();
					break;
				case VIR_CRED_PASSPHRASE:
				case VIR_CRED_NOECHOPROMPT:
					System.out.println(c.prompt);
					System.out.println("WARNING: THE ENTERED PASSWORD WILL NOT BE MASKED!");
					response = in.readLine();
					break;
				default:
					break;
				}
				if (response.equals("") && !c.defresult.equals(""))
				{
					c.result = c.defresult;
				}
				else
				{
					c.result = response;
				}
				if (c.result.equals(""))
				{
					return -1;
				}
			}
		}
		catch (Exception e)
		{
			return -1;
		}
		return 0;
	}

}
