/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import java.util.UUID;

import org.storm.libvirt.jna.ConnectionPointer;
import org.storm.libvirt.jna.DevicePointer;
import org.storm.libvirt.jna.DomainPointer;
import org.storm.libvirt.jna.InterfacePointer;
import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.NetworkFilterPointer;
import org.storm.libvirt.jna.NetworkPointer;
import org.storm.libvirt.jna.SecretPointer;
import org.storm.libvirt.jna.StoragePoolPointer;
import org.storm.libvirt.jna.StorageVolPointer;
import org.storm.libvirt.jna.StreamPointer;
import org.storm.libvirt.jna.virConnectAuth;
import org.storm.libvirt.jna.virNodeInfo;

import com.sun.jna.Memory;
import com.sun.jna.NativeLong;
import com.sun.jna.ptr.LongByReference;

/**
 * The Class Connect.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Connect
{

	// Load the native part
	static
	{
		int result = Libvirt.INSTANCE.virInitialize();
		try
		{
			ErrorHandler.processError(Libvirt.INSTANCE, result);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Connection for domain.
	 * 
	 * @param domain
	 *            the domain
	 * @return the connect
	 */
	public static Connect connectionForDomain(Domain domain)
	{
		ConnectionPointer conn = Libvirt.INSTANCE.virDomainGetConnect(domain.VDP);
		return new Connect(conn);
	}

	/**
	 * Connection for network.
	 * 
	 * @param network
	 *            the network
	 * @return the connect
	 */
	public static Connect connectionForNetwork(Network network)
	{
		ConnectionPointer conn = Libvirt.INSTANCE.virNetworkGetConnect(network.VNP);
		return new Connect(conn);
	}

	/**
	 * Connection for secret.
	 * 
	 * @param secret
	 *            the secret
	 * @return the connect
	 */
	public static Connect connectionForSecret(Secret secret)
	{
		ConnectionPointer conn = Libvirt.INSTANCE.virSecretGetConnect(secret.VSP);
		return new Connect(conn);
	}

	/**
	 * Connection version.
	 * 
	 * @param conn
	 *            the conn
	 * @return the long
	 */
	public static long connectionVersion(Connect conn)
	{
		LongByReference libVer = new LongByReference();
		int result = Libvirt.INSTANCE.virConnectGetLibVersion(conn.VCP, libVer);
		return result != -1 ? libVer.getValue() : -1;
	}

	/**
	 * Convert uuid bytes.
	 * 
	 * @param bytes
	 *            the bytes
	 * @return the int[]
	 */
	public static int[] convertUUIDBytes(byte bytes[])
	{
		int[] returnValue = new int[Libvirt.VIR_UUID_BUFLEN];
		for (int x = 0; x < Libvirt.VIR_UUID_BUFLEN; x++)
		{
			// For some reason, the higher bytes come back wierd.
			// We only want the lower 2 bytes.
			returnValue[x] = bytes[x] & 255;
		}
		return returnValue;
	}

	/**
	 * Creates the uuid bytes.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the byte[]
	 */
	public static byte[] createUUIDBytes(int[] UUID)
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_BUFLEN];
		for (int x = 0; x < Libvirt.VIR_UUID_BUFLEN; x++)
		{
			bytes[x] = (byte) UUID[x];
		}
		return bytes;
	}

	/**
	 * Sets the error callback.
	 * 
	 * @param callback
	 *            the new error callback
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public static void setErrorCallback(Libvirt.VirErrorCallback callback) throws LibvirtException
	{
		Libvirt.INSTANCE.virSetErrorFunc(null, callback);
	}

	/** The vcp. */
	protected ConnectionPointer VCP;

	/** The libvirt. */
	Libvirt libvirt = Libvirt.INSTANCE;

	/**
	 * Instantiates a new connect.
	 * 
	 * @param ptr
	 *            the ptr
	 */
	Connect(ConnectionPointer ptr)
	{
		VCP = ptr;
	}

	/**
	 * Instantiates a new connect.
	 * 
	 * @param VCP
	 *            the vcp
	 */
	@Deprecated
	Connect(long VCP)
	{
		throw new RuntimeException("No longer supported");
	}

	/**
	 * Instantiates a new connect.
	 * 
	 * @param uri
	 *            the uri
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Connect(String uri) throws LibvirtException
	{
		VCP = libvirt.virConnectOpen(uri);
		// Check for an error
		ErrorHandler.processError(libvirt, VCP);
	}

	/**
	 * Instantiates a new connect.
	 * 
	 * @param uri
	 *            the uri
	 * @param readOnly
	 *            the read only
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Connect(String uri, boolean readOnly) throws LibvirtException
	{
		if (readOnly)
		{
			VCP = libvirt.virConnectOpenReadOnly(uri);
		}
		else
		{
			VCP = libvirt.virConnectOpen(uri);
		}
		// Check for an error
		ErrorHandler.processError(libvirt, VCP);
	}

	/**
	 * Instantiates a new connect.
	 * 
	 * @param uri
	 *            the uri
	 * @param auth
	 *            the auth
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Connect(String uri, ConnectAuth auth, int flags) throws LibvirtException
	{
		virConnectAuth vAuth = new virConnectAuth();
		vAuth.cb = auth;
		vAuth.cbdata = null;
		vAuth.ncredtype = auth.credType.length;
		int[] authInts = new int[vAuth.ncredtype];

		for (int x = 0; x < vAuth.ncredtype; x++)
		{
			authInts[x] = auth.credType[x].mapToInt();
		}

		Memory mem = new Memory(4 * vAuth.ncredtype);
		mem.write(0, authInts, 0, vAuth.ncredtype);
		vAuth.credtype = mem.share(0);

		VCP = libvirt.virConnectOpenAuth(uri, vAuth, flags);
		ErrorHandler.processError(Libvirt.INSTANCE, VCP);
	}

	/**
	 * Baseline cpu.
	 * 
	 * @param xmlCPUs
	 *            the xml cp us
	 * @return the string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String baselineCPU(String[] xmlCPUs) throws LibvirtException
	{
		String returnValue = libvirt.virConnectBaselineCPU(VCP, xmlCPUs, xmlCPUs.length, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Close.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int close() throws LibvirtException
	{
		int success = 0;
		if (VCP != null)
		{
			success = libvirt.virConnectClose(VCP);
			ErrorHandler.processError(libvirt, success);
			// If leave an invalid pointer dangling around JVM crashes and burns
			// if someone tries to call a method on us
			// We rely on the underlying libvirt error handling to detect that
			// it's called with a null virConnectPointer
			VCP = null;
		}
		return success;
	}

	/**
	 * Compare cpu.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the cPU compare result
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public CPUCompareResult compareCPU(String xmlDesc) throws LibvirtException
	{
		int rawResult = libvirt.virConnectCompareCPU(VCP, xmlDesc, 0);
		ErrorHandler.processError(libvirt, rawResult);
		return CPUCompareResult.get(rawResult);
	}

	/**
	 * Device create xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the device
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Device deviceCreateXML(String xmlDesc) throws LibvirtException
	{
		Device returnValue = null;
		DevicePointer ptr = libvirt.virNodeDeviceCreateXML(VCP, xmlDesc, 0);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Device(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Device lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the device
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Device deviceLookupByName(String name) throws LibvirtException
	{
		DevicePointer ptr = libvirt.virNodeDeviceLookupByName(VCP, name);
		ErrorHandler.processError(libvirt, ptr);
		return new Device(this, ptr);
	}

	/**
	 * Domain create linux.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainCreateLinux(String xmlDesc, int flags) throws LibvirtException
	{
		Domain returnValue = null;
		DomainPointer ptr = libvirt.virDomainCreateLinux(VCP, xmlDesc, flags);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Domain(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Domain create xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainCreateXML(String xmlDesc, int flags) throws LibvirtException
	{
		Domain returnValue = null;
		DomainPointer ptr = libvirt.virDomainCreateXML(VCP, xmlDesc, flags);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Domain(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Domain define xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainDefineXML(String xmlDesc) throws LibvirtException
	{
		Domain returnValue = null;
		DomainPointer ptr = libvirt.virDomainDefineXML(VCP, xmlDesc);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Domain(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Domain event deregister any.
	 * 
	 * @param callbackID
	 *            the callback id
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int domainEventDeregisterAny(int callbackID) throws LibvirtException
	{
		int returnValue = libvirt.virConnectDomainEventDeregisterAny(VCP, callbackID);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Domain event register any.
	 * 
	 * @param domain
	 *            the domain
	 * @param eventId
	 *            the event id
	 * @param cb
	 *            the cb
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int domainEventRegisterAny(Domain domain, int eventId, Libvirt.VirConnectDomainEventGenericCallback cb) throws LibvirtException
	{
		DomainPointer ptr = domain == null ? null : domain.VDP;
		int returnValue = libvirt.virConnectDomainEventRegisterAny(VCP, ptr, eventId, cb, null, null);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Domain lookup by id.
	 * 
	 * @param id
	 *            the id
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainLookupByID(int id) throws LibvirtException
	{
		Domain returnValue = null;
		DomainPointer ptr = libvirt.virDomainLookupByID(VCP, id);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Domain(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Domain lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainLookupByName(String name) throws LibvirtException
	{
		Domain returnValue = null;
		DomainPointer ptr = libvirt.virDomainLookupByName(VCP, name);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Domain(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Domain lookup by uuid.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainLookupByUUID(int[] UUID) throws LibvirtException
	{
		byte[] uuidBytes = Connect.createUUIDBytes(UUID);
		Domain returnValue = null;
		DomainPointer ptr = libvirt.virDomainLookupByUUID(VCP, uuidBytes);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Domain(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Domain lookup by uuid.
	 * 
	 * @param uuid
	 *            the uuid
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainLookupByUUID(UUID uuid) throws LibvirtException
	{
		return domainLookupByUUIDString(uuid.toString());
	}

	/**
	 * Domain lookup by uuid string.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain domainLookupByUUIDString(String UUID) throws LibvirtException
	{
		Domain returnValue = null;
		DomainPointer ptr = libvirt.virDomainLookupByUUIDString(VCP, UUID);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Domain(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Domain xml from native.
	 * 
	 * @param nativeFormat
	 *            the native format
	 * @param nativeConfig
	 *            the native config
	 * @param flags
	 *            the flags
	 * @return the string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String domainXMLFromNative(String nativeFormat, String nativeConfig, int flags) throws LibvirtException
	{
		String returnValue = libvirt.virConnectDomainXMLFromNative(VCP, nativeFormat, nativeConfig, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Domain xml to native.
	 * 
	 * @param nativeFormat
	 *            the native format
	 * @param domainXML
	 *            the domain xml
	 * @param flags
	 *            the flags
	 * @return the string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String domainXMLToNative(String nativeFormat, String domainXML, int flags) throws LibvirtException
	{
		String returnValue = libvirt.virConnectDomainXMLToNative(VCP, nativeFormat, domainXML, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		close();
	}

	/**
	 * Find storage pool sources.
	 * 
	 * @param type
	 *            the type
	 * @param srcSpecs
	 *            the src specs
	 * @param flags
	 *            the flags
	 * @return the string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String findStoragePoolSources(String type, String srcSpecs, int flags) throws LibvirtException
	{
		String returnValue = libvirt.virConnectFindStoragePoolSources(VCP, type, srcSpecs, flags);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the capabilities.
	 * 
	 * @return the capabilities
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getCapabilities() throws LibvirtException
	{
		String returnValue = libvirt.virConnectGetCapabilities(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the cells free memory.
	 * 
	 * @param startCells
	 *            the start cells
	 * @param maxCells
	 *            the max cells
	 * @return the cells free memory
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getCellsFreeMemory(int startCells, int maxCells) throws LibvirtException
	{
		LongByReference returnValue = new LongByReference();
		libvirt.virNodeGetCellsFreeMemory(VCP, returnValue, startCells, maxCells);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue.getValue();
	}

	/**
	 * Gets the free memory.
	 * 
	 * @return the free memory
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getFreeMemory() throws LibvirtException
	{
		long returnValue = 0;
		returnValue = libvirt.virNodeGetFreeMemory(VCP);
		ErrorHandler.processError(libvirt, returnValue - 1); // 0 is an error
																// for this case
		return returnValue;
	}

	/**
	 * Gets the host name.
	 * 
	 * @return the host name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getHostName() throws LibvirtException
	{
		String returnValue = libvirt.virConnectGetHostname(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;

	}

	/**
	 * Gets the hypervisor version.
	 * 
	 * @param type
	 *            the type
	 * @return the hypervisor version
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getHypervisorVersion(String type) throws LibvirtException
	{
		LongByReference libVer = new LongByReference();
		LongByReference typeVer = new LongByReference();
		int result = libvirt.virGetVersion(libVer, type, typeVer);
		ErrorHandler.processError(libvirt, result);
		return libVer.getValue();
	}

	/**
	 * Gets the lib vir version.
	 * 
	 * @return the lib vir version
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getLibVirVersion() throws LibvirtException
	{
		LongByReference libVer = new LongByReference();
		LongByReference typeVer = new LongByReference();
		int result = libvirt.virGetVersion(libVer, null, typeVer);
		ErrorHandler.processError(libvirt, result);
		return libVer.getValue();
	}

	/**
	 * Gets the max vcpus.
	 * 
	 * @param type
	 *            the type
	 * @return the max vcpus
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int getMaxVcpus(String type) throws LibvirtException
	{
		int returnValue = libvirt.virConnectGetMaxVcpus(VCP, type);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getType() throws LibvirtException
	{
		String returnValue = libvirt.virConnectGetType(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the uri.
	 * 
	 * @return the uri
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getURI() throws LibvirtException
	{
		String returnValue = libvirt.virConnectGetURI(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getVersion() throws LibvirtException
	{
		LongByReference hvVer = new LongByReference();
		int result = libvirt.virConnectGetVersion(VCP, hvVer);
		ErrorHandler.processError(libvirt, result);
		return hvVer.getValue();
	}

	/**
	 * Interface define xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the interface
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Interface interfaceDefineXML(String xmlDesc) throws LibvirtException
	{
		Interface returnValue = null;
		InterfacePointer ptr = libvirt.virInterfaceDefineXML(VCP, xmlDesc, 0);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Interface(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Interface lookup by mac string.
	 * 
	 * @param mac
	 *            the mac
	 * @return the interface
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Interface interfaceLookupByMACString(String mac) throws LibvirtException
	{
		Interface returnValue = null;
		InterfacePointer ptr = libvirt.virInterfaceLookupByMACString(VCP, mac);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Interface(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Interface lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the interface
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Interface interfaceLookupByName(String name) throws LibvirtException
	{
		Interface returnValue = null;
		InterfacePointer ptr = libvirt.virInterfaceLookupByName(VCP, name);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Interface(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Checks if is encrypted.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isEncrypted() throws LibvirtException
	{
		int returnValue = libvirt.virConnectIsEncrypted(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks if is secure.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isSecure() throws LibvirtException
	{
		int returnValue = libvirt.virConnectIsSecure(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * List defined domains.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listDefinedDomains() throws LibvirtException
	{
		int maxnames = numOfDefinedDomains();
		String[] names = new String[maxnames];
		if (maxnames > 0)
		{
			int result = libvirt.virConnectListDefinedDomains(VCP, names, maxnames);
			ErrorHandler.processError(libvirt, result);
		}
		return names;
	}

	/**
	 * List defined interfaces.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listDefinedInterfaces() throws LibvirtException
	{
		int num = numOfDefinedInterfaces();
		String[] returnValue = new String[num];
		if (num > 0)
		{
			int result = libvirt.virConnectListDefinedInterfaces(VCP, returnValue, num);
			ErrorHandler.processError(libvirt, result);
		}
		return returnValue;
	}

	/**
	 * List defined networks.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listDefinedNetworks() throws LibvirtException
	{
		int maxnames = numOfDefinedNetworks();
		String[] names = new String[maxnames];

		if (maxnames > 0)
		{
			int result = libvirt.virConnectListDefinedNetworks(VCP, names, maxnames);
			ErrorHandler.processError(libvirt, result);
		}
		return names;
	}

	/**
	 * List defined storage pools.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listDefinedStoragePools() throws LibvirtException
	{
		int num = numOfDefinedStoragePools();
		String[] returnValue = new String[num];
		int result = libvirt.virConnectListDefinedStoragePools(VCP, returnValue, num);
		ErrorHandler.processError(libvirt, result);
		return returnValue;
	}

	/**
	 * List devices.
	 * 
	 * @param capabilityName
	 *            the capability name
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listDevices(String capabilityName) throws LibvirtException
	{
		int maxDevices = numOfDevices(capabilityName);
		String[] names = new String[maxDevices];

		if (maxDevices > 0)
		{
			int result = libvirt.virNodeListDevices(VCP, capabilityName, names, maxDevices, 0);
			ErrorHandler.processError(libvirt, result);
		}
		return names;
	}

	/**
	 * List domains.
	 * 
	 * @return the int[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int[] listDomains() throws LibvirtException
	{
		int maxids = numOfDomains();
		int[] ids = new int[maxids];

		if (maxids > 0)
		{
			int result = libvirt.virConnectListDomains(VCP, ids, maxids);
			ErrorHandler.processError(libvirt, result);
		}
		return ids;
	}

	/**
	 * List interfaces.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listInterfaces() throws LibvirtException
	{
		int num = numOfInterfaces();
		String[] returnValue = new String[num];
		if (num > 0)
		{
			int result = libvirt.virConnectListInterfaces(VCP, returnValue, num);
			ErrorHandler.processError(libvirt, result);
		}
		return returnValue;
	}

	/**
	 * List network filters.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listNetworkFilters() throws LibvirtException
	{
		int maxnames = numOfNetworkFilters();
		String[] names = new String[maxnames];
		if (maxnames > 0)
		{
			int result = libvirt.virConnectListNWFilters(VCP, names, maxnames);
			ErrorHandler.processError(libvirt, result);
		}
		return names;
	}

	/**
	 * List networks.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listNetworks() throws LibvirtException
	{
		int maxnames = numOfNetworks();
		String[] names = new String[maxnames];

		if (maxnames > 0)
		{
			int result = libvirt.virConnectListNetworks(VCP, names, maxnames);
			ErrorHandler.processError(libvirt, result);
		}
		return names;
	}

	/**
	 * List secrets.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listSecrets() throws LibvirtException
	{
		int num = numOfSecrets();
		String[] returnValue = new String[num];
		int result = libvirt.virConnectListSecrets(VCP, returnValue, num);
		ErrorHandler.processError(libvirt, result);
		return returnValue;
	}

	/**
	 * List storage pools.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listStoragePools() throws LibvirtException
	{
		int num = numOfStoragePools();
		String[] returnValue = new String[num];
		int result = libvirt.virConnectListStoragePools(VCP, returnValue, num);
		ErrorHandler.processError(libvirt, result);
		return returnValue;
	}

	/**
	 * Network create xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the network
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Network networkCreateXML(String xmlDesc) throws LibvirtException
	{
		Network returnValue = null;
		NetworkPointer ptr = libvirt.virNetworkCreateXML(VCP, xmlDesc);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Network(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network define xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the network
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Network networkDefineXML(String xmlDesc) throws LibvirtException
	{
		Network returnValue = null;
		NetworkPointer ptr = libvirt.virNetworkDefineXML(VCP, xmlDesc);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Network(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network filter define xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the network filter
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public NetworkFilter networkFilterDefineXML(String xmlDesc) throws LibvirtException
	{
		NetworkFilter returnValue = null;
		NetworkFilterPointer ptr = libvirt.virNWFilterDefineXML(VCP, xmlDesc);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new NetworkFilter(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network filter lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the network filter
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public NetworkFilter networkFilterLookupByName(String name) throws LibvirtException
	{
		NetworkFilter returnValue = null;
		NetworkFilterPointer ptr = libvirt.virNWFilterLookupByName(VCP, name);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new NetworkFilter(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network filter lookup by uuid.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the network filter
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public NetworkFilter networkFilterLookupByUUID(int[] UUID) throws LibvirtException
	{
		byte[] uuidBytes = Connect.createUUIDBytes(UUID);
		NetworkFilter returnValue = null;
		NetworkFilterPointer ptr = libvirt.virNWFilterLookupByUUID(VCP, uuidBytes);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new NetworkFilter(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network filter lookup by uuid.
	 * 
	 * @param uuid
	 *            the uuid
	 * @return the network filter
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public NetworkFilter networkFilterLookupByUUID(UUID uuid) throws LibvirtException
	{
		return networkFilterLookupByUUIDString(uuid.toString());
	}

	/**
	 * Network filter lookup by uuid string.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the network filter
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public NetworkFilter networkFilterLookupByUUIDString(String UUID) throws LibvirtException
	{
		NetworkFilter returnValue = null;
		NetworkFilterPointer ptr = libvirt.virNWFilterLookupByUUIDString(VCP, UUID);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new NetworkFilter(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the network
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Network networkLookupByName(String name) throws LibvirtException
	{
		Network returnValue = null;
		NetworkPointer ptr = libvirt.virNetworkLookupByName(VCP, name);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Network(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network lookup by uuid.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the network
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	@Deprecated
	public Network networkLookupByUUID(int[] UUID) throws LibvirtException
	{
		byte[] uuidBytes = Connect.createUUIDBytes(UUID);
		Network returnValue = null;
		NetworkPointer ptr = libvirt.virNetworkLookupByUUID(VCP, uuidBytes);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Network(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Network lookup by uuid.
	 * 
	 * @param uuid
	 *            the uuid
	 * @return the network
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Network networkLookupByUUID(UUID uuid) throws LibvirtException
	{
		return networkLookupByUUIDString(uuid.toString());
	}

	/**
	 * Network lookup by uuid string.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the network
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Network networkLookupByUUIDString(String UUID) throws LibvirtException
	{
		Network returnValue = null;
		NetworkPointer ptr = libvirt.virNetworkLookupByUUIDString(VCP, UUID);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Network(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Node info.
	 * 
	 * @return the node info
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public NodeInfo nodeInfo() throws LibvirtException
	{
		virNodeInfo vInfo = new virNodeInfo();
		int result = libvirt.virNodeGetInfo(VCP, vInfo);
		ErrorHandler.processError(libvirt, result);
		return new NodeInfo(vInfo);
	}

	/**
	 * Num of defined domains.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfDefinedDomains() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfDefinedDomains(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of defined interfaces.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfDefinedInterfaces() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfDefinedInterfaces(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of defined networks.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfDefinedNetworks() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfDefinedNetworks(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of defined storage pools.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfDefinedStoragePools() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfDefinedStoragePools(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of devices.
	 * 
	 * @param capabilityName
	 *            the capability name
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfDevices(String capabilityName) throws LibvirtException
	{
		int returnValue = libvirt.virNodeNumOfDevices(VCP, capabilityName, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of domains.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfDomains() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfDomains(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of interfaces.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfInterfaces() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfInterfaces(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of network filters.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfNetworkFilters() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfNWFilters(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of networks.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfNetworks() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfNetworks(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of secrets.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfSecrets() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfSecrets(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Num of storage pools.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfStoragePools() throws LibvirtException
	{
		int returnValue = libvirt.virConnectNumOfStoragePools(VCP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Restore.
	 * 
	 * @param from
	 *            the from
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void restore(String from) throws LibvirtException
	{
		int result = libvirt.virDomainRestore(VCP, from);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Secret define xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the secret
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Secret secretDefineXML(String xmlDesc) throws LibvirtException
	{
		Secret returnValue = null;
		SecretPointer ptr = libvirt.virSecretDefineXML(VCP, xmlDesc, 0);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Secret(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Secret lookup by uuid.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the secret
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Secret secretLookupByUUID(int[] UUID) throws LibvirtException
	{
		byte[] uuidBytes = Connect.createUUIDBytes(UUID);
		Secret returnValue = null;
		SecretPointer ptr = libvirt.virSecretLookupByUUID(VCP, uuidBytes);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Secret(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Secret lookup by uuid.
	 * 
	 * @param uuid
	 *            the uuid
	 * @return the secret
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Secret secretLookupByUUID(UUID uuid) throws LibvirtException
	{
		return secretLookupByUUIDString(uuid.toString());
	}

	/**
	 * Secret lookup by uuid string.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the secret
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Secret secretLookupByUUIDString(String UUID) throws LibvirtException
	{
		Secret returnValue = null;
		SecretPointer ptr = libvirt.virSecretLookupByUUIDString(VCP, UUID);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new Secret(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Sets the connection error callback.
	 * 
	 * @param callback
	 *            the new connection error callback
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setConnectionErrorCallback(Libvirt.VirErrorCallback callback) throws LibvirtException
	{
		libvirt.virConnSetErrorFunc(VCP, null, callback);
	}

	/**
	 * Sets the dom0 memory.
	 * 
	 * @param memory
	 *            the new dom0 memory
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setDom0Memory(long memory) throws LibvirtException
	{
		int result = libvirt.virDomainSetMemory(null, new NativeLong(memory));
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Storage pool create xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the storage pool
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StoragePool storagePoolCreateXML(String xmlDesc, int flags) throws LibvirtException
	{
		StoragePoolPointer ptr = libvirt.virStoragePoolCreateXML(VCP, xmlDesc, flags);
		ErrorHandler.processError(libvirt, ptr);
		return new StoragePool(this, ptr);
	}

	/**
	 * Storage pool define xml.
	 * 
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the storage pool
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StoragePool storagePoolDefineXML(String xml, int flags) throws LibvirtException
	{
		StoragePoolPointer ptr = libvirt.virStoragePoolDefineXML(VCP, xml, flags);
		ErrorHandler.processError(libvirt, ptr);
		return new StoragePool(this, ptr);
	}

	/**
	 * Storage pool lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the storage pool
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StoragePool storagePoolLookupByName(String name) throws LibvirtException
	{
		StoragePoolPointer ptr = libvirt.virStoragePoolLookupByName(VCP, name);
		ErrorHandler.processError(libvirt, ptr);
		return new StoragePool(this, ptr);
	}

	/**
	 * Storage pool lookup by uuid.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the storage pool
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	@Deprecated
	public StoragePool storagePoolLookupByUUID(int[] UUID) throws LibvirtException
	{
		byte[] uuidBytes = Connect.createUUIDBytes(UUID);
		StoragePool returnValue = null;
		StoragePoolPointer ptr = libvirt.virStoragePoolLookupByUUID(VCP, uuidBytes);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new StoragePool(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Storage pool lookup by uuid.
	 * 
	 * @param uuid
	 *            the uuid
	 * @return the storage pool
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StoragePool storagePoolLookupByUUID(UUID uuid) throws LibvirtException
	{
		return storagePoolLookupByUUIDString(uuid.toString());
	}

	/**
	 * Storage pool lookup by uuid string.
	 * 
	 * @param UUID
	 *            the uuid
	 * @return the storage pool
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StoragePool storagePoolLookupByUUIDString(String UUID) throws LibvirtException
	{
		StoragePool returnValue = null;
		StoragePoolPointer ptr = libvirt.virStoragePoolLookupByUUIDString(VCP, UUID);
		ErrorHandler.processError(libvirt, ptr);
		if (ptr != null)
		{
			returnValue = new StoragePool(this, ptr);
		}
		return returnValue;
	}

	/**
	 * Storage vol lookup by key.
	 * 
	 * @param key
	 *            the key
	 * @return the storage vol
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StorageVol storageVolLookupByKey(String key) throws LibvirtException
	{
		StorageVolPointer sPtr = libvirt.virStorageVolLookupByKey(VCP, key);
		ErrorHandler.processError(libvirt, sPtr);
		return new StorageVol(this, sPtr);
	}

	/**
	 * Storage vol lookup by path.
	 * 
	 * @param path
	 *            the path
	 * @return the storage vol
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StorageVol storageVolLookupByPath(String path) throws LibvirtException
	{
		StorageVolPointer sPtr = libvirt.virStorageVolLookupByPath(VCP, path);
		ErrorHandler.processError(libvirt, sPtr);
		return new StorageVol(this, sPtr);
	}

	/**
	 * Stream new.
	 * 
	 * @param flags
	 *            the flags
	 * @return the stream
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Stream streamNew(int flags) throws LibvirtException
	{
		StreamPointer sPtr = libvirt.virStreamNew(VCP, flags);
		ErrorHandler.processError(libvirt, sPtr);
		return new Stream(this, sPtr);
	}

	/**
	 * Checks if is connected.
	 * 
	 * @return true, if is connected
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public boolean isConnected() throws LibvirtException
	{
		return VCP != null ? true : false;
	}
}
