/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.virError;

import com.sun.jna.Pointer;

/**
 * The Class ErrorCallback.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ErrorCallback implements Libvirt.VirErrorCallback
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.libvirt.jna.Libvirt.VirErrorCallback#errorCallback(com.sun.
	 * jna.Pointer, org.storm.libvirt.jna.virError)
	 */
	@Override
	public void errorCallback(Pointer userData, virError error)
	{
		// By default, do nothing. This will silence the default
		// logging done by the C code. Other users can override this
		// and do more interesting things.
	}
}
