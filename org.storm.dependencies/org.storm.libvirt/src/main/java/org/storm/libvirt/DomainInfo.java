/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virDomainInfo;

/**
 * The Class DomainInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DomainInfo
{

	/**
	 * The Enum DomainState.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum DomainState
	{

		/** The vir domain nostate. */
		VIR_DOMAIN_NOSTATE,

		/** The vir domain running. */
		VIR_DOMAIN_RUNNING,

		/** The vir domain blocked. */
		VIR_DOMAIN_BLOCKED,

		/** The vir domain paused. */
		VIR_DOMAIN_PAUSED,

		/** The vir domain shutdown. */
		VIR_DOMAIN_SHUTDOWN,

		/** The vir domain shutoff. */
		VIR_DOMAIN_SHUTOFF,

		/** The vir domain crashed. */
		VIR_DOMAIN_CRASHED
	}

	/** The state. */
	public DomainState state;

	/** The max mem. */
	public long maxMem;

	/** The memory. */
	public long memory;

	/** The nr virt cpu. */
	public int nrVirtCpu;

	/** The cpu time. */
	public long cpuTime;

	/**
	 * Instantiates a new domain info.
	 */
	public DomainInfo()
	{

	}

	/**
	 * Instantiates a new domain info.
	 * 
	 * @param info
	 *            the info
	 */
	public DomainInfo(virDomainInfo info)
	{
		cpuTime = info.cpuTime;
		maxMem = info.maxMem.longValue();
		memory = info.memory.longValue();
		nrVirtCpu = info.nrVirtCpu;
		state = DomainState.values()[info.state];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("state:%s%nmaxMem:%d%nmemory:%d%nnrVirtCpu:%d%ncpuTime:%d%n", state, maxMem, memory, nrVirtCpu, cpuTime);
	}
}
