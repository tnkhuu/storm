/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virDomainBlockInfo;

/**
 * The Class DomainBlockInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DomainBlockInfo
{

	/** The capacity. */
	protected long capacity;

	/** The allocation. */
	protected long allocation;

	/** The physical. */
	protected long physical;

	/**
	 * Instantiates a new domain block info.
	 * 
	 * @param info
	 *            the info
	 */
	public DomainBlockInfo(virDomainBlockInfo info)
	{
		capacity = info.capacity;
		allocation = info.allocation;
		physical = info.physical;
	}

	/**
	 * Gets the allocation.
	 * 
	 * @return the allocation
	 */
	public long getAllocation()
	{
		return allocation;
	}

	/**
	 * Gets the capacity.
	 * 
	 * @return the capacity
	 */
	public long getCapacity()
	{
		return capacity;
	}

	/**
	 * Gets the physical.
	 * 
	 * @return the physical
	 */
	public long getPhysical()
	{
		return physical;
	}

	/**
	 * Sets the allocation.
	 * 
	 * @param allocation
	 *            the new allocation
	 */
	public void setAllocation(long allocation)
	{
		this.allocation = allocation;
	}

	/**
	 * Sets the capacity.
	 * 
	 * @param capacity
	 *            the new capacity
	 */
	public void setCapacity(long capacity)
	{
		this.capacity = capacity;
	}

	/**
	 * Sets the physical.
	 * 
	 * @param physical
	 *            the new physical
	 */
	public void setPhysical(long physical)
	{
		this.physical = physical;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("capacity:%d%nallocation:%d%nphysical:%d%n", capacity, allocation, physical);
	}
}
