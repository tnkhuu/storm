/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virStorageVolInfo;

/**
 * The Class StorageVolInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StorageVolInfo
{

	/**
	 * The Enum VirStorageVolType.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum VirStorageVolType
	{

		/** The vir storage vol file. */
		VIR_STORAGE_VOL_FILE,

		/** The vir storage vol block. */
		VIR_STORAGE_VOL_BLOCK
	}

	/** The type. */
	public VirStorageVolType type;

	/** The capacity. */
	public long capacity;

	/** The allocation. */
	public long allocation;;

	/**
	 * Instantiates a new storage vol info.
	 * 
	 * @param type
	 *            the type
	 * @param capacity
	 *            the capacity
	 * @param allocation
	 *            the allocation
	 */
	StorageVolInfo(int type, long capacity, long allocation)
	{
		switch (type)
		{
		case 0:
			this.type = VirStorageVolType.VIR_STORAGE_VOL_FILE;
			break;
		case 1:
			this.type = VirStorageVolType.VIR_STORAGE_VOL_BLOCK;
			break;
		default:
			assert false;
		}
		this.capacity = capacity;
		this.allocation = allocation;
	}

	/**
	 * Instantiates a new storage vol info.
	 * 
	 * @param volInfo
	 *            the vol info
	 */
	StorageVolInfo(virStorageVolInfo volInfo)
	{
		this(volInfo.type, volInfo.capacity, volInfo.allocation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("type:%s%ncapacity:%d%nallocation:%d%n", type, capacity, allocation);
	}
}
