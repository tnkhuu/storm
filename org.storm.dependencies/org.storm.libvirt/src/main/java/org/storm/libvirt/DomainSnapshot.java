/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.DomainSnapshotPointer;
import org.storm.libvirt.jna.Libvirt;

/**
 * The Class DomainSnapshot.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DomainSnapshot
{

	/** The vdsp. */
	DomainSnapshotPointer VDSP;

	/** The vir connect. */
	@SuppressWarnings("unused")
	private Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new domain snapshot.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VDSP
	 *            the vdsp
	 */
	public DomainSnapshot(Connect virConnect, DomainSnapshotPointer VDSP)
	{
		this.VDSP = VDSP;
		this.virConnect = virConnect;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Delete.
	 * 
	 * @param flags
	 *            the flags
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int delete(int flags) throws LibvirtException
	{
		int success = 0;
		if (VDSP != null)
		{
			success = libvirt.virDomainSnapshotDelete(VDSP, flags);
			ErrorHandler.processError(libvirt, success);
			VDSP = null;
		}

		return success;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VDSP != null)
		{
			success = libvirt.virDomainSnapshotFree(VDSP);
			ErrorHandler.processError(libvirt, success);
			VDSP = null;
		}

		return success;
	}

	/**
	 * Gets the xML desc.
	 * 
	 * @return the xML desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDesc() throws LibvirtException
	{
		String returnValue = libvirt.virDomainSnapshotGetXMLDesc(VDSP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}
}
