/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virDomainBlockStats;

/**
 * The Class DomainBlockStats.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DomainBlockStats
{

	/** The rd_req. */
	public long rd_req;

	/** The rd_bytes. */
	public long rd_bytes;

	/** The wr_req. */
	public long wr_req;

	/** The wr_bytes. */
	public long wr_bytes;

	/** The errs. */
	public long errs;

	/**
	 * Instantiates a new domain block stats.
	 */
	public DomainBlockStats()
	{
	}

	/**
	 * Instantiates a new domain block stats.
	 * 
	 * @param vStats
	 *            the v stats
	 */
	public DomainBlockStats(virDomainBlockStats vStats)
	{
		rd_req = vStats.rd_req;
		rd_bytes = vStats.rd_bytes;
		wr_req = vStats.wr_req;
		wr_bytes = vStats.wr_bytes;
		errs = vStats.errs;
	}
}
