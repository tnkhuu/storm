/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virDomainMemoryStats;

/**
 * The Class MemoryStatistic.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MemoryStatistic
{

	/** The tag. */
	protected int tag;

	/** The val. */
	protected long val;

	/**
	 * Instantiates a new memory statistic.
	 * 
	 * @param stat
	 *            the stat
	 */
	public MemoryStatistic(virDomainMemoryStats stat)
	{
		tag = stat.tag;
		val = stat.val;
	}

	/**
	 * Gets the tag.
	 * 
	 * @return the tag
	 */
	public int getTag()
	{
		return tag;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public long getValue()
	{
		return val;
	}

	/**
	 * Sets the tag.
	 * 
	 * @param tag
	 *            the new tag
	 */
	public void setTag(int tag)
	{
		this.tag = tag;
	}

	/**
	 * Sets the value.
	 * 
	 * @param val
	 *            the new value
	 */
	public void setValue(long val)
	{
		this.val = val;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("tag:%d%nval:%d%n", tag, val);
	}
}
