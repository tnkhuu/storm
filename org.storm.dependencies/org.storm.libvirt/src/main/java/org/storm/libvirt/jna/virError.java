/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt.jna;

import com.sun.jna.Structure;

/**
 * The Class virError.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class virError extends Structure
{

	/** The code. */
	public int code;

	/** The domain. */
	public int domain;

	/** The message. */
	public String message;

	/** The level. */
	public int level;

	/** The conn. */
	public ConnectionPointer conn;

	/** The dom. */
	public DomainPointer dom;

	/** The str1. */
	public String str1;

	/** The str2. */
	public String str2;

	/** The str3. */
	public String str3;

	/** The int1. */
	public int int1;

	/** The int2. */
	public int int2;

	/** The net. */
	public NetworkPointer net;
}
