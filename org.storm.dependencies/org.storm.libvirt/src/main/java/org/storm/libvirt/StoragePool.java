/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.StoragePoolPointer;
import org.storm.libvirt.jna.StorageVolPointer;
import org.storm.libvirt.jna.virStoragePoolInfo;

import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;

/**
 * The Class StoragePool.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StoragePool
{

	/**
	 * The Class BuildFlags.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static final class BuildFlags
	{

		/** The Constant VIR_STORAGE_POOL_BUILD_NEW. */
		static final int VIR_STORAGE_POOL_BUILD_NEW = 0;

		/** The Constant VIR_STORAGE_POOL_BUILD_REPAIR. */
		static final int VIR_STORAGE_POOL_BUILD_REPAIR = 1;

		/** The Constant VIR_STORAGE_POOL_BUILD_RESIZE. */
		static final int VIR_STORAGE_POOL_BUILD_RESIZE = 2;
	}

	/**
	 * The Class DeleteFlags.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static final class DeleteFlags
	{

		/** The Constant VIR_STORAGE_POOL_DELETE_NORMAL. */
		static final int VIR_STORAGE_POOL_DELETE_NORMAL = 0;

		/** The Constant VIR_STORAGE_POOL_DELETE_ZEROED. */
		static final int VIR_STORAGE_POOL_DELETE_ZEROED = 1;
	}

	/** The vspp. */
	protected StoragePoolPointer VSPP;

	/** The vir connect. */
	protected Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new storage pool.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VSPP
	 *            the vspp
	 */
	StoragePool(Connect virConnect, StoragePoolPointer VSPP)
	{
		this.virConnect = virConnect;
		this.VSPP = VSPP;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Builds the.
	 * 
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void build(int flags) throws LibvirtException
	{
		int result = libvirt.virStoragePoolBuild(VSPP, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Creates the.
	 * 
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void create(int flags) throws LibvirtException
	{
		int result = libvirt.virStoragePoolCreate(VSPP, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Delete.
	 * 
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void delete(int flags) throws LibvirtException
	{
		int result = libvirt.virStoragePoolDelete(VSPP, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Destroy.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void destroy() throws LibvirtException
	{
		int result = libvirt.virStoragePoolDestroy(VSPP);
		ErrorHandler.processError(libvirt, result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VSPP != null)
		{
			success = libvirt.virStoragePoolFree(VSPP);
			ErrorHandler.processError(libvirt, success);
			VSPP = null;
		}
		return success;
	}

	/**
	 * Gets the autostart.
	 * 
	 * @return the autostart
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public boolean getAutostart() throws LibvirtException
	{
		IntByReference autoStart = new IntByReference();
		int result = libvirt.virStoragePoolGetAutostart(VSPP, autoStart);
		ErrorHandler.processError(libvirt, result);
		return autoStart.getValue() != 0 ? true : false;
	}

	/**
	 * Gets the connect.
	 * 
	 * @return the connect
	 */
	public Connect getConnect()
	{
		return virConnect;
	}

	/**
	 * Gets the info.
	 * 
	 * @return the info
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StoragePoolInfo getInfo() throws LibvirtException
	{
		virStoragePoolInfo vInfo = new virStoragePoolInfo();
		int result = libvirt.virStoragePoolGetInfo(VSPP, vInfo);
		ErrorHandler.processError(libvirt, result);
		return new StoragePoolInfo(vInfo);
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getName() throws LibvirtException
	{
		String returnValue = libvirt.virStoragePoolGetName(VSPP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the uuid.
	 * 
	 * @return the uuid
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int[] getUUID() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_BUFLEN];
		int success = libvirt.virStoragePoolGetUUID(VSPP, bytes);
		ErrorHandler.processError(libvirt, success);
		int[] returnValue = new int[0];
		if (success == 0)
		{
			returnValue = Connect.convertUUIDBytes(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the uUID string.
	 * 
	 * @return the uUID string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getUUIDString() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_STRING_BUFLEN];
		int success = libvirt.virStoragePoolGetUUIDString(VSPP, bytes);
		ErrorHandler.processError(libvirt, success);
		String returnValue = null;
		if (success == 0)
		{
			returnValue = Native.toString(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the xML desc.
	 * 
	 * @param flags
	 *            the flags
	 * @return the xML desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDesc(int flags) throws LibvirtException
	{
		String returnValue = libvirt.virStoragePoolGetXMLDesc(VSPP, flags);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isActive() throws LibvirtException
	{
		int returnValue = libvirt.virStoragePoolIsActive(VSPP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks if is persistent.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isPersistent() throws LibvirtException
	{
		int returnValue = libvirt.virStoragePoolIsPersistent(VSPP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * List volumes.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listVolumes() throws LibvirtException
	{
		int num = numOfVolumes();
		String[] returnValue = new String[num];
		int result = libvirt.virStoragePoolListVolumes(VSPP, returnValue, num);
		ErrorHandler.processError(libvirt, result);
		return returnValue;
	}

	/**
	 * Num of volumes.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int numOfVolumes() throws LibvirtException
	{
		int returnValue = libvirt.virStoragePoolNumOfVolumes(VSPP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Refresh.
	 * 
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void refresh(int flags) throws LibvirtException
	{
		int result = libvirt.virStoragePoolRefresh(VSPP);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Sets the autostart.
	 * 
	 * @param autostart
	 *            the new autostart
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setAutostart(int autostart) throws LibvirtException
	{
		libvirt.virStoragePoolSetAutostart(VSPP, autostart);
	}

	/**
	 * Storage vol create xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the storage vol
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StorageVol storageVolCreateXML(String xmlDesc, int flags) throws LibvirtException
	{
		StorageVolPointer sPtr = libvirt.virStorageVolCreateXML(VSPP, xmlDesc, flags);
		ErrorHandler.processError(libvirt, sPtr);
		return new StorageVol(virConnect, sPtr);
	}

	/**
	 * Storage vol create xml from.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @param cloneVolume
	 *            the clone volume
	 * @param flags
	 *            the flags
	 * @return the storage vol
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StorageVol storageVolCreateXMLFrom(String xmlDesc, StorageVol cloneVolume, int flags) throws LibvirtException
	{
		StorageVolPointer sPtr = libvirt.virStorageVolCreateXMLFrom(VSPP, xmlDesc, cloneVolume.VSVP, flags);
		ErrorHandler.processError(libvirt, sPtr);
		return new StorageVol(virConnect, sPtr);
	}

	/**
	 * Storage vol lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the storage vol
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StorageVol storageVolLookupByName(String name) throws LibvirtException
	{
		StorageVolPointer sPtr = libvirt.virStorageVolLookupByName(VSPP, name);
		ErrorHandler.processError(libvirt, sPtr);
		return new StorageVol(virConnect, sPtr);
	}

	/**
	 * Undefine.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void undefine() throws LibvirtException
	{
		int result = libvirt.virStoragePoolUndefine(VSPP);
		ErrorHandler.processError(libvirt, result);
	}

}
