/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

/**
 * The Class SchedIntParameter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public final class SchedIntParameter extends SchedParameter
{

	/** The value. */
	public int value;

	/**
	 * Instantiates a new sched int parameter.
	 */
	public SchedIntParameter()
	{

	}

	/**
	 * Instantiates a new sched int parameter.
	 * 
	 * @param value
	 *            the value
	 */
	public SchedIntParameter(int value)
	{
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.libvirt.SchedParameter#getType()
	 */
	@Override
	public int getType()
	{
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.libvirt.SchedParameter#getTypeAsString()
	 */
	@Override
	public String getTypeAsString()
	{
		return "VIR_DOMAIN_SCHED_FIELD_INT";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.libvirt.SchedParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString()
	{
		return Integer.toString(value);
	}
}
