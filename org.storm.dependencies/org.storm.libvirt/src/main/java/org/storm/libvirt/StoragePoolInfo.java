/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virStoragePoolInfo;

/**
 * The Class StoragePoolInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StoragePoolInfo
{

	/**
	 * The Enum StoragePoolState.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum StoragePoolState
	{

		/** The vir storage pool inactive. */
		VIR_STORAGE_POOL_INACTIVE,

		/** The vir storage pool building. */
		VIR_STORAGE_POOL_BUILDING,

		/** The vir storage pool running. */
		VIR_STORAGE_POOL_RUNNING,

		/** The vir storage pool degraded. */
		VIR_STORAGE_POOL_DEGRADED
	}

	/** The state. */
	public StoragePoolState state;

	/** The capacity. */
	public long capacity;

	/** The allocation. */
	public long allocation;

	/** The available. */
	public long available;;

	/**
	 * Instantiates a new storage pool info.
	 * 
	 * @param state
	 *            the state
	 * @param capacity
	 *            the capacity
	 * @param allocation
	 *            the allocation
	 * @param available
	 *            the available
	 */
	StoragePoolInfo(int state, long capacity, long allocation, long available)
	{
		switch (state)
		{
		case 0:
			this.state = StoragePoolState.VIR_STORAGE_POOL_INACTIVE;
			break;
		case 1:
			this.state = StoragePoolState.VIR_STORAGE_POOL_BUILDING;
			break;
		case 2:
			this.state = StoragePoolState.VIR_STORAGE_POOL_RUNNING;
			break;
		case 3:
			this.state = StoragePoolState.VIR_STORAGE_POOL_DEGRADED;
			break;
		default:
			assert false;
		}
		this.capacity = capacity;
		this.allocation = allocation;
		this.available = available;
	}

	/**
	 * Instantiates a new storage pool info.
	 * 
	 * @param vInfo
	 *            the v info
	 */
	StoragePoolInfo(virStoragePoolInfo vInfo)
	{
		this(vInfo.state, vInfo.capacity, vInfo.allocation, vInfo.available);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("state:%s%ncapacity:%d%nallocation:%d%navailable:%d%n", state, capacity, allocation, available);
	}
}
