/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.StoragePoolPointer;
import org.storm.libvirt.jna.StorageVolPointer;
import org.storm.libvirt.jna.virStorageVolInfo;

/**
 * The Class StorageVol.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StorageVol
{

	/**
	 * The Class DeleteFlags.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static final class DeleteFlags
	{

		/** The Constant VIR_STORAGE_POOL_DELETE_NORMAL. */
		static final int VIR_STORAGE_POOL_DELETE_NORMAL = 0;

		/** The Constant VIR_STORAGE_POOL_DELETE_ZEROED. */
		static final int VIR_STORAGE_POOL_DELETE_ZEROED = 1;
	}

	/**
	 * The Enum Type.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum Type
	{

		/** The vir storage vol file. */
		VIR_STORAGE_VOL_FILE,

		/** The vir storage vol block. */
		VIR_STORAGE_VOL_BLOCK
	}

	/** The vsvp. */
	StorageVolPointer VSVP;

	/** The vir connect. */
	protected Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new storage vol.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VSVP
	 *            the vsvp
	 */
	StorageVol(Connect virConnect, StorageVolPointer VSVP)
	{
		this.virConnect = virConnect;
		this.VSVP = VSVP;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Delete.
	 * 
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void delete(int flags) throws LibvirtException
	{
		int result = libvirt.virStorageVolDelete(VSVP, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VSVP != null)
		{
			int result = libvirt.virStorageVolFree(VSVP);
			ErrorHandler.processError(libvirt, result);
			VSVP = null;
		}
		return success;
	}

	/**
	 * Gets the connect.
	 * 
	 * @return the connect
	 */
	public Connect getConnect()
	{
		return virConnect;
	}

	/**
	 * Gets the info.
	 * 
	 * @return the info
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StorageVolInfo getInfo() throws LibvirtException
	{
		virStorageVolInfo vInfo = new virStorageVolInfo();
		int result = libvirt.virStorageVolGetInfo(VSVP, vInfo);
		ErrorHandler.processError(libvirt, result);
		return new StorageVolInfo(vInfo);
	}

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getKey() throws LibvirtException
	{
		String returnValue = libvirt.virStorageVolGetKey(VSVP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getName() throws LibvirtException
	{
		String returnValue = libvirt.virStorageVolGetName(VSVP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the path.
	 * 
	 * @return the path
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getPath() throws LibvirtException
	{
		String returnValue = libvirt.virStorageVolGetPath(VSVP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the xML desc.
	 * 
	 * @param flags
	 *            the flags
	 * @return the xML desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDesc(int flags) throws LibvirtException
	{
		String returnValue = libvirt.virStorageVolGetXMLDesc(VSVP, flags);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Storage pool lookup by volume.
	 * 
	 * @return the storage pool
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public StoragePool storagePoolLookupByVolume() throws LibvirtException
	{
		StoragePoolPointer ptr = libvirt.virStoragePoolLookupByVolume(VSVP);
		ErrorHandler.processError(libvirt, ptr);
		return new StoragePool(virConnect, ptr);
	}

	/**
	 * Wipe.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int wipe() throws LibvirtException
	{
		int returnValue = libvirt.virStorageVolWipe(VSVP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}
}
