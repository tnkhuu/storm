/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt.jna;

import com.sun.jna.Structure;

/**
 * The Class virDomainBlockStats.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class virDomainBlockStats extends Structure
{

	/** The rd_req. */
	public long rd_req; // this is a long long in the code, so a long mapping is
	// correct
	/** The rd_bytes. */
	public long rd_bytes;// this is a long long in the code, so a long mapping
	// is correct
	/** The wr_req. */
	public long wr_req; // this is a long long in the code, so a long mapping is
	// correct
	/** The wr_bytes. */
	public long wr_bytes;// this is a long long in the code, so a long mapping
	// is correct
	/** The errs. */
	public long errs; // this is a long long in the code, so a long mapping is
	// correct
}
