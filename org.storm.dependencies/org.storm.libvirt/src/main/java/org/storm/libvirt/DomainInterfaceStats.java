/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virDomainInterfaceStats;

/**
 * The Class DomainInterfaceStats.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DomainInterfaceStats
{

	/** The rx_bytes. */
	public long rx_bytes;

	/** The rx_packets. */
	public long rx_packets;

	/** The rx_errs. */
	public long rx_errs;

	/** The rx_drop. */
	public long rx_drop;

	/** The tx_bytes. */
	public long tx_bytes;

	/** The tx_packets. */
	public long tx_packets;

	/** The tx_errs. */
	public long tx_errs;

	/** The tx_drop. */
	public long tx_drop;

	/**
	 * Instantiates a new domain interface stats.
	 */
	public DomainInterfaceStats()
	{

	}

	/**
	 * Instantiates a new domain interface stats.
	 * 
	 * @param vStats
	 *            the v stats
	 */
	public DomainInterfaceStats(virDomainInterfaceStats vStats)
	{
		rx_bytes = vStats.rx_bytes;
		rx_packets = vStats.rx_packets;
		rx_errs = vStats.rx_errs;
		rx_drop = vStats.rx_drop;
		tx_bytes = vStats.tx_bytes;
		tx_packets = vStats.tx_packets;
		tx_errs = vStats.tx_errs;
		tx_drop = vStats.tx_drop;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("rx_bytes:%d%nrx_packets:%d%nrx_errs:%d%nrx_drop:%d%ntx_bytes:%d%ntx_packets:%d%ntx_errs:%d%ntx_drop:%d%n", rx_bytes, rx_packets, rx_errs, rx_drop,
				tx_bytes, tx_packets, tx_errs, tx_drop);
	}

}
