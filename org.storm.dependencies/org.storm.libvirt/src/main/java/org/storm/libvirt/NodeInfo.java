/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virNodeInfo;

import com.sun.jna.Native;

/**
 * The Class NodeInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class NodeInfo
{

	/** The model. */
	public String model;

	/** The memory. */
	public long memory;

	/** The cpus. */
	public int cpus;

	/** The mhz. */
	public int mhz;

	/** The nodes. */
	public int nodes;

	/** The sockets. */
	public int sockets;

	/** The cores. */
	public int cores;

	/** The threads. */
	public int threads;

	/**
	 * Instantiates a new node info.
	 */
	public NodeInfo()
	{
	}

	/**
	 * Instantiates a new node info.
	 * 
	 * @param vInfo
	 *            the v info
	 */
	public NodeInfo(virNodeInfo vInfo)
	{
		model = Native.toString(vInfo.model);
		memory = vInfo.memory.longValue();
		cpus = vInfo.cpus;
		mhz = vInfo.mhz;
		nodes = vInfo.nodes;
		sockets = vInfo.sockets;
		cores = vInfo.cores;
		threads = vInfo.threads;
	}

	/**
	 * Max cpus.
	 * 
	 * @return the int
	 */
	public int maxCpus()
	{
		return nodes * sockets * cores * threads;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("model:%s%nmemory:%d%ncpus:%d%nmhz:%d%nnodes:%d%nsockets:%d%ncores:%d%nthreads:%d%n", model, memory, cpus, mhz, nodes, sockets, cores, threads);
	}
}
