/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.SecretPointer;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;

/**
 * The Class Secret.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Secret
{

	/** The vsp. */
	SecretPointer VSP;

	/** The vir connect. */
	@SuppressWarnings("unused")
	private Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new secret.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VSP
	 *            the vsp
	 */
	Secret(Connect virConnect, SecretPointer VSP)
	{
		this.virConnect = virConnect;
		this.VSP = VSP;
		libvirt = virConnect.libvirt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VSP != null)
		{
			success = libvirt.virSecretFree(VSP);
			ErrorHandler.processError(libvirt, success);
			VSP = null;
		}

		return success;
	}

	/**
	 * Gets the usage id.
	 * 
	 * @return the usage id
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getUsageID() throws LibvirtException
	{
		String returnValue = libvirt.virSecretGetUsageID(VSP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the uuid.
	 * 
	 * @return the uuid
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int[] getUUID() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_BUFLEN];
		int success = libvirt.virSecretGetUUID(VSP, bytes);
		ErrorHandler.processError(libvirt, success);
		int[] returnValue = new int[0];
		if (success == 0)
		{
			returnValue = Connect.convertUUIDBytes(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the uUID string.
	 * 
	 * @return the uUID string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getUUIDString() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_STRING_BUFLEN];
		int success = libvirt.virSecretGetUUIDString(VSP, bytes);
		ErrorHandler.processError(libvirt, success);
		String returnValue = null;
		if (success == 0)
		{
			returnValue = Native.toString(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getValue() throws LibvirtException
	{
		String returnValue = libvirt.virSecretGetValue(VSP, new NativeLong(), 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the xML desc.
	 * 
	 * @return the xML desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDesc() throws LibvirtException
	{
		String returnValue = libvirt.virSecretGetXMLDesc(VSP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the value
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int setValue(String value) throws LibvirtException
	{
		int returnValue = libvirt.virSecretSetValue(VSP, value, new NativeLong(value.length()), 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Undefine.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int undefine() throws LibvirtException
	{
		int returnValue = libvirt.virSecretUndefine(VSP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}
}
