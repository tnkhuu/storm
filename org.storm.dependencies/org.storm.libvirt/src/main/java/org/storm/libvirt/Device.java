/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.DevicePointer;
import org.storm.libvirt.jna.Libvirt;

/**
 * The Class Device.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Device
{

	/** The vdp. */
	DevicePointer VDP;

	/** The vir connect. */
	@SuppressWarnings("unused")
	private Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new device.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VDP
	 *            the vdp
	 */
	Device(Connect virConnect, DevicePointer VDP)
	{
		this.virConnect = virConnect;
		this.VDP = VDP;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Destroy.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int destroy() throws LibvirtException
	{
		int success = 0;
		if (VDP != null)
		{
			success = libvirt.virNodeDeviceDestroy(VDP);
			ErrorHandler.processError(libvirt, success);
			VDP = null;
		}

		return success;
	}

	/**
	 * Detach.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int detach() throws LibvirtException
	{
		int num = libvirt.virNodeDeviceDettach(VDP);
		ErrorHandler.processError(libvirt, num);
		return num;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VDP != null)
		{
			success = libvirt.virNodeDeviceFree(VDP);
			ErrorHandler.processError(libvirt, success);
			VDP = null;
		}

		return success;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getName() throws LibvirtException
	{
		String name = libvirt.virNodeDeviceGetName(VDP);
		ErrorHandler.processError(libvirt, name);
		return name;
	}

	/**
	 * Gets the number of capabilities.
	 * 
	 * @return the number of capabilities
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int getNumberOfCapabilities() throws LibvirtException
	{
		int num = libvirt.virNodeDeviceNumOfCaps(VDP);
		ErrorHandler.processError(libvirt, num);
		return num;
	}

	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getParent() throws LibvirtException
	{
		String parent = libvirt.virNodeDeviceGetParent(VDP);
		ErrorHandler.processError(libvirt, parent);
		return parent;
	}

	/**
	 * Gets the xML description.
	 * 
	 * @return the xML description
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDescription() throws LibvirtException
	{
		String desc = libvirt.virNodeDeviceGetXMLDesc(VDP);
		ErrorHandler.processError(libvirt, desc);
		return desc;
	}

	/**
	 * List capabilities.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] listCapabilities() throws LibvirtException
	{
		int maxCaps = getNumberOfCapabilities();
		String[] names = new String[maxCaps];

		if (maxCaps > 0)
		{
			int result = libvirt.virNodeDeviceListCaps(VDP, names, maxCaps);
			ErrorHandler.processError(libvirt, result);
		}
		return names;
	}

	/**
	 * Re attach.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int reAttach() throws LibvirtException
	{
		int num = libvirt.virNodeDeviceReAttach(VDP);
		ErrorHandler.processError(libvirt, num);
		return num;
	}

	/**
	 * Reset.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int reset() throws LibvirtException
	{
		int num = libvirt.virNodeDeviceReset(VDP);
		ErrorHandler.processError(libvirt, num);
		return num;
	}
}
