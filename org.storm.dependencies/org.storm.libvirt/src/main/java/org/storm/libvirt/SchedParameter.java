/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import java.util.Arrays;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.virSchedParameter;
import org.storm.libvirt.jna.virSchedParameterValue;

import com.sun.jna.Native;

/**
 * The Class SchedParameter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class SchedParameter
{

	/**
	 * Creates the.
	 * 
	 * @param vParam
	 *            the v param
	 * @return the sched parameter
	 */
	public static SchedParameter create(virSchedParameter vParam)
	{
		SchedParameter returnValue = null;
		if (vParam != null)
		{
			switch (vParam.type)
			{
			case 1:
				returnValue = new SchedIntParameter(vParam.value.i);
				break;
			case 2:
				returnValue = new SchedUintParameter(vParam.value.ui);
				break;
			case 3:
				returnValue = new SchedLongParameter(vParam.value.l);
				break;
			case 4:
				returnValue = new SchedUlongParameter(vParam.value.ul);
				break;
			case 5:
				returnValue = new SchedDoubleParameter(vParam.value.d);
				break;
			case 6:
				returnValue = new SchedBooleanParameter(vParam.value.b);
				break;
			}
			returnValue.field = Native.toString(vParam.field);
		}
		return returnValue;
	}

	/**
	 * To native.
	 * 
	 * @param param
	 *            the param
	 * @return the vir sched parameter
	 */
	public static virSchedParameter toNative(SchedParameter param)
	{
		virSchedParameter returnValue = new virSchedParameter();
		returnValue.value = new virSchedParameterValue();
		returnValue.field = copyOf(param.field.getBytes(), Libvirt.VIR_DOMAIN_SCHED_FIELD_LENGTH);
		returnValue.type = param.getType();
		switch (param.getType())
		{
		case 1:
			returnValue.value.i = ((SchedIntParameter) param).value;
			returnValue.value.setType(int.class);
			break;
		case 2:
			returnValue.value.ui = ((SchedUintParameter) param).value;
			returnValue.value.setType(int.class);
			break;
		case 3:
			returnValue.value.l = ((SchedLongParameter) param).value;
			returnValue.value.setType(long.class);
			break;
		case 4:
			returnValue.value.ul = ((SchedUlongParameter) param).value;
			returnValue.value.setType(long.class);
			break;
		case 5:
			returnValue.value.d = ((SchedDoubleParameter) param).value;
			returnValue.value.setType(double.class);
			break;
		case 6:
			returnValue.value.b = (byte) (((SchedBooleanParameter) param).value ? 1 : 0);
			returnValue.value.setType(byte.class);
			break;

		}
		return returnValue;
	}

	/**
	 * Copy of.
	 * 
	 * @param original
	 *            the original
	 * @param length
	 *            the length
	 * @return the byte[]
	 */
	public static byte[] copyOf(byte[] original, int length)
	{
		byte[] returnValue = new byte[length];
		int originalLength = original.length;
		Arrays.fill(returnValue, (byte) 0);
		for (int x = 0; x < originalLength; x++)
		{
			returnValue[x] = original[x];
		}
		return returnValue;
	}

	/** The field. */
	public String field;

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public abstract int getType();

	/**
	 * Gets the type as string.
	 * 
	 * @return the type as string
	 */
	public abstract String getTypeAsString();

	/**
	 * Gets the value as string.
	 * 
	 * @return the value as string
	 */
	public abstract String getValueAsString();
}
