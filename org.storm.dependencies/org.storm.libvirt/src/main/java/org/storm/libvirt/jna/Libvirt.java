/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt.jna;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.LongByReference;

/**
 * The Interface Libvirt.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface Libvirt extends Library
{
	// Callbacks
	/**
	 * The Interface VirConnectAuthCallback.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	interface VirConnectAuthCallback extends Callback
	{

		/**
		 * Auth callback.
		 * 
		 * @param cred
		 *            the cred
		 * @param ncred
		 *            the ncred
		 * @param cbdata
		 *            the cbdata
		 * @return the int
		 */
		public int authCallback(virConnectCredential cred, int ncred, Pointer cbdata);
	}

	/**
	 * The Interface VirErrorCallback.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	interface VirErrorCallback extends Callback
	{

		/**
		 * Error callback.
		 * 
		 * @param userData
		 *            the user data
		 * @param error
		 *            the error
		 */
		public void errorCallback(Pointer userData, virError error);
	}

	/**
	 * The Interface VirStreamSinkFunc.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	interface VirStreamSinkFunc extends Callback
	{

		/**
		 * Sink callback.
		 * 
		 * @param virStreamPtr
		 *            the vir stream ptr
		 * @param data
		 *            the data
		 * @param nbytes
		 *            the nbytes
		 * @param opaque
		 *            the opaque
		 * @return the int
		 */
		public int sinkCallback(StreamPointer virStreamPtr, String data, NativeLong nbytes, Pointer opaque);
	}

	/**
	 * The Interface VirStreamSourceFunc.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	interface VirStreamSourceFunc extends Callback
	{

		/**
		 * Source callback.
		 * 
		 * @param virStreamPtr
		 *            the vir stream ptr
		 * @param data
		 *            the data
		 * @param nbytes
		 *            the nbytes
		 * @param opaque
		 *            the opaque
		 * @return the int
		 */
		public int sourceCallback(StreamPointer virStreamPtr, String data, NativeLong nbytes, Pointer opaque);
	}

	/**
	 * The Interface VirStreamEventCallback.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	interface VirStreamEventCallback extends Callback
	{

		/**
		 * Event callback.
		 * 
		 * @param virStreamPointer
		 *            the vir stream pointer
		 * @param events
		 *            the events
		 * @param opaque
		 *            the opaque
		 */
		public void eventCallback(StreamPointer virStreamPointer, int events, Pointer opaque);
	}

	/**
	 * The Interface VirFreeCallback.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	interface VirFreeCallback extends Callback
	{

		/**
		 * Free callback.
		 * 
		 * @param opaque
		 *            the opaque
		 */
		public void freeCallback(Pointer opaque);
	}

	/**
	 * The Interface VirConnectDomainEventGenericCallback.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	interface VirConnectDomainEventGenericCallback extends Callback
	{

		/**
		 * Event callback.
		 * 
		 * @param virConnectPtr
		 *            the vir connect ptr
		 * @param virDomainPointer
		 *            the vir domain pointer
		 * @param opaque
		 *            the opaque
		 */
		public void eventCallback(ConnectionPointer virConnectPtr, DomainPointer virDomainPointer, Pointer opaque);
	}

	/** The instance. */
	Libvirt INSTANCE = (Libvirt) Native.loadLibrary("virt", Libvirt.class);

	// Constants we need
	/** The vir uuid buflen. */
	public static int VIR_UUID_BUFLEN = 16;

	/** The vir uuid string buflen. */
	public static int VIR_UUID_STRING_BUFLEN = 36 + 1;

	/** The vir domain sched field length. */
	public static int VIR_DOMAIN_SCHED_FIELD_LENGTH = 80;

	// Connection Functions
	/**
	 * Vir connect baseline cpu.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xmlCPUs
	 *            the xml cp us
	 * @param ncpus
	 *            the ncpus
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virConnectBaselineCPU(ConnectionPointer virConnectPtr, String[] xmlCPUs, int ncpus, int flags);

	/**
	 * Vir conn copy last error.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param to
	 *            the to
	 * @return the int
	 */
	public int virConnCopyLastError(ConnectionPointer virConnectPtr, virError to);

	/**
	 * Vir connect close.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectClose(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect compare cpu.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virConnectCompareCPU(ConnectionPointer virConnectPtr, String xmlDesc, int flags);

	/**
	 * Vir connect domain event register any.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param eventID
	 *            the event id
	 * @param cb
	 *            the cb
	 * @param opaque
	 *            the opaque
	 * @param freecb
	 *            the freecb
	 * @return the int
	 */
	public int virConnectDomainEventRegisterAny(ConnectionPointer virConnectPtr, DomainPointer virDomainPtr, int eventID, Libvirt.VirConnectDomainEventGenericCallback cb,
			Pointer opaque, Libvirt.VirFreeCallback freecb);

	/**
	 * Vir connect domain event deregister any.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param callbackID
	 *            the callback id
	 * @return the int
	 */
	public int virConnectDomainEventDeregisterAny(ConnectionPointer virConnectPtr, int callbackID);

	/**
	 * Vir conn set error func.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param userData
	 *            the user data
	 * @param callback
	 *            the callback
	 */
	public void virConnSetErrorFunc(ConnectionPointer virConnectPtr, Pointer userData, VirErrorCallback callback);

	/**
	 * Vir connect is encrypted.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectIsEncrypted(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect is secure.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectIsSecure(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect find storage pool sources.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param type
	 *            the type
	 * @param srcSpec
	 *            the src spec
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virConnectFindStoragePoolSources(ConnectionPointer virConnectPtr, String type, String srcSpec, int flags);

	/**
	 * Vir connect get capabilities.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the string
	 */
	public String virConnectGetCapabilities(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect get hostname.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the string
	 */
	public String virConnectGetHostname(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect get lib version.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param libVer
	 *            the lib ver
	 * @return the int
	 */
	public int virConnectGetLibVersion(ConnectionPointer virConnectPtr, LongByReference libVer);

	/**
	 * Vir connect get max vcpus.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param type
	 *            the type
	 * @return the int
	 */
	public int virConnectGetMaxVcpus(ConnectionPointer virConnectPtr, String type);

	/**
	 * Vir connect get type.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the string
	 */
	public String virConnectGetType(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect get uri.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the string
	 */
	public String virConnectGetURI(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect get version.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param hvVer
	 *            the hv ver
	 * @return the int
	 */
	public int virConnectGetVersion(ConnectionPointer virConnectPtr, LongByReference hvVer);

	/**
	 * Vir connect list defined domains.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virConnectListDefinedDomains(ConnectionPointer virConnectPtr, String[] name, int maxnames);

	/**
	 * Vir connect list defined networks.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virConnectListDefinedNetworks(ConnectionPointer virConnectPtr, String[] name, int maxnames);

	/**
	 * Vir connect list defined storage pools.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param names
	 *            the names
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virConnectListDefinedStoragePools(ConnectionPointer virConnectPtr, String[] names, int maxnames);

	/**
	 * Vir connect list defined interfaces.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @param maxNames
	 *            the max names
	 * @return the int
	 */
	public int virConnectListDefinedInterfaces(ConnectionPointer virConnectPtr, String[] name, int maxNames);

	/**
	 * Vir connect list domains.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param ids
	 *            the ids
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virConnectListDomains(ConnectionPointer virConnectPtr, int[] ids, int maxnames);

	/**
	 * Vir connect list interfaces.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @param maxNames
	 *            the max names
	 * @return the int
	 */
	public int virConnectListInterfaces(ConnectionPointer virConnectPtr, String[] name, int maxNames);

	/**
	 * Vir connect list networks.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virConnectListNetworks(ConnectionPointer virConnectPtr, String[] name, int maxnames);

	/**
	 * Vir connect list nw filters.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virConnectListNWFilters(ConnectionPointer virConnectPtr, String[] name, int maxnames);

	/**
	 * Vir connect list secrets.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uids
	 *            the uids
	 * @param maxUids
	 *            the max uids
	 * @return the int
	 */
	public int virConnectListSecrets(ConnectionPointer virConnectPtr, String[] uids, int maxUids);

	/**
	 * Vir connect list storage pools.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param names
	 *            the names
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virConnectListStoragePools(ConnectionPointer virConnectPtr, String[] names, int maxnames);

	/**
	 * Vir connect num of defined domains.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfDefinedDomains(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of defined networks.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfDefinedNetworks(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of defined interfaces.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfDefinedInterfaces(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of defined storage pools.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfDefinedStoragePools(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of domains.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfDomains(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of interfaces.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfInterfaces(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of networks.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfNetworks(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of nw filters.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfNWFilters(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of secrets.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfSecrets(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect num of storage pools.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnectNumOfStoragePools(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect open.
	 * 
	 * @param name
	 *            the name
	 * @return the connection pointer
	 */
	public ConnectionPointer virConnectOpen(String name);

	/**
	 * Vir connect open auth.
	 * 
	 * @param name
	 *            the name
	 * @param auth
	 *            the auth
	 * @param flags
	 *            the flags
	 * @return the connection pointer
	 */
	public ConnectionPointer virConnectOpenAuth(String name, virConnectAuth auth, int flags);

	/**
	 * Vir connect open read only.
	 * 
	 * @param name
	 *            the name
	 * @return the connection pointer
	 */
	public ConnectionPointer virConnectOpenReadOnly(String name);

	/**
	 * Vir conn get last error.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the vir error
	 */
	public virError virConnGetLastError(ConnectionPointer virConnectPtr);

	/**
	 * Vir conn reset last error.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virConnResetLastError(ConnectionPointer virConnectPtr);

	/**
	 * Vir connect domain xml from native.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param nativeFormat
	 *            the native format
	 * @param nativeConfig
	 *            the native config
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virConnectDomainXMLFromNative(ConnectionPointer virConnectPtr, String nativeFormat, String nativeConfig, int flags);

	/**
	 * Vir connect domain xml to native.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param nativeFormat
	 *            the native format
	 * @param domainXML
	 *            the domain xml
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virConnectDomainXMLToNative(ConnectionPointer virConnectPtr, String nativeFormat, String domainXML, int flags);

	// Global functions
	/**
	 * Vir get version.
	 * 
	 * @param libVer
	 *            the lib ver
	 * @param type
	 *            the type
	 * @param typeVer
	 *            the type ver
	 * @return the int
	 */
	public int virGetVersion(LongByReference libVer, String type, LongByReference typeVer);

	/**
	 * Vir initialize.
	 * 
	 * @return the int
	 */
	public int virInitialize();

	/**
	 * Vir copy last error.
	 * 
	 * @param error
	 *            the error
	 * @return the int
	 */
	public int virCopyLastError(virError error);

	/**
	 * Vir get last error.
	 * 
	 * @return the vir error
	 */
	public virError virGetLastError();

	/**
	 * Vir reset last error.
	 */
	public void virResetLastError();

	/**
	 * Vir set error func.
	 * 
	 * @param userData
	 *            the user data
	 * @param callback
	 *            the callback
	 */
	public void virSetErrorFunc(Pointer userData, VirErrorCallback callback);

	// Domain functions
	/**
	 * Vir domain abort job.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainAbortJob(DomainPointer virDomainPtr);

	/**
	 * Vir domain attach device.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param deviceXML
	 *            the device xml
	 * @return the int
	 */
	public int virDomainAttachDevice(DomainPointer virDomainPtr, String deviceXML);

	/**
	 * Vir domain attach device flags.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param deviceXML
	 *            the device xml
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainAttachDeviceFlags(DomainPointer virDomainPtr, String deviceXML, int flags);

	/**
	 * Vir domain block stats.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param path
	 *            the path
	 * @param stats
	 *            the stats
	 * @param size
	 *            the size
	 * @return the int
	 */
	public int virDomainBlockStats(DomainPointer virDomainPtr, String path, virDomainBlockStats stats, int size);

	/**
	 * Vir domain core dump.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param to
	 *            the to
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainCoreDump(DomainPointer virDomainPtr, String to, int flags);

	/**
	 * Vir domain create.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainCreate(DomainPointer virDomainPtr);

	/**
	 * Vir domain create with flags.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainCreateWithFlags(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain create linux.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the domain pointer
	 */
	public DomainPointer virDomainCreateLinux(ConnectionPointer virConnectPtr, String xmlDesc, int flags);

	/**
	 * Vir domain create xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the domain pointer
	 */
	public DomainPointer virDomainCreateXML(ConnectionPointer virConnectPtr, String xmlDesc, int flags);

	/**
	 * Vir domain define xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xmlDesc
	 *            the xml desc
	 * @return the domain pointer
	 */
	public DomainPointer virDomainDefineXML(ConnectionPointer virConnectPtr, String xmlDesc);

	/**
	 * Vir domain destroy.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainDestroy(DomainPointer virDomainPtr);

	/**
	 * Vir domain detach device.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param deviceXML
	 *            the device xml
	 * @return the int
	 */
	public int virDomainDetachDevice(DomainPointer virDomainPtr, String deviceXML);

	/**
	 * Vir domain detach device flags.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param deviceXML
	 *            the device xml
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainDetachDeviceFlags(DomainPointer virDomainPtr, String deviceXML, int flags);

	/**
	 * Vir domain free.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainFree(DomainPointer virDomainPtr);

	/**
	 * Vir domain get autostart.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param value
	 *            the value
	 * @return the int
	 */
	public int virDomainGetAutostart(DomainPointer virDomainPtr, IntByReference value);

	/**
	 * Vir domain get connect.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the connection pointer
	 */
	public ConnectionPointer virDomainGetConnect(DomainPointer virDomainPtr);

	/**
	 * Vir domain get block info.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param path
	 *            the path
	 * @param info
	 *            the info
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainGetBlockInfo(DomainPointer virDomainPtr, String path, virDomainBlockInfo info, int flags);

	/**
	 * Vir domain get id.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainGetID(DomainPointer virDomainPtr);

	/**
	 * Vir domain get info.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param vInfo
	 *            the v info
	 * @return the int
	 */
	public int virDomainGetInfo(DomainPointer virDomainPtr, virDomainInfo vInfo);

	/**
	 * Vir domain get job info.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param vInfo
	 *            the v info
	 * @return the int
	 */
	public int virDomainGetJobInfo(DomainPointer virDomainPtr, virDomainJobInfo vInfo);

	/**
	 * Vir domain get max memory.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the native long
	 */
	public NativeLong virDomainGetMaxMemory(DomainPointer virDomainPtr);

	/**
	 * Vir domain get max vcpus.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainGetMaxVcpus(DomainPointer virDomainPtr);

	/**
	 * Vir domain get name.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the string
	 */
	public String virDomainGetName(DomainPointer virDomainPtr);

	/**
	 * Vir domain get os type.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the string
	 */
	public String virDomainGetOSType(DomainPointer virDomainPtr);

	/**
	 * Vir domain get scheduler parameters.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param params
	 *            the params
	 * @param nparams
	 *            the nparams
	 * @return the int
	 */
	public int virDomainGetSchedulerParameters(DomainPointer virDomainPtr, virSchedParameter[] params, IntByReference nparams);

	/**
	 * Vir domain get scheduler type.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param nparams
	 *            the nparams
	 * @return the string
	 */
	public String virDomainGetSchedulerType(DomainPointer virDomainPtr, IntByReference nparams);

	/**
	 * Vir domain get uuid.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virDomainGetUUID(DomainPointer virDomainPtr, byte[] uuidString);

	/**
	 * Vir domain get uuid string.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virDomainGetUUIDString(DomainPointer virDomainPtr, byte[] uuidString);

	/**
	 * Vir domain get vcpus.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param info
	 *            the info
	 * @param maxInfo
	 *            the max info
	 * @param cpumaps
	 *            the cpumaps
	 * @param maplen
	 *            the maplen
	 * @return the int
	 */
	public int virDomainGetVcpus(DomainPointer virDomainPtr, virVcpuInfo[] info, int maxInfo, byte[] cpumaps, int maplen);

	/**
	 * Vir domain get xml desc.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virDomainGetXMLDesc(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain has current snapshot.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainHasCurrentSnapshot(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain has managed save image.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainHasManagedSaveImage(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain interface stats.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param path
	 *            the path
	 * @param stats
	 *            the stats
	 * @param size
	 *            the size
	 * @return the int
	 */
	public int virDomainInterfaceStats(DomainPointer virDomainPtr, String path, virDomainInterfaceStats stats, int size);

	/**
	 * Vir domain is active.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainIsActive(DomainPointer virDomainPtr);

	/**
	 * Vir domain is persistent.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainIsPersistent(DomainPointer virDomainPtr);

	/**
	 * Vir domain lookup by id.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param id
	 *            the id
	 * @return the domain pointer
	 */
	public DomainPointer virDomainLookupByID(ConnectionPointer virConnectPtr, int id);

	/**
	 * Vir domain lookup by name.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @return the domain pointer
	 */
	public DomainPointer virDomainLookupByName(ConnectionPointer virConnectPtr, String name);

	/**
	 * Vir domain lookup by uuid.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidBytes
	 *            the uuid bytes
	 * @return the domain pointer
	 */
	public DomainPointer virDomainLookupByUUID(ConnectionPointer virConnectPtr, byte[] uuidBytes);

	/**
	 * Vir domain lookup by uuid string.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidstr
	 *            the uuidstr
	 * @return the domain pointer
	 */
	public DomainPointer virDomainLookupByUUIDString(ConnectionPointer virConnectPtr, String uuidstr);

	/**
	 * Vir domain managed save.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainManagedSave(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain managed save remove.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainManagedSaveRemove(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain migrate.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param flags
	 *            the flags
	 * @param dname
	 *            the dname
	 * @param uri
	 *            the uri
	 * @param bandwidth
	 *            the bandwidth
	 * @return the domain pointer
	 */
	public DomainPointer virDomainMigrate(DomainPointer virDomainPtr, ConnectionPointer virConnectPtr, NativeLong flags, String dname, String uri, NativeLong bandwidth);

	/**
	 * Vir domain migrate set max downtime.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param downtime
	 *            the downtime
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainMigrateSetMaxDowntime(DomainPointer virDomainPtr, long downtime, int flags);

	/**
	 * Vir domain migrate to uri.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param duri
	 *            the duri
	 * @param flags
	 *            the flags
	 * @param dname
	 *            the dname
	 * @param bandwidth
	 *            the bandwidth
	 * @return the int
	 */
	public int virDomainMigrateToURI(DomainPointer virDomainPtr, String duri, NativeLong flags, String dname, NativeLong bandwidth);

	/**
	 * Vir domain memory stats.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param stats
	 *            the stats
	 * @param nr_stats
	 *            the nr_stats
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainMemoryStats(DomainPointer virDomainPtr, virDomainMemoryStats[] stats, int nr_stats, int flags);

	/**
	 * Vir domain pin vcpu.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param vcpu
	 *            the vcpu
	 * @param cpumap
	 *            the cpumap
	 * @param maplen
	 *            the maplen
	 * @return the int
	 */
	public int virDomainPinVcpu(DomainPointer virDomainPtr, int vcpu, byte[] cpumap, int maplen);

	/**
	 * Vir domain reboot.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainReboot(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain restore.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param from
	 *            the from
	 * @return the int
	 */
	public int virDomainRestore(ConnectionPointer virConnectPtr, String from);

	/**
	 * Vir domain revert to snapshot.
	 * 
	 * @param virDomainSnapshotPtr
	 *            the vir domain snapshot ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainRevertToSnapshot(DomainSnapshotPointer virDomainSnapshotPtr, int flags);

	/**
	 * Vir domain resume.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainResume(DomainPointer virDomainPtr);

	/**
	 * Vir domain save.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param to
	 *            the to
	 * @return the int
	 */
	public int virDomainSave(DomainPointer virDomainPtr, String to);

	/**
	 * Vir domain set autostart.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param autoStart
	 *            the auto start
	 * @return the int
	 */
	public int virDomainSetAutostart(DomainPointer virDomainPtr, int autoStart);

	/**
	 * Vir domain set max memory.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param maxMemory
	 *            the max memory
	 * @return the int
	 */
	public int virDomainSetMaxMemory(DomainPointer virDomainPtr, NativeLong maxMemory);

	/**
	 * Vir domain set memory.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param maxMemory
	 *            the max memory
	 * @return the int
	 */
	public int virDomainSetMemory(DomainPointer virDomainPtr, NativeLong maxMemory);

	/**
	 * Vir domain set scheduler parameters.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param params
	 *            the params
	 * @param nparams
	 *            the nparams
	 * @return the int
	 */
	public int virDomainSetSchedulerParameters(DomainPointer virDomainPtr, virSchedParameter[] params, int nparams);

	/**
	 * Vir domain set vcpus.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param nvcpus
	 *            the nvcpus
	 * @return the int
	 */
	public int virDomainSetVcpus(DomainPointer virDomainPtr, int nvcpus);

	/**
	 * Vir domain shutdown.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainShutdown(DomainPointer virDomainPtr);

	/**
	 * Vir domain suspend.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainSuspend(DomainPointer virDomainPtr);

	/**
	 * Vir domain update device flags.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainUpdateDeviceFlags(DomainPointer virDomainPtr, String xml, int flags);

	/**
	 * Vir domain undefine.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @return the int
	 */
	public int virDomainUndefine(DomainPointer virDomainPtr);

	// Network functions
	/**
	 * Vir network get connect.
	 * 
	 * @param virnetworkPtr
	 *            the virnetwork ptr
	 * @return the connection pointer
	 */
	public ConnectionPointer virNetworkGetConnect(NetworkPointer virnetworkPtr);

	/**
	 * Vir network create.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virNetworkCreate(NetworkPointer virConnectPtr);

	/**
	 * Vir network create xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xmlDesc
	 *            the xml desc
	 * @return the network pointer
	 */
	public NetworkPointer virNetworkCreateXML(ConnectionPointer virConnectPtr, String xmlDesc);

	/**
	 * Vir network define xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xmlDesc
	 *            the xml desc
	 * @return the network pointer
	 */
	public NetworkPointer virNetworkDefineXML(ConnectionPointer virConnectPtr, String xmlDesc);

	/**
	 * Vir network destroy.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virNetworkDestroy(NetworkPointer virConnectPtr);

	/**
	 * Vir network free.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virNetworkFree(NetworkPointer virConnectPtr);

	/**
	 * Vir network get autostart.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @param value
	 *            the value
	 * @return the int
	 */
	public int virNetworkGetAutostart(NetworkPointer virNetworkPtr, IntByReference value);

	/**
	 * Vir network get bridge name.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @return the string
	 */
	public String virNetworkGetBridgeName(NetworkPointer virNetworkPtr);

	/**
	 * Vir network get name.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @return the string
	 */
	public String virNetworkGetName(NetworkPointer virNetworkPtr);

	/**
	 * Vir network get uuid.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virNetworkGetUUID(NetworkPointer virNetworkPtr, byte[] uuidString);

	/**
	 * Vir network get uuid string.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virNetworkGetUUIDString(NetworkPointer virNetworkPtr, byte[] uuidString);

	/**
	 * Vir network get xml desc.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virNetworkGetXMLDesc(NetworkPointer virNetworkPtr, int flags);

	/**
	 * Vir network is active.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @return the int
	 */
	public int virNetworkIsActive(NetworkPointer virNetworkPtr);

	/**
	 * Vir network is persistent.
	 * 
	 * @param virNetworkPtr
	 *            the vir network ptr
	 * @return the int
	 */
	public int virNetworkIsPersistent(NetworkPointer virNetworkPtr);

	/**
	 * Vir network lookup by name.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @return the network pointer
	 */
	public NetworkPointer virNetworkLookupByName(ConnectionPointer virConnectPtr, String name);

	/**
	 * Vir network lookup by uuid.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidBytes
	 *            the uuid bytes
	 * @return the network pointer
	 */
	public NetworkPointer virNetworkLookupByUUID(ConnectionPointer virConnectPtr, byte[] uuidBytes);

	/**
	 * Vir network lookup by uuid string.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidstr
	 *            the uuidstr
	 * @return the network pointer
	 */
	public NetworkPointer virNetworkLookupByUUIDString(ConnectionPointer virConnectPtr, String uuidstr);

	/**
	 * Vir network set autostart.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param autoStart
	 *            the auto start
	 * @return the int
	 */
	public int virNetworkSetAutostart(NetworkPointer virConnectPtr, int autoStart);

	/**
	 * Vir network undefine.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the int
	 */
	public int virNetworkUndefine(NetworkPointer virConnectPtr);

	// Node functions
	/**
	 * Vir node get info.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param virNodeInfo
	 *            the vir node info
	 * @return the int
	 */
	public int virNodeGetInfo(ConnectionPointer virConnectPtr, virNodeInfo virNodeInfo);

	/**
	 * Vir node get cells free memory.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param freeMems
	 *            the free mems
	 * @param startCell
	 *            the start cell
	 * @param maxCells
	 *            the max cells
	 * @return the int
	 */
	public int virNodeGetCellsFreeMemory(ConnectionPointer virConnectPtr, LongByReference freeMems, int startCell, int maxCells);

	/**
	 * Vir node get free memory.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @return the long
	 */
	public long virNodeGetFreeMemory(ConnectionPointer virConnectPtr);

	// Node/Device functions
	/**
	 * Vir node num of devices.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param capabilityName
	 *            the capability name
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virNodeNumOfDevices(ConnectionPointer virConnectPtr, String capabilityName, int flags);

	/**
	 * Vir node list devices.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param capabilityName
	 *            the capability name
	 * @param names
	 *            the names
	 * @param maxnames
	 *            the maxnames
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virNodeListDevices(ConnectionPointer virConnectPtr, String capabilityName, String[] names, int maxnames, int flags);

	/**
	 * Vir node device lookup by name.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @return the device pointer
	 */
	public DevicePointer virNodeDeviceLookupByName(ConnectionPointer virConnectPtr, String name);

	/**
	 * Vir node device get name.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the string
	 */
	public String virNodeDeviceGetName(DevicePointer virDevicePointer);

	/**
	 * Vir node device get parent.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the string
	 */
	public String virNodeDeviceGetParent(DevicePointer virDevicePointer);

	/**
	 * Vir node device num of caps.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virNodeDeviceNumOfCaps(DevicePointer virDevicePointer);

	/**
	 * Vir node device list caps.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @param names
	 *            the names
	 * @param maxNames
	 *            the max names
	 * @return the int
	 */
	public int virNodeDeviceListCaps(DevicePointer virDevicePointer, String[] names, int maxNames);

	/**
	 * Vir node device get xml desc.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the string
	 */
	public String virNodeDeviceGetXMLDesc(DevicePointer virDevicePointer);

	/**
	 * Vir node device free.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virNodeDeviceFree(DevicePointer virDevicePointer);

	/**
	 * Vir node device dettach.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virNodeDeviceDettach(DevicePointer virDevicePointer);

	/**
	 * Vir node device re attach.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virNodeDeviceReAttach(DevicePointer virDevicePointer);

	/**
	 * Vir node device reset.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virNodeDeviceReset(DevicePointer virDevicePointer);

	/**
	 * Vir node device create xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the device pointer
	 */
	public DevicePointer virNodeDeviceCreateXML(ConnectionPointer virConnectPtr, String xml, int flags);

	/**
	 * Vir node device destroy.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virNodeDeviceDestroy(DevicePointer virDevicePointer);

	// Storage Pool
	/**
	 * Vir storage pool build.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virStoragePoolBuild(StoragePoolPointer storagePoolPtr, int flags);

	/**
	 * Vir storage pool create.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virStoragePoolCreate(StoragePoolPointer storagePoolPtr, int flags);

	/**
	 * Vir storage pool create xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the storage pool pointer
	 */
	public StoragePoolPointer virStoragePoolCreateXML(ConnectionPointer virConnectPtr, String xml, int flags);

	/**
	 * Vir storage pool define xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the storage pool pointer
	 */
	public StoragePoolPointer virStoragePoolDefineXML(ConnectionPointer virConnectPtr, String xml, int flags);

	/**
	 * Vir storage pool delete.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virStoragePoolDelete(StoragePoolPointer storagePoolPtr, int flags);

	/**
	 * Vir storage pool destroy.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the int
	 */
	public int virStoragePoolDestroy(StoragePoolPointer storagePoolPtr);

	/**
	 * Vir storage pool free.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the int
	 */
	public int virStoragePoolFree(StoragePoolPointer storagePoolPtr);

	/**
	 * Vir storage pool get autostart.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param value
	 *            the value
	 * @return the int
	 */
	public int virStoragePoolGetAutostart(StoragePoolPointer storagePoolPtr, IntByReference value);

	/**
	 * Vir storage pool get info.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param info
	 *            the info
	 * @return the int
	 */
	public int virStoragePoolGetInfo(StoragePoolPointer storagePoolPtr, virStoragePoolInfo info);

	/**
	 * Vir storage pool get name.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the string
	 */
	public String virStoragePoolGetName(StoragePoolPointer storagePoolPtr);

	/**
	 * Vir storage pool get uuid.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virStoragePoolGetUUID(StoragePoolPointer storagePoolPtr, byte[] uuidString);

	/**
	 * Vir storage pool get uuid string.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virStoragePoolGetUUIDString(StoragePoolPointer storagePoolPtr, byte[] uuidString);

	/**
	 * Vir storage pool get xml desc.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virStoragePoolGetXMLDesc(StoragePoolPointer storagePoolPtr, int flags);

	/**
	 * Vir storage pool list volumes.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param names
	 *            the names
	 * @param maxnames
	 *            the maxnames
	 * @return the int
	 */
	public int virStoragePoolListVolumes(StoragePoolPointer storagePoolPtr, String[] names, int maxnames);

	/**
	 * Vir storage pool is active.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the int
	 */
	public int virStoragePoolIsActive(StoragePoolPointer storagePoolPtr);

	/**
	 * Vir storage pool is persistent.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the int
	 */
	public int virStoragePoolIsPersistent(StoragePoolPointer storagePoolPtr);

	/**
	 * Vir storage pool lookup by name.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @return the storage pool pointer
	 */
	public StoragePoolPointer virStoragePoolLookupByName(ConnectionPointer virConnectPtr, String name);

	/**
	 * Vir storage pool lookup by uuid.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidBytes
	 *            the uuid bytes
	 * @return the storage pool pointer
	 */
	public StoragePoolPointer virStoragePoolLookupByUUID(ConnectionPointer virConnectPtr, byte[] uuidBytes);

	/**
	 * Vir storage pool lookup by uuid string.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidstr
	 *            the uuidstr
	 * @return the storage pool pointer
	 */
	public StoragePoolPointer virStoragePoolLookupByUUIDString(ConnectionPointer virConnectPtr, String uuidstr);

	/**
	 * Vir storage pool lookup by volume.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @return the storage pool pointer
	 */
	public StoragePoolPointer virStoragePoolLookupByVolume(StorageVolPointer storageVolPtr);

	/**
	 * Vir storage pool num of volumes.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the int
	 */
	public int virStoragePoolNumOfVolumes(StoragePoolPointer storagePoolPtr);

	/**
	 * Vir storage pool refresh.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the int
	 */
	public int virStoragePoolRefresh(StoragePoolPointer storagePoolPtr);

	/**
	 * Vir storage pool set autostart.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param autostart
	 *            the autostart
	 * @return the int
	 */
	public int virStoragePoolSetAutostart(StoragePoolPointer storagePoolPtr, int autostart);

	/**
	 * Vir storage pool undefine.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @return the int
	 */
	public int virStoragePoolUndefine(StoragePoolPointer storagePoolPtr);

	// Storage Vol
	/**
	 * Vir storage vol create xml.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the storage vol pointer
	 */
	public StorageVolPointer virStorageVolCreateXML(StoragePoolPointer storagePoolPtr, String xml, int flags);

	/**
	 * Vir storage vol create xml from.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param xml
	 *            the xml
	 * @param cloneVolume
	 *            the clone volume
	 * @param flags
	 *            the flags
	 * @return the storage vol pointer
	 */
	public StorageVolPointer virStorageVolCreateXMLFrom(StoragePoolPointer storagePoolPtr, String xml, StorageVolPointer cloneVolume, int flags);

	/**
	 * Vir storage vol delete.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virStorageVolDelete(StorageVolPointer storageVolPtr, int flags);

	/**
	 * Vir storage vol free.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @return the int
	 */
	public int virStorageVolFree(StorageVolPointer storageVolPtr);

	/**
	 * Vir storage vol get info.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @param info
	 *            the info
	 * @return the int
	 */
	public int virStorageVolGetInfo(StorageVolPointer storageVolPtr, virStorageVolInfo info);

	/**
	 * Vir storage vol get key.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @return the string
	 */
	public String virStorageVolGetKey(StorageVolPointer storageVolPtr);

	/**
	 * Vir storage vol get name.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @return the string
	 */
	public String virStorageVolGetName(StorageVolPointer storageVolPtr);

	/**
	 * Vir storage vol get path.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @return the string
	 */
	public String virStorageVolGetPath(StorageVolPointer storageVolPtr);

	/**
	 * Vir storage vol get xml desc.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virStorageVolGetXMLDesc(StorageVolPointer storageVolPtr, int flags);

	/**
	 * Vir storage vol lookup by key.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @return the storage vol pointer
	 */
	public StorageVolPointer virStorageVolLookupByKey(ConnectionPointer virConnectPtr, String name);

	/**
	 * Vir storage vol lookup by name.
	 * 
	 * @param storagePoolPtr
	 *            the storage pool ptr
	 * @param name
	 *            the name
	 * @return the storage vol pointer
	 */
	public StorageVolPointer virStorageVolLookupByName(StoragePoolPointer storagePoolPtr, String name);

	/**
	 * Vir storage vol lookup by path.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param path
	 *            the path
	 * @return the storage vol pointer
	 */
	public StorageVolPointer virStorageVolLookupByPath(ConnectionPointer virConnectPtr, String path);

	/**
	 * Vir storage vol wipe.
	 * 
	 * @param storageVolPtr
	 *            the storage vol ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virStorageVolWipe(StorageVolPointer storageVolPtr, int flags);

	// Interface Methods
	/**
	 * Vir interface create.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virInterfaceCreate(InterfacePointer virDevicePointer);

	/**
	 * Vir interface define xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the interface pointer
	 */
	public InterfacePointer virInterfaceDefineXML(ConnectionPointer virConnectPtr, String xml, int flags);

	/**
	 * Vir interface destroy.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virInterfaceDestroy(InterfacePointer virDevicePointer);

	/**
	 * Vir interface free.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virInterfaceFree(InterfacePointer virDevicePointer);

	/**
	 * Vir interface get name.
	 * 
	 * @param virInterfacePtr
	 *            the vir interface ptr
	 * @return the string
	 */
	public String virInterfaceGetName(InterfacePointer virInterfacePtr);

	/**
	 * Vir interface get mac string.
	 * 
	 * @param virInterfacePtr
	 *            the vir interface ptr
	 * @return the string
	 */
	public String virInterfaceGetMACString(InterfacePointer virInterfacePtr);

	/**
	 * Vir interface get xml desc.
	 * 
	 * @param virInterfacePtr
	 *            the vir interface ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virInterfaceGetXMLDesc(InterfacePointer virInterfacePtr, int flags);

	/**
	 * Vir interface is active.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virInterfaceIsActive(InterfacePointer virDevicePointer);

	/**
	 * Vir interface lookup by mac string.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param mac
	 *            the mac
	 * @return the interface pointer
	 */
	public InterfacePointer virInterfaceLookupByMACString(ConnectionPointer virConnectPtr, String mac);

	/**
	 * Vir interface lookup by name.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @return the interface pointer
	 */
	public InterfacePointer virInterfaceLookupByName(ConnectionPointer virConnectPtr, String name);

	/**
	 * Vir interface undefine.
	 * 
	 * @param virDevicePointer
	 *            the vir device pointer
	 * @return the int
	 */
	public int virInterfaceUndefine(InterfacePointer virDevicePointer);

	// Secret Methods
	/**
	 * Vir secret get connect.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @return the connection pointer
	 */
	public ConnectionPointer virSecretGetConnect(SecretPointer virSecretPtr);

	/**
	 * Vir secret free.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @return the int
	 */
	public int virSecretFree(SecretPointer virSecretPtr);

	/**
	 * Vir secret define xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the secret pointer
	 */
	public SecretPointer virSecretDefineXML(ConnectionPointer virConnectPtr, String xml, int flags);

	/**
	 * Vir secret get uuid.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virSecretGetUUID(SecretPointer virSecretPtr, byte[] uuidString);

	/**
	 * Vir secret get uuid string.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virSecretGetUUIDString(SecretPointer virSecretPtr, byte[] uuidString);

	/**
	 * Vir secret get usage id.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @return the string
	 */
	public String virSecretGetUsageID(SecretPointer virSecretPtr);

	/**
	 * Vir secret get value.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @param value_size
	 *            the value_size
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virSecretGetValue(SecretPointer virSecretPtr, NativeLong value_size, int flags);

	/**
	 * Vir secret get xml desc.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virSecretGetXMLDesc(SecretPointer virSecretPtr, int flags);

	/**
	 * Vir secret lookup by usage.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param usageType
	 *            the usage type
	 * @param usageID
	 *            the usage id
	 * @return the secret pointer
	 */
	public SecretPointer virSecretLookupByUsage(ConnectionPointer virConnectPtr, int usageType, String usageID);

	/**
	 * Vir secret lookup by uuid.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidBytes
	 *            the uuid bytes
	 * @return the secret pointer
	 */
	public SecretPointer virSecretLookupByUUID(ConnectionPointer virConnectPtr, byte[] uuidBytes);

	/**
	 * Vir secret lookup by uuid string.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidstr
	 *            the uuidstr
	 * @return the secret pointer
	 */
	public SecretPointer virSecretLookupByUUIDString(ConnectionPointer virConnectPtr, String uuidstr);

	/**
	 * Vir secret set value.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @param value
	 *            the value
	 * @param value_size
	 *            the value_size
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virSecretSetValue(SecretPointer virSecretPtr, String value, NativeLong value_size, int flags);

	/**
	 * Vir secret undefine.
	 * 
	 * @param virSecretPtr
	 *            the vir secret ptr
	 * @return the int
	 */
	public int virSecretUndefine(SecretPointer virSecretPtr);

	// Stream Methods
	/**
	 * Vir stream abort.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @return the int
	 */
	public int virStreamAbort(StreamPointer virStreamPtr);

	/**
	 * Vir stream event add callback.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @param events
	 *            the events
	 * @param cb
	 *            the cb
	 * @param opaque
	 *            the opaque
	 * @param ff
	 *            the ff
	 * @return the int
	 */
	public int virStreamEventAddCallback(StreamPointer virStreamPtr, int events, Libvirt.VirStreamEventCallback cb, Pointer opaque, Libvirt.VirFreeCallback ff);

	/**
	 * Vir stream event update callback.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @param events
	 *            the events
	 * @return the int
	 */
	public int virStreamEventUpdateCallback(StreamPointer virStreamPtr, int events);

	/**
	 * Vir stream event remove callback.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @return the int
	 */
	public int virStreamEventRemoveCallback(StreamPointer virStreamPtr);

	/**
	 * Vir stream finish.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @return the int
	 */
	public int virStreamFinish(StreamPointer virStreamPtr);

	/**
	 * Vir stream free.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @return the int
	 */
	public int virStreamFree(StreamPointer virStreamPtr);

	/**
	 * Vir stream new.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param flags
	 *            the flags
	 * @return the stream pointer
	 */
	public StreamPointer virStreamNew(ConnectionPointer virConnectPtr, int flags);

	/**
	 * Vir stream send.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @param data
	 *            the data
	 * @param size
	 *            the size
	 * @return the int
	 */
	public int virStreamSend(StreamPointer virStreamPtr, String data, NativeLong size);

	/**
	 * Vir stream send all.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @param handler
	 *            the handler
	 * @param opaque
	 *            the opaque
	 * @return the int
	 */
	public int virStreamSendAll(StreamPointer virStreamPtr, Libvirt.VirStreamSourceFunc handler, Pointer opaque);

	/**
	 * Vir stream recv.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @param data
	 *            the data
	 * @param length
	 *            the length
	 * @return the int
	 */
	public int virStreamRecv(StreamPointer virStreamPtr, byte[] data, NativeLong length);

	/**
	 * Vir stream recv all.
	 * 
	 * @param virStreamPtr
	 *            the vir stream ptr
	 * @param handler
	 *            the handler
	 * @param opaque
	 *            the opaque
	 * @return the int
	 */
	public int virStreamRecvAll(StreamPointer virStreamPtr, Libvirt.VirStreamSinkFunc handler, Pointer opaque);

	// DomainSnapshot Methods
	/**
	 * Vir domain snapshot create xml.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @return the domain snapshot pointer
	 */
	public DomainSnapshotPointer virDomainSnapshotCreateXML(DomainPointer virDomainPtr, String xmlDesc, int flags);

	/**
	 * Vir domain snapshot current.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the domain snapshot pointer
	 */
	public DomainSnapshotPointer virDomainSnapshotCurrent(DomainPointer virDomainPtr, int flags);

	/**
	 * Vir domain snapshot delete.
	 * 
	 * @param virDomainSnapshotPtr
	 *            the vir domain snapshot ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainSnapshotDelete(DomainSnapshotPointer virDomainSnapshotPtr, int flags);

	/**
	 * Vir domain snapshot get xml desc.
	 * 
	 * @param virDomainSnapshotPtr
	 *            the vir domain snapshot ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virDomainSnapshotGetXMLDesc(DomainSnapshotPointer virDomainSnapshotPtr, int flags);

	/**
	 * Vir domain snapshot free.
	 * 
	 * @param virDomainSnapshotPtr
	 *            the vir domain snapshot ptr
	 * @return the int
	 */
	public int virDomainSnapshotFree(DomainSnapshotPointer virDomainSnapshotPtr);

	/**
	 * Vir domain snapshot list names.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param names
	 *            the names
	 * @param nameslen
	 *            the nameslen
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainSnapshotListNames(DomainPointer virDomainPtr, String[] names, int nameslen, int flags);

	/**
	 * Vir domain snapshot lookup by name.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param name
	 *            the name
	 * @param flags
	 *            the flags
	 * @return the domain snapshot pointer
	 */
	public DomainSnapshotPointer virDomainSnapshotLookupByName(DomainPointer virDomainPtr, String name, int flags);

	/**
	 * Vir domain snapshot num.
	 * 
	 * @param virDomainPtr
	 *            the vir domain ptr
	 * @param flags
	 *            the flags
	 * @return the int
	 */
	public int virDomainSnapshotNum(DomainPointer virDomainPtr, int flags);

	// Network Filter Methods
	/**
	 * Vir nw filter get xml desc.
	 * 
	 * @param virNWFilterPtr
	 *            the vir nw filter ptr
	 * @param flags
	 *            the flags
	 * @return the string
	 */
	public String virNWFilterGetXMLDesc(NetworkFilterPointer virNWFilterPtr, int flags);

	/**
	 * Vir nw filter define xml.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param xml
	 *            the xml
	 * @return the network filter pointer
	 */
	public NetworkFilterPointer virNWFilterDefineXML(ConnectionPointer virConnectPtr, String xml);

	/**
	 * Vir nw filter free.
	 * 
	 * @param virNWFilterPtr
	 *            the vir nw filter ptr
	 * @return the int
	 */
	public int virNWFilterFree(NetworkFilterPointer virNWFilterPtr);

	/**
	 * Vir nw filter lookup by name.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param name
	 *            the name
	 * @return the network filter pointer
	 */
	public NetworkFilterPointer virNWFilterLookupByName(ConnectionPointer virConnectPtr, String name);

	/**
	 * Vir nw filter lookup by uuid.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidBytes
	 *            the uuid bytes
	 * @return the network filter pointer
	 */
	public NetworkFilterPointer virNWFilterLookupByUUID(ConnectionPointer virConnectPtr, byte[] uuidBytes);

	/**
	 * Vir nw filter lookup by uuid string.
	 * 
	 * @param virConnectPtr
	 *            the vir connect ptr
	 * @param uuidstr
	 *            the uuidstr
	 * @return the network filter pointer
	 */
	public NetworkFilterPointer virNWFilterLookupByUUIDString(ConnectionPointer virConnectPtr, String uuidstr);

	/**
	 * Vir nw filter get name.
	 * 
	 * @param virNWFilterPtr
	 *            the vir nw filter ptr
	 * @return the string
	 */
	public String virNWFilterGetName(NetworkFilterPointer virNWFilterPtr);

	/**
	 * Vir nw filter get uuid.
	 * 
	 * @param virNWFilterPtr
	 *            the vir nw filter ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virNWFilterGetUUID(NetworkFilterPointer virNWFilterPtr, byte[] uuidString);

	/**
	 * Vir nw filter get uuid string.
	 * 
	 * @param virNWFilterPtr
	 *            the vir nw filter ptr
	 * @param uuidString
	 *            the uuid string
	 * @return the int
	 */
	public int virNWFilterGetUUIDString(NetworkFilterPointer virNWFilterPtr, byte[] uuidString);

	/**
	 * Vir nw filter undefine.
	 * 
	 * @param virNWFilterPtr
	 *            the vir nw filter ptr
	 * @return the int
	 */
	public int virNWFilterUndefine(NetworkFilterPointer virNWFilterPtr);
}
