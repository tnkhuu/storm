/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt.jna;

import com.sun.jna.NativeLong;
import com.sun.jna.Structure;

/**
 * The Class virNodeInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class virNodeInfo extends Structure
{

	/**
	 * The Class ByReference.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public class ByReference extends virNodeInfo implements Structure.ByReference
	{
	};

	/**
	 * The Class ByValue.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public class ByValue extends virNodeInfo implements Structure.ByValue
	{
	};

	/** The model. */
	public byte model[] = new byte[32];

	/** The memory. */
	public NativeLong memory;

	/** The cpus. */
	public int cpus;

	/** The mhz. */
	public int mhz;

	/** The nodes. */
	public int nodes;

	/** The sockets. */
	public int sockets;

	/** The cores. */
	public int cores;

	/** The threads. */
	public int threads;
}