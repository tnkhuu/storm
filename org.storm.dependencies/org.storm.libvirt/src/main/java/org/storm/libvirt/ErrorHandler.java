/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.virError;

/**
 * The Class ErrorHandler.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ErrorHandler
{

	/**
	 * Process error.
	 * 
	 * @param libvirt
	 *            the libvirt
	 * @param ret
	 *            the ret
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public static void processError(Libvirt libvirt, Object ret) throws LibvirtException
	{
		if (ret == null)
		{
			processError(libvirt);
		}
		else
		{
			libvirt.virResetLastError();
		}
	}

	/**
	 * Process error.
	 * 
	 * @param libvirt
	 *            the libvirt
	 * @param ret
	 *            the ret
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public static void processError(Libvirt libvirt, int ret) throws LibvirtException
	{
		if (ret == -1)
		{
			processError(libvirt);
		}
		else
		{
			libvirt.virResetLastError();
		}
	}

	/**
	 * Process error.
	 * 
	 * @param libvirt
	 *            the libvirt
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	private static void processError(Libvirt libvirt) throws LibvirtException
	{
		virError vError = new virError();
		int errorCode = libvirt.virCopyLastError(vError);
		if (errorCode > 0)
		{
			Error error = new Error(vError);
			libvirt.virResetLastError();
			/*
			 * FIXME: Don't throw exceptions for VIR_ERR_WARNING level errors
			 */
			if (error.getLevel() == Error.ErrorLevel.VIR_ERR_ERROR)
			{
				throw new LibvirtException(error);
			}
		}
	}
}
