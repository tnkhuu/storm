/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.NetworkFilterPointer;

import com.sun.jna.Native;

/**
 * The Class NetworkFilter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class NetworkFilter
{

	/** The nfp. */
	NetworkFilterPointer NFP;

	/** The vir connect. */
	@SuppressWarnings("unused")
	private Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new network filter.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param NFP
	 *            the nfp
	 */
	public NetworkFilter(Connect virConnect, NetworkFilterPointer NFP)
	{
		this.NFP = NFP;
		this.virConnect = virConnect;
		libvirt = virConnect.libvirt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (NFP != null)
		{
			success = libvirt.virNWFilterFree(NFP);
			ErrorHandler.processError(libvirt, success);
			NFP = null;
		}

		return success;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getName() throws LibvirtException
	{
		String returnValue = libvirt.virNWFilterGetName(NFP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the uuid.
	 * 
	 * @return the uuid
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int[] getUUID() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_BUFLEN];
		int success = libvirt.virNWFilterGetUUID(NFP, bytes);
		ErrorHandler.processError(libvirt, success);
		int[] returnValue = new int[0];
		if (success == 0)
		{
			returnValue = Connect.convertUUIDBytes(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the uUID string.
	 * 
	 * @return the uUID string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getUUIDString() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_STRING_BUFLEN];
		int success = libvirt.virNWFilterGetUUIDString(NFP, bytes);
		ErrorHandler.processError(libvirt, success);
		String returnValue = null;
		if (success == 0)
		{
			returnValue = Native.toString(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the xML desc.
	 * 
	 * @return the xML desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDesc() throws LibvirtException
	{
		String returnValue = libvirt.virNWFilterGetXMLDesc(NFP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Undefine.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void undefine() throws LibvirtException
	{
		int result = libvirt.virNWFilterUndefine(NFP);
		ErrorHandler.processError(libvirt, result);
	}
}
