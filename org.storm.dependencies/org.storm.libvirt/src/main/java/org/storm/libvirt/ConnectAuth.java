/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.virConnectCredential;

import com.sun.jna.Pointer;

/**
 * The Class ConnectAuth.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class ConnectAuth implements Libvirt.VirConnectAuthCallback
{

	/**
	 * The Class Credential.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public class Credential
	{

		/** The type. */
		public CredentialType type;

		/** The prompt. */
		public String prompt;

		/** The challenge. */
		public String challenge;

		/** The defresult. */
		public String defresult;

		/** The result. */
		public String result;

		/**
		 * Instantiates a new credential.
		 * 
		 * @param type
		 *            the type
		 * @param prompt
		 *            the prompt
		 * @param challenge
		 *            the challenge
		 * @param defresult
		 *            the defresult
		 */
		Credential(int type, String prompt, String challenge, String defresult)
		{
			switch (type)
			{
			case 1:
				this.type = CredentialType.VIR_CRED_USERNAME;
				break;
			case 2:
				this.type = CredentialType.VIR_CRED_AUTHNAME;
				break;
			case 3:
				this.type = CredentialType.VIR_CRED_LANGUAGE;
				break;
			case 4:
				this.type = CredentialType.VIR_CRED_CNONCE;
				break;
			case 5:
				this.type = CredentialType.VIR_CRED_PASSPHRASE;
				break;
			case 6:
				this.type = CredentialType.VIR_CRED_ECHOPROMPT;
				break;
			case 7:
				this.type = CredentialType.VIR_CRED_NOECHOPROMPT;
				break;
			case 8:
				this.type = CredentialType.VIR_CRED_REALM;
				break;
			case 9:
				this.type = CredentialType.VIR_CRED_EXTERNAL;
				break;
			default:
				assert false;
			}
			this.prompt = prompt;
			this.challenge = challenge;
			this.defresult = defresult;
		}

	}

	/**
	 * The Enum CredentialType.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum CredentialType
	{

		/** The vir cred none. */
		VIR_CRED_NONE,

		/** The vir cred username. */
		VIR_CRED_USERNAME,

		/** The vir cred authname. */
		VIR_CRED_AUTHNAME,

		/** The vir cred language. */
		VIR_CRED_LANGUAGE,

		/** The vir cred cnonce. */
		VIR_CRED_CNONCE,

		/** The vir cred passphrase. */
		VIR_CRED_PASSPHRASE,

		/** The vir cred echoprompt. */
		VIR_CRED_ECHOPROMPT,

		/** The vir cred noechoprompt. */
		VIR_CRED_NOECHOPROMPT,

		/** The vir cred realm. */
		VIR_CRED_REALM,

		/** The vir cred external. */
		VIR_CRED_EXTERNAL;

		/**
		 * Map to int.
		 * 
		 * @return the int
		 */
		@SuppressWarnings("all")
		public int mapToInt()
		{
			switch (this)
			{
			case VIR_CRED_USERNAME:
				return 1;
			case VIR_CRED_AUTHNAME:
				return 2;
			case VIR_CRED_LANGUAGE:
				return 3;
			case VIR_CRED_CNONCE:
				return 4;
			case VIR_CRED_PASSPHRASE:
				return 5;
			case VIR_CRED_ECHOPROMPT:
				return 6;
			case VIR_CRED_NOECHOPROMPT:
				return 7;
			case VIR_CRED_REALM:
				return 8;
			case VIR_CRED_EXTERNAL:
				return 9;
			}
			// We may never reach this point
			assert false;
			return 0;
		}
	}

	/** The cred type. */
	public CredentialType credType[];

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.libvirt.jna.Libvirt.VirConnectAuthCallback#authCallback(org
	 * .storm.libvirt.jna.virConnectCredential, int, com.sun.jna.Pointer)
	 */
	@Override
	public int authCallback(virConnectCredential cred, int ncred, Pointer cbdata)
	{
		virConnectCredential[] nativeCreds = (virConnectCredential[]) cred.toArray(ncred);
		Credential[] creds = new Credential[ncred];
		for (int x = 0; x < ncred; x++)
		{
			virConnectCredential vCred = nativeCreds[x];
			creds[x] = new Credential(vCred.type, vCred.prompt, vCred.challenge, vCred.defresult);
		}
		int returnValue = callback(creds);
		for (int x = 0; x < ncred; x++)
		{
			virConnectCredential vCred = nativeCreds[x];
			String result = creds[x].result;
			vCred.result = result;
			vCred.resultlen = result.length();
			vCred.write();
		}

		return returnValue;
	}

	/**
	 * Callback.
	 * 
	 * @param cred
	 *            the cred
	 * @return the int
	 */
	public abstract int callback(Credential[] cred);

}
