/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.virVcpuInfo;

/**
 * The Class VcpuInfo.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class VcpuInfo
{

	/**
	 * The Enum VcpuState.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static enum VcpuState
	{

		/** The vir vcpu offline. */
		VIR_VCPU_OFFLINE,
		/** The vir vcpu running. */
		VIR_VCPU_RUNNING,
		/** The vir vcpu blocked. */
		VIR_VCPU_BLOCKED
	}

	/** The number. */
	public int number;

	/** The state. */
	public VcpuState state;

	/** The cpu time. */
	public long cpuTime;

	/** The cpu. */
	public int cpu;;

	/**
	 * Instantiates a new vcpu info.
	 */
	public VcpuInfo()
	{

	}

	/**
	 * Instantiates a new vcpu info.
	 * 
	 * @param vVcpu
	 *            the v vcpu
	 */
	public VcpuInfo(virVcpuInfo vVcpu)
	{
		number = vVcpu.number;
		cpuTime = vVcpu.cpuTime;
		cpu = vVcpu.cpu;
		state = VcpuState.values()[vVcpu.state];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("number:%d%ncpuTime:%d%ncpu:%d%nstate:%s%n", number, cpuTime, cpu, state);
	}
}
