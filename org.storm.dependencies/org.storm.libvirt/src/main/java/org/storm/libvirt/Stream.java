/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.StreamPointer;

import com.sun.jna.NativeLong;

/**
 * The Class Stream.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Stream
{

	/** The vir stream nonblock. */
	public static int VIR_STREAM_NONBLOCK = 1 << 0;

	/** The vsp. */
	StreamPointer VSP;

	/** The vir connect. */
	@SuppressWarnings("unused")
	private Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new stream.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VSP
	 *            the vsp
	 */
	Stream(Connect virConnect, StreamPointer VSP)
	{
		this.virConnect = virConnect;
		this.VSP = VSP;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Abort.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int abort() throws LibvirtException
	{
		int returnValue = libvirt.virStreamAbort(VSP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Adds the callback.
	 * 
	 * @param events
	 *            the events
	 * @param cb
	 *            the cb
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int addCallback(int events, Libvirt.VirStreamEventCallback cb) throws LibvirtException
	{
		int returnValue = libvirt.virStreamEventAddCallback(VSP, events, cb, null, null);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Finish.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int finish() throws LibvirtException
	{
		int returnValue = libvirt.virStreamFinish(VSP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VSP != null)
		{
			success = libvirt.virStreamFree(VSP);
			ErrorHandler.processError(libvirt, success);
			VSP = null;
		}

		return success;
	}

	/**
	 * Receive.
	 * 
	 * @param data
	 *            the data
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int receive(byte[] data) throws LibvirtException
	{
		int returnValue = libvirt.virStreamRecv(VSP, data, new NativeLong(data.length));
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Receive all.
	 * 
	 * @param handler
	 *            the handler
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int receiveAll(Libvirt.VirStreamSinkFunc handler) throws LibvirtException
	{
		int returnValue = libvirt.virStreamRecvAll(VSP, handler, null);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Removes the callback.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int removeCallback() throws LibvirtException
	{
		int returnValue = libvirt.virStreamEventRemoveCallback(VSP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Send.
	 * 
	 * @param data
	 *            the data
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int send(String data) throws LibvirtException
	{
		int returnValue = libvirt.virStreamSend(VSP, data, new NativeLong(data.length()));
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Send all.
	 * 
	 * @param handler
	 *            the handler
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int sendAll(Libvirt.VirStreamSourceFunc handler) throws LibvirtException
	{
		int returnValue = libvirt.virStreamSendAll(VSP, handler, null);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Update callback.
	 * 
	 * @param events
	 *            the events
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int updateCallback(int events) throws LibvirtException
	{
		int returnValue = libvirt.virStreamEventUpdateCallback(VSP, events);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}
}
