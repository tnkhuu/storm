/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.NetworkPointer;

import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;

/**
 * The Class Network.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Network
{

	/** The vnp. */
	NetworkPointer VNP;

	/** The vir connect. */
	protected Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new network.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VNP
	 *            the vnp
	 */
	Network(Connect virConnect, NetworkPointer VNP)
	{
		this.virConnect = virConnect;
		this.VNP = VNP;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Creates the.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void create() throws LibvirtException
	{
		int result = libvirt.virNetworkCreate(VNP);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Destroy.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void destroy() throws LibvirtException
	{
		int result = libvirt.virNetworkDestroy(VNP);
		ErrorHandler.processError(libvirt, result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VNP != null)
		{
			success = libvirt.virNetworkFree(VNP);
			ErrorHandler.processError(libvirt, success);
			VNP = null;
		}

		return success;
	}

	/**
	 * Gets the autostart.
	 * 
	 * @return the autostart
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public boolean getAutostart() throws LibvirtException
	{
		IntByReference autoStart = new IntByReference();
		int result = libvirt.virNetworkGetAutostart(VNP, autoStart);
		ErrorHandler.processError(libvirt, result);
		return autoStart.getValue() != 0 ? true : false;
	}

	/**
	 * Gets the bridge name.
	 * 
	 * @return the bridge name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getBridgeName() throws LibvirtException
	{
		String returnValue = libvirt.virNetworkGetBridgeName(VNP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the connect.
	 * 
	 * @return the connect
	 */
	public Connect getConnect()
	{
		return virConnect;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getName() throws LibvirtException
	{
		String returnValue = libvirt.virNetworkGetName(VNP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the uuid.
	 * 
	 * @return the uuid
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int[] getUUID() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_BUFLEN];
		int success = libvirt.virNetworkGetUUID(VNP, bytes);
		ErrorHandler.processError(libvirt, success);
		int[] returnValue = new int[0];
		if (success == 0)
		{
			returnValue = Connect.convertUUIDBytes(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the uUID string.
	 * 
	 * @return the uUID string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getUUIDString() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_STRING_BUFLEN];
		int success = libvirt.virNetworkGetUUIDString(VNP, bytes);
		ErrorHandler.processError(libvirt, success);
		String returnValue = null;
		if (success == 0)
		{
			returnValue = Native.toString(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the xML desc.
	 * 
	 * @param flags
	 *            the flags
	 * @return the xML desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDesc(int flags) throws LibvirtException
	{
		String returnValue = libvirt.virNetworkGetXMLDesc(VNP, flags);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isActive() throws LibvirtException
	{
		int returnValue = libvirt.virNetworkIsActive(VNP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks if is persistent.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isPersistent() throws LibvirtException
	{
		int returnValue = libvirt.virNetworkIsPersistent(VNP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Sets the autostart.
	 * 
	 * @param autostart
	 *            the new autostart
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setAutostart(boolean autostart) throws LibvirtException
	{
		int autoValue = autostart ? 1 : 0;
		int result = libvirt.virNetworkSetAutostart(VNP, autoValue);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Undefine.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void undefine() throws LibvirtException
	{
		int result = libvirt.virNetworkUndefine(VNP);
		ErrorHandler.processError(libvirt, result);
	}

}
