/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.DomainPointer;
import org.storm.libvirt.jna.DomainSnapshotPointer;
import org.storm.libvirt.jna.Libvirt;
import org.storm.libvirt.jna.virDomainBlockInfo;
import org.storm.libvirt.jna.virDomainBlockStats;
import org.storm.libvirt.jna.virDomainInfo;
import org.storm.libvirt.jna.virDomainInterfaceStats;
import org.storm.libvirt.jna.virDomainJobInfo;
import org.storm.libvirt.jna.virDomainMemoryStats;
import org.storm.libvirt.jna.virSchedParameter;
import org.storm.libvirt.jna.virVcpuInfo;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.ptr.IntByReference;

/**
 * The Class Domain.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Domain
{

	/**
	 * The Class CreateFlags.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static final class CreateFlags
	{

		/** The Constant VIR_DOMAIN_NONE. */
		static final int VIR_DOMAIN_NONE = 0;
	}

	/**
	 * The Class MigrateFlags.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static final class MigrateFlags
	{

		/** The Constant VIR_MIGRATE_LIVE. */
		static final int VIR_MIGRATE_LIVE = 1;
	}

	/**
	 * The Class XMLFlags.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static final class XMLFlags
	{

		/** The Constant VIR_DOMAIN_XML_SECURE. */
		static final int VIR_DOMAIN_XML_SECURE = 1;

		/** The Constant VIR_DOMAIN_XML_INACTIVE. */
		static final int VIR_DOMAIN_XML_INACTIVE = 2;
	}

	/** The vdp. */
	DomainPointer VDP;

	/** The vir connect. */
	private Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new domain.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VDP
	 *            the vdp
	 */
	Domain(Connect virConnect, DomainPointer VDP)
	{
		this.virConnect = virConnect;
		this.VDP = VDP;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Abort job.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int abortJob() throws LibvirtException
	{
		int returnValue = libvirt.virDomainAbortJob(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Attach device.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void attachDevice(String xmlDesc) throws LibvirtException
	{
		int result = libvirt.virDomainAttachDevice(VDP, xmlDesc);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Attach device flags.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void attachDeviceFlags(String xmlDesc, int flags) throws LibvirtException
	{
		int result = libvirt.virDomainAttachDeviceFlags(VDP, xmlDesc, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Block info.
	 * 
	 * @param path
	 *            the path
	 * @return the domain block info
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainBlockInfo blockInfo(String path) throws LibvirtException
	{
		virDomainBlockInfo info = new virDomainBlockInfo();
		int success = libvirt.virDomainGetBlockInfo(VDP, path, info, 0);
		ErrorHandler.processError(libvirt, success);
		return success == 0 ? new DomainBlockInfo(info) : null;
	}

	/**
	 * Block stats.
	 * 
	 * @param path
	 *            the path
	 * @return the domain block stats
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainBlockStats blockStats(String path) throws LibvirtException
	{
		virDomainBlockStats stats = new virDomainBlockStats();
		int success = libvirt.virDomainBlockStats(VDP, path, stats, stats.size());
		ErrorHandler.processError(libvirt, success);
		return success == 0 ? new DomainBlockStats(stats) : null;
	}

	/**
	 * Core dump.
	 * 
	 * @param to
	 *            the to
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void coreDump(String to, int flags) throws LibvirtException
	{
		int result = libvirt.virDomainCoreDump(VDP, to, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Cpu map length.
	 * 
	 * @param maxCpus
	 *            the max cpus
	 * @return the int
	 */
	public int cpuMapLength(int maxCpus)
	{
		return (maxCpus + 7) / 8;
	}

	/**
	 * Creates the.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int create() throws LibvirtException
	{
		int returnValue = libvirt.virDomainCreate(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Creates the.
	 * 
	 * @param flags
	 *            the flags
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int create(int flags) throws LibvirtException
	{
		int returnValue = libvirt.virDomainCreateWithFlags(VDP, flags);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Destroy.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void destroy() throws LibvirtException
	{
		int result = libvirt.virDomainDestroy(VDP);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Detach device.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void detachDevice(String xmlDesc) throws LibvirtException
	{
		int result = libvirt.virDomainDetachDevice(VDP, xmlDesc);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Detach device flags.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void detachDeviceFlags(String xmlDesc, int flags) throws LibvirtException
	{
		int result = libvirt.virDomainDetachDeviceFlags(VDP, xmlDesc, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VDP != null)
		{
			success = libvirt.virDomainFree(VDP);
			ErrorHandler.processError(libvirt, success);
			VDP = null;
		}

		return success;
	}

	/**
	 * Gets the autostart.
	 * 
	 * @return the autostart
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public boolean getAutostart() throws LibvirtException
	{
		IntByReference autoStart = new IntByReference();
		int result = libvirt.virDomainGetAutostart(VDP, autoStart);
		ErrorHandler.processError(libvirt, result);
		return autoStart.getValue() != 0 ? true : false;
	}

	/**
	 * Gets the connect.
	 * 
	 * @return the connect
	 */
	public Connect getConnect()
	{
		return virConnect;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int getID() throws LibvirtException
	{
		int returnValue = libvirt.virDomainGetID(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the info.
	 * 
	 * @return the info
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainInfo getInfo() throws LibvirtException
	{
		DomainInfo returnValue = null;
		virDomainInfo vInfo = new virDomainInfo();
		int success = libvirt.virDomainGetInfo(VDP, vInfo);
		ErrorHandler.processError(libvirt, success);
		if (success == 0)
		{
			returnValue = new DomainInfo(vInfo);
		}
		return returnValue;
	}

	/**
	 * Gets the job info.
	 * 
	 * @return the job info
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainJobInfo getJobInfo() throws LibvirtException
	{
		DomainJobInfo returnValue = null;
		virDomainJobInfo vInfo = new virDomainJobInfo();
		int success = libvirt.virDomainGetJobInfo(VDP, vInfo);
		ErrorHandler.processError(libvirt, success);
		if (success == 0)
		{
			returnValue = new DomainJobInfo(vInfo);
		}
		return returnValue;
	}

	/**
	 * Gets the max memory.
	 * 
	 * @return the max memory
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public long getMaxMemory() throws LibvirtException
	{
		NativeLong returnValue = libvirt.virDomainGetMaxMemory(VDP);
		ErrorHandler.processError(libvirt, returnValue.longValue() - 1); // 0 is
																			// an
																			// error
																			// for
																			// this
																			// call
		return returnValue.longValue();
	}

	/**
	 * Gets the max vcpus.
	 * 
	 * @return the max vcpus
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int getMaxVcpus() throws LibvirtException
	{
		int returnValue = libvirt.virDomainGetMaxVcpus(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getName() throws LibvirtException
	{
		String returnValue = libvirt.virDomainGetName(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the oS type.
	 * 
	 * @return the oS type
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getOSType() throws LibvirtException
	{
		String returnValue = libvirt.virDomainGetOSType(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Gets the scheduler parameters.
	 * 
	 * @return the scheduler parameters
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public SchedParameter[] getSchedulerParameters() throws LibvirtException
	{
		IntByReference nParams = new IntByReference();
		SchedParameter[] returnValue = new SchedParameter[0];
		String scheduler = libvirt.virDomainGetSchedulerType(VDP, nParams);
		ErrorHandler.processError(libvirt, scheduler);
		if (scheduler != null)
		{
			virSchedParameter[] nativeParams = new virSchedParameter[nParams.getValue()];
			returnValue = new SchedParameter[nParams.getValue()];
			int result = libvirt.virDomainGetSchedulerParameters(VDP, nativeParams, nParams);
			ErrorHandler.processError(libvirt, result);
			for (int x = 0; x < nParams.getValue(); x++)
			{
				returnValue[x] = SchedParameter.create(nativeParams[x]);
			}
		}

		return returnValue;
	}

	// getSchedulerType
	// We don't expose the nparams return value, it's only needed for the
	// SchedulerParameters allocations,
	// but we handle that in getSchedulerParameters internally.
	/**
	 * Gets the scheduler type.
	 * 
	 * @return the scheduler type
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] getSchedulerType() throws LibvirtException
	{
		IntByReference nParams = new IntByReference();
		String returnValue = libvirt.virDomainGetSchedulerType(VDP, nParams);
		ErrorHandler.processError(libvirt, returnValue);
		String[] array = new String[1];
		array[0] = returnValue;
		return array;
	}

	/**
	 * Gets the uuid.
	 * 
	 * @return the uuid
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int[] getUUID() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_BUFLEN];
		int success = libvirt.virDomainGetUUID(VDP, bytes);
		ErrorHandler.processError(libvirt, success);
		int[] returnValue = new int[0];
		if (success == 0)
		{
			returnValue = Connect.convertUUIDBytes(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the uUID string.
	 * 
	 * @return the uUID string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getUUIDString() throws LibvirtException
	{
		byte[] bytes = new byte[Libvirt.VIR_UUID_STRING_BUFLEN];
		int success = libvirt.virDomainGetUUIDString(VDP, bytes);
		ErrorHandler.processError(libvirt, success);
		String returnValue = null;
		if (success == 0)
		{
			returnValue = Native.toString(bytes);
		}
		return returnValue;
	}

	/**
	 * Gets the vcpus cpu maps.
	 * 
	 * @return the vcpus cpu maps
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int[] getVcpusCpuMaps() throws LibvirtException
	{
		int[] returnValue = new int[0];
		int cpuCount = getMaxVcpus();

		if (cpuCount > 0)
		{
			NodeInfo nodeInfo = virConnect.nodeInfo();
			int maplength = cpuMapLength(nodeInfo.maxCpus());
			virVcpuInfo[] infos = new virVcpuInfo[cpuCount];
			returnValue = new int[cpuCount * maplength];
			byte[] cpumaps = new byte[cpuCount * maplength];
			int result = libvirt.virDomainGetVcpus(VDP, infos, cpuCount, cpumaps, maplength);
			ErrorHandler.processError(libvirt, result);
			for (int x = 0; x < cpuCount * maplength; x++)
			{
				returnValue[x] = cpumaps[x];
			}
		}
		return returnValue;
	}

	/**
	 * Gets the vcpus info.
	 * 
	 * @return the vcpus info
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public VcpuInfo[] getVcpusInfo() throws LibvirtException
	{
		int cpuCount = getMaxVcpus();
		VcpuInfo[] returnValue = new VcpuInfo[cpuCount];
		virVcpuInfo[] infos = new virVcpuInfo[cpuCount];
		int result = libvirt.virDomainGetVcpus(VDP, infos, cpuCount, null, 0);
		ErrorHandler.processError(libvirt, result);
		for (int x = 0; x < cpuCount; x++)
		{
			returnValue[x] = new VcpuInfo(infos[x]);
		}
		return returnValue;
	}

	/**
	 * Gets the xML desc.
	 * 
	 * @param flags
	 *            the flags
	 * @return the xML desc
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDesc(int flags) throws LibvirtException
	{
		String returnValue = libvirt.virDomainGetXMLDesc(VDP, flags);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks for current snapshot.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int hasCurrentSnapshot() throws LibvirtException
	{
		int returnValue = libvirt.virDomainHasCurrentSnapshot(VDP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks for managed save image.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int hasManagedSaveImage() throws LibvirtException
	{
		int returnValue = libvirt.virDomainHasManagedSaveImage(VDP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Interface stats.
	 * 
	 * @param path
	 *            the path
	 * @return the domain interface stats
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainInterfaceStats interfaceStats(String path) throws LibvirtException
	{
		virDomainInterfaceStats stats = new virDomainInterfaceStats();
		int result = libvirt.virDomainInterfaceStats(VDP, path, stats, stats.size());
		ErrorHandler.processError(libvirt, result);
		return new DomainInterfaceStats(stats);
	}

	/**
	 * Checks if is active.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isActive() throws LibvirtException
	{
		int returnValue = libvirt.virDomainIsActive(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Checks if is persistent.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isPersistent() throws LibvirtException
	{
		int returnValue = libvirt.virDomainIsPersistent(VDP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Managed save.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int managedSave() throws LibvirtException
	{
		int returnValue = libvirt.virDomainManagedSave(VDP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Managed save remote.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int managedSaveRemote() throws LibvirtException
	{
		int returnValue = libvirt.virDomainManagedSaveRemove(VDP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Memory stats.
	 * 
	 * @param number
	 *            the number
	 * @return the memory statistic[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public MemoryStatistic[] memoryStats(int number) throws LibvirtException
	{
		virDomainMemoryStats[] stats = new virDomainMemoryStats[number];
		MemoryStatistic[] returnStats = null;
		int result = libvirt.virDomainMemoryStats(VDP, stats, number, 0);
		ErrorHandler.processError(libvirt, result);
		if (result >= 0)
		{
			returnStats = new MemoryStatistic[result];
			for (int x = 0; x < result; x++)
			{
				returnStats[x] = new MemoryStatistic(stats[x]);
			}
		}
		return returnStats;
	}

	/**
	 * Migrate.
	 * 
	 * @param dconn
	 *            the dconn
	 * @param flags
	 *            the flags
	 * @param dname
	 *            the dname
	 * @param uri
	 *            the uri
	 * @param bandwidth
	 *            the bandwidth
	 * @return the domain
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public Domain migrate(Connect dconn, long flags, String dname, String uri, long bandwidth) throws LibvirtException
	{
		DomainPointer newPtr = libvirt.virDomainMigrate(VDP, dconn.VCP, new NativeLong(flags), dname, uri, new NativeLong(bandwidth));
		ErrorHandler.processError(libvirt, newPtr);
		return new Domain(dconn, newPtr);
	}

	/**
	 * Migrate set max downtime.
	 * 
	 * @param downtime
	 *            the downtime
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int migrateSetMaxDowntime(long downtime) throws LibvirtException
	{
		int returnValue = libvirt.virDomainMigrateSetMaxDowntime(VDP, downtime, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Migrate to uri.
	 * 
	 * @param uri
	 *            the uri
	 * @param flags
	 *            the flags
	 * @param dname
	 *            the dname
	 * @param bandwidth
	 *            the bandwidth
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int migrateToURI(String uri, long flags, String dname, long bandwidth) throws LibvirtException
	{
		int returnValue = libvirt.virDomainMigrateToURI(VDP, uri, new NativeLong(flags), dname, new NativeLong(bandwidth));
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Pin vcpu.
	 * 
	 * @param vcpu
	 *            the vcpu
	 * @param cpumap
	 *            the cpumap
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void pinVcpu(int vcpu, int[] cpumap) throws LibvirtException
	{
		byte[] packedMap = new byte[cpumap.length];
		for (int x = 0; x < cpumap.length; x++)
		{
			packedMap[x] = (byte) cpumap[x];
		}
		int result = libvirt.virDomainPinVcpu(VDP, vcpu, packedMap, cpumap.length);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Reboot.
	 * 
	 * @param flags
	 *            the flags
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void reboot(int flags) throws LibvirtException
	{
		int result = libvirt.virDomainReboot(VDP, flags);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Resume.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void resume() throws LibvirtException
	{
		int result = libvirt.virDomainResume(VDP);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Revert to snapshot.
	 * 
	 * @param snapshot
	 *            the snapshot
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int revertToSnapshot(DomainSnapshot snapshot) throws LibvirtException
	{
		int returnCode = libvirt.virDomainRevertToSnapshot(snapshot.VDSP, 0);
		ErrorHandler.processError(libvirt, returnCode);
		return returnCode;
	}

	/**
	 * Save.
	 * 
	 * @param to
	 *            the to
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void save(String to) throws LibvirtException
	{
		int result = libvirt.virDomainSave(VDP, to);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Sets the autostart.
	 * 
	 * @param autostart
	 *            the new autostart
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setAutostart(boolean autostart) throws LibvirtException
	{
		int autoValue = autostart ? 1 : 0;
		int result = libvirt.virDomainSetAutostart(VDP, autoValue);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Sets the max memory.
	 * 
	 * @param memory
	 *            the new max memory
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setMaxMemory(long memory) throws LibvirtException
	{
		int result = libvirt.virDomainSetMaxMemory(VDP, new NativeLong(memory));
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Sets the memory.
	 * 
	 * @param memory
	 *            the new memory
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setMemory(long memory) throws LibvirtException
	{
		int result = libvirt.virDomainSetMemory(VDP, new NativeLong(memory));
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Sets the scheduler parameters.
	 * 
	 * @param params
	 *            the new scheduler parameters
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setSchedulerParameters(SchedParameter[] params) throws LibvirtException
	{
		virSchedParameter[] input = new virSchedParameter[params.length];
		for (int x = 0; x < params.length; x++)
		{
			input[x] = SchedParameter.toNative(params[x]);
		}
		int result = libvirt.virDomainSetSchedulerParameters(VDP, input, params.length);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Sets the vcpus.
	 * 
	 * @param nvcpus
	 *            the new vcpus
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void setVcpus(int nvcpus) throws LibvirtException
	{
		int result = libvirt.virDomainSetVcpus(VDP, nvcpus);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Shutdown.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void shutdown() throws LibvirtException
	{
		int result = libvirt.virDomainShutdown(VDP);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Snapshot create xml.
	 * 
	 * @param xmlDesc
	 *            the xml desc
	 * @return the domain snapshot
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainSnapshot snapshotCreateXML(String xmlDesc) throws LibvirtException
	{
		DomainSnapshotPointer ptr = libvirt.virDomainSnapshotCreateXML(VDP, xmlDesc, 0);
		ErrorHandler.processError(libvirt, ptr);
		DomainSnapshot returnValue = null;
		if (ptr != null)
		{
			returnValue = new DomainSnapshot(virConnect, ptr);
		}
		return returnValue;
	}

	/**
	 * Snapshot current.
	 * 
	 * @return the domain snapshot
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainSnapshot snapshotCurrent() throws LibvirtException
	{
		DomainSnapshotPointer ptr = libvirt.virDomainSnapshotCurrent(VDP, 0);
		ErrorHandler.processError(libvirt, ptr);
		DomainSnapshot returnValue = null;
		if (ptr != null)
		{
			returnValue = new DomainSnapshot(virConnect, ptr);
		}
		return returnValue;
	}

	/**
	 * Snapshot list names.
	 * 
	 * @return the string[]
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String[] snapshotListNames() throws LibvirtException
	{
		String[] returnValue = null;
		int num = snapshotNum();
		if (num >= 0)
		{
			returnValue = new String[num];
			if (num > 0)
			{
				int result = libvirt.virDomainSnapshotListNames(VDP, returnValue, num, 0);
				ErrorHandler.processError(libvirt, result);
			}
		}
		return returnValue;
	}

	/**
	 * Snapshot lookup by name.
	 * 
	 * @param name
	 *            the name
	 * @return the domain snapshot
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public DomainSnapshot snapshotLookupByName(String name) throws LibvirtException
	{
		DomainSnapshotPointer ptr = libvirt.virDomainSnapshotLookupByName(VDP, name, 0);
		ErrorHandler.processError(libvirt, ptr);
		DomainSnapshot returnValue = null;
		if (ptr != null)
		{
			returnValue = new DomainSnapshot(virConnect, ptr);
		}
		return returnValue;
	}

	/**
	 * Snapshot num.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int snapshotNum() throws LibvirtException
	{
		int returnValue = libvirt.virDomainSnapshotNum(VDP, 0);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Suspend.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void suspend() throws LibvirtException
	{
		int result = libvirt.virDomainSuspend(VDP);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Undefine.
	 * 
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public void undefine() throws LibvirtException
	{
		int result = libvirt.virDomainUndefine(VDP);
		ErrorHandler.processError(libvirt, result);
	}

	/**
	 * Update device flags.
	 * 
	 * @param xml
	 *            the xml
	 * @param flags
	 *            the flags
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int updateDeviceFlags(String xml, int flags) throws LibvirtException
	{
		int returnValue = libvirt.virDomainUpdateDeviceFlags(VDP, xml, flags);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

}
