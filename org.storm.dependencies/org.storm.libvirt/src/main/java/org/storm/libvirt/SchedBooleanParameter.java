/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

/**
 * The Class SchedBooleanParameter.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public final class SchedBooleanParameter extends SchedParameter
{

	/** The value. */
	public boolean value;

	/**
	 * Instantiates a new sched boolean parameter.
	 */
	public SchedBooleanParameter()
	{

	}

	/**
	 * Instantiates a new sched boolean parameter.
	 * 
	 * @param value
	 *            the value
	 */
	public SchedBooleanParameter(boolean value)
	{
		this.value = value;
	}

	/**
	 * Instantiates a new sched boolean parameter.
	 * 
	 * @param value
	 *            the value
	 */
	public SchedBooleanParameter(byte value)
	{
		this.value = value != 0 ? true : false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.libvirt.SchedParameter#getType()
	 */
	@Override
	public int getType()
	{
		return 6;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.libvirt.SchedParameter#getTypeAsString()
	 */
	@Override
	public String getTypeAsString()
	{
		return "VIR_DOMAIN_SCHED_FIELD_BOOLEAN";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.libvirt.SchedParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString()
	{
		return Boolean.toString(value);
	}
}
