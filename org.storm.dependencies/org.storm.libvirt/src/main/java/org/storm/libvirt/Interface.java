/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import org.storm.libvirt.jna.InterfacePointer;
import org.storm.libvirt.jna.Libvirt;

/**
 * The Class Interface.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Interface
{

	/** The vir interface xml inactive. */
	public static int VIR_INTERFACE_XML_INACTIVE = 1;

	/** The vip. */
	InterfacePointer VIP;

	/** The vir connect. */
	@SuppressWarnings("unused")
	private Connect virConnect;

	/** The libvirt. */
	protected Libvirt libvirt;

	/**
	 * Instantiates a new interface.
	 * 
	 * @param virConnect
	 *            the vir connect
	 * @param VIP
	 *            the vip
	 */
	Interface(Connect virConnect, InterfacePointer VIP)
	{
		this.virConnect = virConnect;
		this.VIP = VIP;
		libvirt = virConnect.libvirt;
	}

	/**
	 * Creates the.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int create() throws LibvirtException
	{
		int returnValue = libvirt.virInterfaceCreate(VIP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Destroy.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int destroy() throws LibvirtException
	{
		int returnValue = libvirt.virInterfaceDestroy(VIP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws LibvirtException
	{
		free();
	}

	/**
	 * Free.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int free() throws LibvirtException
	{
		int success = 0;
		if (VIP != null)
		{
			success = libvirt.virInterfaceFree(VIP);
			ErrorHandler.processError(libvirt, success);
			VIP = null;
		}

		return success;
	}

	/**
	 * Gets the mAC string.
	 * 
	 * @return the mAC string
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getMACString() throws LibvirtException
	{
		String name = libvirt.virInterfaceGetMACString(VIP);
		return name;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getName() throws LibvirtException
	{
		String name = libvirt.virInterfaceGetName(VIP);
		return name;
	}

	/**
	 * Gets the xML description.
	 * 
	 * @param flags
	 *            the flags
	 * @return the xML description
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public String getXMLDescription(int flags) throws LibvirtException
	{
		String xml = libvirt.virInterfaceGetXMLDesc(VIP, flags);
		ErrorHandler.processError(libvirt, xml);
		return xml;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int isActive() throws LibvirtException
	{
		int returnValue = libvirt.virInterfaceIsActive(VIP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}

	/**
	 * Undefine.
	 * 
	 * @return the int
	 * @throws LibvirtException
	 *             the libvirt exception
	 */
	public int undefine() throws LibvirtException
	{
		int returnValue = libvirt.virInterfaceUndefine(VIP);
		ErrorHandler.processError(libvirt, returnValue);
		return returnValue;
	}
}
