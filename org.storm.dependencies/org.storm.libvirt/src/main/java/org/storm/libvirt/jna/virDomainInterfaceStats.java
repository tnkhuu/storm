/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt.jna;

import com.sun.jna.Structure;

/**
 * The Class virDomainInterfaceStats.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class virDomainInterfaceStats extends Structure
{

	/** The rx_bytes. */
	public long rx_bytes; // this is a long long in the code, so a long mapping
	// is correct
	/** The rx_packets. */
	public long rx_packets; // this is a long long in the code, so a long
	// mapping is correct
	/** The rx_errs. */
	public long rx_errs; // this is a long long in the code, so a long mapping
	// is correct
	/** The rx_drop. */
	public long rx_drop; // this is a long long in the code, so a long mapping
	// is correct
	/** The tx_bytes. */
	public long tx_bytes; // this is a long long in the code, so a long mapping
	// is correct
	/** The tx_packets. */
	public long tx_packets; // this is a long long in the code, so a long
	// mapping is correct
	/** The tx_errs. */
	public long tx_errs; // this is a long long in the code, so a long mapping
	// is correct
	/** The tx_drop. */
	public long tx_drop; // this is a long long in the code, so a long mapping
	// is correct

}
