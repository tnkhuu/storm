/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt.jna;

import com.sun.jna.Union;

/**
 * The Class virSchedParameterValue.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class virSchedParameterValue extends Union
{

	/** The i. */
	public int i; /* data for integer case */

	/** The ui. */
	public int ui; /* data for unsigned integer case */

	/** The l. */
	public long l; /* data for long long integer case */

	/** The ul. */
	public long ul; /* data for unsigned long long integer case */

	/** The d. */
	public double d; /* data for double case */

	/** The b. */
	public byte b; /* data for char case */
}
