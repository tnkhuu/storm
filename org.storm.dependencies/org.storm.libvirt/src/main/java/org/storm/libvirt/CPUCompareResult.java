/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.libvirt;

import java.util.EnumSet;
import java.util.HashMap;

/**
 * The Enum CPUCompareResult.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public enum CPUCompareResult
{

	/** The vir cpu compare error. */
	VIR_CPU_COMPARE_ERROR(-1),
	/** The vir cpu compare incompatible. */
	VIR_CPU_COMPARE_INCOMPATIBLE(0),
	/** The vir cpu compare identical. */
	VIR_CPU_COMPARE_IDENTICAL(1),
	/** The vir cpu compare superset. */
	VIR_CPU_COMPARE_SUPERSET(2);

	/** The lookup. */
	static HashMap<Integer, CPUCompareResult> lookup = new HashMap<Integer, CPUCompareResult>();

	static
	{
		for (CPUCompareResult s : EnumSet.allOf(CPUCompareResult.class))
		{
			lookup.put(s.getReturnCode(), s);
		}
	}

	/**
	 * Gets the.
	 * 
	 * @param value
	 *            the value
	 * @return the cPU compare result
	 */
	public static CPUCompareResult get(int value)
	{
		return lookup.get(value);
	}

	/** The return code. */
	private final int returnCode;

	/**
	 * Instantiates a new cPU compare result.
	 * 
	 * @param returnCode
	 *            the return code
	 */
	CPUCompareResult(int returnCode)
	{
		this.returnCode = returnCode;
	}

	/**
	 * Gets the return code.
	 * 
	 * @return the return code
	 */
	public int getReturnCode()
	{
		return returnCode;
	}

}
