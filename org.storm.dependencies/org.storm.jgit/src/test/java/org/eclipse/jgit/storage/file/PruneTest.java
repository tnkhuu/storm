package org.eclipse.jgit.storage.file;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import org.eclipse.jgit.junit.LocalDiskRepositoryTestCase;
import org.eclipse.jgit.junit.RepositoryTestCase;
import org.eclipse.jgit.junit.TestRepository;
import org.eclipse.jgit.junit.TestRepository.BranchBuilder;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.storage.file.GC.RepoStatistics;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PruneTest extends LocalDiskRepositoryTestCase
{

	private TestRepository<FileRepository> tr;

	private FileRepository repo;

	private GC gc;

	private RepoStatistics stats;

	@Override
	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		repo = createWorkRepository();
		tr = new TestRepository<FileRepository>((repo));
		gc = new GC(repo);
	}

	@Override
	@After
	public void tearDown() throws Exception
	{
		super.tearDown();
	}

	@Test
	public void testPruneNone() throws Exception
	{
		@SuppressWarnings("rawtypes")
		BranchBuilder bb = tr.branch("refs/heads/master");
		bb.commit().add("A", "A").add("B", "B").create();
		bb.commit().add("A", "A2").add("B", "B2").create();
		new File(repo.getDirectory(), Constants.LOGS + "/refs/heads/master").delete();
		stats = gc.getStatistics();
		assertEquals(8, stats.numberOfLooseObjects);
		gc.setExpireAgeMillis(0);
		fsTick();
		gc.prune(Collections.<ObjectId> emptySet());
		stats = gc.getStatistics();
		assertEquals(8, stats.numberOfLooseObjects);
		fsTick();
		tr.blob("x");
		fsTick();
		stats = gc.getStatistics();
		assertEquals(9, stats.numberOfLooseObjects);
		gc.prune(Collections.<ObjectId> emptySet());

		stats = gc.getStatistics();

		assertEquals(8, stats.numberOfLooseObjects);
	}

	private static void fsTick() throws InterruptedException, IOException
	{
		RepositoryTestCase.fsTick(null);
	}
}
