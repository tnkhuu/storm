/**
 * Logback: the reliable, generic, fast and flexible logging framework.
 * Copyright (C) 1999-2011, QOS.ch. All rights reserved.
 *
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *
 *   or (per the licensee's choosing)
 *
 * under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation.
 */
package ch.qos.logback.classic.pattern;

import static org.fusesource.jansi.Ansi.ansi;
import ch.qos.logback.core.CoreConstants;

/**
 * This abbreviator returns the class name from a fully qualified class name,
 * removing the leading package name.
 * 
 * @author Ceki G&uuml;lc&uuml;
 */
public class ClassNameOnlyAbbreviator implements Abbreviator
{
	public static final int CLASS_LENGTH = 15;

	@Override
	public String abbreviate(String fqClassName)
	{
		// we ignore the fact that the separator character can also be a dollar
		// If the inner class is org.good.AClass#Inner, returning
		// AClass#Inner seems most appropriate
		int lastIndex = fqClassName.lastIndexOf(CoreConstants.DOT);
		if (lastIndex != -1)
		{
			return ansi().fgBright(LevelConverter.getColor()).a(padAndTrancate(fqClassName.substring(lastIndex + 1, fqClassName.length()))).reset().toString();
		}
		else
		{
			return ansi().fgBright(LevelConverter.getColor()).a(padAndTrancate(fqClassName)).reset().toString();
		}
	}

	public String padAndTrancate(String abbreviated)
	{

		int len = abbreviated.length();
		if (len == CLASS_LENGTH)
		{
			return abbreviated;
		}
		else if (len > CLASS_LENGTH)
		{
			return abbreviated.substring(0, CLASS_LENGTH);
		}
		else if (len < CLASS_LENGTH)
		{
			StringBuilder builder = new StringBuilder();
			builder.append(abbreviated);
			for (int i = 0; i < CLASS_LENGTH - len; i++)
			{
				builder.append(" ");
			}

			return builder.toString();
		}
		return abbreviated;
	}
}
