/**
 * Logback: the reliable, generic, fast and flexible logging framework.
 * Copyright (C) 1999-2011, QOS.ch. All rights reserved.
 *
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *
 *   or (per the licensee's choosing)
 *
 * under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation.
 */
package ch.qos.logback.activator;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;

/**
 * A BundleActivator which invokes slf4j loggers
 * 
 * @author Ceki G&uuml;lc&uuml;
 * 
 */
public class Activator implements BundleActivator
{

	private BundleContext m_context = null;

	@Override
	public void start(BundleContext context)
	{
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		StatusPrinter.printInCaseOfErrorsOrWarnings(lc);
		LoggerFactory.getLogger(this.getClass());
		m_context = context;
	}

	@Override
	public void stop(BundleContext context)
	{
		m_context = null;
		LoggerFactory.getLogger(this.getClass());
	}

	public Bundle[] getBundles()
	{
		if (m_context != null)
		{
			return m_context.getBundles();
		}
		return null;
	}
}