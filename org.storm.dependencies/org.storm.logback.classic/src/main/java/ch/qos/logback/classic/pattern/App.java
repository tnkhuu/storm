package ch.qos.logback.classic.pattern;

public class App
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		String word = "Felixx";
		Long l = new Long(word.hashCode() & 0xFFFFFFF);

		System.out.println(Long.toHexString(l));

	}

}
