/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.qos.logback.classic.pattern;

import static org.fusesource.jansi.Ansi.ansi;

import org.fusesource.jansi.Ansi.Color;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * Return the events thread (usually the current thread).
 * 
 * @author Trung Khuu
 */
public class ThreadConverter extends ClassicConverter
{

	/** The Constant EQUALIZER_MASK. */
	private static final int EQUALIZER_MASK = 0xFFFFFF;

	@Override
	public String convert(ILoggingEvent level)
	{
		Color color = Color.DEFAULT;
		if (level.getLevel() == Level.ERROR)
		{
			color = Color.RED;
		}
		else if (level.getLevel() == Level.WARN)
		{
			color = Color.YELLOW;

		}
		else if (level.getLevel() == Level.INFO)
		{
			color = Color.GREEN;
		}
		return ansi().fgBright(color).a(getHashCode(level.getThreadName())).reset().toString();
	}

	/**
	 * Gets a normalized hash code representation of the thread.
	 * 
	 * @param threadName
	 *            the thread name
	 * @return the hash code
	 */
	public String getHashCode(String threadName)
	{
		String val = Long.toHexString(threadName.hashCode() & EQUALIZER_MASK).toUpperCase();
		int add = 6 - val.length();
		while (add-- > 0)
		{
			val = val + "A";
		}

		return val;
	}

}
