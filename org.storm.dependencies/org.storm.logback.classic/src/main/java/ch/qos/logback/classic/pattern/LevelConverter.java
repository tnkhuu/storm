/**
 * Logback: the reliable, generic, fast and flexible logging framework.
 * Copyright (C) 1999-2011, QOS.ch. All rights reserved.
 * 
 * This program and the accompanying materials are dual-licensed under either
 * the terms of the Eclipse Public License v1.0 as published by the Eclipse
 * Foundation
 * 
 * or (per the licensee's choosing)
 * 
 * under the terms of the GNU Lesser General Public License version 2.1 as
 * published by the Free Software Foundation.
 */
package ch.qos.logback.classic.pattern;

import static org.fusesource.jansi.Ansi.ansi;

import org.fusesource.jansi.Ansi.Color;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * Return the event's level.
 * 
 * @author Ceki G&uuml;lc&uuml;
 */
public class LevelConverter extends ClassicConverter
{

	private static final ThreadLocal<Color> traceColor = new ThreadLocal<Color>()
	{
		@Override
		protected Color initialValue()
		{
			return Color.DEFAULT;
		};
	};

	@Override
	public String convert(ILoggingEvent le)
	{
		return decorate(le);
	}

	public String decorate(ILoggingEvent level)
	{
		Color color = Color.DEFAULT;
		if (level.getLevel() == Level.ERROR)
		{
			color = Color.RED;
		}
		else if (level.getLevel() == Level.WARN)
		{
			color = Color.YELLOW;

		}
		else if (level.getLevel() == Level.INFO)
		{
			color = Color.CYAN;
		}
		traceColor.set(color);
		return ansi().fgBright(color).a(level.getLevel()).reset().toString();
	}

	public static Color getColor()
	{
		return traceColor.get();
	}
}
