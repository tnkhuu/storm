package ch.qos.logback.core.property;

import java.io.File;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * @author Ceki G&uuml;c&uuml;
 */
public class FileExistsPropertyDefiner extends PropertyDefinerBase
{

	String path;

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	@Override
	public String getPropertyValue()
	{
		if (path == null)
		{
			return "false";
		}
		File file = new File(path);
		System.out.println(file.getAbsolutePath());
		if (file.exists())
		{
			return "true";
		}
		else
		{
			return "false";
		}
	}
}
