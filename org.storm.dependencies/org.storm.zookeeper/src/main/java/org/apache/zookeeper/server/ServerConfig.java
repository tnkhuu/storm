package org.apache.zookeeper.server;

import java.net.InetSocketAddress;
import java.util.Arrays;

import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig.ConfigException;

public class ServerConfig
{

	protected InetSocketAddress clientPortAddress;
	protected String dataDir;
	protected String dataLogDir;
	protected int tickTime = ZooKeeperServer.DEFAULT_TICK_TIME;
	protected int maxClientCnxns;
	/** defaults to -1 if not set explicitly */
	protected int minSessionTimeout = -1;
	/** defaults to -1 if not set explicitly */
	protected int maxSessionTimeout = -1;

	/**
	 * Parse arguments for server configuration
	 * 
	 * @param args
	 *            clientPort dataDir and optional tickTime
	 * @return ServerConfig configured wrt arguments
	 * @throws IllegalArgumentException
	 *             on invalid usage
	 */
	public void parse(String[] args)
	{
		if (args.length < 2 || args.length > 4)
		{
			throw new IllegalArgumentException("Invalid args:" + Arrays.toString(args));
		}

		clientPortAddress = new InetSocketAddress(Integer.parseInt(args[0]));
		dataDir = args[1];
		dataLogDir = dataDir;
		if (args.length == 3)
		{
			tickTime = Integer.parseInt(args[2]);
		}
		if (args.length == 4)
		{
			maxClientCnxns = Integer.parseInt(args[3]);
		}
	}

	/**
	 * Parse a ZooKeeper configuration file
	 * 
	 * @param path
	 *            the patch of the configuration file
	 * @return ServerConfig configured wrt arguments
	 * @throws ConfigException
	 *             error processing configuration
	 */
	public void parse(String path) throws ConfigException
	{
		QuorumPeerConfig config = new QuorumPeerConfig();
		config.parse(path);

		readFrom(config);
	}

	/**
	 * Read attributes from a QuorumPeerConfig.
	 * 
	 * @param config
	 */
	public void readFrom(QuorumPeerConfig config)
	{
		clientPortAddress = config.getClientPortAddress();
		dataDir = config.getDataDir();
		dataLogDir = config.getDataLogDir();
		tickTime = config.getTickTime();
		maxClientCnxns = config.getMaxClientCnxns();
		minSessionTimeout = config.getMinSessionTimeout();
		maxSessionTimeout = config.getMaxSessionTimeout();
	}

	public InetSocketAddress getClientPortAddress()
	{
		return clientPortAddress;
	}

	public String getDataDir()
	{
		return dataDir;
	}

	public String getDataLogDir()
	{
		return dataLogDir;
	}

	public int getTickTime()
	{
		return tickTime;
	}

	public int getMaxClientCnxns()
	{
		return maxClientCnxns;
	}

	/** minimum session timeout in milliseconds, -1 if unset */
	public int getMinSessionTimeout()
	{
		return minSessionTimeout;
	}

	/** maximum session timeout in milliseconds, -1 if unset */
	public int getMaxSessionTimeout()
	{
		return maxSessionTimeout;
	}
}
