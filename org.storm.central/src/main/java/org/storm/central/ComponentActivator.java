/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.central;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * The Bundle Activator.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator implements BundleActivator
{

	private ArtifactServer server;
	private static final String ARTIFACT_SERVER_PORT = " artifact.server.port";
	private static final String DEFAULT_PORT = "1331";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception
	{
		String port = context.getProperty(ARTIFACT_SERVER_PORT);
		if (port == null)
		{
			port = DEFAULT_PORT;
		}
		server = new ArtifactServer(Integer.valueOf(port));
		server.startArtifactServer();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception
	{
		server.stopServer();

	}

}
