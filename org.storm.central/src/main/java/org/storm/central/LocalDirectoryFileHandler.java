/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.central;

import static io.netty.handler.codec.http.HttpHeaders.isKeepAlive;
import static io.netty.handler.codec.http.HttpHeaders.setContentLength;
import static io.netty.handler.codec.http.HttpHeaders.Names.CACHE_CONTROL;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaders.Names.DATE;
import static io.netty.handler.codec.http.HttpHeaders.Names.EXPIRES;
import static io.netty.handler.codec.http.HttpHeaders.Names.IF_MODIFIED_SINCE;
import static io.netty.handler.codec.http.HttpHeaders.Names.LAST_MODIFIED;
import static io.netty.handler.codec.http.HttpHeaders.Names.LOCATION;
import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.FORBIDDEN;
import static io.netty.handler.codec.http.HttpResponseStatus.FOUND;
import static io.netty.handler.codec.http.HttpResponseStatus.INTERNAL_SERVER_ERROR;
import static io.netty.handler.codec.http.HttpResponseStatus.METHOD_NOT_ALLOWED;
import static io.netty.handler.codec.http.HttpResponseStatus.NOT_FOUND;
import static io.netty.handler.codec.http.HttpResponseStatus.NOT_MODIFIED;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.stream.ChunkedFile;
import io.netty.util.CharsetUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.activation.MimetypesFileTypeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple file server, serving the contents of the <storm.home>/drops directory
 * with a path mapping /artifacts -> /drop i.e. http://host:1331/artifacts ->
 * storm.home/drops
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class LocalDirectoryFileHandler extends ChannelInboundMessageHandlerAdapter<FullHttpRequest>
{

	/** The Constant HTTP_DATE_FORMAT. */
	public static final String HTTP_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";

	/** The Constant HTTP_DATE_GMT_TIMEZONE. */
	public static final String HTTP_DATE_GMT_TIMEZONE = "GMT";

	public static final String ARTIFACTS = "/artifacts";

	public static final String DROPS = "/drop";
	
	public static final String TARGET = "/target";
	
	public static final String WARS = "/webapps";
	
	public static final String REPO = "/repository.xml";

	/** The Constant HTTP_CACHE_SECONDS. */
	public static final int HTTP_CACHE_SECONDS = 60;

	private static Logger s_log = LoggerFactory.getLogger(LocalDirectoryFileHandler.class);

	private static String stormHome;
	
	@SuppressWarnings("unused")
	private static String artifactHome;

	private static String dropHome;

	private static final String DEFAULT_HOME = "/opt/storm";

	public LocalDirectoryFileHandler()
	{
		stormHome = System.getProperty("STORM_HOME", DEFAULT_HOME);
		artifactHome = stormHome + ARTIFACTS;
		dropHome = stormHome + DROPS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.ChannelInboundMessageHandlerAdapter#messageReceived(
	 * io.netty.channel.ChannelHandlerContext, java.lang.Object)
	 */
	@Override
	public void messageReceived(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception
	{

		if (!request.getDecoderResult().isSuccess())
		{
			sendError(ctx, BAD_REQUEST);
			return;
		}

		if (request.getMethod() != GET)
		{
			sendError(ctx, METHOD_NOT_ALLOWED);
			return;
		}

		// final String uri = request.getUri();

		String path = request.getUri();
		if ((path != null) && (path.length() > 0))
		{
			if (path.startsWith(ARTIFACTS))
			{

				path = dropHome + path.replace(ARTIFACTS, "");
				servDirectory(ctx, request, path);
			}
			else if(path.startsWith(WARS))
			{
				path = stormHome + path;
				servDirectory(ctx, request, path);
			}
			else if(path.startsWith(TARGET)) {
				
				path = stormHome + TARGET;
				servDirectory(ctx, request, path);
			}
			else if(path.startsWith("/root")) {
				path = path.replace("/root", "");
				servDirectory(ctx, request, path);
			}
			else
			{
				sendError(ctx, FORBIDDEN);
				return;
			}
		}
		else
		{
			sendError(ctx, NOT_FOUND);
			return;
		}
	}

	private void servDirectory(ChannelHandlerContext ctx, FullHttpRequest request, String path) throws ParseException, IOException
	{
		File file = null;
		String referer = request.headers().get("Referer");
		if(referer != null) {
			try {
				URI refUri = new URI(referer);
				String refPath = refUri.getPath();
				refPath = refPath.replace("/root", "");
				String[] pathSplit = path.split("/");
				if(!refPath.endsWith("/")) {
					refPath = refPath + "/";
				}
				path = refPath +  pathSplit[pathSplit.length - 1];
				file = new File(path);
				
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			file = new File(path);
		}
		if (file.isHidden() || !file.exists())
		{
			sendError(ctx, NOT_FOUND);
			return;
		}

		if (file.isDirectory())
		{
			if (path.endsWith("/"))
			{
				sendListing(ctx, file);
			}
			else
			{
				sendListing(ctx, new File(path));
				//sendRedirect(ctx, path + '/');
			}
			return;
		}

		if (!file.isFile())
		{
			sendError(ctx, FORBIDDEN);
			return;
		}

		// Cache Validation
		String ifModifiedSince = request.headers().get(IF_MODIFIED_SINCE);
		if ((ifModifiedSince != null) && !ifModifiedSince.isEmpty())
		{
			SimpleDateFormat dateFormatter = new SimpleDateFormat(HTTP_DATE_FORMAT, Locale.US);
			Date ifModifiedSinceDate = dateFormatter.parse(ifModifiedSince);

			// Only compare up to the second because the datetime format
			// we
			// send
			// to the client
			// does not have milliseconds
			long ifModifiedSinceDateSeconds = ifModifiedSinceDate.getTime() / 1000;
			long fileLastModifiedSeconds = file.lastModified() / 1000;
			if (ifModifiedSinceDateSeconds == fileLastModifiedSeconds)
			{
				sendNotModified(ctx);
				return;
			}
		}

		RandomAccessFile raf;
		try
		{
			raf = new RandomAccessFile(file, "r");
		}
		catch (FileNotFoundException fnfe)
		{
			sendError(ctx, NOT_FOUND);
			return;
		}
		long fileLength = raf.length();

		HttpResponse response = new DefaultHttpResponse(HTTP_1_1, OK);
		setContentLength(response, fileLength);
		setContentTypeHeader(response, file);
		setDateAndCacheHeaders(response, file);
		if (isKeepAlive(request))
		{
			response.headers().set(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
		}

		// Write the initial line and the header.
		ctx.write(response);

		// Write the content.
		ChannelFuture writeFuture = ctx.write(new ChunkedFile(raf, 0, fileLength, 8192));

		// Decide whether to close the connection or not.
		if (!isKeepAlive(request))
		{
			// Close the connection when the whole content is written
			// out.
			writeFuture.addListener(ChannelFutureListener.CLOSE);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.netty.channel.ChannelHandlerAdapter#exceptionCaught(io.netty.channel
	 * .ChannelHandlerContext, java.lang.Throwable)
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
	{
		s_log.debug(cause.getMessage(), cause);
		if (ctx.channel().isActive())
		{
			sendError(ctx, INTERNAL_SERVER_ERROR);
		}
	}

	/** The Constant INSECURE_URI. */
	private static final Pattern INSECURE_URI = Pattern.compile(".*[<>&\"].*");

	/**
	 * Sanitize uri.
	 * 
	 * @param uri
	 *            the uri
	 * @return the string
	 */
	@SuppressWarnings("unused")
	private static String sanitizeUri(String uri)
	{
		// Decode the path.
		try
		{
			uri = URLDecoder.decode(uri, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			try
			{
				uri = URLDecoder.decode(uri, "ISO-8859-1");
			}
			catch (UnsupportedEncodingException e1)
			{
				throw new Error();
			}
		}

		if (!uri.startsWith("/"))
		{
			return null;
		}

		// Convert file separators.
		uri = uri.replace('/', File.separatorChar);

		if (uri.contains(File.separator + '.') || uri.contains('.' + File.separator) || uri.startsWith(".") || uri.endsWith(".") || INSECURE_URI.matcher(uri).matches())
		{
			return null;
		}

		// Convert to absolute path.
		return System.getProperty("user.dir") + File.separator + uri;
	}

	/** The Constant ALLOWED_FILE_NAME. */
	private static final Pattern ALLOWED_FILE_NAME = Pattern.compile("[A-Za-z0-9][-_A-Za-z0-9\\.]*");

	/**
	 * Send listing.
	 * 
	 * @param ctx
	 *            the ctx
	 * @param dir
	 *            the dir
	 */
	private static void sendListing(ChannelHandlerContext ctx, File dir)
	{
		FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK);
		response.headers().set(CONTENT_TYPE, "text/html; charset=UTF-8");

		StringBuilder buf = new StringBuilder();
		String dirPath = dir.getPath();

		buf.append("<!DOCTYPE html>\r\n");
		buf.append("<html><head><title>");
		buf.append("Listing of: ");
		buf.append(dirPath);
		buf.append("</title></head><body>\r\n");

		buf.append("<h3>Listing of: ");
		buf.append(dirPath);
		buf.append("</h3>\r\n");

		buf.append("<ul>");
		buf.append("<li><a href=\"../\">..</a></li>\r\n");

		for (File f : dir.listFiles())
		{
			if (f.isHidden() || !f.canRead())
			{
				continue;
			}

			String name = f.getName();
			if (!ALLOWED_FILE_NAME.matcher(name).matches())
			{
				continue;
			}

			buf.append("<li><a href=\"");
			buf.append("/root"+ dirPath + "/" + name);
			buf.append("\">");
			buf.append(name);
			buf.append("</a></li>\r\n");
		}

		buf.append("</ul></body></html>\r\n");

		response.data().writeBytes(Unpooled.copiedBuffer(buf, CharsetUtil.UTF_8));
		ctx.write(response).addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * Send redirect.
	 * 
	 * @param ctx
	 *            the ctx
	 * @param newUri
	 *            the new uri
	 */
	@SuppressWarnings("unused")
	private static void sendRedirect(ChannelHandlerContext ctx, String newUri)
	{
		FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, FOUND);
		response.headers().set(LOCATION, newUri);
		ctx.write(response).addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * Send error.
	 * 
	 * @param ctx
	 *            the ctx
	 * @param status
	 *            the status
	 */
	private static void sendError(ChannelHandlerContext ctx, HttpResponseStatus status)
	{
		FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, status, Unpooled.copiedBuffer("Failure: " + status.toString() + "\r\n", CharsetUtil.UTF_8));
		response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
		ctx.write(response).addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * When file timestamp is the same as what the browser is sending up, send a
	 * "304 Not Modified".
	 * 
	 * @param ctx
	 *            Context
	 */
	private static void sendNotModified(ChannelHandlerContext ctx)
	{
		FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, NOT_MODIFIED);
		setDateHeader(response);
		ctx.write(response).addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * Sets the Date header for the HTTP response.
	 * 
	 * @param response
	 *            HTTP response
	 */
	private static void setDateHeader(FullHttpResponse response)
	{
		SimpleDateFormat dateFormatter = new SimpleDateFormat(HTTP_DATE_FORMAT, Locale.US);
		dateFormatter.setTimeZone(TimeZone.getTimeZone(HTTP_DATE_GMT_TIMEZONE));

		Calendar time = new GregorianCalendar();
		response.headers().set(DATE, dateFormatter.format(time.getTime()));
	}

	/**
	 * Sets the Date and Cache headers for the HTTP Response.
	 * 
	 * @param response
	 *            HTTP response
	 * @param fileToCache
	 *            file to extract content type
	 */
	private static void setDateAndCacheHeaders(HttpResponse response, File fileToCache)
	{
		SimpleDateFormat dateFormatter = new SimpleDateFormat(HTTP_DATE_FORMAT, Locale.US);
		dateFormatter.setTimeZone(TimeZone.getTimeZone(HTTP_DATE_GMT_TIMEZONE));

		Calendar time = new GregorianCalendar();
		response.headers().set(DATE, dateFormatter.format(time.getTime()));

		time.add(Calendar.SECOND, HTTP_CACHE_SECONDS);
		response.headers().set(EXPIRES, dateFormatter.format(time.getTime()));
		response.headers().set(CACHE_CONTROL, "private, max-age=" + HTTP_CACHE_SECONDS);
		response.headers().set(LAST_MODIFIED, dateFormatter.format(new Date(fileToCache.lastModified())));
	}

	/**
	 * Sets the content type header for the HTTP Response.
	 * 
	 * @param response
	 *            HTTP response
	 * @param file
	 *            file to extract content type
	 */
	private static void setContentTypeHeader(HttpResponse response, File file)
	{
		MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
		response.headers().set(CONTENT_TYPE, mimeTypesMap.getContentType(file.getPath()));
	}

}
