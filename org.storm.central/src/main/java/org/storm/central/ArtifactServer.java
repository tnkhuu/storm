/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.storm.central;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple file server, serving the contents of the <storm.home>/drops directory
 * with a path mapping /artifacts -> /drop i.e. http://host:1331/artifacts ->
 * storm.home/drops
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ArtifactServer
{
	private int port;
	private ServerBootstrap server;
	private ChannelFuture future;
	private Logger s_log = LoggerFactory.getLogger(ArtifactServer.class);

	public ArtifactServer(int port)
	{
		this.port = port;
	}

	/**
	 * Start artifact server.
	 */
	public void startArtifactServer()
	{
		server = new ServerBootstrap();
		try
		{
			server.group(new NioEventLoopGroup(), new NioEventLoopGroup()).channel(NioServerSocketChannel.class).childHandler(new ArtifactServerInitializer());
			future = server.bind(port).sync();
			s_log.info("Started Artifact Server @0.0.0.0:{} ", port);
		}
		catch (InterruptedException e)
		{
			s_log.debug(e.getMessage(), e);
		}
	}

	public void stopServer()
	{
		s_log.info("Shutting down the Artifact Server @0.0.0.0:{} ", port);
		if (future != null)
		{
			future.channel().close();
		}
		if (server != null)
		{
			server.shutdown();
		}
	}

	public static void main(String[] args)
	{
		new ArtifactServer(1331).startArtifactServer();
	}
}
