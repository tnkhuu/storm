/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lvm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import junit.framework.Assert;

import org.apache.commons.compress.archivers.ArchiveException;
import org.storm.api.Storm;
import org.storm.api.services.LogicalVolumeService;
import org.storm.nexus.api.Role;
import org.storm.tools.compression.Extract;

public class TestLogicalVolumnServiceImpl
{

	LogicalVolumeService service = new LogicalVolumnServiceImpl();
	

	
	
	//@Test
	public void testExistsCreateDestroy() {
		
		String path =	service.createLogicalVolumn(TestLogicalVolumnServiceImpl.class.getSimpleName(), 50, Storm.LVM_VG);
		Assert.assertEquals(LogicalVolumeService.DEFAULT_MNT_DIR + "/" + TestLogicalVolumnServiceImpl.class.getSimpleName(), path);
		boolean exists = service.logicalVolumnExists(TestLogicalVolumnServiceImpl.class.getSimpleName(), Storm.LVM_VG);
		Assert.assertTrue(exists);
		
		service.destroyLogicalVolumn(TestLogicalVolumnServiceImpl.class.getSimpleName(), Storm.LVM_VG);
		
		exists = service.logicalVolumnExists(TestLogicalVolumnServiceImpl.class.getSimpleName(), Storm.LVM_VG);
		Assert.assertFalse(exists);
	}
	

	//@Test
	public void testSnapshotExistsCreateDestroy() {
		
		String path =	service.createLogicalVolumn("snapshot", 50, Storm.LVM_VG);
		System.out.println(path);
		String snapPath =	service.createSnapshotVolumn(TestLogicalVolumnServiceImpl.class.getSimpleName(), Role.DNS.name(), 50, Storm.LVM_VG, path);
		Assert.assertEquals(LogicalVolumeService.DEFAULT_MNT_DIR + "/" + TestLogicalVolumnServiceImpl.class.getSimpleName(), snapPath);
		
		
		service.destroyLogicalVolumn(TestLogicalVolumnServiceImpl.class.getSimpleName(), Storm.LVM_VG);
		service.destroyLogicalVolumn("snapshot", Storm.LVM_VG);
		boolean exists = service.logicalVolumnExists(TestLogicalVolumnServiceImpl.class.getSimpleName(), Storm.LVM_VG);
		Assert.assertFalse(exists);
	}
	
//	@Test
	public void testLxcImage() throws FileNotFoundException, IOException, ArchiveException {
		String path =	service.createLogicalVolumn("base", 1000, Storm.LVM_VG);
		File rootfs = new File(path + "/rootfs");
		if(!rootfs.exists()) {
			rootfs.mkdirs();
		}
		Extract.targz(new File("/home/tkhuu/.storm/dist/storm-cloud-1.0.0-lxc-rootfs.tar.gz"),rootfs);
		
		
	}
	
	//@Test
	public void testLxcSnapshotImage() throws FileNotFoundException, IOException, ArchiveException {
	//	String path =	service.createLxcBackedLogicalVolumn("ubuntu-storm", "test", "/home/tkhuu/.storm/dist/storm-cloud-1.0.0-lxc-rootfs.tar.gz", "/home/tkhuu/.ssh/id_rsa.pub", "DNS", "8.8.8.8", 1500, "lxc");
		
		
		
	}
}
