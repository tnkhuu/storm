/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.lvm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.exception.LogicalVolumeException;
import org.storm.api.services.LogicalVolumeService;
import org.storm.tools.system.StormPlatform;

/**
 * The Logical Volunm service.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component
@Service({LogicalVolumeService.class})
public class LogicalVolumnServiceImpl implements LogicalVolumeService
{
	private static final Logger	s_log	= LoggerFactory.getLogger(LogicalVolumnServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.LogicalVolumeService#isReady()
	 */
	@Override
	public boolean isReady()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.LogicalVolumeService#destroyLogicalVolumn(java
	 * .lang.String)
	 */
	@Override
	public boolean destroyLogicalVolumn(String name, String volumnGroup) throws LogicalVolumeException
	{
		boolean success = false;
		try
		{
			String result = execute("sudo lvremove -f " + volumnGroup + "/" + name);

			execute("sudo rm -rf " + DEFAULT_MNT_DIR + "/" + name);
			s_log.debug(result);
			success = true;
		}
		catch (IOException e)
		{
			throw new LogicalVolumeException(e.getMessage(), e);
		}
		return success;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.LogicalVolumeService#destroyVolumnGroup(java.lang
	 * .String)
	 */
	@Override
	public boolean destroyVolumnGroup(String name) throws LogicalVolumeException
	{
		boolean success = false;
		try
		{
			String result = execute("sudo vgremove -f " + name);
			s_log.debug(result);
			success = true;
		}
		catch (IOException e)
		{
			throw new LogicalVolumeException(e.getMessage(), e);
		}
		return success;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.LogicalVolumeService#createVolumnGroup(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public String createVolumnGroup(String name, String... devices) throws LogicalVolumeException
	{
		String volPath = null;
		try
		{
			StringBuilder b = new StringBuilder();
			for (String d : devices)
			{
				b.append(d);
				b.append(" ");
			}
			String result = execute("sudo vgcreate " + name + " " + b.toString());
			s_log.debug(result);
			volPath = "/dev/" + name;
		}
		catch (IOException e)
		{
			throw new LogicalVolumeException(e.getMessage(), e);
		}
		return volPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.LogicalVolumeService#createLogicalVolumn(java.
	 * lang.String, int, java.lang.String)
	 */
	@Override
	public String createLogicalVolumn(String name, int sizeInMB, String volumnGroup) throws LogicalVolumeException
	{
		String volPath = null;
		try
		{

			String result = execute("sudo lvcreate -L " + sizeInMB + "M -n " + name + " " + volumnGroup);
			s_log.debug(result);
			volPath = "/dev/" + volumnGroup + "/" + name;
			execute("sudo mkdir -p " + DEFAULT_MNT_DIR + "/" + name);
			execute("sudo mount -t ext4 " + volPath + " " + DEFAULT_MNT_DIR + "/" + name);
			volPath = DEFAULT_MNT_DIR + "/" + name;
		}
		catch (IOException e)
		{
			throw new LogicalVolumeException(e.getMessage(), e);
		}
		return volPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.api.services.LogicalVolumeService#createSnapshotVolumn(java
	 * .lang.String, int, java.lang.String)
	 */
	@Override
	public String createSnapshotVolumn(String newLogicalSnapshotName, String role, int sizeInMB, String snapshotTarget, String volumnGroup) throws LogicalVolumeException
	{
		String volPath = null;
		String name = sanitize(newLogicalSnapshotName);
		volPath = "/dev/" + volumnGroup + "/" + name;
		if(new File(volPath).exists()) {
			return volPath;
		}
		try
		{
			
			execute("sudo lvcreate -s -L " + sizeInMB + "M -n " + name + " " + snapshotTarget);
			execute("sudo mkdir -p " + DEFAULT_MNT_DIR + "/" + name);
			execute("sudo mount -t ext4 " + volPath + " " + DEFAULT_MNT_DIR + "/" + name);
			execute("sudo touch " + DEFAULT_MNT_DIR + "/" + name + "/role");
			execute("sudo chmod 777 " + DEFAULT_MNT_DIR + "/" + name + "/role");
			execute("sudo chmod 777 " + DEFAULT_MNT_DIR + "/" + name + "/etc/rc.local");
			File snapshotRole = new File(DEFAULT_MNT_DIR + "/" + name + "/role");
			FileUtils.writeStringToFile(snapshotRole, role);
		}
		catch (IOException e)
		{
			throw new LogicalVolumeException(e.getMessage(), e);
		}
		return volPath;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.LogicalVolumeService#createLxcBaseLogicalVolumn(java.lang.String, java.lang.String, java.lang.String, int)
	 */
	@Override
	public String createLxcBaseLogicalVolumn(String name, String role, String dns, int sizeInMB)
	{
		name = sanitize(name);
		try
		{
			String volPath = "/dev/lxc/" + name;
			String[] commandStr = new String[] { "sudo", "lxc-create", "-n", name, "-t", "ubuntu-storm", "-B","lvm", "--lvname",name,"--fstype","ext4","--fssize",sizeInMB + "M", "--", "-T",
					StormPlatform.INSTANCE.getStormImage(), "--auth-key", StormPlatform.INSTANCE.getSSHPublicKey(), "--role", role, "--dns", dns, ">",
					"/dev/null" };

			execute(commandStr);


			execute("sudo mkdir -p " + DEFAULT_MNT_DIR + "/" + name);
			execute("sudo mount -t ext4 " + volPath + " " + DEFAULT_MNT_DIR + "/" + name);
			execute("sudo chmod 777 " + DEFAULT_MNT_DIR + "/" + name + "/etc/");
		}
		catch (IOException e)
		{
			throw new LogicalVolumeException(e.getMessage(), e);
		}
		return LogicalVolumeService.BASE_LVM_DIR + name;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.LogicalVolumeService#setBaseLxcDNS(java.lang.String)
	 */
	public void setBaseLxcDNS(String dns)
	{
		try
		{
			FileUtils.write(new File(DEFAULT_MNT_DIR + "/" + BASE_LVM_IMAGE_NAME + "/etc/resolv.conf"), "nameserver " + dns);
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.LogicalVolumeService#baseLxcLogicalVolumnExists()
	 */
	public boolean baseLxcLogicalVolumnExists()
	{
		return new File(LogicalVolumeService.BASE_LXC_LVM_DIR).exists();
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.LogicalVolumeService#logicalVolumnExists(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean logicalVolumnExists(String name, String volumnGroup)
	{
		boolean exists = false;
		if (new File("/dev/" + volumnGroup + "/" + name).exists())
		{
			exists = true;
		}
		return exists;
	}

	/**
	 * Execute.
	 * 
	 * @param command
	 *            the command
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private String execute(String command) throws IOException
	{
		String result = null;

		Process p = Runtime.getRuntime().exec(command);
		result = getOutput(p.getInputStream());

		return result;
	}
	
	/**
	 * Execute.
	 * 
	 * @param command
	 *            the command
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private String execute(String[] command) throws IOException
	{
		String result = null;

		Process p = Runtime.getRuntime().exec(command);
		result = getOutput(p.getInputStream());

		return result;
	}

	/**
	 * Sanitize.
	 * 
	 * @param domain
	 *            the domain
	 * @return the string
	 */
	private String sanitize(String domain)
	{
		return domain;
	}

	/**
	 * Gets the output.
	 * 
	 * @param in
	 *            the in
	 * @return the output
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private String getOutput(InputStream in) throws IOException
	{
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader bi = new BufferedReader(reader);
		StringBuilder builder = new StringBuilder();
		for (String s = bi.readLine(); s != null; s = bi.readLine())
		{
			builder.append(s + "\n");
		}
		return builder.toString();

	}
}
