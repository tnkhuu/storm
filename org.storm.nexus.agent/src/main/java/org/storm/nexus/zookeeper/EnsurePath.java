package org.storm.nexus.zookeeper;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.zookeeper.ZooKeeper;

/**
 * <p>
 * Utility to ensure that a particular path is created.
 * </p>
 * 
 * <p>
 * The first time it is used, a synchronized call to
 * {@link ZKPaths#mkdirs(ZooKeeper, String)} is made to ensure that the entire
 * path has been created (with an empty byte array if needed). Subsequent calls
 * with the instance are un-synchronized NOPs.
 * </p>
 * 
 * <p>
 * Usage:<br/>
 * <code><pre>
 *         EnsurePath       ensurePath = new EnsurePath(aFullPathToEnsure);
 *         ...
 *         String           nodePath = aFullPathToEnsure + "/foo";
 *         ensurePath.ensure(zk);   // first time syncs and creates if needed
 *         zk.create(nodePath, ...);
 *         ...
 *         ensurePath.ensure(zk);   // subsequent times are NOPs
 *         zk.create(nodePath, ...);
 *     </pre></code>
 * </p>
 */
public class EnsurePath
{
	private final String path;
	private final boolean makeLastNode;
	private final boolean lastNodeEphemerald;
	private final byte[] data;
	private final AtomicReference<Helper> helper;

	private static final Helper doNothingHelper = new Helper()
	{
		@Override
		public void ensure(CuratorZookeeperClient client, String path, final boolean makeLastNode, boolean lastNodeEphemerald, byte[] data) throws Exception
		{
			// NOP
		}
	};

	private interface Helper
	{
		public void ensure(CuratorZookeeperClient client, String path, final boolean makeLastNode, boolean lastNodeEphemerald, byte[] data) throws Exception;
	}

	/**
	 * @param path
	 *            the full path to ensure
	 */
	public EnsurePath(String path)
	{
		this(path, null, true, false);
	}
	
	/**
	 * @param path
	 *            the full path to ensure
	 */
	public EnsurePath(String path, boolean lastNodeEphemerald, byte[] data)
	{
		this(path, null, true, lastNodeEphemerald);
	}

	/**
	 * First time, synchronizes and makes sure all nodes in the path are
	 * created. Subsequent calls with this instance are NOPs.
	 * 
	 * @param client
	 *            ZK client
	 * @throws Exception
	 *             ZK errors
	 */
	public void ensure(CuratorZookeeperClient client) throws Exception
	{
		Helper localHelper = helper.get();
		localHelper.ensure(client, path, makeLastNode, lastNodeEphemerald, data);
	}

	/**
	 * Returns a view of this EnsurePath instance that does not make the last
	 * node. i.e. if the path is "/a/b/c" only "/a/b" will be ensured
	 * 
	 * @return view
	 */
	public EnsurePath excludingLast()
	{
		return new EnsurePath(path, helper,false, false);
	}

	private EnsurePath(String path, AtomicReference<Helper> helper, boolean makeLastNode, boolean lastNodeEphemerald)
	{
		this(path,helper,makeLastNode,lastNodeEphemerald, new byte[0]);
	}
	
	private EnsurePath(String path, AtomicReference<Helper> helper, boolean makeLastNode, boolean lastNodeEphemerald, byte[] data)
	{
		this.path = path;
		this.makeLastNode = makeLastNode;
		this.data = data;
		this.lastNodeEphemerald = lastNodeEphemerald;
		this.helper = (helper != null) ? helper : new AtomicReference<Helper>(new InitialHelper());
	}

	private class InitialHelper implements Helper
	{
		private boolean isSet = false; // guarded by synchronization

		@Override
		public synchronized void ensure(final CuratorZookeeperClient client, final String path, final boolean makeLastNode, final boolean lastNodeEphemerald, final byte[] data) throws Exception
		{
			if (!isSet)
			{
				RetryLoop.callWithRetry(client, new Callable<Object>()
				{
					@Override
					public Object call() throws Exception
					{
						ZKPaths.mkdirs(client.getZooKeeper(), path, makeLastNode,lastNodeEphemerald, data);
						helper.set(doNothingHelper);
						isSet = true;
						return null;
					}
				});
			}
		}
	}
}
