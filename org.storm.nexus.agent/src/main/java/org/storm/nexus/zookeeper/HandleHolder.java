/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.zookeeper;

import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

/**
 * The Class HandleHolder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class HandleHolder
{
	private final ZookeeperClientFactory zookeeperFactory;
	private final Watcher watcher;
	private final EnsembleProvider ensembleProvider;
	private final int sessionTimeout;
	private final boolean canBeReadOnly;

	private volatile Helper helper;

	/**
	 * The Interface Helper.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private interface Helper
	{
		
		/**
		 * Gets the zoo keeper.
		 * 
		 * @return the zoo keeper
		 * @throws Exception
		 *             the exception
		 */
		ZooKeeper getZooKeeper() throws Exception;

		/**
		 * Gets the connection string.
		 * 
		 * @return the connection string
		 */
		String getConnectionString();
	}

	/**
	 * Instantiates a new handle holder.
	 * 
	 * @param zookeeperFactory
	 *            the zookeeper factory
	 * @param watcher
	 *            the watcher
	 * @param ensembleProvider
	 *            the ensemble provider
	 * @param sessionTimeout
	 *            the session timeout
	 * @param canBeReadOnly
	 *            the can be read only
	 */
	HandleHolder(ZookeeperClientFactory zookeeperFactory, Watcher watcher, EnsembleProvider ensembleProvider, int sessionTimeout, boolean canBeReadOnly)
	{
		this.zookeeperFactory = zookeeperFactory;
		this.watcher = watcher;
		this.ensembleProvider = ensembleProvider;
		this.sessionTimeout = sessionTimeout;
		this.canBeReadOnly = canBeReadOnly;
	}

	/**
	 * Gets the zoo keeper.
	 * 
	 * @return the zoo keeper
	 * @throws Exception
	 *             the exception
	 */
	ZooKeeper getZooKeeper() throws Exception
	{
		return helper.getZooKeeper();
	}

	/**
	 * Gets the connection string.
	 * 
	 * @return the connection string
	 */
	String getConnectionString()
	{
		return (helper != null) ? helper.getConnectionString() : null;
	}

	/**
	 * Checks for new connection string.
	 * 
	 * @return true, if successful
	 */
	boolean hasNewConnectionString()
	{
		String helperConnectionString = (helper != null) ? helper.getConnectionString() : null;
		return (helperConnectionString != null) && !ensembleProvider.getConnectionString().equals(helperConnectionString);
	}

	/**
	 * Close and clear.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	void closeAndClear() throws Exception
	{
		internalClose();
		helper = null;
	}

	/**
	 * Close and reset.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	void closeAndReset() throws Exception
	{
		internalClose();

		// first helper is synchronized when getZooKeeper is called. Subsequent
		// calls
		// are not synchronized.
		helper = new Helper()
		{
			private volatile ZooKeeper zooKeeperHandle = null;
			private volatile String connectionString = null;

			@Override
			public ZooKeeper getZooKeeper() throws Exception
			{
				synchronized (this)
				{
					if (zooKeeperHandle == null)
					{
						connectionString = ensembleProvider.getConnectionString();
						zooKeeperHandle = zookeeperFactory.newZooKeeperClient(connectionString, sessionTimeout, watcher, canBeReadOnly);
					}

					helper = new Helper()
					{
						@Override
						public ZooKeeper getZooKeeper() throws Exception
						{
							return zooKeeperHandle;
						}

						@Override
						public String getConnectionString()
						{
							return connectionString;
						}
					};

					return zooKeeperHandle;
				}
			}

			@Override
			public String getConnectionString()
			{
				return connectionString;
			}
		};
	}

	/**
	 * Internal close.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void internalClose() throws Exception
	{
		try
		{
			ZooKeeper zooKeeper = (helper != null) ? helper.getZooKeeper() : null;
			if (zooKeeper != null)
			{
				zooKeeper.close();
			}
		}
		catch (InterruptedException dummy)
		{
			Thread.currentThread().interrupt();
		}
	}
}