/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.zookeeper;

import java.io.Closeable;
import java.io.IOException;

import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;


/**
 * The Interface EnsembleProvider.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface EnsembleProvider extends Closeable
{
	
	/**
	 * Curator will call this method when {@link CuratorZookeeperClient#start()}
	 * is called.
	 * 
	 * @throws Exception
	 *             errors
	 */
	public void start() throws Exception;

	/**
	 * Return the current connection string to use. Curator will call this each
	 * time it needs to create a ZooKeeper instance
	 * 
	 * @return connection string (per
	 *         {@link ZooKeeper#ZooKeeper(String, int, Watcher)} etc.)
	 */
	public String getConnectionString();

	/**
	 * Curator will call this method when {@link CuratorZookeeperClient#close()}
	 * is called.
	 * 
	 * @throws IOException
	 *             errors
	 */
	@Override
	public void close() throws IOException;
}