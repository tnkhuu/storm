/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.zookeeper;

import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

/**
 * A factory for creating ZookeeperClient objects.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface ZookeeperClientFactory
{
	
	/**
	 * Allocate a new ZooKeeper instance for a client on the ensemble.
	 * 
	 * @param connectString
	 *            the connection string
	 * @param sessionTimeout
	 *            session timeout in milliseconds
	 * @param watcher
	 *            optional watcher
	 * @param canBeReadOnly
	 *            if true, allow ZooKeeper client to enter read only mode in
	 *            case of a network partition. See
	 * @return the instance
	 * @throws Exception
	 *             errors
	 *             {@link ZooKeeper#ZooKeeper(String, int, Watcher, long, byte[], boolean)}
	 *             for details
	 */
	public ZooKeeper newZooKeeperClient(String connectString, int sessionTimeout, Watcher watcher, boolean canBeReadOnly) throws Exception;
}