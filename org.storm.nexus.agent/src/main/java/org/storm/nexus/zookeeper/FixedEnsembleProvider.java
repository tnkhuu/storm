/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.zookeeper;

import java.io.IOException;


/**
 * The Class FixedEnsembleProvider.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class FixedEnsembleProvider implements EnsembleProvider
{
	private final String connectionString;

	/**
	 * The connection string to use.
	 * 
	 * @param connectionString
	 *            connection string
	 */
	public FixedEnsembleProvider(String connectionString)
	{
		this.connectionString = connectionString;
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.zookeeper.EnsembleProvider#start()
	 */
	@Override
	public void start() throws Exception
	{
		// NOP
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.zookeeper.EnsembleProvider#close()
	 */
	@Override
	public void close() throws IOException
	{
		// NOP
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.zookeeper.EnsembleProvider#getConnectionString()
	 */
	@Override
	public String getConnectionString()
	{
		return connectionString;
	}
}