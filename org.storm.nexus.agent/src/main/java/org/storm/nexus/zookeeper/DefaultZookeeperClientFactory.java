/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.zookeeper;

import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

/**
 * A factory for creating DefaultZookeeperClient objects.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class DefaultZookeeperClientFactory implements ZookeeperClientFactory
{
	
	/* (non-Javadoc)
	 * @see org.storm.nexus.zookeeper.ZookeeperClientFactory#newZooKeeperClient(java.lang.String, int, org.apache.zookeeper.Watcher, boolean)
	 */
	@Override
	public ZooKeeper newZooKeeperClient(String connectString, int sessionTimeout, Watcher watcher, boolean canBeReadOnly) throws Exception
	{
		return new ZooKeeper(connectString, sessionTimeout, watcher, canBeReadOnly);
	}
}