/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.netty;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jboss.netty.logging.InternalLogger;
import org.jboss.netty.logging.InternalLoggerFactory;
import org.msgpack.MessagePack;
import org.storm.nexus.agent.service.ClientTransport;
import org.storm.nexus.agent.service.config.StreamClientConfig;
import org.storm.nexus.agent.session.Session;

/**
 * The Class PooledStreamClientTransport.
 * 
 * @param <Channel>
 *            the generic type
 * @param <PendingBuffer>
 *            the generic type
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class PooledStreamClientTransport<Channel, PendingBuffer extends OutputStream> implements ClientTransport
{
	private static final InternalLogger LOG = InternalLoggerFactory.getInstance(PooledStreamClientTransport.class);

	private final Object lock = new Object();
	private final List<Channel> pool = new ArrayList<Channel>();
	private final List<Channel> errorChannelPool = new ArrayList<Channel>();
	protected final Session session;
	protected final StreamClientConfig config;
	protected final MessagePack messagePack;
	private int reconnectionLimit;
	private int connecting = 0;
	/**
	 * Instantiates a new pooled stream client transport.
	 * 
	 * @param config
	 *            the config
	 * @param session
	 *            the session
	 */
	public PooledStreamClientTransport(StreamClientConfig config, Session session)
	{
		this.session = session;
		this.config = config;
		this.reconnectionLimit = config.getReconnectionLimit();
		this.messagePack = session.getEventLoop().getMessagePack();
	}

	/**
	 * Gets the session.
	 * 
	 * @return the session
	 */
	protected Session getSession()
	{
		return session;
	}

	/**
	 * Gets the config.
	 * 
	 * @return the config
	 */
	protected StreamClientConfig getConfig()
	{
		return config;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.ClientTransport#sendMessage(java.lang.Object
	 * )
	 */
	@Override
	public void sendMessage(Object msg)
	{
		synchronized (lock)
		{
			if (connecting == -1)
			{
				return;
			} // already closed
			if (pool.isEmpty())
			{
				if (connecting == 0)
				{
					connecting++;
					startConnection();
				}
				if (pool.isEmpty())
				{ // may be already connected
					try
					{
						messagePack.write(getPendingBuffer(), msg);
					}
					catch (IOException e)
					{
						// FIXME
					}
					return;
				}
			}
			// FIXME pseudo connection load balancing
			Channel c = pool.get(0);
			sendMessageChannel(c, msg);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.agent.service.ClientTransport#close()
	 */
	@Override
	public void close()
	{
		synchronized (lock)
		{
			LOG.info("Close all channels");
			if (pendingBuffer != null)
			{
				closePendingBuffer(pendingBuffer);
				pendingBuffer = null;
			}
			connecting = -1;
			for (Channel c : pool)
			{
				closeChannel(c);
			}
			for (Channel c : errorChannelPool)
			{
				closeChannel(c);
			}
			pool.clear();
			errorChannelPool.clear();
		}
	}

	/**
	 * On connected.
	 * 
	 * @param c
	 *            the c
	 */
	public void onConnected(Channel c)
	{
		synchronized (lock)
		{
			if (connecting == -1)
			{
				closeChannel(c);
				return;
			} // already closed
			LOG.debug("Success to connect new channel " + c);
			pool.add(c);
			connecting = 0;
			if (pendingBuffer != null)
			{
				flushPendingBuffer(pendingBuffer, c);
			}
		}
	}

	/**
	 * On connect failed.
	 * 
	 * @param c
	 *            the c
	 * @param cause
	 *            the cause
	 */
	public void onConnectFailed(Channel c, Throwable cause)
	{
		synchronized (lock)
		{
			if (connecting == -1)
			{
				return;
			} // already closed
			if (connecting < reconnectionLimit)
			{
				LOG.info(String.format("Reconnect %s(retry:%s)", c, connecting + 1), cause);
				connecting++;
				if (pool.remove(c))
				{// remove error channel
					errorChannelPool.add(c);
				}
				startConnection();
			}
			else
			{
				LOG.error(String.format("Fail to connect %s(tried %s times)", c, reconnectionLimit), cause);
				connecting = 0;
				if (pendingBuffer != null)
				{
					resetPendingBuffer(pendingBuffer);
				}
				session.transportConnectFailed();
			}
		}
	}

	/**
	 * On closed.
	 * 
	 * @param c
	 *            the c
	 */
	public void onClosed(Channel c)
	{
		synchronized (lock)
		{
			if (connecting == -1)
			{
				return;
			} // already closed
			LOG.info(String.format("Close channel %s", c));
			pool.remove(c);
			errorChannelPool.remove(c);
		}
	}

	private PendingBuffer pendingBuffer = null;

	/**
	 * Gets the pending buffer.
	 * 
	 * @return the pending buffer
	 */
	protected PendingBuffer getPendingBuffer()
	{
		if (pendingBuffer == null)
		{
			pendingBuffer = newPendingBuffer();
		}
		return pendingBuffer;
	}

	/**
	 * New pending buffer.
	 * 
	 * @return the pending buffer
	 */
	protected abstract PendingBuffer newPendingBuffer();

	/**
	 * Reset pending buffer.
	 * 
	 * @param b
	 *            the b
	 */
	protected abstract void resetPendingBuffer(PendingBuffer b);

	/**
	 * Flush pending buffer.
	 * 
	 * @param b
	 *            the b
	 * @param c
	 *            the c
	 */
	protected abstract void flushPendingBuffer(PendingBuffer b, Channel c);

	/**
	 * Close pending buffer.
	 * 
	 * @param b
	 *            the b
	 */
	protected abstract void closePendingBuffer(PendingBuffer b);

	/**
	 * Start connection.
	 */
	protected abstract void startConnection();

	/**
	 * Send message channel.
	 * 
	 * @param c
	 *            the c
	 * @param msg
	 *            the msg
	 */
	protected abstract void sendMessageChannel(Channel c, Object msg);

	/**
	 * Close channel.
	 * 
	 * @param c
	 *            the c
	 */
	protected abstract void closeChannel(Channel c);
}
