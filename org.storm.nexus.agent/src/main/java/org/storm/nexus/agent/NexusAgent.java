/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.storm.nexus.agent;

import static org.apache.zookeeper.ZooDefs.Ids.OPEN_ACL_UNSAFE;

import java.util.Collections;
import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.Stat;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.wiring.BundleWiring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.factories.FilterFactory;
import org.storm.api.services.DistributedService;
import org.storm.api.services.ServiceGateway;
import org.storm.nexus.agent.service.ServiceGatewayImpl;
import org.storm.nexus.api.Agent;
import org.storm.nexus.api.AgentCallback;
import org.storm.nexus.api.Endpoints;
import org.storm.nexus.api.Instance;
import org.storm.nexus.api.Message;
import org.storm.nexus.api.Role;
import org.storm.nexus.api.Services;
import org.storm.nexus.zookeeper.CuratorZookeeperClient;
import org.storm.nexus.zookeeper.DefaultZookeeperClientFactory;
import org.storm.nexus.zookeeper.DynamicEnsembleProvider;
import org.storm.nexus.zookeeper.EnsurePath;
import org.storm.nexus.zookeeper.ZKPaths;

import com.netflix.curator.retry.ExponentialBackoffRetry;

/**
 * A agent is what keeps a container connected,wired up and synchronized on the
 * storm cloud. It is the hook into the zookeeper ensemblem, and always exists
 * across every node.
 * 
 * It provides a gateway to access services within the cloud and is responsible
 * for managing the state and service pipeline of the node that its currently
 * running on. It registers against the nexus ensemble via a direct DNS lookup
 * and publishes all known services the node was configured to provide.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("rawtypes")
public class NexusAgent implements Agent, ServiceListener, Watcher
{
	private static final Logger s_log = LoggerFactory.getLogger(NexusAgent.class);
	private ServiceGateway serviceGateway;
	private BundleContext context;
	private Instance agentInstance;
	private RPCManager rpcManager;
	private CuratorZookeeperClient client;
	private volatile boolean isRunning = false;
	private static final int SESSION_TIMEOUT = 300000;
	private static final int CONN_TIMEOUT = 30000;
	private Role role;

	/**
	 * Instantiates a new nexus agent in slave mode.
	 * 
	 * @param context
	 *            the context
	 * @param instance
	 *            the instance
     * @param role
	 *           the role of the current node
     */
	public NexusAgent(final BundleContext context, Instance instance, Role role)
	{

		this.context = context;
		this.agentInstance = instance;
		this.role = role;
		this.rpcManager = new RPCManager(context, agentInstance.getRpcPort());

	}

	/**
	 * Initialize the agent and bind this node into the zookeeper ensemble, then
	 * publish and track all services this container provides.
	 * 
	 * @param ensemble
	 */
	public void init(String ensemble)
	{

		try
		{
			bind((Watcher) this, ensemble).join(AGENT_GROUPS, agentInstance).track(context);
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}

		setRunning(true);
	}

	/**
	 * Set the service gateway for this agent.
	 * 
	 * @param service
	 */
	protected void setServiceGateway(ServiceGateway service)
	{
		this.serviceGateway = service;
	}

	/**
	 * Set the running state of this agent.
	 * 
	 * @param running
	 */
	private void setRunning(boolean running)
	{
		this.isRunning = running;
	}

	/**
	 * Bind the agent to a zoo keeper backed quorum node.
	 * 
	 * TODO: Make the DNS return a round robin set of ip's that points to the
	 * active nodes which are participation as a quorum on the ensemble.
	 * gateway(services.stormclowd.com.) ?
	 * 
	 * @param service
	 *            the naming service
	 * @param serviceGateway
	 *            the service gateway
	 * @return a self reference
	 */
	private NexusAgent bind(Watcher serviceGateway, String ensembleDomain)
	{
		boolean started = false;
		try
		{

			client = new CuratorZookeeperClient(new DefaultZookeeperClientFactory(), new DynamicEnsembleProvider(Endpoints.ENSEMBLE.getDomain()), SESSION_TIMEOUT, CONN_TIMEOUT,
					serviceGateway, new ExponentialBackoffRetry(3000, 10), true);
			client.start();
			if (client.blockUntilConnectedOrTimedOut())
			{
				started = true;
			}
			if (!started)
			{
				throw new RuntimeException("Could not connect to ensemble at " + ensembleDomain);
			}

		}
		catch (Exception e)
		{
			client.close();
			throw new RuntimeException(e);
		}
		return this;
	}

	/**
	 * Join the ensemble.
	 * 
	 * @param group
	 *            the group
	 * @param instance
	 *            the instance
	 * @return the nexus agent
	 * @throws Exception
	 */
	private NexusAgent join(String group, Instance instance) throws Exception
	{
		boolean error = false;
		try
		{
			String name;
			if (role == Role.INITIAL)
			{
				name = "localhost";
			}
			else
			{
				name = role.name().toLowerCase();
			}
			String groupAndRole = group + "/" + name;
			Stat stat = client.getZooKeeper().exists(groupAndRole, false);
			if (stat == null)
			{
				EnsurePath ensurePath = new EnsurePath(groupAndRole);
				ensurePath.ensure(client);
			}

			Stat services = client.getZooKeeper().exists(Services.SERVICES_PATH, false);
			if (services == null)
			{
				EnsurePath ensurePath = new EnsurePath(Services.SERVICES_PATH);
				ensurePath.ensure(client);
			}
			String path = groupAndRole + PATH_SEP + instance.getServerAddress();
			stat = client.getZooKeeper().exists(path, false);
			if (stat == null)
			{
				ObjectMapper mapper = new ObjectMapper();
				client.getZooKeeper().create(path, mapper.writeValueAsBytes(instance), OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
			}
			s_log.info("Agent : {} now ready to serve in the ensemble at {}", instance.getName(), path);
		}
		catch (Exception e)
		{
			error = true;
			if (e instanceof KeeperException.ConnectionLossException)
			{
				s_log.info("Flushing connection");
			}
			else
			{
				s_log.error(e.getMessage(), e);
			}

			throw e;

		}
		finally
		{
			if (error)
			{
				client.close();
			}
		}

		return this;
	}

	/**
	 * Track.
	 * 
	 * @param context
	 *            the context
	 */
	private void track(BundleContext context)
	{

		try
		{
			// Find all distributed services that was registered before this
			// bundle loaded.
			ServiceReference<?>[] services = context.getAllServiceReferences(DistributedService.class.getName(), null);
			if (services != null)
			{
				for (ServiceReference<?> s : services)
				{
					addingService(s);
				}
			}

			// monitor all future registrations.
			context.addServiceListener(this, FilterFactory.createServiceFilterString(DistributedService.class));
		}
		catch (InvalidSyntaxException e)
		{
			s_log.error(e.getMessage(), e);
			// should never happen.
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}

	}

	/**
	 * Add and register a service to this agent.
	 * 
	 * @param reference
	 *            the reference
	 */
	private void addingService(ServiceReference reference)
	{
		addingService(reference, false);
	}

	/**
	 * Add and register a service to this agent.
	 * 
	 * @param reference
	 *            the reference
	 * @param update
	 *            the update
	 */
	@SuppressWarnings("unchecked")
	private void addingService(ServiceReference reference, boolean update)
	{
		String[] classes = (String[]) reference.getProperty(Constants.OBJECTCLASS);
		if (classes != null)
		{
			for (String service : classes)
			{
				boolean wired = false;
				if (!DistributedService.class.getName().equals(service))
				{
					if (!rpcManager.containsService(service) || (rpcManager.containsService(service) && update))
					{
						Class<?> serv = null;
						Object impl = null;
						String anchor = null;
						try
						{
							serv = reference.getBundle().adapt(BundleWiring.class).getClassLoader().loadClass(service);
							impl = context.getService(reference);
							anchor = getPathForService(service);
							create(anchor, new byte[0]);
							wired = (serv != null) && (impl != null) && (anchor != null);

						}
						catch (ClassNotFoundException e)
						{
							throw new IllegalStateException(e);
						}
						catch (Exception e)
						{
							s_log.error(e.getMessage(), e);
						}
						finally
						{
							if (wired)
							{
								rpcManager.addService(serv, impl);
								s_log.info("{} was registered as a distributed service under {}", serv.getSimpleName(), anchor);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Removed a service from this agent
	 * 
	 * @param reference
	 *            the reference
	 */
	private void removedService(ServiceReference reference)
	{
		String[] classes = (String[]) reference.getProperty(Constants.OBJECTCLASS);
		for (String distservice : classes)
		{
			if (!DistributedService.class.getName().equals(distservice))
			{
				delete(getPathForService(distservice));
				rpcManager.removeService(distservice);
				s_log.info("{} was deregistered as a distributed service.", distservice);
			}
		}
	}

	/**
	 * Build the path for service of some provided service.
	 * 
	 * @param serviceInterface
	 *            the service interface
	 * @return the path for service
	 */
	public String getPathForService(String serviceInterface)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(Services.SERVICES_PATH);
		builder.append(PATH_SEP);
		builder.append(serviceInterface);
		builder.append(PATH_SEP);
		builder.append(agentInstance.getServerAddress());
		builder.append(PORT_SEP);
		builder.append(agentInstance.getRpcPort());
		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.ServiceListener#serviceChanged(org.osgi.framework.
	 * ServiceEvent)
	 */
	@Override
	public void serviceChanged(ServiceEvent event)
	{
		switch (event.getType())
		{
		case ServiceEvent.REGISTERED:
		{
			addingService(event.getServiceReference());
			break;
		}
		case ServiceEvent.UNREGISTERING:
		{
			removedService(event.getServiceReference());
			break;
		}
		case ServiceEvent.MODIFIED:
		{
			addingService(event.getServiceReference(), true);
			break;
		}
		}

	}

	/**
	 * Returns true if the agent is able to provide the service who's fully
	 * qualified name is <code>serviceName</code>.
	 * 
	 * @param serviceName
	 *            - the fully qualified class name.
	 */
	@Override
	public boolean hasService(String serviceName)
	{
		return rpcManager.containsService(serviceName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#get(java.lang.String, boolean)
	 */
	@Override
	public byte[] get(String path, boolean watch) throws Exception
	{
		byte[] data = null;
		try
		{
			if (client.isConnected())
			{
				data = client.getZooKeeper().getData(path, watch, new Stat());
			}
		}
		catch (KeeperException e)
		{
			if (!(e.code() == Code.NONODE))
			{
				throw e;
			}
			if (e instanceof KeeperException.ConnectionLossException)
			{
				// ignore
				s_log.warn("The get operation on {} did not succeed due to the zookeeper server becoming unavailable", path);
			}
			else
			{
				throw new RuntimeException(e);
			}
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#getChildren(java.lang.String, boolean)
	 */
	@Override
	public List<String> getChildren(String path, boolean watch) throws Exception
	{
		List<String> children = Collections.emptyList();
		try
		{
			if (client.isConnected())
			{
				children = client.getZooKeeper().getChildren(path, watch);
			}
		}
		catch (KeeperException e)
		{
			if (e instanceof KeeperException.ConnectionLossException)
			{
				// ignore
				s_log.warn("The getChildren operation on {} did not succeed due to the zookeeper server becoming unavailable", path);
			}
			else
			{
				throw e;
			}
		}
		return children;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#create(java.lang.String, byte[])
	 */
	@Override
	public void create(String path, byte[] data)
	{
		try
		{
			if (client.isConnected())
			{
				ZKPaths.mkdirs(client.getZooKeeper(), path, true, true);
			}
		}
		catch (Exception e)
		{
			if (e instanceof KeeperException.ConnectionLossException)
			{
				// ignore
				s_log.warn("The create operation on {} did not succeed due to the zookeeper server becoming unavailable", path);
			}
			else
			{
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Ensures a path exists for the given path.
	 */
	@Override
	public void ensurePath(String path)
	{
		ensurePath(path, false, new byte[0]);
	}

	/**
	 * Ensures a path exists for the given path.
	 */
	public void ensurePath(String path, boolean lastNodeEphemerald, byte[] data)
	{
		try
		{
			EnsurePath ensurePath = new EnsurePath(path, lastNodeEphemerald, data);
			ensurePath.ensure(client);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#delete(java.lang.String)
	 */
	@Override
	public void delete(String path)
	{
		try
		{
			if (client.isConnected())
			{
				client.getZooKeeper().delete(path, -1);
			}
		}
		catch (Exception e)
		{
			if (e instanceof KeeperException.ConnectionLossException)
			{
				// ignore
				s_log.warn("The delete operation on {} did not succeed due to the zookeeper server becoming unavailable", path);
			}
			else
			{
				throw new RuntimeException(e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#exists(java.lang.String)
	 */
	@Override
	public boolean exists(String path)
	{
		boolean exists = false;
		try
		{
			Stat stat = client.getZooKeeper().exists(path, false);
			if (stat != null)
			{
				exists = true;
			}
		}
		catch (Exception e)
		{
			// ignore
		}

		return exists;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#getName()
	 */
	@Override
	public String getAgentName()
	{
		return agentInstance.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#getHost()
	 */
	@Override
	public String getServerIpAddress()
	{
		return agentInstance.getServerAddress();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#getPort()
	 */
	@Override
	public int getPort()
	{
		return agentInstance.getPort();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#getRpcPort()
	 */
	@Override
	public int getRpcPort()
	{
		return agentInstance.getRpcPort();
	}

	/**
	 * Gets the service gateway.
	 * 
	 * @return the service gateway
	 */
	public ServiceGateway getServiceGateway()
	{
		return serviceGateway;
	}

	/**
	 * Gets the bundle context.
	 * 
	 * @return the bundle context
	 */
	public BundleContext getBundleContext()
	{
		return context;
	}

	/**
	 * Returns a random server in the ensemble deployed with a specific
	 * {@link Role}.
	 * 
	 * @param role
	 * @return
	 */
	public String getServersWithRole(Role role)
	{
		String addr = null;

		try
		{
			List<String> servers = getChildren(Agent.AGENT_GROUPS + "/" + role.name().toLowerCase(), true);
			int size = servers.size();
			if (size == 1)
			{
				addr = servers.get(0);
			}
			else if (size > 1)
			{
				addr = servers.get((int) Math.round(Math.random() * size));
			}
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}

		return addr;
	}

	/**
	 * Wait for a agent with a specific role, and ip to join the ensemble. This
	 * is a blocking call.
	 */
	public <V> V waitForAgent(Role role, AgentCallback<V> callback)
	{
		V result = null;
		int attempts = 45;
		boolean found = false;
		while (attempts-- > 0 && !found)
		{
			try
			{

				List<String> servers = getChildren(Agent.AGENT_GROUPS + "/" + role.name().toLowerCase(), true);
				for (String s : servers)
				{
					if (callback.getAgentAddress().equals(s))
					{
						result = callback.call();
						found = true;
						break;
					}
				}

			}
			catch (Exception e)
			{
				s_log.debug(e.getMessage(), e);
			}
			if (!found)
			{
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e)
				{
					s_log.debug(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.api.Agent#receive(org.storm.nexus.api.Message)
	 */
	@Override
	public void receive(Message message)
	{

	}

	/**
	 * Stop the nexus agent.
	 */
	public void stop(BundleContext context)
	{
		if (isRunning)
		{
			synchronized (this)
			{
				if (isRunning)
				{
					if (client != null)
					{
						client.close();
						client = null;
					}
					if (rpcManager != null)
					{
						rpcManager.shutdown();
						rpcManager = null;
					}
					isRunning = false;
				}

			}
		}

	}

	/**
	 * Overwritten equals impl.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		if (!(obj instanceof Agent)) return false;
		return hashCode() == obj.hashCode();
	}

	/**
	 * Overwritten hash code impl.
	 */
	@Override
	public int hashCode()
	{
		return getServerIpAddress().hashCode();
	}

	/**
	 * Process a zookeeper call back for this agent.
	 * 
	 */
	@Override
	public void process(WatchedEvent event)
	{
		if (serviceGateway != null)
		{
			ServiceGatewayImpl gateway = (ServiceGatewayImpl) serviceGateway;
			gateway.process(event);
		}

	}

}
