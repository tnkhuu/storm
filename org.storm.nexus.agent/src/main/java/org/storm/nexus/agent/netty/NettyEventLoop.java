/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.netty;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

import org.jboss.netty.channel.socket.ClientSocketChannelFactory;
import org.jboss.netty.channel.socket.ServerSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.msgpack.MessagePack;
import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.ClientTransport;
import org.storm.nexus.agent.service.Server;
import org.storm.nexus.agent.service.ServerTransport;
import org.storm.nexus.agent.service.config.TcpClientConfig;
import org.storm.nexus.agent.service.config.TcpServerConfig;
import org.storm.nexus.agent.session.Session;

/**
 * The Class NettyEventLoop.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class NettyEventLoop extends EventLoop
{
	
	/**
	 * Instantiates a new netty event loop.
	 * 
	 * @param workerExecutor
	 *            the worker executor
	 * @param ioExecutor
	 *            the io executor
	 * @param scheduledExecutor
	 *            the scheduled executor
	 * @param messagePack
	 *            the message pack
	 */
	public NettyEventLoop(ExecutorService workerExecutor, ExecutorService ioExecutor, ScheduledExecutorService scheduledExecutor, MessagePack messagePack)
	{
		super(workerExecutor, ioExecutor, scheduledExecutor, messagePack);
	}

	private ClientSocketChannelFactory clientFactory = null;
	private ServerSocketChannelFactory serverFactory = null;

	/**
	 * Gets the client factory.
	 * 
	 * @return the client factory
	 */
	public synchronized ClientSocketChannelFactory getClientFactory()
	{
		if (clientFactory == null)
		{
			clientFactory = new NioClientSocketChannelFactory(getIoExecutor(), getWorkerExecutor()); // TODO:
																										// workerCount
		}
		return clientFactory;
	}

	/**
	 * Gets the server factory.
	 * 
	 * @return the server factory
	 */
	public synchronized ServerSocketChannelFactory getServerFactory()
	{
		if (serverFactory == null)
		{
			serverFactory = new NioServerSocketChannelFactory(getIoExecutor(), getWorkerExecutor()); // TODO:
																										// workerCount
			// messages will be dispatched to worker thread on server.
			// see useThread(true) in NettyTcpClientTransport().
		}
		return serverFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.loop.EventLoop#openTcpTransport(org.storm.nexus
	 * .agent.service.config.TcpClientConfig,
	 * org.storm.nexus.agent.session.Session)
	 */
	@Override
	protected ClientTransport openTcpTransport(TcpClientConfig config, Session session)
	{
		return new NettyTcpClientTransport(config, session, this);
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.agent.loop.EventLoop#listenTcpTransport(org.storm.nexus.agent.service.config.TcpServerConfig, org.storm.nexus.agent.service.Server)
	 */
	@Override
	protected ServerTransport listenTcpTransport(TcpServerConfig config, Server server)
	{
		return new NettyTcpServerTransport(config, server, this);
	}
}
