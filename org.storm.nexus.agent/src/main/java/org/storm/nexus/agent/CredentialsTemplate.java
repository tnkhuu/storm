/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent;

import java.io.IOException;

import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.template.AbstractTemplate;
import org.msgpack.unpacker.Unpacker;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.security.Credentials;

/**
 * The Class CredentialsTemplate.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class CredentialsTemplate extends AbstractTemplate<Credentials>
{

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#write(org.msgpack.packer.Packer, java.lang.Object, boolean)
	 */
	@Override
	public void write(Packer pk, Credentials target, boolean required) throws IOException
	{
		if (target == null)
		{
			if (required)
			{
				throw new MessageTypeException("Attempted to write null");
			}
			pk.writeNil();
			return;
		}
		
		pk.write(target.getAccessKey());
		pk.write(target.getProvider().name());
		pk.write(target.getSecret());
		
	}

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#read(org.msgpack.unpacker.Unpacker, java.lang.Object, boolean)
	 */
	@Override
	public Credentials read(Unpacker u, Credentials to, boolean required) throws IOException
	{
		if (!required && u.trySkipNil())
		{
			return null;
		}
		
		String accessKey =	u.readString();
		CloudProvider provider = CloudProvider.valueOf(u.readString());
		String secret = u.readString();
		return new Credentials(accessKey, secret, provider);
	}

}
