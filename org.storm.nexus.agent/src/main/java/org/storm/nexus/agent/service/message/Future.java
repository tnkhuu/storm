/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.message;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.msgpack.type.Value;
import org.msgpack.unpacker.Converter;
import org.storm.nexus.api.exception.RemoteRPCException;

/**
 * The Class Future.
 * 
 * @param <V>
 *            the value type
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("rawtypes")
public class Future<V> implements java.util.concurrent.Future<V>
{
	private FutureImpl impl;
	private Template resultTemplate;
	private MessagePack messagePack;

	/**
	 * Instantiates a new future.
	 * 
	 * @param messagePack
	 *            the message pack
	 * @param impl
	 *            the impl
	 */
	public Future(MessagePack messagePack, FutureImpl impl)
	{
		this(messagePack, impl, null);
	}

	/**
	 * Instantiates a new future.
	 * 
	 * @param messagePack
	 *            the message pack
	 * @param impl
	 *            the impl
	 * @param resultTemplate
	 *            the result template
	 */
	public Future(MessagePack messagePack, FutureImpl impl, Template resultTemplate)
	{
		this.impl = impl;
		this.resultTemplate = resultTemplate;
		this.messagePack = messagePack;
	}

	/**
	 * Instantiates a new future.
	 * 
	 * @param messagePack
	 *            the message pack
	 * @param future
	 *            the future
	 * @param resultClass
	 *            the result class
	 */
	public Future(MessagePack messagePack, Future<Value> future, Class<V> resultClass)
	{
		this(messagePack, future.impl, null);
		if (resultClass != void.class)
		{
			this.resultTemplate = messagePack.lookup(resultClass);
		}
	}

	/**
	 * Instantiates a new future.
	 * 
	 * @param messagePack
	 *            the message pack
	 * @param future
	 *            the future
	 * @param resultTemplate
	 *            the result template
	 */
	public Future(MessagePack messagePack, Future<Value> future, Template resultTemplate)
	{
		this(messagePack, future.impl, resultTemplate);
	}

	/**
	 * Attach callback.
	 * 
	 * @param callback
	 *            the callback
	 */
	public void attachCallback(Runnable callback)
	{
		impl.attachCallback(callback);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Future#get()
	 */
	@Override
	public V get() throws InterruptedException
	{
		join();
		checkThrowError();
		return getResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	@Override
	public V get(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException
	{
		join(timeout, unit);
		checkThrowError();
		return getResult();
	}

	/**
	 * Join.
	 * 
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	public void join() throws InterruptedException
	{
		impl.join();
	}

	/**
	 * Join.
	 * 
	 * @param timeout
	 *            the timeout
	 * @param unit
	 *            the unit
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws TimeoutException
	 *             the timeout exception
	 */
	public void join(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException
	{
		impl.join(timeout, unit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Future#isDone()
	 */
	@Override
	public boolean isDone()
	{
		return impl.isDone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Future#cancel(boolean)
	 */
	@Override
	public boolean cancel(boolean mayInterruptIfRunning)
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Future#isCancelled()
	 */
	@Override
	public boolean isCancelled()
	{
		return false;
	}

	/**
	 * Gets the result.
	 * 
	 * @return the result
	 */
	@SuppressWarnings("unchecked")
	public V getResult()
	{
		Value result = impl.getResult();
		if (resultTemplate == null)
		{
			return (V) result;
		}
		else if (result.isNilValue())
		{
			return null;
		}
		else
		{
			try
			{
				return (V) resultTemplate.read(new Converter(messagePack, result), null);
			}
			catch (IOException e)
			{
				return null;
			}
		}
	}

	/**
	 * Gets the error.
	 * 
	 * @return the error
	 */
	public Value getError()
	{
		return impl.getError();
	}

	/**
	 * Check throw error.
	 */
	private void checkThrowError()
	{
		if (!getError().isNilValue())
		{
			throw new RemoteRPCException(getError());
		}
	}
}
