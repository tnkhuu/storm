/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.session;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;

/**
 * The Class IPAddress.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class IPAddress extends Address
{

	/** The address. */
	private byte[] address;

	/** The port. */
	private int port;

	/** zoo keeper path. */
	private String path;

	/**
	 * Instantiates a new iP address.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public IPAddress(String host, int port) throws UnknownHostException
	{
		this.address = InetAddress.getByName(host).getAddress();
		this.port = port;
		this.path = host + HOST_PORT_SEP + port;
	}

	/**
	 * Instantiates a new iP address.
	 * 
	 * @param port
	 *            the port
	 */
	public IPAddress(int port)
	{
		try
		{
			this.address = new InetSocketAddress(port).getAddress().getAddress();
			this.port = port;
			this.path = InetAddress.getLocalHost().getHostName() + HOST_PORT_SEP + port;
		}
		catch (UnknownHostException e)
		{
			// should never happen.
			throw new RuntimeException(e);
		}
	}

	/**
	 * Instantiates a new iP address.
	 * 
	 * @param addr
	 *            the addr
	 */
	public IPAddress(InetSocketAddress addr)
	{
		this.address = addr.getAddress().getAddress();
		this.port = addr.getPort();
		this.path = addr.getHostName() + HOST_PORT_SEP + port;

	}

	/**
	 * Instantiates a new iP address.
	 * 
	 * @param addr
	 *            the addr
	 * @param port
	 *            the port
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public IPAddress(InetAddress addr, int port) throws UnknownHostException
	{
		this.address = addr.getAddress();
		this.port = port;
		this.path = addr.getHostName() + HOST_PORT_SEP + port;
	}

	/**
	 * Gets the inet socket address.
	 * 
	 * @return the inet socket address
	 */
	public InetSocketAddress getInetSocketAddress()
	{
		try
		{
			return new InetSocketAddress(InetAddress.getByAddress(address), port);
		}
		catch (UnknownHostException e)
		{
			throw new RuntimeException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.agent.session.Address#getSocketAddress()
	 */
	@Override
	public SocketAddress getSocketAddress()
	{
		return getInetSocketAddress();
	}

	@Override
	public String getPath()
	{
		return path;
	}
}
