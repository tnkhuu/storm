/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.service.dispatcher;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.storm.nexus.agent.service.message.Request;
import org.storm.nexus.agent.service.proxy.Invoker;
import org.storm.nexus.agent.service.proxy.MethodSelector;
import org.storm.nexus.agent.service.proxy.Reflect;

/**
 * The Class MethodDispatcher.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MethodDispatcher implements Dispatcher
{ 
	protected Map<String, Invoker> methodMap;
	protected Object target;
	protected Reflect reflect;

	/**
	 * Instantiates a new method dispatcher.
	 * 
	 * @param reflect
	 *            the reflect
	 * @param target
	 *            the target
	 */
	public MethodDispatcher(Reflect reflect, Object target)
	{
		this(reflect, target, target.getClass());
	}

	/**
	 * Instantiates a new method dispatcher.
	 * 
	 * @param reflect
	 *            the reflect
	 * @param target
	 *            the target
	 * @param iface
	 *            the iface
	 */
	public MethodDispatcher(Reflect reflect, Object target, Class<?> iface)
	{
		this(reflect, target, MethodSelector.selectRpcServerMethod(iface));
	}

	/**
	 * Instantiates a new method dispatcher.
	 * 
	 * @param reflect
	 *            the reflect
	 * @param target
	 *            the target
	 * @param methods
	 *            the methods
	 */
	public MethodDispatcher(Reflect reflect, Object target, Method[] methods)
	{

		this.target = target;
		this.methodMap = new HashMap<String, Invoker>();
		this.reflect = reflect;
		for (Method method : methods)
		{
			methodMap.put(method.getName(), reflect.getInvoker(method));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.dispatcher.Dispatcher#dispatch(org.storm
	 * .nexus.agent.service.proxy.Request)
	 */
	@Override
	public void dispatch(Request request) throws Exception
	{
		Invoker ivk = methodMap.get(request.getMethodName());
		if (ivk == null)
		{
			throw new IOException(".CallError.NoMethodError");
		}
		ivk.invoke(target, request);
	}
}
