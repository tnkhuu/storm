/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.loop;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.msgpack.MessagePack;
import org.storm.nexus.agent.service.ClientTransport;
import org.storm.nexus.agent.service.Server;
import org.storm.nexus.agent.service.ServerTransport;
import org.storm.nexus.agent.service.config.ClientConfig;
import org.storm.nexus.agent.service.config.ServerConfig;
import org.storm.nexus.agent.service.config.TcpClientConfig;
import org.storm.nexus.agent.service.config.TcpServerConfig;
import org.storm.nexus.agent.session.Session;

/**
 * The Class EventLoop.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class EventLoop
{
	static private EventLoopFactory loopFactory;
	static private EventLoop defaultEventLoop;
	private ExecutorService workerExecutor;
	private ExecutorService ioExecutor;
	private ScheduledExecutorService scheduledExecutor;
	private MessagePack messagePack;

	/**
	 * Sets the factory.
	 * 
	 * @param factory
	 *            the new factory
	 */
	static public synchronized void setFactory(EventLoopFactory factory)
	{
		loopFactory = factory;
	}

	/**
	 * Gets the factory.
	 * 
	 * @return the factory
	 */
	static private synchronized EventLoopFactory getFactory()
	{
		if (loopFactory == null)
		{
			loopFactory = new NettyEventLoopFactory();
		}
		return loopFactory;
	}

	/**
	 * Sets the default event loop.
	 * 
	 * @param eventLoop
	 *            the new default event loop
	 */
	static public synchronized void setDefaultEventLoop(EventLoop eventLoop)
	{
		defaultEventLoop = eventLoop;
	}

	/**
	 * Default event loop.
	 * 
	 * @return the event loop
	 */
	static public synchronized EventLoop defaultEventLoop()
	{
		if (defaultEventLoop == null)
		{
			defaultEventLoop = start();
		}
		return defaultEventLoop;
	}

	/**
	 * Start.
	 * 
	 * @return the event loop
	 */
	static public EventLoop start()
	{
		return start(Executors.newCachedThreadPool());
	}

	/**
	 * Start.
	 * 
	 * @param workerExecutor
	 *            the worker executor
	 * @return the event loop
	 */
	static public EventLoop start(ExecutorService workerExecutor)
	{
		return start(workerExecutor, Executors.newCachedThreadPool());
	}

	/**
	 * Start.
	 * 
	 * @param workerExecutor
	 *            the worker executor
	 * @param ioExecutor
	 *            the io executor
	 * @return the event loop
	 */
	static public EventLoop start(ExecutorService workerExecutor, ExecutorService ioExecutor)
	{
		return start(workerExecutor, ioExecutor, Executors.newScheduledThreadPool(2), new MessagePack());
	}

	/**
	 * Start.
	 * 
	 * @param workerExecutor
	 *            the worker executor
	 * @param ioExecutor
	 *            the io executor
	 * @param scheduledExecutor
	 *            the scheduled executor
	 * @param messagePack
	 *            the message pack
	 * @return the event loop
	 */
	static public EventLoop start(ExecutorService workerExecutor, ExecutorService ioExecutor, ScheduledExecutorService scheduledExecutor, MessagePack messagePack)
	{
		return getFactory().make(workerExecutor, ioExecutor, scheduledExecutor, messagePack);
	}

	/**
	 * Start.
	 * 
	 * @param messagePack
	 *            the message pack
	 * @param asyncCores
	 *            the async cores
	 * @return the event loop
	 */
	static public EventLoop start(MessagePack messagePack, int asyncCores)
	{
		return getFactory().make(Executors.newCachedThreadPool(), Executors.newCachedThreadPool(), Executors.newScheduledThreadPool(asyncCores), messagePack);
	}

	/**
	 * Gets the message pack.
	 * 
	 * @return the message pack
	 */
	public MessagePack getMessagePack()
	{
		return messagePack;
	}

	/**
	 * Sets the message pack.
	 * 
	 * @param messagePack
	 *            the new message pack
	 */
	public void setMessagePack(MessagePack messagePack)
	{
		this.messagePack = messagePack;
	}

	/**
	 * Instantiates a new event loop.
	 * 
	 * @param workerExecutor
	 *            the worker executor
	 * @param ioExecutor
	 *            the io executor
	 * @param scheduledExecutor
	 *            the scheduled executor
	 * @param messagePack
	 *            the message pack
	 */
	public EventLoop(ExecutorService workerExecutor, ExecutorService ioExecutor, ScheduledExecutorService scheduledExecutor, MessagePack messagePack)
	{
		this.workerExecutor = workerExecutor;
		this.scheduledExecutor = scheduledExecutor;
		this.ioExecutor = ioExecutor;
		this.messagePack = messagePack;
	}

	/**
	 * Gets the worker executor.
	 * 
	 * @return the worker executor
	 */
	public ExecutorService getWorkerExecutor()
	{
		return workerExecutor;
	}

	/**
	 * Gets the io executor.
	 * 
	 * @return the io executor
	 */
	public ExecutorService getIoExecutor()
	{
		return ioExecutor;
	}

	/**
	 * Gets the scheduled executor.
	 * 
	 * @return the scheduled executor
	 */
	public ScheduledExecutorService getScheduledExecutor()
	{
		return scheduledExecutor;
	}

	/**
	 * Shutdown.
	 */
	public void shutdown()
	{
		scheduledExecutor.shutdown();
		ioExecutor.shutdown();
		workerExecutor.shutdown();
	}

	/**
	 * Join.
	 * 
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	public void join() throws InterruptedException
	{
		scheduledExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		ioExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		workerExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
	}

	/**
	 * Open transport.
	 * 
	 * @param config
	 *            the config
	 * @param session
	 *            the session
	 * @return the client transport
	 */
	public ClientTransport openTransport(ClientConfig config, Session session)
	{
		if (config instanceof TcpClientConfig)
		{
			return openTcpTransport((TcpClientConfig) config, session);
		}

		throw new RuntimeException("Unknown transport config: " + config.getClass().getName());
	}

	/**
	 * Listen transport.
	 * 
	 * @param config
	 *            the config
	 * @param server
	 *            the server
	 * @return the server transport
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public ServerTransport listenTransport(ServerConfig config, Server server) throws IOException
	{
		if (config instanceof TcpServerConfig)
		{
			return listenTcpTransport((TcpServerConfig) config, server);
		}

		throw new RuntimeException("Unknown transport config: " + config.getClass().getName());
	}

	/**
	 * Open tcp transport.
	 * 
	 * @param config
	 *            the config
	 * @param session
	 *            the session
	 * @return the client transport
	 */
	protected abstract ClientTransport openTcpTransport(TcpClientConfig config, Session session);

	/**
	 * Listen tcp transport.
	 * 
	 * @param config
	 *            the config
	 * @param server
	 *            the server
	 * @return the server transport
	 */
	protected abstract ServerTransport listenTcpTransport(TcpServerConfig config, Server server);
}
