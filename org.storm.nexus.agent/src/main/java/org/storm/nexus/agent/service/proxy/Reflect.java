/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.proxy;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.msgpack.MessagePack;

/**
 * The Class Reflect.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Reflect
{

	/** The proxy cache. */
	private Map<Class<?>, Proxy<?>> proxyCache = new HashMap<Class<?>, Proxy<?>>();

	/** The invoker cache. */
	private Map<Method, Invoker> invokerCache = new HashMap<Method, Invoker>();

	/** The invoker builder. */
	private InvokerBuilder invokerBuilder;

	/** The proxy builder. */
	private ProxyBuilder proxyBuilder;

	/**
	 * Instantiates a new reflect.
	 * 
	 * @param messagePack
	 *            the message pack
	 */
	public Reflect(MessagePack messagePack)
	{
		invokerBuilder = new ReflectionInvokerBuilder(messagePack);
		proxyBuilder = new ReflectionProxyBuilder(messagePack);
	}

	/**
	 * Instantiates a new reflect.
	 * 
	 * @param invokerBuilder
	 *            the invoker builder
	 * @param proxyBuilder
	 *            the proxy builder
	 */
	public Reflect(InvokerBuilder invokerBuilder, ProxyBuilder proxyBuilder)
	{
		this.invokerBuilder = invokerBuilder;
		this.proxyBuilder = proxyBuilder;
	}

	/**
	 * Gets the proxy.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param iface
	 *            the iface
	 * @return the proxy
	 */
	@SuppressWarnings("unchecked")
	public synchronized <T> Proxy<T> getProxy(Class<T> iface)
	{
		Proxy<?> proxy = proxyCache.get(iface);
		if (proxy == null)
		{
			proxy = proxyBuilder.buildProxy(iface);
			proxyCache.put(iface, proxy);
		}
		return (Proxy<T>) proxy;
	}

	/**
	 * Gets the invoker.
	 * 
	 * @param method
	 *            the method
	 * @return the invoker
	 */
	public synchronized Invoker getInvoker(Method method)
	{
		Invoker invoker = invokerCache.get(method);
		if (invoker == null)
		{
			invoker = invokerBuilder.buildInvoker(method);
			invokerCache.put(method, invoker);
		}
		return invoker;
	}
}
