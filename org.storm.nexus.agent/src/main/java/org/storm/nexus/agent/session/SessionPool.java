/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.session;

import java.io.Closeable;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.config.ClientConfig;
import org.storm.nexus.agent.service.config.TcpClientConfig;

/**
 * The Class SessionPool.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SessionPool implements Closeable
{

	/** The config. */
	private ClientConfig config;

	/** The loop. */
	private EventLoop loop;

	/** The pool. */
	private Map<Address, Session> pool = new HashMap<Address, Session>();

	/** The timer. */
	private ScheduledFuture<?> timer;

	/**
	 * Instantiates a new session pool.
	 */
	public SessionPool()
	{
		this(new TcpClientConfig());
	}

	/**
	 * Instantiates a new session pool.
	 * 
	 * @param config
	 *            the config
	 */
	public SessionPool(ClientConfig config)
	{
		this(config, EventLoop.defaultEventLoop());
	}

	/**
	 * Instantiates a new session pool.
	 * 
	 * @param loop
	 *            the loop
	 */
	public SessionPool(EventLoop loop)
	{
		this(new TcpClientConfig(), loop);
	}

	/**
	 * Instantiates a new session pool.
	 * 
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 */
	public SessionPool(ClientConfig config, EventLoop loop)
	{
		this.config = config;
		this.loop = loop;
		startTimer();
	}

	/**
	 * Gets the event loop.
	 * 
	 * @return the event loop
	 */
	public EventLoop getEventLoop()
	{
		return loop;
	}

	/**
	 * Gets the session.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @return the session
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public Session getSession(String host, int port) throws UnknownHostException
	{
		return getSession(new IPAddress(host, port));
	}

	/**
	 * Gets the session.
	 * 
	 * @param address
	 *            the address
	 * @return the session
	 */
	public Session getSession(InetSocketAddress address)
	{
		return getSession(new IPAddress(address));
	}

	/**
	 * Gets the session.
	 * 
	 * @param address
	 *            the address
	 * @return the session
	 */
	Session getSession(Address address)
	{
		synchronized (pool)
		{
			Session s = pool.get(address);
			if (s == null)
			{
				s = new Session(address, config, loop);
				pool.put(address, s);
			}
			return s;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close()
	{
		timer.cancel(false);
		synchronized (pool)
		{
			for (Map.Entry<Address, Session> pair : pool.entrySet())
			{
				Session s = pair.getValue();
				s.closeSession();
			}
			pool.clear();
		}
	}

	/**
	 * Start timer.
	 */
	private void startTimer()
	{
		Runnable command = new Runnable()
		{
			@Override
			public void run()
			{
				stepTimeout();
			}
		};
		timer = loop.getScheduledExecutor().scheduleAtFixedRate(command, 1000, 1000, TimeUnit.MILLISECONDS);
	}

	/**
	 * Step timeout.
	 */
	void stepTimeout()
	{
		synchronized (pool)
		{
			for (Map.Entry<Address, Session> pair : pool.entrySet())
			{
				Session s = pair.getValue();
				s.stepTimeout();
			}
		}
	}
}
