/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.service.config;

import java.util.HashMap;
import java.util.Map;

import org.storm.nexus.agent.session.Address;

/**
 * The Class ServerConfig.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ServerConfig
{

	private Address listenAddress;
	private Map<String, Object> options = new HashMap<String, Object>();

	/**
	 * Instantiates a new server config.
	 * 
	 * @param listenAddress
	 *            the listen address
	 */
	public ServerConfig(Address listenAddress)
	{
		this.listenAddress = listenAddress;
	}

	/**
	 * Gets the listen address.
	 * 
	 * @return the listen address
	 */
	public Address getListenAddress()
	{
		return listenAddress;
	}

	/**
	 * Sets the option.
	 * 
	 * @param key
	 *            the key
	 * @param o
	 *            the o
	 */
	public void setOption(String key, Object o)
	{
		options.put(key, o);
	}

	/**
	 * Gets the option.
	 * 
	 * @param key
	 *            the key
	 * @return the option
	 */
	public Object getOption(String key)
	{
		return options.get(key);
	}

	/**
	 * Gets the options.
	 * 
	 * @return the options
	 */
	public Map<String, Object> getOptions()
	{
		return options;
	}
}
