/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.loop;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

import org.msgpack.MessagePack;


/**
 * A factory for creating EventLoop objects.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface EventLoopFactory
{

	/**
	 * Make an event loop session for rpc calls.
	 * 
	 * @param workerExecutor
	 *            the worker executor
	 * @param ioExecutor
	 *            the io executor
	 * @param scheduledExecutor
	 *            the scheduled executor
	 * @param messagePack
	 *            the message pack
	 * @return the event loop
	 */
	public EventLoop make(ExecutorService workerExecutor, ExecutorService ioExecutor, ScheduledExecutorService scheduledExecutor, MessagePack messagePack);

}
