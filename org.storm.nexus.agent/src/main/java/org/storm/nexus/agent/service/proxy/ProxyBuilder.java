/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.proxy;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.storm.nexus.agent.service.message.Future;

/**
 * The Class ProxyBuilder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class ProxyBuilder
{

	/**
	 * The Class MethodEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class MethodEntry
	{

		private Method method;
		private String className;
		private String rpcName;
		private Type genericReturnType;
		private boolean async;
		private InvokerBuilder.ArgumentEntry[] argumentEntries;

		/**
		 * Instantiates a new method entry.
		 * 
		 * @param method
		 *            the method
		 * @param rpcName
		 *            the rpc name
		 * @param genericReturnType
		 *            the generic return type
		 * @param async
		 *            the async
		 * @param argumentEntries
		 *            the argument entries
		 */
		public MethodEntry(String interfaceName, Method method, String rpcName, Type genericReturnType, boolean async, InvokerBuilder.ArgumentEntry[] argumentEntries)
		{
			this.className = interfaceName;
			this.method = method;
			this.rpcName = rpcName;
			this.genericReturnType = genericReturnType;
			this.async = async;
			this.argumentEntries = argumentEntries;
		}

		/**
		 * Gets the method.
		 * 
		 * @return the method
		 */
		public Method getMethod()
		{
			return method;
		}

		/**
		 * Gets the rpc name.
		 * 
		 * @return the rpc name
		 */
		public String getRpcName()
		{
			return rpcName;
		}

		/**
		 * Gets the methods owning interface.
		 * 
		 * @return the methods class
		 */
		public String getClassName()
		{
			return className;
		}

		/**
		 * Gets the generic return type.
		 * 
		 * @return the generic return type
		 */
		public Type getGenericReturnType()
		{
			return genericReturnType;
		}

		/**
		 * Checks if is return type void.
		 * 
		 * @return true, if is return type void
		 */
		public boolean isReturnTypeVoid()
		{
			return (genericReturnType == void.class) || (genericReturnType == Void.class);
		}

		/**
		 * Checks if is async.
		 * 
		 * @return true, if is async
		 */
		public boolean isAsync()
		{
			return async;
		}

		/**
		 * Gets the argument entries.
		 * 
		 * @return the argument entries
		 */
		public InvokerBuilder.ArgumentEntry[] getArgumentEntries()
		{
			return argumentEntries;
		}
	}

	/**
	 * Builds the proxy.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param iface
	 *            the iface
	 * @param entries
	 *            the entries
	 * @return the proxy
	 */
	public abstract <T> Proxy<T> buildProxy(Class<T> iface, MethodEntry[] entries);

	/**
	 * Builds the proxy.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param iface
	 *            the iface
	 * @return the proxy
	 */
	public <T> Proxy<T> buildProxy(Class<T> iface)
	{
		checkValidation(iface);
		MethodEntry[] entries = readMethodEntries(iface);
		return buildProxy(iface, entries);
	}

	/**
	 * Checks if is async method.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @return true, if is async method
	 */
	static boolean isAsyncMethod(Method targetMethod)
	{
		// return type is Future<T>
		return targetMethod.getReturnType().equals(Future.class) || targetMethod.getReturnType().equals(java.util.concurrent.Future.class);
	}

	/**
	 * Check validation.
	 * 
	 * @param iface
	 *            the iface
	 */
	private static void checkValidation(Class<?> iface)
	{
		if (!iface.isInterface())
		{
			throw new IllegalArgumentException("not interface: " + iface);
		}
	}

	/**
	 * Read method entries.
	 * 
	 * @param iface
	 *            the iface
	 * @return the method entry[]
	 */
	static MethodEntry[] readMethodEntries(Class<?> iface)
	{
		Method[] methods = MethodSelector.selectRpcClientMethod(iface);

		MethodEntry[] result = new MethodEntry[methods.length];
		for (int i = 0; i < methods.length; i++)
		{
			Method method = methods[i];

			InvokerBuilder.ArgumentEntry[] argumentEntries = InvokerBuilder.readArgumentEntries(method, false);

			boolean async = isAsyncMethod(method);

			String rpcName = method.getName();
			/*
			 * if (async) { if (rpcName.endsWith("Async")) { rpcName =
			 * rpcName.substring(0, rpcName.length() - 5); } }
			 */

			Type returnType = method.getGenericReturnType();
			if (async)
			{
				returnType = ((ParameterizedType) returnType).getActualTypeArguments()[0];
			}

			result[i] = new MethodEntry(iface.getName(), method, rpcName, returnType, async, argumentEntries);
		}

		return result;
	}
}
