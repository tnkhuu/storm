/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.services.ServiceGateway;
import org.storm.nexus.api.Agent;
import org.storm.nexus.api.Services;
import org.storm.nexus.api.exception.AgentUnavailableException;
import org.storm.nexus.api.exception.ServiceNotFoundException;

/**
 * The Distributed Services Gateway implementation. This is where each node agent goes to obtain remote services offered by other nodes on the cloud ensemble. Handles the dynamic
 * registration/life cycle of services as they come and go.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unused")
public class ServiceGatewayImpl implements ServiceGateway, Watcher
{

	private static final Logger s_log = LoggerFactory.getLogger(ServiceGatewayImpl.class);
	private static final String URL_SEP = "/";
	private static final String PORT_SEP = ":";
	private final Map<Class<?>, List<Client>> distributedServices = new HashMap<Class<?>, List<Client>>();
	private final Agent agent;
	private final BundleContext context;
	private final Random random = new Random();

	/**
	 * Instantiates a new service gateway impl.
	 * 
	 * @param agent
	 *            the agent
	 * @param context
	 *            the context
	 */
	public ServiceGatewayImpl(Agent agent, BundleContext context)
	{
		this.agent = agent;
		this.context = context;
		boolean success = false;
		try
		{
			List<String> services = agent.getChildren(Services.SERVICES_PATH, true);
			if (services != null && services.size() > 0)
			{
				for (String s : services)
				{
					refreshService(s);
				}
			}
			success = true;
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}
		finally
		{
			if (success)
			{
				context.registerService(ServiceGateway.class, this, null);
				s_log.info("The Service Gateway is now online");
			}
		}

	}

	/**
	 * Process some arbitrary change event from the zookeeper ensemble. This is a callback which is called and provided by zookeeper.
	 * 
	 * @param event
	 *            - the change that occured.
	 */
	@Override
	public void process(WatchedEvent event)
	{
		try
		{
			String path = event.getPath();
			if (path != null)
			{

				List<String> children = agent.getChildren(path, true);
				for (String c : children)
				{
					refreshService(c);
				}

			}
		}
		catch (Exception e)
		{
			s_log.error(e.getMessage(), e);
		}
	}

	/**
	 * Lookup a distributed service on the nexus ensemble. If their are multiple agents providing the same service, one will be picked at random.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param service
	 *            the service
	 * @return a proxied, remote service.
	 * 
	 * @throws {@link ServiceNotFoundException} if the service could not be found
	 */
	@Override
	public <T> T lookup(Class<T> service) throws ServiceNotFoundException
	{
		T instance = null;
		List<Client> clients = distributedServices.get(service);
		if ((clients != null) && (clients.size() > 0))
		{
			int client = 0;
			if (clients.size() > 1)
			{
				client = random.nextInt(clients.size() - 1);
				if (client < 0)
				{
					client = 0;
				}
			}

			Client randomServiceProvider = clients.get(client);
			if (agent.exists(getCorrespondingZookeeperPath(service, randomServiceProvider)))
			{
				// agent still serving on the ensemble. safe to proxy.
				instance = randomServiceProvider.proxy(service);
			}
			else
			{
				// agent no longer available. remove and recurse.
				clients.remove(client);
				lookup(service);
			}
		}
		else
		{
			try
			{
				List<String> children = agent.getChildren(Services.SERVICES_PATH + "/" + service.getName(), true);
				if (children != null && children.size() > 0)
				{
					for (String node : children)
					{
						refresh(service.getName(), node);
					}
					return lookup(service);
				}

			}
			catch (Exception e)
			{

				s_log.error(e.getMessage(), e);
			}
		}
		return instance;
	}

	/**
	 * Lookup a distributed service on a specific agent in the ensemble.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param service
	 *            the service
	 * @param targetAgent
	 *            the target agent
	 * @return a proxied, remote service.
	 * 
	 * @throws {@link ServiceNotFoundException} if the service could not be found
	 * @throws {@link AgentUnavailableException} if the agent is no longer available.
	 */
	@Override
	public <T> T lookup(Class<T> service, Agent targetAgent) throws ServiceNotFoundException, AgentUnavailableException
	{
		T instance = null;

		try
		{
			String addr = targetAgent.getServerIpAddress();
			int rpcPort = targetAgent.getRpcPort();
			boolean online = agent.exists(getAgentPath(service.getName(), addr, rpcPort));
			if (online)
			{
				Client targetClient = new Client(addr, Integer.valueOf(rpcPort));
				List<Client> agents = distributedServices.get(service);
				if (agents == null)
				{
					agents = new ArrayList<>();
				}
				agents.add(targetClient);
				distributedServices.put(service, agents);
				instance = targetClient.proxy(service);

			}
			else
			{
				throw new AgentUnavailableException("No service of type " + service + " was found for agent " + targetAgent);
			}
		}
		catch (UnknownHostException e)
		{
			// shouldn't happen, but just incase.
			if (s_log.isDebugEnabled())
			{
				s_log.error(e.getMessage(), e);
			}
			throw new RuntimeException(e);
		}

		return instance;
	}

	/**
	 * Service has changed on the ensemble. The path for all services actively published on the ensemble would be of the format :
	 * 
	 * /services/<interface name>/agentName:agentPort where agentName and agentPort would map to the actual IP/Port address of where that agent resides, and is offering the service
	 * from.
	 * 
	 * @param event
	 *            the event
	 * @throws Exception
	 *             the exception
	 */
	private void serviceChanged(WatchedEvent event) throws Exception
	{
		switch (event.getType())
		{
		case NodeCreated:
		{
			s_log.info("Node Creation event from ensemble: {}", event.getPath());
			refreshService(event.getPath());

			/*
			 * Object proxy = client.proxy(service); context.registerService(serviceName, proxy, null); s_log.debug("Proxy Service" + proxy + " created for service : " +
			 * serviceName);
			 */

			break;
		}
		case NodeDeleted:
		{
			s_log.debug("Received Node Deleted Event");
			break;
		}
		case NodeDataChanged:
		{
			s_log.debug("Received Node Data Changed Event");
			break;
		}
		case NodeChildrenChanged:
		{
			s_log.debug("Received Node Children Changed Event");
			break;
		}
		default:
		{
			s_log.debug("Received No event");
			break;
		}
		}

		agent.getChildren(Services.SERVICES_PATH, true);

	}

	/**
	 * @param event
	 * @throws UnknownHostException
	 * @throws ClassNotFoundException
	 */
	private void refreshService(String path) throws UnknownHostException, ClassNotFoundException
	{
		if (path != null)
		{
			String[] servicePath = path.split(URL_SEP);
			if (servicePath.length > 1)
			{
				String serviceName = servicePath[0];
				String server = servicePath[1];
				refresh(serviceName, server);
			}
		}
	}

	private void refresh(String serviceName, String path) throws UnknownHostException, ClassNotFoundException
	{
		String[] server = path.split(PORT_SEP);
		Client client = new Client(server[0], Integer.valueOf(server[1]));
		Class<?> service = Class.forName(serviceName);
		List<Client> clients = distributedServices.get(service);
		if (clients == null)
		{
			clients = new ArrayList<>();
		}
		if (!clients.contains(client))
		{
			clients.add(client);
		}
		s_log.info("Discovered {} from {}", serviceName, client);
		Object proxy = client.proxy(service);
		context.registerService(new String[]
		{ serviceName }, proxy, null);
		distributedServices.put(service, clients);
	}

	/**
	 * Returns the matching agent provider path which should exist in zoo keeper is the agent is still active.
	 * 
	 * @param service
	 * @param target
	 * @return
	 */
	private String getCorrespondingZookeeperPath(Class<?> service, Client target)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(Services.SERVICES_PATH);
		builder.append(URL_SEP);
		builder.append(service.getName());
		builder.append(URL_SEP);
		builder.append(target.getPath());
		return builder.toString();
	}

	/**
	 * Return the agents anchoring path in zookeeper.
	 * 
	 * @param host
	 * @param port
	 * @return
	 */
	private String getAgentPath(String service, String host, int port)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(Services.SERVICES_PATH);
		builder.append(URL_SEP);
		builder.append(service);
		builder.append(URL_SEP);
		builder.append(host);
		builder.append(PORT_SEP);
		builder.append(port);
		return builder.toString();
	}

	@Override
	public String toString()
	{
		return "ServiceGatewayImpl [distributedServices=" + distributedServices + "]";
	}

}
