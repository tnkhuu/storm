/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.dispatcher;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.storm.nexus.agent.service.message.Request;
import org.storm.nexus.agent.service.proxy.Invoker;
import org.storm.nexus.agent.service.proxy.MethodSelector;
import org.storm.nexus.agent.service.proxy.Reflect;

/**
 * The Class ServiceDispatcher.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ServiceDispatcher implements Dispatcher
{
	protected Map<String, Invoker> proxies;
	protected Object target = null;

	/**
	 * Instantiates a new method dispatcher.
	 * 
	 * @param reflect
	 *            the reflect
	 * @param target
	 *            the target
	 */
	public ServiceDispatcher(Reflect reflect, Object target)
	{
		this(reflect, target, target.getClass());
	}

	/**
	 * Instantiates a new method dispatcher.
	 * 
	 * @param reflect
	 *            the reflect
	 * @param target
	 *            the target
	 * @param iface
	 *            the iface
	 */
	public ServiceDispatcher(Reflect reflect, Object target, Class<?> iface)
	{
		this(reflect, target, MethodSelector.selectRpcServerMethod(iface), iface);
	}

	/**
	 * Instantiates a new method dispatcher.
	 * 
	 * @param reflect
	 *            the reflect
	 * @param target
	 *            the target
	 * @param methods
	 *            the methods
	 * @param interfaceClazz
	 *            the interface clazz
	 */
	public ServiceDispatcher(Reflect reflect, Object target, Method[] methods, Class<?> interfaceClazz)
	{
		this.target = target;
		proxies = new HashMap<String, Invoker>();
		for (Method method : methods)
		{
			proxies.put(method.getName(), reflect.getInvoker(method));
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.agent.service.dispatcher.Dispatcher#dispatch(org.storm.nexus.agent.service.message.Request)
	 */
	@Override
	public void dispatch(Request request) throws Exception
	{

		if (proxies.containsKey(request.getMethodName()))
		{
			Invoker ivk = proxies.get(request.getMethodName());
			ivk.invoke(target, request);
		}
		else
		{
			throw new IOException("Could not find a method with name " + request.getMethodName() + " for interface " + request.getClassName());
		}
	}

}
