/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.netty;

import java.util.Map;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.buffer.ChannelBufferOutputStream;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.buffer.HeapChannelBufferFactory;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.Channels;
import org.storm.nexus.agent.codec.RpcMessageHandler;
import org.storm.nexus.agent.service.config.TcpClientConfig;
import org.storm.nexus.agent.session.Session;

/**
 * The Class NettyTcpClientTransport.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class NettyTcpClientTransport extends PooledStreamClientTransport<Channel, ChannelBufferOutputStream>
{

	private static final String TCP_NO_DELAY = "tcpNoDelay";
	private final ClientBootstrap bootstrap;

	/**
	 * Instantiates a new netty tcp client transport.
	 * 
	 * @param config
	 *            the config
	 * @param session
	 *            the session
	 * @param loop
	 *            the loop
	 */
	NettyTcpClientTransport(TcpClientConfig config, Session session, NettyEventLoop loop)
	{
		// TODO check session.getAddress() instanceof IPAddress
		super(config, session);

		RpcMessageHandler handler = new RpcMessageHandler(session);

		bootstrap = new ClientBootstrap(loop.getClientFactory());
		bootstrap.setPipelineFactory(new StreamPipelineFactory(loop.getMessagePack(), handler));
		Map<String, Object> options = config.getOptions();
		setIfNotPresent(options, TCP_NO_DELAY, Boolean.TRUE, bootstrap);
		bootstrap.setOptions(options);
	}

	/** The connect listener. */
	private final ChannelFutureListener connectListener = new ChannelFutureListener()
	{
		@Override
		public void operationComplete(ChannelFuture future) throws Exception
		{
			if (!future.isSuccess())
			{
				onConnectFailed(future.getChannel(), future.getCause());
				return;
			}
			Channel c = future.getChannel();
			c.getCloseFuture().addListener(closeListener);
			onConnected(c);
		}
	};

	/** The close listener. */
	private final ChannelFutureListener closeListener = new ChannelFutureListener()
	{
		@Override
		public void operationComplete(ChannelFuture future) throws Exception
		{
			Channel c = future.getChannel();
			onClosed(c);
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.netty.PooledStreamClientTransport#startConnection()
	 */
	@Override
	protected void startConnection()
	{
		ChannelFuture f = bootstrap.connect(session.getAddress().getSocketAddress());
		f.addListener(connectListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.netty.PooledStreamClientTransport#newPendingBuffer
	 * ()
	 */
	@Override
	protected ChannelBufferOutputStream newPendingBuffer()
	{
		return new ChannelBufferOutputStream(ChannelBuffers.dynamicBuffer(HeapChannelBufferFactory.getInstance()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.netty.PooledStreamClientTransport#resetPendingBuffer
	 * (java.io.OutputStream)
	 */
	@Override
	protected void resetPendingBuffer(ChannelBufferOutputStream b)
	{
		b.buffer().clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.netty.PooledStreamClientTransport#flushPendingBuffer
	 * (java.io.OutputStream, java.lang.Object)
	 */
	@Override
	protected void flushPendingBuffer(ChannelBufferOutputStream b, Channel c)
	{
		Channels.write(c, b.buffer());
		b.buffer().clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.netty.PooledStreamClientTransport#closePendingBuffer
	 * (java.io.OutputStream)
	 */
	@Override
	protected void closePendingBuffer(ChannelBufferOutputStream b)
	{
		b.buffer().clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.netty.PooledStreamClientTransport#sendMessageChannel
	 * (java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void sendMessageChannel(Channel c, Object msg)
	{
		Channels.write(c, msg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.netty.PooledStreamClientTransport#closeChannel(
	 * java.lang.Object)
	 */
	@Override
	protected void closeChannel(Channel c)
	{
		c.close();
	}

	/**
	 * Sets the if not present.
	 * 
	 * @param options
	 *            the options
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 * @param bootstrap
	 *            the bootstrap
	 */
	private static void setIfNotPresent(Map<String, Object> options, String key, Object value, ClientBootstrap bootstrap)
	{
		if (!options.containsKey(key))
		{
			bootstrap.setOption(key, value);
		}
	}
}
