/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.dispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.nexus.agent.service.message.Request;

/**
 * The Class StopWatchDispatcher.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StopWatchDispatcher implements Dispatcher
{
	private final static Logger logger = LoggerFactory.getLogger(StopWatchDispatcher.class);
	private Dispatcher innerDispatcher;
	private boolean verbose = true;

	/**
	 * Checks if is verbose.
	 * 
	 * @return true, if is verbose
	 */
	public boolean isVerbose()
	{
		return verbose;
	}

	/**
	 * Sets the verbose.
	 * 
	 * @param verbose
	 *            the new verbose
	 */
	public void setVerbose(boolean verbose)
	{
		this.verbose = verbose;
	}

	/**
	 * Instantiates a new stop watch dispatcher.
	 * 
	 * @param inner
	 *            the inner
	 */
	public StopWatchDispatcher(Dispatcher inner)
	{
		this.innerDispatcher = inner;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.dispatcher.Dispatcher#dispatch(org.storm
	 * .nexus.agent.service.proxy.Request)
	 */
	@Override
	public void dispatch(Request request) throws Exception
	{
		if (verbose)
		{
			logger.info(String.format("Begin dispatching %s with args %s", request.getMethodName(), request.getArguments().toString()));
		}
		long start = System.currentTimeMillis();
		try
		{
			innerDispatcher.dispatch(request);
			long diff = System.currentTimeMillis() - start;
			logger.info(String.format("Dispatch %s in %s msecs", request.getMethodName(), diff));
		}
		catch (Exception e)
		{
			long diff = System.currentTimeMillis() - start;
			logger.info(String.format("%s : %s while dispatching %s,(in %s msecs)", e.getClass().getSimpleName(), e.getMessage(), request.getMethodName(), diff));
			throw e;
		}
	}
}
