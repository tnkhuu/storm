/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.msgpack.type.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.config.ClientConfig;
import org.storm.nexus.agent.service.config.ServerConfig;
import org.storm.nexus.agent.service.config.TcpServerConfig;
import org.storm.nexus.agent.service.dispatcher.DefaultDispatcherBuilder;
import org.storm.nexus.agent.service.dispatcher.Dispatcher;
import org.storm.nexus.agent.service.dispatcher.ServiceDispatcherBuilder;
import org.storm.nexus.agent.service.message.Request;
import org.storm.nexus.agent.service.proxy.MessageSendable;
import org.storm.nexus.agent.session.IPAddress;
import org.storm.nexus.agent.session.SessionPool;
import org.storm.nexus.api.exception.RPCException;

/**
 * The Class Server.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Server extends SessionPool
{


	private final static Logger s_log = LoggerFactory.getLogger(Server.class);
	private ServerTransport stran;
	protected Map<String, Dispatcher> dispatchers = new HashMap<>();
	private ServiceDispatcherBuilder dispatcherBuilder = new DefaultDispatcherBuilder();

	/**
	 * Instantiates a new server.
	 */
	public Server()
	{
		super();
	}

	/**
	 * Instantiates a new server.
	 * 
	 * @param config
	 *            the config
	 */
	public Server(ClientConfig config)
	{
		super(config);
	}

	/**
	 * Instantiates a new server.
	 * 
	 * @param loop
	 *            the loop
	 */
	public Server(EventLoop loop)
	{
		super(loop);
	}

	/**
	 * Instantiates a new server.
	 * 
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 */
	public Server(ClientConfig config, EventLoop loop)
	{
		super(config, loop);
	}

	/**
	 * Gets the dispatcher builder.
	 * 
	 * @return the dispatcher builder
	 */
	public ServiceDispatcherBuilder getDispatcherBuilder()
	{
		return dispatcherBuilder;
	}

	/**
	 * Sets the dispatcher builder.
	 * 
	 * @param dispatcherBuilder
	 *            the new dispatcher builder
	 */
	public void setDispatcherBuilder(ServiceDispatcherBuilder dispatcherBuilder)
	{
		this.dispatcherBuilder = dispatcherBuilder;
	}

	/**
	 * Serve.
	 * 
	 * @param dp
	 *            the dp
	 */
	public Server serve(Class<?> iface, Dispatcher dp)
	{
		dispatchers.put(iface.getName(), dp);
		return this;
	}

	/**
	 * Dynamically add new rpc services to support.
	 * 
	 * @param iface
	 * @param impl
	 * @return
	 */
	public Server add(Class<?> iface, Object impl)
	{
		Dispatcher dp = dispatcherBuilder.build(iface, impl, this.getEventLoop().getMessagePack());
		dispatchers.put(iface.getName(), dp);
		return this;
	}

	/**
	 * Remove registered rpc services.
	 * 
	 * @param className
	 */
	public void remove(String className)
	{
		dispatchers.remove(className);
	}

	/**
	 * Checks if theres a service with name <code>className</code> already
	 * registered.
	 * 
	 * @param className
	 */
	public boolean contains(String className)
	{
		return dispatchers.containsKey(className);
	}

	/**
	 * Serve.
	 * 
	 * @param handler
	 *            the handler
	 */
	public Server serve(Class<?> iface, Object handler)
	{
		Dispatcher dp = dispatcherBuilder.build(iface, handler, this.getEventLoop().getMessagePack());
		dispatchers.put(iface.getName(), dp);
		return this;
	}

	/**
	 * Listen.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @throws UnknownHostException
	 *             the unknown host exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void listen(String host, int port) throws UnknownHostException, IOException
	{
		listen(new TcpServerConfig(new IPAddress(host, port)));
	}

	/**
	 * Listen.
	 * 
	 * @param address
	 *            the address
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void listen(InetSocketAddress address) throws IOException
	{
		listen(new TcpServerConfig(new IPAddress(address)));
	}

	/**
	 * Listen.
	 * 
	 * @param port
	 *            the port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void listen(int port) throws IOException
	{
		listen(new TcpServerConfig(new IPAddress(port)));
	}

	/**
	 * Listen.
	 * 
	 * @param config
	 *            the config
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void listen(ServerConfig config) throws IOException
	{
		stran = getEventLoop().listenTransport(config, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.agent.session.SessionPool#close()
	 */
	@Override
	public void close()
	{
		if (stran != null)
		{
			dispatchers.clear();
			stran.close();
		}
		super.close();
	}

	/**
	 * On request.
	 * 
	 * @param channel
	 *            the channel
	 * @param msgid
	 *            the msgid
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	public void onRequest(MessageSendable channel, int msgid, String className, String method, Value args)
	{
		if (dispatchers.containsKey(className))
		{
			Request request = new Request(channel, msgid, className, method, args);
			Dispatcher dp = dispatchers.get(className);
			try
			{
				dp.dispatch(request);
			}
			catch (RPCException e)
			{
				request.sendError(e.getCode(), e);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				s_log.error("Unexpected error occured while invoking method " + method + " on class " + className, e);
				if (e.getMessage() == null)
				{
					request.sendError("");
				}
				else
				{
					request.sendError(e.getMessage());
				}
			}
		}
		else
		{
			throw new IllegalStateException("No proxy by the name of " + className + " exists.");
		}
	}

	/**
	 * On notify.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	public void onNotify(String className, String method, Value args)
	{
		if (dispatchers.containsKey(className))
		{
			Request request = new Request(className, method, args);
			Dispatcher dp = dispatchers.get(className);
			try
			{
				dp.dispatch(request);
			}
			catch (Exception e)
			{
				s_log.error("Unexpected error occured while trying to notify class " + className + " method " + method, e);
				if (e.getMessage() == null)
				{
					request.sendError("");
				}
				else
				{
					request.sendError(e.getMessage());
				}
			}
		}
		else
		{
			throw new IllegalStateException("No proxy by the name of " + className + " exists.");
		}
	}
}
