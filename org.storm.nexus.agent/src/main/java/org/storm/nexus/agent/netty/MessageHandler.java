/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.netty;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.msgpack.type.Value;
import org.storm.nexus.agent.codec.RpcMessageHandler;

/**
 * The Class MessageHandler.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class MessageHandler extends SimpleChannelUpstreamHandler
{
	private RpcMessageHandler handler;
	private ChannelAdaptor adaptor;

	/**
	 * Instantiates a new message handler.
	 * 
	 * @param handler
	 *            the handler
	 */
	MessageHandler(RpcMessageHandler handler)
	{
		this.handler = handler;
	}

	/* (non-Javadoc)
	 * @see org.jboss.netty.channel.SimpleChannelUpstreamHandler#channelOpen(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.ChannelStateEvent)
	 */
	@Override
	public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception
	{
		this.adaptor = new ChannelAdaptor(e.getChannel());
		ctx.sendUpstream(e);
	}

	/* (non-Javadoc)
	 * @see org.jboss.netty.channel.SimpleChannelUpstreamHandler#messageReceived(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.MessageEvent)
	 */
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
	{
		Object m = e.getMessage();
		if (!(m instanceof Value))
		{
			ctx.sendUpstream(e);
			return;
		}

		Value msg = (Value) m;
		handler.handleMessage(adaptor, msg);
	}

	/* (non-Javadoc)
	 * @see org.jboss.netty.channel.SimpleChannelUpstreamHandler#exceptionCaught(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.ExceptionEvent)
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception
	{
		if (ctx.getChannel().isOpen() || ctx.getChannel().isConnected())
		{
			ctx.getChannel().close();
		}
	}
}
