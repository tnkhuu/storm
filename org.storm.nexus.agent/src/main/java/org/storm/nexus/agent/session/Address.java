package org.storm.nexus.agent.session;

import java.net.SocketAddress;

/**
 * The Class Address.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class Address
{

	String HOST_PORT_SEP = ":";

	/**
	 * Gets the socket address.
	 * 
	 * @return the socket address
	 */
	abstract public SocketAddress getSocketAddress();

	/**
	 * 
	 * @return a zookeeper path of this address.
	 */
	abstract public String getPath();
}
