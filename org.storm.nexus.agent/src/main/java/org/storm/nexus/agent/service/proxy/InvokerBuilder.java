/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.service.proxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.msgpack.MessagePack;
import org.msgpack.MessageTypeException;
import org.msgpack.annotation.Ignore;
import org.msgpack.annotation.Index;
import org.msgpack.annotation.Message;
import org.msgpack.annotation.MessagePackMessage;
import org.msgpack.annotation.NotNullable;
import org.msgpack.annotation.Optional;
import org.msgpack.template.FieldOption;

/**
 * The Class InvokerBuilder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class InvokerBuilder
{

	/**
	 * The Class ArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public static class ArgumentEntry
	{
		private int index;
		private Type genericType;
		private FieldOption option;

		/**
		 * Instantiates a new argument entry.
		 */
		public ArgumentEntry()
		{
			this.index = -1;
			this.genericType = null;
			this.option = FieldOption.IGNORE;
		}

		/**
		 * Instantiates a new argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		public ArgumentEntry(ArgumentEntry e)
		{
			this.index = e.index;
			this.genericType = e.genericType;
			this.option = e.option;
		}

		/**
		 * Instantiates a new argument entry.
		 * 
		 * @param index
		 *            the index
		 * @param genericType
		 *            the generic type
		 * @param option
		 *            the option
		 */
		public ArgumentEntry(int index, Type genericType, FieldOption option)
		{
			this.index = index;
			this.genericType = genericType;
			this.option = option;
		}

		/**
		 * Gets the index.
		 * 
		 * @return the index
		 */
		public int getIndex()
		{
			return index;
		}

		/**
		 * Gets the type.
		 * 
		 * @return the type
		 */
		public Class<?> getType()
		{
			if (genericType instanceof ParameterizedType)
			{
				return (Class<?>) ((ParameterizedType) genericType).getRawType();
			}
			else
			{
				return (Class<?>) genericType;
			}
		}

		/**
		 * Gets the java type name.
		 * 
		 * @return the java type name
		 */
		public String getJavaTypeName()
		{
			Class<?> type = getType();
			if (type.isArray())
			{
				return arrayTypeToString(type);
			}
			else
			{
				return type.getName();
			}
		}

		/**
		 * Gets the generic type.
		 * 
		 * @return the generic type
		 */
		public Type getGenericType()
		{
			return genericType;
		}

		/**
		 * Gets the option.
		 * 
		 * @return the option
		 */
		public FieldOption getOption()
		{
			return option;
		}

		/**
		 * Checks if is available.
		 * 
		 * @return true, if is available
		 */
		public boolean isAvailable()
		{
			return option != FieldOption.IGNORE;
		}

		/**
		 * Checks if is required.
		 * 
		 * @return true, if is required
		 */
		public boolean isRequired()
		{
			return option == FieldOption.NOTNULLABLE;
		}

		/**
		 * Checks if is optional.
		 * 
		 * @return true, if is optional
		 */
		public boolean isOptional()
		{
			return option == FieldOption.OPTIONAL;
		}

		/**
		 * Checks if is nullable.
		 * 
		 * @return true, if is nullable
		 */
		public boolean isNullable()
		{
			return option != FieldOption.NOTNULLABLE;
		}

		/**
		 * Array type to string.
		 * 
		 * @param type
		 *            the type
		 * @return the string
		 */
		static String arrayTypeToString(Class<?> type)
		{
			int dim = 1;
			Class<?> baseType = type.getComponentType();
			while (baseType.isArray())
			{
				baseType = baseType.getComponentType();
				dim += 1;
			}
			StringBuilder sb = new StringBuilder();
			sb.append(baseType.getName());
			for (int i = 0; i < dim; ++i)
			{
				sb.append("[]");
			}
			return sb.toString();
		}
	}

	/**
	 * Builds the invoker.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @param entries
	 *            the entries
	 * @param async
	 *            the async
	 * @return the invoker
	 */
	public abstract Invoker buildInvoker(Method targetMethod, ArgumentEntry[] entries, boolean async);

	/**
	 * Builds the invoker.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @param implicitOption
	 *            the implicit option
	 * @return the invoker
	 */
	public Invoker buildInvoker(Method targetMethod, FieldOption implicitOption)
	{
		checkValidation(targetMethod);
		boolean async = isAsyncMethod(targetMethod);
		return buildInvoker(targetMethod, readArgumentEntries(targetMethod, implicitOption, async), async);
	}

	/**
	 * Builds the invoker.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @return the invoker
	 */
	public Invoker buildInvoker(Method targetMethod)
	{
		checkValidation(targetMethod);
		FieldOption implicitOption = readImplicitFieldOption(targetMethod);
		boolean async = isAsyncMethod(targetMethod);
		return buildInvoker(targetMethod, readArgumentEntries(targetMethod, implicitOption, async), async);
	}

	/**
	 * Select default invoker builder.
	 * 
	 * @param messagePack
	 *            the message pack
	 * @return the invoker builder
	 */
	@SuppressWarnings("unused")
	private static InvokerBuilder selectDefaultInvokerBuilder(MessagePack messagePack)
	{
		return new ReflectionInvokerBuilder(messagePack);
	}

	/**
	 * Checks if is async method.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @return true, if is async method
	 */
	static boolean isAsyncMethod(Method targetMethod)
	{

		return Future.class.isAssignableFrom(targetMethod.getReturnType());
	}

	/**
	 * Check validation.
	 * 
	 * @param targetMethod
	 *            the target method
	 */
	private static void checkValidation(Method targetMethod)
	{
	}

	/**
	 * Read argument entries.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @param async
	 *            the async
	 * @return the argument entry[]
	 */
	static ArgumentEntry[] readArgumentEntries(Method targetMethod, boolean async)
	{
		FieldOption implicitOption = readImplicitFieldOption(targetMethod);
		return readArgumentEntries(targetMethod, implicitOption, async);
	}

	/**
	 * Read argument entries.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @param implicitOption
	 *            the implicit option
	 * @param async
	 *            the async
	 * @return the argument entry[]
	 */
	static ArgumentEntry[] readArgumentEntries(Method targetMethod, FieldOption implicitOption, boolean async)
	{
		Type[] types = targetMethod.getGenericParameterTypes();
		Annotation[][] annotations = targetMethod.getParameterAnnotations();

		int paramsOffset = 0;
		/*
		 * if (async) { paramsOffset = 1; }
		 */

		List<ArgumentEntry> indexed = new ArrayList<ArgumentEntry>();
		int maxIndex = -1;
		for (int i = 0 + paramsOffset; i < types.length; i++)
		{
			Type t = types[i];
			Annotation[] as = annotations[i];

			FieldOption opt = readFieldOption(t, as, implicitOption);
			if (opt == FieldOption.IGNORE)
			{
				// skip
				continue;
			}

			int index = readFieldIndex(t, as, maxIndex);

			if ((indexed.size() > index) && (indexed.get(index) != null))
			{
				throw new MessageTypeException("duplicated index: " + index);
			}
			if (index < 0)
			{
				throw new MessageTypeException("invalid index: " + index);
			}

			while (indexed.size() <= index)
			{
				indexed.add(null);
			}
			indexed.set(index, new ArgumentEntry(i, t, opt));

			if (maxIndex < index)
			{
				maxIndex = index;
			}
		}

		ArgumentEntry[] result = new ArgumentEntry[maxIndex + 1];
		for (int i = 0; i < indexed.size(); i++)
		{
			ArgumentEntry e = indexed.get(i);
			if (e == null)
			{
				result[i] = new ArgumentEntry();
			}
			else
			{
				result[i] = e;
			}
		}

		return result;
	}

	/**
	 * Read implicit field option.
	 * 
	 * @param targetMethod
	 *            the target method
	 * @return the field option
	 */
	private static FieldOption readImplicitFieldOption(Method targetMethod)
	{
		MessagePackMessage a = targetMethod.getAnnotation(MessagePackMessage.class);
		if (a == null)
		{
			Message b = targetMethod.getAnnotation(Message.class);
			if (b != null)
			{
				return b.value();
			}
			else
			{
				return FieldOption.DEFAULT;
			}
		}
		return a.value();
	}

	/**
	 * Read field option.
	 * 
	 * @param type
	 *            the type
	 * @param as
	 *            the as
	 * @param implicitOption
	 *            the implicit option
	 * @return the field option
	 */
	private static FieldOption readFieldOption(Type type, Annotation[] as, FieldOption implicitOption)
	{
		if (isAnnotated(as, Ignore.class))
		{
			return FieldOption.IGNORE;
		}
		else if (isAnnotated(as, NotNullable.class))
		{
			return FieldOption.NOTNULLABLE;
		}
		else if (isAnnotated(as, Optional.class))
		{
			return FieldOption.OPTIONAL;
		}
		else
		{
			if ((type instanceof Class<?>) && ((Class<?>) type).isPrimitive())
			{
				return FieldOption.NOTNULLABLE;
			}
			else
			{
				return implicitOption;
			}
		}
	}

	/**
	 * Read field index.
	 * 
	 * @param type
	 *            the type
	 * @param as
	 *            the as
	 * @param maxIndex
	 *            the max index
	 * @return the int
	 */
	private static int readFieldIndex(Type type, Annotation[] as, int maxIndex)
	{
		Index a = getAnnotation(as, Index.class);
		if (a == null)
		{
			return maxIndex + 1;
		}
		else
		{
			return a.value();
		}
	}

	/**
	 * Checks if is annotated.
	 * 
	 * @param array
	 *            the array
	 * @param with
	 *            the with
	 * @return true, if is annotated
	 */
	private static boolean isAnnotated(Annotation[] array, Class<? extends Annotation> with)
	{
		return getAnnotation(array, with) != null;
	}

	/**
	 * Gets the annotation.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param array
	 *            the array
	 * @param key
	 *            the key
	 * @return the annotation
	 */
	@SuppressWarnings("unchecked")
	private static <T extends Annotation> T getAnnotation(Annotation[] array, Class<T> key)
	{
		for (Annotation a : array)
		{
			if (key.isInstance(a))
			{
				return (T) a;
			}
		}
		return null;
	}
}
