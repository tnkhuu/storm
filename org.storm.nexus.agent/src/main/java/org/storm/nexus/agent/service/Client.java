/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service;

import java.io.Closeable;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.config.ClientConfig;
import org.storm.nexus.agent.service.config.TcpClientConfig;
import org.storm.nexus.agent.service.proxy.Reflect;
import org.storm.nexus.agent.session.Address;
import org.storm.nexus.agent.session.IPAddress;
import org.storm.nexus.agent.session.Session;

/**
 * The Class Client.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Client extends Session implements Closeable
{

	private ScheduledFuture<?> timer;

	/**
	 * Instantiates a new client.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public Client(String host, int port) throws UnknownHostException
	{
		this(new IPAddress(host, port), new TcpClientConfig(), EventLoop.defaultEventLoop());
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @param config
	 *            the config
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public Client(String host, int port, ClientConfig config) throws UnknownHostException
	{
		this(new IPAddress(host, port), config, EventLoop.defaultEventLoop());
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @param loop
	 *            the loop
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public Client(String host, int port, EventLoop loop) throws UnknownHostException
	{
		this(new IPAddress(host, port), new TcpClientConfig(), loop);
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @param loop
	 *            the loop
	 * @param reflect
	 *            the reflect
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public Client(String host, int port, EventLoop loop, Reflect reflect) throws UnknownHostException
	{
		this(new IPAddress(host, port), new TcpClientConfig(), loop, reflect);
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public Client(String host, int port, ClientConfig config, EventLoop loop) throws UnknownHostException
	{
		this(new IPAddress(host, port), config, loop);
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param address
	 *            the address
	 */
	public Client(InetSocketAddress address)
	{
		this(new IPAddress(address), new TcpClientConfig(), EventLoop.defaultEventLoop());
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param address
	 *            the address
	 * @param config
	 *            the config
	 */
	public Client(InetSocketAddress address, ClientConfig config)
	{
		this(new IPAddress(address), config, EventLoop.defaultEventLoop());
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param address
	 *            the address
	 * @param loop
	 *            the loop
	 */
	public Client(InetSocketAddress address, EventLoop loop)
	{
		this(new IPAddress(address), new TcpClientConfig(), loop);
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param address
	 *            the address
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 */
	public Client(InetSocketAddress address, ClientConfig config, EventLoop loop)
	{
		this(new IPAddress(address), config, loop);
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param address
	 *            the address
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 * @param reflect
	 *            the reflect
	 */
	public Client(InetSocketAddress address, ClientConfig config, EventLoop loop, Reflect reflect)
	{
		this(new IPAddress(address), config, loop, reflect);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (!(obj instanceof Client))
		{
			return false;
		}
		Client comparator = (Client) obj;
		return (getPath().equals(comparator.getPath()));
	};

	@Override
	public int hashCode()
	{
		return getPath().hashCode();
	};

	/**
	 * Instantiates a new client.
	 * 
	 * @param address
	 *            the address
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 */
	Client(Address address, ClientConfig config, EventLoop loop)
	{
		super(address, config, loop);
		startTimer();
	}

	/**
	 * Instantiates a new client.
	 * 
	 * @param address
	 *            the address
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 * @param reflect
	 *            the reflect
	 */
	Client(Address address, ClientConfig config, EventLoop loop, Reflect reflect)
	{
		super(address, config, loop, reflect);
		startTimer();
	}

	/**
	 * 
	 * @return the zookeeper friendly path of this client.
	 */
	public String getPath()
	{
		return address.getPath();
	}

	/**
	 * Start timer.
	 */
	private void startTimer()
	{
		Runnable command = new Runnable()
		{
			@Override
			public void run()
			{
				stepTimeout();
			}
		};
		timer = loop.getScheduledExecutor().scheduleAtFixedRate(command, 1000, 1000, TimeUnit.MILLISECONDS);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close()
	{
		timer.cancel(false);
		closeSession();
	}
	
	@Override
	public String toString()
	{
		
		return getPath();
	}
}
