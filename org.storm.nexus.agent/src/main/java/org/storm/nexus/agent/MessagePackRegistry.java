/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent;


import org.msgpack.MessagePack;
import org.storm.api.cloud.AvailabilityZone;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.cloud.InstanceSummary;
import org.storm.api.cloud.Region;
import org.storm.api.network.ChainType;
import org.storm.api.network.Protocol;
import org.storm.api.network.TableType;
import org.storm.api.security.Credentials;
import org.storm.api.server.Load;
import org.storm.api.services.vm.ContainerOptions;
import org.storm.api.services.vm.ContainerState;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.api.services.vm.InstanceType;
import org.storm.nexus.api.Role;

/**
 * The Class MessagePackRegistry.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MessagePackRegistry
{

	/**
	 * Register classes.
	 * 
	 * @param msgpack
	 *            the msgpack
	 */
	public static void registerClasses(MessagePack msgpack) {
		msgpack.register(Role.class);
		msgpack.register(HypervisorDriver.class);
		msgpack.register(HypervisorType.class);
		msgpack.register(ContainerState.class);
		msgpack.register(Load.class);
		msgpack.register(ChainType.class);
		msgpack.register(Protocol.class);
		msgpack.register(TableType.class);
		msgpack.register(InstanceType.class);
		msgpack.register(ContainerOptions.class);
		msgpack.register(CloudProvider.class);
		msgpack.register(Region.class);
		msgpack.register(AvailabilityZone.class, new AvailabilityZoneTemplate());
		msgpack.register(Credentials.class, new CredentialsTemplate());
		msgpack.register(InstanceSummary.class, new InstanceSummaryTemplate());
	}
}
