/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.msgpack.type.Value;
import org.msgpack.type.ValueFactory;
import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.ClientTransport;
import org.storm.nexus.agent.service.config.ClientConfig;
import org.storm.nexus.agent.service.message.Future;
import org.storm.nexus.agent.service.message.FutureImpl;
import org.storm.nexus.agent.service.message.NotifyMessage;
import org.storm.nexus.agent.service.message.RequestMessage;
import org.storm.nexus.agent.service.proxy.Reflect;

/**
 * The Class Session.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Session
{

	protected Address address;
	protected EventLoop loop;
	private ClientTransport transport;
	private Reflect reflect;
	private int requestTimeout;
	private AtomicInteger seqid = new AtomicInteger(0);
	private Map<Integer, FutureImpl> reqtable = new HashMap<Integer, FutureImpl>();

	/**
	 * Instantiates a new session.
	 * 
	 * @param address
	 *            the address
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 */
	public Session(Address address, ClientConfig config, EventLoop loop)
	{
		this(address, config, loop, new Reflect(loop.getMessagePack()));
	}

	/**
	 * Instantiates a new session.
	 * 
	 * @param address
	 *            the address
	 * @param config
	 *            the config
	 * @param loop
	 *            the loop
	 * @param reflect
	 *            the reflect
	 */
	public Session(Address address, ClientConfig config, EventLoop loop, Reflect reflect)
	{
		this.address = address;
		this.loop = loop;
		this.requestTimeout = config.getRequestTimeout();
		this.transport = loop.openTransport(config, this);
		this.reflect = reflect;
	}

	/**
	 * Proxy.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param iface
	 *            the iface
	 * @return the t
	 */
	public <T> T proxy(Class<T> iface)
	{
		return reflect.getProxy(iface).newProxyInstance(this);
	}

	/**
	 * Gets the address.
	 * 
	 * @return the address
	 */
	public Address getAddress()
	{
		return address;
	}

	/**
	 * Gets the event loop.
	 * 
	 * @return the event loop
	 */
	public EventLoop getEventLoop()
	{
		return loop;
	}

	/**
	 * Timeout seconds.
	 * 
	 * @return the request timeout
	 */
	public int getRequestTimeout()
	{
		return requestTimeout;
	}

	/**
	 * Sets the request timeout.
	 * 
	 * @param requestTimeout
	 *            the new request timeout
	 */
	public void setRequestTimeout(int requestTimeout)
	{
		this.requestTimeout = requestTimeout;
	}

	/**
	 * Call apply.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 * @return the value
	 */
	public Value callApply(String className, String method, Object[] args)
	{
		Future<Value> f = sendRequest(className, method, args);
		while (true)
		{
			try
			{
				if (requestTimeout <= 0)
				{
					return f.get();
				}
				else
				{
					return f.get(requestTimeout, TimeUnit.SECONDS);
				}
			}
			catch (InterruptedException e)
			{

			}
			catch (TimeoutException e)
			{

				throw new RuntimeException("Time out to call method:" + method, e);

			}
		}
	}

	/**
	 * Call async apply.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 * @return the future
	 */
	public Future<Value> callAsyncApply(String className, String method, Object[] args)
	{
		return sendRequest(className, method, args);
	}

	/**
	 * Notify apply.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	public void notifyApply(String className, String method, Object[] args)
	{
		sendNotify(className, method, args);
	}

	/**
	 * Send request.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 * @return the future
	 */
	public Future<Value> sendRequest(String className, String method, Object[] args)
	{
		int msgid = seqid.getAndAdd(1);
		RequestMessage msg = new RequestMessage(msgid, className, method, args);
		FutureImpl f = new FutureImpl(this);

		synchronized (reqtable)
		{
			reqtable.put(msgid, f);
		}
		transport.sendMessage(msg);

		return new Future<Value>(loop.getMessagePack(), f);
	}

	/**
	 * Send notify.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	public void sendNotify(String className, String method, Object[] args)
	{
		NotifyMessage msg = new NotifyMessage(className, method, args);
		transport.sendMessage(msg);
	}

	/**
	 * Close session.
	 */
	public void closeSession()
	{
		transport.close();
		synchronized (reqtable)
		{
			for (Map.Entry<Integer, FutureImpl> pair : reqtable.entrySet())
			{
				FutureImpl f = pair.getValue();
				f.setResult(null, ValueFactory.createRawValue("session closed"));
			}
			reqtable.clear();
		}
	}

	/**
	 * Transport connect failed.
	 */
	public void transportConnectFailed()
	{

	}

	/**
	 * On response.
	 * 
	 * @param msgid
	 *            the msgid
	 * @param result
	 *            the result
	 * @param error
	 *            the error
	 */
	public void onResponse(int msgid, Value result, Value error)
	{
		FutureImpl f;
		synchronized (reqtable)
		{
			f = reqtable.remove(msgid);
		}
		if (f == null)
		{
			return;
		}
		f.setResult(result, error);
	}

	/**
	 * Step timeout.
	 */
	public void stepTimeout()
	{
		List<FutureImpl> timedout = new ArrayList<FutureImpl>();
		synchronized (reqtable)
		{
			for (Iterator<Map.Entry<Integer, FutureImpl>> it = reqtable.entrySet().iterator(); it.hasNext();)
			{
				Map.Entry<Integer, FutureImpl> pair = it.next();
				FutureImpl f = pair.getValue();
				if (f.stepTimeout())
				{
					it.remove();
					timedout.add(f);
				}
			}
		}
		for (FutureImpl f : timedout)
		{
			f.setResult(null, ValueFactory.createRawValue("timedout"));
		}
	}
}
