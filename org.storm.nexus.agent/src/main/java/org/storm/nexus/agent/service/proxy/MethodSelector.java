/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.service.proxy;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class MethodSelector.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unchecked")
public class MethodSelector
{

	/**
	 * Select rpc server method.
	 * 
	 * @param iface
	 *            the iface
	 * @return the method[]
	 */
	@SuppressWarnings("rawtypes")
	public static Method[] selectRpcServerMethod(Class<?> iface)
	{
		List<Method> methods = new ArrayList();
		for (Method method : iface.getDeclaredMethods())
		{
			if (isRpcServerMethod(method))
			{
				methods.add(method);
			}
		}
		return methods.toArray(new Method[0]);
	}

	/**
	 * Select rpc client method.
	 * 
	 * @param iface
	 *            the iface
	 * @return the method[]
	 */
	@SuppressWarnings("rawtypes")
	public static Method[] selectRpcClientMethod(Class<?> iface)
	{
		List<Method> methods = new ArrayList();
		for (Method method : iface.getMethods())
		{
			if (isRpcClientMethod(method))
			{
				methods.add(method);
			}
		}
		return methods.toArray(new Method[0]);
	}

	/**
	 * Checks if is rpc server method.
	 * 
	 * @param method
	 *            the method
	 * @return true, if is rpc server method
	 */
	public static boolean isRpcServerMethod(Method method)
	{
		return isRpcMethod(method); // && !ProxyBuilder.isAsyncMethod(method);
	}

	/**
	 * Checks if is rpc client method.
	 * 
	 * @param method
	 *            the method
	 * @return true, if is rpc client method
	 */
	public static boolean isRpcClientMethod(Method method)
	{
		return isRpcMethod(method);
	}

	/**
	 * Checks if is rpc method.
	 * 
	 * @param method
	 *            the method
	 * @return true, if is rpc method
	 */
	private static boolean isRpcMethod(Method method)
	{

		int mod = method.getModifiers();
		if (Modifier.isStatic(mod))
		{
			return false;
		}

		if (!Modifier.isPublic(mod))
		{
			return false;
		}

		return true;
	}

}
