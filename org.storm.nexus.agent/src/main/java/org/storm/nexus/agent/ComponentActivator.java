/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.msgpack.template.Template;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.event.Events;
import org.storm.api.event.StormEvent;
import org.storm.api.kernel.Actor;
import org.storm.api.services.ServiceGateway;
import org.storm.nexus.agent.service.ServiceGatewayImpl;
import org.storm.nexus.api.Agent;
import org.storm.nexus.api.Instance;
import org.storm.nexus.api.Role;
import org.storm.nexus.api.Services;
import org.storm.nexus.io.InstanceTemplate;

/**
 * The Nexus Agent Activator and Bundle Bootstrapper.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator extends Actor implements BundleActivator
{
	public static final String RPCSERVER_ENABLED = "rpcserver.enabled";
	private static final Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);
	private NexusAgent agent;
	private ServiceGateway serviceGateway;
	private Instance agentInstance;
	private ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
	private BundleContext context;
	private volatile boolean started = false;
	private volatile boolean callbackFired = false;
	private Object mutex = new Object();

	/**
	 * Start the agent bundle.
	 * 
	 * @param context
	 *            - the bundle context.
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public void start(final BundleContext context) throws Exception
	{
		if (!started)
		{
			synchronized (mutex)
			{
				if (!started)
				{
					started = true;
					if (getRole() == Role.INITIAL)
					{
						Events.registerCallback(StormEvent.SERVICE_ENSEMBLE_READY, context, new EventHandler()
						{

							@Override
							public void handleEvent(Event event)
							{

								if (!callbackFired)
								{
									callbackFired = true;
									String ensembleAddress = (String) event.getProperty(StormEvent.SERVICE_ADDRESS);
									s_log.info("Ensemble Address launched with ip {}", ensembleAddress);
									s_log.info("Connecting agent");
									doStart(context);
								}
							}
						});
					}
					else
					{
						doStart(context);
					}

				}
			}
		}
	}

	/**
	 * Internal do start method.
	 * 
	 * @param context
	 *            the context
	 * @throws NumberFormatException
	 *             the number format exception
	 */
	private void doStart(final BundleContext context) throws NumberFormatException
	{

		String description = context.getProperty(Agent.AGENT_DESCRIPTION);
		String port = context.getProperty(Agent.NEXUS_PORT);
		String rpcPort = context.getProperty(Agent.RPC_PORT);
		Dictionary<String, Object> props = new Hashtable<String, Object>();
		props.put(Services.TEMPLATE_IMPL, Instance.class.getName());
		context.registerService(new String[] { Template.class.getName() }, new InstanceTemplate(), props);
		this.context = context;
		this.agentInstance = new Instance(getHostName(), description, getOutboundAddress(), port == null ? Agent.DEFAULT_NEXUS_PORT : Integer.valueOf(port),
				rpcPort == null ? Agent.DEFAULT_RPC_PORT : Integer.valueOf(rpcPort));

		if (getRole() == Role.DNS || getRole() == Role.INITIAL)
		{
			exec.schedule(new Runnable()
			{

				@Override
				public void run()
				{
					startAgent(context);
				}
			}, 0, TimeUnit.SECONDS);
		}
		else
		{
			startAgent(context);
			s_log.info("Registered {} into the ensemble", agent);

		}
	}

	/**
	 * Start agent.
	 * 
	 * @param context
	 *            the context
	 */
	private void startAgent(final BundleContext context)
	{
		startAgent(context, Role.ENSEMBLE.getDomain(false));
	}

	/**
	 * Start agent.
	 * 
	 * @param context
	 *            the context
	 * @param ensemble
	 *            the ensemble
	 */
	private void startAgent(final BundleContext context, String ensemble)
	{
		agent = new NexusAgent(this.context, agentInstance, getRole());
		agent.init(ensemble);
		serviceGateway = new ServiceGatewayImpl(agent, context);
		agent.setServiceGateway(serviceGateway);
		context.registerService(Agent.class, agent, null);
	}

	/**
	 * Stop the nexus agent bundle.
	 * 
	 * @param context
	 *            the context
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public void stop(BundleContext context) throws Exception
	{
		if (started)
		{
			synchronized (mutex)
			{
				if (started)
				{
					started = false;
					try
					{
						if (agent != null)
						{
							agent.stop(context);
							agent = null;
						}
					}
					catch (Exception e)
					{
						s_log.warn("An error occured while trying to stop the agent. " + e.getMessage());
						if (s_log.isDebugEnabled())
						{
							s_log.debug(e.getMessage(), e);
						}
					}

					agentInstance = null;
				}
			}
		}
	}

	/**
	 * Gets the host name.
	 * 
	 * @return the host name
	 */
	private String getHostName()
	{
		if (getRole() == Role.INITIAL)
		{
			return "localhost";
		}
		try
		{
			return InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e)
		{
			throw new IllegalStateException("Unknown host name");
		}
	}

}
