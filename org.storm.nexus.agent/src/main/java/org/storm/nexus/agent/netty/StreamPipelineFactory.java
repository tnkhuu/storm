/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.netty;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.msgpack.MessagePack;
import org.storm.nexus.agent.codec.MessagePackEncoder;
import org.storm.nexus.agent.codec.MessagePackStreamDecoder;
import org.storm.nexus.agent.codec.RpcMessageHandler;


/**
 * A factory for creating StreamPipeline objects.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class StreamPipelineFactory implements ChannelPipelineFactory
{

	/** The handler. */
	private RpcMessageHandler handler;

	/** The message pack. */
	private MessagePack messagePack;

	/**
	 * Instantiates a new stream pipeline factory.
	 * 
	 * @param messagePack
	 *            the message pack
	 * @param handler
	 *            the handler
	 */
	StreamPipelineFactory(MessagePack messagePack, RpcMessageHandler handler)
	{
		this.handler = handler;
		this.messagePack = messagePack;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jboss.netty.channel.ChannelPipelineFactory#getPipeline()
	 */
	@Override
	public ChannelPipeline getPipeline() throws Exception
	{
		ChannelPipeline p = Channels.pipeline();
		p.addLast("msgpack-decode-stream", new MessagePackStreamDecoder(messagePack));
		p.addLast("msgpack-encode", new MessagePackEncoder(messagePack));
		p.addLast("message", new MessageHandler(handler));
		return p;
	}
}
