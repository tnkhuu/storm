/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.service.config;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class ClientConfig.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class ClientConfig
{

	private Map<String, Object> options = new HashMap<String, Object>();
	protected int requestTimeout = 120;

	/**
	 * Sets the request timeout.
	 * 
	 * @param sec
	 *            the new request timeout
	 */
	public void setRequestTimeout(int sec)
	{
		this.requestTimeout = sec;
	}

	/**
	 * Gets the request timeout.
	 * 
	 * @return the request timeout
	 */
	public int getRequestTimeout()
	{
		return this.requestTimeout;
	}

	/**
	 * Gets the option.
	 * 
	 * @param key
	 *            the key
	 * @return the option
	 */
	public Object getOption(String key)
	{
		return options.get(key);
	}

	/**
	 * Gets the options.
	 * 
	 * @return the options
	 */
	public Map<String, Object> getOptions()
	{
		return options;
	}

	/**
	 * Sets the option.
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public void setOption(String key, Object value)
	{
		options.put(key, value);
	}
}
