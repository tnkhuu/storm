/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.codec;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferOutputStream;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.msgpack.MessagePack;

/**
 * The RPC Message Pack Encoder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class MessagePackEncoder extends OneToOneEncoder
{

	private final int estimatedLength;
	private MessagePack messagePack;

	/**
	 * Instantiates a new message pack encoder.
	 * 
	 * @param messagePack
	 *            the message pack
	 */
	public MessagePackEncoder(MessagePack messagePack)
	{
		this(1024, messagePack);
	}

	/**
	 * Instantiates a new message pack encoder.
	 * 
	 * @param estimatedLength
	 *            the estimated length
	 * @param messagePack
	 *            the message pack
	 */
	public MessagePackEncoder(int estimatedLength, MessagePack messagePack)
	{
		this.estimatedLength = estimatedLength;
		this.messagePack = messagePack;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jboss.netty.handler.codec.oneone.OneToOneEncoder#encode(org.jboss
	 * .netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel,
	 * java.lang.Object)
	 */
	@Override
	protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception
	{
		if (msg instanceof ChannelBuffer)
		{
			return msg;
		}

		ChannelBufferOutputStream out = new ChannelBufferOutputStream(ChannelBuffers.dynamicBuffer(estimatedLength, ctx.getChannel().getConfig().getBufferFactory()));

		messagePack.write(out, msg);

		ChannelBuffer result = out.buffer();
		return result;
	}
}
