/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent;

import java.io.IOException;

import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.template.AbstractTemplate;
import org.msgpack.unpacker.Unpacker;
import org.storm.api.cloud.AvailabilityZone;

/**
 * The Class AvailabilityZoneTemplate.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class AvailabilityZoneTemplate extends AbstractTemplate<AvailabilityZone>
{

	@Override
	public void write(Packer pk, AvailabilityZone target, boolean required) throws IOException
	{
		if (target == null)
		{
			if (required)
			{
				throw new MessageTypeException("Attempted to write null");
			}
			pk.writeNil();
			return;
		}
		String[] zones = target.getZones();
		pk.write(zones.length);
		for(int i = 0; i < zones.length; i++) {
			pk.write(zones[i]);
		}
		
		
		
	}

	@Override
	public AvailabilityZone read(Unpacker u, AvailabilityZone to, boolean required) throws IOException
	{
		if (!required && u.trySkipNil())
		{
			return null;
		}
		int size = u.readInt();
		String[] zones = new String[size];
		for(int i = 0; i < size; i++) {
			zones[i] = u.readString();
		}
		AvailabilityZone provider = new AvailabilityZone(zones);
		return provider;
	}

}
