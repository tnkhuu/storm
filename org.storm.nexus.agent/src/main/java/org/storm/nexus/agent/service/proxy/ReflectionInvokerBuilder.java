/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.proxy;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.concurrent.FutureTask;

import org.msgpack.MessagePack;
import org.msgpack.MessageTypeException;
import org.msgpack.template.Template;
import org.msgpack.type.Value;
import org.msgpack.unpacker.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.nexus.agent.service.message.Request;

/**
 * The Class ReflectionInvokerBuilder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class ReflectionInvokerBuilder extends InvokerBuilder
{

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(ReflectionInvokerBuilder.class);

	/** The message pack. */
	protected MessagePack messagePack;

	/**
	 * Instantiates a new reflection invoker builder.
	 * 
	 * @param messagePack
	 *            the message pack
	 */
	public ReflectionInvokerBuilder(MessagePack messagePack)
	{
		this.messagePack = messagePack;
	}

	/**
	 * The Class ReflectionArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static abstract class ReflectionArgumentEntry extends ArgumentEntry
	{

		/**
		 * Instantiates a new reflection argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		ReflectionArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/**
		 * Convert.
		 * 
		 * @param params
		 *            the params
		 * @param obj
		 *            the obj
		 * @throws MessageTypeException
		 *             the message type exception
		 */
		public abstract void convert(Object[] params, Value obj) throws MessageTypeException;

		/**
		 * Sets the null.
		 * 
		 * @param params
		 *            the new null
		 */
		public void setNull(Object[] params)
		{
			params[getIndex()] = null;
		}
	}

	/**
	 * The Class NullArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class NullArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new null argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		NullArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
		}
	}

	/**
	 * The Class BooleanArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class BooleanArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new boolean argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		BooleanArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			params[getIndex()] = obj.asBooleanValue().getBoolean();
		}
	}

	/**
	 * The Class ByteArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class ByteArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new byte argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		ByteArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			params[getIndex()] = obj.asIntegerValue().getByte();
		}
	}

	/**
	 * The Class ShortArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class ShortArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new short argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		ShortArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			params[getIndex()] = obj.asIntegerValue().getShort();
		}
	}

	/**
	 * The Class IntArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class IntArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new int argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		IntArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			params[getIndex()] = obj.asIntegerValue().getInt();
		}
	}

	/**
	 * The Class LongArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class LongArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new long argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		LongArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			params[getIndex()] = obj.asIntegerValue().getLong();
		}
	}

	/**
	 * The Class FloatArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class FloatArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new float argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		FloatArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			params[getIndex()] = obj.asFloatValue().getFloat();
		}
	}

	/**
	 * The Class DoubleArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class DoubleArgumentEntry extends ReflectionArgumentEntry
	{

		/**
		 * Instantiates a new double argument entry.
		 * 
		 * @param e
		 *            the e
		 */
		DoubleArgumentEntry(ArgumentEntry e)
		{
			super(e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			params[getIndex()] = obj.asFloatValue().getDouble();
		}
	}

	/**
	 * The Class ObjectArgumentEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	@SuppressWarnings("rawtypes")
	static class ObjectArgumentEntry extends ReflectionArgumentEntry
	{

		/** The template. */
		private Template template;

		/** The message pack. */
		private MessagePack messagePack;

		/**
		 * Instantiates a new object argument entry.
		 * 
		 * @param messagePack
		 *            the message pack
		 * @param e
		 *            the e
		 * @param template
		 *            the template
		 */
		ObjectArgumentEntry(MessagePack messagePack, ArgumentEntry e, Template template)
		{
			super(e);
			this.template = template;
			this.messagePack = messagePack;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.storm.nexus.agent.service.proxy.ReflectionInvokerBuilder.
		 * ReflectionArgumentEntry#convert(java.lang.Object[],
		 * org.msgpack.type.Value)
		 */
		@Override
		@SuppressWarnings({ "unchecked" })
		public void convert(Object[] params, Value obj) throws MessageTypeException
		{
			try
			{
				params[getIndex()] = template.read(new Converter(messagePack, obj), null);// messagePack.convert(obj,template);
			}
			catch (IOException e)
			{
				new MessageTypeException(e);
			}
		}
	}

	/**
	 * The Class ReflectionInvoker.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class ReflectionInvoker implements Invoker
	{

		/** The method. */
		protected Method method;

		/** The parameter length. */
		protected int parameterLength;

		/** The entries. */
		protected ReflectionArgumentEntry[] entries;

		/** The minimum array length. */
		protected int minimumArrayLength;

		/** The async. */
		boolean async;

		/**
		 * Instantiates a new reflection invoker.
		 * 
		 * @param method
		 *            the method
		 * @param entries
		 *            the entries
		 * @param async
		 *            the async
		 */
		public ReflectionInvoker(Method method, ReflectionArgumentEntry[] entries, boolean async)
		{
			this.method = method;
			this.parameterLength = method.getParameterTypes().length;
			this.entries = entries;
			this.async = async;
			this.minimumArrayLength = 0;
			for (int i = 0; i < entries.length; i++)
			{
				ReflectionArgumentEntry e = entries[i];
				if (!e.isOptional())
				{// e.isRequired() || e.isNullable()) {
					this.minimumArrayLength = i + 1;
				}
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.nexus.agent.service.proxy.Invoker#invoke(java.lang.Object,
		 * org.storm.nexus.agent.service.proxy.Request)
		 */
		@Override
		@SuppressWarnings("rawtypes")
		public void invoke(Object target, Request request) throws Exception
		{
			Object[] params = new Object[parameterLength];
			if (async)
			{
				params[0] = request;
			}

			try
			{
				Value args = request.getArguments();

				Value[] array = args.asArrayValue().getElementArray();
				int length = array.length;
				if (length < minimumArrayLength)
				{
					throw new MessageTypeException(String.format("Method needs at least %s args.But only %s args are passed", minimumArrayLength, length));
				}

				int i;
				for (i = 0; i < minimumArrayLength; i++)
				{
					ReflectionArgumentEntry e = entries[i];
					if (!e.isAvailable())
					{
						continue;
					}

					Value obj = array[i];
					if (obj.isNilValue())
					{
						if (e.isRequired())
						{
							// Required + nil => exception
							throw new MessageTypeException();
						}
						else if (e.isOptional())
						{
							// Optional + nil => keep default value
						}
						else
						{ // Nullable
							// Nullable + nil => set null
							e.setNull(params);
						}
					}
					else
					{
						try
						{
							e.convert(params, obj);
						}
						catch (MessageTypeException mte)
						{
							logger.error(String.format("Expect Method:%s ArgIndex:%s Type:%s. But passed:%s", request.getMethodName(), i, e.getGenericType(), obj));
							throw new MessageTypeException(String.format("%sth argument type is %s.But wrong type is sent.", i + 1, e.getJavaTypeName()));
						}
					}
				}

				int max = length < entries.length ? length : entries.length;
				for (; i < max; i++)
				{
					ReflectionArgumentEntry e = entries[i];
					if (!e.isAvailable())
					{
						continue;
					}

					Value obj = array[i];
					if (obj.isNilValue())
					{
						// this is Optional field becaue i >= minimumArrayLength
						// Optional + nil => keep default value
					}
					else
					{

						try
						{
							e.convert(params, obj);
						}
						catch (MessageTypeException mte)
						{
							logger.error(String.format("Expect Method:%s ArgIndex:%s Type:%s. But passed:%s", request.getMethodName(), i, e.getGenericType(), obj));
							throw new MessageTypeException(String.format("%sth argument type is %s.But wrong type is sent.", i + 1, e.getJavaTypeName()));
						}
					}
				}

				// latter entries are all Optional + nil => keep default value

			}
			catch (MessageTypeException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				// e.printStackTrace();
				throw e;
			}

			Object result = null;
			try
			{
				result = method.invoke(target, params);
			}
			catch (InvocationTargetException e)
			{
				if ((e.getCause() != null) && (e.getCause() instanceof Exception))
				{
					throw (Exception) e.getCause();
				}
				else
				{
					throw e;
				}
			}
			if (!async)
			{
				request.sendResult(result);
			}
			else
			{
				FutureTask async = (FutureTask) result;
				async.run();
				request.sendResult(async.get());
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.proxy.InvokerBuilder#buildInvoker(java.
	 * lang.reflect.Method,
	 * org.storm.nexus.agent.service.proxy.InvokerBuilder.ArgumentEntry[],
	 * boolean)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public Invoker buildInvoker(Method targetMethod, ArgumentEntry[] entries, boolean async)
	{
		int mod = targetMethod.getModifiers();
		if (!Modifier.isPublic(mod))
		{
			targetMethod.setAccessible(true);
		}

		ReflectionArgumentEntry[] res = new ReflectionArgumentEntry[entries.length];
		for (int i = 0; i < entries.length; i++)
		{
			ArgumentEntry e = entries[i];
			Class<?> type = e.getType();
			if (!e.isAvailable())
			{
				res[i] = new NullArgumentEntry(e);
			}
			else if (type.equals(boolean.class))
			{
				res[i] = new BooleanArgumentEntry(e);
			}
			else if (type.equals(byte.class))
			{
				res[i] = new ByteArgumentEntry(e);
			}
			else if (type.equals(short.class))
			{
				res[i] = new ShortArgumentEntry(e);
			}
			else if (type.equals(int.class))
			{
				res[i] = new IntArgumentEntry(e);
			}
			else if (type.equals(long.class))
			{
				res[i] = new LongArgumentEntry(e);
			}
			else if (type.equals(float.class))
			{
				res[i] = new FloatArgumentEntry(e);
			}
			else if (type.equals(double.class))
			{
				res[i] = new DoubleArgumentEntry(e);
			}
			else
			{

				Type t = e.getGenericType();
				Template tmpl = messagePack.lookup(t);
				if (tmpl == null)
				{
					messagePack.register((Class<?>) t);
					tmpl = messagePack.lookup(t);
				}
				res[i] = new ObjectArgumentEntry(messagePack, e, tmpl);
			}
		}

		return new ReflectionInvoker(targetMethod, res, async);
	}
}
