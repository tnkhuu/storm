/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.proxy;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.msgpack.type.Value;
import org.msgpack.unpacker.Converter;
import org.storm.nexus.agent.service.message.Future;
import org.storm.nexus.agent.session.Session;

/**
 * The Class ReflectionProxyBuilder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ReflectionProxyBuilder extends ProxyBuilder
{

	/**
	 * The Class ReflectionMethodEntry.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class ReflectionMethodEntry
	{
		private String className;

		/** The rpc name. */
		private String rpcName;

		/** The return type template. */
		private Template returnTypeTemplate;

		/** The async. */
		private boolean async;

		/** The argument entries. */
		private InvokerBuilder.ArgumentEntry[] argumentEntries;

		/**
		 * Instantiates a new reflection method entry.
		 * 
		 * @param e
		 *            the e
		 * @param returnTypeTemplate
		 *            the return type template
		 */
		public ReflectionMethodEntry(MethodEntry e, Template returnTypeTemplate)
		{
			this.rpcName = e.getRpcName();
			this.className = e.getClassName();
			this.returnTypeTemplate = returnTypeTemplate;
			this.async = e.isAsync();
			this.argumentEntries = e.getArgumentEntries();
		}

		/**
		 * Gets the rpc name.
		 * 
		 * @return the rpc name
		 */
		public String getRpcName()
		{
			return rpcName;
		}

		/**
		 * 
		 * @return
		 */
		public String getClassName()
		{
			return className;
		}

		/**
		 * Gets the return type template.
		 * 
		 * @return the return type template
		 */
		public Template getReturnTypeTemplate()
		{
			return returnTypeTemplate;
		}

		/**
		 * Checks if is async.
		 * 
		 * @return true, if is async
		 */
		public boolean isAsync()
		{
			return async;
		}

		/**
		 * Sort.
		 * 
		 * @param args
		 *            the args
		 * @return the object[]
		 */
		public Object[] sort(Object[] args)
		{
			Object[] params = new Object[argumentEntries.length];

			for (int i = 0; i < argumentEntries.length; i++)
			{
				InvokerBuilder.ArgumentEntry e = argumentEntries[i];
				if (!e.isAvailable())
				{
					continue;
				}
				if (params.length < e.getIndex())
				{

				}
				if (e.isRequired() && (args[i] == null))
				{

				}
				params[i] = args[e.getIndex()];
			}

			return params;
		}
	}

	/**
	 * The Class ReflectionHandler.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public class ReflectionHandler implements InvocationHandler
	{

		/** The s. */
		private Session s;

		/** The entry map. */
		private Map<Method, ReflectionMethodEntry> entryMap;

		/** The interface that were proxying. */
		private Class<?> iface;

		/**
		 * Instantiates a new reflection handler.
		 * 
		 * @param s
		 *            the s
		 * @param entryMap
		 *            the entry map
		 */
		public ReflectionHandler(Class<?> iface, Session s, Map<Method, ReflectionMethodEntry> entryMap)
		{
			this.iface = iface;
			this.s = s;
			this.entryMap = entryMap;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object,
		 * java.lang.reflect.Method, java.lang.Object[])
		 */
		@Override
		public Object invoke(Object proxy, Method method, Object[] args)
		{
			ReflectionMethodEntry e = entryMap.get(method);
			if (e == null)
			{

			}
			Object[] params = e.sort(args);
			if (e.isAsync())
			{
				Future<Value> f = s.callAsyncApply(e.getClassName(), e.getRpcName(), params);
				return new Future<Object>(messagePack, f, e.getReturnTypeTemplate());
			}
			else
			{
				Value obj = s.callApply(iface.getName(), e.getRpcName(), params);
				if (obj.isNilValue())
				{
					return null;
				}
				else
				{
					Template tmpl = e.getReturnTypeTemplate();
					if (tmpl == null)
					{
						return null;
					}
					try
					{
						return tmpl.read(new Converter(messagePack, obj), null);
						// return messagePack.convert(obj,tmpl);//
						// obj.convert(tmpl);
					}
					catch (IOException e1)
					{
						return null;
					}
				}
			}
		}
	}

	/**
	 * The Class ReflectionProxy.
	 * 
	 * @param <T>
	 *            the generic type
	 * @author Trung Khuu
	 * @since 1.0
	 */
	public class ReflectionProxy<T> implements Proxy<T>
	{

		/** The iface. */
		private Class<T> iface;

		/** The entry map. */
		private Map<Method, ReflectionMethodEntry> entryMap;

		/**
		 * Instantiates a new reflection proxy.
		 * 
		 * @param iface
		 *            the iface
		 * @param entryMap
		 *            the entry map
		 */
		public ReflectionProxy(Class<T> iface, Map<Method, ReflectionMethodEntry> entryMap)
		{
			this.iface = iface;
			this.entryMap = entryMap;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.storm.nexus.agent.service.proxy.Proxy#newProxyInstance(org.storm
		 * .nexus.agent.session.Session)
		 */
		@Override
		public T newProxyInstance(Session s)
		{
			ReflectionHandler handler = new ReflectionHandler(iface, s, entryMap);
			return (T) java.lang.reflect.Proxy.newProxyInstance(iface.getClassLoader(), new Class[] { iface }, handler);
		}
	}

	/** The message pack. */
	private MessagePack messagePack;

	/**
	 * Instantiates a new reflection proxy builder.
	 * 
	 * @param messagePack
	 *            the message pack
	 */
	public ReflectionProxyBuilder(MessagePack messagePack)
	{

		this.messagePack = messagePack;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.proxy.ProxyBuilder#buildProxy(java.lang
	 * .Class, org.storm.nexus.agent.service.proxy.ProxyBuilder.MethodEntry[])
	 */
	@Override
	public <T> Proxy<T> buildProxy(Class<T> iface, MethodEntry[] entries)
	{
		for (MethodEntry e : entries)
		{
			Method method = e.getMethod();
			int mod = method.getModifiers();
			if (!Modifier.isPublic(mod))
			{
				method.setAccessible(true);
			}
		}

		Map<Method, ReflectionMethodEntry> entryMap = new HashMap<Method, ReflectionMethodEntry>();
		for (int i = 0; i < entries.length; i++)
		{
			MethodEntry e = entries[i];
			Template tmpl;
			if (e.isReturnTypeVoid())
			{
				tmpl = null;
			}
			else if (e.isAsync())
			{
				tmpl = messagePack.lookup(e.getGenericReturnType());
			}
			else
			{
				tmpl = messagePack.lookup(e.getGenericReturnType());// TemplateRegistry.lookup(e.getGenericReturnType(),
																	// true);
			}

			entryMap.put(e.getMethod(), new ReflectionMethodEntry(e, tmpl));
		}

		return new ReflectionProxy<T>(iface, entryMap);
	}
}
