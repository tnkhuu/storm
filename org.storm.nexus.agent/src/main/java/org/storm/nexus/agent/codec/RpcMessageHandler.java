/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.codec;

import org.msgpack.type.NilValue;
import org.msgpack.type.Value;
import org.msgpack.type.ValueFactory;
import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.Server;
import org.storm.nexus.agent.service.message.Messages;
import org.storm.nexus.agent.service.proxy.MessageSendable;
import org.storm.nexus.agent.session.Session;

/**
 * The RPC Message Handler.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RpcMessageHandler
{
	protected final Session session;
	protected final Server server;
	protected final EventLoop loop;
	protected boolean useThread = false;

	/**
	 * Instantiates a new rpc message handler.
	 * 
	 * @param session
	 *            the session
	 */
	public RpcMessageHandler(Session session)
	{
		this(session, null);
	}

	/**
	 * Instantiates a new rpc message handler.
	 * 
	 * @param server
	 *            the server
	 */
	public RpcMessageHandler(Server server)
	{
		this(null, server);
	}

	/**
	 * Instantiates a new rpc message handler.
	 * 
	 * @param session
	 *            the session
	 * @param server
	 *            the server
	 */
	public RpcMessageHandler(Session session, Server server)
	{
		this.session = session;
		this.server = server;
		if (session == null)
		{
			this.loop = server.getEventLoop();
		}
		else
		{
			this.loop = session.getEventLoop();
		}
	}

	/**
	 * Use thread.
	 * 
	 * @param value
	 *            the value
	 */
	public void useThread(boolean value)
	{
		useThread = value;
	}

	/**
	 * The Class HandleMessageTask.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	static class HandleMessageTask implements Runnable
	{
		private RpcMessageHandler handler;
		private MessageSendable channel;
		private Value msg;

		/**
		 * Instantiates a new handle message task.
		 * 
		 * @param handler
		 *            the handler
		 * @param channel
		 *            the channel
		 * @param msg
		 *            the msg
		 */
		HandleMessageTask(RpcMessageHandler handler, MessageSendable channel, Value msg)
		{
			this.handler = handler;
			this.channel = channel;
			this.msg = msg;
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			try
			{
				handler.handleMessageImpl(channel, msg);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Handle message.
	 * 
	 * @param channel
	 *            the channel
	 * @param msg
	 *            the msg
	 */
	public void handleMessage(MessageSendable channel, Value msg)
	{
		if (useThread)
		{
			loop.getWorkerExecutor().submit(new HandleMessageTask(this, channel, msg));
		}
		else
		{
			handleMessageImpl(channel, msg);
		}
	}

	/**
	 * Handle message impl.
	 * 
	 * @param channel
	 *            the channel
	 * @param msg
	 *            the msg
	 */
	private void handleMessageImpl(MessageSendable channel, Value msg)
	{
		Value[] array = msg.asArrayValue().getElementArray();

		// TODO check array.length
		int type = array[0].asIntegerValue().getInt();
		if (type == Messages.REQUEST)
		{
			// REQUEST
			int msgid = array[1].asIntegerValue().getInt();
			String className = array[2].asRawValue().getString();
			String method = array[3].asRawValue().getString();
			Value args = null;
			if (array.length > 4)
			{
				args = array[4];
			}
			else
			{
				args = ValueFactory.createArrayValue(new Value[] { NilValue.getInstance() });
			}

			handleRequest(channel, msgid, className, method, args);

		}
		else if (type == Messages.RESPONSE)
		{
			// RESPONSE
			int msgid = array[1].asIntegerValue().getInt();
			Value error = array[2];
			Value result = array[3];
			handleResponse(channel, msgid, result, error);

		}
		else if (type == Messages.NOTIFY)
		{
			// NOTIFY
			String className = array[1].asRawValue().getString();
			String method = array[2].asRawValue().getString();
			Value args = null;
			if (array.length > 3)
			{
				args = array[3];
			}
			else
			{
				args = ValueFactory.createArrayValue(new Value[] { NilValue.getInstance() });
			}
			handleNotify(channel, className, method, args);

		}
		else
		{
			throw new RuntimeException("unknown message type: " + type);
		}
	}

	/**
	 * Handle request.
	 * 
	 * @param channel
	 *            the channel
	 * @param msgid
	 *            the msgid
	 * @param className
	 *            the class name
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	private void handleRequest(MessageSendable channel, int msgid, String className, String method, Value args)
	{
		if (server == null)
		{
			return;
		}
		server.onRequest(channel, msgid, className, method, args);
	}

	/**
	 * Handle notify.
	 * 
	 * @param channel
	 *            the channel
	 * @param className
	 *            the class name
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	private void handleNotify(MessageSendable channel, String className, String method, Value args)
	{
		if (server == null)
		{
			return;
		}
		server.onNotify(className, method, args);
	}

	/**
	 * Handle response.
	 * 
	 * @param channel
	 *            the channel
	 * @param msgid
	 *            the msgid
	 * @param result
	 *            the result
	 * @param error
	 *            the error
	 */
	private void handleResponse(MessageSendable channel, int msgid, Value result, Value error)
	{
		if (session == null)
		{
			return;
		}
		session.onResponse(msgid, result, error);
	}
}
