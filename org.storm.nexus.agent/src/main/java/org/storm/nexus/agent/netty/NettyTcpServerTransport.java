/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.netty;

import java.util.Map;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.storm.nexus.agent.codec.RpcMessageHandler;
import org.storm.nexus.agent.service.Server;
import org.storm.nexus.agent.service.ServerTransport;
import org.storm.nexus.agent.service.config.TcpServerConfig;
import org.storm.nexus.agent.session.Address;

/**
 * The Class NettyTcpServerTransport.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class NettyTcpServerTransport implements ServerTransport
{
	private Channel listenChannel;
	private final static String CHILD_TCP_NODELAY = "child.tcpNoDelay";
	private final static String REUSE_ADDRESS = "reuseAddress";
	private ServerBootstrap bootstrap;

	/**
	 * Instantiates a new netty tcp server transport.
	 * 
	 * @param config
	 *            the config
	 * @param server
	 *            the server
	 * @param loop
	 *            the loop
	 */
	NettyTcpServerTransport(TcpServerConfig config, Server server, NettyEventLoop loop)
	{
		if (server == null)
		{
			throw new IllegalArgumentException("Server must not be null");
		}

		Address address = config.getListenAddress();
		RpcMessageHandler handler = new RpcMessageHandler(server);
		handler.useThread(true);

		bootstrap = new ServerBootstrap(loop.getServerFactory());
		bootstrap.setPipelineFactory(new StreamPipelineFactory(loop.getMessagePack(), handler));
		final Map<String, Object> options = config.getOptions();
		setIfNotPresent(options, CHILD_TCP_NODELAY, Boolean.TRUE, bootstrap);
		setIfNotPresent(options, REUSE_ADDRESS, Boolean.TRUE, bootstrap);
		bootstrap.setOptions(options);
		this.listenChannel = bootstrap.bind(address.getSocketAddress());
	}

	/* (non-Javadoc)
	 * @see org.storm.nexus.agent.service.ServerTransport#close()
	 */
	@Override
	public void close()
	{
		listenChannel.close();
	}

	/**
	 * Sets the if not present.
	 * 
	 * @param options
	 *            the options
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 * @param bootstrap
	 *            the bootstrap
	 */
	private static void setIfNotPresent(Map<String, Object> options, String key, Object value, ServerBootstrap bootstrap)
	{
		if (!options.containsKey(key))
		{
			bootstrap.setOption(key, value);
		}
	}
}
