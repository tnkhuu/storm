/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.codec;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.Channels;
import org.storm.nexus.agent.service.ClientTransport;

/**
 * The Class ChannelAdaptor.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
class ChannelAdaptor implements ClientTransport
{

	private Channel channel;

	/**
	 * Instantiates a new channel adaptor.
	 * 
	 * @param channel
	 *            the channel
	 */
	ChannelAdaptor(Channel channel)
	{
		this.channel = channel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.ClientTransport#sendMessage(java.lang.Object
	 * )
	 */
	@Override
	public void sendMessage(Object msg)
	{
		Channels.write(channel, msg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.agent.service.ClientTransport#close()
	 */
	@Override
	public void close()
	{
		channel.close();
	}
}
