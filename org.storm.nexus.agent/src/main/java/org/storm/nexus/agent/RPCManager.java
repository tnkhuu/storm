/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent;

import java.io.IOException;
import java.net.UnknownHostException;

import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.wiring.BundleWiring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.Server;
import org.storm.nexus.api.Services;

/**
 * The {@link RPCManager} is the coordinator for managing invocation and life
 * cycles of distributed services
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class RPCManager implements ServiceListener
{
	private static final Logger s_log = LoggerFactory.getLogger(RPCManager.class);
	private EventLoop loop;
	private Server server;
	private int rpcPort;
	private MessagePack messagePack;
	private BundleContext context;
	private volatile boolean isEnabled = false;

	/**
	 * Instantiates a new RPC manager.
	 * 
	 * @param port
	 *            the port
	 */
	public RPCManager(BundleContext context, int port)
	{
		try
		{
			this.context = context;
			messagePack = new MessagePack();
			MessagePackRegistry.registerClasses(messagePack);
			rpcPort = port;
			loop = EventLoop.start(messagePack, Runtime.getRuntime().availableProcessors());
			server = new Server(loop);
			server.listen(rpcPort);
			isEnabled = true;
			processRegisteredSerializers(context);
			s_log.info("Started Storm RPC Management Engine @ 0.0.0.0:{}", port);
			
		}
		catch (UnknownHostException e)
		{
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Register a new serializer into the message pack serialization engine.
	 * 
	 * @param reference
	 */
	private void addSerializer(ServiceReference reference)
	{
		String templateClass = (String) reference.getProperty(Services.TEMPLATE_IMPL);
		try
		{
			Class template = reference.getBundle().adapt(BundleWiring.class).getClassLoader().loadClass(templateClass);
			Template impl = (Template) context.getService(reference);
			messagePack.register(template, (Template) impl);
			s_log.info("Registering template {} with impl {}", template, impl);
		}
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}

	}

	/**
	 * Looks for bundles that are offering serialization services for data they
	 * which to provide as a distributed service (i.e. Pojo's with specific
	 * {@link Template}) implementations and registers them to the message pack
	 * registry so they can be identified.
	 * 
	 * @param context
	 */
	private void processRegisteredSerializers(BundleContext context)
	{
		try
		{
			ServiceReference<?>[] services = context.getAllServiceReferences(Template.class.getName(), null);
			if (services != null)
			{
				for (ServiceReference<?> s : services)
				{
					addSerializer(s);
				}
			}
		}
		catch (InvalidSyntaxException e)
		{
			s_log.error(e.getMessage(), e);
		}
	}

	/**
	 * Remove a serializer from the message pack serialization engine.
	 * 
	 * @param reference
	 */
	private void removeSerializer(ServiceReference reference)
	{
		String templateClass = (String) reference.getProperty(Services.TEMPLATE_IMPL);
		try
		{
			Class template = reference.getBundle().adapt(BundleWiring.class).getClassLoader().loadClass(templateClass);
			messagePack.unregister(template);
		}
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * The OSGI callback for a service change event.
	 * 
	 * @param event
	 *            - the event that changed
	 */
	@Override
	public void serviceChanged(ServiceEvent event)
	{
		switch (event.getType())
		{
		case ServiceEvent.REGISTERED:
		{
			addSerializer(event.getServiceReference());
			break;
		}
		case ServiceEvent.UNREGISTERING:
		{
			removeSerializer(event.getServiceReference());
			break;
		}
		}

	}

	/**
	 * Retrieve the RPC server.
	 * 
	 * @return the server
	 */
	public Server getServer()
	{
		return server;
	}

	/**
	 * The RPC port.
	 * 
	 * @return the rpcPort
	 */
	public int getRpcPort()
	{
		return rpcPort;
	}

	/**
	 * Shutdown the RPC server.
	 */
	public void shutdown()
	{
		if (isEnabled)
		{
			synchronized (this)
			{
				if (isEnabled)
				{

					s_log.info("RPC Services are shutting down.");
					server.close();

					isEnabled = false;
					server = null;
					loop = null;
					context = null;
				}

			}
		}

	}

	/**
	 * Dynamically add new RPC services to support.
	 * 
	 * @param iface
	 *            the iface
	 * @param impl
	 *            the impl
	 */
	public void addService(Class<?> iface, Object impl)
	{
		server.add(iface, impl);
	}

	/**
	 * Remove registered RPC services.
	 * 
	 * @param iface
	 *            the iface
	 */
	public void removeService(String iface)
	{
		server.remove(iface);
	}

	/**
	 * Checks if their is a service with name <code>className</code> already
	 * registered.
	 * 
	 * @param className
	 */
	public boolean containsService(String iface)
	{
		return server.contains(iface);
	}

}
