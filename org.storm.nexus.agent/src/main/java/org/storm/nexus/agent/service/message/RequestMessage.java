/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.service.message;

import java.io.IOException;

import org.msgpack.MessagePackable;
import org.msgpack.packer.Packer;
import org.msgpack.unpacker.Unpacker;

/**
 * The Class RequestMessage.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class RequestMessage implements MessagePackable
{

	private int msgid;
	private String className;
	private String method;
	private Object[] args;

	/**
	 * Instantiates a new request message.
	 * 
	 * @param msgid
	 *            the msgid
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	public RequestMessage(int msgid, String className, String method, Object[] args)
	{
		this.className = className;
		this.msgid = msgid;
		this.method = method;
		this.args = args;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msgpack.MessagePackable#writeTo(org.msgpack.packer.Packer)
	 */
	@Override
	public void writeTo(Packer pk) throws IOException
	{
		pk.writeArrayBegin(5);
		pk.write(Messages.REQUEST);
		pk.write(msgid);
		pk.write(className);
		pk.write(method);
		pk.writeArrayBegin(args.length);
		for (Object arg : args)
		{
			pk.write(arg);
		}
		pk.writeArrayEnd();
		pk.writeArrayEnd();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msgpack.MessagePackable#readFrom(org.msgpack.unpacker.Unpacker)
	 */
	@Override
	public void readFrom(Unpacker u) throws IOException
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Message pack.
	 * 
	 * @param pk
	 *            the pk
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void messagePack(Packer pk) throws IOException
	{
		writeTo(pk);
	}
}
