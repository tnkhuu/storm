/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.storm.nexus.agent.service.config;

/**
 * The Class StreamClientConfig.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public abstract class StreamClientConfig extends ClientConfig
{
	protected double connectTimeout = 30;
	protected int reconnectionLimit = 3;

	/**
	 * Gets the connect timeout.
	 * 
	 * @return the connect timeout
	 */
	public double getConnectTimeout()
	{
		return connectTimeout;
	}

	/**
	 * Sets the connect timeout.
	 * 
	 * @param sec
	 *            the new connect timeout
	 */
	public void setConnectTimeout(double sec)
	{
		connectTimeout = sec;
	}

	/**
	 * Gets the reconnection limit.
	 * 
	 * @return the reconnection limit
	 */
	public int getReconnectionLimit()
	{
		return reconnectionLimit;
	}

	/**
	 * Sets the reconnection limit.
	 * 
	 * @param num
	 *            the new reconnection limit
	 */
	public void setReconnectionLimit(int num)
	{
		reconnectionLimit = num;
	}
}
