/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.message;

import org.msgpack.type.Value;
import org.storm.nexus.agent.service.proxy.Callback;
import org.storm.nexus.agent.service.proxy.MessageSendable;

/**
 * The Class Request.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Request implements Callback<Object>
{
	private MessageSendable channel;
	private int msgid;
	private String method;
	private Value args;
	private String className;

	/**
	 * Instantiates a new request.
	 * 
	 * @param channel
	 *            the channel
	 * @param msgid
	 *            the msgid
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	public Request(MessageSendable channel, int msgid, String className, String method, Value args)
	{
		this.channel = channel;
		this.msgid = msgid;
		this.method = method;
		this.args = args;
		this.className = className;
	}

	/**
	 * Instantiates a new request.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 */
	public Request(String className, String method, Value args)
	{
		this.channel = null;
		this.msgid = 0;
		this.className = className;
		this.method = method;
		this.args = args;
	}

	/**
	 * Gets the method name.
	 * 
	 * @return the method name
	 */
	public String getMethodName()
	{
		return method;
	}

	/**
	 * Gets the arguments.
	 * 
	 * @return the arguments
	 */
	public Value getArguments()
	{
		return args;
	}

	/**
	 * Return the fully qualified class name
	 * 
	 * @return the fully qualified class name
	 */
	public String getClassName()
	{
		return className;
	}

	/**
	 * Gets the message id.
	 * 
	 * @return the message id
	 */
	public int getMessageID()
	{
		return msgid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.proxy.Callback#sendResult(java.lang.Object)
	 */
	@Override
	public void sendResult(Object result)
	{
		sendResponse(result, null);
	}

	/**
	 * Send error.
	 * 
	 * @param error
	 *            the error
	 */
	public void sendError(Object error)
	{
		sendResponse(null, error);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.proxy.Callback#sendError(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void sendError(Object error, Object data)
	{
		sendResponse(data, error);
	}

	/**
	 * Send response.
	 * 
	 * @param result
	 *            the result
	 * @param error
	 *            the error
	 */
	public synchronized void sendResponse(Object result, Object error)
	{
		if (channel == null)
		{
			return;
		}
		ResponseMessage msg = new ResponseMessage(msgid, error, result);
		channel.sendMessage(msg);
		channel = null;
	}
}
