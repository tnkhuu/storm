/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.agent.service.dispatcher;

import org.msgpack.MessagePack;

/**
 * The Class StopWatchDispatcherBuilder.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class StopWatchDispatcherBuilder implements DispatcherBuilder
{

	/** The base builder. */
	protected DispatcherBuilder baseBuilder;

	/** The verbose. */
	protected boolean verbose = false;

	/**
	 * Checks if is verbose.
	 * 
	 * @return true, if is verbose
	 */
	public boolean isVerbose()
	{
		return verbose;
	}

	/**
	 * Sets the verbose.
	 * 
	 * @param verbose
	 *            the new verbose
	 */
	public void setVerbose(boolean verbose)
	{
		this.verbose = verbose;
	}

	/**
	 * With verbose output.
	 * 
	 * @param verbose
	 *            the verbose
	 * @return the stop watch dispatcher builder
	 */
	public StopWatchDispatcherBuilder withVerboseOutput(boolean verbose)
	{
		this.verbose = verbose;
		return this;
	}

	/**
	 * Instantiates a new stop watch dispatcher builder.
	 * 
	 * @param baseBuilder
	 *            the base builder
	 */
	public StopWatchDispatcherBuilder(DispatcherBuilder baseBuilder)
	{
		this.baseBuilder = baseBuilder;
	}

	/**
	 * Decorate.
	 * 
	 * @param innerDispatcher
	 *            the inner dispatcher
	 * @return the dispatcher
	 */
	public Dispatcher decorate(Dispatcher innerDispatcher)
	{
		StopWatchDispatcher disp = new StopWatchDispatcher(innerDispatcher);
		disp.setVerbose(verbose);
		return disp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.storm.nexus.agent.service.dispatcher.DispatcherBuilder#build(java
	 * .lang.Object, org.msgpack.MessagePack)
	 */
	@Override
	public Dispatcher build(Object handler, MessagePack messagePack)
	{
		return decorate(baseBuilder.build(handler, messagePack));
	}
}
