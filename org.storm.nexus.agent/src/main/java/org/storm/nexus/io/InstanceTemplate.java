/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package org.storm.nexus.io;

import java.io.IOException;

import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.template.AbstractTemplate;
import org.msgpack.unpacker.Unpacker;
import org.storm.nexus.api.Instance;

/**
 * The Class InstanceTemplate.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class InstanceTemplate extends AbstractTemplate<Instance>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msgpack.template.Template#write(org.msgpack.packer.Packer,
	 * java.lang.Object, boolean)
	 */
	@Override
	public void write(Packer pk, Instance v, boolean required) throws IOException
	{
		if (v == null)
		{
			if (required)
			{
				throw new MessageTypeException("Attempted to write null");
			}
			pk.writeNil();
			return;
		}
		String addr = v.getServerAddress();
		String desc = v.getDescription();
		String name = v.getName();
		int port = v.getPort();
		int rpcPort = v.getRpcPort();

		pk.write(addr);
		pk.write(name);
		pk.write(desc);
		pk.write(port);
		pk.write(rpcPort);

	}

	/* (non-Javadoc)
	 * @see org.msgpack.template.Template#read(org.msgpack.unpacker.Unpacker, java.lang.Object, boolean)
	 */
	@Override
	public Instance read(Unpacker u, Instance to, boolean required) throws IOException
	{
		if (!required && u.trySkipNil())
		{
			return null;
		}
		String addr = u.readString();
		String desc = u.readString();
		String name = u.readString();
		int port = u.readInt();
		int rpcPort = u.readInt();

		Instance instance = new Instance(addr, name, desc, port, rpcPort);
		return instance;

	}

}
