/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.rpc;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class TestInterfaceImpl implements TestInterface
{
	public TestInterfaceImpl()
	{
	}

	@Override
	public String m1(String a1)
	{
		return "m1 " + a1;
	}

	@Override
	public Future<String> m1Async(final String a1)
	{
		return new FutureTask<String>(new Callable<String>()
		{
			@Override
			public String call() throws Exception
			{
				Thread.sleep(1000);
				return "m1Async " + a1;
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.nexus.rpc.TestInterface#m2(java.lang.Integer)
	 */
	@Override
	public String m2(Integer time_millis)
	{
		return "" + time_millis;
	}
}