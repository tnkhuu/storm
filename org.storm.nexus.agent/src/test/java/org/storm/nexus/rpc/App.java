package org.storm.nexus.rpc;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

import com.netflix.curator.CuratorZookeeperClient;
import com.netflix.curator.framework.CuratorFramework;
import com.netflix.curator.retry.ExponentialBackoffRetry;

public class App implements Watcher
{
	static CuratorFramework c;
	private static App instance = new App();
	static CuratorZookeeperClient zc;
	public static void main(String[] args) throws Exception
	{
	
		/*c = CuratorFrameworkFactory.builder().connectString("localhost:2181").retryPolicy((RetryPolicy) new ExponentialBackoffRetry(2000, 3)).build();
		c.start();*/
		long start = System.currentTimeMillis();
		 zc = new CuratorZookeeperClient("gateway.stormclowd.com:2181", 10000, 3000, instance, new ExponentialBackoffRetry(3000, 2));
	zc.start();
	zc.getZooKeeper().getChildren("/services", false);
	long end = System.currentTimeMillis() - start;
	System.out.println("done in " + end + "secs");
	//	 zc.getZooKeeper().getChildren("/watching", true);
	//	Stat s = c.checkExists().forPath("/watching");
	//	if(s == null) {
//			c.create().forPath("/watching");
//		}
		/*PathChildrenCache cache = new PathChildrenCache(c,"/watching", true);
		cache.start();
		*/
	//	zc.getZooKeeper().getChildren("/watching", true);
	//	c.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath("/watching");
	//	c.getChildren().usingWatcher(instance).forPath("/watching");
		while(true)
		{
			Thread.sleep(2000);
			//System.out.println(	cache.getCurrentData());
		}
	}
	@Override
	public void process(WatchedEvent event)
	{
		try
		{
		System.out.println("Something happened: " + event.getPath() + " state:" + event.getState());
		
		zc.getZooKeeper().getChildren("/watching", true);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	

	
}
