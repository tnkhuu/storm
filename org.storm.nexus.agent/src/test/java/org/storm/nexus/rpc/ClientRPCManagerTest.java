/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.rpc;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.storm.api.factories.ImageFactory;
import org.storm.api.factories.ImageFactory.Distribution;
import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.Client;

public class ClientRPCManagerTest
{

	private Client client = null;
	private EventLoop loop;

	/**
	 * Test client rpc.
	 * 
	 * @throws UnknownHostException
	 *             the unknown host exception
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * @throws MalformedURLException
	 */
	// @Test //@Ignore("Needs the platform to be running to work.")
	public void testClientRPC() throws UnknownHostException, InterruptedException, ExecutionException, MalformedURLException
	{

		loop = EventLoop.start();
		client = new Client("127.0.0.1", 1338, loop);
		client.setRequestTimeout(5000);//
		ImageFactory<?> service = client.proxy(ImageFactory.class);
		Future<?> val = service.createImage(Distribution.UBUNTU, new URL("http://www.stormclowd.com/spec/default"), "/tmp/ubuntu.tar.gz");

		System.out.println(val.get());
	}
}
