/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.rpc;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.storm.api.services.LogicalVolumeService;
import org.storm.api.services.VirtualizationProvider;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.lvm.LogicalVolumnServiceImpl;
import org.storm.lxc.LxcProvider;
import org.storm.nexus.agent.loop.EventLoop;
import org.storm.nexus.agent.service.Client;
import org.storm.nexus.agent.service.Server;
import org.storm.nexus.api.Role;

/**
 * The Class RPCServerClientTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Ignore
public class RPCServerClientTest
{

	/**
	 * Test future.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testFuture() throws Exception
	{
		EventLoop loop = EventLoop.start();
		Server svr = new Server(loop);
		svr.serve(TestInterface.class, new TestInterfaceImpl());
		svr.serve(LogicalVolumeService.class, new LogicalVolumnServiceImpl());
		svr.serve(VirtualizationProvider.class, new LxcProvider());
		svr.listen(1339);

		Client cli = new Client("127.0.0.1", 1339, loop);
		LogicalVolumeService c = cli.proxy(LogicalVolumeService.class);
		VirtualizationProvider p = cli.proxy(VirtualizationProvider.class);
		try
		{
			System.out.println("Creating container");
	/*		String res =			c.createSnapshotVolumn("www.storm.clowd", Role.SERVER, 1500, "/dev/lxc/storm-lxc", "lxc");
		*/	
			String snapshot = c.createSnapshotVolumn("www.stormZ.clowd", Role.SERVER.name(), 1500, LogicalVolumeService.BASE_LXC_LVM_DIR, LogicalVolumeService.LXC_VOLUMN_NAME);
			p.createLVMBackedContainer(HypervisorDriver.LXC, Role.SERVER.name(), "www.stormZ.clowd", snapshot, "8.8.8.8");
			//c.createContainer(HypervisorDriver.LXC, Role.SERVER.name(), "www.stormclowd.com", "8.8.8.8");
			System.out.println("DONE");
		//	final String ipAddress = c.startContainer("www.stormclowd.com");
			//System.out.println(ipAddress);
			
			/*String f1 = c.m1("a1");
			String f2 = c.m1("a2");
			Future<String> f3 = c.m1Async("a3");

			Assert.assertEquals("m1 a1", f1);
			Assert.assertEquals("m1 a2", f2);
			Assert.assertEquals("m1Async a3", f3.get());*/

		}
		finally
		{
			svr.close();
			cli.close();
		}
	}

	@After
	public void afterClass()
	{

	}

}
