/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.nexus.rpc;

import java.util.concurrent.Future;

/**
 * The Interface TestInterface.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public interface TestInterface
{

	/**
	 * M1.
	 * 
	 * @param a1
	 *            the a1
	 * @return the string
	 */
	public String m1(String a1);

	/**
	 * M1 async.
	 * 
	 * @param a1
	 *            the a1
	 * @return the string
	 */
	public Future<String> m1Async(String a1);

	/**
	 * M2.
	 * 
	 * @param time_millis
	 *            the time_millis
	 * @return the string
	 */
	public String m2(Integer time_millis);
}