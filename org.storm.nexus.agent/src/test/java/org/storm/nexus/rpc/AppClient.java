package org.storm.nexus.rpc;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.data.Stat;

import com.netflix.curator.RetryPolicy;
import com.netflix.curator.framework.CuratorFramework;
import com.netflix.curator.framework.CuratorFrameworkFactory;
import com.netflix.curator.framework.api.CuratorWatcher;
import com.netflix.curator.retry.ExponentialBackoffRetry;

public class AppClient implements CuratorWatcher
{
	@SuppressWarnings("unused")
	private static AppClient instance = new AppClient();
	private static CuratorFramework c = null;
	public static void main(String[] args) throws Exception
	{
	
		c = CuratorFrameworkFactory.builder().connectString("localhost:2181").retryPolicy((RetryPolicy) new ExponentialBackoffRetry(2000, 3)).build();
		c.start();
		Stat s= c.checkExists().forPath("/watching/node");
		if(s != null) 
		{
			c.delete().forPath("/watching/node");
		}

		c.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath("/watching/node");
		while(true)
		{
			Thread.sleep(5000);
		}
	}

	@Override
	public void process(WatchedEvent event) throws Exception
	{
		System.out.println(event.getPath());
		
	}

}
