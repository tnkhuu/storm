/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.tools.http;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Download.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Download
{
	
	/** The s_log. */
	private static Logger s_log = LoggerFactory.getLogger(Download.class);
	
	/** The Constant BLOCK_SIZE. */
	private static final int BLOCK_SIZE = 4096;
	
	/** The Constant BUFFER_SIZE. */
	private static final int BUFFER_SIZE = 4096;
	
	/** The exec. */
	private static Executor exec = Executors.newCachedThreadPool();

	/**
	 * File.
	 *
	 * @param from the from
	 * @param to the to
	 * @param connections the connections
	 */
	public static void file(final URL from, final File to, final int connections)
	{
		HttpURLConnection conn = null;
		final CountDownLatch latch = new CountDownLatch(connections);
		try
		{
			conn = (HttpURLConnection) from.openConnection();
			final int contentLength = validateRequest(conn);
			final int partSize = Math.round(((float) contentLength / connections) / BLOCK_SIZE) * BLOCK_SIZE;
			for (int i = 0; i < connections; i++)
			{
				final int count = i;
				exec.execute(new Runnable()
				{

					@Override
					public void run()
					{
						try
						{
							RandomAccessFile raf = new RandomAccessFile(to, "rw");
							HttpURLConnection conn = (HttpURLConnection) new URL(from.toString()).openConnection();
							conn.setConnectTimeout(10000);
							String byteRange = null;
							if (count == connections - 1)
							{
								byteRange = (count * partSize) + 1 + "-" + contentLength;
								raf.seek((count * partSize) + 1);
							}
							else if (count == 0)
							{
								byteRange = 0 + "-" + ((count + 1) * partSize);
								raf.seek(0);
							}
							else
							{
								byteRange = (count * partSize) + 1 + "-" + ((count + 1) * partSize);
								raf.seek((count * partSize) + 1);
							}
							s_log.debug("Thread: {} downloading {}", count, byteRange);
							conn.setRequestProperty("Range", "bytes=" + byteRange);
							conn.connect();
							BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
							int numRead = 0;
							byte data[] = new byte[BUFFER_SIZE];
							while (((numRead = in.read(data, 0, BUFFER_SIZE)) != -1))
							{
								raf.write(data, 0, numRead);
							}

							s_log.debug("Thread {} completed", count);
							raf.close();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
						finally
						{
							latch.countDown();
						}

					}
				});
			}
			latch.await();

		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		catch (InterruptedException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * File.
	 *
	 * @param from the from
	 * @param to the to
	 * @param connections the connections
	 */
	public static void file(final URL from, final String to, final int connections)
	{
		file(from, new File(to), connections);
	}
	
	/**
	 * File.
	 *
	 * @param from the from
	 * @param to the to
	 * @param connections the connections
	 * @param headers the headers
	 */
	public static void file(final URL from,final File to, final int connections, final Map<String,String> headers) {

		final CountDownLatch latch = new CountDownLatch(connections);
		try
		{
			
			Request req =	Request.get(from);
			from.openConnection();
			Iterator<String> keys = headers.keySet().iterator();
			while(keys.hasNext()) {
				String key = keys.next();
				req.header(key, headers.get(key));
			}
			
			final int contentLength = req.contentLength();
			System.out.println(contentLength);
			
			System.exit(0);
			final int partSize = Math.round(((float) contentLength / connections) / BLOCK_SIZE) * BLOCK_SIZE;
			for (int i = 0; i < connections; i++)
			{
				final int count = i;
				exec.execute(new Runnable()
				{

					@Override
					public void run()
					{
						try
						{
							RandomAccessFile raf = new RandomAccessFile(to, "rw");
							HttpURLConnection conn = (HttpURLConnection) new URL(from.toString()).openConnection();
							conn.setConnectTimeout(10000);
							String byteRange = null;
							if (count == connections - 1)
							{
								byteRange = (count * partSize) + 1 + "-" + contentLength;
								raf.seek((count * partSize) + 1);
							}
							else if (count == 0)
							{
								byteRange = 0 + "-" + ((count + 1) * partSize);
								raf.seek(0);
							}
							else
							{
								byteRange = (count * partSize) + 1 + "-" + ((count + 1) * partSize);
								raf.seek((count * partSize) + 1);
							}
							s_log.debug("Thread: {} downloading {}", count, byteRange);
							conn.setRequestProperty("Range", "bytes=" + byteRange);
							conn.connect();
							BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
							int numRead = 0;
							byte data[] = new byte[BUFFER_SIZE];
							while (((numRead = in.read(data, 0, BUFFER_SIZE)) != -1))
							{
								raf.write(data, 0, numRead);
							}

							s_log.debug("Thread {} completed", count);
							raf.close();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
						finally
						{
							latch.countDown();
						}

					}
				});
			}
			latch.await();

		}
		catch (IOException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		catch (InterruptedException e)
		{
			s_log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Validate request.
	 *
	 * @param conn the conn
	 * @return the int
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private static int validateRequest(HttpURLConnection conn) throws IOException
	{
		int contentLength = conn.getContentLength();
		int responseCode = conn.getResponseCode();
		if (responseCode / 100 != 2)
		{
			throw new IllegalArgumentException("URL Returned a response code of " + conn.getResponseCode());
		}

		if (contentLength < 1)
		{
			throw new IllegalArgumentException("Content Length was zero bytes ");
		}
		return contentLength;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws MalformedURLException the malformed url exception
	 */
	public static void main(String[] args) throws MalformedURLException
	{
		file(new URL("https://cloud-images.ubuntu.com/releases/quantal/release-20130206/ubuntu-12.10-server-cloudimg-amd64-root.tar.gz"), new File("/tmp/cloud.tar.gz"), 8);
	}
}
