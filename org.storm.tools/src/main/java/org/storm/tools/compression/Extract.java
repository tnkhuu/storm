/** 
* Copyright 2013 Trung Khuu
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of this License at : 
* 
* http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.storm.tools.compression;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extraction utilities for various compressed tar and tar.gz files.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Extract
{
	
	/** The class logger. */
	private static final Logger s_log = LoggerFactory.getLogger(Extract.class);
	
	/** The extraction buffer size.. */
	private static final int WRITE_BUFFER = 4096;
	
	/** Tar archive file extension*/
	private static final String TAR_ARCHIVE = "tar";
	
	/**
	 * The Class GzipWriterThread.
	 * 
	 * @author Trung Khuu
	 * @since 1.0
	 */
	private static class GzipWriterThread extends Thread
	{
		
		/** The output stream. */
		OutputStream out;
		
		/** The input stream */
		InputStream in;

		/**
		 * Instantiates a new gzip writer thread.
		 * 
		 * @param out
		 * @param in
		 */
		public GzipWriterThread(OutputStream out, InputStream in)
		{
			this.out = out;
			this.in = in;
		}

		/* (non-Javadoc)
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run()
		{
		    boolean errorThrown = false;
			try
			{
				byte[] buffer = new byte[WRITE_BUFFER];
				for (int read = in.read(buffer); read != -1; read = in.read(buffer))
				{
					out.write(buffer, 0, read);
				}

			}
			catch (Exception e)
			{
				s_log.error(e.getMessage(), e);
				errorThrown = true;
				throw new RuntimeException(e);
			}
			finally
			{
				try
				{
					in.close();
				}
				catch (IOException e)
				{
					//Don't mask the original exception if it was a write, rather then the close that failed.
					if(!errorThrown) {
						s_log.error(e.getMessage(), e);
						throw new RuntimeException(e);
					}
					
				}
			}
		}
	}

	/**
	 * Extract the contents of a tar.gz file to the specified directory.
	 * 
	 * @param sourceFile - the source tar.gz file
	 * @param destDir    - the output directory
	 * @return the list
	 * @throws FileNotFoundException
	 * @throws IOException 
	 * @throws ArchiveException the archive exception
	 */
	public static List<File> targz(final File sourceFile, final File destDir) throws FileNotFoundException, IOException, ArchiveException
	{

		s_log.info("Ungzipping {} to {}.", sourceFile.getAbsolutePath(), destDir.getAbsolutePath());
		final List<File> untaredFiles = new LinkedList<File>();

		final GZIPInputStream in = new GZIPInputStream(new FileInputStream(sourceFile));
		final PipedOutputStream pout = new PipedOutputStream();
		
		//pipe the gzip input to the tar input and do a direct extraction.
		PipedInputStream pipedIn = new PipedInputStream();
		pipedIn.connect(pout);
		GzipWriterThread pipedGzipStream = new GzipWriterThread(pout, in);
		
		//kick off the .gz extraction. 
		pipedGzipStream.start();
		
		//now extract the stream (which is still tarred) to a directory.
		final InputStream is = pipedIn;
		final TarArchiveInputStream sourceInputStream = (TarArchiveInputStream) new ArchiveStreamFactory().createArchiveInputStream(TAR_ARCHIVE, is);
		TarArchiveEntry entry = null;
		while ((entry = (TarArchiveEntry) sourceInputStream.getNextEntry()) != null)
		{
			File extractedFile = new File(destDir, entry.getName());
			File parent = new File(extractedFile.getParent());
			if (!parent.exists())
			{
				parent.mkdirs();
			}
			if (entry.isDirectory())
			{
				if (!extractedFile.exists())
				{
					if (!extractedFile.mkdirs())
					{
						throw new IllegalStateException("Error creating :" + extractedFile.getAbsolutePath());
					}
				}
			}
			else
			{
				extractedFile.createNewFile();
				final OutputStream outputFileStream = new FileOutputStream(extractedFile);
				IOUtils.copy(sourceInputStream, outputFileStream);
				outputFileStream.close();
			}
			untaredFiles.add(extractedFile);
		}

		pout.close();
		sourceInputStream.close();
		in.close();
		return untaredFiles;
	}



	/**
	 * Extract the tar file to the target directory.
	 * 
	 * @param source the tar source file
	 * @param targetDir the destination directory
	 * @return A list of files that was extracted.
	 * @throws FileNotFoundException 
	 * @throws IOException
	 * @throws ArchiveException
	 */
	public static List<File> tar(final File source, final File targetDir) throws FileNotFoundException, IOException, ArchiveException
	{

		s_log.info("Extracting {} to dir {}.", source.getAbsolutePath(), targetDir.getAbsolutePath());

		final List<File> untaredFiles = new LinkedList<File>();
		final InputStream is = new FileInputStream(source);
		final TarArchiveInputStream tarInput = (TarArchiveInputStream) new ArchiveStreamFactory().createArchiveInputStream(TAR_ARCHIVE, is);
		TarArchiveEntry entry = null;
		while ((entry = (TarArchiveEntry) tarInput.getNextEntry()) != null)
		{
			final File extractedOutputFile = new File(targetDir, entry.getName());
			if (entry.isDirectory())
			{
				if (!extractedOutputFile.exists())
				{
					if (!extractedOutputFile.mkdirs())
					{
						throw new IllegalStateException("Error creating : " + extractedOutputFile.getAbsolutePath());
					}
				}
			}
			else
			{
				final OutputStream outputFileStream = new FileOutputStream(extractedOutputFile);
				IOUtils.copy(tarInput, outputFileStream);
				outputFileStream.close();
			}
			untaredFiles.add(extractedOutputFile);
		}
		tarInput.close();
		return untaredFiles;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ArchiveException
	 *             the archive exception
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException, ArchiveException
	{
		System.out.println(Extract.targz(new File("/home/tkhuu/Downloads/apache-maven-3.0.5-bin.tar.gz"), new File("/tmp/unzip")));
	}
}
