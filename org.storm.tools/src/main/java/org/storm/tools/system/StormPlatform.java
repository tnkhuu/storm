/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.tools.system;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.storm.api.Storm;
import org.storm.api.kernel.Actor;
import org.storm.nexus.api.Role;

/**
 * The Class StormPlatform.
 */
public class StormPlatform extends Actor
{

	/** The ide. */
	private static Pattern		IDE					= Pattern.compile(".*org.storm.tools.target.classes");

	/** The Constant DEPLOY_PRJ_NAME. */
	private static final String	DEPLOY_PRJ_NAME		= "org.storm.deploy";

	/** The Constant STORM_WORKING_DIR. */
	public static final String	STORM_WORKING_DIR	= ".storm";

	/** The Constant STORM_DIST_PREFIX. */
	private static final String	STORM_DIST_PREFIX	= "dist";

	/** The Constant JAR. */
	private static final String	JAR					= ".jar";

	/** The Constant USER_HOME. */
	public static final String	USER_HOME			= "user.home";

	/** The mode. */
	private RuntimeMode			mode;

	/** The stormhome. */
	private File				stormhome;

	/** The instance. */
	public static StormPlatform	INSTANCE			= new StormPlatform();

	/** The usrhome. */
	private static String		USRHOME;

	/** The basedir. */
	private File			BASEDIR;

	/**
	 * Instantiates a new storm platform.
	 */
	private StormPlatform()
	{
		if (Role.INITIAL == getRole())
		{
			USRHOME = System.getProperty(USER_HOME);
			File working = new File(USRHOME + File.separator + STORM_WORKING_DIR);
			if (!working.exists())
			{
				throw new IllegalStateException("Storm image and distribution binaries not found. Has storm been setup correctly ?");
			}
			this.BASEDIR = working;
		}
	}

	/**
	 * The Enum RuntimeMode.
	 */
	public static enum RuntimeMode
	{

		/** The ide. */
		IDE,

		/** The binary. */
		BINARY
	}

	/**
	 * Gets the storm source home.
	 * 
	 * @return the storm source home
	 */
	public File getStormSourceHome()
	{
		File jar = new File(StormPlatform.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		Matcher m = IDE.matcher(jar.toString());
		if (m.matches())
		{
			String pathStr = jar.toString();
			String[] path = pathStr.split("\\\\");
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < path.length - 3; i++)
			{
				builder.append(path[i]);
				builder.append(File.separator);
			}
			return new File(builder.toString());
		}
		return null;
	}

	/**
	 * Gets the storm home.
	 * 
	 * @return the storm home
	 */
	public File getStormHome()
	{
		if (stormhome == null)
		{
			File jar = new File(StormPlatform.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			Matcher m = IDE.matcher(jar.toString());
			if (m.matches())
			{
				String pathStr = jar.toString();
				String[] path = pathStr.split("\\" + File.separator);
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < path.length - 3; i++)
				{
					builder.append(path[i]);
					builder.append(File.separator);
				}
				builder.append(DEPLOY_PRJ_NAME);
				File home = new File(builder.toString());
				if (!home.exists()) throw new RuntimeException("Could not determine storm home in source mode");
				stormhome = home;
				mode = RuntimeMode.IDE;
			}
			else if (jar.toString().endsWith(JAR))
			{
				String pathStr = jar.toString();
				String[] path = pathStr.split("\\" + File.separator);
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < path.length - 2; i++)
				{
					builder.append(path[i]);
					builder.append(File.separator);
				}
				File home = new File(builder.toString());
				if (!home.exists()) throw new RuntimeException("Could not determine storm home in jar mode");
				stormhome = home;
				mode = RuntimeMode.BINARY;
			}
		}
		return stormhome;
	}

	/**
	 * Gets the runtime mode.
	 * 
	 * @return the runtime mode
	 */
	public RuntimeMode getRuntimeMode()
	{
		if (mode == null)
		{
			getStormHome();
		}
		return mode;
	}

	/**
	 * Gets the sSH public key.
	 * 
	 * @return the sSH public key
	 */
	public String getSSHPublicKey()
	{
		return USRHOME + File.separator + ".ssh/id_rsa.pub";
	}

	public String createMacAddress(long mac)
	{

		if (mac > 0xFFFFFFFFFFFFL || mac < 0) throw new IllegalArgumentException("mac out of range");

		StringBuffer m = new StringBuffer(Long.toString(mac, 16));
		while (m.length() < 12)
			m.insert(0, "0");

		for (int j = m.length() - 2; j >= 2; j -= 2)
			m.insert(j, ":");
		return m.toString().toUpperCase();
	}

	public String createRandomMacAddress()
	{
		long mac = (long) (Math.random() * Integer.MAX_VALUE);
		if (mac > 0xFFFFFFFFFFFFL || mac < 0) throw new IllegalArgumentException("mac out of range");

		StringBuffer m = new StringBuffer(Long.toString(mac, 16));
		while (m.length() < 12)
			m.insert(0, "0");

		for (int j = m.length() - 2; j >= 2; j -= 2)
			m.insert(j, ":");
		return m.toString().toUpperCase();
	}

	/**
	 * Gets the storm image.
	 * 
	 * @return the storm image
	 */
	public String getStormImage()
	{
		String base = BASEDIR.getAbsolutePath();
		return base + File.separator + STORM_DIST_PREFIX + File.separator + Storm.STORM_DIST_TAR;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		System.out.println(INSTANCE.createMacAddress((long) (Math.random() * Integer.MAX_VALUE)));
	}
}
