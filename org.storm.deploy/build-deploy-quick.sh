#!/bin/bash
# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================


#Setup global env variables
USER_HOME=$(eval echo ~${SUDO_USER})
CURRENT_USER=$(eval echo ${SUDO_USER})
BASEDIR=$(dirname $0)
BASEDIRPATH=`readlink -f $(dirname $0)`
echo "Build home is $BASEDIR"
STORM_HOME="$USER_HOME/.storm"
sudo iptables -t nat -D PREROUTING 1 2>&1 > /dev/null
sudo iptables -t nat -D PREROUTING 1 2>&1 > /dev/null
sudo chmod 777 /etc/resolv.conf
sudo echo "nameserver	8.8.8.8" > /etc/resolv.conf
pushd $BASEDIR/../ > /dev/null
JAVA_HOME="$USER_HOME/.storm/java/jdk1.7.0"
export JAVA_HOME
pushd "$BASEDIRPATH/../" > /dev/null
if [ "$1" == "compile" ]; then
	sudo rm -rf $STORM_HOME/dist/storm-cloud-1.0.0-lxc-rootfs.tar.gz
	mvn clean install -DskipTests
fi

cd $BASEDIR
echo "Rebuilding Storm Image ..."
sudo rm -rf $USER_HOME/work/*
sudo $BASEDIRPATH/scripts/setup/setup.sh -c compile
popd > /dev/null
