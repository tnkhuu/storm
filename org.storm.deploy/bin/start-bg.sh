#! /bin/bash
if [ "$JAVA_HOME" == "" ]; then
	echo ""
	echo "	Error: "
	echo " 	JAVA_HOME is not set. "
	echo " 	Please set JAVA_HOME to a valid Java 7 JRE or SDK before attempting to launch Storm."
	echo ""
	exit 0;
fi
BASEDIR=$(dirname $0)
exec $JAVA_HOME/bin/java -Dlogback.configurationFile=$BASEDIR/conf/logback.xml -jar $BASEDIR/storm-bootstrap.jar >> $BASEDIR/../storm.log 2>&1 &
echo "Storm platform is now running ..."
echo "Check the log file in $BASEDIR../storm.log for more details."

