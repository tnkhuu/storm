#!/bin/bash
# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================

downloadJava()
{
	if [ -f "$STORM_JAVA_BIN_HOME/$JAVA_TAR" ]
	then
		echo "	- Found existing java binaries at: $STORM_JAVA_BIN_HOME/$JAVA_TAR"
	else
		pushd "$STORM_JAVA_BIN_HOME" > /dev/null
		sudo wget --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie;" http://download.oracle.com/otn-pub/java/jdk/7u65-b17/jdk-7u65-linux-x64.tar.gz || { echo "Could not download java"; exit 1; }
		sudo tar xfz $JAVA_TAR
		sudo chmod -R 777 *
		export JAVA_HOME=$STORM_JAVA_BIN_HOME/jdk1.7.0
		export PATH=$PATH:$JAVA_HOME/jdk1.7.0/bin
		echo "export JAVA_HOME=$STORM_JAVA_BIN_HOME/jdk1.7.0" >> $USER_HOME/.profile
		echo "export PATH=\$PATH:\$JAVA_HOME/jdk1.7.0/bin" >> $USER_HOME/.profile
		popd > /dev/null
	fi
	
}
