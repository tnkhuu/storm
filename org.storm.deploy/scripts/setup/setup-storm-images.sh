#!/bin/bash

# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================



downloadLxcImages()
{
	
	RELEASE="precise"
	VERSION="release-20130222"
	IMG_URL="https://cloud-images.ubuntu.com/releases/$RELEASE/$VERSION/$CLOUD_IMG"
	if [ -f "$STORM_DIST_HOME/$CLOUD_IMG" ]; then
		echo "	- LXC Tar already exists"
	else

		pushd $STORM_DIST_HOME > /dev/null
		echo "	- About to download $IMG_URL"
		sudo mcurl $IMG_URL || { echo "	- Couldn't find cloud image $IMG_URL."; exit 1; }
		popd > /dev/null
	fi
}


