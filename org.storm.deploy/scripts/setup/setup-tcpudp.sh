#!/bin/bash
# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================


optimizeTcpUdpSettings()
{
	if [ -f "$STORM_HOME/.tcpudp" ]; then 
		echo "	- Already configured TCP/UDP settings."
	else
		sudo sysctl -w net.ipv4.ip_forward=1 > /dev/null && echo "	-Set net.ipv4.ip_forward to 1" || { echo "	-Error : Could not set net.ipv4.ip_forward"; exit 1; }
		sudo sysctl -w net.ipv4.netfilter.ip_conntrack_udp_timeout=3 > /dev/null && echo "	-Set net.ipv4.netfilter.ip_conntrack_udp_timeout to 3" || { echo "	-INFO : Did not set net.ipv4.netfilter.ip_conntrack_udp_timeout"; }
		sudo sysctl -w net.ipv4.netfilter.ip_conntrack_udp_timeout_stream=10 > /dev/null && echo "	-Set net.ipv4.netfilter.ip_conntrack_udp_timeout_stream to 10" || { echo "	-INFO : Did not set net.ipv4.netfilter.ip_conntrack_udp_timeout_stream"; }
		sudo sysctl -w net.ipv4.netfilter.ip_conntrack_tcp_timeout_time_wait=10 > /dev/null && echo "	-Set net.ipv4.netfilter.ip_conntrack_tcp_timeout_time_wait to 10" || { echo "	-INFO : Did not set net.ipv4.netfilter.ip_conntrack_tcp_timeout_time_wait"; }
		sudo sysctl -w net.ipv4.ip_local_port_range=10000 > /dev/null && echo "	-Set net.ipv4.ip_local_port_range from 10k - 65k" || { echo "	-Error : Could not set net.ipv4.ip_local_port_range"; exit 1; }
		sudo sysctl -w net.ipv4.netfilter.ip_conntrack_tcp_timeout_established=600 > /dev/null && echo "	-Set net.ipv4.netfilter.ip_conntrack_tcp_timeout_established to 600" || { echo "	-INFO : Did not set net.ipv4.netfilter.ip_conntrack_tcp_timeout_established"; }
		sudo sysctl -w net.core.rmem_max=268435456 > /dev/null && echo "	-Set net.core.rmem_max to 268435456" || { echo "	-Error : Could not set net.core.rmem_max"; exit 1; }
		sudo sysctl -p
		sudo touch "$STORM_HOME/.tcpudp"
	fi
}
