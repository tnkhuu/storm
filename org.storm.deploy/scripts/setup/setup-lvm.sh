#!/bin/bash

# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================


setupLvm()
{
DEVICE=$1
if [ "$(id -u)" != "0" ]; then
    echo "Error: This script should needs to be run with sudo or with 'root' permissions"
    exit 1;
fi
if [ "$DEVICE" == "" ]; then
	echo "Error: You need to specify a device path to setup LVM partition with storm. Storm will default to a secondary device in /dev/sdb (i.e. ./setup-lvm.sh /dev/sdb) if none is specified or LVM support will be disabled if /dev/sdb doesn't exist. ";
	DEVICE="/dev/sdb"
fi

sudo fdisk -l | grep "$DEVICE" > /dev/null
RES="$?"
if [ $RES -eq 0 ]; then
	echo "	- Setting up LVM Partitions ... "
	type lvm > /dev/null 2>&1 && echo "	- LVM2 package is already Installed." || apt-get install -y lvm2

	sudo vgdisplay | grep "lxc" > /dev/null
	RES="$?"
	if [ $RES -eq 1 ]; then
		if $IS_CLOUD ; then
			sudo umount /mnt > /dev/null
		fi
		
		echo "	- Formatting a partition into LVM ... "
		cat <<EOF | fdisk $DEVICE
n
p



t
8e
w
EOF

		echo "	- Complete."
		sudo partprobe
		FIRSTDEV="1"
		sudo vgcreate lxc "$DEVICE$FIRSTDEV"
		echo "	- LVM Setup complete. "
	else
		echo "	- LVM Volumn Group for lxc already defined."
		echo "	- LVM Setup complete. "
	fi
else
	echo "	- WARNING: $DEVICE does not exist. Will not setup LVM."
	echo "	- WARNING: NOT setting up LVM will result in suboptimal, poor performance."
	echo "	- WARNING: Please setup a secondary dedicated drive at /dev/sdb and then re-run this script(This script will handle resumes) "
	exit 1;
fi
}
