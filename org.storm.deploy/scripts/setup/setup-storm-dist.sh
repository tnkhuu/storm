#!/bin/bash

# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================
buildNodeJs()
{
	type make > /dev/null 2>&1 && echo "	- Make Already Installed." || sudo apt-get install -y build-essential
	
	if [ ! -f "$STORM_NODE_HOME/$DIST" ]; then
		sudo mkdir -p $STORM_NODE_HOME
		pushd $STORM_NODE_HOME > /dev/null
		echo "	- About to download $NODEURL"
		sudo mcurl $NODEURL || { echo "	- Couldn't find node binary at $NODEURL."; exit 1; }
		popd > /dev/null
	fi
	sudo mkdir -p $STORM_WORK_HOME/opt/node
	sudo cp "$STORM_NODE_HOME/$DIST" "$STORM_WORK_HOME/opt/node"
	pushd $STORM_WORK_HOME/opt/node > /dev/null
	sudo tar xfz "$DIST"
	sudo rm -rf "$DIST"
	sudo mv $DIRNAME/* .
	sudo rm -rf $DIRNAME > /dev/null 2>&1
	popd > /dev/null
	sudo cp "$STORM_WORK_HOME/etc/hosts" "$STORM_WORK_HOME/etc/hosts.bak"
	sudo rm -rf "$STORM_WORK_HOME/etc/resolv.conf" > /dev/null 2>&1
	sudo touch "$STORM_WORK_HOME/etc/resolv.conf"
	sudo cp -f "/etc/resolv.conf" "$STORM_WORK_HOME/etc/resolv.conf"
	if [ ! -f "$STORM_WORK_HOME/etc/resolv.conf" ]; then
			sudo chmod 777 "$STORM_WORK_HOME/etc/resolv.conf"
			sudo cp -f "/etc/resolv.conf" "$STORM_WORK_HOME/etc/resolv.conf"
	fi
	
	sudo cp -f "/etc/apt/sources.list" "$STORM_WORK_HOME/etc/apt/sources.list"	
	sudo echo "127.0.0.1	$HOSTNAME" >>  "$STORM_WORK_HOME/etc/hosts"
	setupLxc
	sudo chroot $STORM_WORK_HOME sudo tar xfz /opt/$STORM_BRAND-$STORM_VERSION/node/$STORM_TTY.tar.gz
	sudo chroot $STORM_WORK_HOME sudo mv $STORM_TTY /opt/$STORM_BRAND-$STORM_VERSION/node
	sudo chroot $STORM_WORK_HOME sudo /bin/npm install /opt/$STORM_BRAND-$STORM_VERSION/node/$STORM_TTY -g
	sudo chmod 777 $STORM_WORK_HOME/bin/tty.js
	sudo chroot $STORM_WORK_HOME apt-get remove -y build-essential > /dev/null 2>&1
	sudo chroot $STORM_WORK_HOME apt-get remove -y gcc-4.6 > /dev/null 2>&1
	sudo chroot $STORM_WORK_HOME apt-get remove -y g++-4.6 > /dev/null 2>&1
	sudo chroot $STORM_WORK_HOME apt-get remove -y iso-codes > /dev/null 2>&1
	sudo chroot $STORM_WORK_HOME apt-get remove -y vim aptitude vim-runtime > /dev/null 2>&1
	sudo chroot $STORM_WORK_HOME apt-get autoclean > /dev/null 2>&1
	sudo chroot $STORM_WORK_HOME apt-get autoremove -y > /dev/null 2>&1
	sudo rm -rf $STORM_WORK_HOME/var/cache > /dev/null 2>&1
	sudo rm -rf $STORM_WORK_HOME/var/lib/apt > /dev/null 2>&1
	mkdir -p  $STORM_WORK_HOME/var/cache/apt
	sudo cp "$STORM_WORK_HOME/etc/hosts.bak" "$STORM_WORK_HOME/etc/hosts"
}



setupLxc()
{

echo "Setting up LXC requirements."
BASESETUPDIR="/var/lib/lxc/storm"
sudo mkdir -p $STORM_WORK_HOME
sudo mkdir -p $BASESETUPDIR
sudo rm "$BASESETUPDIR/config" > /dev/null 2>&1
sudo cat << EOF >>  "$BASESETUPDIR/config"
#lxc setup
lxc.network.type=veth
lxc.network.link=lxcbr0
lxc.network.flags=up
lxc.network.hwaddr = 00:16:3e:44:e3:b1
lxc.utsname = storm

lxc.tty = 12
lxc.pts = 1024
lxc.rootfs = $STORM_WORK_HOME
lxc.mount  = $BASESETUPDIR/fstab
lxc.arch = amd64
lxc.cap.drop = sys_module mac_admin
lxc.pivotdir = lxc_putold

# uncomment the next line to run the container unconfined:
#lxc.aa_profile = unconfined

lxc.cgroup.devices.deny = a
# Allow any mknod (but not using the node)
lxc.cgroup.devices.allow = c *:* m
lxc.cgroup.devices.allow = b *:* m
# /dev/null and zero
lxc.cgroup.devices.allow = c 1:3 rwm
lxc.cgroup.devices.allow = c 1:5 rwm
# consoles
lxc.cgroup.devices.allow = c 5:1 rwm
lxc.cgroup.devices.allow = c 5:0 rwm
#lxc.cgroup.devices.allow = c 4:0 rwm
#lxc.cgroup.devices.allow = c 4:1 rwm
# /dev/{,u}random
lxc.cgroup.devices.allow = c 1:9 rwm
lxc.cgroup.devices.allow = c 1:8 rwm
lxc.cgroup.devices.allow = c 136:* rwm
lxc.cgroup.devices.allow = c 5:2 rwm
# rtc
lxc.cgroup.devices.allow = c 254:0 rwm
#fuse
lxc.cgroup.devices.allow = c 10:229 rwm
#tun
lxc.cgroup.devices.allow = c 10:200 rwm
#full
lxc.cgroup.devices.allow = c 1:7 rwm
#hpet
lxc.cgroup.devices.allow = c 10:228 rwm
#kvm
lxc.cgroup.devices.allow = c 10:232 rwm

EOF
	sudo rm "$BASESETUPDIR/fstab" > /dev/null 2>&1
	sudo cat << EOF >>  "$BASESETUPDIR/fstab"
proc            proc         proc    nodev,noexec,nosuid 0 0
sysfs           sys          sysfs defaults  0 0
EOF

	seed_d="$STORM_WORK_HOME/var/lib/cloud/seed/nocloud-net"
	rhostid=$(uuidgen | cut -c -8)
	host_id=$rhostid
	sudo mkdir -p $seed_d
	sudo cat > "$seed_d/meta-data" <<EOF
instance_id: lxc-$host_id
EOF
	sudo cat > "$seed_d/user-data" <<EOF
#cloud-config
output: {all: '| tee -a /var/log/cloud-init-output.log'}
manage_etc_hosts: localhost
locale: en_AU.UTF-8
password: ubuntu
chpasswd: { expire: False }
package_update: true
packages:
- linux-headers-virtual 
- openvswitch-datapath-source 
- openvswitch-datapath-dkms 
- openvswitch-common 
- openvswitch-switch 
- openvswitch-brcompat
- build-essential
runcmd:
- [ sudo, ln, -s,  /opt/node/bin/node, /bin/node]
- [ sudo, ln, -s, /opt/node/bin/npm, /bin/npm]
- [ sudo, npm, install, node-gyp, -g ]
- [ sudo, apt-get, remove, -y, vim ]
- [ sudo, apt-get, remove, -y, linux-image-3.2.0-38-virtual ]
- [ sudo, apt-get, remove, -y, linux-headers-3.2.0-38 ]
- [ sudo, apt-get, remove, -y, linux-headers-3.2.0-38-virtual ]
- [ sh, -c, echo "Done" > /tmp/lxc-install-status ]
EOF
sudo cp /etc/apt/sources.list $STORM_WORK_HOME/etc/cloud/templates/sources.list.tmpl
echo "Starting Cloud Init on LXC Bundle."
sudo lxc-start -n storm -d
while [ ! -f "$STORM_WORK_HOME/var/log/cloud-init-output.log" ]
do
	sleep 1
done
echo "Watching for log messages to indicate completion"
sudo tail -f $STORM_WORK_HOME/var/log/cloud-init-output.log &
WAIT_PID=$!
while [ ! -f "$STORM_WORK_HOME/tmp/lxc-install-status" ]
do
	sleep 1
done
echo "Terminating Cloud Init LXC."
kill -9 $WAIT_PID
sudo lxc-stop -n storm


}

setupStormDist()
{
	VERSION="node-v0.10.4"
	ARCH="x64"
	OS="linux"
	DIRNAME="$VERSION-$OS-$ARCH"
	DIST="$VERSION-$OS-$ARCH.tar.gz"
	NODEURL="http://nodejs.org/dist/latest/node-v0.10.33.tar.gz"
	BASESETUPDIR="/var/lib/lxc/storm"
	if [ -f "$STORM_DIST_HOME/$STORM_DIST_IMG_NAME" ]; then
			echo "	- Storm LXC Image Already Exists @ $STORM_DIST_HOME/$STORM_DIST_IMG_NAME"
	else
	echo "	- Extracting LXC Image to Create Storm Image ..."
	sudo cp "$STORM_DIST_HOME/$CLOUD_IMG" $STORM_WORK_HOME
	pushd $STORM_WORK_HOME > /dev/null
	sudo tar xfz $CLOUD_IMG
	sudo rm $CLOUD_IMG

	sudo cat << EOF >> "$STORM_WORK_HOME/etc/init/storm-network.conf"
#!upstart
author      "Trung Khuu (trung.khuu@gmail.com)"
description "Storm Network Notification Services"

start on (net-device-up IFACE!=lo)
stop on shutdown

script
    IP=\`/sbin/ifconfig eth0 | grep \"inet addr\" | awk -F: '{print \$2}' | awk '{print \$1}'\`
    echo "\$IP" > /ip
end script

EOF


	sudo cat << EOF >> "$STORM_WORK_HOME/etc/init/stormtty.conf"
#!upstart
author      "Trung Khuu (trung.khuu@gmail.com)"
description "Storm Platform Terminal Services"

start on startup
stop on shutdown

script
    echo $$ > /var/run/stormtty.pid
    nohup sudo -u ubuntu /bin/node /bin/tty.js -p 3333 -d > /var/log/stormtty.log 2>&1
end script

pre-start script
    echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/stormtty.log
end script

pre-stop script
    rm /var/run/stormtty.pid
    echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Stopping" >> /var/log/stormtty.log
end script

EOF

	sudo cat << EOF >> "$STORM_WORK_HOME/etc/init/storm.conf"
#Storm Platform upstart job.
author "Trung Khuu (trung.khuu@gmail.com)"
description "Storm Platform Launcher"
start on runlevel [235]
stop on runlevel [06]
console output
#exec /opt/java/jdk1.7.0/bin/java -DisNexusAgent=true -jar /opt/$STORM_BRAND-$STORM_VERSION/bin/storm-bootstrap.jar > /opt/$STORM_BRAND-$STORM_VERSION/storm.log 2>&1
exec /opt/java/jdk1.7.0/bin/java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=7777 -DisNexusAgent=true -jar /opt/$STORM_BRAND-$STORM_VERSION/bin/storm-bootstrap.jar 2>&1 > /opt/$STORM_BRAND-$STORM_VERSION/storm.log

EOF

	
	sudo chmod 777 "$STORM_WORK_HOME/etc/init/storm.conf"
	sudo chmod 777 "$STORM_WORK_HOME/etc/init/stormtty.conf"
	echo "	- Embedding Java into the image"
	sudo sudo mkdir -p $STORM_WORK_HOME/opt/java
	sudo cp $STORM_JAVA_BIN_HOME/$JAVA_TAR $STORM_WORK_HOME/opt/java
	popd > /dev/null
	pushd $STORM_WORK_HOME/opt/java > /dev/null
	sudo tar xfz $JAVA_TAR
	sudo rm -rf $STORM_WORK_HOME/opt/java/jdk1.7.0/src.zip
	sudo rm -rf $STORM_WORK_HOME/opt/java/jdk1.7.0/demo
	sudo rm -rf $STORM_WORK_HOME/etc/init.d/whoopsie
	sudo rm -rf $STORM_WORK_HOME/etc/init/whoopsie.conf
	sudo rm -rf $STORM_WORK_HOME/etc/default/whoopsie
	sudo rm -rf $STORM_WORK_HOME/usr/bin/whoopsie
	sudo rm -rf $STORM_WORK_HOME/usr/share/doc/whoopsie
	sudo rm -rf $STORM_WORK_HOME/usr/share/gnome-control-center/ui/whoopsie.ui
	sudo rm -rf $STORM_WORK_HOME/usr/share/polkit-1/actions/com.ubuntu.whoopsiepreferences.policy
	sudo rm -rf $STORM_WORK_HOME/usr/bin/whoopsie-preferences
	sudo rm -rf $JAVA_TAR
	echo "PATH=$PATH:/opt/java/jdk1.7.0/bin" >> $STORM_WORK_HOME/etc/environment
	echo "JAVA_HOME=/opt/java/jdk1.7.0"  >> $STORM_WORK_HOME/etc/environment
	export JAVA_HOME=$USER_HOME/.storm/java/jdk1.7.0
	popd > /dev/null
	if [ ! -f $BASEDIR/../../target/$STORM_APP_TAR_MIN ]; then
		cd $BASEDIR/../../../ > /dev/null
		CWD=`pwd`
		echo "Running mvn clean install on $CWD"
		export MAVEN_OPTS="-Xms1512m -Xmx1512m"
		sudo -E -u $CURRENT_USER mvn clean install -DskipTests
		echo "	- Finished Building $STORM_APP_TAR_MIN "
		popd > /dev/null
	fi
		sudo chmod 440 $STORM_WORK_HOME/etc/sudoers
		sudo cp $BASEDIR/../../target/$STORM_APP_TAR_MIN $STORM_WORK_HOME/opt
		pushd $STORM_WORK_HOME/opt > /dev/null
		sudo tar xfz $STORM_APP_TAR_MIN
		sudo mv $STORM_APP_MIN $STORM_APP
		echo "STORM_HOME=/opt/$STORM_APP_TAR" >> $STORM_WORK_HOME/etc/environment
		sudo rm $STORM_APP_TAR_MIN
		sudo cp /usr/lib/locale/locale-archive $STORM_WORK_HOME/usr/lib/locale/locale-archive
		echo "	- Integrating NodeJS and TTY Terminal "
		buildNodeJs
		popd > /dev/null
		echo "	- Embedding $STORM_APP_TAR_MIN into the storm lxc base image"
		pushd $STORM_WORK_HOME > /dev/null
		sudo tar cfz $STORM_DIST_HOME/$STORM_DIST_IMG_NAME * > /dev/null 2>&1
		chmod 777 $STORM_DIST_HOME/$STORM_DIST_IMG_NAME
		sudo rm -rf $STORM_WORK_HOME/*
		sudo lxc-destroy -f -n storm > /dev/null
		echo "	- Generated Storm LXC Image $STORM_DIST_HOME/$STORM_DIST_IMG_NAME"
		popd > /dev/null
	fi
}

