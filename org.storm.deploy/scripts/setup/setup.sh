#!/bin/bash
# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================


# Main setup script which ensures dependencies, required libraries and dependant 
# LXC images are installed and configured. Typically kicked off on the root node of 
# the storm cloud or development environment. 



#Setup global env variables
USER_HOME=$(eval echo ~${SUDO_USER})
CURRENT_USER=$(eval echo ${SUDO_USER})
BASEDIR=`readlink -f $(dirname $0)`
MAVEN_DIST="http://apache.mirror.uber.com.au/maven/maven-3/3.0.5/binaries/apache-maven-3.0.5-bin.tar.gz"
IS_CLOUD=false
LVM_DEVICE="/dev/sdf"

#Storm specific env variables.
STORM_BRAND="storm-cloud"
STORM_BRAND_MIN="storm-cloud-min"
STORM_VERSION="1.0.0"
STORM_INSTALL=$BASEDIR/../../
STORM_HOME="$USER_HOME/.storm"
STORM_TTY="storm-tty-$STORM_VERSION"
STORM_JAVA_BIN_HOME="$STORM_HOME/java"
STORM_WORK_HOME="$STORM_HOME/work"
STORM_DIST_HOME="$STORM_HOME/dist"
STORM_NODE_HOME="$STORM_HOME/node"
STORM_BIN_HOME="$STORM_HOME/bin"
STORM_MAVEN_HOME="$STORM_HOME/maven"
STORM_DEB_NAME="storm-platform-1.0.0.deb"
STORM_DIST_IMG_NAME="$STORM_BRAND-$STORM_VERSION-lxc-rootfs.tar.gz"
STORM_APP_TAR="$STORM_BRAND-$STORM_VERSION.tar.gz"
STORM_APP="$STORM_BRAND-$STORM_VERSION"
STORM_APP_TAR_MIN="$STORM_BRAND_MIN-$STORM_VERSION.tar.gz"
STORM_APP_MIN="$STORM_BRAND_MIN-$STORM_VERSION"
JAVA_TAR="jdk-7u65-linux-x64.tar.gz"
CLOUD_IMG="ubuntu-12.04-server-cloudimg-amd64-root.tar.gz"
JAVA_HOME="$USER_HOME/.storm/java/jdk1.7.0"

#Setup script includes. 
source "$BASEDIR/setup-env.sh"
source "$BASEDIR/setup-java.sh"
source "$BASEDIR/setup-lvm.sh"
source "$BASEDIR/setup-openswitch.sh"
source "$BASEDIR/setup-storm-images.sh"
source "$BASEDIR/setup-storm-dist.sh"
source "$BASEDIR/setup-tcpudp.sh"

usage()
{
    cat <<EOF

Storm System Setup

	Usage: [ sudo ./setup.sh -options ]
	
	Where options is one of:

		[ -a ]: Performs a complete install. Attempt to setup the current node with all packages/dependencies/bundles. 
		[ -c ]: Deletes the storm dist and performs a core storm dist java recompilation + rebuild of lxc dependencies
		[ -f ]: Updates the current storm lxc image to the latest version of the storm jars.


EOF
    return 0
}



MODE="$1"


if [ "$MODE" == "-a" ]; then
	
	echo "Running initial system setup"
	#Checking to see if were running on a cloud env. i.e. amazon
	

	echo ""
	echo " In cloud : $IS_CLOUD"
	echo ""

	echo "1/8. Setting up openswitch"
	setupOpenSwitch

	echo "2/8. Setting up storm base environment."
	setupEnv

	echo "3/8. Setting up LVM partition and drivers."
	setupLvm $LVM_DEVICE

	echo "4/8. Setting up Java"
	downloadJava

	echo "5/8. Downloading LXC Images."
	downloadLxcImages

	echo "6/8. Setup Storm Images."
	setupStormDist

	echo "7/8. Tune Environment."
	optimizeTcpUdpSettings

	echo "8/8. Running Storm..."
	sleep 2
	cd $BASEDIR/../../../org.storm.deploy/bin
	sudo -E -u $CURRENT_USER $BASEDIR/../../../org.storm.deploy/bin/start.sh $STORM_JAVA_BIN_HOME/jdk1.7.0
elif [ "$MODE" == "-c" ]; then

	echo "Running core system setup"
	sudo iptables -t nat -D PREROUTING 1 2>&1 > /dev/null
	sudo iptables -t nat -D PREROUTING 1 2>&1 > /dev/null
	sudo chmod 777 /etc/resolv.conf
	sudo echo "nameserver	8.8.8.8" > /etc/resolv.conf
	pushd $BASEDIR/../ > /dev/null
	if [ "$2" == "compile" ]; then
		pushd $BASEDIR/../../../ > /dev/null
		sudo rm -rf $STORM_HOME/dist/storm-cloud-1.0.0-lxc-rootfs.tar.gz
		sudo -E -u $CURRENT_USER mvn clean install -DskipTests
		popd > /dev/null
	fi
	cd $BASEDIR
	echo "Rebuilding Storm Image ..."
	sudo rm -rf $STORM_HOME/work/*
	popd > /dev/null
	setupStormDist
	cd $BASEDIR/../../../org.storm.deploy/bin
	sudo -E -u $CURRENT_USER $BASEDIR/../../../org.storm.deploy/bin/start.sh $STORM_JAVA_BIN_HOME/jdk1.7.0
elif [ "$MODE" == "-f" ]; then

	echo "Running fast system setup"
	sudo rm -rf "$STORM_HOME/work"
	sudo mkdir -p "$STORM_HOME/work"
	sudo cp "$STORM_HOME/dist/storm-cloud-1.0.0-lxc-rootfs.tar.gz" "$STORM_HOME/work"
	pushd "$STORM_HOME/work" > /dev/null
	sudo tar xfz storm-cloud-1.0.0-lxc-rootfs.tar.gz
	sudo rm storm-cloud-1.0.0-lxc-rootfs.tar.gz
	popd > /dev/null
	pushd $BASEDIR/../../../ > /dev/null
	export JAVA_HOME
	export MAVEN_OPTS="-Xms1024m -Xmx1024m"
	sudo -E -u $CURRENT_USER mvn clean install -DskipTests
	echo "	- Finished Building $STORM_APP_TAR_MIN "
	popd > /dev/null
	sudo rm -rf $STORM_HOME/work/opt/storm-cloud-1.0.0/bundle/*
	sudo rm -rf $STORM_HOME/work/opt/storm-cloud-1.0.0/webapps/*
	sudo rm -rf $STORM_HOME/work/opt/storm-cloud-1.0.0/conf/*
	sudo rm -rf $STORM_HOME/work/opt/storm-cloud-1.0.0/bin/*
	sudo rm -rf $STORM_HOME/work/opt/storm-cloud-1.0.0/node/*
	sudo cp -f $BASEDIR/../../../org.storm.deploy/bundle/* $STORM_HOME/work/opt/storm-cloud-1.0.0/bundle/
	sudo cp -f $BASEDIR/../../../org.storm.deploy/bin/* $STORM_HOME/work/opt/storm-cloud-1.0.0/bin/
	sudo cp -f $BASEDIR/../../../org.storm.deploy/conf/* $STORM_HOME/work/opt/storm-cloud-1.0.0/conf/
	sudo cp -f $BASEDIR/../../../org.storm.deploy/node/* $STORM_HOME/work/opt/storm-cloud-1.0.0/node/
	sudo cp -f $BASEDIR/../../../org.storm.deploy/webapps/* $STORM_HOME/work/opt/storm-cloud-1.0.0/webapps/
	pushd $STORM_WORK_HOME > /dev/null
	sudo tar cfz $STORM_DIST_HOME/$STORM_DIST_IMG_NAME *
	sudo chmod 777 $STORM_DIST_HOME/$STORM_DIST_IMG_NAME
	sudo rm -rf $STORM_WORK_HOME/*
	
	sudo -E -u $CURRENT_USER $BASEDIR/../../../org.storm.deploy/bin/start.sh $STORM_JAVA_BIN_HOME/jdk1.7.0
else
	usage
fi
