#!/bin/bash
# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================

echo "Running apt update" >> /var/log/setupamazon.log
sudo apt-get -y update >> /var/log/setupamazon.log
echo "Installing curl ..." >> /var/log/setupamazon.log
sudo apt-get -y install curl wget >> /var/log/setupamazon.log
sudo apt-get -y install zookeeper >> /var/log/setupamazon.log
