#!/bin/bash
# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================

setupOpenSwitch()
{
	DOINSTALL="FALSE"
	type ovs-vsctl > /dev/null 2>&1 && echo "	- Openswitch Already Installed." || DOINSTALL="TRUE"
	if [ "$DOINSTALL" == "TRUE" ]; then
		echo "	- Setting up openvswitch and libraries and compiling the dkms modules ... "
		sudo apt-get -y install openvswitch-switch || true > /dev/null 2>&1
		#echo "	- Downloading the linux distro headers ... "
		#sudo apt-get -y install linux-headers-`uname -r` || true
		#echo "	- Installing the datapath and brcompat modules ... "
		#sudo apt-get -y install --reinstall openvswitch-datapath-dkms || true
		sudo /etc/init.d/openvswitch-switch start
		sudo ovs-vsctl add-br stormbridge
		echo "	- The ovs storm bridge is now online.  "
	fi
#lxc-create -t ubuntu-storm -n vswitch -f /etc/lxc/lxc-ovs.conf
#sudo ovs-vsctl add-port br0 gre0 -- set interface gre0 type=gre options:remote_ip=x.x.x.x
	
}
