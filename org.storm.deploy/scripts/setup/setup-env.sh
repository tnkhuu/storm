#!/bin/bash
# ==========================================================================
# Copyright 2013 Trung Khuu (trung.khuu@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of this License at : 
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==========================================================================

setupEnv()
{
	if [ ! -f "$STORM_HOME/.ranaptgetupdate" ]; then
		sudo apt-get update
		touch "$STORM_HOME/.ranaptgetupdate"
	fi
	
	echo "	- Storm home is set to $STORM_HOME"
	sudo apt-get install -y curl || true
	type lxc > /dev/null 2>&1 && echo "	- LXC Already Installed." || apt-get install -y lxc

	#Environment script and config prerequsites
	DIR=`readlink -f $(dirname $0)`
	#On Ubuntu 12.10, templates are stored here
	if [ -d "/usr/share/lxc/templates/" ]; then
		sudo cp $DIR/../package/dist/ubuntu/lxc-ubuntu-storm /usr/share/lxc/templates/
		echo "	- Copied $DIR/../package/dist/ubuntu/lxc-ubuntu-storm to /usr/share/lxc/templates/"
		sudo chmod 777 /usr/share/lxc/templates/lxc-ubuntu-storm
	#On Ubuntu 12.04, templates are stored here instead. 
	elif [ -d "/usr/lib/lxc/templates/" ]; then
		sudo cp $DIR/../package/dist/ubuntu/lxc-ubuntu-storm /usr/lib/lxc/templates/
		echo "	- Copied $DIR/../package/dist/ubuntu/lxc-ubuntu-storm to /usr/lib/lxc/templates/"
		sudo chmod 777 /usr/lib/lxc/templates/lxc-ubuntu-storm
	fi
	sudo cp $DIR/../bin/mcurl.sh /usr/bin/mcurl
	sudo cp $DIR/../bin/lxc-ip /usr/bin/lxc-ip
	sudo chmod +x /usr/bin/mcurl
	sudo chmod +x /usr/bin/lxc-ip
	#Setup required dirs.
	sudo mkdir -p "$STORM_DIST_HOME"
	sudo mkdir -p "$STORM_BIN_HOME"
	sudo mkdir -p "$STORM_JAVA_BIN_HOME"
	sudo mkdir -p "$STORM_WORK_HOME"
	sudo mkdir -p "$STORM_MAVEN_HOME"
	sudo mkdir -p "/mnt/storm/lxc"

	if [ ! -f "$STORM_MAVEN_HOME/bin/mvn" ]; then
	pushd $STORM_MAVEN_HOME
	sudo mcurl $MAVEN_DIST
	sudo tar xvf apache-maven-3.0.5-bin.tar.gz
	sudo mv apache-maven-3.0.5/* .
	sudo rm -rf apache-maven-3.0.5-bin.tar.gz apache-maven-3.0.5
	sudo chmod -R 755 .
	sudo ln -s $STORM_MAVEN_HOME/bin/mvn /usr/bin/mvn

	popd > /dev/null
	fi
	
	#Ensure the user has a valid rsa key.
	if [ ! -f "$USER_HOME/.ssh/id_rsa.pub" ]; then
		sudo -E -u $CURRENT_USER mkdir $USER_HOME/.ssh/
		sudo -E -u $CURRENT_USER ssh-keygen -P "" -f "$USER_HOME/.ssh/id_rsa" || { echo "Could not setup RSA Keys"; exit 1; }
	fi;
	
	#ensure user sudo permissions.
	if [ -f "$STORM_HOME/.wrotesudoperm" ]; then 
		echo "	- Already configured sudo permissions."
	else	
		sudo echo "$CURRENT_USER	ALL=NOPASSWD: ALL" >> /etc/sudoers
		sudo touch "$STORM_HOME/.wrotesudoperm"
	fi
	HOSTNAME=`hostname`
	sudo echo "127.0.0.1	$HOSTNAME" >> /etc/hosts
	
	#increase max open files
	if [ -f "$STORM_HOME/.wrotenofile" ]; then 
		echo "	- Already set max file limits."
	else	
		sudo echo "$CURRENT_USER		 hard	 nofile		 819715" >> /etc/security/limits.conf
		sudo echo "$CURRENT_USER		 soft	 nofile		 819715" >> /etc/security/limits.conf
		sudo echo "root		 soft	 nofile		 819715" >> /etc/security/limits.conf
		sudo echo "root		 hard	 nofile		 819715" >> /etc/security/limits.conf
		touch "$STORM_HOME/.wrotenofile"
	fi
	
	#Open up permissions to editing the resolv.conf file.
	sudo chmod 777 /etc/resolv.conf 
	echo "	- Environment Configuration Complete"
}
