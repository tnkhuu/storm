/**
 * Copyright 2013 Trung Khuu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * this License at :
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.storm.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;
import org.storm.framework.util.Util;
import org.storm.tools.system.StormPlatform;

/**
 * <p>
 * This class is the default way to instantiate and execute the framework. It is
 * not intended to be the only way to instantiate and execute the framework;
 * rather, it is one example of how to do so. When embedding the framework in a
 * host application, this class can serve as a simple guide of how to do so. It
 * may even be worthwhile to reuse some of its property handling capabilities.
 * </p>
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class Main
{
	/**
	 * Switch for specifying bundle directory.
	 **/
	public static final String BUNDLE_DIR_SWITCH = "-b";

	/**
	 * The property name used to specify whether the launcher should install a
	 * shutdown hook.
	 **/
	public static final String SHUTDOWN_HOOK_PROP = "shutdown.hook";
	/**
	 * The property name used to specify an URL to the system property file.
	 **/
	public static final String SYSTEM_PROPERTIES_PROP = "system.properties";
	/**
	 * The default name used for the system properties file.
	 **/
	public static final String SYSTEM_PROPERTIES_FILE_VALUE = "system.properties";
	/**
	 * The property name used to specify an URL to the configuration property
	 * file to be used for the created the framework instance.
	 **/
	public static final String CONFIG_PROPERTIES_PROP = "config.properties";

	/** The bundle start order */
	public static final String STARTUP_PROPERTIES_PROP = "startup.properties";
	/**
	 * The default name used for the configuration properties file.
	 **/
	public static final String CONFIG_PROPERTIES_FILE_VALUE = "config.properties";
	/**
	 * Name of the configuration directory.
	 */
	public static final String CONFIG_DIRECTORY = "conf";

	/** The m_fwk. */
	private static Framework m_fwk = null;

	/**
	 * <p>
	 * This method performs the main task of constructing an framework instance
	 * and starting its execution. The following functions are performed when
	 * invoked:
	 * </p>
	 * <ol>
	 * <li><i><b>Examine and verify command-line arguments.</b></i> The launcher
	 * accepts a "<tt>-b</tt>" command line switch to set the bundle auto-deploy
	 * directory and a single argument to set the bundle cache directory.</li>
	 * <li><i><b>Read the system properties file.</b></i> This is a file
	 * containing properties to be pushed into <tt>System.setProperty()</tt>
	 * before starting the framework. This mechanism is mainly shorthand for
	 * people starting the framework from the command line to avoid having to
	 * specify a bunch of <tt>-D</tt> system property definitions. The only
	 * properties defined in this file that will impact the framework's behavior
	 * are the those concerning setting HTTP proxies, such as
	 * <tt>http.proxyHost</tt>, <tt>http.proxyPort</tt>, and
	 * <tt>http.proxyAuth</tt>. Generally speaking, the framework does not use
	 * system properties at all.</li>
	 * <li><i><b>Read the framework's configuration property file.</b></i> This
	 * is a file containing properties used to configure the framework instance
	 * and to pass configuration information into bundles installed into the
	 * framework instance. The configuration property file is called
	 * <tt>config.properties</tt> by default and is located in the
	 * <tt>conf/</tt> directory of the Felix installation directory, which is
	 * the parent directory of the directory containing the <tt>felix.jar</tt>
	 * file. It is possible to use a different location for the property file by
	 * specifying the desired URL using the <tt>felix.config.properties</tt>
	 * system property; this should be set using the <tt>-D</tt> syntax when
	 * executing the JVM. If the <tt>config.properties</tt> file cannot be
	 * found, then default values are used for all configuration properties.
	 * Refer to the <a href="Felix.html#Felix(java.util.Map)"><tt>Felix</tt></a>
	 * constructor documentation for more information on framework configuration
	 * properties.</li>
	 * <li><i><b>Copy configuration properties specified as system properties
	 * into the set of configuration properties.</b></i> Even though the Felix
	 * framework does not consult system properties for configuration
	 * information, sometimes it is convenient to specify them on the command
	 * line when launching Felix. To make this possible, the Felix launcher
	 * copies any configuration properties specified as system properties into
	 * the set of configuration properties passed into Felix.</li>
	 * <li><i><b>Add shutdown hook.</b></i> To make sure the framework shutdowns
	 * cleanly, the launcher installs a shutdown hook; this can be disabled with
	 * the <tt>felix.shutdown.hook</tt> configuration property.</li>
	 * <li><i><b>Create and initialize a framework instance.</b></i> The OSGi
	 * standard <tt>FrameworkFactory</tt> is retrieved from
	 * <tt>META-INF/services</tt> and used to create a framework instance with
	 * the configuration properties.</li>
	 * <li><i><b>Auto-deploy bundles.</b></i> All bundles in the auto-deploy
	 * directory are deployed into the framework instance.</li>
	 * <li><i><b>Start the framework.</b></i> The framework is started and the
	 * launcher thread waits for the framework to shutdown.</li>
	 * </ol>
	 * <p>
	 * It should be noted that simply starting an instance of the framework is
	 * not enough to create an interactive session with it. It is necessary to
	 * install and start bundles that provide a some means to interact with the
	 * framework; this is generally done by bundles in the auto-deploy directory
	 * or specifying an "auto-start" property in the configuration property
	 * file. If no bundles providing a means to interact with the framework are
	 * installed or if the configuration property file cannot be found, the
	 * framework will appear to be hung or deadlocked. This is not the case, it
	 * is executing correctly, there is just no way to interact with it.
	 * </p>
	 * <p>
	 * The launcher provides two ways to deploy bundles into a framework at
	 * startup, which have associated configuration properties:
	 * </p>
	 * <ul>
	 * <li>Bundle auto-deploy - Automatically deploys all bundles from a
	 * specified directory, controlled by the following configuration
	 * properties:
	 * <ul>
	 * <li><tt>felix.auto.deploy.dir</tt> - Specifies the auto-deploy directory
	 * from which bundles are automatically deploy at framework startup. The
	 * default is the <tt>bundle/</tt> directory of the current directory.</li>
	 * <li><tt>felix.auto.deploy.action</tt> - Specifies the auto-deploy actions
	 * to be found on bundle JAR files found in the auto-deploy directory. The
	 * possible actions are <tt>install</tt>, <tt>update</tt>, <tt>start</tt>,
	 * and <tt>uninstall</tt>. If no actions are specified, then the auto-deploy
	 * directory is not processed. There is no default value for this property.</li>
	 * </ul>
	 * </li>
	 * <li>Bundle auto-properties - Configuration properties which specify URLs
	 * to bundles to install/start:
	 * <ul>
	 * <li><tt>felix.auto.install.N</tt> - Space-delimited list of bundle URLs
	 * to automatically install when the framework is started, where <tt>N</tt>
	 * is the start level into which the bundle will be installed (e.g.,
	 * felix.auto.install.2).</li>
	 * <li><tt>felix.auto.start.N</tt> - Space-delimited list of bundle URLs to
	 * automatically install and start when the framework is started, where
	 * <tt>N</tt> is the start level into which the bundle will be installed
	 * (e.g., felix.auto.start.2).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <p>
	 * These properties should be specified in the <tt>config.properties</tt> so
	 * that they can be processed by the launcher during the framework startup
	 * process.
	 * </p>
	 * 
	 * @param args
	 *            Accepts arguments to set the auto-deploy directory and/or the
	 *            bundle cache directory.
	 * @throws Exception
	 *             If an error occurs.
	 **/
	public static void main(String[] args) throws Exception
	{
		File stormHome = StormPlatform.INSTANCE.getStormHome();

		/** Maps to Storm.STORM_HOME */
		System.setProperty("STORM_HOME", stormHome.getAbsolutePath());
		// Look for bundle directory and/or cache directory.
		// We support at most one argument, which is the bundle
		// cache directory.
		String bundleDir = stormHome.getAbsolutePath() + "/bundle";
		String cacheDir = stormHome.getAbsolutePath() + "/cache";
		String logbackConfig = stormHome.getAbsolutePath() + "/conf/logback.xml";
		String jettyHome = stormHome.getAbsolutePath();
		String cmDir = stormHome.getAbsolutePath() + "/conf";
		String obrRepo = stormHome.getAbsolutePath() + "/repository.xml";

		setDefaultValue("felix.cm.dir", cmDir);
		setDefaultValue("logback.configurationFile", logbackConfig);
		setDefaultValue("obr.repository.url", obrRepo);
		setDefaultValue("jetty.home", jettyHome);
		setDefaultValue("jetty.port", "8081");
		// Read configuration properties.
		Map<String, String> configProps = Main.loadConfigProperties(new File(stormHome.getAbsoluteFile() + "/conf/config.properties"));
		if (configProps == null)
		{
			configProps = new HashMap<String, String>();
		}

		// Copy framework properties from the system properties.
		Main.copySystemProperties(configProps);
		configProps.put(AutoProcessor.AUTO_DEPLOY_DIR_PROPERY, bundleDir);
		configProps.put(Constants.FRAMEWORK_STORAGE, cacheDir);

		// If enabled, register a shutdown hook to make sure the framework is
		// cleanly shutdown when the VM exits.
		String enableHook = configProps.get(SHUTDOWN_HOOK_PROP);
		if (enableHook == null || !enableHook.equalsIgnoreCase("false"))
		{
			Runtime.getRuntime().addShutdownHook(new Thread("Felix Shutdown Hook")
			{
				@Override
				public void run()
				{
					try
					{
						if (m_fwk != null)
						{
							m_fwk.stop();
							m_fwk.waitForStop(5000);
						}
					}
					catch (Exception ex)
					{
						System.err.println("Error stopping framework: " + ex);
					}
				}
			});
		}

		try
		{
			// Create an instance of the framework.
			FrameworkFactory factory = getFrameworkFactory();
			m_fwk = factory.newFramework(configProps);
			// Initialize the framework, but don't start it yet.
			m_fwk.init();
			// Use the system bundle context to process the auto-deploy
			// and auto-install/auto-start properties.
			AutoProcessor.process(configProps, loadStartupOrder(), m_fwk.getBundleContext());
			FrameworkEvent event;
			do
			{
				// Start the framework.
				m_fwk.start();
				// Wait for framework to stop to exit the VM.
				event = m_fwk.waitForStop(0);
			}
			// If the framework was updated, then restart it.
			while (event.getType() == FrameworkEvent.STOPPED_UPDATE);
			// Otherwise, exit.
			System.exit(0);
		}
		catch (Exception ex)
		{
			System.err.println("Could not create framework: " + ex);
			ex.printStackTrace();
			System.exit(0);
		}
	}
	
	@SuppressWarnings("unused")
	private static void runCmd(String cmd) {
		try
		{
			Runtime.getRuntime().exec(cmd);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private static void setDefaultValue(String key, String defaults)
	{
		String value = System.getProperty(key);
		if (value == null)
		{
			System.setProperty(key, defaults);
		}
	}

	/**
	 * Simple method to parse META-INF/services file for framework factory.
	 * Currently, it assumes the first non-commented line is the class name of
	 * the framework factory implementation.
	 * 
	 * @return The created <tt>FrameworkFactory</tt> instance.
	 * @throws Exception
	 *             if any errors occur.
	 **/
	private static FrameworkFactory getFrameworkFactory() throws Exception
	{
		URL url = Main.class.getClassLoader().getResource("META-INF/services/org.osgi.framework.launch.FrameworkFactory");
		if (url != null)
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			try
			{
				for (String s = br.readLine(); s != null; s = br.readLine())
				{
					s = s.trim();
					// Try to load first non-empty, non-commented line.
					if (s.length() > 0 && s.charAt(0) != '#')
					{
						return (FrameworkFactory) Class.forName(s).newInstance();
					}
				}
			}
			finally
			{
				if (br != null)
				{
					br.close();
				}
			}
		}

		throw new Exception("Could not find framework factory.");
	}

	public static Properties loadStartupOrder()
	{
		Properties props = new Properties();
		try
		{
			URL propURL = null;
			String custom = System.getProperty(STARTUP_PROPERTIES_PROP);
			if (custom != null)
			{
				propURL = new URL(custom);
				props.load(propURL.openStream());
			}
		}
		catch (MalformedURLException ex)
		{
			System.err.print("Main: " + ex);
			throw new IllegalStateException("Unable to load the bundle start order.");
		}
		catch (IOException e)
		{
			throw new IllegalStateException("Unable to load the bundle start order.");
		}

		return props;
	}

	/**
	 * <p>
	 * Loads the configuration properties in the configuration property file
	 * associated with the framework installation; these properties are
	 * accessible to the framework and to bundles and are intended for
	 * configuration purposes. By default, the configuration property file is
	 * located in the <tt>conf/</tt> directory of the installation directory and
	 * is called "<tt>config.properties</tt>". The installation directory is
	 * assumed to be the parent directory of the <tt>storm-bootstrap.jar</tt>
	 * file.
	 * </p>
	 * 
	 * @return A <tt>Properties</tt> instance or <tt>null</tt> if there was an
	 *         error.
	 **/
	public static Map<String, String> loadConfigProperties(File configProps)
	{
		// See if the property URL was specified as a property.

		// Read the properties file.
		Properties props = new Properties();

		try (InputStream is = new FileInputStream(configProps))
		{
			props.load(is);
			is.close();
		}
		catch (Exception ex)
		{
			throw new IllegalStateException("Could not load config.properties: " + configProps);
		}

		// Perform variable substitution for system properties and
		// convert to dictionary.
		Map<String, String> map = new HashMap<String, String>();
		for (Enumeration<?> e = props.propertyNames(); e.hasMoreElements();)
		{
			String name = (String) e.nextElement();
			map.put(name, Util.substVars(props.getProperty(name), name, null, props));
		}

		return map;
	}

	/**
	 * Gets the jar install directory.
	 * 
	 * @return the jar install directory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String getJarInstallDirectory() throws IOException
	{
		String jarPath = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		if (SystemUtils.IS_OS_WINDOWS)
		{
			int index = jarPath.lastIndexOf('/');
			if (index == -1)
			{
				return jarPath;
			}
			return jarPath.substring(0, index);
		}
		else
		{
			int index = jarPath.lastIndexOf('/');
			if (index == -1)
			{
				return jarPath;
			}
			return jarPath.substring(0, index);
		}
	}

	/**
	 * Copy system properties.
	 * 
	 * @param configProps
	 *            the config props
	 */
	public static void copySystemProperties(Map<String, String> configProps)
	{
		for (Enumeration<?> e = System.getProperties().propertyNames(); e.hasMoreElements();)
		{
			String key = (String) e.nextElement();
			if (key.startsWith("felix.") || key.startsWith("org.osgi.framework."))
			{
				configProps.put(key, System.getProperty(key));
			}
		}
	}
}
