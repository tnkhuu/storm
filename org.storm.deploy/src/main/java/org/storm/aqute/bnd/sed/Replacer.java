package org.storm.aqute.bnd.sed;

public interface Replacer
{
	String process(String line);
}
