package org.storm.aqute.bnd.service;

import java.util.Set;

import org.storm.aqute.bnd.build.Project;

public interface DependencyContributor
{
	void addDependencies(Project project, Set<String> dependencies);
}
