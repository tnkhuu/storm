package org.storm.aqute.bnd.service.diff;

import org.storm.aqute.bnd.osgi.Analyzer;
import org.storm.aqute.bnd.osgi.Jar;

/**
 * Compare two Jars and report the differences.
 */
public interface Differ
{
	Tree tree(Analyzer source) throws Exception;

	Tree tree(Jar source) throws Exception;

	Tree deserialize(Tree.Data data) throws Exception;
}
