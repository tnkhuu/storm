package org.storm.aqute.bnd.build;

import org.storm.aqute.bnd.service.action.Action;

public class ScriptAction implements Action
{
	final String script;
	final String type;

	public ScriptAction(String type, String script)
	{
		this.script = script;
		this.type = type;
	}

	@Override
	public void execute(Project project, String action) throws Exception
	{
		project.script(type, script);
	}

}
