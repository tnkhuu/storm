package org.storm.aqute.bnd.service;

import java.io.File;

public interface Refreshable
{
	boolean refresh() throws Exception;

	File getRoot();
}
