package org.storm.aqute.bnd.service.action;

public interface NamedAction extends Action
{
	String getName();
}
