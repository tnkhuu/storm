package org.storm.aqute.bnd.build.clauses;

import org.osgi.framework.Constants;
import org.storm.aqute.bnd.header.Attrs;

public class ExportedPackage extends HeaderClause
{

	public ExportedPackage(String packageName, Attrs attribs)
	{
		super(packageName, attribs);
	}

	@Override
	protected boolean newlinesBetweenAttributes()
	{
		return false;
	}

	public void setVersionString(String version)
	{
		attribs.put(Constants.VERSION_ATTRIBUTE, version);
	}

	public String getVersionString()
	{
		return attribs.get(Constants.VERSION_ATTRIBUTE);
	}

	public boolean isProvided()
	{
		return Boolean.valueOf(attribs.get(org.storm.aqute.bnd.osgi.Constants.PROVIDE_DIRECTIVE));
	}

	public void setProvided(boolean provided)
	{
		if (provided)
		{
			attribs.put(org.storm.aqute.bnd.osgi.Constants.PROVIDE_DIRECTIVE, Boolean.toString(true));
		}
		else
		{
			attribs.remove(org.storm.aqute.bnd.osgi.Constants.PROVIDE_DIRECTIVE);
		}
	}

	@Override
	public ExportedPackage clone()
	{
		return new ExportedPackage(this.name, new Attrs(this.attribs));
	}
}
