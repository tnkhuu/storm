package org.storm.aqute.bnd.service;

public enum Strategy
{
	LOWEST, EXACT, HIGHEST;
}
