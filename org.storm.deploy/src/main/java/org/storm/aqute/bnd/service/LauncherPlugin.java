package org.storm.aqute.bnd.service;

import org.storm.aqute.bnd.build.Project;
import org.storm.aqute.bnd.build.ProjectLauncher;
import org.storm.aqute.bnd.build.ProjectTester;

public interface LauncherPlugin
{
	ProjectLauncher getLauncher(Project project) throws Exception;

	ProjectTester getTester(Project project);
}
