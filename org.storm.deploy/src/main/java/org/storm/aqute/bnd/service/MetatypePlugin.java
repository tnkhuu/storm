package org.storm.aqute.bnd.service;

import java.util.Collection;

import org.storm.aqute.bnd.header.Parameters;
import org.storm.aqute.bnd.osgi.Analyzer;
import org.storm.aqute.bnd.osgi.Clazz;
import org.storm.aqute.bnd.osgi.Clazz.QUERY;
import org.storm.aqute.bnd.osgi.Constants;
import org.storm.aqute.bnd.osgi.Jar;

import aQute.bnd.annotation.metatype.Meta;

/**
 * This class is responsible for meta type types. It is a plugin that can
 * 
 * @author aqute
 */
public class MetatypePlugin implements AnalyzerPlugin
{

	@Override
	public boolean analyzeJar(Analyzer analyzer) throws Exception
	{

		Parameters map = analyzer.parseHeader(analyzer.getProperty(Constants.METATYPE));

		Jar jar = analyzer.getJar();
		for (String name : map.keySet())
		{
			Collection<Clazz> metatypes = analyzer.getClasses("", QUERY.ANNOTATED.toString(), Meta.OCD.class.getName(), //
					QUERY.NAMED.toString(), name //
					);
			for (Clazz c : metatypes)
			{
				jar.putResource("OSGI-INF/metatype/" + c.getFQN() + ".xml", new MetaTypeReader(c, analyzer));
			}
		}
		return false;
	}
}
