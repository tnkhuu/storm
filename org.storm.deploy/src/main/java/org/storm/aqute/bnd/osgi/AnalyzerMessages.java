package org.storm.aqute.bnd.osgi;

import java.util.Collection;

import org.storm.aqute.bnd.osgi.Descriptors.PackageRef;
import org.storm.aqute.bnd.reporter.Messages;

public interface AnalyzerMessages extends Messages
{

	WARNING Export_Has_PrivateReferences_(PackageRef exported, int count, Collection<PackageRef> local);
	/**/
}
