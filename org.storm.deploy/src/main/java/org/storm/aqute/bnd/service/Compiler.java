package org.storm.aqute.bnd.service;

import java.io.File;
import java.util.Collection;

import org.storm.aqute.bnd.build.Container;
import org.storm.aqute.bnd.build.Project;

public interface Compiler
{
	boolean compile(Project project, Collection<File> sources, Collection<Container> buildpath, File bin) throws Exception;
}
