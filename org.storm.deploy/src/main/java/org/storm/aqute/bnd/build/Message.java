package org.storm.aqute.bnd.build;

public @interface Message
{
	String value();
}
