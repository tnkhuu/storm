package org.storm.aqute.bnd.service;

import java.io.File;

import org.storm.aqute.bnd.osgi.Jar;

public interface RepositoryListenerPlugin
{

	/**
	 * Called when a bundle is added to a repository.
	 * 
	 * @param repository
	 * @param jar
	 * @param file
	 */
	void bundleAdded(RepositoryPlugin repository, Jar jar, File file);
}
