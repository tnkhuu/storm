package org.storm.aqute.bnd.build.clauses;

import org.osgi.framework.Constants;
import org.storm.aqute.bnd.header.Attrs;

public class ImportPattern extends VersionedClause implements Cloneable
{

	public ImportPattern(String pattern, Attrs attributes)
	{
		super(pattern, attributes);
	}

	public boolean isOptional()
	{
		String resolution = attribs.get(org.storm.aqute.bnd.osgi.Constants.RESOLUTION_DIRECTIVE);
		return Constants.RESOLUTION_OPTIONAL.equals(resolution);
	}

	public void setOptional(boolean optional)
	{
		if (optional)
		{
			attribs.put(org.storm.aqute.bnd.osgi.Constants.RESOLUTION_DIRECTIVE, Constants.RESOLUTION_OPTIONAL);
		}
		else
		{
			attribs.remove(org.storm.aqute.bnd.osgi.Constants.RESOLUTION_DIRECTIVE);
		}
	}

	@Override
	public ImportPattern clone()
	{
		return new ImportPattern(this.name, new Attrs(this.attribs));
	}
}
