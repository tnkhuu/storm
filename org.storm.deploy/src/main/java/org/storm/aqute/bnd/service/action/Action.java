package org.storm.aqute.bnd.service.action;

import org.storm.aqute.bnd.build.Project;

public interface Action
{
	void execute(Project project, String action) throws Exception;
}
