package org.storm.aqute.bnd.collections;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;

public class LineCollection implements Iterator<String>, Closeable
{
	final BufferedReader reader;
	String next;

	public LineCollection(InputStream in) throws IOException
	{
		this(new InputStreamReader(in, "UTF8"));
	}

	public LineCollection(File in) throws IOException
	{
		this(new InputStreamReader(new FileInputStream(in), "UTF-8"));
	}

	public LineCollection(Reader reader) throws IOException
	{
		this(new BufferedReader(reader));
	}

	public LineCollection(BufferedReader reader) throws IOException
	{
		this.reader = reader;
		next = reader.readLine();
	}

	@Override
	public boolean hasNext()
	{
		return next != null;
	}

	@Override
	public String next()
	{
		if (next == null)
		{
			throw new IllegalStateException("Iterator has finished");
		}
		try
		{
			String result = next;
			next = reader.readLine();
			if (next == null)
			{
				reader.close();
			}
			return result;
		}
		catch (Exception e)
		{
			// ignore
			return null;
		}
	}

	@Override
	public void remove()
	{
		if (next == null)
		{
			throw new UnsupportedOperationException("Cannot remove");
		}
	}

	@Override
	public void close() throws IOException
	{
		reader.close();
	}
}
