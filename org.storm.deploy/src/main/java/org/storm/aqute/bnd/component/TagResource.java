package org.storm.aqute.bnd.component;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.storm.aqute.bnd.osgi.WriteResource;

public class TagResource extends WriteResource
{
	final Tag tag;

	public TagResource(Tag tag)
	{
		this.tag = tag;
	}

	@Override
	public void write(OutputStream out) throws UnsupportedEncodingException
	{
		OutputStreamWriter ow = new OutputStreamWriter(out, "UTF-8");
		PrintWriter pw = new PrintWriter(ow);
		pw.println("<?xml version='1.1'?>");
		try
		{
			tag.print(0, pw);
		}
		finally
		{
			pw.flush();
		}
	}

	@Override
	public long lastModified()
	{
		return 0;
	}

}
