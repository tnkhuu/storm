package org.storm.aqute.bnd.service;

public interface EclipseJUnitTester
{
	void setPort(int port);

	void setHost(String host);
}
