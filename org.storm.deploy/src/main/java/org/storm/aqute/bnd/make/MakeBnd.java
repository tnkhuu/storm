package org.storm.aqute.bnd.make;

import java.io.File;
import java.util.Map;
import java.util.regex.Pattern;

import org.storm.aqute.bnd.build.ProjectBuilder;
import org.storm.aqute.bnd.osgi.Builder;
import org.storm.aqute.bnd.osgi.Constants;
import org.storm.aqute.bnd.osgi.Jar;
import org.storm.aqute.bnd.osgi.JarResource;
import org.storm.aqute.bnd.osgi.Processor;
import org.storm.aqute.bnd.osgi.Resource;
import org.storm.aqute.bnd.service.MakePlugin;

public class MakeBnd implements MakePlugin, Constants
{
	final static Pattern JARFILE = Pattern.compile("(.+)\\.(jar|ipa)");

	@Override
	public Resource make(Builder builder, String destination, Map<String, String> argumentsOnMake) throws Exception
	{
		String type = argumentsOnMake.get("type");
		if (!"bnd".equals(type))
		{
			return null;
		}

		String recipe = argumentsOnMake.get("recipe");
		if (recipe == null)
		{
			builder.error("No recipe specified on a make instruction for " + destination);
			return null;
		}
		File bndfile = builder.getFile(recipe);
		if (bndfile.isFile())
		{
			// We do not use a parent because then we would
			// build ourselves again. So we can not blindly
			// inherit the properties.
			Builder bchild = builder.getSubBuilder();
			bchild.removeBundleSpecificHeaders();

			// We must make sure that we do not include ourselves again!
			bchild.setProperty(Constants.INCLUDE_RESOURCE, "");
			bchild.setProperty(Constants.INCLUDERESOURCE, "");
			bchild.setProperties(bndfile, builder.getBase());

			Jar jar = bchild.build();
			Jar dot = builder.getTarget();

			if (builder.hasSources())
			{
				for (String key : jar.getResources().keySet())
				{
					if (key.startsWith("OSGI-OPT/src"))
					{
						dot.putResource(key, jar.getResource(key));
					}
				}
			}
			builder.getInfo(bchild, bndfile.getName() + ": ");
			String debug = bchild.getProperty(DEBUG);
			if (Processor.isTrue(debug))
			{
				if (builder instanceof ProjectBuilder)
				{
					ProjectBuilder pb = (ProjectBuilder) builder;
					File target = pb.getProject().getTarget();
					String bsn = bchild.getBsn();
					File output = new File(target, bsn + ".jar");
					jar.write(output);
					pb.getProject().getWorkspace().changedFile(output);
				}
			}
			return new JarResource(jar);
		}
		return null;
	}

}
