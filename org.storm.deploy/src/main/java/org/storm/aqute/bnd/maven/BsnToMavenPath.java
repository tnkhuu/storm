package org.storm.aqute.bnd.maven;

public interface BsnToMavenPath
{
	String[] getGroupAndArtifact(String bsn);
}
