package org.storm.aqute.bnd.differ;

import java.util.ArrayList;
import java.util.List;

import org.storm.aqute.bnd.service.RepositoryPlugin;
import org.storm.aqute.bnd.service.diff.Delta;
import org.storm.aqute.bnd.service.diff.Type;
import org.storm.aqute.bnd.version.Version;

public class RepositoryElement
{

	public static Element getTree(RepositoryPlugin repo) throws Exception
	{
		List<Element> programs = new ArrayList<Element>();
		for (String bsn : repo.list(null))
		{
			List<Element> versions = new ArrayList<Element>();
			for (Version version : repo.versions(bsn))
			{
				versions.add(new Element(Type.VERSION, version.toString()));
			}
			programs.add(new Element(Type.PROGRAM, bsn, versions, Delta.MINOR, Delta.MAJOR, null));
		}
		return new Element(Type.REPO, repo.getName(), programs, Delta.MINOR, Delta.MAJOR, repo.getLocation());
	}

}
