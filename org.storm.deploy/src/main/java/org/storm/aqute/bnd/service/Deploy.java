package org.storm.aqute.bnd.service;

import java.io.InputStream;

import org.storm.aqute.bnd.build.Project;

/**
 * Deploy this artifact to maven.
 */
public interface Deploy
{
	boolean deploy(Project project, String jarName, InputStream jarStream) throws Exception;
}
