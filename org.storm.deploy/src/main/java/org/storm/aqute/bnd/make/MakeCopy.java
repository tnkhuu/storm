package org.storm.aqute.bnd.make;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.storm.aqute.bnd.osgi.Builder;
import org.storm.aqute.bnd.osgi.EmbeddedResource;
import org.storm.aqute.bnd.osgi.FileResource;
import org.storm.aqute.bnd.osgi.Resource;
import org.storm.aqute.bnd.osgi.URLResource;
import org.storm.aqute.bnd.service.MakePlugin;

public class MakeCopy implements MakePlugin
{

	@Override
	public Resource make(Builder builder, String destination, Map<String, String> argumentsOnMake) throws Exception
	{
		String type = argumentsOnMake.get("type");
		if (!type.equals("copy"))
		{
			return null;
		}

		String from = argumentsOnMake.get("from");
		if (from == null)
		{
			String content = argumentsOnMake.get("content");
			if (content == null)
			{
				throw new IllegalArgumentException("No 'from' or 'content' field in copy " + argumentsOnMake);
			}
			return new EmbeddedResource(content.getBytes("UTF-8"), 0);
		}
		File f = builder.getFile(from);
		if (f.isFile())
		{
			return new FileResource(f);
		}
		try
		{
			URL url = new URL(from);
			return new URLResource(url);
		}
		catch (MalformedURLException mfue)
		{
			// We ignore this
		}
		throw new IllegalArgumentException("Copy source does not exist " + from + " for destination " + destination);
	}

}
