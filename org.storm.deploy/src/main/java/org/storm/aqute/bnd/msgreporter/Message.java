package org.storm.aqute.bnd.msgreporter;

public @interface Message
{
	String value();
}
