package org.storm.aqute.bnd.build;

import java.lang.reflect.Method;

import org.storm.aqute.bnd.service.action.Action;

public class ReflectAction implements Action
{
	String what;

	public ReflectAction(String what)
	{
		this.what = what;
	}

	@Override
	public void execute(Project project, String action) throws Exception
	{
		Method m = project.getClass().getMethod(what);
		m.invoke(project);
	}

	@Override
	public String toString()
	{
		return "ra:" + what;
	}
}
