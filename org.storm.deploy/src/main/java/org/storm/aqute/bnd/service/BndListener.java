package org.storm.aqute.bnd.service;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

import org.storm.aqute.bnd.reporter.Reporter;

public class BndListener
{
	final AtomicInteger inside = new AtomicInteger();

	public void changed(File file)
	{
	}

	public void begin()
	{
		inside.incrementAndGet();
	}

	public void end()
	{
		inside.decrementAndGet();
	}

	public boolean isInside()
	{
		return inside.get() != 0;
	}

	public void signal(Reporter reporter)
	{

	}
}
