#! /bin/bash

#if [ "$(id -u)" != "0" ]; then
#    echo "This script should be run as 'root'"
#    exit 1
#fi
BASEDIR=$(dirname $0)
BASEDIRPATH=`readlink $(dirname $0)`
PID=""
if [ ! "$1" = "" ]; then
	JAVA_HOME="$1"
fi	

if [ "$JAVA_HOME" = "" ]; then
	echo ""
	echo " 	JAVA_HOME is not set. "
	echo " 	Please set JAVA_HOME to a valid Java 7 JRE or SDK or this may fail."
	echo ""
	java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=7777 -Dlogback.configurationFile=$BASEDIR/conf/logback.xml -jar $BASEDIR/storm-bootstrap.jar
	PID=$!
else
	$JAVA_HOME/bin/java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=7777 -Dlogback.configurationFile=$BASEDIR/conf/logback.xml -jar $BASEDIR/storm-bootstrap.jar
	PID=$!
fi

#Cleanup containers when were shutting down. 
#Good for development. Not so good in production. 
echo "Removing running containers ..."
for lxc in `ls /var/lib/lxc`
do
	echo "unmounting /mnt/storm/lxc/$lxc"
	sudo umount /mnt/storm/lxc/$lxc > /dev/null
done

for lxc in `ls /var/lib/lxc`
do
	echo "destroying lxc $lxc"
	sudo lxc-destroy -f -n $lxc > /dev/null
done

for lxc in `ls /dev/lxc`
do
	echo "removing logical volumn /dev/lxc/$lxc"
	sudo lvremove -f /dev/lxc/$lxc > /dev/null
done

