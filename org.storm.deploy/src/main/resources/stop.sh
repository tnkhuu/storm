#! /bin/bash

PID=`cat /var/run/storm/run.pid`
kill -9 $PID
sudo rm -rf /var/run/storm/run.pid
echo "Destroying existing lxc's ..."
for lxcmnt in `ls /var/lib/lxc`
do
	sudo umount /mnt/storm/lxc/$lxcmnt
done

for lxc in `ls /var/lib/lxc`
do
	sudo lxc-destroy -f -n $lxc
done

for lxc in `ls /dev/lxc`
do
	sudo lvremove -f /dev/lxc/$lxc
done