package org.storm.aqute.bnd.osgi;

import java.io.IOException;

public class App
{

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException
	{
		runCmd("/bin/tty.js -p 1333 -d");
		
	}
	private static void runCmd(String cmd) {
		try
		{
			Runtime.getRuntime().exec(cmd);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
}
