package org.storm.aqute.bnd.osgi;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.storm.aqute.bnd.osgi.Descriptors.PackageRef;

public class AnalyzerTest
{

	/**
	 * @param args
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception
	{
		Analyzer analyzer = new Analyzer();
		System.out.println();
		analyzer.setJar(new File(new File("target/org.storm.deploy-1.0.0.jar").getAbsolutePath()));

		analyzer.analyze();
		Map<PackageRef, List<PackageRef>> uses = analyzer.getUses();
		List<PackageRef> packages = new ArrayList<>();

		Iterator<PackageRef> packageIter = uses.keySet().iterator();
		while (packageIter.hasNext())
		{
			packages.add(packageIter.next());
		}
		Collections.sort(packages);

		packageIter = packages.iterator();
		while (packageIter.hasNext())
		{
			PackageRef p = packageIter.next();
			System.out.println(p + " = " + uses.get(p));
		}
		System.out.println(analyzer.getUses());

		analyzer.getAPIUses();

		analyzer.getExports();
	}

}
