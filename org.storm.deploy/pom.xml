<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<parent>
		<groupId>org.storm</groupId>
		<artifactId>parent</artifactId>
		<version>1.0.0</version>
		<relativePath>../pom.xml</relativePath>
	</parent>
	<modelVersion>4.0.0</modelVersion>
	<packaging>bundle</packaging>
	<name>Storm Platform Deployer</name>
	<artifactId>org.storm.deploy</artifactId>
	<dependencies>
		<dependency>
			<groupId>org.osgi</groupId>
			<artifactId>org.osgi.core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.osgi</groupId>
			<artifactId>org.osgi.compendium</artifactId>
		</dependency>
		<dependency>
			<groupId>org.storm</groupId>
			<artifactId>org.storm.osgi</artifactId>
		</dependency>
		<dependency>
			<groupId>org.storm</groupId>
			<artifactId>org.storm.tools</artifactId>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
		</dependency>
		<dependency>
			<groupId>org.reflections</groupId>
			<artifactId>reflections</artifactId>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-bundle-plugin</artifactId>
				<version>${mvn.bundle.version}</version>
				<extensions>true</extensions>
				<configuration>
					<instructions>
						<Export-Package>
							!org.slf4j.*,
							!ch.qos.*,
							!aQute*,
							*</Export-Package>
						<Main-Class>org.storm.main.Main</Main-Class>
						<Private-Package>
							org.storm.aqute.*,
							org.storm.tools.system,
							org.storm.tools.compression
						</Private-Package>
						<Import-Package>*;resolution:=optional</Import-Package>
						<Bundle-Version>${pom.version}</Bundle-Version>
						<Bundle-SymbolicName>${pom.artifactId}</Bundle-SymbolicName>
						<Bundle-Vendor>Storm Cloud</Bundle-Vendor>
						<Bundle-Name>Storm Platform Deployer</Bundle-Name>
					</instructions>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-antrun-plugin</artifactId>
				<version>1.7</version>
				<executions>
					<execution>
						<id>copy-dist</id>
						<phase>package</phase>
						<configuration>
							<tasks>
								<delete dir="${basedir}/cache" />
								<delete dir="${basedir}/webapps" />
								<delete dir="${basedir}/bundle" />
								<delete dir="${basedir}/data" />
								<delete dir="${basedir}/bin" />
								<delete dir="${basedir}/node" />
								<delete dir="${basedir}/drop" />
								<delete dir="${basedir}/conf" />
								<mkdir dir="${basedir}/conf" />
								<mkdir dir="${basedir}/webapps" />
								<mkdir dir="${basedir}/bundle" />
								<mkdir dir="${basedir}/bin" />
								<mkdir dir="${basedir}/node" />
								<mkdir dir="${basedir}/data" />
								<mkdir dir="${basedir}/drop" />
								<!-- Copy wrapped dependencies on certain bnd processed bundles for 
									better modularity -->
								<copy file="${basedir}/target/org.storm.deploy-${pom.version}.jar"
									tofile="${basedir}/bin/storm-bootstrap.jar" />
								<copy
									file="${basedir}/../org.storm.war/target/org.storm.war-1.0.0.war"
									tofile="${basedir}/webapps/org.storm.war-1.0.0.war" />
								<copy
									file="${basedir}/../org.storm.sample.war/target/org.storm.sample.war-1.0.0.war"
									tofile="${basedir}/webapps/org.storm.sample.war-1.0.0.war" />
								<copydir
									src="${basedir}/../org.storm.dns/org.storm.dns.server/target/classes/"
									dest="${basedir}/bundle" includes="*.jar"
									excludes="org.storm.*.jar,biz.aQute.*.jar, org.apache.felix.scr*.jar" />
								<copydir src="${basedir}/../org.storm.nexus/target/classes/"
									dest="${basedir}/bundle" includes="*.jar"
									excludes="org.storm.*.jar, biz.aQute.*.jar, com.netflix.*.jar, org.apache.felix.scr*.jar" />
								<copydir src="${basedir}/../org.storm.server/target/classes/"
									dest="${basedir}/bundle" includes="*.jar"
									excludes="org.storm.*.jar, biz.aQute.*.jar, org.apache.felix.scr*.jar" />
								<copydir
									src="${basedir}/../org.storm.dependencies/org.storm.jetty/target/classes/"
									dest="${basedir}/bundle" includes="*.jar"
									excludes="org.storm.*.jar, biz.aQute.*.jar, org.apache.felix.scr*.jar" />
								<!-- Configuration Files. -->
								<copy file="${basedir}/target/classes/config.properties"
									todir="${basedir}/conf" />
								<copy file="${basedir}/target/classes/startup.properties"
									todir="${basedir}/conf" />
								<copy file="${basedir}/target/classes/zookeeper.properties"
									todir="${basedir}/conf" />
								<copy
									file="${basedir}/../org.storm.tty/target/storm-tty-1.0.0.tar.gz"
									todir="${basedir}/node" />
								<copy file="${basedir}/target/classes/logback.xml" todir="${basedir}/conf" />
								<copy file="${basedir}/target/classes/start.sh" todir="${basedir}/bin" />
								<copy file="${basedir}/target/classes/start-bg.sh" todir="${basedir}/bin" />
								<copy file="${basedir}/target/classes/start.bat" todir="${basedir}/bin" />
								<copy file="${basedir}/target/classes/stop.sh" todir="${basedir}/bin" />
							</tasks>
						</configuration>
						<goals>
							<goal>run</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<executions>
					<execution>
						<id>copy</id>
						<phase>package</phase>
						<goals>
							<goal>copy</goal>
						</goals>
						<configuration>
							<artifactItems>
								<!-- Storm Foundation & Kernel (Vanilla with dependencies) [Goes 
									into deploy/bundle] -->
								<artifactItem>
									<groupId>commons-fileupload</groupId>
									<artifactId>commons-fileupload</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.commons</groupId>
									<artifactId>commons-compress</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.tukaani</groupId>
									<artifactId>xz</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.commons</groupId>
									<artifactId>commons-exec</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.apache.felix.scr</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.osgi.service.obr</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.apache.felix.shell</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.apache.felix.scr.annotations</artifactId>
									<version>1.7.0</version>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.apache.felix.scr</artifactId>
									<version>1.6.0</version>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.apache.felix.configadmin</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.apache.felix.eventadmin</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>net.sourceforge.cglib</groupId>
									<artifactId>com.springsource.net.sf.cglib</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<!-- JSP Support Start -->
								<artifactItem>
									<groupId>org.eclipse.jetty.orbit</groupId>
									<artifactId>javax.el</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<!-- EL Implementation -->
								<artifactItem>
									<groupId>org.eclipse.jetty.orbit</groupId>
									<artifactId>com.sun.el</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.eclipse.jetty.orbit</groupId>
									<artifactId>org.eclipse.jdt.core</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.logback.classic</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.logback.core</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.messaging</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.felix</groupId>
									<artifactId>org.apache.felix.scr</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.xpp3</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.xmlpull</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.api</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.kernel</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.nexus.api</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.heuristics</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.console.runtime</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.tools</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.velocity</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.openflowj</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.ssh</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.hawtdispatch</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.shell</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.openswitch</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.console.ui</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
		
								<!-- Storm Cloud Specific Add On's & Optional Extensions [Goes into 
									deploy/drop] -->
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.cloud.services</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.json</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.spark</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.floodlight</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.xstream</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.lxc</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.lvm</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.dns.server</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.dns.client</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.shell.engine</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.lxc.img</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.harmonize</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.lxc.img</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.libvirt</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.disruptor</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.central</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.nvm</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.jgit</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>

								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.zookeeper</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.nexus.agent</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.jetty</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/bundle</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.nexus</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.server</artifactId>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/drop</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>${project.groupId}</groupId>
									<artifactId>org.storm.war</artifactId>
									<type>war</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.basedir}/webapps</outputDirectory>
								</artifactItem>
							</artifactItems>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-antrun-plugin</artifactId>
				<version>1.7</version>
				<executions>
					<execution>
						<id>apply-permissions</id>
						<phase>install</phase>
						<configuration>
							<tasks>
								<chmod file="${basedir}/bin/*" perm="754" />
								<chmod file="${basedir}/scripts/*" perm="754" />
								<chmod file="${basedir}/drop/*" perm="754" />
								<chmod file="${basedir}/bundle/*" perm="754" />
								<chmod file="${basedir}/conf/*" perm="754" />
								<chmod file="${basedir}/webapps/*" perm="754" />
							</tasks>
						</configuration>
						<goals>
							<goal>run</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<executions>
					<execution>
						<id>assemble</id>
						<phase>install</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<descriptors>
								<descriptor>assembly.xml</descriptor>
							</descriptors>
							<appendAssemblyId>false</appendAssemblyId>
							<finalName>storm-cloud-${product.version}</finalName>
						</configuration>
					</execution>
					<execution>
						<id>assemble-min</id>
						<phase>install</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<descriptors>
								<descriptor>assembly-min.xml</descriptor>
							</descriptors>
							<appendAssemblyId>false</appendAssemblyId>
							<finalName>storm-cloud-min-${product.version}</finalName>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<version>1.2.1</version>
				<executions>
					<execution>
						<phase>deploy</phase>
						<goals>
							<goal>java</goal>
						</goals>
					</execution>
				</executions>
				<!--Equivalent boot strap when running from command line is : > java 
					-cp lib/*.jar -jar bin\storm-bootstrap.jar -->
				<configuration>
					<includeProjectDependencies>true</includeProjectDependencies>
					<includePluginDependencies>false</includePluginDependencies>
					<mainClass>org.storm.main.Main</mainClass>
					<arguments>
						<argument>-b</argument>
						<argument>${basedir}/bundle</argument>
						<argument>${basedir}/cache</argument>
					</arguments>
					<systemProperties>
						<systemProperty>
							<key>config.properties</key>
							<value>file://localhost/${basedir}/conf/config.properties</value>
						</systemProperty>
						<systemProperty>
							<key>startup.properties</key>
							<value>file://localhost/${basedir}/conf/startup.properties</value>
						</systemProperty>
						<systemProperty>
							<key>logback.configurationFile</key>
							<value>file://localhost/${basedir}/conf/logback.xml</value>
						</systemProperty>
						<systemProperty>
							<key>felix.cm.dir</key>
							<value>${basedir}/conf</value>
						</systemProperty>
						<systemProperty>
							<key>obr.repository.url</key>
							<value>file:///${basedir}/repository.xml</value>
						</systemProperty>
						<systemProperty>
							<key>jetty.home</key>
							<value>${basedir}/</value>
						</systemProperty>
					</systemProperties>
				</configuration>
			</plugin>
			<plugin>
   			 <artifactId>maven-clean-plugin</artifactId>
    			 <version>2.5</version>
    			 <configuration>
     			 <filesets>
        			<fileset>
          				<directory>node</directory>
          				<includes>
            					<include>**/*</include>
          				</includes>
          				<followSymlinks>false</followSymlinks>
        			</fileset>
				<fileset>
          				<directory>webapps</directory>
          				<includes>
            					<include>**/*</include>
          				</includes>
          				<followSymlinks>false</followSymlinks>
        			</fileset>
				<fileset>
          				<directory>drop</directory>
          				<includes>
            					<include>**/*</include>
          				</includes>
          				<followSymlinks>false</followSymlinks>
        			</fileset>
				<fileset>
          				<directory>bundle</directory>
          				<includes>
            					<include>**/*</include>
          				</includes>
          				<followSymlinks>false</followSymlinks>
        			</fileset>
     			</filesets>
    			</configuration>
  			</plugin>
			<!-- <plugin> <artifactId>jdeb</artifactId> <groupId>org.vafer</groupId> 
				<version>1.0</version> <executions> <execution> <id>bundle-storm</id> <phase>install</phase> 
				<goals> <goal>jdeb</goal> </goals> <configuration> <deb>${project.basedir}/target/${product.name}-${product.version}.deb</deb> 
				<installDir>/opt/storm</installDir> <dataSet> <data> <src>${project.basedir}/bundle</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/bundle</prefix> <filemode>755</filemode> </mapper> </data> 
				<data> <src>${project.basedir}/bin</src> <type>directory</type> <dst>/opt/storm/bin</dst> 
				<includes>*</includes> <mapper> <type>perm</type> <prefix>/opt/storm/bin</prefix> 
				<filemode>755</filemode> </mapper> </data> <data> <src>${project.basedir}/scripts/bin</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/scripts/bin</prefix> <filemode>755</filemode> </mapper> 
				</data> <data> <src>${project.basedir}/scripts/package/dist/ubuntu</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/scripts/package/dist/ubuntu</prefix> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/setup</src> <type>directory</type> 
				<includes>*</includes> <mapper> <type>perm</type> <prefix>/opt/storm/scripts/setup</prefix> 
				<filemode>755</filemode> </mapper> </data> <data> <src>${project.basedir}/conf</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/conf</prefix> <filemode>766</filemode> </mapper> </data> 
				<data> <src>${project.basedir}/drop</src> <type>directory</type> <includes>*</includes> 
				<mapper> <prefix>/opt/storm/drop</prefix> <type>perm</type> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/repository.xml</src> <type>file</type> 
				<mapper> <prefix>/opt/storm</prefix> <type>perm</type> <filemode>766</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/bin/mcurl.sh</src> 
				<type>file</type> <mapper> <prefix>/usr/bin</prefix> <type>perm</type> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/bin/create-storm-img</src> 
				<type>file</type> <mapper> <prefix>/usr/bin</prefix> <type>perm</type> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/package/dist/ubuntu/lxc-ubuntu-storm</src> 
				<type>file</type> <mapper> <prefix>/usr/lib/lxc/templates</prefix> <type>perm</type> 
				<filemode>755</filemode> </mapper> </data> <data> <type>link</type> <linkName>/usr/bin/start-storm</linkName> 
				<linkTarget>/opt/storm/bin/start.sh</linkTarget> <symlink>true</symlink> 
				</data> </dataSet> </configuration> </execution> <execution> <id>bundle-storm-min</id> 
				<phase>install</phase> <goals> <goal>jdeb</goal> </goals> <configuration> 
				<deb>${project.basedir}/target/${product.name}-min-${product.version}.deb</deb> 
				<installDir>/opt/storm</installDir> <dataSet> <data> <src>${project.basedir}/bundle</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/bundle</prefix> <filemode>755</filemode> </mapper> </data> 
				<data> <src>${project.basedir}/bin</src> <type>directory</type> <dst>/opt/storm/bin</dst> 
				<includes>*</includes> <mapper> <type>perm</type> <prefix>/opt/storm/bin</prefix> 
				<filemode>755</filemode> </mapper> </data> <data> <src>${project.basedir}/scripts/bin</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/scripts/bin</prefix> <filemode>755</filemode> </mapper> 
				</data> <data> <src>${project.basedir}/scripts/package/dist/ubuntu</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/scripts/package/dist/ubuntu</prefix> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/setup</src> <type>directory</type> 
				<includes>*</includes> <mapper> <type>perm</type> <prefix>/opt/storm/scripts/setup</prefix> 
				<filemode>755</filemode> </mapper> </data> <data> <src>${project.basedir}/conf</src> 
				<type>directory</type> <includes>*</includes> <mapper> <type>perm</type> 
				<prefix>/opt/storm/conf</prefix> <filemode>766</filemode> </mapper> </data> 
				<data> <src>${project.basedir}/drop-min</src> <type>directory</type> <includes>*</includes> 
				<mapper> <type>perm</type> <prefix>/opt/storm/drop</prefix> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/repository.xml</src> <type>file</type> 
				<mapper> <prefix>/opt/storm</prefix> <type>perm</type> <filemode>766</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/bin/mcurl.sh</src> 
				<type>file</type> <mapper> <prefix>/usr/bin</prefix> <type>perm</type> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/bin/create-storm-img</src> 
				<type>file</type> <mapper> <prefix>/usr/bin</prefix> <type>perm</type> <filemode>755</filemode> 
				</mapper> </data> <data> <src>${project.basedir}/scripts/package/dist/ubuntu/lxc-ubuntu-storm</src> 
				<type>file</type> <mapper> <prefix>/usr/lib/lxc/templates</prefix> <type>perm</type> 
				<filemode>755</filemode> </mapper> </data> <data> <type>link</type> <linkName>/usr/bin/start-storm</linkName> 
				<linkTarget>/opt/storm/bin/start.sh</linkTarget> <symlink>true</symlink> 
				</data> </dataSet> </configuration> </execution> </executions> </plugin> -->
		</plugins>
	</build>
</project>
