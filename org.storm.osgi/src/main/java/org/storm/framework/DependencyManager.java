/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.framework;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.reflections.Reflections;
import org.storm.framework.annotation.Inject;

/**
 * CDI Dependency Inject Support into bundles.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unused")
public class DependencyManager
{

	private Reflections reflection;
	private final Set<Field> fields;
	private final Bundle bundle;
	private final Felix felix;
	private Map<Class<?>, Object> instances = new HashMap<>();
	/**
	 * Instantiates a new dependency manager.
	 * A convenient constructor where given {@code Object...} parameter types can be either:
     * <ul>
     *     <li>{@link String} - would add urls using {@link ClasspathHelper#forPackage(String, ClassLoader...)} ()}</li>
     *     <li>{@link Class} - would add urls using {@link ClasspathHelper#forClass(Class, ClassLoader...)} </li>
     *     <li>{@link ClassLoader} - would use this classloaders in order to find urls in {@link ClasspathHelper#forPackage(String, ClassLoader...)} and {@link ClasspathHelper#forClass(Class, ClassLoader...)}</li>
     *     <li>{@link Scanner} - would use given scanner, overriding the default scanners</li>
     *     <li>{@link URL} - would add the given url for scanning</li>
     *     <li>{@link Object[]} - would use each element as above</li>
     * </ul>
	 * @param basePackages
	 *            the base packages
	 */
	private DependencyManager(Bundle bundle,Felix felix, Object... basePackages)
	{
		this.bundle = bundle;
		this.felix = felix;
		this.reflection = new Reflections(basePackages);
		this.fields = reflection.getFieldsAnnotatedWith(Inject.class);
	}
	
	public void activateBundle(Bundle bundle, boolean fireEvent) throws BundleException {
		if(fields == null || fields.size() == 0) {
			felix.activateBundle((BundleImpl) bundle, fireEvent);
		}
		else
		{
			for(Field f : fields) {
				Class<?> clazz =	f.getDeclaringClass();
			}
		}
	}
}
