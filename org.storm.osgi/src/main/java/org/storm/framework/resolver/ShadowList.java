/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.storm.framework.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@SuppressWarnings("hiding")
public class ShadowList<T> implements List<T>
{
	private final List<T> m_original;
	private final List<T> m_shadow;

	public ShadowList(List<T> original)
	{
		m_original = original;
		m_shadow = new ArrayList<T>(original);
	}

	public List<T> getOriginal()
	{
		return m_original;
	}

	@Override
	public int size()
	{
		return m_shadow.size();
	}

	@Override
	public boolean isEmpty()
	{
		return m_shadow.isEmpty();
	}

	@Override
	public boolean contains(Object o)
	{
		return m_shadow.contains(o);
	}

	@Override
	public Iterator<T> iterator()
	{
		return m_shadow.iterator();
	}

	@Override
	public Object[] toArray()
	{
		return m_shadow.toArray();
	}

	@Override
	public <T> T[] toArray(T[] ts)
	{
		return m_shadow.toArray(ts);
	}

	@Override
	public boolean add(T e)
	{
		return m_shadow.add(e);
	}

	@Override
	public boolean remove(Object o)
	{
		return m_shadow.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> clctn)
	{
		return m_shadow.containsAll(clctn);
	}

	@Override
	public boolean addAll(Collection<? extends T> clctn)
	{
		return m_shadow.addAll(clctn);
	}

	@Override
	public boolean addAll(int i, Collection<? extends T> clctn)
	{
		return m_shadow.addAll(i, clctn);
	}

	@Override
	public boolean removeAll(Collection<?> clctn)
	{
		return m_shadow.removeAll(clctn);
	}

	@Override
	public boolean retainAll(Collection<?> clctn)
	{
		return m_shadow.retainAll(clctn);
	}

	@Override
	public void clear()
	{
		m_shadow.clear();
	}

	@Override
	public T get(int i)
	{
		return m_shadow.get(i);
	}

	@Override
	public T set(int i, T e)
	{
		return m_shadow.set(i, e);
	}

	@Override
	public void add(int i, T e)
	{
		m_shadow.add(i, e);
	}

	@Override
	public T remove(int i)
	{
		return m_shadow.remove(i);
	}

	@Override
	public int indexOf(Object o)
	{
		return m_shadow.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o)
	{
		return m_shadow.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator()
	{
		return m_shadow.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int i)
	{
		return m_shadow.listIterator(i);
	}

	@Override
	public List<T> subList(int i, int i1)
	{
		return m_shadow.subList(i, i1);
	}
}