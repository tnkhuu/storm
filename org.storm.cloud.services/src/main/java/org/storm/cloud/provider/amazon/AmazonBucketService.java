/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud.provider.amazon;

import java.io.File;

import org.jclouds.ContextBuilder;
import org.jclouds.aws.s3.AWSS3ProviderMetadata;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.blobstore.options.CreateContainerOptions.Builder;
import org.storm.api.security.Credentials;
import org.storm.api.services.BlobStoreService;
import org.storm.cloud.AbstractBlobStoreService;

/**
 * A Amazon backed implementation of the {@link BlobStoreService}.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class AmazonBucketService extends AbstractBlobStoreService<AmazonBucketService>
{

	private BlobStoreContext context;
	private BlobStore blobStore;
	private Credentials credentials;

	/** Default constructor. */
	public AmazonBucketService()
	{

	}

	/**
	 * Instantiates a new amazon bucket service.
	 * 
	 * @param credentials
	 *            the credentials
	 */
	public AmazonBucketService(Credentials credentials)
	{
		setCredentials(credentials);

	}

	/**
	 * Return this concrete instance of the bucket service.
	 */
	@Override
	public AmazonBucketService getProvider()
	{
		return this;
	}

	/**
	 * Set the credentials for this bucket service.
	 * 
	 * @param credentials
	 */
	public void setCredentials(Credentials credentials)
	{
		this.credentials = credentials;
		context = ContextBuilder.newBuilder(AWSS3ProviderMetadata.builder().build()).credentials(this.credentials.getAccessKey(), this.credentials.getSecret())
				.buildView(BlobStoreContext.class);
		blobStore = context.getBlobStore();
	}

	/**
	 * Put some bytes into the container
	 * 
	 * @param container
	 *            the container
	 * @param name
	 *            the name
	 * @param data
	 *            the data
	 * @return the string
	 */
	public String put(String container, String name, byte[] data)
	{
		if (!blobStore.containerExists(container))
		{
			boolean success = blobStore.createContainerInLocation(null, container, Builder.publicRead());
			if (success)
			{
				Blob b = blobStore.blobBuilder(name).payload(data).build();
				return blobStore.putBlob(container, b);
			}
			throw new IllegalArgumentException("Could not create container " + name);
		}
		else
		{

			Blob b = blobStore.blobBuilder(name).payload(data).build();
			return blobStore.putBlob(container, b);

		}
	}

	/**
	 * Put A File into the container
	 * 
	 * @param container
	 *            the container
	 * @param name
	 *            the name
	 * @param data
	 *            the data
	 * @return the string
	 */
	@Override
	public String put(String container, String name, File data)
	{
		if (!blobStore.containerExists(container))
		{
			boolean success = blobStore.createContainerInLocation(null, container, Builder.publicRead());
			if (success)
			{
				Blob b = blobStore.blobBuilder(name).payload(data).build();
				return blobStore.putBlob(container, b);
			}
			throw new IllegalArgumentException("Could not create container " + name);
		}
		else
		{

			Blob b = blobStore.blobBuilder(name).payload(data).build();
			return blobStore.putBlob(container, b);

		}
	}


}
