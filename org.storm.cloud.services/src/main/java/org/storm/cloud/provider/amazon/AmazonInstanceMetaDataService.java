/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud.provider.amazon;

import org.storm.api.services.MetaDataService;
import org.storm.tools.http.Request;
import org.storm.tools.http.Request.HttpRequestException;

/**
 * The Class AmazonInstanceMetaDataService.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class AmazonInstanceMetaDataService implements MetaDataService
{
	private static final String CLOUD_METADATA_URL = "http://169.254.169.254/latest/meta-data/";
	private String ipAddress = null;
	private boolean isOnCloud = false;
	private boolean isOnCloudSet = false;

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#isOnCloud()
	 */
	@Override
	public boolean isOnCloud()
	{
		if (!isOnCloudSet)
		{
			isOnCloudSet = true;
			try
			{
				isOnCloud = getIp4Address() != null;
			}
			catch (Exception e)
			{
				
			}
		}
		return isOnCloud;
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getIp4Address()
	 */
	@Override
	public String getIp4Address()
	{
		if (ipAddress == null)
		{
			try
			{
				Request req = Request.get(CLOUD_METADATA_URL + "public-ipv4");
				ipAddress = req.body();
			}
			catch (HttpRequestException e)
			{
				throw new RuntimeException(e);
			}
		}
		return ipAddress;
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getMacAddress()
	 */
	@Override
	public String getMacAddress()
	{
		try
		{
			Request req = Request.get(CLOUD_METADATA_URL + "mac");
			return req.body();
		}
		catch (HttpRequestException e)
		{
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getInstanceId()
	 */
	@Override
	public String getInstanceId()
	{

		try
		{
			Request req = Request.get(CLOUD_METADATA_URL + "instance-id");
			return req.body();
		}
		catch (HttpRequestException e)
		{
			throw new RuntimeException(e);
		}

	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getAmiID()
	 */
	@Override
	public String getImageID()
	{

		try
		{
			Request req = Request.get(CLOUD_METADATA_URL + "ami-id");
			return req.body();
		}
		catch (HttpRequestException e)
		{
			throw new RuntimeException(e);
		}

	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getInstanceHostName()
	 */
	@Override
	public String getHostName()
	{
		try
		{
			Request req = Request.get(CLOUD_METADATA_URL + "hostname");
			return req.body();
		}
		catch (HttpRequestException e)
		{
			throw new RuntimeException(e);
		}

	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getReservationID()
	 */
	@Override
	public String getReservationID()
	{
		try
		{
			Request req = Request.get(CLOUD_METADATA_URL + "reservation-id");
			return req.body();
		}
		catch (HttpRequestException e)
		{
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getNetworkInterfaces()
	 */
	@Override
	public String getNetworkInterfaces()
	{

		try
		{
			Request req = Request.get(CLOUD_METADATA_URL + "interfaces");
			return req.body();
		}
		catch (HttpRequestException e)
		{
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.storm.kernel.cloud.MetaDataService#getAmazonLaunchIndex()
	 */
	@Override
	public String getLaunchIndex()
	{

		try
		{
			Request req = Request.get(CLOUD_METADATA_URL + "ami-launch-index");
			return req.body();
		}
		catch (HttpRequestException e)
		{
			throw new RuntimeException(e);
		}

	}
}
