/**
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at :
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.cloud.Region;
import org.storm.api.security.Credentials;
import org.storm.api.services.BlobStoreService;
import org.storm.api.services.ComputeService;
import org.storm.api.services.DistributedService;
import org.storm.api.services.MetaDataService;
import org.storm.cloud.compute.ComputeServiceImpl;
import org.storm.cloud.provider.amazon.AmazonBucketService;
import org.storm.cloud.provider.amazon.AmazonInstanceMetaDataService;
import org.storm.cloud.rest.RestOperations;

/**
 * The Bundle Activator
 *
 * @author Trung Khuu
 * @since 1.0
 */
public class ComponentActivator extends DependencyActivatorBase
{

    private Logger s_log = LoggerFactory.getLogger(ComponentActivator.class);
    private ComputeService computeService;
    private BlobStoreService<AmazonBucketService> blobStore;
    private static final String ACCESSKEY = "AKIAIJHJMLIOVQD4HU4Q";
    private static final String SECRET = "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";
    private volatile boolean initialized = false;
    private final Object mutex = new Object();


    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception
    {
        if (!initialized)
        {
            synchronized (mutex)
            {
                if (!initialized)
                {
                    initialized = true;
                    computeService = new ComputeServiceImpl(new Credentials(ACCESSKEY, SECRET, CloudProvider.AMAZON), CloudProvider.AMAZON, Region.AP_SOUTHEAST_2, context);
                    blobStore = new AmazonBucketService(new Credentials(ACCESSKEY, SECRET, CloudProvider.AMAZON));
                    manager.add(createComponent().setInterface(new String[]{ComputeService.class.getName(), DistributedService.class.getName()}, null)
                                                 .setImplementation(computeService));

                    manager.add(createComponent().setInterface(new String[]{BlobStoreService.class.getName(), DistributedService.class.getName()}, null)
                                                 .setImplementation(blobStore));

                    manager.add(createComponent().setInterface(MetaDataService.class.getName(), null)
                                                 .setImplementation(AmazonInstanceMetaDataService.class));
                    
                    manager.add(createComponent().setImplementation(RestOperations.class));

                    s_log.info("Cloud Services initialized.");
                }
            }
        }
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception
    {


    }

}
