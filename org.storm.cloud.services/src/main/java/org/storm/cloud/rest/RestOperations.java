package org.storm.cloud.rest;

import static org.storm.spark.Spark.*;

import org.storm.spark.Request;
import org.storm.spark.Response;
import org.storm.spark.Route;

/**
 * The Rest implementation
 *
 * @author Trung Khuu
 * @since 1.0
 */
public class RestOperations
{
    

    public void init()
    {

    }

    public void start()
    {
        get(new Route("/hello") {
            @Override
            public Object handle(Request request, Response response) {
                return "Hello World!";
            }
        });
        
        post(new Route("/hello") {
            @Override
            public Object handle(Request request, Response response) {
                return "Hello World: " + request.body();
            }
        });
        
        get(new Route("/private") {
            @Override
            public Object handle(Request request, Response response) {
                response.status(401);
                return "Go Away!!!";
            }
        });
        
        get(new Route("/users/:name") {
            @Override
            public Object handle(Request request, Response response) {
                return "Selected user: " + request.params(":name");
            }
        });
    }

    public void stop()
    {

    }

    public void destroy()
    {

    }
}
