/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud.compute;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.jclouds.ContextBuilder;
import org.jclouds.View;
import org.jclouds.aws.ec2.compute.AWSEC2TemplateOptions;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.RunScriptOnNodesException;
import org.jclouds.compute.domain.*;
import org.jclouds.compute.options.TemplateOptions;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.ec2.EC2ApiMetadata;
import org.jclouds.ec2.EC2Client;
import org.jclouds.ec2.compute.options.EC2TemplateOptions;
import org.jclouds.ec2.domain.IpProtocol;
import org.jclouds.ec2.domain.KeyPair;
import org.jclouds.ec2.services.InstanceClient;
import org.jclouds.ec2.services.KeyPairClient;
import org.jclouds.ec2.services.SecurityGroupClient;
import org.jclouds.scriptbuilder.domain.Statements;
import org.jclouds.scriptbuilder.statements.java.InstallJDK;
import org.osgi.framework.BundleContext;
import org.osgi.framework.wiring.BundleWiring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.cloud.Region;
import org.storm.api.exception.OperationFailedException;
import org.storm.api.security.Credentials;
import org.storm.api.services.ComputeService;
import org.storm.api.services.vm.HypervisorDriver;
import org.storm.api.services.vm.HypervisorType;
import org.storm.api.services.vm.InstanceType;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.nexus.api.Role;

import com.google.common.base.Charsets;
import com.google.common.collect.Iterables;
import com.google.common.io.Files;

import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.predicates.NodePredicates.runningInGroup;

/**
 * A Cloud Compute Service.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@SuppressWarnings("unused")
public class ComputeServiceImpl implements ComputeService
{
	private final static Logger s_log = LoggerFactory.getLogger(ComputeServiceImpl.class);
	private final static String ROOT_EBS = "rootDeviceType";
	private final static String EBS = "ebs";
	private final static String UBUNTU_QUERY = ".*12\\.04.*";
	private final static String LXC_LVM_PATH = "/dev/sdb";
	private final Credentials credentials;
	private final ComputeServiceContext context;
	private final org.jclouds.compute.ComputeService client;
	private final Region servicingRegion;
	private final CloudProvider provider; 
	private final String SSH_PRIVATE_KEY = System.getProperty("user.home") + "/.ssh/storm.pem";

	/**
	 * Instantiates a new compute service impl.
	 * 
	 * @param credentials
	 *            the credentials
	 * @param cloudProvider
	 *            the cloud provider
	 * @param region
	 *            the region
	 * @param bundleContext
	 *            the bundle context
	 */
	public ComputeServiceImpl(Credentials credentials, CloudProvider cloudProvider, Region region, BundleContext bundleContext)
	{
		this.credentials = credentials;
		this.servicingRegion = region;
		this.provider = cloudProvider;

		ClassLoader oldClassLoader = Thread.currentThread().getContextClassLoader();
		if (bundleContext != null)
		{
			ClassLoader bundleClassloader = bundleContext.getBundle().adapt(BundleWiring.class).getClassLoader();

			Thread.currentThread().setContextClassLoader(bundleClassloader);
		}
		this.context = ContextBuilder.newBuilder(cloudProvider.getId()).endpoint(HTTP + region.getAddress())
				.credentials(this.credentials.getAccessKey(), this.credentials.getSecret()).build(ComputeServiceContext.class);
		this.client = context.getComputeService();
		if (bundleContext != null)
		{
			Thread.currentThread().setContextClassLoader(oldClassLoader);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#createInstance(org.storm.nexus.api.Role, org.storm.api.services.vm.HypervisorDriver, org.storm.api.services.vm.HypervisorType)
	 */
	@Override
	public VirtualMachine createSpotInstance(float price, Role role, HypervisorDriver driver, HypervisorType type) throws OperationFailedException
	{
		Template template = createTemplate(role).locationId(DEFAULT_REGION).build();
		VirtualMachine vm = null;
		try
		{
			template.getOptions().as(AWSEC2TemplateOptions.class).spotPrice(price).installPrivateKey(Files.toString(new File(SSH_PRIVATE_KEY), Charsets.UTF_8));
			template.getImage().getUserMetadata().put(ROOT_EBS, EBS);
			NodeMetadata node = Iterables.getOnlyElement(context.getComputeService().createNodesInGroup(DEFAULT_SECURITY_GROUP, 1, template));
			vm = new VirtualMachine(node.getPublicAddresses(), node.getHostname(), InstanceType.LARGE, node.getId(), node.getImageId(), node.getStatus().name(), SSH_PRIVATE_KEY);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.out);
			s_log.error(e.getMessage(), e);
			throw new OperationFailedException(e.getMessage(), e);
		}
		catch (RunNodesException e)
		{
			e.printStackTrace(System.out);
			throw new OperationFailedException(e.getMessage(), e);
		}
		return vm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#createInstance(org.storm.nexus.api.Role, org.storm.api.services.vm.HypervisorDriver, org.storm.api.services.vm.HypervisorType)
	 */
	public VirtualMachine createInstance(Role role, byte[] userData, HypervisorDriver driver, HypervisorType type) throws OperationFailedException
	{
		return createInstance(role, 1, userData, driver, type)[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#createInstance(org.storm.nexus.api.Role, org.storm.api.services.vm.HypervisorDriver, org.storm.api.services.vm.HypervisorType)
	 */
	
	public VirtualMachine[] createInstance(Role role, int instances, byte[] userData, HypervisorDriver driver, HypervisorType type) throws OperationFailedException
	{

		VirtualMachine vm[] = new VirtualMachine[instances];
        String group = role.name();

		KeyPairClient keyPairClient = context.unwrap(EC2ApiMetadata.CONTEXT_TOKEN).getApi().getKeyPairServices();
        SecurityGroupClient securityGroupClient = context.unwrap(EC2ApiMetadata.CONTEXT_TOKEN).getApi().getSecurityGroupServices();
        InstanceClient instanceClient = context.unwrap(EC2ApiMetadata.CONTEXT_TOKEN).getApi().getInstanceServices();


        securityGroupClient.createSecurityGroupInRegion(null, group, group);
        securityGroupClient.authorizeSecurityGroupIngressInRegion(null, group, IpProtocol.TCP, 0, 10000, "0.0.0.0/0");

        Template template = createTemplate(role).locationId(DEFAULT_REGION).build();
		TemplateOptions options = template.getOptions();
        options.as(EC2TemplateOptions.class).securityGroups(group);

        KeyPair result = keyPairClient.createKeyPairInRegion(null, group);
        options.as(EC2TemplateOptions.class).keyPair(result.getKeyName());


        options.overrideLoginPrivateKey(result.getKeyMaterial());


        // an arbitrary command to run
       // options.runScript(Statements.exec("find /usr"));


		try
		{
            options.as(AWSEC2TemplateOptions.class).spotPrice(0.005f).userData(userData).blockUntilRunning(true).mapNewVolumeToDeviceName("/dev/sdb", 10, true);
            Set<? extends NodeMetadata> nodes = client.createNodesInGroup(group, instances, options);

			Iterator<? extends NodeMetadata> iter = nodes.iterator();
			int i = 0;
			while (iter.hasNext())
			{
				NodeMetadata node = iter.next();
				vm[i] = new VirtualMachine(node.getPublicAddresses(), node.getHostname(), InstanceType.LARGE, node.getId(), node.getImageId(), node.getStatus().name(),
						SSH_PRIVATE_KEY);
				i++;
			}
            LoginCredentials creds =   LoginCredentials.builder().user(nodes.iterator().next().getCredentials().identity).privateKey(result.getKeyMaterial())
                    .build();
            runScriptWithCreds(group, creds, "sudo apt-get update");
            runScriptWithCreds(group, creds, "sudo apt-get install zookeeper");
            runScriptWithCreds(group, creds, "sudo echo \" + i +\" > /var/lib/zookeeper/myid");
            runScriptWithCreds(group, creds, "sudo sed -i s/#server.1=zookeeper1:2888:3888/server.1=" + vm[0].getFirstIpAddress() + ":2888:3888/g /etc/zookeeper/conf/zoo.cfg");
            runScriptWithCreds(group, creds, "sudo sed -i s/#server.2=zookeeper2:2888:3888/server.2=" + vm[0].getFirstIpAddress() + ":2888:3888/g /etc/zookeeper/conf/zoo.cfg");
            runScriptWithCreds(group, creds, "sudo sed -i s/#server.3=zookeeper3:2888:3888/server.3=" + vm[0].getFirstIpAddress() + ":2888:3888/g /etc/zookeeper/conf/zoo.cfg");
            runScriptWithCreds(group, creds, "sudo /usr/share/zookeeper/bin/zkServer.sh start");

		}
		catch (Exception e)
		{
			e.printStackTrace(System.out);
			s_log.error(e.getMessage(), e);
			throw new OperationFailedException(e.getMessage(), e);
		}
		return vm;
	}

    protected Map<? extends NodeMetadata, ExecResponse> runScriptWithCreds(final String group, LoginCredentials creds, String command) throws RunScriptOnNodesException
    {
        return client.runScriptOnNodesMatching(runningInGroup(group), Statements.exec(command), overrideLoginCredentials(creds).nameTask("runScriptWithCreds"));
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#createInstance(org.storm.nexus.api.Role, int[], java.lang.String, org.storm.api.services.vm.HypervisorDriver,
	 * org.storm.api.services.vm.HypervisorType, boolean)
	 */
	public VirtualMachine createInstance(Role role, int[] openInBoundPorts, String script, HypervisorDriver driver, HypervisorType type, boolean blockingUntilRunning)
			throws OperationFailedException
	{
		Template template = createTemplate(role).locationId(DEFAULT_REGION).build();
		VirtualMachine vm = null;
		try
		{

			template.getOptions().as(AWSEC2TemplateOptions.class).spotPrice(0.005f).runAsRoot(true).runScript(script).blockUntilRunning(blockingUntilRunning)
					.inboundPorts(openInBoundPorts).mapNewVolumeToDeviceName(LXC_LVM_PATH, 10, true).authorizePublicKey(Files.toString(new File(SSH_PRIVATE_KEY), Charsets.UTF_8));
			template.getImage().getUserMetadata().put(ROOT_EBS, EBS);
			NodeMetadata node = Iterables.getOnlyElement(context.getComputeService().createNodesInGroup(DEFAULT_SECURITY_GROUP, 1, template));
			vm = new VirtualMachine(node.getPublicAddresses(), node.getHostname(), InstanceType.LARGE, node.getId(), node.getImageId(), node.getStatus().name(), SSH_PRIVATE_KEY);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.out);
			s_log.error(e.getMessage(), e);
			throw new OperationFailedException(e.getMessage(), e);
		}
		catch (RunNodesException e)
		{
			e.printStackTrace(System.out);
			throw new OperationFailedException(e.getMessage(), e);
		}
		return vm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#createInitialInstance(org.storm.nexus.api.Role, org.storm.api.services.vm.HypervisorDriver, byte[],
	 * org.storm.api.services.vm.HypervisorType, boolean)
	 */
	public VirtualMachine createInitialInstance(Role role, HypervisorDriver driver, byte[] userData, HypervisorType type, boolean blockingUntilRunning)
			throws OperationFailedException
	{
		Template template = createTemplate(role).locationId(DEFAULT_REGION).build();

		VirtualMachine vm = null;
		try
		{
			template.getOptions().as(AWSEC2TemplateOptions.class).spotPrice(0.005f).runAsRoot(true).blockUntilRunning(blockingUntilRunning)
					.mapNewVolumeToDeviceName(LXC_LVM_PATH, 10, true).authorizePublicKey(Files.toString(new File(SSH_PRIVATE_KEY), Charsets.UTF_8));

			template.getImage().getUserMetadata().put(ROOT_EBS, EBS);
			NodeMetadata node = Iterables.getOnlyElement(context.getComputeService().createNodesInGroup(DEFAULT_SECURITY_GROUP, 1, template));
			vm = new VirtualMachine(node.getPublicAddresses(), node.getHostname(), InstanceType.LARGE, node.getId(), node.getImageId(), node.getStatus().name(), SSH_PRIVATE_KEY);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.out);
			s_log.error(e.getMessage(), e);
			throw new OperationFailedException(e.getMessage(), e);
		}
		catch (RunNodesException e)
		{
			e.printStackTrace(System.out);
			throw new OperationFailedException(e.getMessage(), e);
		}
		return vm;
	}

	/**
	 * Creates the template.
	 * 
	 * @param role
	 *            the role
	 * @return the template builder
	 */
	private TemplateBuilder createTemplate(Role role)
	{
		TemplateBuilder template = context.getComputeService().templateBuilder();
		switch (role)
		{
		case INITIAL:
		{
			template.smallest().osFamily(OsFamily.UBUNTU).os64Bit(true).imageNameMatches(UBUNTU_QUERY);
			break;
		}

		default:
		{
			template.smallest().osFamily(OsFamily.UBUNTU).os64Bit(true).imageNameMatches(UBUNTU_QUERY);
			// template.minCores(4).minRam(12000).os64Bit(true).osFamily(OsFamily.UBUNTU).imageNameMatches(UBUNTU_QUERY);
			break;
		}
		}

		return template;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#getServicingRegion()
	 */
	@Override
	public Region getServicingRegion()
	{
		return servicingRegion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#getBackingProvider()
	 */
	@Override
	public CloudProvider getBackingProvider()
	{
		return provider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#createInstance(org.storm.api.cloud.Region, java.lang.String)
	 */
	public VirtualMachine createInstance(Region region, String amiID)
	{
		VirtualMachine vm = null;
		try
		{

			Template template = createTemplate(Role.DEFAULT).imageId(region.getName() + "/" + amiID).build();
			template.getOptions().as(AWSEC2TemplateOptions.class).spotPrice(0.005f).runAsRoot(true).blockUntilRunning(true);
			NodeMetadata node = Iterables.getOnlyElement(context.getComputeService().createNodesInGroup(DEFAULT_SECURITY_GROUP, 1, template));
			vm = new VirtualMachine(node.getPublicAddresses(), node.getHostname(), InstanceType.LARGE, node.getId(), node.getImageId(), node.getStatus().name(), SSH_PRIVATE_KEY);
		}
		catch (RunNodesException e)
		{
			throw new RuntimeException(e);
		}
		return vm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#terminateInstance(java.lang.String)
	 */
	public boolean terminateInstance(String instanceId)
	{
		boolean success = true;
		context.getComputeService().destroyNode(instanceId);
		return success;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#rebootInstance(java.lang.String)
	 */
	public boolean rebootInstance(String instanceId)
	{
		boolean success = true;
		context.getComputeService().rebootNode(instanceId);
		return success;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.storm.api.services.ComputeService#getRunningInstances()
	 */
	@Override
	public Set<VirtualMachine> getRunningInstances()
	{
		Set<VirtualMachine> vms = new HashSet<>();
		return vms;
	}

}
