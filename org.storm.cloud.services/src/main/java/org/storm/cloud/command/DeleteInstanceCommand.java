/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud.command;

import org.apache.felix.service.command.Descriptor;

import aQute.bnd.annotation.component.Component;

/**
 * Deletes a cloud node instance
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component(properties = { "osgi.command.scope=storm", "osgi.command.function=deleteInstance" }, provide = Object.class)
public class DeleteInstanceCommand
{

	/**
	 * Delete instance.
	 * 
	 * @param instanceName
	 *            the instance name
	 * @param secret
	 *            the secret
	 * @param accessKey
	 *            the access key
	 */
	@Descriptor("Delete and terminate the storm cloud agent with the specified name and credentials")
	public void deleteInstance(String instanceName, String secret, String accessKey)
	{
		System.out.println("Deleting Instance....");
	}
}
