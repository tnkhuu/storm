/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud.command;

import org.apache.felix.service.command.Descriptor;

import aQute.bnd.annotation.component.Component;

/**
 * Describes a new cloud node instance
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component(properties = { "osgi.command.scope=storm", "osgi.command.function=describe" }, provide = Object.class)
public class DescribeInstanceCommand
{

	/**
	 * Describe.
	 */
	@Descriptor("Describe the storm cloud agent with the given name using the given credentials")
	public void describe()
	{
		System.out.println("Creating Instance....");
	}
}
