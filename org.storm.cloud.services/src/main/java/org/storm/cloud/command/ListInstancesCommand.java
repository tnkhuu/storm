/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud.command;

import java.util.Iterator;
import java.util.Set;

import org.apache.felix.service.command.Descriptor;
import org.storm.api.services.ComputeService;
import org.storm.api.services.vm.VirtualMachine;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

/**
 * Lists all the cloud nodes for the given provider
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Component(properties = { "osgi.command.scope=storm", "osgi.command.function=list" }, provide = Object.class)
public class ListInstancesCommand
{
	private ComputeService computeService;

	@Reference
	public void setComputeService(ComputeService computeService)
	{
		this.computeService = computeService;
	}

	/**
	 * List.
	 */
	@Descriptor("Create a new storm cloud agent in the specified cloud provider using the specified credentials")
	public void list(String provider, String accessKey, String secret)
	{

	//	Credentials credentials = new Credentials(accessKey, secret, CloudProvider.valueOf(provider));

		Set<VirtualMachine> nodes = computeService.getRunningInstances();
		Iterator<VirtualMachine> nodeIter = nodes.iterator();
		while (nodeIter.hasNext())
		{
			System.out.println(nodeIter.next());
		}
	}
}
