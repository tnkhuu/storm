/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.compute;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.osgi.framework.BundleContext;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.cloud.Region;
import org.storm.api.security.Credentials;
import org.storm.api.services.ComputeService;
import org.storm.api.services.vm.VirtualMachine;
import org.storm.cloud.compute.ComputeServiceImpl;

/**
 * The Compute Service Test.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Ignore
public class ComputeServiceImplTest
{
	private static final String ACCESSKEY = "AKIAIJHJMLIOVQD4HU4Q";
	private static final String SECRET = "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";
	private ComputeService compute;
	@SuppressWarnings("unused")
	private BundleContext context = Mockito.mock(BundleContext.class);

	/**
	 * Test create instance.
	 */
	@Test
	public void testCreateInstance()
	{

		try
		{
			compute = new ComputeServiceImpl(new Credentials(ACCESSKEY, SECRET, CloudProvider.AMAZON), CloudProvider.AMAZON, Region.AP_SOUTHEAST_2, null);

			//VirtualMachine vm = compute.createInstance(Role.INITIAL, HypervisorDriver.LXC, HypervisorType.PARAVIRTUALIZED);
			long time = System.currentTimeMillis();
			VirtualMachine vm = compute.createInstance(Region.AP_SOUTHEAST_2, "ami-58920262");//compute.createInitialInstance(Role.INITIAL, HypervisorDriver.LXC, null, HypervisorType.PARAVIRTUALIZED, true);
			System.out.println(vm);
			
			long end = System.currentTimeMillis() - time;
			System.out.println("Time Taken: " + end / 1000 + " seconds");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
