/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.amazon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.storm.api.cloud.CloudProvider;
import org.storm.api.security.Credentials;
import org.storm.cloud.provider.amazon.AmazonBucketService;

/**
 * The Class AmazonBucketServiceTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
@Ignore
public class AmazonBucketServiceTest
{
	private static final String ACCESSKEY = "AKIAIJHJMLIOVQD4HU4Q";
	private static final String SECRET = "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";
	private AmazonBucketService provider;

	/**
	 * Setup.
	 */
	@Before
	public void setup()
	{
		provider = new AmazonBucketService(new Credentials(ACCESSKEY, SECRET, CloudProvider.AMAZON));
	}

	/**
	 * Test list buckets.
	 * 
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testListBuckets() throws FileNotFoundException, IOException
	{
		String url = provider.put("storm-cloudz", "test", IOUtils.toByteArray(new FileInputStream(new File("/home/tkhuu/Documents/images.txt"))));
		System.out.println(url);
	}

	
}
