/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.cloud.provider.amazon;

import org.junit.Assert;
import org.junit.Test;
import org.storm.api.services.MetaDataService;
import org.storm.cloud.provider.amazon.AmazonInstanceMetaDataService;



/**
 * The Class AmazonInstanceMetaDataServiceTest.
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class AmazonInstanceMetaDataServiceTest {
	
	private static MetaDataService service = new AmazonInstanceMetaDataService();
  



  /**
	 * Gets the amazon launch index.
	 * 
	 * @return the amazon launch index
	 */
  @Test
  public void getAmazonLaunchIndex() {
	  if(service.isOnCloud()) {
		  Assert.assertNotNull(service.getLaunchIndex());
	  }
  }

  /**
	 * Gets the ami id.
	 * 
	 * @return the ami id
	 */
  @Test
  public void getAmiID() {
    if(service.isOnCloud()) {
    	Assert.assertNotNull(service.getImageID());
    }
  }

  /**
	 * Gets the instance host name.
	 * 
	 * @return the instance host name
	 */
  @Test
  public void getInstanceHostName() {
	  if(service.isOnCloud()) {
	    	Assert.assertNotNull(service.getHostName());
	    }
  }

  /**
	 * Gets the instance id.
	 * 
	 * @return the instance id
	 */
  @Test
  public void getInstanceId() {
	  if(service.isOnCloud()) {
	    	Assert.assertNotNull(service.getInstanceId());
	    }
  }

  /**
	 * Gets the ip4 address.
	 * 
	 * @return the ip4 address
	 */
  @Test
  public void getIp4Address() {
	  if(service.isOnCloud()) {
	    	Assert.assertNotNull(service.getIp4Address());
	    }
  }

  /**
	 * Gets the mac address.
	 * 
	 * @return the mac address
	 */
  @Test
  public void getMacAddress() {
	  if(service.isOnCloud()) {
	    	Assert.assertNotNull(service.getMacAddress());
	    }
  }

  /**
	 * Gets the network interfaces.
	 * 
	 * @return the network interfaces
	 */
  @Test
  public void getNetworkInterfaces() {
	  if(service.isOnCloud()) {
	    	Assert.assertNotNull(service.getNetworkInterfaces());
	    }
  }

  /**
	 * Gets the reservation id.
	 * 
	 * @return the reservation id
	 */
  @Test
  public void getReservationID() {
	  if(service.isOnCloud()) {
	    	Assert.assertNotNull(service.getReservationID());
	    }
  }
}
