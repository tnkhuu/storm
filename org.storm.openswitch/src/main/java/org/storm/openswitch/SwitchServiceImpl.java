/** 
 * Copyright 2013 Trung Khuu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of this License at : 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.storm.openswitch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.storm.api.network.Protocol;
import org.storm.api.services.SwitchService;

/**
 * A Virtual Switch Provider using the underlying openswitch library.
 * Requires openswitch to be installed. 
 * 
 * @author Trung Khuu
 * @since 1.0
 */
public class SwitchServiceImpl implements SwitchService
{
	private static final String VIRTUAL_SWITCH_CONTROLLER = "ovs-vsctl";
	private static final String ADD_BRIDGE = "add-br";
	private static final String DEL_BRIDGE = "del-br";
	private static final String ADD_PORT = "add-port";
	private static final String DEL_PORT = "del-port";
	private static final String GET_CONTROLLER = "get-controller";
	private static final String DEL_CONTROLLER = "del−controller";
	private static final String SET_CONTROLLER = "set-controller";
	
	/* (non-Javadoc)
	 * @see org.storm.api.services.SwitchService#createBridge(java.lang.String)
	 */
	@Override
	public boolean createSwitch(String bridgeName)
	{
		try
		{
			runSwitchCommand(ADD_BRIDGE + " " + bridgeName);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.storm.api.services.SwitchService#addPort(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean addPort(String bridgeName, String portName)
	{
		try
		{
			runSwitchCommand(ADD_PORT + " " + bridgeName + " " + portName);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return true;
	}
	
	public boolean delPort(String bridgeName, String portName)
	{
		try
		{
			runSwitchCommand(DEL_PORT + " " + bridgeName + " " + portName);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return true;
	}

	@Override
	public boolean hasSwitch(String name)
	{
		int res = 0;
		try
		{
			 res = runSwitchCommand("show | grep \"Bridge \"" + name);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return res == 0 ? true : false;
	}

	@Override
	public boolean deleteSwitch(String name)
	{
		boolean success = false;
		try
		{
			runSwitchCommand(DEL_BRIDGE + " " + name);
			success = true;
		}
		catch(IOException e) {
			throw new RuntimeException(e);
		}
		return success;
	}
	
	
	private int runSwitchCommand(String cmd) throws IOException
	{
		Process p = Runtime.getRuntime().exec("sudo " + VIRTUAL_SWITCH_CONTROLLER + " " + cmd);
		readError(p.getErrorStream());
		return p.exitValue();
	}
	
	
	/**
	 * Read error.
	 * 
	 * @param errorStream
	 *            the error stream
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private static void readError(InputStream errorStream) throws IOException
	{
		BufferedReader b = new BufferedReader(new InputStreamReader(errorStream));
		String error = "";
		for (String line = b.readLine(); line != null; line = b.readLine())
		{
			error += line;
		}
		if (error.length() > 0)
		{
			throw new IOException(error);
		}
	}

	@Override
	public boolean setController(String bridge, Protocol protocol, String target)
	{
		boolean success = false;
		try
		{
			runSwitchCommand(SET_CONTROLLER + " " + bridge + " " + protocol.name().toLowerCase() + ":" + target);
			success = true;
		}
		catch(IOException e) {
			throw new RuntimeException(e);
		}
		return success;
	}

	@Override
	public boolean delController(String name)
	{
		boolean success = false;
		try
		{
			runSwitchCommand(DEL_CONTROLLER + " " + name);
			success = true;
		}
		catch(IOException e) {
			throw new RuntimeException(e);
		}
		return success;
	}

	@Override
	public String getController(String name)
	{
		String controller = null;
		try
		{
			runSwitchCommand(GET_CONTROLLER + " " + name);
		}
		catch(IOException e) {
			throw new RuntimeException(e);
		}
		return controller;
	}

}
